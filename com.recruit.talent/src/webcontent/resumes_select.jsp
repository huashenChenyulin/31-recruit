<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-03-14 17:23:31
  - Description:
-->
<head>
<title>简历查看</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script
	src="<%= request.getContextPath() %>/Talent/js/bootstrapValidator.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/bootstrapValidator.min.css">
	
	
	<!-- 新增、编辑、查看简历通用CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/currency.css">
	<!-- 查看简历CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/resumes_select.css">
	<!-- 查看简历JS -->
	<script src="<%= request.getContextPath() %>/Talent/js/resumes_select.js"
	type="text/javascript"></script>

<%
    	String id=request.getParameter("id");
    	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
		String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
     
     %>
<style type="text/css">
	#content_centreV2{
		padding-left: 0px;
	}
	.table-striped th{
		padding: 0px!important;
		padding-left: 8px!important;
		padding-bottom: 8px!important;
		font-size: 13px;
	}
	
	
	/* 附件样式 */
	#resumes{
	   float: left;
       margin-left: 4%;
       margin-bottom: 5%;
       margin-top: -3%;
	}
	.recruitAcc{
		overflow: hidden;
			text-overflow:ellipsis;
			white-space: nowrap;
	}
</style>
</head>
<body>
	<div id="main">
		<!-- 左布局 -->
		<div id="main-left">
			<div id="content-top">
				<!-- 头部的个人信息 -->
				<div id="content-top-left">
					<table class="table">
						<caption id="name"></caption>
						<tbody>
							<tr>
								<td><samp id="talentpoolSex"></samp> <samp>|</samp> <samp
										id="study"></samp></td>
							</tr>

						</tbody>
					</table>
				</div>
				<div id="content-top-right">
					<table class="table">
						<tbody>
							<tr>
								<td><samp>创建人：</samp>
									<samp id="leaderName"></samp></td>
							</tr>
							<tr>
								<td><samp>人才状态：</samp>
									<samp id="state"></samp></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="content-top-jumpUpdate">
					<a href=""><i class="layui-icon">&#xe642;</i>编辑</a>
				</div>
				<div id="content-top-info" >
					<table class="table">
						<tbody>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/date.png">
									</samp> <samp id="birthTime"></samp></td>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/address.png">
									</samp> <samp id="now_site"></samp></td>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/phone.png">
									</samp> <samp id="phone"></samp></td>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/email.png">
									</samp> <samp id="email"></samp></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- 中部的求职意向信息 -->
			<div id="content-centre">
				<table class="table">
					<caption>求职意向</caption>
					<thead>
						<tr>
							<th>意向职位</th>
							<th>意向职位类别</th>
							<th>期望薪资</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="intentionPosition"></td>
							<td id="intentionPositionInterior"></td>
							<td id="expectationPay"></td>
						</tr>

					</tbody>
				</table>

				<table class="table">
					<thead>
						<tr>
							<th>工作意向地(1)</th>
							<th>工作意向地(2)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="intention_site"></td>
							<td id="intention_site_v2"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- 尾部的面试情况信息 -->
			<div id="centent-bottom">
				<table class="table">
					<caption>面试情况</caption>
					<thead>
						<tr>
							
							<th>面试部门</th>
							<th>面试职位</th>
							<th>最终环节</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							
							<td id="orgBranch"></td>
							<td id="interviewPositon"></td>
							<td id="abandonLink"></td>
						</tr>
					</tbody>
				</table>
				<table class="table">
					<thead>
						<tr>
							<th>原因</th>
							<th>情况描述</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="abandonCondition" style="width: 34%;"></td>
							<td id="abandonCause"></td>
						</tr>
					</tbody>
				</table>
			</div>


		</div>
		<!-- 右布局 -->
		<div id="main-right">
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="preview"
					name="preview" onclick="preview2()">下载简历附件</button>
			</div>
			<div id="resumes">
				<p style="font-weight: bold;margin-top: 3%;">
					附件：
				</p>
				<div id="file_upload">
					
				</div>
			</div>
			<div id="hr">
				<hr>
			</div>

			<div id="content_centreV2">
				<form id="form">
					<div class="col-lg-11">
						<label>添加到我的工单</label> <select class="form-control"
							id="positionName" onchange="officeId(this.value)" onblur="validator()">
							<option value="0">职位</option>
						</select> <select class="form-control" id="odd_numbers" name="odd_numbers"
							onchange="position(this.value)" onblur="validator()">
							<option disabled selected>工单号</option>

						</select> 
					</div>
				</form>
				<div id="content_centre_button">
					<button type="button" class="btn btn-warning" id="add" name="add"
						onclick="add()">添 加</button>
				</div>
			</div>
			<div id="hr">
				<hr>
			</div>

			<div id="content_bottomV2" style="width: 90%;">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>已在工单列表</th>
							<th>招聘负责人</th>
							<th>候选人状态</th>
						</tr>
					</thead>
					<tbody id="tt">

					</tbody>
				</table>
			</div>
			<div style="float: left;margin-left: 4%;margin-bottom: 5%;">
				<label style="font-weight: bold;">标签：</label>
				<div id="content_label2"></div>
			</div>
		</div>
		<a href="" download=""></a>
	</div>
	
	<!--模态层-->
	<div id="" class="talent-detail-modal">
		<div id="" class="talent-detail-mask-view"></div>
		<div id="" class="talent-detail-cont-wrapper-cont">
			<div id="left-area">
				<div id = "header">
					<span id="candidateName"></span>
				</div>
				<!-- 初筛 -->
				<div class="interview first_data" style="display: none;">
					<div class="interview1_left interview_info ">
						<b>初筛</b>
					</div>
					<div>
						<p id="interview1_right"></p>
					</div>
				</div>
				<!-- 电话面试 -->
				<div class="interview phone_data" style="display: none;">
					<div class="interview2_left interview_info ">
						<b>电话面试</b>
					</div>
					<div class="interview_info_right">
						<p id="interview_phone_right"></p>
					</div>
					<div class="interview_right_bottom">
						<p id="interview_phone_right_bottom"></p>
					</div>
				</div>
				<!-- 人资面试-->
				<div class="interview human_data" style="display: none;">
					<div class="interview2_left interview_info ">
						<b>人资面试</b>
					</div>
					<div class="interview_info_right">
						<p id="interview_human_right"></p>
					</div>
					<div class="interview_right_bottom">
						<p id="interview_human_right_bottom"></p>
					</div>
				</div>
				<!-- 部门面试-->
				<div class="interview org_data" style="display: none;">
					<div class="interview2_left interview_info ">
						<b>部门面试</b>
					</div>
					<div class="interview_right_nothave">
						<p id="interview_org_right"></p>
					</div>
				</div>
				<!-- 待入职-->
				<div class="interview waiting_data" style="display: none;">
					<div class="interview2_left interview_info ">
						<b>待入职</b>
					</div>
					<div class="interview_right_bottom">
						<p id="interview_waiting_right"></p>
					</div>
				</div>
				<!-- 放弃原因-->
				<div class="interview fangqi_data" style="display: none;">
					<div class="interview2_left interview_info ">
						<b>放弃原因</b>
					</div>
					<div class="interview_right_bottom">
						<p id="interview_fangqi_right"></p>
					</div>
				</div>
			</div>

			<!-- 右半部分 -->
			<div id="right-area">
				<!-- 内容 -->
				<div class="assess-top" align="center">评估表</div>
				<div class="assess">
				</div>
			</div>
			<div id="btn-cont" class="layui-icon" onclick="closeMode()">&#x1006;</div>
		</div>
	</div>
	
	
	
	<script type="text/javascript">
	
		
		
		var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	    var empname="<%=empname %>"	;//赋值当前登录人姓名
	    var empid="<%=empid %>";	//登录人id
	    var ID="<%=id %>";	//接收其它页面传来的值
	    
	    //跳转编辑简历页面
	    $("#content-top-jumpUpdate a").attr("href","<%= request.getContextPath() %>/Talent/resumes_update.jsp?talentId="+ID+"");
	    
	    	
	    	
	</script>
</body>
</html>
