<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-12 10:20:37
  - Description:
-->
<head>
<title>新增简历</title>
<%
	String id=request.getParameter("id");
	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
		String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<!--  新增、编辑、查看简历通用CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/currency.css">
	<!--  新增、编辑简历通用CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/insertAndUpdate.css">
	<link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/Talent/css/label.css">


<script
	src="<%= request.getContextPath() %>/Talent/js/distpicker.data.js"
	type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/Talent/js/distpicker.js"
	type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/Talent/js/main.js"
	type="text/javascript"></script>


<!-- 非空验证CSS+js -->
<script
	src="<%= request.getContextPath() %>/Talent/js/bootstrapValidator.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/bootstrapValidator.min.css">

	<!-- 上传附件 -->
 <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
	
	
<style type="text/css">
	.resume{
    		cursor: pointer;
    	}
	.resume:HOVER{
		color: #1689e4;
	}
	
#resumes{
	float: left;
       margin-left: 5%;
}
</style>
</head>
<body>
	<div id="main">
		<!-- 左布局 -->
		<div id="main-left">
			<form id="form">
				<div id="content">
					<div class="col-lg-6">
						<label>招聘负责人姓名<samp>*</samp></label> <input type="text"
							class="form-control" id="leaderName" name="leaderName" disabled>
						<input type="hidden" id="leaderId" name="leaderId"
							value=""> <input type="hidden" id="isInworkorder"
							name="isInworkorder" value="0">
						<input type="hidden" id="resumeId"
							value="">
							<input type="hidden" id="candId"
							value="">
					</div>
					<div class="col-lg-6">
						<label>人才姓名<samp>*</samp></label> <input type="text"
							class="form-control name" id="name" name="name" onblur="validator()">
					</div>
				</div>

				<!-- 求职意向 -->
				<div id="content_top">
					<h4>求职意向</h4>
					<div class="col-lg-6">
						<label>意向职位<samp>*</samp></label> <input type="text"
							class="form-control intentionPosition" id="intentionPosition"
							name="intentionPosition" onblur="validator()">
					</div>
					<div class="col-lg-6">
						<label>人才电话<samp>*</samp></label> <input type="text"
							class="form-control phone" id="phone" name="phone" onblur="validator()">
					</div>
					<div class="col-lg-6">
						<label>意向职位类别<samp>*</samp></label> <select class="form-control intentionPositionInterior"
							id="intentionPositionInterior" name="intentionPositionInterior" onblur="validator()">
							<option></option>

						</select>
					</div>
					<div class="col-lg-6 parentCls">
						<label>人才邮箱<samp>*</samp></label> <input type="text"
							class="form-control inputElem email" id="email" name="email" onblur="validator()">
					</div>
					<div class="col-lg-6">
						<label>期望薪资<samp>*</samp></label> <select class="form-control"
							id="expectationPay" name="expectationPay" onblur="validator()"> 
							<option></option>

						</select>
					</div>
				</div>

				<div id="content_address">
					<label class="currency">工作意向地(1)<samp>*</samp></label>
					<div id="distpicker5">
						<select class="form-control province province_1" id="province"
							name="province" onblur="validator()"></select> <select class="form-control city city_1"
							id="city" name="city" onblur="validator()"></select> <select
							class="form-control district" id="district" name="district"></select>
					</div>
				</div>
				<div id="content_address_2">
					<label class="currency">工作意向地(2)</label>
					<div id="distpicker1">
						<select class="form-control province" id="province_2"
							name="province_2"></select> <select class="form-control city"
							id="city_2" name="city_2"></select> <select
							class="form-control district" id="district_2" name="district_2"></select>
					</div>
				</div>


				<!-- 人才信息 -->
				<div id="content_centre">
					<h4>人才信息</h4>
					<div class="col-lg-4">
						<label>学历<samp>*</samp></label> <select class="form-control study" id="study"
							name="study" onblur="validator()">
							<option></option>

						</select>
					</div>
					<div class="col-lg-4">
						<label>出生日期</label> <input type="text" class="form-control"
							id="birthTime" name="birthTime" placeholder="yyyy-MM-dd">
					</div>
					<div class="col-lg-4">
						<label>性别</label> <select class="form-control" id="talentpoolSex"
							name="talentpoolSex">
							<option></option>
							<option value="1">男</option>
							<option value="2">女</option>
						</select>
					</div>
				</div>
				<div id="content_nowSite">
					<label class="currency">现居住地<samp>*</samp></label>
					<div id="distpicker2">
						<select class="form-control province province_nowSite" id="province_nowSite"
							name="province_nowSite" onblur="validator()"></select> <select
							class="form-control city city_nowSite" id="city_nowSite" name="city_nowSite" onblur="validator()"></select>
						<select class="form-control district" id="district_nowSite"
							name="district_nowSite"></select>
					</div>
				</div>

				<!-- 人才面试情况 -->
				<div id="content_bottom">
					<h4>面试情况</h4>
				
					<div class="col-lg-6">
						<label>面试部门</label> <input type="text" class="form-control"
							id="orgBranch" onmouseover="selbox($(this))" onclick="getOrg(this)" data="" alias="" placeholder="部门选择">
					</div>
					<div class="col-lg-6">
						<label>面试职位</label> <input type="text" class="form-control"
							id="interviewPositon" name="interviewPositon">
					</div>
					<div class="col-lg-6">
						<label>最终环节</label> <select class="form-control" id="abandonLink"
							name="abandonLink">
							<option></option>
							<option value="1">初筛</option>
							<option value="2">电话面试</option>
							<option value="3">人力面试</option>
							<option value="4">部门面试</option>
							<option value="5">待入职环节</option>
						</select>
					</div>
					<div class="col-lg-6">
						<label>原因</label> <select class="form-control"
							id="abandonCondition" name="abandonCondition">
							<option></option>

						</select>
					</div>
				</div>
				<div id="content_textarea">
					<div>
						<label class="currency">情况描述</label>
						<textarea class="form-control" rows="3" id="abandonCause"
							name="abandonCause"></textarea>
					</div>

				</div>
			</form>
		</div>

		<!-- 右布局 -->
		<div id="main-right">
			<!-- 按钮 -->
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="success"
					name="success" onclick="selectResume()">保  存</button>
				<button type="button" class="btn btn-default" id="reset" onclick="location.reload();">重 置</button>
			</div>
			<!-- 头部：渠道、供应商、人才状态 -->
			<div id="content_centre">
				<form id="form2">
					<div class="col-lg-6">
						<label>简历渠道<samp>*</samp></label> <select class="form-control channel"
							id="channel" name="channel" onchange="suppliers(this.value)" onblur="validator()">
							<option></option>
						</select>
					</div>
					<div class="col-lg-6">
						<label>供应商</label><select class="form-control"
							id="supplier" name="supplier">
							<option></option>
						</select>
					</div>
					<br>
					<div class="col-lg-10">
						<label>人才状态</label> <select class="form-control"
							id="state" name="state">
							<option value="1">维护阶段</option>
						</select>
					</div>
				</form>
			</div>
			<!-- 中部：上传 -->
			<div id="resumes">
				<label>附件:</label><br>
				<div id="file_upload" ></div>
			</div>
			<div id="content_upload">
				<div id="uploader" class="wu-example">
			        <div class="queueList">
			            <div id="dndArea" class="placeholder">
			                <div id="filePicker"></div>
			                <p>点击文件上传或将文件拖到这里</p>
			                <p style="font-size: 14px;">支持：word、excel、html、ptf、jpg等格式</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
			</div>

			<!-- 尾部：添加标签 -->
			<div id="content_label">
				<div id="content_label_button">
					<button type="button" class="btn btn-warning" id="add_label">添加标签</button>
				</div>
				<div id="content_label2"></div>
			</div>
		</div>

	</div>
	<!-- 模态框 -->
	<div id="modalFrame">
	
	</div>
	
	<!-- 标签 -->
	<div id="main-center">
		<div id="content-one_label">
			<ul class="list-group">
			</ul>
		</div>
		<div id="content-Two_label"></div>
	</div>
	
	<!-- 新增简历JS -->
	<script src="<%= request.getContextPath() %>/Talent/js/synchronization.js"
		type="text/javascript"></script>
		
	<script type="text/javascript">
		
		//接收页面传过来的id
		var id="<%=id %>" ;
		//简历id
		var resume_id="";
		 
		 $("#resumes").css("display","none");
		
		function aHref(id){
		 	var url="<%= request.getContextPath() %>/Talent/resumes_select.jsp?id="+id;
		 	window.open(url);
		 }
		
		 //查询简历信息
	    	function select_talent(){
	    		var json={
	    			id:id
	    		};
	    		$.ajax({
	    			url:"com.recruit.talent.recruitCandidate.selectCandidate.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				resume_id=data.cand[0].recruitResume.id;
		  				$("#resumeId").val(data.cand[0].recruitResume.id);
		  				$("#candId").val(data.cand[0].id);
		  				$("#leaderId").val(data.cand[0].recruiterId);
		  				$("#leaderName").val(data.cand[0].recruiterName);
		  				$("#channel").val(data.cand[0].recruitChannels);
		  				$("#name").val(data.cand[0].recruitResume.candidateName);
		  				$("#phone").val(data.cand[0].recruitResume.candidatePhone);
		  				$("#study").val(data.cand[0].recruitResume.degree);
		  				$("#talentpoolSex").val(data.cand[0].recruitResume.gender);
		  				$("#email").val(data.cand[0].recruitResume.candidateMail);
		  				$("#expectationPay").val(data.cand[0].recruitResume.expectedSalary);
		  				$("#birthTime").val(data.cand[0].recruitResume.birthDate);
		  				
		  				
		  				//把已知的渠道跟供应商传到suppliers（）方法里面
		  				suppliers(data.cand[0].recruitChannels,data.cand[0].recruitSuppliers);
		  				
		  				 //判断是否存在附件
						 if(data.recruitAcc.length <= 0){
						 
							}else{
								$("#resumes").show();
								for(var i=0;i<data.recruitAcc.length;i++){
									var accessoryUrl=basePath+"/"+data.recruitAcc[i].accessoryUrl; //拼接附件路径
									$("#file_upload").append("<p id='recruitAcc"+data.recruitAcc[i].id+"'><a href="+accessoryUrl+"  target='_blank'>"+data.recruitAcc[i].accessoryName+"</a>"
												+"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' "
												+"onclick='resumnedelete("+data.recruitAcc[i].id+",\""+data.recruitAcc[i].accessoryPath+"\",\""+data.recruitAcc[i].accessoryName+"\")'> X </a></p>");
								}
								
							}
					} 
				});
		}
	</script>

</body>
<script src="<%= request.getContextPath() %>/Talent/js/resumeupload.js"></script>
</html>