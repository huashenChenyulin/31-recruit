<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.UserObject"%>
<%@page import="com.eos.foundation.eoscommon.ResourcesMessageUtil"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="commonj.sdo.*,java.util.*,java.text.SimpleDateFormat"%>
<%@page import="com.eos.web.taglib.util.XpathUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wenjin
  - Date: 2018-03-19 15:30:58
  - Description:
-->

<head>
<title>校招人才搜索页面</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link type="text/css" rel="styleSheet"
	href="<%= request.getContextPath() %>/Talent/css/resume_conditional_select.css" />
	<style type="text/css">
		#degree,#degree2{
			width: 35%;
    		display: initial;
		}
		#degree2{
			margin-left: 0px;
		}
	</style>
</head>
<body>
	<div id="content_left" style="height: 720px;">
		<p>毕业时间</p>
		<input type="text" class="layui-input" id="graduationdate" name="graduationdate" style="width: 83%;height: 34px;margin-left: 25px;" placeholder="yyyy-MM-dd">

		<p>宣讲学校</p>
		<input type="text" class="form-control" id="recruitmentcampus"
			name="recruitmentcampus">

		<p>面试职位类别</p>
		<select class="form-control" id="campusposition"
			name="campusposition">
			<option value=""></option>
		</select>
		
		<p>推荐岗位</p>
		<input type="text" class="form-control" id="recommendedpost"
			name="recommendedpost">
			
		<div>
			<p>学历</p>
			<select class="form-control" id="degree" name="degree" title>
				<option value=""></option>
			</select>
			<span style="margin-left: 10px;margin-right: 10px;">—</span>
			<select class="form-control" id="degree2" name="degree2" title>
			</select>
		</div>

		<p>性别</p>
		<select class="form-control" id="gender" name="gender" style="width: 83%;">
			<option value=""></option>
			<option value="1">男</option>
			<option value="2">女</option>
		</select>
		
		<p>专业</p>
		<input type="text" class="form-control" id="major"
			name="major">
			
		<p>工作意向地</p>
		<input type="text" class="form-control" id="exceptedworkarea" name="exceptedworkarea">
		<button type="button" class="btn btn-primary" id="select"
			name="select" onclick="selectsubmit()">搜 索</button>
		<button type="button" class="btn btn-default" id="clean"
		name="clean" onclick="clean()">重置</button>


	</div>
	<div id="content_topright"></div>

	<div id="content_bottomright">
		<ul id="mytalentTab" class="nav nav-tabs">
			<li class="active" onclick=""><a href="" data-toggle="tab">
					校招库 </a></li>
		</ul>
		<div id="talentTop">
			<ul id="talentsend" style="width: 62%;">
				<li>基本信息</li>
			</ul>
			<ul id="talentdate" style="width: 18%;">
				<li>更新日期</li>
			</ul>
			<ul id="talentstuta">
				<li>操作</li>
			</ul>
		</div>
		<div id="content"></div>
		<div id="pagination"></div>
	</div>
	<%
	//获取当前招聘负责人ID和名字
	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	%>


	<script type="text/javascript">
	
			//回车键触发点击事件
			$("body").keydown(function() {
				var event=arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异
	             if (event.keyCode == "13") {//keyCode=13是回车键
	                 $('#select').click();
	             }
	         });
	
			//初始数据
			selectsubmit();
			//获取数据字典
			dictionarycombox();
			//DateTime 转换为 Date
			Date.prototype.format = function(fmt){
		    var o = { 
		        "M+" : this.getMonth()+1,                 //月份 
		        "d+" : this.getDate(),                    //日 
		        "h+" : this.getHours(),                   //小时 
		        "m+" : this.getMinutes(),                 //分 
		        "s+" : this.getSeconds(),                 //秒 
		        "q+" : Math.floor((this.getMonth()+3)/3), //季度 
		        "S"  : this.getMilliseconds()             //毫秒 
		    }; 
		    if(/(y+)/.test(fmt)) {
		    	fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
		    }
		    for(var k in o) {
		    	if(new RegExp("("+ k +")").test(fmt)){
		             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
		         }
		     }
		    return fmt; 
			};
			
			
			layui.use('laydate', function(){
  				var laydate = layui.laydate;
	  			laydate.render({
			   	 elem: '#graduationdate'
			   	 ,type: 'month'
				});
 			});
			//查询校招库
			function selectsubmit(){
			
				var graduationdate =$("#graduationdate").val();
				var recruitmentcampus =$("#recruitmentcampus").val();
				var campusposition =$("#campusposition").val();
				var degree =$("#degree").val();
				var gender =$("#gender").val();
				var major =$("#major").val();
				var exceptedworkarea =$("#exceptedworkarea").val();
				var recommendedpost =$("#recommendedpost").val();
				var degree2 =$("#degree2").val();
		    	
				var json={
					graduationdate:graduationdate,
					recruitmentcampus:recruitmentcampus,	
					campusposition:campusposition,
					degree:degree,
					gender:gender,
					major:major,
					exceptedworkarea:exceptedworkarea,
					recommendedpost:recommendedpost,
					degree2:degree2
				};
			 	$.ajax({
					url:"com.recruit.talent.campusrecruitment.selectCampustalent.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
						console.log(data);
						if(data.resultanlt.length<1){
							$("#content").empty();
							$("#content").append('<p style="text-align: center;color: #9E9E9E;font-weight: 100;margin-bottom: 10px;">无数据</p>');
							$("#pagination").css("display","none");
							return;
						}
						var dataArray = new Array();
						dataArray = data.resultanlt;
						layui.use(['laypage','layer'], function(){
						 var laypage= layui.laypage;
						 laypage.render({
						    elem: 'pagination'
						    ,count: dataArray.length
						    ,limit: 5
						    ,jump: function(obj){
						      //模拟渲染
						      	document.getElementById('content').innerHTML = function(){
						        var arr = []
						        ,thisData = dataArray.concat().splice(obj.curr*obj.limit - obj.limit, obj.limit);
						        layui.each(thisData, function(index, item){
						        //性别
						         var gender = sexstatus(item.gender);
						        
						         //学历
						         var degree = educationstatus(item.degree);
						         
						         //职位类别
						         var catagory=Positioncatagory(item.campus_position);
						         
						         //datatime转换成data
						          var oldTime = (new Date(''+item.update_time+'')).getTime();
								  var curTime = new Date(oldTime).format("yyyy-MM-dd");
								 //判断是否有第二工作意向地
								 var wroke_b ="";
								 //防止当第二工作意向的为NULL时，显示在页面上
								 var excepted_work_b =item.excepted_work_area_b;
								 if(excepted_work_b == null || excepted_work_b == ""){
								 	wroke_b = "";
								 }else{
								 	wroke_b =item.excepted_work_area_b;
								 }
								 
								 var recruitment_campus = "";
								 if(item.recruitment_campus == null || item.recruitment_campus ==""){
									recruitment_campus = "";
								 }else{
								 	recruitment_campus =item.recruitment_campus;
								 }
						          arr.push('<table class="talentTable" onclick="click1('+item.id+')" >'+
						          '<tr class="tr">'+
						          '<td class="td"  id="talentname" style="font-size: 14px;height:35px;padding-left: 15px;">'+item.candidate_name+'</td>'+
						          '<td class="td" colspan="11" id="sex" style="font-size: 14px;">'+gender+'</td>'+
						          '<td class="td"  id="date" style="font-size: 13px;padding-left:1%;">'+curTime+'</td>'+
						          '<td class="td"  id="talentdo" style="font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="addmyorder('+item.id+')" style="color: #f0ad4e;font-weight: bold;margin-left:8%;">编辑</a></td>'+
						          '</tr>'+
						          '<tr class="tt" style="color:#999999;font-size: 12px;">'+
						          '<td><i class="layui-icon" style="font-size: 16px;padding-left: 15px;">&#xe705;</i>&nbsp;&nbsp;'+degree+'</td>'+
						          '<td colspan="11">'+catagory+'</td>'+
						          '</tr>'+
						          '<tr class="tt" style="color:#999999;font-size: 12px;">'+
						          '<td style="padding-bottom: 10px;"><i class="layui-icon" style="font-size: 15px;padding-left: 15px;">&#xe6af;</i>&nbsp;&nbsp;'+item.major+'</td>'+
						          '<td colspan="11" style="padding-bottom: 10px;"><i class="layui-icon" style="font-size: 16px;">&#xe622;</i>&nbsp;&nbsp;'+item.excepted_work_area_a+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+wroke_b+'</td>'+
						          '</tr>'+
						          '<tr class="tt" style="border-top:1px #e6e6e6 solid;">'+
						          '<td style="color:#999999;font-size:13px;padding-left: 15px;">职位：<span style="color:#333">'+item.recommended_post+'</span></td>'+
						          '<td style="color:#999999;font-size: 12px;">毕业时间：<span style="color:#333">'+item.graduation_date+'</span></td>'+
						          '<td colspan="11" style="color:#999999;font-size: 12px;">宣讲学校：<span style="color:#333">'+recruitment_campus+'</span>&nbsp;|&nbsp;毕业学校：<span style="color:#333">'+item.college+'</span></td>'+
						          '</tr>'+
						          '</table>'+
						          '<hr>'
						          );
						          
						        });
						        return arr.join('');
						      }();
						    }
						  });
						});
					}
				});
		 	}
		 	
		 	$("#degree").change(function(){
	  			//获取选中的下拉框的值
	  			var value=$(this).children('option:selected').val();
	  			
	  			selectDegree(value);
	  		});
		
		//查询学历范围
			function selectDegree(degree){
				$.ajax({
					url: "com.recruit.talent.selectTalentPool.selectDegree.biz.ext",
					type:'POST',
					success:function(data){
					//清除id是degree2的下拉框中的option
					$("#degree2 option").remove();
					//定义接收拼接的变量名
					var option="";
					//拼接
					option+='<option value=""></option>';
					//循环
						for(var i=0;i<data.redegree.length;i++){
						//判断如果循环出来的值小于等于接收到的值就进行拼接
							if(data.redegree[i].dictID<=degree){
								option+='<option value="'+data.redegree[i].dictID+'">'+data.redegree[i].dictName+'</option>';
							}
						}
						$("#degree2").append(option);
					}
				});
			}
			
			$("#degree").change(function(){ 

		         //获取下拉框的文本值
		
		         var checkText=$(this).find("option:selected").text();
		
		         //修改title值
		
		        $(this).attr("title",checkText);
		
		   });
		   $("#degree2").change(function(){ 

		         //获取下拉框的文本值
		
		         var checkText=$(this).find("option:selected").text();
		
		         //修改title值
		
		        $(this).attr("title",checkText);
		
		   });
		 	
		 	
		//获取下拉框数据字典数据
		function dictionarycombox() {
			$.ajax({
				url : "com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
				type : 'POST',
				success : function(data) {
					//职位类别
					for (var i = 0; i < data.reposition.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reposition[i].dictID);
						$(option).text(data.reposition[i].dictName);
						$('#campusposition').append(option);
					}
					//学历
					for (var i = 0; i < data.redegree.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.redegree[i].dictID);
						$(option).text(data.redegree[i].dictName);
						$('#degree').append(option);
					}
				}
			});
		}
		function clean(){
			$('#graduationdate').val("");
			$('#recruitmentcampus').val("");
			$('#campusposition').val("");
			$('#recommendedpost').val("");
			$('#degree').val("");
			$('#degree2').val("");
			$('#gender').val("");
			$('#major').val("");
			$('#exceptedworkarea').val("");
		}
		function click1(id){
			window.open("<%= request.getContextPath()%>/Talent/campusrecruitment_details.jsp?id="+id);
		}
		
		function isok(){
			//初始查询
					selectsubmit();
		}
	</script>
</body>
</html>
