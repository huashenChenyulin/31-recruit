		
	$(function() {
			queryDisease();//默认加载下拉框取值
			sublable();//默认加载一级标签
			secondLevel(1);//默认加载二三级标签

		});

		//时间控件
    	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#birthTime' //指定元素
		    ,value: '1990-01-01' //必须遵循format参数设定的格式
		    	,isInitValue: false //是否允许填充初始值，默认为 true
		  });
		});
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
		
		
    	var resume_id;//接收查询到的简历id
    	var talent_id;//接收查询到的人才库id
    	
    	//查询是否有相同数据
    	function selectResume(){
    		validator();
    		//非空判断
    		if(isPass==false){
    			layer.msg(arr[0]);
    			return;
    		}
    		var name =$("#name").val();//获取人才姓名
    		var phone =$("#phone").val();//获取人才电话
    		
    		var json={
    			name:name,
    			phone:phone
    		};
    		$.ajax({
				url: "com.recruit.talent.talent.selectResume.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					if(data.success=='ok'){
						isEnclosureExist();//判断是否还有附件未上传
					}else if(data.success=='-1'){
						resume_id=data.result[0].resumeId;
						talent_id=data.result[0].talentId;
						var select=("<div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"
							+"<div class='modal-dialog'>"
							+"	<div class='modal-content'>"
							+"	<div class='modal-header'>"
							+"	<h4 class='modal-title' id='myModalLabel'>温馨提示！</h4></div>"
							+"		<div class='modal-body'>该简历已存在，是否更改，更改请点击查看简历对比！"
							+"		<a "
										+ "' class='resume' target='_Blank' onclick='aHref("+resume_id+")'>查看简历</a></div>"
										+ "		<div class='modal-footer'>"
										+ "			<button type='button' class='btn btn-default' data-dismiss='modal'>关闭</button>"
										+ "		</div>" + "	</div>" + "</div>" + "</div>");
								$("#modalFrame").html(select);
								$("#myModal").modal('show');//打开模态框
					}else if(data.success=='-2'){
						resume_id=data.result[0].resumeId;
						talent_id=data.result[0].talentId;
						isEnclosureExistV2(resume_id);
					}
				}
			});
		}

		//下拉框赋值
		function queryDisease() {
			$.ajax({
				url : "com.recruit.talent.talent.queryDictionary.biz.ext",
				type : 'POST',
				success : function(data) {
					//意向职位类别循环拼接赋值
					for (var i = 0; i < data.category.length; i++) {
						$("#intentionPositionInterior").append(
								"<option value='"+data.category[i].dictID+"'>"
										+ data.category[i].dictName
										+ "</option>");
					}
					//学历循环拼接赋值
					for (var i = 0; i < data.education.length; i++) {
						$("#study").append(
								"<option value='"+data.education[i].dictID+"'>"
										+ data.education[i].dictName
										+ "</option>");
					}
					//期望薪资循环拼接赋值
					for (var i = 0; i < data.salary.length; i++) {
						$("#expectationPay")
								.append(
										"<option value='"+data.salary[i].dictID+"'>"
												+ data.salary[i].dictName
												+ "</option>");
					}
					//渠道循环拼接赋值
					for (var i = 0; i < data.channel.length; i++) {
						$("#channel").append(
								"<option value='"+data.channel[i].dictID+"'>"
										+ data.channel[i].dictName
										+ "</option>");
					}
					//放弃原因循环拼接赋值
					for (var i = 0; i < data.reason.length; i++) {
						$("#abandonCondition")
								.append(
										"<option value='"+data.reason[i].dictID+"'>"
												+ data.reason[i].dictName
												+ "</option>");
					}
				}
			});
		}

		//根据渠道获取供应商
		function suppliers(id) {
			var json = {
				parentid : id
			};
			if(id==null || id==""){
				$("#supplier")[0].length = 0;
			}else{
				$.ajax({
					url : "com.recruit.talent.talent.queryDictionary.biz.ext",
					type : 'POST',
					data : json,
					success : function(data) {
						$("#supplier")[0].length = 0;
						//供应商循环拼接赋值
						for (var i = 0; i < data.supplier.length; i++) {
							$("#supplier").append(
									"<option value='"+data.supplier[i].dictid+"'>"
											+ data.supplier[i].dictname
											+ "</option>");
						}
					}
				});
			}
		}

		
		//附件是否还有未上传的
		function isEnclosureExist(){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				submit();//保存方法
				
			}
		}
		
		//附件是否还有未上传的
		function isEnclosureExistV2(id){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				updateAndAdd(id);//保存方法
				
			}
		}
		