		
		$(function() {
			queryDisease();//默认加载下拉框取值
			sublable();//默认加载一级标签
			secondLevel(1);//默认加载二三级标签

		});

		//时间控件
    	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#birthTime' //指定元素
		  });
		});
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
		
		
		//接收选中的标签
        var testMap={};
        
		$("#add_label").click(function(){
			$("#main-center").show(250);
		});
		$("#main-center").mouseleave(function(){
		   $(this).hide(250);
		});
		
		
		//查询一级标签
		function sublable(){
			var json={
				isSublable:'1'
			};
			$.ajax({
				url: "com.recruit.talent.label.selectParentsLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					for(var i=0;i<data.sublabel.length;i++){
						$(".list-group").append("<li class='list-group-item' id='one_label"+i+"' value='"+data.sublabel[i].id+"' onmouseover=gettext(\'one_label"+i+"\',this.value)>"+data.sublabel[i].lable+"<i class='layui-icon xe623'>&#xe623;</i></li>");
					}
					$("#one_label0").css("background-color","#ffffff");
					$("#one_label0").css("border-right-color","#ffffff");
					$("#one_label0 i").css("color","#417fd6");
				}
			});
		}
		
		//
		function gettext(id,label_id){
			//触发时改变样式
			$(".list-group-item").css("background-color","#f2f2f2");
			$(".list-group-item").css("border-right-color","#ddd");
			$(".xe623").css("color","#616161");
			
			$("#"+id).css("background-color","#ffffff");
			$("#"+id).css("border-right-color","#ffffff");
			$("#"+id+" i").css("color","#417fd6");
			
			secondLevel(label_id);
			
		}
		
		
		
		//查询二三级标签
		function secondLevel(id){
			var json={
				lable_id:id
			};
			$.ajax({
				url: "com.recruit.talent.label.secondLevelLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					$("#content-Two_label").empty();//清空数据
					 for(var i=0;i<data.result.length;i++){
						var content_Two_label ="<div class='panel-heading'>"
							+"<h6 class='panel-title'>"
							+"<a data-toggle='collapse' data-parent='#accordion' href='#collapseOne"+i+"'>"+data.result[i].lable+" >>"
							+"</a></h6></div>"
							+"<div id='collapseOne"+i+"' class='panel-collapse collapse in'>"
							+"<div class='panel-body' id='panel_label'><ul>";
							var recruitSublable = data.result[i].recruitSublable;//将二级标签中的子标签赋值到recruitSublable
							//判断  recruitSublable 是否空
							if(typeof(recruitSublable) != "undefined"){
								//如果  recruitSublable 长度大于0则循环加载子标签
								if(recruitSublable.length>0){
									for(var j=0;j<recruitSublable.length;j++){
										content_Two_label +="<li><a data='"+recruitSublable[j].id+"'><span>"+recruitSublable[j].lable+"</span></a></li>";
									}
								}
							}
							+"</ul></div></div></div>";
							
						$("#content-Two_label").append(content_Two_label);
						
					}
					 	var value="";
						var text="";
					  $("#panel_label ul li a").each(function(index){
						  $(this).click(function(){
							 /*value=$(this).children('input').val();*/
							 value =$(this).attr("data");
						     text =$(this).children('span').html();
						     var num=0;
						     for(var key in testMap){
						    	 if(key == value){
						    		 num++;
						    	 }
						    	 
						     }
						    
						     if(num == 0){
					    		 testMap[value] = text;
						    	 $("#content_label2").append("<span class='label label-info' data='"+value+"' id='"+value+"'>"+text+"<i class='layui-icon xe617'  onclick='delete_label(this)'  data='"+value+"'>&#x1006;</i></span>");
						     }
						  });
					  });
				}
			});
		}
		
		//提交数据到数据库
    	function submit(){
    		var resumeId=$("#resumeId").val();
    		var candId=$("#candId").val();
    		var leaderName =$("#leaderName").val();//招聘负责人
    		var leaderId =$("#leaderId").val();
    		var name =$("#name").val();//人才名
    		var intentionPosition =$("#intentionPosition").val();//意向职位
    		var phone =$("#phone").val();//电话
    		var intentionPositionInterior =$("#intentionPositionInterior").val();//意向职位类别
    		var email =$("#email").val();//邮箱
    		var expectationPay =$("#expectationPay").val();//期望薪资
    		var study =$("#study").val();//学历
    		var birthTime =$("#birthTime").val();//出生日期
    		var talentpoolSex =$("#talentpoolSex").val();//性别
    		var orgcentre =$("#orgBranch").val();//面试部门
    		var orgcentreId=$("#orgBranch").attr("alias");
    		var L1 = $("#orgBranch").attr("l1");
    		var L2 = $("#orgBranch").attr("l2");
    		var L3 = $("#orgBranch").attr("l3");
    		var L4 = $("#orgBranch").attr("l4");
    		var L1name = $("#orgBranch").attr("l1name");
    		var L2name = $("#orgBranch").attr("l2name");
    		var L3name = $("#orgBranch").attr("l3name");
    		var L4name = $("#orgBranch").attr("l4name");
    		var interviewPositon =$("#interviewPositon").val();//面试职位
    		var abandonLink =$("#abandonLink").val();//放弃环节
    		var abandonCondition =$("#abandonCondition").val();//放弃原因
    		var abandonCause =$("#abandonCause").val();//具体放弃情况描述
    		var supplier =$("#supplier").val();//渠道
    		var channel =$("#channel").val();//供应商
    		var state=$("#state").val();//状态
    		var isInworkorder=$("#isInworkorder").val();//是否已在工单
    		
    		//地址拼接
    		var address ="";
				if($('#province').val()!=""){
					address=$('#province').val();
				}
				if($("#city").val()!=""){
					address=$('#province').val()+"-"+$("#city").val();
				}
				if($("#district").val()!=""){
					address=$('#province').val()+"-"+$("#city").val()+"-"+$("#district").val();
				}
			
			var address_2 ="";
				if($('#province_2').val()!=""){
					address_2=$('#province_2').val();
				}
				if($("#city_2").val()!=""){
					address_2=$('#province_2').val()+"-"+$("#city_2").val();
				}
				if($("#district_2").val()!=""){
					address_2=$('#province_2').val()+"-"+$("#city_2").val()+"-"+$("#district_2").val();
				}
			
			var nowSite ="";
				if($('#province_nowSite').val()!=""){
					nowSite=$('#province_nowSite').val();
				}
				if($("#city_nowSite").val()!=""){
					nowSite=$('#province_nowSite').val()+"-"+$("#city_nowSite").val();
				}
				if($("#district_nowSite").val()!=""){
					nowSite=$('#province_nowSite').val()+"-"+$("#city_nowSite").val()+"-"+$("#district_nowSite").val();
				}
			
    		
    		
    		//获取标签id
    		var obj="";
    		for(var key1 in testMap){
					obj +=key1+",";
				} 
    		
    		//获取上传地址
    		var accessory_url="";
    		var accessory_name="";
    		var accessory_path="";
    		 $(".afiles").each(function(i,obj){
    			 accessory_url += getCaption($(obj).attr("href"))+"^";
    			 accessory_name += $(obj).text()+"^";
    			 accessory_path += $(obj).attr("data")+"^";
    		 });
    		
    		
    		
    		var json={
    			candId:candId,
    			resumeId:resumeId,
				leaderName:leaderName,	
				leaderId:leaderId,
				name:name,
				intentionPosition:intentionPosition,
				phone:phone,
				intentionPositionInterior:intentionPositionInterior,
				email:email,
				expectationPay:expectationPay,
				address:address,
				address_2:address_2,
				study:study,
				birthTime:birthTime,
				talentpoolSex:talentpoolSex,
				nowSite:nowSite,
				orgcentre:orgcentre,
				orgcentreId:orgcentreId,
				l1:L1,
				l1name:L1name,
				l2:L2,
				l2name:L2name,
				l3:L3,
				l3name:L3name,
				l4:L4,
				l4name:L4name,
				abandonLink:abandonLink,
				abandonCondition:abandonCondition,
				abandonCause:abandonCause,
				supplier:supplier,
				channel:channel,
				state:state,
				isInworkorder:isInworkorder,
				interviewPositon:interviewPositon,
				obj:obj,
				accessory_url:accessory_url,
				accessory_name:accessory_name,
				accessory_path:accessory_path
			};
			$.ajax({
				url: "com.recruit.talent.recruitCandidate.insertTalent.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					if(data.success=="ok"){
						layer.msg('保存成功');
						setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
							window.close();
						},1000);
					}else{
						layer.msg('保存失败');
					}
				}
			});
    	}
		
		
    	//删除已选好的标签
		function delete_label(numId){
			var id =$(numId).attr("data");
			/*$(numId).parent('span').empty();*/
			$(numId).parent().empty();
			delete testMap[id];
			var obj="#content_label2 #"+id;
			$(obj).remove();//清除
		}
		
    	
		//下拉框赋值
		function queryDisease() {
			$.ajax({
				url : "com.recruit.talent.talent.queryDictionary.biz.ext",
				type : 'POST',
				success : function(data) {
					//意向职位类别循环拼接赋值
					for (var i = 0; i < data.category.length; i++) {
						$("#intentionPositionInterior").append(
								"<option value='"+data.category[i].dictID+"'>"
										+ data.category[i].dictName
										+ "</option>");
					}
					//学历循环拼接赋值
					for (var i = 0; i < data.education.length; i++) {
						$("#study").append(
								"<option value='"+data.education[i].dictID+"'>"
										+ data.education[i].dictName
										+ "</option>");
					}
					//期望薪资循环拼接赋值
					for (var i = 0; i < data.salary.length; i++) {
						$("#expectationPay")
								.append(
										"<option value='"+data.salary[i].dictID+"'>"
												+ data.salary[i].dictName
												+ "</option>");
					}
					//渠道循环拼接赋值
					for (var i = 0; i < data.channel.length; i++) {
						$("#channel").append(
								"<option value='"+data.channel[i].dictID+"'>"
										+ data.channel[i].dictName
										+ "</option>");
					}
					//放弃原因循环拼接赋值
					for (var i = 0; i < data.reason.length; i++) {
						$("#abandonCondition")
								.append(
										"<option value='"+data.reason[i].dictID+"'>"
												+ data.reason[i].dictName
												+ "</option>");
					}
					select_talent();//查询简历信息
				}
			
			});
		}

		//根据渠道获取供应商
		function suppliers(id,suppliers) {
			var json = {
				parentid : id
			};
			if(id==null || id==""){
				$("#supplier")[0].length = 0;
			}else{
				$.ajax({
					url : "com.recruit.talent.talent.queryDictionary.biz.ext",
					type : 'POST',
					data : json,
					success : function(data) {
						$("#supplier")[0].length = 0;
						
						if(suppliers=="" || suppliers==null){
							suppliers = data.supplier[0].dictid;
						}
						//供应商循环拼接赋值
						for (var i = 0; i < data.supplier.length; i++) {
							$("#supplier").append(
									"<option value='"+data.supplier[i].dictid+"'>"
											+ data.supplier[i].dictname
											+ "</option>");
						}
						$("#supplier").val(suppliers);
					}
				});
			}
			
		}
		
		//删除附件
		function resumnedelete(id,filepath,name){
			var json = {
				id:id,
				filepath:filepath
			};
			if(id==0){
				$.ajax({
					url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
					type : 'POST',
					async: false,
					data : json,
					success : function(data) {
					}
				});
			}else{
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,content: '<div style="padding: 20px 75px;">附件名：'+name+'</div><div style="text-align: center;"><br>是否确定要删除该附件？</div>'
			        ,closeBtn: false
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
			        ,closeBtn: false//不显示关闭图标
			        ,yes: function(index){
			        	$.ajax({
							url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
							type : 'POST',
							async: false,
							data : json,
							success : function(data) {
								var obj="#recruitAcc"+id;
								$(obj).remove();//清除
								layer.msg('附件删除成功');
								layer.close(index);//关闭页面
								var text=$("#file_upload").html();
					        	if(text=="" || text==null){
					        		 $("#resumes").css("display","none");
					        	}
							}
						});
			        }
			        ,btn2: function(){
			          layer.closeAll();//关闭页面
			        }
			  }); 
			}
		}
		
		//当附件上传未提交时删除附件
		 $("#resumes").css("display","none");
		 function delectfile(upid,url){
			 resumnedelete(0,url);//删除服务器上的附件
	            var id="#"+upid.id;
	        	$(id).remove();//清除当前id
	        	var text=$("#file_upload").html();
	        	if(text=="" || text==null){
	        		 $("#resumes").css("display","none");
	        	}
    	}
		
		
		//截取简历地址
		function getCaption(obj){
		    var index=obj.lastIndexOf("resume/");
		    obj=obj.substring(index,obj.length);
		    return obj;
	    }

		
		//判断是否存在人才库
		function selectResume(){
			//非空判断
			validator();
     		if(isPass==false){
     			layer.msg(arr[0]);
     			return;
     		}
    		var json={
    			id:resume_id,
    		};
    		$.ajax({
				url: "com.recruit.talent.recruitCandidate.selectIsTalent.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					
					if(data.success!='ok'){
						submit();//提交数据
					}else{
						var select=("<div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"
							+"<div class='modal-dialog'>"
							+"	<div class='modal-content'>"
							+"	<div class='modal-header'>"
							+"	<h4 class='modal-title' id='myModalLabel'>温馨提示！</h4></div>"
							+"		<div class='modal-body'>该简历已存在，是否更改，更改请点击查看简历对比！"
							+"		<a "
										+ "' class='resume' target='_Blank' onclick='aHref("+resume_id+")'>查看简历</a></div>"
										+ "		<div class='modal-footer'>"
										+ "			<button type='button' class='btn btn-default' data-dismiss='modal'>关闭</button>"
										+ "		</div>" + "	</div>" + "</div>" + "</div>");
								$("#modalFrame").html(select);
								$("#myModal").modal('show');//打开模态框
							}
						}
					});
		}
		
		
		
		 var isPass=true;//标识是否通过验证
		   var isPassMsg="";//非空验证提示输出
		   var arr=[];
	   	//失去焦点验证
	   	function validator(){
	   		isPassMsg="";
	   		isPass=true;
	   		arr=[];
	   		//姓名
	  		 	if($('.name').val()==null || $('.name').val()=="" ){
	           	$(".name").css("border","1px solid red");
	           	isPassMsg+="请输入姓名,";
	           	isPass=false;
	           }else{
	           	$(".name").css("border","1px solid #ccc");
	           }
	   		
	   		//意向职位
	  		 	if($('.intentionPosition').val()==null || $('.intentionPosition').val()=="" ){
	           	$(".intentionPosition").css("border","1px solid red");
	           	isPassMsg+="请输入意向职位,";
	           	isPass=false;
	           }else{
	           	$(".intentionPosition").css("border","1px solid #ccc");
	           }
	   		
	   		//意向职位类别
	  		 	if($('.intentionPositionInterior').val()==null || $('.intentionPositionInterior').val()=="" ){
	           	$(".intentionPositionInterior").css("border","1px solid red");
	           	isPassMsg+="请选择意向职位类别,";
	           	isPass=false;
	           }else{
	           	$(".intentionPositionInterior").css("border","1px solid #ccc");
	           }
	   		
	   		//电话
				var phone=/^1\d{10}$/;//电话正则
	  		 	if($('.phone').val()==null || $('.phone').val()=="" || !phone.test($(".phone").val())){
	           	$(".phone").css("border","1px solid red");
	           	isPassMsg+="请输入正确的手机号码,";
	           	isPass=false;
	           }else{
	           	$(".phone").css("border","1px solid #ccc");
	           }
	  		 	
				//邮箱
				var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
	  		 	if($('.email').val()==null || $('.email').val()=="" || !email.test($(".email").val())){
	           	$(".email").css("border","1px solid red");
	           	isPassMsg+="请输入正确的邮箱格式，如：XXX@qq.com,";
	           	isPass=false;
	           }else{
	           	$(".email").css("border","1px solid #ccc");
	           }
	  		 	
	  		//期待薪资
	  		 	if($('#expectationPay').val()==null || $('#expectationPay').val()==""){
	           	$("#expectationPay").css("border","1px solid red");
	           	isPassMsg+="请选择期待薪资,";
	           	isPass=false;
	           }else{
	           	$("#expectationPay").css("border","1px solid #ccc");
	           }
	  		 	
	   		 //工作意向地的省份
	           if($('.province_1').val()==null || $('.province_1').val()=="" || $('.city_1').val()==null || $('.city_1').val()==""){
	           	$(".province_1").css("border","1px solid red");
	           	$(".city_1").css("border","1px solid red");
	           	isPassMsg+="请选择工作意向地,";
	           	isPass=false;
	           }else{
	           	$(".province_1").css("border","1px solid #ccc");
	           	$(".city_1").css("border","1px solid #ccc");
	           }
	           //学历
	  		 	if($('.study').val()==null || $('.study').val()=="" ){
	           	$(".study").css("border","1px solid red");
	           	isPassMsg+="请选择学历,";
	           	isPass=false;
	           }else{
	           	$(".study").css("border","1px solid #ccc");
	           }
	   		
	            //现居地的省份
	           if($('.province_nowSite').val()==null || $('.province_nowSite').val()=="" || $('.city_nowSite').val()==null || $('.city_nowSite').val()==""){
	           	$(".province_nowSite").css("border","1px solid red");
	           	$(".city_nowSite").css("border","1px solid red");
	           	isPassMsg+="请选择现居地,";
	           	isPass=false;
	           }else{
	           	$(".province_nowSite").css("border","1px solid #ccc");
	           	$(".city_nowSite").css("border","1px solid #ccc");
	           }
	           
	           //渠道
	  		 	if($('.channel').val()==null || $('.channel').val()==""){
	           	$(".channel").css("border","1px solid red");
	           	isPassMsg+="请选择渠道,";
	           	isPass=false;
	           }else{
	           	$(".channel").css("border","1px solid #ccc");
	           }
	  		 	
	  		//添加标签
	  		 	if($("#content_label2").html()==null || $("#content_label2").html()==""){
	  		 		isPassMsg+="标签至少选择一个,";
		           	isPass=false;
	  		 	}
	  		 	
	  		 	//文件上传
	  		 	if($("#file_upload").html()==null || $("#file_upload").html()==""){
	  		 		isPassMsg+="附件至少要上传一个";
		           	isPass=false;
	  		 	}
	  		 	arr=isPassMsg.split(",");
	   	} 