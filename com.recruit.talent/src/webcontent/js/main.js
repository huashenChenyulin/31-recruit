$(function () {

  'use strict';

  $('#distpicker1').distpicker({
	  autoSelect: false
  });

  $('#distpicker2').distpicker({
	  autoSelect: false
  });

  $('#distpicker3').distpicker({
    province: '浙江省',
    city: '杭州市',
    district: '西湖区'
  });

  $('#distpicker4').distpicker({
    placeholder: false
  });

  $('#distpicker5').distpicker({
    autoSelect: false
  });
  
  $('#distpicker6').distpicker({
	   autoSelect: false
  });

});
