	
		$(function(){
			
			sublable();
			secondLevel(1);
		});
		
		$("#add_label").click(function(){
			$("#main-center").show(250);
		});
		$("#main-center").mouseleave(function(){
		   $(this).hide(250);
		});
		
		
		
	//查询一级标签
		function sublable(){
			var json={
				isSublable:'1'
			};
			$.ajax({
				url: "com.recruit.talent.label.selectParentsLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					for(var i=0;i<data.sublabel.length;i++){
						$(".list-group").append("<li class='list-group-item' id='one_label"+i+"' value='"+data.sublabel[i].id+"' onmouseover=gettext(\'one_label"+i+"\',this.value)>"+data.sublabel[i].lable+"<i class='layui-icon xe623'>&#xe65a;</i></li>");
					}
					$("#one_label0").css("background-color","#ffffff");
					$("#one_label0").css("border-right-color","#ffffff");
					$("#one_label0 i").css("color","#417fd6");
				}
			});
		}
		
		//
		function gettext(id,label_id){
			//触发时改变样式
			$(".list-group-item").css("background-color","#f2f2f2");
			$(".list-group-item").css("border-right-color","#ddd");
			$(".xe623").css("color","#616161");
			
			$("#"+id).css("background-color","#ffffff");
			$("#"+id).css("border-right-color","#ffffff");
			$("#"+id+" i").css("color","#417fd6");
			
			secondLevel(label_id);
			
		}
		
		//接收选中的标签
        var testMap={};
		
		//查询二三级标签
		function secondLevel(id){
			var json={
				lable_id:id
			};
			$.ajax({
				url: "com.recruit.talent.label.secondLevelLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					$("#content-Two_label").empty();//清空数据
					 for(var i=0;i<data.result.length;i++){
						var content_Two_label ="<div class='panel-heading'>"
							+"<h6 class='panel-title'>"
							+"<a data-toggle='collapse' data-parent='#accordion' href='#collapseOne"+i+"'>"+data.result[i].lable+" >>"
							+"</a></h6></div>"
							+"<div id='collapseOne"+i+"' class='panel-collapse collapse in'>"
							+"<div class='panel-body' id='panel_label'><ul>";
							var recruitSublable = data.result[i].recruitSublable;//将二级标签中的子标签赋值到recruitSublable
							//判断  recruitSublable 是否空
							if(typeof(recruitSublable) != "undefined"){
								//如果  recruitSublable 长度大于0则循环加载子标签
								if(recruitSublable.length>0){
									for(var j=0;j<recruitSublable.length;j++){
										content_Two_label +="<li><a data='"+recruitSublable[j].id+"'><span>"+recruitSublable[j].lable+"</span></a></li>";
									}
								}
							}
							+"</ul></div></div></div>";
							
						$("#content-Two_label").append(content_Two_label);
						
					}
					 	var value="";
						var text="";
					  $("#panel_label ul li a").each(function(index){
						  $(this).click(function(){
							 /*value=$(this).children('input').val();*/
							 value =$(this).attr("data");
						     text =$(this).children('span').html();
						     var num=0;
						     for(var key in testMap){
						    	 if(key == value){
						    		 num++;
						    	 }
						    	 
						     }
						    
						     if(num == 0){
					    		 testMap[value] = text;
						    	 $("#content_label2").append("<span class='label label-info' id='"+value+"'>"+text+"<i class='layui-icon xe617'  onclick='delete_label(this)'  data='"+value+"'>&#x1006;</i></span>");
						     }
						  });
					  });
				}
			});
		}	
		
		//删除已选好的标签
		function delete_label(numId){
			var id =$(numId).attr("data");
			/*$(numId).parent('span').empty();*/
			$(numId).parent().empty();
			delete testMap[id];
			var obj="#content_label2 #"+id;
			$(obj).remove();//清除
		}
		
	 	