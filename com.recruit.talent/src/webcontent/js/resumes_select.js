		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
	
		$(function(){
			select_talent();//查询简历信息
			select_position();//查询职位
		});
    		
			var talentId="";
		
    		//查询简历信息
	    	function select_talent(){
	    		 var id=ID;
	    		var json={
	    			talent_id:id
	    		};
	    		$.ajax({
	    			url:"com.recruit.talent.talent.selectTalent.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				talentId= data.talent[0].talent_id;
		  				
		  				var status=talent(data.talent[0].talent_status);//把人才状态的值传入talent方法进行解析
		  				var sex=sexstatus(data.talent[0].gender);//把性别的值传入sexstatus方法进行解析
		  				//添加 隐式标签 分别是：人才库id，简历id，招聘负责人id，渠道类型，供应商类型
		  				$("#content-top-right").append("<input type='hidden' id='talent_id' value='"+data.talent[0].id+"'>");
		  				$("#content-top-right").append("<input type='hidden' id='resume_id' value='"+data.talent[0].resume_id+"'>");
		  				$("#content-top-right").append("<input type='hidden' id='leader_id' value='"+data.talent[0].recruiter_id+"'>"); 
		  				$("#content-top-right").append("<input type='hidden' id='channel' value='"+data.talent[0].recruit_channels+"'>");
		  				$("#content-top-right").append("<input type='hidden' id='supplier' value='"+data.talent[0].recruit_suppliers+"'>"); 
		  				
		  				 
		  				$("#leaderName").text(data.talent[0].recruiter_name);//招聘负责人姓名赋值
		  				$("#name").text(data.talent[0].candidate_name);//人才姓名赋值
		  				
		  				$("#state").text(status);//人才状态赋值
		  				$("#intentionPosition").text(data.talent[0].expected_position);//意向职位
		  				$("#phone").text(data.talent[0].candidate_phone);//人才电话
		  				
		  				$("#intentionPositionInterior").text(data.talent[0].position_catagory);//意向职位类别
		  				$("#email").text(data.talent[0].candidate_mail);//人才邮箱
		  				$("#expectationPay").text(data.talent[0].expected_salary);//期望薪资
		  				
		  				$("#intention_site").text(data.talent[0].excepted_work_area_a);//工作意向地
		  				
						$("#intention_site_v2").text(data.talent[0].excepted_work_area_b);//工作意向地2
						
						$("#now_site").text(data.talent[0].address);//人才现居地
		  				
		  				var study=educationstatus(data.talent[0].degree);
		  				$("#study").text(study);//学历
		  				
		  				var birthTime=data.talent[0].birth_date;
		  				if(birthTime!=null && birthTime!="" && typeof(birthTime) != "undefined"){
		  					$("#birthTime").text(birthTime.slice(0,7)); //出生日期
		  				}else{
		  					$("#birthTime").text(birthTime);
		  				}
		  				
		  				
		  				$("#talentpoolSex").text(sex);//性别
		  				
		  				$("#orgBranch").text(data.talent[0].orgcentre);//面试部门
		  				$("#interviewPositon").text(data.talent[0].interview_positon);//面试职位
		  				
		  				var abandonLink=Abandonmentstatus(data.talent[0].abandon_process_id);
		  				$("#abandonLink").text(abandonLink);//放弃环节
		  				$("#abandonCondition").text(data.talent[0].abandon_reason);//放弃原因
		  				$("#abandonCause").text(data.talent[0].abandon_cause);//具体放弃情况描述
		  				
		  				
		  				//把  简历id 传入resume_url方法
		  				resume_url(data.talent[0].resume_id);
		  				
		  				select_candidate(data.talent[0].talent_id);//查询已在工单列表
		  				
		  				label_select(data.talent[0].id);//查询标签
		  			}
	    		});
	    	}
	    	
	    	//查询已在工单列表
	    	function select_candidate(id){
	    		
	    		var json={
	    			talents_id:id,
	    		};
	    		$.ajax({
	    			url:"com.recruit.talent.talent.selectCandidate.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				//循环拼接工单号、招聘负责人、目前所在环节
		  				for(var i=0;i<data.result.length;i++){
			  				var candidateStatus= candidatestatus(data.result[i].candidateStatus);
			  				//工单号
			  				var recuitOrderId = data.result[i].recuitOrderId;
		  					var candidate=("<tr><td><a href='javascript:void(0)' class='cand' onclick='openModal(\""+recuitOrderId+"\")'>"+recuitOrderId+"</a></td>" 
		  								+"<td>"+data.result[i].recruiterName+"</td>"
		  								+"<td>"+candidateStatus+"</td</tr>"); 
		  					$("#tt").append(candidate);
		  				}
		  				
		  			}
		  		});
	    	}
	    	
	    	var candidateId ="";//候选人ID
	    	//查看工单信息
	    	function openModal(order){
	    		var name =$("#name").text();
	    		$("#candidateName").text(name);
	    		$(".talent-detail-modal").show();
	    		//ID 简历id  order工单号
	    		var json = {"resumeId":ID,"recuitOrderId":order};
	    		$.ajax({
	    			url:'com.recruit.talent.selecEmptlinkInfo.selectCandidateLinkInfo.biz.ext',
	    			type:"POST",
		  			data:json,
		  			async:false,
		  			success:function(data){
		  				//返回结果的长度要大于0
		  				if(data.data.length>0){
		  					var num =0;
		  					for(var i = 0; i<data.data.length;i++){
		  						var html="";
		  							candidateId =data.data[0].id;//候选人ID
		  						if(data.data[i].link=="1"){//初筛环节
		  							$(".first_data").show();//显示
		  							$("#interview1_right").text(isNullBlank(data.data[i].interview_evaluation));
		  						}else if(data.data[i].link=="2"){//电话面试环节
		  							$(".phone_data").show();//显示
		  							var html ="<p>电话沟通时间："+getDateTime(data.data[i].end_time)+"</p>";
		  								html +="<p>计划人资面试时间："+getDateTime(data.data[i].interview_time)+"</p>";
		  								html +="<p>计划部门面试时间："+getDateTime(data.data[i].section_time)+"</p>";
		  							$("#interview_phone_right").append(html);
		  							$("#interview_phone_right_bottom").text(data.data[i].interview_evaluation);
		  							
		  						}else if(data.data[i].link=="3"){//人资面试环节
		  							$(".human_data").show();//显示
		  							var html ="<p>实际面试时间："+getDateTime(data.data[i].actual_interview_time)+"</p>";
			  							html +="<p>面试官："+isNullBlank(data.data[i].interviewer_name)+"</p>";
			  							html +="<p>部门面试时间："+getDateTime(data.data[i].section_time)+"</p>";
		  							$("#interview_human_right").append(html);
		  							$("#interview_human_right_bottom").text(isNullBlank(data.data[i].interview_evaluation));
		  							
		  						}else if(data.data[i].link=="4"){//部门面试环节
		  							$(".org_data").show();//显示
			  							num++;
			  							html +="<p><span class='layui-badge layui-bg-gray'>第"+num+"轮</span></p>";
			  							html +="<p>实际面试时间："+getDateTime(data.data[i].actual_interview_time)+"</p>";
			  							html +="<p>面试官："+isNullBlank(data.data[i].interviewer_name)+"</p>";
			  							html +="<p class='bott' id='tp"+num+"'>评价："+isNullBlank(data.data[i].interview_evaluation)+"</p>";
		  							$("#interview_org_right").append(html);
		  							
		  						}else if(data.data[i].link=="5"){//待入职环节
		  							$(".waiting_data").show();//显示
		  							$("#tp"+num).css("border-bottom","0px solid #ccc");
		  							var html ="<p>计划入职时间："+data.data[i].planned_hire_date+"</p>";
		  								html +="<p>延期时间："+data.data[i].delayed_date+"</p>";
		  							$.ajax({
		  				    			url:'com.recruit.talent.selecEmptlinkInfo.selectHealthInfo.biz.ext',
		  				    			type:"POST",
		  					  			data:{"candidateId":candidateId},
		  					  			async:false,
		  					  			success:function(dataHealth){
		  					  				if(dataHealth.data[0]!=null){
			  					  				html +="<p class='healthcl'>"+dataHealth.data[0].health_report_name+"</p>";
			  					  				html +="<p>体检反馈："+isNullBlank(dataHealth.data[0].health_feedback_reason)+"</p>";
			  					  				$("#interview_waiting_right").append(html);
		  					  				}
		  					  			}
		  					  		});
		  							
		  						}else if(data.data[i].link=="6"){//放弃环节
		  							$(".fangqi_data").show();//显示
		  							var html ="<p>放弃原因："+isNullBlank(data.data[i].abandon_reason)+"</p>";
		  								html +="<p>具体描述："+isNullBlank(data.data[i].abandon_details)+"</p>";
		  							$("#interview_fangqi_right").append(html);
		  						}
		  					}
		  					selectTemplate();//加载评估表
		  				}
		  			}
	    		});
	    	}
	    	
	    	/* 关闭模态层 */
			function closeMode(){
				$("#interview_phone_right").html("");//电话面试
				$("#interview_human_right").html("");//人资环节
				$("#interview_org_right").html("");//部门环节
				$("#interview_waiting_right").html("");//待入职环节
				$("#interview_fangqi_right").html("");//放弃环节
				$(".interview").hide();
				$(".talent-detail-modal").hide();
			}
	    	
	    	//查询职位
	    	function select_position(){
	    		var json={
	    				empid:empid
	    		};
	    		$.ajax({
	    			url:"com.recruit.talent.talent.selectAllPosition.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				//循环输出值并拼接到对应的位置
		  				for(var i=0;i<data.result.length;i++){
		  					$("#positionName").append("<option>"+data.result[i].position+"</option>"); 
		  				}
		  				officeId($("#positionName").val());
		  			}
		  		});
	    	}
	    	
	    	//根据职位查询工单
	    	function officeId(id){
	    		var json={
	    			position:id,
	    		};
    			$.ajax({
	    			url:"com.recruit.talent.talent.selectOfficeId.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				var result =data['result'];
		  				$("#odd_numbers")[0].length=1;
		  			//循环输出值并拼接到对应的位置
		  				for(var i=0;i<result.length;i++){
		  					$("#odd_numbers").append("<option>"+result[i].recuitOrderId+"</option>"); 
		  				}
		  			}
		  		});
	    	}
	    	
	    	
	    	var isPass=true;//标识是否通过验证
			var isPassMsg="";//非空验证提示输出
			var arr=[];
		   	//失去焦点验证
		   	function validator(){
		   		isPassMsg="";
		   		isPass=true;
		   		arr=[];
		   		//职位
		  		 	if($('#positionName').val()==null || $('#positionName').val()=="" || $('#positionName').val()==0){
		           	$("#positionName").css("border","1px solid red");
		           	isPassMsg+="请选择职位,";
		           	isPass=false;
		           }else{
		           	$("#positionName").css("border","1px solid #ccc");
		           }
		   		
		   		//工单号
		  		 	if($('#odd_numbers').val()==null || $('#odd_numbers').val()=="" ){
		           	$("#odd_numbers").css("border","1px solid red");
		           	isPassMsg+="请选择工单";
		           	isPass=false;
		           }else{
		           	$("#odd_numbers").css("border","1px solid #ccc");
		           }
		  		 	arr=isPassMsg.split(",");
		   	} 
	    	
	    	
	    	//把人才信息添加到候选人表
	    	 function add(){
	    		 validator();
	    		//非空判断
	     		if(isPass==false){
	     			layer.msg(arr[0]);
	     			return;
	     		}
	    		var odd_numbers=$("#odd_numbers").val();//工单号
	    		var json1 = {
						id:talentId
					};
					$.ajax({
						url:"com.recruit.talent.talentpoolselect.selectOrderByid.biz.ext",
						type:'POST',
						data:json1,
						success:function(data){
							//标识是否已经添加到工单，1已添加，其他未添加
							var recommendord ="";
							for(var i =0;i<data.recommend.length;i++){
								if(data.recommend[i].recuitOrderId == odd_numbers){
									recommendord = 1;
									}
								}
								if(recommendord == 1){
									layer.msg('已加入工单，请勿重复操作');
									return;
								}else{
									
									var candidate_status=0;//候选人状态
						    		var talent_id =$("#talent_id").val();//人才库id
						    		var resume_id=$("#resume_id").val();//简历id
						    		var supplier=$("#supplier").val();//供应商
						    		var channel=$("#channel").val();//渠道
						    		var leaderName=empname;//招聘负责人姓名
						    		var leader_id =empid;//招聘人id
						    		var recruit_process_id=1;//所在环节
						    		
									var json2={
										talent_id:talent_id,
						    			resume_id:resume_id,
						    			supplier:supplier,
						    			channel:channel,
						    			leaderName:leaderName,
						    			leader_id:leader_id,
						    			odd_numbers:odd_numbers,
						    			candidate_status:candidate_status,
						    			recruit_process_id:recruit_process_id	
						    		};
									$.ajax({
						    			url:"com.recruit.talent.talent.insertCandidate.biz.ext",
							  			type:"POST",
							  			data:json2,
							  			success:function(data){
							  				//拼接查询出来的工单号，招聘负责人和招聘状态
							  				if(data.success=="ok"){
							  					var candidateStatus= candidatestatus(data.cand.candidateStatus);
							  					var candidate=("<tr><td>"+data.cand.recuitOrderId+"</td>" 
						  								+"<td>"+data.cand.recruiterName+"</td>"
						  								+"<td>"+candidateStatus+"</td</tr>"); 
							  					$("#tt").append(candidate);
							  					layer.msg("添加成功！");
							  					
							  				}else{
							  					layer.msg("添加失败！");
							  				}
							  			}
									});
								}
							}
						});
					}
	    		
	    		
	    	 //根据人才id查询标签
	    	 function label_select(id){
	    		 var json={
	    			id:id	 
	    		 };
	    		 $.ajax({
		    			url:"com.recruit.talent.resumesSelect.selectLabelId.biz.ext",
			  			type:"POST",
			  			data:json,
			  			success:function(data){
			  			//判断如果sublable对象长度小于等于0时不做操作
			  				if(data.sublable.length<=0){
			  					
			  				}else{
			  					for(var i=0;i<data.sublable.length;i++){
			  						//拼接标签
				  					$("#content_label2").append("<span class='label label-info' id='"+data.sublable[i].id+"'>"
					  						+"<i class='layui-icon xe617'>&#xe617;</i>"+data.sublable[i].lable+"</span>");
				  				}
			  				}
			  				
			  			}
				  	});
	    	 }
	    	 
	    	 	//查询简历地址
		    	function resume_url(id){
		    		var json={
		    			resumeid:id
		    		};
		    		$.ajax({
		    			url:"com.recruit.interpolation.interpolationindex.selectResume.biz.ext",
			  			type:"POST",
			  			data:json,
			  			success:function(data){
			  				//判断如果returnAcc对象长度小于等于0时不做操作
		  				 if(data.returnAcc.length <= 0){
								
							}else{
								for(var i=0;i<data.returnAcc.length;i++){
									var accessoryUrl1=data.returnAcc[i].accessoryUrl;//简历地址
									var accessoryUrl=basePath+data.returnAcc[i].accessoryUrl;//拼接附件路径
									var accessoryName=data.returnAcc[i].accessoryName;//简历名称
									//把简历放在所对应的DIV里面
									$("#file_upload").append("<p id='recruitAcc"+data.returnAcc[i].id+"' class='recruitAcc'>"
											+"<a href="+accessoryUrl+"  target='_blank'  title='"+accessoryName+"'>"+accessoryName+"</a></p>");
									resumeUrl+=accessoryUrl1+",";
									resumeName+=accessoryName+",";
									
								}
								
							}
			  			}
				  	});
		    	}
		    	
		    	//点击下载附件需要进行的方法
		    	function preview2(){
		    		if(resumeUrl=="" || resumeUrl==null){
		    			
		    		}else{
		    			resumeUrl=resumeUrl.substring(0,resumeUrl.length-1);//去掉最后一个字符
						resumeName=resumeName.substring(0,resumeName.length-1);//去掉最后一个字符
		    			resumeUrl2=resumeUrl.split(",");//转换成数组
		    			resumeName2=resumeName.split(",");//转换成数组
		    			//循环url
		    			for(var i=0;i<resumeUrl2.length;i++){
		    				//循环文件名
		    				for(var j=i;j<resumeName2.length;j++){
		    					//简历下载
		    					download(resumeUrl2[i],resumeName2[j]);
		    					break;
		    				}
		    			}
		    			
		    		}
		    	}
		    	
		    	//简历下载
		    	function download(href, title) {
				    const a = document.createElement('a');
				    href=basePath+href;
				    a.setAttribute('href', href);
				    a.setAttribute('download', title);
				    a.click();
				};
				
				//查询评估表
				function selectTemplate(){
					$.ajax({
			  			url:"com.recruit.process.addRemplateRecord.queryTemplateRecord.biz.ext",
			  			type:"POST",
			  			datatype: "json",
			  			data:{
			  				"candidateId":candidateId
			  			},
			  			success:function(data){
			  				$(".assess").text("");
			  				$(data.RecruitTemplateRecord).each(function(i,obj){
			  					var url="";
			  					var html='<div class="panel panel-info" style="margin-left: 5%;width: 360px;margin-bottom: 10px;">';
			  					html+='<div class="panel-heading">';
			  					html+='<h3 class="panel-title">面试评估表</h3>';
			  					html+='</div>';
			  					html+='<div class="panel-body">';
			  						url=server_context+"/process/recruit_template/template_update.jsp?id="+obj.id+"&entry=1&sign=g"; 
			  						html+='<span>人资总分：<span>'+obj.hrScore+'</span></span><span style="margin-left: 30px;">部门总分：<span>'+obj.orgScore+'</span></span><a style="float: right;color:#337dbc;margin-right: 10px;"href="'+url+'"  target="_Blank">查看</a></div>';
			  					html+='</div>';
			  					$(".assess").append(html);	
			  				})
			  				
			  				
			  			}
			  		});
				}
				
				//排除空值
				function isNullBlank(str){
					if(str==null||str==''){
						return '';
					}else{
						return str;
					}
				}
	    	
