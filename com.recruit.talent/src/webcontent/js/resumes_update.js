//时间控件
    	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#birthTime' //指定元素
		    	,value: '1990-01-01' //必须遵循format参数设定的格式
			    	,isInitValue: false //是否允许填充初始值，默认为 true
		  });
		});
		
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
		
		
		$(function(){
			queryDisease();//下拉框赋值
				sublable();//默认加载一级标签
    		secondLevel(1);//默认加载二三级标签
		});
		
    		//下拉框赋值
    		function queryDisease(){
    			$.ajax({
    				url: "com.recruit.talent.talent.queryDictionary.biz.ext",
					type:'POST',
					success:function(data){
						//意向职位类别循环拼接赋值
						for(var i=0;i<data.category.length;i++){
		  					$("#intentionPositionInterior").append("<option value='"+data.category[i].dictID+"'>"+data.category[i].dictName+"</option>"); 
		  				}
		  				//学历循环拼接赋值
		  				for(var i=0;i<data.education.length;i++){
		  					$("#study").append("<option value='"+data.education[i].dictID+"'>"+data.education[i].dictName+"</option>"); 
		  				}
		  				//期望薪资循环拼接赋值
		  				for(var i=0;i<data.salary.length;i++){
		  					$("#expectationPay").append("<option value='"+data.salary[i].dictID+"'>"+data.salary[i].dictName+"</option>"); 
		  				}
		  				//放弃原因循环拼接赋值
		  				for(var i=0;i<data.reason.length;i++){
		  					$("#abandonCondition").append("<option value='"+data.reason[i].dictID+"'>"+data.reason[i].dictName+"</option>"); 
		  				}
		  				select_talent();//查询简历信息
					}
    			});
    		}
    		
    		var resume_id;
    		var candidate_name;
    		var candidate_phone;
    		//查询简历信息
	    	function select_talent(){
	    		
	    		var json={
	    			id:id
	    		};
	    		$.ajax({
	    			url:"com.recruit.talent.talent.updateTalent.biz.ext",
		  			type:"POST",
		  			data:json,
		  			success:function(data){
		  				resume_id=data.talent[0].resume_id;
		  				$("#leaderName").val(data.talent[0].recruiter_name);//招聘负责人
		  				$("#name").val(data.talent[0].candidate_name);//人才名
		  				candidate_name=data.talent[0].candidate_name;//赋值人才姓名
		  				$("#intentionPosition").val(data.talent[0].expected_position);//意向职位
		  				$("#phone").val(data.talent[0].candidate_phone);//电话
		  				candidate_phone=data.talent[0].candidate_phone;//赋值人才电话
		  				$("#intentionPositionInterior").val(data.talent[0].position_catagory);//意向职位类别
		  				$("#email").val(data.talent[0].candidate_mail);//邮箱
		  				$("#expectationPay").val(data.talent[0].expected_salary);//期望薪资
		  				$("#study").val(data.talent[0].degree);//学历
		  				$("#birthTime").val(data.talent[0].birth_date);//出生日期
		  				$("#talentpoolSex").val(data.talent[0].gender);//性别
		  				
		  				$("#orgBranch").val(data.talent[0].orgcentre);//面试部门
		  				$("#interviewPositon").val(data.talent[0].interview_positon);//面试职位
		  				$("#abandonLink").val(data.talent[0].abandon_process_id);//放弃环节
		  				$("#abandonCondition").val(data.talent[0].abandon_reason);//放弃原因
		  				$("#abandonCause").val(data.talent[0].abandon_cause);//具体放弃情况描述
		  				
		  				//地址拆分
		  				var excepted_work_area_a=data.talent[0].excepted_work_area_a;
		  				if(excepted_work_area_a==null || excepted_work_area_a==""){
		  					
		  				}else{
			  				var excepted_a=excepted_work_area_a.split("-");
			  				
			  				$('#distpicker5').distpicker('destroy');
			  				$('#distpicker5').distpicker({
							    province: excepted_a[0],
							    city: excepted_a[1],
							    district: excepted_a[2],
							    autoSelect: false
							  });
		  				}
						  
						 //地址拆分
		  				var excepted_work_area_b=data.talent[0].excepted_work_area_b;
		  				if(excepted_work_area_b==null || excepted_work_area_b==""){
		  					
		  				}else{
			  				var excepted_b=excepted_work_area_b.split("-");
			  				
			  				$('#distpicker1').distpicker('destroy');
			  				$('#distpicker1').distpicker({
							    province: excepted_b[0],
							    city: excepted_b[1],
							    district: excepted_b[2],
							    autoSelect: false
							  });
						}  
						//地址拆分
		  				var content_nowSite=data.talent[0].address;
		  				if(content_nowSite==null || content_nowSite==""){
		  					
		  				}else{
			  				var address=content_nowSite.split("-");
			  				
			  				$('#distpicker2').distpicker('destroy');
			  				$('#distpicker2').distpicker({
							    province: address[0],
							    city: address[1],
							    district: address[2],
							    autoSelect: false
							  });
		  				}
						 
						 //添加 隐式标签 分别是：渠道类型，供应商类型，人才状态
						 $("#content_top").append("<input class='channel' type='hidden' id='channel' value='"+data.talent[0].recruit_channels+"'>");
						 $("#content_top").append("<input type='hidden' id='supplier' value='"+data.talent[0].recruit_suppliers+"'>");
						 $("#content_top").append("<input type='hidden' id='state' value='"+data.talent[0].talent_status+"'>");
						 
						 talent_id=data.talent[0].talent_id;
						 
						 //显示已存在的标签
						 for(var i=0;i<data.label.length;i++){
						 	var value =data.label[i].label_id;
						     var text =data.label[i].lable;
						     var num=0;
						      for(var key in testMap){
						    	 if(key == value){
						    	 	num++;
						    	 }
						     }
						      if(num == 0){
					    		 testMap[value] = text;
					    		 }
						 	 $("#content_label2").append("<span class='label label-info' id='"+data.label[i].label_id+"'>"+data.label[i].lable+""
						 			 +"<i class='layui-icon xe617'  onclick='delete_label2(this)'  data='"+data.label[i].lab_id+"' opp='"+data.label[i].label_id+"'>&#x1006;</i></span>");
						 }
						 
						 //判断是否存在附件
						 if(data.recruitAcc.length <= 0){
							 
							}else{
								$("#resumes").show();
								for(var i=0;i<data.recruitAcc.length;i++){
									var accessoryUrl=basePath+data.recruitAcc[i].accessoryUrl;//拼接附件路径
									$("#file_upload").append("<p id='recruitAcc"+data.recruitAcc[i].id+"' class='recruitAcc'><a href="+accessoryUrl+"  target='_blank'>"+data.recruitAcc[i].accessoryName+"</a>"
												+"&nbsp;&nbsp;&nbsp;&nbsp;<a class='delete'"
												+"onclick='resumnedelete("+data.recruitAcc[i].id+",\""+data.recruitAcc[i].accessoryPath+"\",\""+data.recruitAcc[i].accessoryName+"\")'> X </a></p>");
								}
								
							}
						 
						}
					});
	    		
	    		}
    		
    	var talent_id;//接收当前人才库id
		//修改
		function update() {
			var talentid = talent_id;
			var leaderName = $("#leaderName").val();//招聘负责人
			var name = $("#name").val();//人才名
			var intentionPosition = $("#intentionPosition").val();//意向职位
			var phone = $("#phone").val();//电话
			var intentionPositionInterior = $("#intentionPositionInterior").val();//意向职位类别
			var email = $("#email").val();//邮箱
			var expectationPay = $("#expectationPay").val();//期望薪资
			var study = $("#study").val();//学历
			var birthTime = $("#birthTime").val();//出生日期
			var talentpoolSex = $("#talentpoolSex").val();//性别
			
			var orgcentre =$("#orgBranch").val();//面试部门
    		var orgcentreId=$("#orgBranch").attr("alias");
    		var L1 = $("#orgBranch").attr("l1");
    		var L2 = $("#orgBranch").attr("l2");
    		var L3 = $("#orgBranch").attr("l3");
    		var L4 = $("#orgBranch").attr("l4");
    		var L1name = $("#orgBranch").attr("l1name");
    		var L2name = $("#orgBranch").attr("l2name");
    		var L3name = $("#orgBranch").attr("l3name");
    		var L4name = $("#orgBranch").attr("l4name");
    		
			var interviewPositon = $("#interviewPositon").val();//面试职位
			var abandonLink = $("#abandonLink").val();//放弃环节
			var abandonCondition = $("#abandonCondition").val();//放弃原因
			var abandonCause = $("#abandonCause").val();//具体放弃情况描述
			var channel = $("#channel").val();//渠道
			var supplier = $("#supplier").val();//供应商
			var state = $("#state").val();//状态

			//地址拼接
			var address = "";
			if ($('#province').val() != "") {
				address = $('#province').val();
			}
			if ($("#city").val() != "") {
				address = $('#province').val() + "-" + $("#city").val();
			}
			if ($("#district").val() != "") {
				address = $('#province').val() + "-" + $("#city").val() + "-"
						+ $("#district").val();
			}

			var address_2 = "";
			if ($('#province_2').val() != "") {
				address_2 = $('#province_2').val();
			}
			if ($("#city_2").val() != "") {
				address_2 = $('#province_2').val() + "-" + $("#city_2").val();
			}
			if ($("#district_2").val() != "") {
				address_2 = $('#province_2').val() + "-" + $("#city_2").val()
						+ "-" + $("#district_2").val();
			}

			var nowSite = "";
			if ($('#province_nowSite').val() != "") {
				nowSite = $('#province_nowSite').val();
			}
			if ($("#city_nowSite").val() != "") {
				nowSite = $('#province_nowSite').val() + "-"
						+ $("#city_nowSite").val();
			}
			if ($("#district_nowSite").val() != "") {
				nowSite = $('#province_nowSite').val() + "-"
						+ $("#city_nowSite").val() + "-"
						+ $("#district_nowSite").val();
			}

			//获取标签id
			var obj = "";
			for ( var key1 in testMap) {
				obj += key1 + ",";
			}

			//获取上传地址
    		var accessory_url="";
    		var accessory_name="";
    		var accessory_path="";
    		 $(".afiles").each(function(i,obj){
    			 accessory_url+= getCaption($(obj).attr("href"))+"^";
    			 accessory_name+=$(obj).text()+"^";
    			 accessory_path+=$(obj).attr("data")+"^";
    		 });
    		

			var json = {
				id : id,
				talentid : talentid,
				leaderName : leaderName,
				name : name,
				intentionPosition : intentionPosition,
				phone : phone,
				intentionPositionInterior : intentionPositionInterior,
				email : email,
				expectationPay : expectationPay,
				study : study,
				birthTime : birthTime,
				sax : talentpoolSex,
				orgcentre:orgcentre,
				orgcentreId:orgcentreId,
				l1:L1,
				l1name:L1name,
				l2:L2,
				l2name:L2name,
				l3:L3,
				l3name:L3name,
				l4:L4,
				l4name:L4name,
				interviewPositon : interviewPositon,
				abandonLink : abandonLink,
				abandonCondition : abandonCondition,
				abandonCause : abandonCause,
				content_address : address,
				content_address_2 : address_2,
				content_nowSite : nowSite,
				channel : channel,
				supplier : supplier,
				state : state,
				obj : obj,
				accessory_url:accessory_url,
				accessory_name:accessory_name,
				accessory_path:accessory_path
			};
			$.ajax({
				url : "com.recruit.talent.talent.updateResume.biz.ext",
				type : "POST",
				data : json,
				success : function(data) {
					if (data.success == "ok") {
						layer.msg("修改成功！");
						$("#file_upload").html("");
						//查询当前简历的附件
						selectFile_upload();
						window.opener.isok();
					} else {
						layer.msg("修改失败！");
					}
				}
			});
		}
		
		function selectResume(){
			validator();
    		//非空判断
    		if(isPass==false){
    			layer.msg(arr[0]);
    			return;
    		}
    		var name =$("#name").val();//获取人才姓名
    		var phone =$("#phone").val();//获取人才电话
    		
    		var json={
    			name:name,
    			phone:phone
    		};
    		$.ajax({
				url: "com.recruit.talent.talent.selectResume.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					if(data.success=='ok'){
						isEnclosureExist();//判断是否还有附件未上传
					}else if(candidate_name==name && candidate_phone==phone){
						isEnclosureExist();//判断是否还有附件未上传
					}else{
						layer.msg('存在重名重电话号码的人才');
					}
				}
			});
			
		}
		
		//查询当前简历的附件
		function selectFile_upload(){
			var resumeid=resume_id;
			var json={
				flag:1,
				resumeid:resumeid
			}
			$.ajax({
				url:"com.recruit.interpolation.interpolationindex.selectResume.biz.ext",
				type:"post",
				data : json,
				success : function(data) {
					//判断是否存在附件
					 if(data.returnAcc.length <= 0){
						 
						}else{
							$("#resumes").show();
							for(var i=0;i<data.returnAcc.length;i++){
								var accessoryUrl=basePath+data.returnAcc[i].accessoryUrl;//拼接附件路径
								$("#file_upload").append("<p id='recruitAcc"+data.returnAcc[i].id+"' class='recruitAcc'><a href="+accessoryUrl+"  target='_blank'>"+data.returnAcc[i].accessoryName+"</a>"
											+"&nbsp;&nbsp;&nbsp;&nbsp;<a class='delete'"
											+"onclick='resumnedelete("+data.returnAcc[i].id+",\""+data.returnAcc[i].accessoryPath+"\",\""+data.returnAcc[i].accessoryName+"\")'> X </a></p>");
							}
							
						}
				}
			});
		}
		
		//返回方法
		function historyGo(){
			//遍历触发点击方法
			$(".deleteUrl").each(function(i,obj){
				$(obj).find(".delete").click();
				
			});
			history.back();
		}
		
		//附件是否还有未上传的
		function isEnclosureExist(){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				update();
				
			}
		}
