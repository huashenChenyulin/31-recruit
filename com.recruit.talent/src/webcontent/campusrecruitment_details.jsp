<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.UserObject"%>
<%@page import="com.eos.foundation.eoscommon.ResourcesMessageUtil"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="commonj.sdo.*,java.util.*,java.text.SimpleDateFormat"%>
<%@page import="com.eos.web.taglib.util.XpathUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wenjin
  - Date: 2018-03-12 10:20:37
  - Description:
-->
<head>
<title>校招人才录入页面</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script
	src="<%=request.getContextPath()%>/css/bootstrap-3.3.7-dist/js/bootstrapValidator.min.js"></script>
<link
	href="<%=request.getContextPath()%>/css/bootstrap-3.3.7-dist/css/bootstrapValidator.min.css"
	rel="stylesheet" />
<link
	href="<%=request.getContextPath()%>/Talent/css/campusrecruitment_details.css?v=1.01"
	rel="stylesheet" />
	
	<link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
   	
   	
   	<!-- 工作意向地的js -->
   	<script src="<%= request.getContextPath() %>/Talent/js/distpicker.data.js"
		type="text/javascript"></script>
	<script src="<%= request.getContextPath() %>/Talent/js/distpicker.js"
		type="text/javascript"></script>
	<script src="<%= request.getContextPath() %>/Talent/js/main.js"
		type="text/javascript"></script>
   	
<style type="text/css">
	#clean{
		width: 64px;
	}
	#batch-template,#test4{
		color: #e28603;
	   	margin-left: 22px;
	    font-size: 12px;
	} 
	#batch-template,#test4:HOVER {
		cursor: pointer;
	} 
</style>
</head>
<%
		//session获取校招ID
		String id = request.getParameter("id");
		//获取当前招聘负责人姓名和ID
		String empname = DataContextManager.current().getMUODataContext()
				.getUserObject().getUserRealName();
		String empid = DataContextManager.current().getMUODataContext()
				.getUserObject().getUserId();
	%>
<body>
	<div id="main">
		<div id="main-left">
			<fieldset class="layui-elem-field layui-field-title">
			  <% if(id == "" || id == null ) { %>
				<legend>新增校招简历</legend>
				<% } else { %>
				<legend>编辑校招简历</legend>
				<% } %>
			  
			  
			</fieldset>
			<form id="form">
				<div class="form-group">
					<div id="content">
						<h4>基本信息</h4>
						<div class="col-lg-6">
							<label>姓名<samp>*</samp></label> <input type="text"
								class="form-control" id="name" name="name"  onblur="validator()">
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="id"
								name="id" style="display:none;">
						</div>
						<div class="col-lg-6">
							<label>性别<samp>*</samp></label>
								<select class="form-control" id="gender" name="gender" onblur="validator()">
									<option value=""></option>
									<option value="1">男</option>
									<option value="2">女</option>
								</select>
						</div>
						<div class="col-lg-6">
								<label>毕业时间<samp>*</samp></label>
						        <input type="text" class="form-control" id="graduationdate" name="graduationdate" 
						         placeholder="yyyy-MM-dd" onblur="validator()">
						</div>
						<input type="hidden" class="form-control" id="resumeid"
							name="resumeid">
						<div class="col-lg-6">
							<label>宣讲学校</label> <input type="text"
								class="form-control" id="recruitmentcampus"
								name="recruitmentcampus">
						</div>
						<div class="col-lg-6">
							<label>电话<samp>*</samp></label> <input type="text"
								class="form-control phone" id="phone" name="phone" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>邮箱<samp>*</samp></label> <input type="text"
								class="form-control email" id="mail" name="mail" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>学校<samp>*</samp></label> <input type="text" class="form-control"
								id="college" name="college" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>学历<samp>*</samp></label>
							<select class="form-control study" id="degree" name="degree" onblur="validator()">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>专业<samp>*</samp></label> <input type="text" class="form-control"
								id="major" name="major" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>挂科情况<samp>*</samp></label>
							<select class="form-control" id="flunkeddetials" name="flunkeddetials" onblur="validator()">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>英语等级</label>
							<select class="form-control" id="englishlevel" name="englishlevel">
								<option value=""></option>
							</select>
						</div>
					</div>
					<div id="content_nowSite">
						<label class="currency">现居住地</label>
						<div id="distpicker2">
							<select class="form-control province" id="province_nowSite" ></select> 
								<select class="form-control city" id="city_nowSite"></select>
							<select class="form-control district" id="district_nowSite" ></select>
						</div>
					</div>
					<div id="content_nowSite2">
						<label class="currency">户口所在地</label>
						<div id="distpicker6">
							<select class="form-control province" id="province_nowSite_2"></select> 
								<select class="form-control city " id="city_nowSite_2"></select>
							<select class="form-control district" id="district_nowSite_2"></select>
						</div>
					</div>

					<!-- 被推荐人信息 -->
					<div id="content_top">
						<h4>面试环节</h4>
						<div id="content_address">
							<label class="currency">工作意向地(1)<samp>*</samp></label>
							<div id="distpicker5">
								<select class="form-control province province_1" id="province"
									name="province" onblur="validator()"></select> 
									<select class="form-control city city_1"
									id="city" name="city" onblur="validator()"></select> 
									<select class="form-control district" id="district" name="district"></select>
							</div>
						</div>
						<div id="content_address_2">
							<label class="currency">工作意向地(2)</label>
							<div id="distpicker1">
								<select class="form-control province" id="province_2"
									name="province_2"></select> <select class="form-control city"
									id="city_2" name="city_2"></select> <select
									class="form-control district" id="district_2" name="district_2"></select>
							</div>
						</div>
						<div class="col-lg-6">
							<label>面试职位类别<samp>*</samp></label>
							<select class="form-control" id="positioncategory" name="positioncategory" onblur="validator()">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>第一申请岗位</label> <input type="text" class="form-control"
								id="appliedposition1" name="appliedposition1">
						</div>
						<div class="col-lg-6">
							<label>第二申请岗位</label> <input type="text" class="form-control"
								id="appliedposition2" name="appliedposition2">
						</div>
						<div class="col-lg-6">
							<label>推荐岗位<samp>*</samp></label> <input type="text" class="form-control recommendedpost"
								id="recommendedpost" name="recommendedpost" onblur="validator()">
						</div>
					</div>
					<div id="content_middle">
						<div class="col-lg-6">
							<label>无领导小组讨论面试官</label> <input type="text" class="form-control"
								id="groupinterviewe" name="groupinterviewe">
						</div>
						<div class="col-lg-6">
							<label>无领导小组讨论评分</label> <input type="text" class="form-control"
								id="groupscore" name="groupscore">
						</div>
						<div id="content_textarea">
							<label>无领导小组讨论评价</label>
							<textarea class="form-control" rows="3" id="groupevaluation"
								name="groupevaluation"></textarea>
						</div>
						<div class="col-lg-6">
							<label>一对一面试官</label> <input type="text" class="form-control"
								id="facetofaceinterviewer" name="facetofaceinterviewer">
						</div>
						<div class="col-lg-6">
							<label>一对一面试评分</label> <input type="text" class="form-control"
								id="facetofaceinterview" name="facetofaceinterview">
						</div>
						<div id="content_textarea">
							<label>一对一面试评价</label>
							<textarea class="form-control" rows="3" id="facetofaceevaluation"
								name="facetofaceevaluation"></textarea>
						</div>
						
						<div class="col-lg-6">
							<label>复试跟进人</label> <input type="text" class="form-control"
								id="retrialerfollowup" name="retrialerfollowup">
						</div>
						<div class="col-lg-6">
							<label>复试面试官</label> <input type="text" class="form-control"
								id="retrialinterviewer" name="retrialinterviewer">
						</div>
						<div class="col-lg-6">
							<label>复试状态</label>
							<select class="form-control" id="retrialstatus" name="retrialstatus">
								<option value=""></option>
							</select>
						</div>
						<div id="content_textarea">
							<label>复试评价</label>
							<textarea class="form-control" rows="3" id="retrialevaluation"
								name="retrialevaluation"></textarea>
						</div>
					</div>

					<!-- 处理情况 -->
					<div id="content_bottom">
						<h4>面试其他信息</h4>
						<div class="col-lg-6">
							<label>不录用原因分类</label> 
							<select class="form-control" id="unacceptablereasonid" name="unacceptablereasonid">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>各选拔环节状态</label>
							<select class="form-control" id="processid" name="processid">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>Offer跟进人</label> <input type="text" class="form-control"
								id="offerfollowup" name="offerfollowup">
						</div>
						<div class="col-lg-6">
							<label>Offer状态</label>
							<select class="form-control" id="offerstatus" name="offerstatus">
								<option value=""></option>
							</select>
						</div>
						<div class="col-lg-6">
							<label>放弃原因分类</label>
							<select class="form-control" id="abandonreason" name="abandonreason">
								<option value=""></option>
							</select>
						</div>
						<div id="content_textarea">
							<label>放弃Offer原因</label>
							<textarea class="form-control" rows="3" id="abandondetails"
								name="abandondetails"></textarea>
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfo"
								name="upInfo" style="display:none;">
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfoname"
								name="upInfoname" style="display:none;">
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfopath"
								name="upInfopath" style="display:none;">
						</div>
					</div>
				</div>
			</form>
		</div>

		<div id="main-right">
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="success"
					name="success" onclick="isEnclosureExist()">保 存</button>
				<button type="button" class="btn btn-warning" id="updata"
					name="updata" onclick="updata()" style="display:none;width: 75px;">保 存</button>
				<button type="button" class="btn btn-default" id="clean"
					name="clean" onclick="clean()">重 置</button>
					
				<a class="glyphicon glyphicon-open" id="test4">信息批导</a>
				
				<a id="batch-template" class="glyphicon glyphicon-save" 
					href = "<%=request.getContextPath()%>/Talent/校招库批导模板.xls">模版下载</a>
					
					<%-- <button type="button" class="layui-btn layui-btn-primary file " id="test4">批导</button>
				<!--批导模板按钮  -->
				<a id="a-batch-template" href = "<%=request.getContextPath()%>/Talent/校招库批导模板.xls">
				<button type="button"  id="batch-template"  class="layui-btn btn-default" onclick="">模板
				</button>
				</a> --%>
			</div>
			 <div id="content_file" style="float: left;width: 99%;margin-left: 3%;display:none;">
					<label>附件:</label>
					<div id="file_upload"></div>
			</div>
			<div id="content_upload">
				<div id="uploader" class="wu-example">
			        <div class="queueList">
			            <div id="dndArea" class="placeholder">
			                <div id="filePicker"></div>
			                <p>点击文件上传或将文件拖到这里</p>
			                <p style="font-size: 14px;">支持：word、excel、html、ptf、jpg等格式</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
	<!--Excel导出  -->
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	
	<script type="text/javascript">
		//初始化业务字典
		dictionarycombox();
		//初始数据
		initlazation();
		
		var ID="<%=id %>";
		
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});
		
		layui.use([ 'layer' ], function() {
		});
		layui.use('laydate', function(){
  			var laydate = layui.laydate;
	  		laydate.render({
			    elem: '#graduationdate'
			});
 		});
 		
		
		function isaddorupdata(){
    		var id = <%=id %>;
    		if(id == "" || id == null){
    			 $("#success").css("display","");
    			 
    		}else{
    			 $("#success").css("display","");
    			 $("#updata").css("display","none");
    			 $("#clean").css("display","none");
    			 $("#test4").css("display","none");
    			 $("#batch-template").css("display","none");
    		}
    	}
    	 function updata(){
     	 	var id =$("#id").val();
     	 	submit(id);
     	 
     	 }

		//初始数据
		function initlazation() {
			var id = <%=id%>;
			if (id == null || id == "") {//非空判断
				return;
			} else {
				var json = {
					id : id
				};
				$.ajax({
					url : "com.recruit.talent.campusrecruitment.selectCampus.biz.ext",
					type : 'POST',
					data : json,
					success : function(data) {
						$("#name").val(data.campusobject[0].candidate_name);
						$("#resumeid").val(data.campusobject[0].resume_id);
						$("#gender").val(data.campusobject[0].gender);
						$("#graduationdate").val(data.campusobject[0].graduation_date);
						$("#recruitmentcampus").val(data.campusobject[0].recruitment_campus);
						$("#phone").val(data.campusobject[0].candidate_phone);
						$("#mail").val(data.campusobject[0].candidate_mail);
						$("#college").val(data.campusobject[0].college);
						$("#degree").val(data.campusobject[0].degree);
						$("#major").val(data.campusobject[0].major);
						$("#flunkeddetials").val(data.campusobject[0].flunked_detials);
						$("#englishlevel").val(data.campusobject[0].english_level);
					
						//现居地 地址拆分
		  				var address=data.campusobject[0].address;
		  				if(address!=null && address!=""){
		  				
			  				var nowSite_a=address.split("-");
			  				
			  				$('#distpicker2').distpicker('destroy');
			  				$('#distpicker2').distpicker({
							    province: nowSite_a[0],
							    city: nowSite_a[1],
							    district: nowSite_a[2],
							    autoSelect: false
							  });
		  				}
		  				//户口所在地 地址拆分
		  				var birthplace=data.campusobject[0].birthplace;
		  				if(birthplace!=null && birthplace!=""){
		  				
			  				var nowSite_b=birthplace.split("-");
			  				
			  				$('#distpicker6').distpicker('destroy');
			  				$('#distpicker6').distpicker({
							    province: nowSite_b[0],
							    city: nowSite_b[1],
							    district: nowSite_b[2],
							    autoSelect: false
							  });
		  				}
						
						
						//工作意向地 地址拆分
		  				var excepted_work_area_a=data.campusobject[0].excepted_work_area_a;
		  				if(excepted_work_area_a != null && excepted_work_area_a != ""){
		  				
			  				var excepted_a=excepted_work_area_a.split("-");
			  				
			  				$('#distpicker5').distpicker('destroy');
			  				$('#distpicker5').distpicker({
							    province: excepted_a[0],
							    city: excepted_a[1],
							    district: excepted_a[2],
							    autoSelect: false
							  });
		  				}
						  
						 //工作意向地 地址拆分
		  				var excepted_work_area_b=data.campusobject[0].excepted_work_area_b;
		  				if(excepted_work_area_b!=null && excepted_work_area_b!=""){
		  					
			  				var excepted_b=excepted_work_area_b.split("-");
			  				
			  				$('#distpicker1').distpicker('destroy');
			  				$('#distpicker1').distpicker({
							    province: excepted_b[0],
							    city: excepted_b[1],
							    district: excepted_b[2],
							    autoSelect: false
							  });
						}
						
						$("#positioncategory").val(data.campusobject[0].campus_position);
						$("#appliedposition1").val(data.campusobject[0].applied_position_1);
						$("#appliedposition2").val(data.campusobject[0].applied_position_2);
						$("#recommendedpost").val(data.campusobject[0].recommended_post);
						$("#groupinterviewe").val(data.campusobject[0].group_interviewe);
						$("#groupscore").val(data.campusobject[0].group_score);
						$("#groupevaluation").val(data.campusobject[0].group_evaluation);
						$("#facetofaceinterviewer").val(data.campusobject[0].face_to_face_interviewer);
						$("#facetofaceinterview").val(data.campusobject[0].face_to_face_interview);
						$("#facetofaceevaluation").val(data.campusobject[0].face_to_face_evaluation);
						$("#retrialerfollowup").val(data.campusobject[0].retrialer_followup);
						$("#retrialinterviewer").val(data.campusobject[0].retrial_interviewer);
						$("#retrialstatus").val(data.campusobject[0].retrial_status);
						$("#retrialevaluation").val(data.campusobject[0].retrial_evaluation);
						$("#unacceptablereasonid").val(data.campusobject[0].unacceptable_reason_id);
						$("#processid").val(data.campusobject[0].process_id);
						$("#offerfollowup").val(data.campusobject[0].offer_followup);
						$("#offerstatus").val(data.campusobject[0].offer_status);
						$("#abandonreason").val(data.campusobject[0].abandon_reason);
						$("#abandondetails").val(data.campusobject[0].abandon_details);
						$("#resumeurl").val(data.campusobject[0].resume_url);
						$("#label").val(data.campusobject[0].label);
						
						isaddorupdata();
						//初始附件数据
						resumeaccessory();
						
					}
				});
			}
		}
		
		
		 var isPass=true;//标识是否通过验证
		   var isPassMsg="";//非空验证提示输出
		  var arr=[];
	   	//失去焦点验证
	   	function validator(){
	   		isPassMsg="";
	   		isPass=true;
	   		arr=[];
	   		//姓名
	  		 	if($('#name').val()==null || $('#name').val()=="" ){
	           	$("#name").css("border","1px solid red");
	           	isPassMsg+="请输入姓名,";
	           	isPass=false;
	           }else{
	           	$("#name").css("border","1px solid #ccc");
	           }
			   
			 //性别
	  		 	if($('#gender').val()==null || $('#gender').val()=="" ){
	           	$("#gender").css("border","1px solid red");
	           	isPassMsg+="请选择性别,";
	           	isPass=false;
	           }else{
	           	$("#gender").css("border","1px solid #ccc");
	           }
			   
			   //毕业时间
	  		 	if($('#graduationdate').val()==null || $('#graduationdate').val()=="" ){
	           	$("#graduationdate").css("border","1px solid red");
	           	isPassMsg+="请输入毕业时间,";
	           	isPass=false;
	           }else{
	           	$("#graduationdate").css("border","1px solid #ccc");
	           }
	   		
	   		//电话
				var phone=/^1\d{10}$/;//电话正则
	  		 	if($('.phone').val()==null || $('.phone').val()=="" || !phone.test($(".phone").val())){
	           	$(".phone").css("border","1px solid red");
	           	isPassMsg+="请输入正确的手机号码,";
	           	isPass=false;
	           }else{
	           	$(".phone").css("border","1px solid #ccc");
	           }
	  		 	
				//邮箱
				var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
	  		 	if($('.email').val()==null || $('.email').val()=="" || !email.test($(".email").val())){
	           	$(".email").css("border","1px solid red");
	           	isPassMsg+="请输入正确的邮箱格式，如：XXX@qq.com,";
	           	isPass=false;
	           }else{
	           	$(".email").css("border","1px solid #ccc");
	           }
			   
			   //学校
	  		 	if($('#college').val()==null || $('#college').val()=="" ){
	           	$("#college").css("border","1px solid red");
	           	isPassMsg+="请输入学校,";
	           	isPass=false;
	           }else{
	           	$("#college").css("border","1px solid #ccc");
	           }
	  		 	
	           //学历
	  		 	if($('.study').val()==null || $('.study').val()=="" ){
	           	$(".study").css("border","1px solid red");
	           	isPassMsg+="请选择学历,";
	           	isPass=false;
	           }else{
	           	$(".study").css("border","1px solid #ccc");
	           }
			   
			   //专业
	  		 	if($('#major').val()==null || $('#major').val()=="" ){
	           	$("#major").css("border","1px solid red");
	           	isPassMsg+="请输入专业,";
	           	isPass=false;
	           }else{
	           	$("#major").css("border","1px solid #ccc");
	           }
	           
			   //挂科情况
	  		 	if($('#flunkeddetials').val()==null || $('#flunkeddetials').val()=="" ){
	           	$("#flunkeddetials").css("border","1px solid red");
	           	isPassMsg+="请选择挂科情况,";
	           	isPass=false;
	           }else{
	           	$("#flunkeddetials").css("border","1px solid #ccc");
	           }

				 //工作意向地的省份
	           if($('.province_1').val()==null || $('.province_1').val()=="" || $('.city_1').val()==null || $('.city_1').val()==""){
		           	$(".province_1").css("border","1px solid red");
		           	$(".city_1").css("border","1px solid red");
		           	isPassMsg+="请选择工作意向地,";
		           	isPass=false;
	           }else{
		           	$(".province_1").css("border","1px solid #ccc");
		           	$(".city_1").css("border","1px solid #ccc");
	           }
			   
			   //面试职位类别
	  		 	if($('#positioncategory').val()==null || $('#positioncategory').val()=="" ){
	           	$("#positioncategory").css("border","1px solid red");
	           	isPassMsg+="请选择面试职位类别,";
	           	isPass=false;
	           }else{
	           	$("#positioncategory").css("border","1px solid #ccc");
	           }
			   
	           //推荐岗位
	  		 	if($('.recommendedpost').val()==null || $('.recommendedpost').val()==""){
	           	$(".recommendedpost").css("border","1px solid red");
	           	isPassMsg+="请输入推荐岗位";
	           	isPass=false;
	           }else{
	           	$(".recommendedpost").css("border","1px solid #ccc");
	           }
	           
	          arr=isPassMsg.split(",");
	   	} 
		
		
		
		//提交事件
		function submit() {
			validator();
    		//非空判断
    		if(isPass==false){
    			layer.msg(arr[0]);
    			return;
    		}	           	
				//获取输入框的值
				var id =<%=id%>;
				var empname = "<%=empname %>";
				var empid = <%=empid %>;
				var resumeid = $("#resumeid").val();
				var name = $("#name").val();
				var graduationdate = $("#graduationdate").val();
				var recruitmentcampus = $("#recruitmentcampus").val();
				var gender = $("#gender").val();
				var phone = $("#phone").val();
				var mail = $("#mail").val();
				var college = $("#college").val();
				var degree = $("#degree").val();
				var major = $("#major").val();
				var englishlevel = $("#englishlevel").val();
				var flunkeddetials = $("#flunkeddetials").val();
				
				//现居地 地址拼接
	    		var address ="";
					if($('#province_nowSite').val()!=""){
						address+=$('#province_nowSite').val();
					}
					if($("#city_nowSite").val()!=""){
						address+="-"+$("#city_nowSite").val();
					}
					if($("#district_nowSite").val()!=""){
						address+="-"+$("#district_nowSite").val();
					}
					
				//户口所在地 地址拼接
				var birthplace ="";
					if($('#province_nowSite_2').val()!=""){
						birthplace+=$('#province_nowSite_2').val();
					}
					if($("#city_nowSite_2").val()!=""){
						birthplace+="-"+$("#city_nowSite_2").val();
					}
					if($("#district_nowSite_2").val()!=""){
						birthplace+="-"+$("#district_nowSite_2").val();
					}
				
				//工作意向地 地址拼接
	    		var exceptedworkarea ="";
					if($('#province').val()!=""){
						exceptedworkarea+=$('#province').val();
					}
					if($("#city").val()!=""){
						exceptedworkarea+="-"+$("#city").val();
					}
					if($("#district").val()!=""){
						exceptedworkarea+="-"+$("#district").val();
					}
					
				//工作意向地 地址拼接
				var exceptedworkareb ="";
					if($('#province_2').val()!=""){
						exceptedworkareb+=$('#province_2').val();
					}
					if($("#city_2").val()!=""){
						exceptedworkareb+="-"+$("#city_2").val();
					}
					if($("#district_2").val()!=""){
						exceptedworkareb+="-"+$("#district_2").val();
					}
				
				var positioncategory = $("#positioncategory").val();
				var appliedposition1 = $("#appliedposition1").val();
				var appliedposition2 = $("#appliedposition2").val();
				var recommendedpost = $("#recommendedpost").val();
				var groupinterviewe = $("#groupinterviewe").val();
				var groupscore = $("#groupscore").val();
				var groupevaluation = $("#groupevaluation").val();
				var facetofaceinterviewer = $("#facetofaceinterviewer").val();
				var facetofaceinterview = $("#facetofaceinterview").val();
				var facetofaceevaluation = $("#facetofaceevaluation").val();
				var retrialerfollowup = $("#retrialerfollowup").val();
				var retrialinterviewer = $("#retrialinterviewer").val();
				var retrialstatus = $("#retrialstatus").val();
				var retrialevaluation = $("#retrialevaluation").val();
				var unacceptablereasonid = $("#unacceptablereasonid").val();
				var processid = $("#processid").val();
				var offerfollowup = $("#offerfollowup").val();
				var offerstatus = $("#offerstatus").val();
				var abandondetails = $("#abandondetails").val();
				var abandonreason = $("#abandonreason").val();
				var resumeurl = $("#resumeurl").val();
				var label = $("#label").val();
				var filename = $("#upInfoname").val();
		    	var fileurl = $("#upInfo").val();
		    	var filepath = $("#upInfopath").val();
				
				$('#upInfoname').val("");
				$('#upInfo').val("");
				$('#upInfopath').val("");
				
				var json = {
					//传递参数
					id : id,
					empname : empname,
					empid : empid,
					resumeid : resumeid,
					name : name,
					graduationdate : graduationdate,
					recruitmentcampus : recruitmentcampus,
					gender : gender,
					phone : phone,
					mail : mail,
					college : college,
					degree : degree,
					major : major,
					address : address,
					birthplace : birthplace,
					englishlevel : englishlevel,
					flunkeddetials : flunkeddetials,
					exceptedworkarea : exceptedworkarea,
					exceptedworkareb : exceptedworkareb,
					positioncategory : positioncategory,
					appliedposition1 : appliedposition1,
					appliedposition2 : appliedposition2,
					recommendedpost : recommendedpost,
					groupinterviewe : groupinterviewe,
					groupscore : groupscore,
					groupevaluation : groupevaluation,
					facetofaceinterviewer : facetofaceinterviewer,
					facetofaceinterview : facetofaceinterview,
					facetofaceevaluation : facetofaceevaluation,
					retrialerfollowup : retrialerfollowup,
					retrialinterviewer : retrialinterviewer,
					retrialstatus : retrialstatus,
					retrialevaluation : retrialevaluation,
					unacceptablereasonid : unacceptablereasonid,
					processid : processid,
					offerfollowup : offerfollowup,
					offerstatus : offerstatus,
					abandondetails : abandondetails,
					abandonreason : abandonreason,
					resumeurl : resumeurl,
					label : label,
					Filename : filename,
					Fileurl : fileurl,
					Filepath : filepath

				};
				$.ajax({
					url : "com.recruit.talent.campusrecruitment.addCampusResume.biz.ext",
					type : 'POST',
					data : json,
					success : function(data) {
						layer.msg('保存成功');
						isupdata(data.returncaid);
						if(ID!="" && ID!=null && ID!="null"){
							window.opener.isok();
						}
					}
				});
		}
		
		function isupdata(reid){
    		$("#id").val(reid);
    		if(reid == "" || reid == null){
    			
    		}else{
    			 $("#success").css("display","none");
    			 $("#updata").css("display","");
    			 
    		}
    	
    	}
    	
		//获取下拉框数据字典数据
		function dictionarycombox() {
			$.ajax({
				url : "com.recruit.talent.campusrecruitment.selectDictionCampus.biz.ext",
				type : 'POST',
				success : function(data) {
					//职位类别
					for (var i = 0; i < data.reposition.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reposition[i].dictID);
						$(option).text(data.reposition[i].dictName);
						$('#positioncategory').append(option);
					}
					//学历
					for (var i = 0; i < data.redegree.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.redegree[i].dictID);
						$(option).text(data.redegree[i].dictName);
						$('#degree').append(option);
					}
					//挂科情况
					for (var i = 0; i < data.reflunkeddetials.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reflunkeddetials[i].dictID);
						$(option).text(data.reflunkeddetials[i].dictName);
						$('#flunkeddetials').append(option);
					}
					//英语等级
					for (var i = 0; i < data.reenglishlevel.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reenglishlevel[i].dictID);
						$(option).text(data.reenglishlevel[i].dictName);
						$('#englishlevel').append(option);
					}
					//复试状态
					for (var i = 0; i < data.reretrialstatus.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reretrialstatus[i].dictID);
						$(option).text(data.reretrialstatus[i].dictName);
						$('#retrialstatus').append(option);
					}
					//不录用原因分类
					for (var i = 0; i < data.rerecruit_reason.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.rerecruit_reason[i].dictID);
						$(option).text(data.rerecruit_reason[i].dictName);
						$('#unacceptablereasonid').append(option);
					}
					//各选拔环节状态
					for (var i = 0; i < data.reprocessid.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reprocessid[i].dictID);
						$(option).text(data.reprocessid[i].dictName);
						$('#processid').append(option);
					}
					//Offer状态
					for (var i = 0; i < data.reofferstatus.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reofferstatus[i].dictID);
						$(option).text(data.reofferstatus[i].dictName);
						$('#offerstatus').append(option);
					}
					//放弃原因分类
					for (var i = 0; i < data.rerecruit_reason.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.rerecruit_reason[i].dictID);
						$(option).text(data.rerecruit_reason[i].dictName);
						$('#abandonreason').append(option);
					}
					
				}
			});
		}
		 //查询简历地址
    	function resumeaccessory(){
    		  //debugger;
    		  var flag ="1";
    		  var resumeid = $("#resumeid").val();
    		  
    		  if(resumeid == "" || resumeid == null){
    		  	  return;
    		  }else{
	    		  json={
				        resumeid:resumeid,
			    		flag:flag
			    	};
		    		$.ajax({
						url: "com.recruit.interpolation.interpolationindex.selectResume.biz.ext",
						type:'POST',
						data:json,
						success:function(data){
							if(data.returnAcc.length == 0){
								
							}else{
								$("#content_file").css("display","inline");
								for(var i =0;i<data.returnAcc.length;i++ ){
									$("#file_upload").append("<p class='recruitAcc'><a href="+data.returnAcc[i].accessoryUrl+" target='_blank'>"+data.returnAcc[i].accessoryName+"</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#'  class='delete' onclick='resumnedelete("+data.returnAcc[i].id+",\""+data.returnAcc[i].accessoryPath+"\")'> X </a></p>");
								}
							}
						}
					});
			}
    	
    	} 
    	
    	//删除上传但未保存简历附件
    	function delectfile(upid,filepath){
			resumnedelete(0,filepath);
            var id="#"+upid.id;
            subStringfile(id);
        	$(id).remove();
        	var text=$("#file_upload").text();
        	if(text=="" || text==null){
        		 $("#content_file").css("display","none");
        	}
    	}
        
        //对未上传执行的链接进行删除
        function subStringfile(id){
        	var Arrid = id.substring(3, 4);
        	var filename = $("#upInfoname").val();
	    	var fileurl = $("#upInfo").val();
	    	var filepath = $("#upInfopath").val();
	    	var filenameArr = filename.split("^");
	    	var fileurlArr = fileurl.split("^");
	    	var filepathArr = filepath.split("^");
	    	
	    	var returnfilename = "";
	    	var returnfileuri = "";
	    	var returnpath = "";
	    	for (var i=0 ; i< filenameArr.length ; i++)
			{
				if(i == Arrid){
				
				}else{
					if(returnfilename == "" || returnfilename == null){
						returnfilename = filenameArr[i];
						returnfileuri = fileurlArr[i];
						returnpath = filepathArr[i];
					}else{
						returnfilename = returnfilename+"^"+filenameArr[i];
						returnfileuri = returnfileuri+"^"+fileurlArr[i];
						returnpath = returnpath+"^"+filepathArr[i];
					}
					
				}
			}
        	$('#upInfoname').val(returnfilename);
			$('#upInfo').val(returnfileuri);
			$('#upInfopath').val(returnpath);
        }
        	
    	//删除简历
    	function resumnedelete(id,filepath){
    			var json = {
					id:id,
					filepath:filepath
				};
    			if(id == 0){
    				$.ajax({
						url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
						type : 'POST',
						async: false,
						data : json,
						success : function(data) {
	   					}
	 	 	 		});
    			}else{
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,content: '<div style="padding: 20px 75px 0px;">确认删除附件？</div>'
			        ,closeBtn: false//不显示关闭图标
			        ,btn:['确定','取消']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
			        ,yes: function(index){
			        	$.ajax({
						url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
						type : 'POST',
						async: false,
						data : json,
						success : function(data) {
							if(id == 0){
							}else{
							layer.msg('附件删除成功');
							$("#file_upload").empty();
							resumeaccessory();
							layer.close(index);//关闭页面
							var text=$("#file_upload").text();
				        	if(text=="" || text==null){
				        		 $("#content_file").css("display","none");
				        	}
							}
	   					}
	 	 	 		});
		      	 	}
			        ,btn2: function(index, layero){
					    layer.close(index);//关闭页面
					}
				});
			}
		}
			
			
		//重置
		function clean(){
			location.reload([true]);
		}	
		
        //附件未上传保存提示
        function isEnclosureExist(){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				submit();
				
			}
		}
		
layui.use('upload', function(){
  	var $ = layui.jquery
  	,upload = layui.upload;
  	//人才库批导上传文件插件	
  	upload.render({ //允许上传的文件后缀
	    elem: '#test4'
	    ,url: '<%=request.getContextPath()%>/reports/ExcelFile_uploadserver.jsp'
	    ,accept: 'file' //普通文件
	    ,exts: 'xls' //只允许上传压缩文件
	    ,done: function(res){
	      
	      if(res.msg=="成功"){
	      var json = {"filePath":res.data.filePath};
	      $.ajax({
       		url: 'com.recruit.talent.campusrecruitmentExcelGuide.campusRecruitExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){ 
				
				//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(data.alreadyExist!=null){
		            	//配置Excel表头内容
		            	var head = '{"stutas":"保存状态说明","candidateName":"姓名","candidatePhone":"电话"}';
		            	/* ,"candidateMail":"邮箱","exceptedWorkAreaA":"工作意向地(1)","exceptedWorkAreaB":"工作意向地(2)","degree":"学历","college":"毕业学校","major":"专业","englishLevel":"英语等级",'
		            	+'"gender":"性别","birthplace":"户口所在地","address":"人才现居地（省市区）","recruitmentCampus":"宣讲学校",'
		            	+'"campusPosition":"面试职位类别","flunkedDetials":"挂科情况","groupInterviewe":"无领导小组讨论面试","groupScore":"无领导小组讨论评分","groupEvaluation":"无领导小组讨论评价",'
		            	+'"faceToFaceInterviewer":"一对一面试官","faceToFaceInterview":"一对一面试评分","faceToFaceEvaluation":"一对一面试评价","appliedPosition1":"第一申请岗位",'
		            	+'"appliedPosition2":"第二申请岗位","recommendedPost":"推荐岗位","retrialerFollowup":"复试跟进人姓名","retrialInterviewer":"复试面试官姓名",'
		            	+'"unacceptableReasonId":"不录用原因分类类型","retrialEvaluation":"复试评价","processId":"各校招环节状态","offerFollowup":"offer跟进人姓名","offerStatus":"offer状态",'
		            	+'"abandonReason":"放弃原因分类类型","abandonDetails":"放弃原因描述","retrialStatus":"复试状态","graduationDate":"毕业时间" */
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.alreadyExist);
		            	//获取Excel工作簿名称
		            	var sheetName = "校招人才库批导结果";
		            	//获取Excel文件名称
		            	var fileName = "校招人才库批导结果";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			        }
				}
			})
	     
	      }
	    }
	  });
	}) 
        
	</script>
	
</body>
<script src="<%= request.getContextPath() %>/interpolation/js/resumeupload.js"></script>
</html>
