<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-03-24 10:08:11
  - Description:
-->
<head>
<title>Title</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<style type="text/css">

.btn{
	margin-left: 20px;
}
.layui-tree li ul {
	margin-left: 35px;
}
.modal-content {
	width: 520px;
}
.layui-form-item{
	width: 120px;
}

.laytable-cell-1-0{
	padding-top: 7px;
}

/* 输入框，单选框样式 */
#input{
padding-left: 20px;
}
#label_grade,#label_name,#label_state{
	height: 34px;
	padding: 6px 12px;
    font-size: 14px;
    border: 1px solid #ccc;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border-radius: 4px;
    margin: 10px;
}

#table{
width: 98%;
    margin: auto;
}
/* 模态框 */
.layui-inline{
	width: 100%;
	margin-bottom: 10px;
}
.layui-input-inline{
	width: 320px;
	margin-left: 10px;
	
}
.layui-input{
 border: 1px solid #ccc;
 background-color: #fff;
    background-image: none;
	 border-radius: 4px;
}
.layui-form-label{
	width: 120px;
	text-align: center;
}
#grade,#parentsLabel,#twoLevelLabel{
height: 38px;
	border: 1px solid #ccc;
 background-color: #fff;
    background-image: none;
	 border-radius: 4px;
	 width: 100%;
}
.labelId,.twoLevelLabel{
	display: none;
}
.layui-table-body{
	
}

/* 状态：使用的背景颜色 */
.layui-form-onswitch{
	    background-color: #f5c47f;
    border-color: #f5c47f;
}
</style>
</head>
<body>
	<!-- 按钮 -->
	<div id="input">
		 <label>标签名：</label>
		 <input type="text" id="label_name">
		 
		 <label>标签等级：</label>
			<select id="label_grade">
				<option></option>
				<option value="1">一级</option>
				<option value="2">二级</option>
				<option value="3">三级</option>
			</select>
		 
		 <label>状态：</label>
			<select id="label_state">
				<option></option>
				<option value="1">使用</option>
				<option value="2">停用</option>
			</select>
		
		<button class="btn btn-primary btn-sm" type="button" id="select" onclick="select()">查询</button>
	</div>
		<div id="button">
			<button type="button" class="btn btn-primary btn-sm" id="addLabel"
				onclick="add_label()">添加标签</button>
		</div>


	<!-- table内容 -->
	<div id="table">
		<table id="demo" lay-filter="test"></table>
	</div>
	

	<!-- 一个隐藏的模态框 -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"></div>
				<div class="modal-body">
					<div class="layui-inline labelId">
						<label class="layui-form-label">标签id</label>
						<div class="layui-input-inline">
							<input type="text" id="id" autocomplete="off" class="layui-input" disabled>
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">标签等级</label>
						<div class="layui-input-inline" >
							<select id="grade" onchange="selectParentsLabel(this.value)">
								<option value="0">一级</option>
								<option value="1">二级</option>
								<option value="2">三级</option>
							</select>
						</div>
					</div>
					<div class="layui-inline parentsLabel">
						<label class="layui-form-label ">父标签</label>
						<div class="layui-input-inline">
							<select id="parentsLabel" onchange="twoLevel_label(this.value)">
								<option></option>
							</select>
						</div>
					</div>
					<div class="layui-inline twoLevelLabel">
						<label class="layui-form-label">二级标签</label>
						<div class="layui-input-inline">
							<select id="twoLevelLabel">
								<option></option>
							</select>
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">标签名</label>
						<div class="layui-input-inline">
							<input type="text" id="name" autocomplete="off" class="layui-input">
						</div>
					</div>
				</div>	
				<div class="modal-footer">
					
					<button type="button" class="btn btn-primary" id="success"
						onclick="">提交更改</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<!-- 状态复选框 -->
	<script type="text/html" id="switchTpl">
  	
 	<input type="checkbox" name="state" value="{{d.id}}" lay-skin="switch" lay-text="使用|停用" lay-filter="sexDemo"  {{ d.ableStatus == 1 ? 'checked' : '' }}>
	</script>
	
	<!-- 标签等级重名 -->
	<script type="text/html" id="isSublable">
		
  		{{#  if(d.isSublable == "1"){ }}
   			 一级
  		{{#  } else if(d.isSublable == "2"){ }}
   			二级
  		{{#  } else if(d.isSublable == "3"){ }}
			三级
		{{#  } }}
	
	</script>
	
	

	<script type="text/javascript">
		
		$(function(){
			//按钮默认加载不可操作
			$("#edit").attr("disabled", true);
			$("#delete").attr("disabled", true);
			
			label();//查询所有标签
			select_labelId();
			
			 $('#myModal').on('hide.bs.modal', function () {
     			 window.location.reload();//页面刷新
     		});
		});
		
		var obj;//接收点击节点回调的参数
		var dataOn;//接收监控到的对象属性
		
		//根据输入条件查询标签
		function select(){
			var label_grade=$("#label_grade").val();
			var label_name=$("#label_name").val();
			var label_state=$("#label_state").val();
			label(label_state,label_name,label_grade);
		}
		//查询所有标签
		function label(label_state,label_name,label_grade){
			var json={
				label_state:label_state,
				label_name:label_name,
				label_grade:label_grade
			};
				$.ajax({
					url : "com.recruit.talent.label.selectAllLabel.biz.ext",
					type : "POST",
					data:json,
					success : function(data) {
						var result=data.result;
						obj=data.result;
						layui.use('table', function(){
						  var table = layui.table ,form = layui.form;
						  
						  //展示已知数据
						  table.render({
						    elem: '#demo'
						    ,cols: [[ //标题栏
						    	{field: 'id', title: '序号', sort: true,width:'15%',unresize:true}
						      ,{field: 'lable', title: '标签',event: 'lableSign',width:'25%',unresize:true}
						      ,{field: 'isSublable', title: '标签等级',sort: true,templet: '#isSublable',width:'15%',unresize:true}
						      ,{field: 'supLable', title: '父标签名',width:'25%',unresize:true}
						      ,{field: 'ableStatus', title: '状态',templet: '#switchTpl', unresize: true,width:'18.9%',unresize:true}
						    ]]
						    ,id:"idTest"
						    ,data: result
						    ,height: 472
						    ,page: true //是否显示分页
						    ,limits: [10, 20, 50]
						    ,limit: 10 //每页默认显示的数量
						    
						  });
						  
						  //监听单元格事件
						  table.on('tool(test)', function(obj){
						    var data = obj.data;
						    if(obj.event === 'lableSign'){
						      layer.prompt({
						        formType: 2
						        ,title: '修改 序号 为  [ '+ data.id +' ]  的标签名'
						        ,value: data.lable	//当前输入框的值
						      }, function(value, index){
						        layer.close(index);
						        //修改标签名
						        updateLable(data.id,value,data.isSublable,data.lableId,data.ableStatus);
						        //同步更新表格和缓存对应的值
						        obj.update({
						          lable: value
						        });
						      });
						    }
						  });
						  
						    //监听标签状态操作
					        form.on('switch(sexDemo)', function(obj){
							  var condition=obj.elem.checked;	//接收条件信息
							  var ID=this.value;//当前标签id
							  
							  //如果条件为false时
							  if(condition==false){
							  	var state=2;	//状态为2
							  	updateState(ID,state);//执行修改方法
							  	
							  	//如果条件true时
							  }else if(condition==true){
							 	 var state=1;		//状态为1
							  	updateState(ID,state);	//执行修改方法
							  }
							 });
						});
					}
				});
			}
		
		var label_Id;//接收 select_labelId（）方法的结果
		//查询总ID+1
		function select_labelId() {
			$.ajax({
				url : "com.recruit.talent.label.selectLabel.biz.ext",
				type : "POST",
				success : function(data) {
					label_Id = 1 + data.label[0].subId;
				}
			});
		}
		//下拉框取值
		function selectParentsLabel(grade){
			var id;
			if(grade==2){	//判断标签等级为三级时，父标签改为一级，二级标签为显示
				id=1;
				$(".parentsLabel label").html("一级标签");
				$(".twoLevelLabel").show();
			}else{		//二级标签为隐藏，一级标签改回父标签
				id=grade;
				$(".parentsLabel label").html("父标签");
				$(".twoLevelLabel").hide();
			}
			
			var json={
				grade:id
			};
			$.ajax({
				url : "com.recruit.talent.label.selectLabelId.biz.ext",
				type : "POST",
				data:json,
				success : function(data) {
					if(grade==1){	//如果标签等级为二级时，则清空内容并拼接下拉框的值
						$("#parentsLabel")[0].length=0;
						for(var i = 0 ;i<data.label.length;i++){
						  $("#parentsLabel").append("<option value="+data.label[i].id+">"+data.label[i].lable+"</option>");
						}
					}else if(grade==2){	//如果标签等级为三级时，则清空内容并拼接下拉框的值
						$("#parentsLabel")[0].length=0;
						for(var i = 0 ;i<data.label.length;i++){
						  $("#parentsLabel").append("<option value="+data.label[i].id+">"+data.label[i].lable+"</option>");
						}
						twoLevel_label($("#parentsLabel").val());//取父标签的值查询子标签
					}else{
						$("#parentsLabel")[0].length=0;
					}
				}
			});
		}
		
		//二级标签下拉框
		function twoLevel_label(id){
			var json={
				id:id
			};
			$.ajax({
				url : "com.recruit.talent.label.selectLabelId.biz.ext",
				type : "POST",
				data:json,
				success : function(data) {
				//清空内容并拼接下拉框
					$("#twoLevelLabel")[0].length=0;
					 for(var i = 0 ;i<data.label.length;i++){
						  $("#twoLevelLabel").append("<option value="+data.label[i].id+">"+data.label[i].lable+"</option>");
						} 
				}
			});
		}

		//添加标签
		function add_label() {
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>添加标签</h4>");
			$("#id").val(label_Id);//标签id
			$("#success").attr("onclick", "submit_add_label()"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		function submit_add_label() {
			var id = $("#id").val();
			var name = $("#name").val();
			var grade=$("#grade").val();//接收标签等级
			var parentLabel;//接收父标签
			var grade_2;
			if(grade==0){//判断标签等级为一级
				parentLabel=0;
				grade_2=1;
			}else if(grade==1){//判断标签等级为二级
				parentLabel=$("#parentsLabel").val();
				grade_2=2;
			}else if(grade==2){//判断标签等级为三级
				parentLabel=$("#twoLevelLabel").val();
				grade_2=3;
			}
			
			var json = {
				id : id,
				name : name,
				grade : grade_2,
				parentLabel : parentLabel
			};
			
			if(name==null || name==""){	//如果标签名为空则提示输入标签名
				layer.msg("请输入标签名！");
			}else{
				$.ajax({
					url : "com.recruit.talent.label.insertSublabel.biz.ext",
					type : "POST",
					data : json,
					success : function(data) {
						if (data.success == 'ok') {
							layer.msg("添加成功！");
							$('#myModal').modal('hide');//模态框关闭
							setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
								window.location.reload();//页面刷新
							},500);
						} else {
							layer.msg("添加失败！");
						}
						;
					}
				});
			}
		}
		
		//根据id修改标签名
		function updateLable(id,label,isSublable,lableId,ableStatus){
			var json={
	        	id:id,
	        	lable:label,
	        	is_sublable:isSublable,
	        	lable_id:lableId,
	        	able_status:ableStatus
	        };
	        $.ajax({
				url : "com.recruit.talent.label.updateLabelById.biz.ext",
				type : "POST",
				data:json,
				success : function(data) {
					if(data.success=="ok"){
						layer.msg("修改成功！");
						/* setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
							window.location.reload();//页面刷新
						},1000); */
					}else{
						layer.msg("修改失败！");
					}
				}
	        });
		}
		
		//根据id修改标签状态
		function updateState(id,state){
			var json={
				id:id,
				state:state
			}
			$.ajax({
				url : "com.recruit.talent.label.updateLabelState.biz.ext",
				type : "POST",
				data:json,
				success : function(data) {
					if(data.success=="ok"){
						layer.msg("修改成功！");
						setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
							window.location.reload();//页面刷新
						},500);
					}else{
						layer.msg("修改失败！");
					}
				}
	        });
		}
	</script>
</body>
</html>