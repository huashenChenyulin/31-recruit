<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-04-25 16:15:31
  - Description:
-->
<head>
<title>Title</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/Talent/label_manage/b.tabs.css" type="text/css">

<style type="text/css">
	
	div.menuSideBar li.nav-header { font-size: 14px; padding: 3px 15px; }
	div.menuSideBar .nav-list > li > a, div.menuSideBar .dropdown-menu li a { -webkit-border-radius: 0px; -moz-border-radius: 0px; -ms-border-radius: 0px; border-radius: 0px; }
	
	.col-md-2{
		width: 20%;
		min-height:480px;
		overflow-y:auto;
		max-height:480px;
	}
	.col-md-10{
		width: 80%;
	}
	.container{
		    width: 90%;
    	
	}
	.tab-content{
		height: 445px!important;
	}
	h3{
		color: #f0ad4e;
	}
	.nav-tabs li{
		display: none;
	}
	
	/* 添加、删除 */
	#add-label,#update-label{
		margin-right:10px;
	    cursor: pointer;
	    font-size: 14px; 
	    color: #f0ad4e;
	    text-decoration: underline;
	}
	
	
	/* 模态框 */
	.modal-dialog{
		width: 350px;
	}
	.layui-input-inline{
	 	width: 220px;
	}
	.layui-input{
		 border: 1px solid #ccc;
		 background-color: #fff;
		 background-image: none;
		 border-radius: 4px;
	}
	
</style>
</head>
<body>
	<div class="content">
		<div class="container">
			<h3 class="page-header">标签管理</h3>
			<div class="">
				<div class="row-fluid">
					<div class="col-md-2" style="padding-left: 0px;">
						<div class="well menuSideBar" style="padding: 8px 0px;">
							<ul class="nav nav-list" id="menuSideBar">
								<li class="nav-header">
									<i class="layui-icon" id="add-label" onclick="add_label()" title='添加一级标签'>&#xe654;新增</i>
									
									<i class="layui-icon" id="update-label" onclick="">&#xe642;编辑</i>	
								</li>
								<li class="nav-divider"></li>
								
							</ul>
						</div>
					</div>
					<div class="col-md-10" id="mainFrameTabs" style="padding: 0px;">

						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							 
						</ul>


						 <!-- Tab panes -->
					      <div class="tab-content">
					        <div class="tab-pane active" id="bTabs_navTabsMainPage">
					          
					        </div>
					      </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 一个隐藏的模态框 -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"></div>
				<div class="modal-body">
					
					<div class="layui-inline">
						<label class="layui-form-label">标签名</label>
						<div class="layui-input-inline">
							<input type="text" id="name" autocomplete="off" class="layui-input">
						</div>
						<input type="hidden" id="hidden">
					</div>
				</div>	
				<div class="modal-footer">
					
					<button type="button" class="btn btn-primary" id="success"
						onclick="">保存</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						 id="remove" onclick="delete_label()">删除</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						 id="close">关闭
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	
<script type="text/javascript" src="<%=request.getContextPath()%>/Talent/label_manage/b.tabs.js" ></script> 
	<script type="text/javascript" src="<%=request.getContextPath()%>/Talent/label_manage/demo.js" ></script>
	<script type="text/javascript">
		
		$(function(){
			sublable();
			select_labelId();
			
			//成功或错误提示
			layui.use('layer', function(){
			  var layer = layui.layer;
			  
			}); 
			
		});
		
		//查询一级菜单
		function sublable(){
			var json={
				isSublable:'1'
			};
			$.ajax({
				url: "com.recruit.talent.label.selectParentsLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					//循环进行拼接
					for(var i=0;i<data.sublabel.length;i++){
						//定义参数接收加载链接
					    var funurl="<%=request.getContextPath()%>/Talent/label_manage/label_children.jsp?id="+data.sublabel[i].id;
						$("#menuSideBar").append("<li mid='tab"+i+"' funurl='"+funurl+"'>"
								+"<a tabindex='-1' href='javascript:void(0);' id='"+data.sublabel[i].id+"'>"+data.sublabel[i].lable+"</a>"
								+"</li>");
					}
					//加载监听方法
					monitors();
					
				}
			});
		} 
		
		//监听
		function monitors(){
			//监听点击方法
			$('a',$('#menuSideBar')).on('click', function(e) {
				e.stopPropagation();
				var li = $(this).closest('li');
				var menuId = $(li).attr('mid');
				var url = $(li).attr('funurl');
				var id = $(this).attr('id');
				var title = $(this).text();
				$('#mainFrameTabs').bTabsAdd(menuId,title,url);
				
				//修改方法
				update(id,title);
			});
		}
		
		var label_Id;//接收 select_labelId（）方法的结果
		//查询总ID+1
		function select_labelId() {
			$.ajax({
				url : "com.recruit.talent.label.selectLabel.biz.ext",
				type : "POST",
				success : function(data) {
					label_Id = 1 + data.label[0].subId;
				}
			});
		}
		
		//添加标签模态框
		function add_label() {
			$("#remove").hide();
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>添加一级标签</h4>");
			$("#name").val("");
			$("#success").text("保存");//把按钮改为保存
			$("#success").attr("onclick", "input_label()"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		//添加方法
		function input_label() {
		
			var name = $("#name").val();
			
			var json = {
				id : label_Id,
				name : name,
				grade : "1",
				parentLabel : "0",
				state:"1"
			};
			
			if(name==null || name==""){	//如果标签名为空则提示输入标签名
				layer.msg("请输入标签名！");
			}else{
				$.ajax({
					url : "com.recruit.talent.label.insertSublabel.biz.ext",
					type : "POST",
					data : json,
					success : function(data) {
						if (data.success == 'ok') {
							//定义参数接收加载链接
							 var funurl="<%=request.getContextPath()%>/Talent/label_manage/label_children.jsp?id="+data.label.id;
							$("#menuSideBar").append("<li mid='tab"+data.label.id+"' funurl='"+funurl+"'>"
								+"<a tabindex='-1' href='javascript:void(0);' id='"+data.label.id+"'>"+data.label.lable+"</a></li>");
							layer.msg("添加成功！");
							select_labelId();//查询id并进行+1
							//加载监听方法
							monitors();
							$('#myModal').modal('hide');//模态框关闭
							
						} else {
							layer.msg("添加失败！");
						}
						
					}
				});
			}
		}
		
		//传参到修改方法
		function update(id,name){
			$("#update-label").attr("onclick", "update_label("+id+",\""+name+"\")");
		}
		//修改标签模态框
		function update_label(id,name){
			$("#remove").show();
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>修改标签</h4>");
			$("#hidden").val(id);
			$("#name").val(name);
			$("#success").text("修改");//把按钮改为修改
			$("#success").attr("onclick", "update_label2("+id+")"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		//修改方法
		function update_label2(id){
			var name=$("#name").val();
			var json={
				id:id,
				lable:name
			}
			$.ajax({
				url : "com.recruit.talent.label.updateLabelById.biz.ext",
				type : "POST",
				data : json,
				success : function(data) {
					if (data.success == 'ok') {
						var obj=".nav-list #"+id;
						$(obj).text(name);
						layer.msg("修改成功！");
						$('#myModal').modal('hide');//模态框关闭
					} else {
						layer.msg("修改失败！");
					}
				}
			});
		}
		
		
		//删除方法
		function delete_label(){
			var id=$("#hidden").val();
			var name = $("#name").val();
			var json={
				id:id,
				state:"2"
			};
			layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content: '<div style="padding: 20px 75px;">标签名为：'+name+'<br>是否确定要删除此标签？</div>'
		        ,closeBtn: false
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,closeBtn: false//不显示关闭图标
		        ,yes: function(index){
		        	$.ajax({
	                 	url: "com.recruit.talent.label.updateLabelState.biz.ext",
						type:'POST',
						data:json,
						success:function(data){
							if(data.success=="ok"){
								layer.msg("删除成功！");
								layer.close(index);//关闭页面
								setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
									location.reload();//页面刷新
								},500);
							}else{
								layer.msg("删除失败！");
							}
						}
	                 });
		        }
		        ,btn2: function(){
		          layer.closeAll();//关闭页面
		        }
		  }); 
		}
		
		
	</script>
	
</body>
</html>