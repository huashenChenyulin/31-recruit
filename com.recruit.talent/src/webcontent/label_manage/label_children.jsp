<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-04-26 09:17:44
  - Description:
-->
<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <%
    	String id=request.getParameter("id");
     %>
     
     <style type="text/css">
     	
     	/* 添加编辑按钮 */
     	#header-button{
			margin-bottom: 15px;
		}
		#add-label{
		    cursor: pointer;
		    font-size: 14px; 
		    color: #f0ad4e;
		    text-decoration: underline;
		    margin: 20px;
		}
     	/* end */
     	
     	#content-Two_label{
		    overflow-y: auto;
			border-radius:4px;
			background-color: #ffffff;
		}
		#content-Two_label h6 a{
			cursor:pointer;
			font-size: 14px;
			margin-right: 50px;
		}
		#content-Two_label h6 i{
			margin-right: 10px;
			cursor: pointer;
		}
		.list-group-item{
			font-size:15px;
			background-color: #f2f2f2;
		}
		.panel-title{
			font-size: 12px;
			height: 25px;
   			 line-height: 25px;
		}
		.panel-body p{
			width: 25%;
		    float: left;
		    line-height: 30px;
		    color: #0b8df5;
   			display: block;
   			
		}
		.panel-body span{
			margin-right: 40px;
			cursor: pointer;
		}
		.panel-body span:HOVER{
			text-decoration:underline;
		}
		
		
		#update{
			display: none;
			cursor: pointer;
		}
		
		
		/* 模态框 */
		.modal-dialog{
			width: 350px;
		}
		.layui-input-inline{
		 	width: 220px;
		}
		.layui-input{
			 border: 1px solid #ccc;
			 background-color: #fff;
			 background-image: none;
			 border-radius: 4px;
		}
		#remove{
			display: none;
		}
		
		
		#update-label{
			display: none;
		}
		#content-Two_label h6 i{
			color: #f0ad4e;
		}
     </style>
</head>
<body>

<div id="wrap">
	
	<div id="header-button">
		<i class="layui-icon" id="add-label" onclick="add_label()" title='添加二级标签'>&#xe654;新增</i>
	</div>
	<!-- 标签 -->
	<div id="main-center">
		<div id="content-Two_label"></div>
	</div>
</div>
	
<!-- 一个隐藏的模态框 -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"></div>
				<div class="modal-body">
					
					<div class="layui-inline">
						<label class="layui-form-label">标签名</label>
						<div class="layui-input-inline">
							<input type="text" id="name" autocomplete="off" class="layui-input">
						</div>
							<input type="hidden" id="hidden">
							<input type="hidden" id="sublabel">
					</div>
				</div>	
				<div class="modal-footer">
					
					<button type="button" class="btn btn-primary" id="success"
						onclick="">保存</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						 id="remove" onclick="delete_label()">删除</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						 id="close">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->	
	</div>

	<script type="text/javascript">
	
		var labelId="<%=id %>";
		
		$(function(){
			secondLevel();//查询二三级标签
			select_labelId();//查询最大id值进行+1
			//成功或错误提示
			layui.use('layer', function(){
			  var layer = layui.layer;
			  
			}); 
		});
		
		var label_Id;//接收 select_labelId（）方法的结果
		//查询总ID+1
		function select_labelId(){
			$.ajax({
				url : "com.recruit.talent.label.selectLabel.biz.ext",
				type : "POST",
				success : function(data) {
					label_Id = 1 + data.label[0].subId;
				}
			});
		}
		
		//查询二三级标签
		function secondLevel(){
			var json={
				lable_id:labelId
			};
			$.ajax({
				url: "com.recruit.talent.label.secondLevelLabel.biz.ext",
				type:'POST',
				data:json,
				success:function(data){
					$("#content-Two_label").empty();//清空数据
					 for(var i=0;i<data.result.length;i++){
						var content_Two_label ="<div><div class='panel-heading'>"
							+"<h6 class='panel-title'>"
							+"<a data-toggle='collapse' id='"+data.result[i].id+"' onclick='update_label(1,"+data.result[i].id+",\""+data.result[i].lable+"\")'>"
							+""+data.result[i].lable+"</a>"
							+"<i class='layui-icon' id='update-label' title='编辑' onclick='update_label(1,"+data.result[i].id+",\""+data.result[i].lable+"\")'>&#xe642;</i>"
							+"<br>"
							+"<i class='layui-icon' id='add-threeLevel-label' title='添加三级标签'  onclick='threeLevel_label("+data.result[i].id+")' >&#xe654;</i>"
							+"</h6></div>"
							+"<div class='panel-collapse collapse in'>"
							+"<div class='panel-body' id='panel_label'><ul class='"+data.result[i].id+"'>";
							var recruitSublable = data.result[i].recruitSublable;//将二级标签中的子标签赋值到recruitSublable
							//判断  recruitSublable 是否空
							if(typeof(recruitSublable) != "undefined"){
								//如果  recruitSublable 长度大于0则循环加载子标签
								if(recruitSublable.length>0){
									for(var j=0;j<recruitSublable.length;j++){
										content_Two_label +="<li><p><span id='"+recruitSublable[j].id+"' onclick='update_label(2,"+recruitSublable[j].id+",\""+recruitSublable[j].lable+"\")'>"+recruitSublable[j].lable+"</span>"
										+"<i class='layui-icon update' title='编辑' id='update' data='"+recruitSublable[j].id+"' onclick=''>&#xe642;</i>"
										+"</p></li>";
									}
								}
							}
							+"</ul></div></div></div></div>";
							
						$("#content-Two_label").append(content_Two_label);
						
					}
					
				}
			});
		}
		
		//添加二级标签模态框
		function add_label() {
			$("#remove").hide();
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>添加二级标签</h4>");
			var id=labelId;
			$("#name").val("");
			$("#success").text("保存");//把按钮改为保存
			$("#success").attr("onclick", "submit_add(2,"+id+")"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		// 添加三级标签模态框 
		function threeLevel_label(id) {
			$("#remove").hide();
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>添加三级标签</h4>");
			$("#name").val("");
			$("#success").text("保存");//把按钮改为修改保存
			$("#success").attr("onclick", "submit_add(3,"+id+")"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		//添加方法
		function submit_add(grade,id) {
			var name = $("#name").val();
			
			var json = {
				id:label_Id,
				name : name,
				grade:grade,
				parentLabel : id,
				state:"1"
			};
			
			if(name==null || name==""){	//如果标签名为空则提示输入标签名
				layer.msg("请输入标签名！");
			}else{
				$.ajax({
					url : "com.recruit.talent.label.insertSublabel.biz.ext",
					type : "POST",
					data : json,
					success : function(data) {
						//如果是添加三级标签
						if(grade==3){
							if (data.success == 'ok') {
							 	var obj="."+id;
								$(obj).append("<li><p><span onclick='update_label(2,"+data.label.id+",\""+data.label.lable+"\")' id='"+data.label.id+"'>"+data.label.lable+"</span></p></li>");
								select_labelId();//查询id并进行+1
								layer.msg("添加成功！");
								$('#myModal').modal('hide');//模态框关闭
							} else {
								layer.msg("添加失败！");
							};
						
						//如果是添加二级标签
						}else if(grade==2){
							if (data.success == 'ok') {
								var content_Two_label ="<div><div class='panel-heading'>"
									+"<h6 class='panel-title'>"
									+"<a data-toggle='collapse' id='"+data.label.id+"' onclick='update_label(1,"+data.label.id+",\""+data.label.lable+"\")'>"+data.label.lable+"</a>"
									+"<i class='layui-icon' id='update-label' title='编辑' onclick='update_label(1,"+data.label.id+",\""+data.label.lable+"\")'>&#xe642;</i>"
									+"<br>"
									+"<i class='layui-icon' id='add-threeLevel-label' title='添加三级标签'  onclick='threeLevel_label("+data.label.id+")' >&#xe654;</i>"
									+"</h6></div>"
									+"<div class='panel-collapse collapse in'>"
									+"<div class='panel-body' id='panel_label'><ul class='"+data.label.id+"'>";
									+"</ul></div></div></div></div>";
									
								$("#content-Two_label").append(content_Two_label);
								select_labelId();//查询id并进行+1
								layer.msg("添加成功！");
								$('#myModal').modal('hide');//模态框关闭
								
							} else {
								layer.msg("添加失败！");
							};
						}
					}
				});
			}
		}
		
		
		//根据id修改标签名
		function update_label(i,id,name) {
			$("#remove").show();
			$(".modal-header").empty();//清空内容
			$(".modal-header").append("<h4 class='modal-title' id='myModalLabel'>修改标签</h4>");
			$("#name").val(name);//
			$("#hidden").val(id);
			$("#sublabel").val(i);
			$("#success").text("修改");//把按钮改为修改
			$("#success").attr("onclick", "update_lable("+i+","+id+")"); //改变提交更改触发的方法
			$("#myModal").modal('show');//模态框打开
		}
		//修改方法
		function update_lable(i,id){
			var name = $("#name").val();
		
			var json={
	        	id:id,
	        	lable:name
	        };
	        $.ajax({
				url : "com.recruit.talent.label.updateLabelById.biz.ext",
				type : "POST",
				data:json,
				success : function(data) {
					//标识为1时
					if(i==1){
						if(data.success=="ok"){
							var obj=".panel-title #"+id;
							$(obj).text(name);
							secondLevel();//查询二三级标签
							layer.msg("修改成功！");
							$('#myModal').modal('hide');//模态框关闭
							
						}else{
							layer.msg("修改失败！");
						}
					//标识为2时
					}else if(i==2){
						if(data.success=="ok"){
							var obj=".collapse ul li p #"+id;
							$(obj).text(name);
							secondLevel();//查询二三级标签
							layer.msg("修改成功！");
							$('#myModal').modal('hide');//模态框关闭
							
						}else{
							layer.msg("修改失败！");
						}
					}
					
				}
	        });
		}
		
		//删除方法
		function delete_label(){
			var id=$("#hidden").val();
			var name = $("#name").val();
			var i= $("#sublabel").val();	//标识
			var json={
				id:id,
				state:"2"
			};
			layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content: '<div style="padding: 20px 75px;">标签名为：'+name+'<br>是否确定要删除此标签？</div>'
		        ,closeBtn: false
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,closeBtn: false//不显示关闭图标
		        ,yes: function(index){
		        	$.ajax({
	                 	url: "com.recruit.talent.label.updateLabelState.biz.ext",
						type:'POST',
						data:json,
						success:function(data){
							//标识为2的时候
							if(i==2){
								if(data.success=="ok"){
									var obj=".collapse ul li p #"+id;
									var obj2=$(obj).parent().parent();//查找obj的父两级
									obj2.remove();//清除
									layer.msg("删除成功！");
									layer.close(index);//关闭页面
									
								}else{
									layer.msg("删除失败！");
								}
							
							//标识为1的时候
							}else if(i==1){
								if(data.success=="ok"){
									var obj=".panel-heading h6 #"+id;
									var obj2=$(obj).parent().parent().parent();//查找obj的父三级
									obj2.remove();//清除
									layer.msg("删除成功！");
									layer.close(index);//关闭页面
								}else{
									layer.msg("删除失败！");
								}
							}
						}
	                 });
		        }
		        ,btn2: function(){
		          layer.closeAll();//关闭页面
		        }
		  });
			
		}
    </script>
</body>
</html>