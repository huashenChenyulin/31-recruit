<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.io.FileInputStream"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import ="java.io.IOException" %>

<%
	//获得请求文件名 
	String filename = request.getParameter("filename");
	String name = request.getParameter("name");
	System.out.println(filename);

	//设置文件MIME类型  根据文件的类型设置response对象的ContentType 
	//response.setContentType("application/octet-stream"); //-> （ 二进制流，不知道下载文件类型）
	response.setContentType(getServletContext().getMimeType(filename));  	//getMimeType(String) -->返回指定文件名的MIME类型。典型情况是基于文件扩展名，而不是文件本身的内容（它可以不必存在）。如果MIME类型未知，可以返回null
	//设置Content-Disposition  设置response的头信息 
	name = new String(name.getBytes(), "ISO-8859-1");
	response.setHeader("Content-Disposition", "attachment;filename="+ name);
	//读取目标文件，通过response将目标文件写到客户端 
	//获取目标文件的绝对路径 
	String fullFileName = getServletContext().getRealPath(filename);
	System.out.println(fullFileName);
	//读取文件 
	InputStream in = new FileInputStream(fullFileName);
	OutputStream outs = response.getOutputStream();

	//写文件 
	int b;
	while ((b = in.read()) != -1) {
		outs.write(b);
	}
	out.clear();   //不加上去会报getOutputStream() has already been called for this response异常 
	out = pageContext.pushBody(); //不加上去会报异常 --   // 1、返回一个新的BodyContent(代表一个HTML页面的BODY部分内容）2、保存JspWriter实例的对象out3、更新PageContext的out属性的内容
	outs.close();
	
%>