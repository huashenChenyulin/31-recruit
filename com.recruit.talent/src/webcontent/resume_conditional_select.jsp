<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.UserObject"%>
<%@page import="com.eos.foundation.eoscommon.ResourcesMessageUtil"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="commonj.sdo.*,java.util.*,java.text.SimpleDateFormat"%>
<%@page import="com.eos.web.taglib.util.XpathUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wenjin
  - Date: 2018-03-19 15:30:58
  - Description:
-->

<head>
<title>人才库展示页</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link type="text/css" rel="styleSheet"
	href="<%= request.getContextPath() %>/Talent/css/resume_conditional_select.css" />
	
	<style type="text/css">
		#select2{
			margin-left: 28%;
		}
		#degree,#degree2{
			width: 35%;
    		display: initial;
		}
		#degree2{
			margin-left: 0px;
		}
	</style>
</head>
<body>
	<div id="content_left">
		<div id="talent_sum">
			
		</div>
		<div id="query_talent_sum">
			
		</div>
		<div id="content_label">
				<div id="content_label_button">
					<div id="add_label">
						标签选择<i class="layui-icon">&#xe602;</i>
					</div>
				</div>
				<div id="content_label2"></div>
				
			
		</div>
		
		<hr style="background-color: #797979; width: 82%; margin-left: 27px;margin-top: 8px;margin-bottom: 20px;">
		
		<div id="condition" >
			<p>职位类别</p>
			<select class="form-control" id="positioncatagory"
				name="positioncatagory">
				<option value=""></option>
			</select>
	
			<p>意向职位</p>
			<input type="text" class="form-control" id="expectedposition"
				name="expectedposition">
	
			<p>意向工作地</p>
			<input type="text" class="form-control" id="exceptedworkarea" name="exceptedworkarea">
	
			<p>期待薪资</p>
			<select class="form-control" id="expectedsalary" name="expectedsalary">
				<option value=""></option>
			</select>
			<div>
				<p>学历</p>
				<select class="form-control" id="degree" name="degree">
					<option value=""></option>
				</select>
				<span style="margin-left: 10px;margin-right: 10px;">—</span>
				<select class="form-control" id="degree2" name="degree2">
				</select>
			</div>
	
			<!-- <p>年龄</p>
			<input type="text" class="form-control" id="birthdate"
				name="birthdate"> -->
			<p>姓名</p>
			<input type="text" class="form-control" id="userName"
				name="userName">
				<p>创建人姓名</p>
			<input type="text" class="form-control" id="recruitName"
				name="recruitName">
			<!-- <p style="margin-left: 55%; margin-top: -66px;">性别</p>
			<select class="form-control" id="gender" name="gender"
				style="margin-left: 55%;">
				<option value=""></option>
				<option value="1">男</option>
				<option value="2">女</option>
			</select> -->
	
			
			<p>面试部门</p>
			<input class="layui-input" id="orgcentre" onclick="getOrg(this)" data="" alias="" placeholder="部门选择" value="" style="width: 82%;margin-left: 25px;margin-top: 13px;">
	
			<p>放弃原因</p>
			<select class="form-control" id="abandonreason" name="abandonreason">
				<option value=""></option>
			</select>
	
			<p>人才状态</p>
			<select class="form-control" id="talentstatus" name="talentstatus">
				<option></option>
			</select>
		</div>
		<button type="button" class="btn btn-primary" id="select"
			name="select" onclick="selectsubmit()">搜 索</button>
		<button type="button" class="btn btn-default btn-center" id="clean"
		name="clean" onclick="clean()">重 置</button>


	</div>

	<div id="content_left1">
		<a href="#" onclick="otherbselct()"
			style="color: #0033cc; line-height: 40px;"><i class="layui-icon"
			style="font-size: 16px;">&#xe609;</i>&nbsp;返回 </a>
	</div>

	<div id="content_topright"></div>

	<div id="content_bottomright">
		<!-- <ul id="mytalentTab" class="nav nav-tabs">
			<li class="active" onclick=""><a href="" data-toggle="tab">
					人才库 </a></li>
		</ul> -->
		<div id="talentTop">
			<ul id="talentsend" style="width: 62%;">
				<li>基本信息</li>
			</ul>
			<ul id="talentdate" style="width: 18%;">
				<li>更新日期</li>
			</ul>
			<ul id="talentstuta">
				<li>操作</li>
			</ul>
		</div>
		<hr>
		<div id="content"></div>
		<div id="pagination"></div>
	</div>
	
	
	<!-- 标签 -->
	<div id="main-center">
		<div id="content-one_label">
			<ul class="list-group">
			</ul>
		</div>
		<div id="content-Two_label"></div>
	</div>
	
	
<script
	src="<%= request.getContextPath() %>/Talent/js/resume_conditional_select.js"
	type="text/javascript"></script>
	
	
	<%
	//获取当前招聘负责人ID和名字
	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
%>


	<script type="text/javascript">
		
		//回车键触发点击事件
		$("body").keydown(function() {
			var event=arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异
             if (event.keyCode == "13") {//keyCode=13是回车键
                 $('#select').click();
             }
         });
	
			//获取数据字典
			dictionarycombox();
			 //保存已经查询的所在工单数据集
			var inorder;
			$("#content_left1").hide();
			function otheraselct(){//关键字搜索
				$("#content_left1").show();
				$("#content_left").hide();
			}
			function otherbselct(){//多条件搜索
				$("#content_left1").hide();
				$("#content_left").show();
			}
			var count;
			var dicname;
			//查询人才库
			function selectsubmit(curr,limit,type){
			
					var positioncatagory =$("#positioncatagory").val();
					var expectedposition =$("#expectedposition").val();
					var exceptedworkarea =$("#exceptedworkarea").val();
					var expectedsalary =$("#expectedsalary").val();
					var degree =$("#degree").val();
					var degree2 =$("#degree2").val();
					//var birthdate =$("#birthdate").val();
					var userName = $("#userName").val();
					//var gender =$("#gender").val();
					var recruitName = $("#recruitName").val();
					var abandonreason =$("#abandonreason").val();
					var talentstatus =$("#talentstatus").val();
					var orgcentreid = $("#orgcentre").attr("alias");	
		    		
		    		var da = new Date();  
			    	var nowYear = da.getFullYear(); 
			    	//年龄转换为日期
			    	/* var age ="";
			    	if(birthdate == "" || birthdate == null){
		    			 age = "";
		    		}else{
		    			 age = (nowYear - birthdate);
		    		} */
		    		//获取标签id
		    		var obj="";
		    		for(var key1 in testMap){
						obj +=key1+",";
					} 
		    		obj=obj.substring(0, obj.length-1); 
		    		 
					var json={
						positioncatagory:positioncatagory,
						expectedposition:expectedposition,	
						exceptedworkarea:exceptedworkarea,
						expectedsalary:expectedsalary,
						degree:degree,
						//birthdate:age,
						userName:userName,
						//gender:gender,
						recruitName:recruitName,
						abandonreason:abandonreason,
						talentstatus:talentstatus,
						curr:curr,
						limit:limit,
						orgcentreid:orgcentreid,
						labelId:obj,
						type:type,
						degree1:degree2
					};
			 	$.ajax({
					url: "com.recruit.talent.selectTalentPool.selectTalentpool.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
						if(data.resultanlt.length<1){
							laypage(data.repagecond.count,curr,type);
							$("#content").empty();
							$("#content").append('<p style="text-align: center;color: #9E9E9E;font-weight: 100;margin-bottom: 10px;">无数据</p>');
							return;
						}
						$("#query_talent_sum").text('搜索总数：'+data.repagecond.count+'');
							//主数据
							laypage(data.repagecond.count,curr,type);
							
							var dataArray = new Array();
							//人才库数据集
							dataArray = data.resultanlt;
							
							$("#content").empty();
							for(var i =0;i<dataArray.length;i++ ){
								var gender = sexstatus(dataArray[i].gender);
						        
						     	//渠道
						     	var channels="";
						     	for(var k = 0; k < data.channel.length; k++){
									if(data.resultanlt[i].recruit_channels == data.channel[k].dictID){
										channels = data.channel[k].dictName;
									}
								}
								
						     	//供应商
						     	var provider="";
						     	for(var x = 0; x < data.provider.length; x++){
									if(data.resultanlt[i].recruit_suppliers == data.provider[x].dictID){
										provider = data.provider[x].dictName;
									}
								}
						     	
						     	//薪资
						     	var salary="";
						     	for(var j = 0; j < data.salary.length; j++){
									if(data.resultanlt[i].expected_salary == data.salary[j].dictID){
										salary = data.salary[j].dictName;
									}
								}
								
						        //学历
						        var degree = educationstatus(dataArray[i].degree);
						         
						        //职位类别
						        var catagory=Positioncatagory(dataArray[i].position_catagory);
						         
						        //人才状态
						        var status = talent(dataArray[i].talent_status);
						         
						        //出生年月日转换成年龄
						        var age ="";
						        if(dataArray[i].birth_date =="" || dataArray[i].birth_date == null){
						        	age = "未知";
						        }else{
						         	age = getAge(dataArray[i].birth_date);
						        }
						          
						        //datatime转换成data
						        var curTime;
						        if(dataArray[i].update_time == null || dataArray[i].update_time == ""){
						        	 curTime = "";
						        }else{
							        var oldTime = (new Date(''+dataArray[i].update_time+'')).getTime();
									curTime = new Date(oldTime).format("yyyy-MM-dd");
								}
								 
								//判断是否有第二工作意向地
								var wroke_b ="";
								//防止当第二工作意向的为NULL时，显示在页面上
								var excepted_work_b =dataArray[i].excepted_work_area_b;
								if(excepted_work_b == null || excepted_work_b == "" ){
									wroke_b = "";
								}else{
									wroke_b =dataArray[i].excepted_work_area_b;
								}
								
								var position = "";
								if(dataArray[i].expected_position == "" || dataArray[i].expected_position == null){
									position = "";
								}else{
									position = dataArray[i].expected_position;
								}
								
								//判断是否有第二工作意向地
								var wroke_a ="";
								//防止当第二工作意向的为NULL时，显示在页面上
								var excepted_work_a =dataArray[i].excepted_work_area_a;
								if(excepted_work_a == null || excepted_work_a == "" ){
									wroke_a = "";
								}else{
									wroke_a =dataArray[i].excepted_work_area_a;
								}
							var pushhebe =('<table class="talentTable" onclick="click1('+dataArray[i].resumeid+')" >'+
						          '<tr class="tr">'+
						          '<td class="td"  id="talentname" style="font-size: 14px;height:35px;padding-left: 15px;">'+dataArray[i].candidate_name+'</td>'+
						          '<td class="td" colspan="11" id="sex" style="font-size: 14px;">'+gender+'    |  '+age+'</td>'+
						          '<td class="td"  id="date" style="font-size: 13px;padding-left:8px;">'+curTime+'</td>'+
						          '<td class="td"  id="talentdo" style="font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="addmyorder('+dataArray[i].id+')" style="color: #f0ad4e;font-weight: bold;">添加到我的工单</a></td>'+
						          '</tr>'+
						          '<tr class="tt" style="color:#999999;font-size: 12px;">'+
						          '<td><i class="layui-icon" style="font-size: 16px;padding-left: 15px;">&#xe705;</i>&nbsp;&nbsp;'+degree+'</td>'+
						          '<td colspan="11">'+catagory+'</td>'+
						          '</tr>'+
						          '<tr class="tt" style="color:#999999;font-size: 12px;">'+
						          '<td style="padding-bottom: 10px;"><i class="layui-icon" style="font-size: 15px;padding-left: 15px;">&#xe65e;</i>&nbsp;&nbsp;'+salary+'</td>'+
						          '<td colspan="11" style="padding-bottom: 10px;"><i class="layui-icon" style="font-size: 16px;">&#xe622;</i>&nbsp;&nbsp;'+wroke_a+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+wroke_b+'</td>'+
						          '</tr>'+
						          '<tr class="tt" style="border-top:1px #e6e6e6 solid;">'+
						          '<td style="color:#999999;font-size:13px;padding-left: 15px;">职位：<span style="color:#333">'+position+'</span></td>'+
						          '<td style="color:#999999;font-size: 12px;">人才状态：<span style="color:#333">'+status+'</span></td>'+
						          '<td colspan="14" style="color:#999999;font-size: 12px;">渠道：<span style="color:#333">'+channels+'</span>&nbsp;|&nbsp;供应商：<span style="color:#333">'+provider+'</span></td>'+
						          '</tr>'+
						          '</table>'+
						          '<hr>'
						          );
							$("#content").append(pushhebe);
							
							}
							
						}
					});
			 	}
			 	//分页
		 	function laypage(count,curr,type){
		 		layui.use(['laypage','layer'], function(){
						 var laypage= layui.laypage;
						 laypage.render({
						    elem: 'pagination'
						    ,count: count
						    ,limit: 7
						    ,curr:curr
						    //,layout: ['prev', 'page', 'next', 'skip','count','limit']
						    ,jump: function(obj,frist){
						    	if(!frist){
		    						selectsubmit(obj.curr,obj.limit,type);
						    	}
							}
						});
					});
				}
				
		 	
		 	//加载添加工单框
			function addmyorder(id){
   				event.stopPropagation();//阻止事件冒泡即可
				debugger;
				layer.open({
				  title: '添加到我的工单',
				  area:['330px','200px'],
				  type:1,
				  content: '<div id="content_centre">'+
					'<label style="margin-left: 22px;margin-top: 20px;">职位</label>'+
					'<select class="form-control" id="positions" name="positions" style="width: 230px;margin-left: 70px;margin-top: -29px;">'+
					'<option></option>'+
					'</select>'+
					'</div>'+
					'<div id="content_set">'+
					'<label style="margin-left: 22px;margin-top: 20px;">单号</label>'+
					'<select class="form-control" id="supplier" name="supplier" style="width: 230px;margin-left: 70px;margin-top: -29px;">'+
					'<option></option>'+
					'</select>'+
					'<hr style="margin-top: 10px;">'+
					'<button type="button" style="background-color: #ec971f;border-color: #ec971f;margin-left: 74%;margin-top: 5px;" class="btn btn-warning" id="set" name="set" onclick="setsubmit('+id+')" >添 加</button>'+
					'</div>'
				});   
							
				queryorders();
				//二级联动查询单号
				$("#positions").change(function(){
				    $("#supplier").html("");
				    //获取当前选中职位
				    var position =$(this).val();
				    
				    if(position == null || position == ""){
				    	$("#supplier").html("");
				    	return;
				    }else{
					    json={
					        id:<%=empid %>,
				    		position:position
				    	};
		    		$.ajax({
						url: "com.recruit.interpolation.interpolationindex.selectOrder.biz.ext",
						type:'POST',
						data:json,
						success:function(data){
							//往单号循环赋值
							for(var i =0;i<data.recommend.length;i++ ){
								$("#supplier").append("<option>"+data.recommend[i].recuitOrderId+"</option>");
								}
							}
						});
					}
			 	});
	 	 	 }
	 	 	 
	 	 	 //查询下拉框职位
			function queryorders(){
				var json={
				  id:<%=empid%>
				};
				$.ajax({
					url: "com.recruit.interpolation.interpolationindex.selectPosition.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
					//往职位下拉框循环赋值
					for(var i =0;i<data.recommend.length;i++ ){
						$("#positions").append("<option>"+data.recommend[i].position+"</option>");
						}
					}
				});
		 	}
		 	
			//添加到工单
	    	function setsubmit(id){
	    		//debugger;
	    		//获取当前工单号ID
	    		var oid = id;
	    		//当工单号ID为空时
			 	if(oid == null || oid == ""){
			 		layer.msg('未选中人才，请重新操作');
			 	  	return;
			 	}else{//当工单号ID不为空
		    		var obj = $("#supplier option:selected");
					//获取工单号
					var  orderid  = obj.val();
					if(orderid == "" || orderid == null){//工单号的值为空
						layer.msg('未选择工单');
						return;
					}else{
						//获取当前选择工单
						var json1 = {
							id:oid
						};
						$.ajax({
							url:"com.recruit.talent.talentpoolselect.selectOrderByid.biz.ext",
							type:'POST',
							data:json1,
							success:function(data){
								//标识是否已经添加到工单，1已添加，其他未添加
								var recommendord ="";
								for(var i =0;i<data.recommend.length;i++){
									if(data.recommend[i].recuitOrderId == orderid){
										recommendord = 1;
										}
									}
									if(recommendord == 1){
										layer.msg('已加入工单，请勿重复操作');
										return;
									}else{
										var recrname = "<%=empname%>";
							    		var json2={
										  	id:oid,
										  	orderid:orderid,
										  	//获取当前招聘人ID及姓名
										  	recruiterid:<%=empid%>,
											recruitername : recrname
										};
										$.ajax({
												url : "com.recruit.talent.talentpoolselect.addTalentadOrder.biz.ext",
												type : 'POST',
												data : json2,
												success : function(data) {
													layer.msg('添加成功');
													setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
													layer.closeAll();//关闭窗口
													},1000);
												}
											});
										}
									}
								});
							}
						}
					}
		//获取下拉框数据字典数据
		function dictionarycombox() {
			$.ajax({
				url : "com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
				type : 'POST',
				success : function(data) {
					//职位类别
					for (var i = 0; i < data.reposition.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reposition[i].dictID);
						$(option).text(data.reposition[i].dictName);
						$('#positioncatagory').append(option);
					}
					//期待薪资
					for (var i = 0; i < data.reexpected.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reexpected[i].dictID);
						$(option).text(data.reexpected[i].dictName);
						$('#expectedsalary').append(option);
					}
					//学历
					for (var i = 0; i < data.redegree.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.redegree[i].dictID);
						$(option).text(data.redegree[i].dictName);
						$('#degree').append(option);
					}
					//放弃原因
					for (var i = 0; i < data.reabandonreason.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reabandonreason[i].dictID);
						$(option).text(data.reabandonreason[i].dictName);
						$('#abandonreason').append(option);
					}
					//候选人状态
					for (var i = 0; i < data.retalentstatus.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.retalentstatus[i].dictID);
						$(option).text(data.retalentstatus[i].dictName);
						$('#talentstatus').append(option);
					}
					//初始查询
					selectsubmit(1,7);
				}
			});
		}
		
  		$("#degree").change(function(){
  			//获取选中的下拉框的值
  			var value=$(this).children('option:selected').val();
  			
  			selectDegree(value);
  		});
		
		//查询学历范围
			function selectDegree(degree){
				$.ajax({
					url: "com.recruit.talent.selectTalentPool.selectDegree.biz.ext",
					type:'POST',
					success:function(data){
					//清除id是degree2的下拉框中的option
					$("#degree2 option").remove();
					//定义接收拼接的变量名
					var option="";
					//拼接
					option+='<option value=""></option>';
					//循环
						for(var i=0;i<data.redegree.length;i++){
						//判断如果循环出来的值小于等于接收到的值就进行拼接
							if(data.redegree[i].dictID<=degree){
								option+='<option value="'+data.redegree[i].dictID+'">'+data.redegree[i].dictName+'</option>';
							}
						}
						$("#degree2").append(option);
					}
				});
			}
			$("#degree").change(function(){ 

		         //获取下拉框的文本值
		
		         var checkText=$(this).find("option:selected").text();
		
		         //修改title值
		
		        $(this).attr("title",checkText);
		
		   });
		   $("#degree2").change(function(){ 

		         //获取下拉框的文本值
		
		         var checkText=$(this).find("option:selected").text();
		
		         //修改title值
		
		        $(this).attr("title",checkText);
		
		   });
		
		
		function clean(){
			$('#positioncatagory').val("");
			$('#expectedposition').val("");
			$('#exceptedworkarea').val("");
			$('#expectedsalary').val("");
			$('#degree').val("");
			$('#degree2').val("");
			//$('#birthdate').val("");
			$('#userName').val("");
			//$('#gender').val("");
			$('#recruitName').val("");
			$('#abandonreason').val("");
			$('#talentstatus').val(1);
			$('#orgcentre').val("");
			$('#orgcentre').val("");
			$("#orgcentre").attr("alias","");
			testMap={};
			var obj="#content_label2 span";
			$(obj).remove();//清除
		}
		function click1(id){
			window.open("<%= request.getContextPath()%>/Talent/resumes_select.jsp?id="+id);
		}
		
		
		function isok(){
			//初始查询
			selectsubmit(1,7);
		}
		selectTalentSum();
		//查询人才库总数
		function selectTalentSum(){
			$.ajax({
				url: "com.recruit.talent.selectTalentPool.selectTalentSum.biz.ext",
				type:'POST',
				success:function(data){
					$("#talent_sum").append('简历总数：'+data.sum+'');
				}
			});
		}
	</script>
</body>
</html>
