package com.recruit.talent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;*/




import java.util.Map;
import java.util.regex.Pattern;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;
@Bizlet("校招/人才库批量导入Excel")
public class readExcelTalentPool {
	/**
	 * 校招人才Excel数据导入
	* 查询指定目录中电子表格中所有的数据
	* @param file 文件完整路径
	* @param sheetFile Excel工作簿名称
	* @return
	*/
	@Bizlet("校招人才库Excel导入")
	public static List getAllByExcelSchool(String file){
	   List list=new ArrayList();
	   try {
	   	//判断是否为数字类型值
	   	Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
		   WorkbookSettings ws = new WorkbookSettings();
	       ws.setCellValidationDisabled(true);
	       Workbook rwb=Workbook.getWorkbook(new File(file),ws);
	       Sheet rs=rwb.getSheet(0);//或者rwb.getSheet(0)
	       int clos=rs.getColumns();//得到所有的列
	       int rows=rs.getRows();//得到所有的行
	       
	       System.out.println(clos+" rows:"+rows);
	       for (int i = 1; i < rows; i++) {
	           for (int j = 0; j < 1; j++) {
	           	Map map = new HashMap();
	           	//获取每行所有数据
	               //第一个是列数，第二个是行数
	           	//默认最左边编号也算一列 所以这里得j++
	           	String candidateName=rs.getCell(j++, i).getContents();//人才姓名
	           	String candidatePhone=rs.getCell(j++, i).getContents();//电话
	           	
	           	if((candidatePhone!=null || candidatePhone !=null)&&(candidatePhone!="" || candidatePhone !="")){
	           		
	           	String candidateMail=rs.getCell(j++, i).getContents();//邮箱
	           	String exceptedWorkAreaA=rs.getCell(j++, i).getContents();//工作意向地1（省市区）
	           	String exceptedWorkAreaB=rs.getCell(j++, i).getContents();//工作意向地1（省市区）
	           	String degree=rs.getCell(j++, i).getContents();//学历编码
	           	if(degree!=null&&degree!=""&&degree.contains("-")){
	           	//截取degree编码“-”前面的编码值
	           	degree = degree.substring(0, degree.indexOf('-')); 
	           	}
	           	
	           	String college=rs.getCell(j++, i).getContents();//毕业学校
	           	String major=rs.getCell(j++, i).getContents();//专业
	           	
	           	String englishLevel = rs.getCell(j++, i).getContents();//英语等级编码
	           	if(englishLevel!=null&&englishLevel!=""&&englishLevel.contains("-")){
	           	//截取englishLevel编码“-”前面的编码值
	           	englishLevel = englishLevel.substring(0, englishLevel.indexOf('-')); 
	           	}
	           	String gender=rs.getCell(j++, i).getContents();//性别
	           	if(gender!=null&&gender!=""&&gender.contains("-")){
	           	//截取gender编码“-”前面的编码值
	           	gender = gender.substring(0, gender.indexOf('-')); 
	           	}
	           	String birthplace=rs.getCell(j++, i).getContents();//户口所在地
	           	String address=rs.getCell(j++, i).getContents();//人才现居地（省市区
	           	
	           	String recruitmentCampus=rs.getCell(j++, i).getContents();//宣讲学校
	           	String campusPosition=rs.getCell(j++, i).getContents();//面试职位类别
	           	if(campusPosition!=null&&campusPosition!=""&&campusPosition.contains("-")){
	           	//截取campusPosition编码“-”前面的编码值
	           	campusPosition = campusPosition.substring(0, campusPosition.indexOf('-')); 
	           	}
	           	
	           	String flunkedDetials=rs.getCell(j++, i).getContents();//挂科情况
	           	if(flunkedDetials!=null&&flunkedDetials!=""&&flunkedDetials.contains("-")){
	           	//截取flunkedDetials编码“-”前面的编码值
	           	flunkedDetials = flunkedDetials.substring(0, flunkedDetials.indexOf('-')); 
	           	}
	           	
	           	String groupInterviewe=rs.getCell(j++, i).getContents();//无领导小组讨论面试
	           	String groupScore=rs.getCell(j++, i).getContents();//无领导小组讨论评分
	           	String groupEvaluation=rs.getCell(j++, i).getContents();//无领导小组讨论评价
	           	String faceToFaceInterviewer=rs.getCell(j++, i).getContents();//一对一面试官
	           	String faceToFaceInterview=rs.getCell(j++, i).getContents();//一对一面试评分
	           	String faceToFaceEvaluation=rs.getCell(j++, i).getContents();//一对一面试评价
	           	String appliedPosition1=rs.getCell(j++, i).getContents();//第一申请岗位
	           	String appliedPosition2=rs.getCell(j++, i).getContents();//第二申请岗位
	           	String recommendedPost=rs.getCell(j++, i).getContents();//推荐岗位
	           	String retrialerFollowup=rs.getCell(j++, i).getContents();//复试跟进人姓名
	           	String retrialInterviewer=rs.getCell(j++, i).getContents();//复试面试官姓名
	           	
	           	String unacceptableReasonId=rs.getCell(j++, i).getContents();//不录用原因分类类型编码
	           	if(unacceptableReasonId!=null&&unacceptableReasonId!=""&&unacceptableReasonId.contains("-")){
	           	//截取unacceptableReasonId编码“-”前面的编码值
	           	unacceptableReasonId = unacceptableReasonId.substring(0, unacceptableReasonId.indexOf('-')); 
	           	}
	           	String retrialEvaluation=rs.getCell(j++, i).getContents();//复试评价
	           	String processId=rs.getCell(j++, i).getContents();//各校招环节状态（类型编码）
	           	if(processId!=null&&processId!=""&&processId.contains("-")){
	           	//截取processId编码“-”前面的编码值
	           	processId = processId.substring(0, processId.indexOf('-')); 
	           	}
	           	String offerFollowup=rs.getCell(j++, i).getContents();//offer跟进人姓名
	           	String offerStatus=rs.getCell(j++, i).getContents();//offer状态编码
	           	if(offerStatus!=null&&offerStatus!=""&&offerStatus.contains("-")){
	           	//截取processId编码“-”前面的编码值
	           	offerStatus = offerStatus.substring(0, offerStatus.indexOf('-')); 
	           	}
	           	String abandonReason=rs.getCell(j++, i).getContents();//放弃原因分类类型编码
	           	if(abandonReason!=null&&abandonReason!=""&&abandonReason.contains("-")){
	           	//截取abandonReason编码“-”前面的编码值
	           	abandonReason = abandonReason.substring(0, abandonReason.indexOf('-')); 
	           	}
	           	String abandonDetails=rs.getCell(j++, i).getContents();//放弃原因描述
	           	String retrialStatus=rs.getCell(j++, i).getContents();//复试状态
	           	if(retrialStatus!=null&&retrialStatus!=""&&retrialStatus.contains("-")){
	           		//截取retrialStatus编码“-”前面的编码值
	           		retrialStatus = retrialStatus.substring(0, retrialStatus.indexOf('-')); 
	           	}
	           	
	           	String graduationDate=rs.getCell(j++, i).getContents();//毕业时间
	           	Date BirthDate = new Date();
	          //日期字符串转时间格式日期
	           	if(graduationDate!=null && graduationDate !=""){
	           		BirthDate = new SimpleDateFormat("yyyy-MM-dd").parse(graduationDate);////毕业时间 
	           	}
		           	map.put("unacceptableReasonId", unacceptableReasonId);
		           	map.put("retrialEvaluation", retrialEvaluation);
		           	map.put("processId", processId);
		           	map.put("offerFollowup", offerFollowup);
		           	map.put("offerStatus", offerStatus);
		           	map.put("abandonReason", abandonReason);
		           	map.put("abandonDetails", abandonDetails);
		           	map.put("retrialStatus", retrialStatus);
		           	map.put("graduationDate", BirthDate);
		           	map.put("recruitmentCampus", recruitmentCampus);
	           	
	               map.put("campusPosition", campusPosition);
	               map.put("flunkedDetials", flunkedDetials);
	               map.put("groupInterviewe", groupInterviewe);
	               map.put("groupScore", groupScore);
	               map.put("groupEvaluation", groupEvaluation);
	               map.put("faceToFaceInterviewer", faceToFaceInterviewer);
	               map.put("faceToFaceInterview", faceToFaceInterview);
	               map.put("faceToFaceEvaluation", faceToFaceEvaluation);
	               map.put("appliedPosition1", appliedPosition1);
	               map.put("appliedPosition2", appliedPosition2);
	               
	               map.put("recommendedPost", recommendedPost);
	               map.put("retrialerFollowup", retrialerFollowup);
	               map.put("retrialInterviewer", retrialInterviewer);
	               map.put("candidateName", candidateName);
	               map.put("candidatePhone", candidatePhone);
	               map.put("candidateMail", candidateMail);
	               map.put("exceptedWorkAreaA", exceptedWorkAreaA);
	               map.put("exceptedWorkAreaB", exceptedWorkAreaB);
	               map.put("degree", degree);
	               map.put("college", college);
	               map.put("major", major);
	               
	               map.put("gender", gender);
	               map.put("birthplace", birthplace);
	               map.put("address", address);
	               map.put("englishLevel", englishLevel);
	               
	               list.add(map);
	        	}
	           }
	       }
	   } catch (Exception e) {
	       // TODO Auto-generated catch block
	       e.printStackTrace();
	   } finally {
	     	 File downloadFile=new File(file);
	    	   downloadFile.delete();
	       }
	   return list;
	}
	 /**
	  * 人才Excel数据导入
     * 查询指定目录中电子表格中所有的数据
     * @param file 文件完整路径
     * @param sheetFile Excel工作簿名称
     * @return
     */
@Bizlet("人才库Excel导入")
 public static List getAllByExcelTalents(String file){
	 	List list=new ArrayList();
        try {
        	WorkbookSettings ws = new WorkbookSettings();
        	ws.setCellValidationDisabled(true);
        	//判断是否为数字类型值
        	Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
            Workbook rwb=Workbook.getWorkbook(new File(file),ws);
            if (rwb.getNumberOfSheets() == 0)
            {
                System.out.println("No sheet!");
            }
            Sheet rs=rwb.getSheet(0);//或者rwb.getSheet(0)
            int clos=rs.getColumns();//得到所有的列
            int rows=rs.getRows();//得到所有的行
            
            System.out.println(clos+" rows:"+rows);
            for (int i = 1; i < rows; i++) {
                for (int j = 0; j < 1; j++) {
                	Map map = new HashMap();
                	//获取每行所有数据
                    //第一个是列数，第二个是行数
                	//默认最左边编号也算一列 所以这里得j++
                    String recruitChannels = rs.getCell(j++, i).getContents();//渠道
                    if(recruitChannels!=null&&recruitChannels!=""&&recruitChannels.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	recruitChannels = recruitChannels.substring(0, recruitChannels.indexOf('-')); 
                      }else if(recruitChannels!=null&&recruitChannels!=""){
                     	 DataObject [] data = com.eos.foundation.eoscommon.BusinessDictUtil.getCurrentDictInfoByType("recruit_channel");
                    	 
                    	 for (DataObject dataObject : data) {
    						if(dataObject.getString("dictName").equals(recruitChannels)||recruitChannels==dataObject.getString("dictName")){
    							recruitChannels = dataObject.getString("dictID");
    							break;
    						}
    					}
                     }
                    String recruitSuppliers = rs.getCell(j++, i).getContents();//供应商
                    if(recruitSuppliers!=null&&recruitSuppliers!=""&&recruitSuppliers.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	recruitSuppliers = recruitSuppliers.substring(0, recruitSuppliers.indexOf('-')); 
                      }
                    String recruiterName = rs.getCell(j++, i).getContents();//招聘负责人姓名
                    String recruiterId = rs.getCell(j++, i).getContents();//招聘负责人工号
                    String candidateName=rs.getCell(j++, i).getContents();//人才姓名
                    String candidatePhone=rs.getCell(j++, i).getContents();//电话
                    //人才姓名和电话不能为空
                    if((candidateName!=null||candidatePhone!=null)&&(candidateName!=""||candidatePhone!="")){
                    String candidateMail=rs.getCell(j++, i).getContents();//邮箱
                    String expectedPosition=rs.getCell(j++, i).getContents();//意向职位
                    String positionCatagory=rs.getCell(j++, i).getContents();//意向职位类别
                    
                    if(positionCatagory!=null&&positionCatagory!=""&&positionCatagory.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	positionCatagory = positionCatagory.substring(0, positionCatagory.indexOf('-')); 
                      }
                    String expectedSalary=rs.getCell(j++, i).getContents();//期望薪资类型代码
                    if(expectedSalary!=null&&expectedSalary!=""&&expectedSalary.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	expectedSalary = expectedSalary.substring(0, expectedSalary.indexOf('-')); 
                      }
                    String exceptedWorkAreaA=rs.getCell(j++, i).getContents();//工作意向地1（省市区）
                    String exceptedWorkAreaB=rs.getCell(j++, i).getContents();//工作意向地2（省市区）
                    String degree=rs.getCell(j++, i).getContents();//学历编码
                    if(degree!=null&&degree!=""&&degree.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	degree = degree.substring(0, degree.indexOf('-')); 
                      }
                    String date=rs.getCell(j++, i).getContents();//出生日期
                    //日期字符串转时间格式日期
                    if(date!=null && date !=""){
                    	Date birthDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);//出生日期转日期格式 
                    }
                    String gender=rs.getCell(j++, i).getContents();//性别
                    if(gender!=null&&gender!=""&&gender.contains("-")){
                       	//截取positionCatagory编码“-”前面的编码值
                    	gender = gender.substring(0, gender.indexOf('-')); 
                      }
                    String address=rs.getCell(j++, i).getContents();//人才现居地（省市区
                    
                    //将获取的Excel每行数据存入Map中
                    map.put("recruiterName", recruiterName);//招聘负责人姓名
                    map.put("recruiterId", recruiterId);//招聘负责人工号
                    map.put("recruitChannels", recruitChannels);//渠道
                    map.put("recruitSuppliers", recruitSuppliers);//供应商
                    map.put("candidateName", candidateName);
                    map.put("candidatePhone", candidatePhone);
                    map.put("candidateMail", candidateMail);
                    map.put("expectedPosition", expectedPosition);
                    map.put("positionCatagory", positionCatagory);
                    map.put("expectedSalary", expectedSalary);
                    map.put("exceptedWorkAreaA", exceptedWorkAreaA);
                    map.put("exceptedWorkAreaB", exceptedWorkAreaB);
                    map.put("degree", degree);
                    map.put("date", date);
                    map.put("gender", gender);
                    map.put("address", address);
                    
                    list.add(map);
                    }
                    
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  finally {
     	  File downloadFile=new File(file);
    	   downloadFile.delete();
       }
        return list;
 }
/**
 * 批量导入中，数据已存在或者添加失败的数据整理
* @param map 已存在的候选人信息
* @param List 已存在的候选人信息集合
* @param stutas 保存失败原因状态
* @return
*/
@Bizlet("人才批导失败数据返回值")
public static List getAllList(Map map,List List,String stutas){
	List list=new ArrayList();
	//List集合不为空，需要添加传入List集合
	if(List!=null){
		list.addAll(List);
	}
	Map gather = new HashMap();
	gather.put("candidateName", map.get("candidateName"));
	gather.put("candidatePhone", map.get("candidatePhone"));
	if(map!=null){
		//判断传入人才批导放回状态，并写入map中 -1保存失败、-2人才已存在、1简历更新成功
		if(stutas=="-1"||"-1".equals(stutas)){
			gather.put("stutas", "人才信息保存失败");
		}else if(stutas=="-2"||"-2".equals(stutas)){
			gather.put("stutas", "人才信息已经存在");
		}else if(stutas=="1"||"1".equals(stutas)){
			gather.put("stutas", "简历已存在，更新成功");
		}
		list.add(gather);
	}
	return list;
}
/**
 * 批量导入中，数据已存在或者添加失败的数据整理
* @param map 已存在的候选人信息
* @param List 已存在的候选人信息集合
* @param stutas 保存失败原因状态
* @return
*/
@Bizlet("校招人才批导失败数据返回值")
public static List getAllListSchool(Map map,List List,String stutas){
	List list=new ArrayList();
	//List集合不为空，需要添加传入List集合
	if(List!=null){
		list.addAll(List);
	}
	Map gather = new HashMap();
	gather.put("candidateName", map.get("candidateName"));
	gather.put("candidatePhone", map.get("candidatePhone"));
	if(map!=null){
		//判断传入人才批导放回状态，并写入map中 -1保存失败、-2人才已存在、1简历更新成功
		if(stutas=="-1"||"-1".equals(stutas)){
			gather.put("stutas", "校招人才信息保存失败");
		}else if(stutas=="-2"||"-2".equals(stutas)){
			gather.put("stutas", "校招人才信息已经存在");
		}else if(stutas=="1"||"1".equals(stutas)){
			gather.put("stutas", "简历已存在，更新成功");
		}
		list.add(gather);
	}
	
	return list;
}
public static void main(String[] args) {
	getAllByExcelSchool("F:/Excel导入测试文件/校招需要批导字段.xls");
}
}
