<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%@ page import="org.apache.commons.fileupload.FileItem"%>
<%@ page import="org.apache.commons.fileupload.FileUploadException"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.codehaus.jettison.json.JSONObject"%>
<%
	 //上传保存路径
	 String configPath = "resume/";
	 String dirTemp = "resume/temp/";
	 request.setCharacterEncoding("UTF-8");
	 response.setContentType("text/html;charset=UTF-8"); 
	//文件保存路径
	String rootpath = request.getSession().getServletContext().getRealPath("");
	String savePath = rootpath+"/"+configPath;
	
	//通过业务字典获取IP
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	rooturl = rooturl+=configPath;
	
	//临时文件目录
	String tempPath = request.getContextPath()+"/"+ dirTemp;
	
	File f1 = new File(savePath);
	
	//如果文件不存在,就新建一个
	if (!f1.exists()) {
		f1.mkdirs();
	}
	// 创建临时文件夹  
    File dirTempFile = new File(tempPath);
    if (!dirTempFile.exists()){
        dirTempFile.mkdirs();  
    }  

     DiskFileItemFactory factory = new DiskFileItemFactory();
     factory.setSizeThreshold(1024 * 1024 * 20); // 设定使用内存超过20M时，将产生临时文件并存储于临时目录中。
     factory.setRepository(new File(tempPath)); // 设定存储临时文件的目录。  
	 ServletFileUpload upload = new ServletFileUpload(factory);
	 upload.setHeaderEncoding("utf-8");
	 
	List<FileItem> formItems = upload.parseRequest(request);
	if (formItems != null && formItems.size() > 0) {
	
	 			// 迭代表单数据
                for (FileItem item : formItems) {
                    // 处理不在表单中的字段
                    if (!item.isFormField()) { 
                    	//获取时间戳
                    	long timeMillis = System.currentTimeMillis();
                    	//获取文件名
                        String fileName = new File(item.getName()).getName();
                        
                        //获取文件名去除后缀
                        String subFile = fileName.substring(0,fileName.indexOf("."));
          
                        //获取文件后缀名
                        String fileExt = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
                        //拼接保存新的文件名
                        String newFileName=subFile+timeMillis+fileExt;
                       	//访问地址
                        rooturl =rooturl+=newFileName;
                      	
                        String filePath = savePath + newFileName;
                       	try{
	                        File storeFile = new File(filePath);
	                        OutputStream os = new FileOutputStream(storeFile);
	                        InputStream is = item.getInputStream();
	                        byte buf[] = new byte[1024];// 可以修改 1024 以提高读取速度
	                        int length = 0;
	                        while ((length = is.read(buf)) > 0)  
	                        {  
	                            os.write(buf, 0, length);  
	                        } 
	                         // 关闭流  
	                        os.flush();  
	                        os.close();  
	                        is.close();
	                         
	                     
	                     	System.out.print("文件保存路径："+filePath+"\n");
	                     	System.out.print("访问地址："+rooturl);
	                     	//保存文件
	                        item.write(storeFile);
	                        filePath = filePath.replaceAll("\\\\","/");
	                        String end = "{\"code\": 0,\"configPath\": \""+configPath+"\",\"msg\": \"成功\",\"data\": {\"src\": \"" + rooturl + "\",\"filePath\": \"" + filePath + "\",\"fileType\":\""+fileExt+"\",\"fileName\":\""+subFile+"\"}}"; 
	                        JSONObject jsonObject = new JSONObject();  
	   						jsonObject.put("src", rooturl); 
	   						jsonObject.put("code", 0); 
	   						jsonObject.put("configPath", configPath);
	   						
	                        response.getWriter().print(end);
                        
                        }catch(Exception e){
                        	e.printStackTrace();
                        }
                     } 
               }

	}
	
	
 %>