<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.UserObject"%>
<%@page import="com.eos.foundation.eoscommon.ResourcesMessageUtil"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="commonj.sdo.*,java.util.*,java.text.SimpleDateFormat"%>
<%@page import="com.eos.web.taglib.util.XpathUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wenjin
  - Date: 2018-03-12 10:20:37
  - Description:
-->
<head>
<title>内推任务详情</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link
	href="<%= request.getContextPath() %>/interpolation/css/recommend_details.css"
	rel="stylesheet" />
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
<style type="text/css">
	#return,#clean,#success{
		width: 75px;
	}
</style>
</head>
<%
	//session获取内推任务ID
	String id=request.getParameter("id");
	//session获取内推任务read
	String read=request.getParameter("read");
	//session获取内推任务页码数
	String curr=request.getParameter("curr");
	//session获取内推任务的筛选条件
	String storageJson = request.getParameter("storageJson");
	//获取当前招聘负责人姓名和ID
	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	
	
%>
<body>
	<div id="main">
		<div id="main-left">
			<form id="form">
			<fieldset class="layui-elem-field layui-field-title">
			  	<% if(id == "" || id == null ) { %>
				<legend>添加内推任务</legend>
				<% } else { %>
				<legend>编辑内推任务</legend>
				<% } %>
			  	
			</fieldset>
				<div class="form-group">
					<div id="content">
						<h4>推荐人信息</h4>
						<div class="col-lg-6">
							<label>推荐人姓名<samp>*</samp></label>
							<input class="layui-input getStaff"  id="recommendname" name="recommendname" data="" onblur="validator()" value="" type="text" style="height: 34px;border-color: #cccccc;width: 91%;">
							<i class="layui-icon personnel" id="recommendstaff" onclick="getEntryStaff2(this)" style="border: 1px solid #ccc;border-radius: 4px;background: #f0ad4e;font-size: 29px;margin-top: -10%;float: right;"></i>
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="id"
								name="id" style="display:none;">
						</div>
						<div class="col-lg-6">
							<label>推荐人手机号码<samp>*</samp></label> <input type="text"
								class="form-control phone" id="recommendphone" name="recommendphone" disabled=""
								 onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>推荐人和被推荐人关系</label> <input type="text"
								class="form-control" id="recommendrelationship" name="recommendrelationship" disabled=""
								>
						</div>
						<div class="col-lg-6">
							<label>推荐来源类型</label>
							<select class="form-control"
								id="recommendresource" name="recommendresource" onblur="validator()" disabled="">
								<option></option>
							</select>
						</div>
					</div>

					<!-- 被推荐人信息 -->
					<div id="content_top">
						<h4>被推荐人信息</h4>
						<div class="col-lg-6">
							<label>被推荐人姓名<samp>*</samp></label> <input type="text"
								class="form-control" id="interpolatename" name="interpolatename" disabled="" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>被推荐人手机号码<samp>*</samp></label> <input type="text"
								class="form-control phone2" id="interpolatephone"
								name="interpolatephone" disabled="" onblur="validator()">
						</div>
						<div class="col-lg-6">
							<label>被推荐人邮箱</label> <input type="text" class="form-control"
								id="interpolatemail" name="interpolatemail" disabled="">
						</div>
						<div class="col-lg-6">
							<label>所属公司<samp>*</samp></label>
								<select class="form-control"
								id="company" name="company" onblur="validator()" >
								<option value="0"></option>
							</select>
						</div>
					</div>
					<div id="content_middle">
						<div class="col-lg-6 Invisible">
							<label>应聘部门</label>
							<input class="layui-input" id="orgcentre_dept" name="orgcentre_dept" readonly="readonly" onblur="validator()" onmouseover="selbox($(this))" disabled="" onclick="getOrg(this)" data="" alias="" l1="" l2="" l3="" l4="" l1name="" l2name="" l3name="" l4name="" placeholder="部门选择" value="" style="height: 34px;margin-bottom: -20px;border-color: #cccccc;">
						</div>
						<div class="col-lg-6 parentCls">
							<label>职位类别<samp>*</samp></label>
							<select class="form-control"
								id="positioncategory" name="positioncategory" onblur="validator()" disabled="">
								<option value="0"></option>
							</select>
						</div>
						<div class="col-lg-6 parentCls">
							<label>推荐职位<samp>*</samp></label> <input type="text"
								class="form-control inputElem" id="recommendposition"
								name="recommendposition" onblur="validator()" disabled="">
						</div>
						<div class="col-lg-6 parentCls">
							<label>内推奖励  / 元</label> <input type="text"
								class="form-control inputElem" id="interpolatebonus"
								name="interpolatebonus" disabled="">
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfo"
								name="upInfo" style="display:none;" >
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfoname"
								name="upInfoname" style="display:none;">
						</div>
						<div class="col-lg-6 parentCls">
							<input type="text"
								class="form-control inputElem" id="upInfopath"
								name="upInfopath" style="display:none;">
						</div>
					</div>
					<!-- 处理情况 -->
					<div id="content_bottom">
						<h4>处理情况</h4>
						
						
						<div class="col-lg-6">
							<label>建议招聘负责人<samp>*</samp></label>
							<input class="layui-input getStaff"  id="suggestname" name="suggestname"  readonly="readonly" onmouseover="selbox($(this))" onblur="validator()" onclick="getEntryStaff(this)" data="" disabled=""  placeholder="人员选择" value="" type="text" style="border-color: #cccccc;">
						</div>
						
						
						<div class="col-lg-6">
							<label>该简历是否合适</label> <select class="form-control"
								id="issuitable" name="issuitable" disabled="">
								<option value="0"></option>
								<option value="1">是</option>
								<option value="2">否</option>
							</select>
						</div>
					</div>
					<div id="content_textarea">
						<div>
							<label>电话沟通情况</label>
							<textarea class="form-control" rows="5" id="communicationcontent"
								name="communicationcontent" disabled=""></textarea>
						</div>
					</div>
				</div>
			</form>
		</div>

		<div id="main-right">
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="success" 
					name="success" disabled="" onclick="isEnclosureExist()">保 存</button>
				<button type="button" class="btn btn-warning" id="updata"
					name="updata" onclick="updata()" style="display:none;width: 75px;">保 存</button>
				<button type="button" class="btn btn-default" id="clean"
					name="clean" onclick="clean()">重 置</button>
				<button type="button" class="btn btn-default" id="return"
					name="return" onclick="clikreturn()">返 回</button>
			</div>
			<div id="content_file" style="float: left;width: 99%;margin-left: 5%;display:none;">
				<label>附件:</label>
				<div id="file_upload"></div>
			</div>
			<div id="content_upload">
				<div id="uploader" class="wu-example">
			        <div class="queueList">
			            <div id="dndArea" class="placeholder">
			                <div id="filePicker"></div>
			                <p>点击文件上传或将文件拖到这里</p>
			                <p style="font-size: 14px;">支持：word、excel、html、ptf、jpg等格式</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div id="content_leftall">
				<div id="hr">
					<hr>
				</div>
				<div id="content_centre">
					<div class="col-lg-11">
							<label>添加到我的工单</label> <select class="form-control"
							id="positions" name="positions" disabled="">
								<option value="0">职位</option>
							</select> <select class="form-control"
							id="supplier" name="supplier" disabled="">
								<option disabled selected value="0">工单号</option>
	
							</select>
					</div>
					<div id="content_centre_button">
						<button type="button" class="btn btn-warning" disabled="" id="set" name="set"
						onclick="setsubmit()">添 加</button>
					</div>
				</div>
				
				<div id="hr">
					<hr>
				</div>
				<div id="content_label">
				
					<table class="table table-striped" style="width: 91%;">
					<thead>
						<tr>
							<th>已在工单列表</th>
							<th>招聘负责人</th>
							<th>候选人状态</th>
						</tr>
					</thead>
					<tbody id="tbody">

					</tbody>
				</table>
					<!-- <ul id="ordid" style="width: 49%;float: left;">
						<li style="border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;">已在工单列表</li>
					</ul>
					<ul id="recruiter" style="width: 30%;float: left;">
						<li style="border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;">招聘负责人</li>
					</ul>
					<ul id="recruitestuta" style="width: 20%;float: left;">
						<li style="border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;">招聘状态</li>
					</ul> -->
				</div>
				<button type="button" class="btn btn-warning" disabled="" id="over" name="over"
					onclick="oversubmit()" style="width: 84%; margin-left: 19px;">结束任务</button>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		 //绑定文本框
		 $(function(){
		    $('#recommendname').bind('input propertychange', function(){
		        var recommendN = $('#recommendname').val();
		        if(recommendN == "" || recommendN == null){
		        	$("#recommendname").attr("data","");
		        }
		    });   
		 });   
		 var isid=<%=id %>;
         if(isid == null || isid == ""){
         	addrecommend();
         }else{
         	updatacommend();
         }
          //查询业务字典
         dictionarycombox();
         //初始化所属公司
         selectCompany();
		 //初始页面加载数据，queryorders()初始化当前招聘负责人招聘职位
         queryorders();
         //初始化表单数据
         initlazation();
         //初始化内推任务所在工单
         selectorder();
         //保存已经查询的所在工单数据集
         var inorder;
         //判断是否新增页面
         
         function addrecommend(){
			$("#content_leftall").hide();
			$("#recommendname").attr("disabled",false);
			$("#recommendphone").attr("disabled",false);
			$("#interpolatename").attr("disabled",false);
			$("#interpolatephone").attr("disabled",false);
// 			$("#company").attr("disabled",false);
			$("#recommendrelationship").attr("disabled",false);
			$("#interpolatemail").attr("disabled",false);
			$("#positioncategory").attr("disabled",false);
			$("#recommendposition").attr("disabled",false);
			$("#interpolatebonus").attr("disabled",false);
			$("#issuitable").attr("disabled",false);
			$("#communicationcontent").attr("disabled",false);
			$("#orgcentre_dept").attr("disabled",false);
			$("#suggestname").attr("disabled",false);
			$("#recommendresource").attr("disabled",false);
		 }
		 function updatacommend(){
			$("#content_leftall").show();
			$("#recommendname").attr("disabled",true);
			$("#recommendname").css("background-color","#eee");
			$("#recommendphone").attr("disabled",true);
			$("#interpolatename").attr("disabled",true);
			$("#interpolatephone").attr("disabled",true);
			$("#recommendstaff").css("display","none");
			$("#recommendname").css("width","100%");
			$("#recommendrelationship").attr("disabled",false);
			$("#interpolatemail").attr("disabled",false);
			$("#positioncategory").attr("disabled",false);
			$("#recommendposition").attr("disabled",false);
			$("#interpolatebonus").attr("disabled",false);
			$("#issuitable").attr("disabled",false);
			$("#communicationcontent").attr("disabled",false);
			$("#orgcentre_dept").attr("disabled",false);
			$("#suggestname").attr("disabled",false);
			$("#recommendresource").attr("disabled",false);
			$("#company").attr("disabled",false);
			
		 }
		
         //初始化控件
         layui.use(['layer'], function(){
     	 });
     	 
     	 function updata(){
     	 	var id =$("#id").val();
     	 	isEnclosureExist(id);
     	 
     	 }
     	 
     	 
     	  var isPass=true;//标识是否通过验证
		   var isPassMsg="";//非空验证提示输出
		   var arr=[];
	   	//失去焦点验证
	   	function validator(){
	   		isPassMsg="";
	   		isPass=true;
			arr=[];
	   		//推荐人姓名
	  		 	if($('#recommendname').val()==null || $('#recommendname').val()=="" ){
	           	$("#recommendname").css("border","1px solid red");
	           	isPassMsg+="请选择推荐人,";
	           	isPass=false;
	           }else{
	           	$("#recommendname").css("border","1px solid #ccc");
	           }
			   
	   		//推荐人手机号码
				var phone=/^1\d{10}$/;//电话正则
	  		 	if($('.phone').val()==null || $('.phone').val()=="" || !phone.test($(".phone").val())){
	           	$(".phone").css("border","1px solid red");
	           	isPassMsg+="请输入正确的推荐人手机号,";
	           	isPass=false;
	           }else{
	           	$(".phone").css("border","1px solid #ccc");
	           }
			   
			   //被推荐人姓名
	  		 	if($('#interpolatename').val()==null || $('#interpolatename').val()=="" ){
	           	$("#interpolatename").css("border","1px solid red");
	           	isPassMsg+="请输入被推荐人姓名,";
	           	isPass=false;
	           }else{
	           	$("#interpolatename").css("border","1px solid #ccc");
	           }
			   
			   //被推荐人手机号码
	  		 	if($('.phone2').val()==null || $('.phone2').val()=="" || !phone.test($(".phone2").val())){
	           	$(".phone2").css("border","1px solid red");
	           	isPassMsg+="请输入正确的被推荐人手机号,";
	           	isPass=false;
	           }else{
	           	$(".phone2").css("border","1px solid #ccc");
	           }
			   
			   //所属公司
	  		 	if($('#company').val()==null || $('#company').val()=="" || $('#company').val()==0){
	           	$("#company").css("border","1px solid red");
	           	isPassMsg+="请选择所属公司,";
	           	isPass=false;
	           }else{
	           	$("#company").css("border","1px solid #ccc");
	           }
			   //职位类别
	  		 	if($('#positioncategory').val()==null || $('#positioncategory').val()=="" || $('#positioncategory').val()==0){
	           	$("#positioncategory").css("border","1px solid red");
	           	isPassMsg+="请选择职位类别,";
	           	isPass=false;
	           }else{
	           	$("#positioncategory").css("border","1px solid #ccc");
	           }
			   
			   //推荐职位
	  		 	if($('#recommendposition').val()==null || $('#recommendposition').val()==""){
	           	$("#recommendposition").css("border","1px solid red");
	           	isPassMsg+="请输入推荐职位,";
	           	isPass=false;
	           }else{
	           	$("#recommendposition").css("border","1px solid #ccc");
	           }
			   
			   //建议招聘负责人
	  		 	if($('#suggestname').val()==null || $('#suggestname').val()==""){
	           	$("#suggestname").css("border","1px solid red");
	           	isPassMsg+="请选择建议招聘负责人";
	           	isPass=false;
	           }else{
	           	$("#suggestname").css("border","1px solid #ccc");
	           }
			  arr=isPassMsg.split(",");
	   	} 
     	 
     	 
          //查询当前招聘负责人工单
		 function queryorders(){
			 var json={
			  	id:<%=empid %>
			 };
			 $.ajax({
				 url: "com.recruit.interpolation.interpolationindex.selectPosition.biz.ext",
				 type:'POST',
				 data:json,
				 success:function(data){
				 	//职位下拉框赋值
					for(var i =0;i<data.recommend.length;i++ ){
						$("#positions").append("<option>"+data.recommend[i].position+"</option>");
					}
				}
					
			});
	 	}
		 	
        //初始数据
        function initlazation(){
		 	  var id = <%=id %>
		 	  //判断是否有内推主键ID,如果有就初始化，否则输入框为空
		 	  if(id == null || id == ""){
		 	  	  $("#success").attr("disabled",false);
		 	  	  return;
		 	  }else{
				  var json={
				  	id:id
				  };
				  $.ajax({
					 url: "com.recruit.interpolation.interpolationindex.selectInteroilationSend.biz.ext",
					 type:'POST',
					 data:json,
					 success:function(data){
					 	 //往输入框赋值
					 	 $("#recommendname").val(data.recommend[0].recommenderName);
						 $("#recommendname").attr("data",data.recommend[0].recommenderId);
						 $("#recommendphone").val(data.recommend[0].recommenderPhone);
						 $("#interpolatename").val(data.recommend[0].candicateName);
						 $("#interpolatephone").val(data.recommend[0].candicatePhone);
						 $("#interpolatemail").val(data.recommend[0].candicateMail);
						 $("#positioncategory").val(data.recommend[0].positionCategory);
						 $("#recommendresource").val(data.recommend[0].recommendresource);
						 $("#orgcentre_dept").val(data.recommend[0].orgcentreDept);
						 $("#orgcentre_dept").attr("alias",data.recommend[0].orgcentreDeptid);
						 $("#recommendposition").val(data.recommend[0].recommendedPosition);
						 $("#interpolatebonus").val(data.recommend[0].recommendedRewards);
						 $("#suggestname").val(data.recommend[0].suggestedRecruiterName);
						 $("#suggestname").attr("data",data.recommend[0].suggestedRecruiterId);
						 $("#communicationcontent").val(data.recommend[0].communicationContent);
						 $("#issuitable").val(data.recommend[0].isSuitable);
						 $("#recommendrelationship").val(data.recommend[0].recommendRelationship);
						 $("#company").val(data.recommend[0].company);//所属公司
						 updatastate(data.recommendstate,data.recommend[0].suggestedRecruiterId);
						 var resumetrr = updatastate(data.recommendstate,data.recommend[0].suggestedRecruiterId);
						 //初始附件数据
						 resumeaccessory(data.recommendstate,resumetrr);
						 isaddorupdata();
						}
					});
				}
		}
		
		function updatastate(statse,suggesteid){
			var empid = <%=empid %>;
			var returnstauta = "";
			if(statse == 2){
				$("#success").css("background-color", "#999");
				$("#over").css("background-color", "#999");
				$("#success").attr("disabled",true);
				$("#set").attr("disabled",false);
				$("#over").attr("disabled",true);
				$("#content_upload").css("display","none");
				$("#positions").attr("disabled",false);
				$("#supplier").attr("disabled",false);
				$("#recommendrelationship").attr("disabled",true);
				$("#interpolatemail").attr("disabled",true);
				$("#positioncategory").attr("disabled",true);
				$("#recommendposition").attr("disabled",true);
				$("#interpolatebonus").attr("disabled",true);
				$("#issuitable").attr("disabled",true);
				$("#communicationcontent").attr("disabled",true);
				$("#orgcentre_dept").attr("disabled",true);
				$("#suggestname").attr("disabled",true);
				$("#recommendresource").attr("disabled",true);
				$("#company").attr("disabled",true);
				$("#orgcentre_dept").css("background-color","#eee");
				$("#suggestname").css("background-color","#eee");
			}else{
				if(empid == suggesteid){
					$("#success").attr("disabled",false);
					$("#set").attr("disabled",false);
					$("#over").attr("disabled",false);
					$("#positions").attr("disabled",false);
					$("#supplier").attr("disabled",false);
				}else{
					$("#success").attr("disabled",true);
					$("#set").attr("disabled",false);
					$("#over").attr("disabled",true);
					$("#positions").attr("disabled",false);
					$("#supplier").attr("disabled",false);
					$("#content_upload").css("display","none");
					$("#recommendrelationship").attr("disabled",true);
					$("#interpolatemail").attr("disabled",true);
					$("#positioncategory").attr("disabled",true);
					$("#recommendposition").attr("disabled",true);
					$("#interpolatebonus").attr("disabled",true);
					$("#issuitable").attr("disabled",true);
					$("#communicationcontent").attr("disabled",true);
					$("#orgcentre_dept").attr("disabled",true);
					$("#suggestname").attr("disabled",true);
					$("#orgcentre_dept").css("background-color","#eee");
					$("#suggestname").css("background-color","#eee");
					$("#recommendresource").attr("disabled",true);
					$("#company").attr("disabled",true); //禁止所属公司控件
					returnstauta = 1;
				}
			}
			return returnstauta;
		}
		
	    //二级联动
		$("#positions").change(function(){
		    $("#supplier").html("");
		    //获取当前选中的职位
		    var position =$(this).val();
		    if(position == null || position == ""){
		    	$("#supplier").html("");
		    	return;
			}else{
			    json={
			        id:<%=empid %>,
		    		position:position
		    	};
	    		$.ajax({
					url: "com.recruit.interpolation.interpolationindex.selectOrder.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
					//往下拉框单号赋值
					for(var i =0;i<data.recommend.length;i++ ){
						$("#supplier").append("<option>"+data.recommend[i].recuitOrderId+"</option>");
						}
					}
				});
			}
		 });
	    
	    //查询简历地址
    	function resumeaccessory(state,resumetrr){
    		  //debugger;
    		  var flag ="2";
    		  json={
			        resumeid:<%=id %>,
		    		flag:flag
		    	};
	    		$.ajax({
					url: "com.recruit.interpolation.interpolationindex.selectResume.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
						if(data.returnAcc.length == 0){
						}else{
							$("#content_file").css("display","inline");
							for(var i =0;i<data.returnAcc.length;i++ ){
								if(state == 2 || resumetrr == 1){
									$("#file_upload").append("<p class='recruitAcc'><a href="+data.returnAcc[i].accessoryUrl+" target='_blank' >"+data.returnAcc[i].accessoryName+"</a>&nbsp;&nbsp;&nbsp;&nbsp;</p>");
								}else{
									$("#file_upload").append("<p class='recruitAcc'><a href="+data.returnAcc[i].accessoryUrl+" target='_blank' >"+data.returnAcc[i].accessoryName+"</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='#'  class='delete' onclick='resumnedelete("+data.returnAcc[i].id+",\""+data.returnAcc[i].accessoryPath+"\")'> X </a></p>");
								}
								
							}
						}
					}
				});
    	
    	} 
    	
    	function isaddorupdata(){
    		var id = <%=id %>;
    		if(id == "" || id == null){
    			 $("#success").css("display","");
    		}else{
    			 $("#success").css("display","");
    			 $("#updata").css("display","none");
    			 $("#clean").css("display","none");
    		}
    	
    	}
    	//返回
    	function clikreturn(){
    		 var read =<%=read %>;
    		 var curr =<%=curr %>;
    		 var storageJson = <%=storageJson %>;
    		 //json对象转json字符串
    		 storageJson = JSON.stringify(storageJson);
    		 var url="/interpolation/recommend_index.jsp?read="+read+"&storageJson="+storageJson+"&curr="+curr;
    		 parent.goToUrl(url);
    	}
    	
    	//提交事件
	    function submit(id){
	    	validator();
    		//非空判断
    		if(isPass==false){
    			layer.msg(arr[0]);
    			return;
    		}
			       
		        //获取输入框的值
		        var recommendid = "";
		        if(id == "" || id == null ){
		        	recommendid = <%=id %>;
		        }else{
		        	recommendid = id;	
		        }
		        
		        var recommendname =$("#recommendname").val();
	    		var recommendnameid =$("#recommendname").attr("data");
	    		var recommendphone =$("#recommendphone").val();
	    		var positioncategory=$("#positioncategory").val();
	    		var recommendresource=$("#recommendresource").val();
	    		var channel =$("#channel").val();
	    		var interpolatename =$("#interpolatename").val();
	    		var interpolatephone =$("#interpolatephone").val();
	    		var interpolatemail =$("#interpolatemail").val();
	    		var orgcentre_dept =$("#orgcentre_dept").val();
	    		var orgcentre_deptid = $("#orgcentre_dept").attr("alias");
	    		var recommendposition =$("#recommendposition").val();
	    		var interpolatebonus =$("#interpolatebonus").val();
	    		var suggestname =$("#suggestname").val();
	    		var suggestedrecruiterId =$("#suggestname").attr("data");
	    		var communicationcontent =$("#communicationcontent").val();
		    	var issuitable = $("#issuitable").val();
		    	var filename = $("#upInfoname").val();
		    	var fileurl = $("#upInfo").val();
		    	var filepath = $("#upInfopath").val();
		    	var recommendrelationship =$("#recommendrelationship").val();
		    	var company =$("#company").val();
		    	
	    		var json={
	    			//传值
	    			recommendid:recommendid,
					recommendname:recommendname,	
					recommendnameid:recommendnameid,
					recommendphone:recommendphone,
					channel:channel,
					interpolatename:interpolatename,
					interpolatephone:interpolatephone,
					interpolatemail:interpolatemail,
					orgcentre_dept:orgcentre_dept,
					orgcentre_deptid:orgcentre_deptid,
					recommendposition:recommendposition,
					positioncategory:positioncategory,
					interpolatebonus:interpolatebonus,
					suggestname:suggestname,
					communicationcontent:communicationcontent,
					issuitable:issuitable,
					suggestedrecruiterId:suggestedrecruiterId,
					Filename:filename,
					Fileurl:fileurl,
					Filepath:filepath,
					recommendrelationship:recommendrelationship,
					recommendresource:recommendresource,
					company:company
					
				};
				$.ajax({
					url: "com.recruit.interpolation.interpolationindex.updataInteroilationSocend.biz.ext",
					type:'POST',
					data:json,
					success:function(data){
						layer.msg('保存成功');
						isupdata(data.recommendid);
					}
				});
    	}
    	
    	function isupdata(reid){
    		$("#id").val(reid);
    		if(reid == "" || reid == null){
    			
    		}else{
    			 $("#success").css("display","none");
    			 $("#updata").css("display","");
    			 
    		}
    	
    	}
    	//添加到工单
    	function setsubmit(){
    		//debugger;
    		var id = <%=id %>;
    		//判断是否获取到ID
		 	if(id == null || id == ""){	
		 		layer.msg('未找到当前招聘任务，请先保存');
		 	  	return;
		 	}else{
	    		var obj = $("#supplier option:selected");
				//获取当前选中工单号
				var  orderid  = obj.val();
				if(orderid == 0){
					//提示信息
					layer.open({
						        type: 1
						        ,offset:'auto' 
						        ,content: '<div style="padding: 20px 75px 0px;">未选择工单？</div>'
						        ,closeBtn: false//不显示关闭图标
						        ,btn:['确定']
						        ,btnAlign: 'c' //按钮居中
						        ,shade: 0 //不显示遮罩
						        ,yes: function(index){
						        	layer.close(index);//关闭页面
						        }
						       
						});
					return;
				}else{
					//获取当前选择工单
					var decideadd=isadded(orderid);
					if(decideadd == 1){
						layer.msg('已加入工单，请勿重复操作');
						return;
					}else if(decideadd == 2){
						layer.msg('已添加该工单,候选人已归档');
						return;
					}else{
						var recruname="<%=empname%>";
			    		var json={
						  	id:id,
						  	orderid:orderid,
						  	//获取当前招聘人ID及姓名
						  	recruiterid:<%=empid%>,
						  	recruitername:recruname
						  };
					  	$.ajax({
							url: "com.recruit.interpolation.interpolationindex.addCandidate.biz.ext",
							type:'POST',
							data:json,
						    success:function(data){
								layer.msg('添加成功');
								$("#ordid").html("<li style='border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;'>已在工单列表</li>");
								$("#recruiter").html("<li style='border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;'>招聘负责人</li>"); 
								$("#recruitestuta").html("<li style='border-bottom: 2px solid #dddddd;padding-bottom: 30px;font-weight:bold;'>候选人状态</li>"); 
								selectorder();
								}
							});
						}
					}
				}
			}
			
			//删除简历
			function resumnedelete(id,filepath){
				var json = {
					id:id,
					filepath:filepath
				};
    			if(id == 0){
    				$.ajax({
						url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
						type : 'POST',
						async: false,
						data : json,
						success : function(data) {
	   					}
	 	 	 		});
    			}else{
					layer.open({
				        type: 1
				        ,offset:'auto' 
				        ,content: '<div style="padding: 20px 75px 0px;">确认删除附件？</div>'
				        ,closeBtn: false//不显示关闭图标
				        ,btn:['确定','取消']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index){
				        	$.ajax({
								url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
								type : 'POST',
								async: false,
								data : json,
								success : function(data) {
									if(id == 0){
									}else{
										layer.msg('附件删除成功');
										$("#file_upload").empty();
										resumeaccessory();
										var text=$("#file_upload").text();
							        	if(text=="" || text==null){
						        		 	$("#content_file").css("display","none");
					        		    }
										layer.close(index);//关闭页面
									}
		   						}
		 	 	 			});
			      	 	}
				        ,btn2: function(index, layero){
						    layer.close(index);//关闭页面
						}
					});
				}
			}
			
		//删除上传但未保存简历附件
    	function delectfile(upid,filepath){
			resumnedelete(0,filepath);
            var id="#"+upid.id;
            subStringfile(id);
        	$(id).remove();
        	var text=$("#file_upload").text();
        	if(text=="" || text==null){
        		 $("#content_file").css("display","none");
        	}
    	}
        
        //对未上传执行的链接进行删除
        function subStringfile(id){
        	var Arrid = id.substring(3, 4);
        	var filename = $("#upInfoname").val();
	    	var fileurl = $("#upInfo").val();
	    	var filepath = $("#upInfopath").val();
	    	var filenameArr = filename.split("^");
	    	var fileurlArr = fileurl.split("^");
	    	var filepathArr = filepath.split("^");
	    	
	    	var returnfilename = "";
	    	var returnfileuri = "";
	    	var returnpath = "";
	    	for (var i=0 ; i< filenameArr.length ; i++)
			{
				if(i == Arrid){
				
				}else{
					if(returnfilename == "" || returnfilename == null){
						returnfilename = filenameArr[i];
						returnfileuri = fileurlArr[i];
						returnpath = filepathArr[i];
					}else{
						returnfilename = returnfilename+"^"+filenameArr[i];
						returnfileuri = returnfileuri+"^"+fileurlArr[i];
						returnpath = returnpath+"^"+filepathArr[i];
					}
					
				}
			}
        	$('#upInfoname').val(returnfilename);
			$('#upInfo').val(returnfileuri);
			$('#upInfopath').val(returnpath);
        }
			
			
        
			
			//查询内推任务所在工单
			function selectorder(){
				var id = <%=id %>;
				
	    		var json={
				  	id:id
				  };
			  	$.ajax({
					url: "com.recruit.interpolation.interpolationindex.selectCandidate.biz.ext",
					type:'POST',
					data:json,
				    success:function(data){
				    	//获取数据集
				    	inorder = data;
				    	//往展示当前内推任务所在工单赋值
				    	$("#tbody").empty();
				    	for(var i =0;i<data.candidates.length;i++){
				    		if(data.candidates[i].candidateStatus == 3){
				    		
				    		}else{
				    		var canddatestatus =candidate(data.candidates[i].candidateStatus);
				    		var tr = "<tr>";
				    			tr +="<td>"+data.candidates[i].recuitOrderId+"</td>";
				    			tr +="<td>"+data.candidates[i].recruiterName+"</td>";
				    			tr +="<td>"+canddatestatus+"</td>";
				    			tr +="</tr>";
				    			$("#tbody").append(tr);
				    		}
							/* $("#ordid").append("<li>"+data.candidates[i].recuitOrderId+"</li>");
							$("#recruiter").append("<li>"+data.candidates[i].recruiterName+"</li>"); 
							$("#recruitestuta").append("<li>"+canddatestatus+"</li>");  */
				    	}
					}
				});
			}
			//结束工单
			function oversubmit(){
				var empid = <%=empid %>;
				var suggestedrecruiterid = $("#suggestname").attr("data");
				var recruiteridArray=new Array();
				recruiteridArray = suggestedrecruiterid.split(",");
				//标识
				var rak="";
				for(var i = 0;i < recruiteridArray.length ; i++){
					if(empid == recruiteridArray[i]){
						rak = 1;
					}
				}
				if(rak == 1){
					var id = <%=id %>
					if (id == "" || id == null) {//非空判断
						layer.msg('未找到当前招聘任务，请先保存');
						return;
					}
					layer.open({
				        type: 1
				        ,offset:'auto' 
				        ,content: '<div style="padding: 20px 75px 0px;">确认结束任务？</div>'
				        ,closeBtn: false//不显示关闭图标
				        ,btn:['确定','取消']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index){
				        	var json = {
							id : id
							};
							$.ajax({
								url : "com.recruit.interpolation.interpolationindex.updataRecommen.biz.ext",
								type : 'POST',
								data : json,
								success : function(data) {
									layer.msg('成功结束任务');
									setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
									location.reload();//页面刷新
									},1000);
								}
							});
				        }
			        ,btn2: function(index, layero){
					    layer.close(index);//关闭页面
					}
				});
					
				}else{
					layer.msg('抱歉你没有权限操作');
				}
					
			}
			
		//获取下拉框数据字典数据
		function dictionarycombox() {
			$.ajax({
				url : "com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
				type : 'POST',
				success : function(data) {
					//职位类别
					for (var i = 0; i < data.reposition.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.reposition[i].dictID);
						$(option).text(data.reposition[i].dictName);
						$('#positioncategory').append(option);
					}
					//内推来源类型
					for (var i = 0; i < data.rerecoemmendresoue.length; i++) {
						var option = document.createElement("option");
						$(option).val(data.rerecoemmendresoue[i].dictID);
						$(option).text(data.rerecoemmendresoue[i].dictName);
						$('#recommendresource').append(option);
					}
				}
			});
		}
		//判断是否已经添加到工单
		function isadded(ordid) {
			var decideadd = "";
			//获取所在工单单号对新增的单号对比
			for (var i = 0; i < inorder.candidates.length; i++) {
				if (inorder.candidates[i].recuitOrderId == ordid) {
					if(inorder.candidates[i].candidateStatus == 3){
						decideadd = 2;
					}else{
						decideadd = 1;
					}
				}
			}
			return decideadd;
		}
		
		function clean(){
			location.reload([true]);
		}

		//候选人状态
		function candidate(status) {
			var candstatus = "";
			if (status == 0) {//招聘中
				candstatus = "招聘中";
			} else if (status == 1) {//待入职
				candstatus = "待入职";
			} else if (status == 2) {//已入职
				candstatus = "已入职";
			} else if (status == 3) {//已归档
				candstatus = "已归档";
			} else if (status == 4) {//已离职
				candstatus = "已离职";
			}
			return candstatus;
		}
		
		//附件是否还有未上传的
		function isEnclosureExist(id){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				submit(id);//保存方法
			}
		}
		//人员选择控件
		function getEntryStaff2(idName){
			var StaffId=""//人员工号
			var StaffName=""//人员姓名
			layer.open({
				type: 2,
				title: '选择入职人员',
				content: server_context+'/coframe/tools/plugIn/selectStaff.jsp',
				area: ['1212px', '520px'],
				btn: ['确定', '取消'],
				yes: function(index, layero){ 
					    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
					    var a=iframeWin.getData();
					    
					    $(a).each(function(i,obj){
					    	if(i=="0"){
					    		StaffId+=obj.order;
					    		StaffName+=obj.username;
					    	}else{
					    		StaffId+=","+obj.order;
					    		StaffName+=obj.username;
					    	}
					    	
					    })
					    	$(idName).prev().val(StaffName);
					    	$(idName).prev().attr("data",StaffId);
				  			layer.close(layer.index);
					    
				},
				btn2: function(index, layero){
					   layer.close(layer.index);
					  }    
				
				
			});
		}
		//查询所属公司
		function selectCompany(){
			 //初始化查询所属公司
          	 $.ajax({
          		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
          		 type:'POST',
          		 cache: false,
          		 success:function(data){
          			 for(var i=0;i<data.data.length;i++){
          				 $("#company").append('<option value="'+data.data[i].companyId+'">'+data.data[i].companyName+'</option>');
          			 }
          		 }
          	 });
		}
	</script>
</body>
<script src="<%= request.getContextPath() %>/interpolation/js/resumeupload.js"></script>
</html>
