<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.UserObject"%>
<%@page import="com.eos.foundation.eoscommon.ResourcesMessageUtil"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="commonj.sdo.*,java.util.*,java.text.SimpleDateFormat"%>
<%@page import="com.eos.web.taglib.util.XpathUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wenjin
  - Date: 2018-03-12 10:11:26
  - Description:
-->
<head>
<title>内推任务</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link type="text/css" rel="styleSheet"
	href="<%=request.getContextPath()%>/interpolation/css/recommend_index.css" />
	<style type="text/css">
		.div_button{
		    text-align: center;
	   		margin-top: 10px;
		} 
	</style>
</head>
<%
	String empname = DataContextManager.current().getMUODataContext()
			.getUserObject().getUserRealName();
	String empid = DataContextManager.current().getMUODataContext()
			.getUserObject().getUserId();
	//session获取内推任务read
	String read=request.getParameter("read");
	//session获取内推任务页码数
	String curr=request.getParameter("curr");
	//session获取内推任务的筛选条件
	String storageJson = request.getParameter("storageJson");
%>
<body>

	<ul id="myTab" class="nav nav-tabs">
		<li class="active" id="recruitment" onclick="recruitment()"><a href=""
			data-toggle="tab"> 我的内推任务</a></li>
		<li class="" id="archive" onclick="archive()"><a href=""
			data-toggle="tab">全部内推任务</a></li>
		<li id="addbut">
			<button type="button" class="btn btn-warning" id="add" name="add"
				onclick="addsubmit()" style="width: 91%; margin-left: 19px;">新增任务</button>
		</li>
	</ul>
	<div class="tab-content">
		<!-- 我的内推任务 -->
		<div class="content">
			<div id="search-conditions">
				<!--筛选条件 -->
				
				申请职位：<input type="text" class="form-control input" id="position_2">
				被推荐人姓名：<input type="text" class="form-control input" id="name_2">
				任务状态：<select id="recommended_status_2" class="form-control input" >
									<option value=""></option> 
									<option value="0">未处理</option> 
									<option value="1">处理中</option> 
									<option value="2">已结束</option> 
							</select>
				<button type="button" onclick="seleruitment(1)" class="btn btn-primary" id="search" data-complete-text="Loading finished">搜索</button>
				<button type="button" class="btn btn-default btn-center" onclick="reset()" id="reset">重置</button>
			</div>
			<div id="content1"></div>
			<div id="pagination"></div>

		</div>
		<!-- 全部内推任务 -->
		<div class="content_1">
			<div id="search-conditions">
				<!--筛选条件 -->
				OA流程号：<input type="text" class="form-control input" id="oaNum">
				招聘负责人：<input type="text" class="form-control input" id="recruiterName">
				申请职位：<input type="text" class="form-control input" id="position">
				被推荐人姓名：<input type="text" class="form-control input" id="name">
				任务状态：<select id="recommended_status" class="form-control input" >
									<option value=""></option> 
									<option value="0">未处理</option> 
									<option value="1">处理中</option> 
									<option value="2">已结束</option> 
							</select>
				<div class="div_button">
					<button type="button" onclick="seleruitment(2)" class="btn btn-primary" id="search" data-complete-text="Loading finished">搜索</button>
					<button type="button" class="btn btn-default btn-center" onclick="reset()" id="reset">重置</button>
				</div>
			</div>
			<div id="content2"></div>
			<div id="pagination1"></div>
		</div>

	</div>
	<script type="text/javascript">
		$(function(){
			//获取内推任务的筛选条件
			var conditionsJson = <%=storageJson %>;
			//内推任务的筛选条件不为空
			if(conditionsJson!=null && conditionsJson!=""){
				//获取OA流程号
				var jsonOaId = conditionsJson["oaProcessId"];
				//获取招聘负责人
				var jsonRecruiterName = conditionsJson["suggestedRecruiterName"];
				//获取请职位
				var jsonRecommendedPosition = conditionsJson["recommendedPosition"];
				//获取被推荐人姓名
				var jsonCandicateName = conditionsJson["candicateName"];
				if(jsonOaId!=""){
					$("#oaNum").val(jsonOaId);
				}
				if(jsonRecruiterName!=""){
					$("#recruiterName").val(jsonRecruiterName);
				}
				if(jsonRecommendedPosition!=""){
					$("#position").val(jsonRecommendedPosition);
				}
				if(jsonCandicateName!=""){
					$("#name").val(jsonCandicateName);
				}
			}
			//初始数据
	   		readaction();
		})
       //保存逻辑流名称
	   var urlStr ="";
	   //控件属性
	   var elem = '';
	   var content ='';
	   function readaction(){
	   	//debugger;
	   	  var read = <%=read %>;
	   	  var currs = <%=curr %>;
	  	  if(read == "" || read == null){
	   		 seleruitment(1);
	      }else{
	      	 if(read == 1){
	      	 	$("#myTab li").removeAttr("class"); 
	      	 	$("#recruitment").addClass("active");
	      	 	recruitment(currs);
	      	 }else{
	      	 	$("#myTab li").removeAttr("class"); 
	      	 	$("#archive").addClass("active");
	      	 	archive(currs);
	      	 }
	      }
	   }
	   
	   //我的内推任务
    	function recruitment(currs){
    		$(".content").show();
    		$(".content_1").hide();
    		seleruitment(1,currs);
    	}
    	//全部内推任务
    	function archive(currs){
    		$(".content").hide();
    		$(".content_1").show();
    		seleruitment(2,currs);
    	}
    	//存储筛选条件值
    	var storageJson = {};
    	var curr;
        function seleruitment(read,currs){//read为判断标识
        	var currlog;
        	if(currs == "" || currs == null){
        		currlog = 0;
        	}else{
        		currlog = currs;
        	}
        	
        	//传当前招聘负责人ID
	        var json={
	  			id:<%=empid%>
	     	};
	        if(read == 1){//1为查询当前招聘负责人内推任务
	        	
	        	 //申请职位
	        	 var position = $("#position_2").val();
	        	 //被推荐人姓名
	        	 var name = $("#name_2").val();
	        	 //状态
	        	 var status=$("#recommended_status_2").val();
	        	 json = {"recommendedPosition":position,"candicateName":name,"status":status};
	        	 //赋值条件值为json格式
	        	 storageJson = json;
	        	 
		         urlStr ="com.recruit.interpolation.interpolationindex.selectInteroilationIndex.biz.ext";
		         elem = 'pagination';
		         content ='content1';
	        }else if(read == 2){//2为查询所有招聘负责人内推任务
	        	 //oa流程号
	        	 var oaProcessId = $("#oaNum").val();
	        	 //招聘负责人
	        	 var recruiterName = $("#recruiterName").val();
	        	 //申请职位
	        	 var position = $("#position").val();
	        	 //被推荐人姓名
	        	 var name = $("#name").val();
	        	  //状态
	        	 var status=$("#recommended_status").val();
	        	 json = {"oaProcessId":oaProcessId,"suggestedRecruiterName":recruiterName,
	        	 			 "recommendedPosition":position,"candicateName":name,"status":status};
	        	 //赋值条件值为json格式
	        	 storageJson = json;			 
	        	 urlStr ="com.recruit.interpolation.interpolationindex.selectInteroilationAll.biz.ext";
	    	     elem ='pagination1';
	    		 content = 'content2';
	        }
	       
	     	
	        $.ajax({
				url: urlStr,
				type:'POST',
				data:json,
				success:function(data){
				var dataArray = new Array();
				console.log(data);
				if(read==1){
					dataArray = data.recommend;
				}else{
					dataArray = data.data;
				}
				if(dataArray.length<1){//判断是否有数据
					if(read == 1){
						$("#content1").empty();
						$("#content1").append('<p style="text-align: center;color: #9E9E9E;font-weight: 100;margin-bottom: 10px;font-size: 16px;">暂无任务</p>');
					}else{
						$("#content2").empty();
						$("#content2").append('<p style="text-align: center;color: #9E9E9E;font-weight: 100;margin-bottom: 10px;font-size: 16px;">暂无任务</p>');
					}
					
				}
				
				layui.use(['laypage','layer'], function(){
					 var laypage= layui.laypage;
					  
					 //要在这里面写你的代码哦
					 laypage.render({
					    elem: elem
					    ,count: dataArray.length
					    ,limit: 5
					    ,curr:currlog
					    ,jump: function(obj){
					      curr =obj.curr;
					      //模拟渲染
					      	document.getElementById(content).innerHTML = function(){
					        var arr = []
					        ,thisData = dataArray.concat().splice(obj.curr*obj.limit - obj.limit, obj.limit);
					        layui.each(thisData, function(index, item){
					          
					          var curTime;
						      if(item.approvedTime == null || item.approvedTime == ""){
						        	curTime = "";
						      }else{
							        var oldTime = (new Date(''+item.approvedTime+'')).getTime();
									curTime = new Date(oldTime).format("yyyy-MM-dd");
							  }
					          
					          
					          var core = "";
					          if(item.orgcentreDept == "" || item.orgcentreDept == null){
					          	core="";
					          }else{
					          	core=item.orgcentreDept;
					          }
					      		
					      	  var innerHtml = "";
					      	  var prcessid = "";	    
					          if(item.oaProcessId == "" || item.oaProcessId == null){
					          	  innerHtml = '<td>手工创建单</td>';
					          	  if(item.recommendProcessId == "" || item.recommendProcessId == null){
					          	  	prcessid="";
					          	  }else{
					          	  	prcessid = item.recommendProcessId;
					          	  }
					          }else{
					          	  innerHtml = '<td>OA流程号：'+item.oaProcessId+'</td>';
					          	  if(item.recommendProcessId == "" || item.recommendProcessId == null){
					          	  	prcessid="";
					          	  }else{
					          	  	prcessid = item.recommendProcessId;
					          	  }
					          }
					          
					          var innerHtml1 = "";
					          var recommended_status=recommendstatus(item.recommendedStatus);
					          if(item.recommendedStatus == 0){
					          	  innerHtml1 = '<div style="border: 1px solid;color: red;width: 20%;text-align: center;float: right;margin-right: 52%;">'+recommended_status+'</div>';
					          }else if(item.recommendedStatus == 1){
					              innerHtml1 = '<div style="border: 1px solid;color: #52b694;width: 20%;text-align: center;float: right;margin-right: 52%;">'+recommended_status+'</div>';
					          }else{
					          	  innerHtml1 = '<div style="border: 1px solid;color: #f6b756;width: 20%;text-align: center;float: right;margin-right: 52%;">'+recommended_status+'</div>';
					          }
					         
					          arr.push('<table class="table" onclick="clik('+item.id+','+read+')" >'+
					          '<tr class="tr">'+
					          '<td class="td" colspan="4" id="oaofficeid">单号：'+prcessid+'</td>'+
					          '</tr>'+
					          '<tr class="tt">'+
					          ''+innerHtml+''+
					          '<td>日期：'+curTime+'</td>	'+
					          '<td>建议招聘负责人：'+item.suggestedRecruiterName+'</td>'+
					          '<td>任务状态： '+innerHtml1+'</td>'+
					          '</tr>'+
					          '<tr class="tt">'+
					          '<td>推荐人姓名：'+item.recommenderName+'</td>'+
					          '<td>推荐人电话：'+item.recommenderPhone+'</td>'+
					          '<td>应聘部门：'+core+'</td>'+
					          '<td>推荐职位：'+item.recommendedPosition+' </td>'+
					          '</tr>'+
					          '</table>'
					          );
					          
					        });
					        return arr.join('');
					      }();
					    }
					  });
					});
				}
				});
        }
        
        //跳转到详细页面
    	function clik(id,read){
    		//json对象转json字符串
    		 storageJson = JSON.stringify(storageJson);
    		 var url="/interpolation/recommend_details.jsp?id="+id+"&storageJson="+storageJson+"&read="+read+"&curr="+curr;
    		 parent.goToUrl(url);
    	}
    	//新增内推任务
    	function addsubmit(){
    		var url="/interpolation/recommend_details.jsp";
    		parent.goToUrl(url);
		}
		//重置
		function reset(){
			$(".input").val("");
		}
		
		//回车事件
		document.onkeydown = function(e){
		var ev = document.all ? window.event : e;
			if(ev.keyCode==13) {
				//执行查询全部内推任务
				seleruitment(2);
			}
		};
	</script>
</body>
</html>