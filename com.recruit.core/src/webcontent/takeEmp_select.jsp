<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-19 15:58:46
  - Description:
-->
<head>
<title>HR人员选择控件</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/task/css/takeEmp_select.css?v=1.0">
     <script type="text/javascript" src="<%=request.getContextPath()%>/task/js/takeEmp_select.js?v=1.0"></script>
    <%@include file="/common/common.jsp" %>
    <style type="text/css">
    
    </style>
</head>

<body style="width: 100%;height: 100%;">
<div style="width: 100%;height: 80%; padding: 0px 20px;">
	<div class="panel panel-default par">
			<div class="panel-body" style="width: 100%; height: 100%;">
				<form class="form-inline" style="width: 100%; height: 100%;">
					<table style="width: 100%; height: 100%;">
						<tr>
							<td style="text-align: right;"><label>招聘负责人工号：</label> 
								<input type="text" class="form-control" id="empid" name="" placeholder=""> 
								<span style="display: inline-block; width: 30px;"></span>
							</td>
							<td>
								<span style="display: inline-block; width: 30px;"></span>
								<label>招聘负责人姓名：</label> 
								<input type="text" class="form-control" id="empname" name="" placeholder="">
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" style="padding-top: 12px;">
								<button class="btn btn-warning" type="button" onclick="searchEmp()">搜索</button>
								<span style="display: inline-block; width: 25px;"></span>
								<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<table id="demoEmp" lay-filter="test"></table>
	</div>
	<p class="p-text" style="display: none;">备注：若为招聘中的工单，选择更改招聘负责人后将归档原工单，自动生成新工单。</p>
	<input id="emp" value="" type="text" style="display: none;">
	
	<!-- 部门 -->
	<script type="text/html" id="tempOrgname">
		{{'<div class="title" title="'+d.orgname+'">'+d.orgname+'</div>'}}
	</script>
	
	<script type="text/javascript">
	
    </script>
</body>
</html>