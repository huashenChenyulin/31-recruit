<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): jiangsanbin
  - Date: 2018-04-26 17:38:25
  - Description:
-->
<head>
<title>修改工单数据</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/task/css/order_update.css?v=1.0">
<script type="text/javascript" src="<%=request.getContextPath()%>/task/js/order_update.js?v=1.0"></script>

</head>
<body>
	<div class="all">
		 <table class="table-left">
			<tr>
				<td class="adv"><label class="layui-form-label left">招聘负责人：</label><label class="layui-form-label right" id="recruiterName"></label></td>
			</tr>
			<tr>
				<td class="dextro"><label class="layui-form-label left ">招聘职位：</label><label class="layui-form-label right " id="position"></label></td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label left ">审批日期：</label><label class="layui-form-label right " id="auditTime"></label></td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label left">招聘状态：</label><label class="layui-form-label right" id="roStatus"></label></td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label left">剩余招聘周期：</label><label class="layui-form-label right" id="surplusPeriod"></label></td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label left">OA流程号：</label><label class="layui-form-label right" id="oaProcessId"></label></td>
			</tr>
		</table> 
		 <table class="table-center">
			<tr>
				<td class="dextro"><label class="layui-form-label seq-left">部<span class="center-span"></span>门：</label><label class="layui-form-label right-text" id="orgName" title=""></label></td>
			</tr>
			<tr>
				 <td class="dextro"><label class="layui-form-label  seq-left">招聘原因：</label><label class="layui-form-label right-text">
						<select class="form-control reason" id="mainReason">
							<option value="业务增量">业务增量</option>
							<option value="员工离职">员工离职</option>
							<option value="员工辞退">员工辞退</option>
							<option value="新增业务">新增业务</option>
							<option value="新设岗位">新设岗位</option>
							<option value="晋升或异动">晋升或异动</option>
						</select>
					</label>
				</td> 
			</tr>
			<tr>
				<td class="dextro"><label class="layui-form-label seq-left movement">职位类别：</label><label class="layui-form-label right-text" >
					<select class="form-control state" id="jobCategory">
							
						</select>
					</label>
				</td>
			</tr>
			<tr>
				<td class="dextro"><label class="layui-form-label seq-left">发送offer时间：</label><label class="layui-form-label right-text" id="offerTime" ></label></td>
			</tr>
			<tr class="relevance_order_id">
				<td class="dextro"><label class="layui-form-label seq-left move">已关联工单：</label><label class="layui-form-label right-text"><a onclick="relevanceOrder(this)" id="relevance_order_id" data="" dataId="" class="recru-color">请选择</a><span class="rec_span">（审批日期：<span id="relevanceauditTime"></span>）</span></label></td>
			</tr>
			
		</table>
		<table class="table-right">
			
			<tr>
				 <td class="dextro"><label class="layui-form-label  rig-left">招聘周期：</label><label class="layui-form-label rig-text">
						<select class="form-control state" id="recruitPeriod">
							<option value="15">15天</option>
							<option value="30">30天</option>
							<option value="45">45天</option>
							<option value="60">60天</option>
							<option value="90">90天</option>
						</select>
					</label>
				</td> 
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label rig-left move">是否难招：</label><label class="layui-form-label rig-text" >
					<select class="form-control drop" id="isDifficult"><option value="1" >是</option><option value="0">否</option></select>
					</label>
				</td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label rig-left">职位属性：</label><label class="layui-form-label rig-text" >
    				<select class="form-control drop" id="categoryAttribute"><option value="1">计时</option><option value="2">计件</option></select>
					</label>
				</td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label rig-left">是否实习生：</label><label class="layui-form-label rig-text" >
    				<select class="form-control drop" id="isIntern"><option value="1">是</option><option value="0">否</option></select>
					</label>
				</td>
			</tr>
			<tr>
				<td class="adv"><label class="layui-form-label rig-left">所属公司：</label><label class="layui-form-label rig-text" id="company" ></label>
				</td>
			</tr>
		</table>  
			<input type="text" style="display: none;" id="id">
		</div>
		<div class="demo-table">
			<table id="demoCandid" lay-filter="test"></table>
		</div>
	
	<!-- 	放弃原因 -->
	<script type="text/html" id="tempabandonDetails">
		{{'<div class="title" title="'+d.abandonDetails+'">'+filtrationNull(d.abandonDetails)+'</div>'}}
	</script>
	
	<!-- 	放弃情况-->
	<script type="text/html" id="tempabandonReason">
		{{'<div class="title" title="'+d.abandonReason+'">'+filtrationNull(d.abandonReason)+'</div>'}}
	</script>
	
	<!-- 	候选人姓名-->
	<script type="text/html" id="tempResume">
		{{'<div  class="title" title="'+d.recruitResume.candidateName+'">'+d.recruitResume.candidateName+'</div>'}}
	</script>

	<script type="text/javascript">
		
                
	</script>
</body>
</html>