<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
	<%
		//获取从当前任务点击关联工单传过来的参数
		String isCurrentTask = request.getParameter("isCurrentTask");
	 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000100
  - Date: 2018-05-24 10:09:50
  - Description:
-->
<head>
<title>选择关联工单</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<%=request.getContextPath() %>/task/js/relevanceOrder_select.js?v=1.0"></script>
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/task/css/relevanceOrder_select.css?v=1.0">
    
</head>
<body>
	<div style="width: 100%;height: 80%; padding: 0px 20px;">
	<div class="panel panel-default par">
			<div class="panel-body rel">
				<form class="form-inline" style="width: 100%; height: 100%;">
					<table style="width: 100%; height: 100%;">
						<tr>
							<td><label>招聘负责人工号：</label> 
								<input type="text" class="form-control left" id="recruiterId" name="" placeholder=""> 
							</td>
							<td>
								<label class="lab-center">招聘负责人姓名：</label> 
								<input type="text" class="form-control center"  id="recruiter_name" name="" placeholder="">
							</td>
							<td>
								<label class="lab-right">OA流程号：</label> 
								<input type="text" class="form-control right"  id="oaProcessId" name="" placeholder="">
							</td>
						</tr>
						<tr>
							<td align="center" colspan="3" class="td-bottom">
								<button class="btn btn-warning" type="button" onclick="searchOrder()">搜索</button>
								<span style="display: inline-block; width: 25px;"></span>
								<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<table id="demoOrder" lay-filter="test"></table>
		<!-- 分页条 -->
		<div id="pagingToolbar"></div>
		<input id="orderId" value="" type="text" style="display: none;">
	</div>
	
<!-- 	转换日期格式 -->
	<script type="text/html" id="tempTime">
		<div>{{getDate(d.auditTime)}}</div>
	</script> 
<!-- 	部门-->
	<script type="text/html" id="tempDepartmen">
		{{'<div class="title" title="'+formatDepartment(d.l1Name,d.l2Name,d.l3Name,d.l4Name)+'">'+formatDepartment(d.l1Name,d.l2Name,d.l3Name,d.l4Name)+'</div>'}}
	</script>
	<!-- 	职位类别 -->
	<script type="text/html" id="tempjobCategory">
		{{'<div class="title" title="'+Positioncatagory(d.jobCategory)+'">'+Positioncatagory(d.jobCategory)+'</div>'}}
	</script>
	<!-- 职位-->
	<script type="text/html" id="tempPostion">
		{{'<div class="title" title="'+d.position+'">'+d.position+'</div>'}}
	</script>
	
	<script type="text/javascript">
		//用来区分是是否从当前任务的关联工单点过来的
		var isCurrentTask = '<%=isCurrentTask %>';
		if(isCurrentTask == "yes"){
			includeLinkStyle("<%= request.getContextPath() %>/myTask/css/currentRelevanceOrder.css");
		}
		//引用外部css样式
		function includeLinkStyle(url) {
		 var link = document.createElement("link");
		 link.rel = "stylesheet";
		 link.type = "text/css";
		 link.href = url;
		 document.getElementsByTagName("head")[0].appendChild(link);
		}
 

    </script>
</body>
</html>