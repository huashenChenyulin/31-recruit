<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-12 10:49:32
  - Description:
-->
<head>
<title>我的任务</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/task/css/takeMan_select.css?v=1.0">
 <script type="text/javascript" src="<%=request.getContextPath()%>/task/js/taskMan_select.js?v=1.0"></script>
</head>
<body>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td class="twin"><label class="layui-form-label">审批日期：</label> <input type="text"
							class="form-control import hour" id="startTime"
							placeholder="  yyyy-MM-dd"><span class="td-sol-span">至</span>
							<input type="text" class="form-control import hour" id="endTime" 
								placeholder="  yyyy-MM-dd">
						</td>
						
						<td class="td-pos" align="center">
							<label class="layui-form-label label-center">招聘负责人：</label>
							<input id="recruiter_name" class="form-control import-center" type="text"  >
						</td>
						<td class="td"><label class="layui-form-label">部<span class="center-span"></span>门：</label>
							<input class="form-control getStaff end" id="orgName" onclick="getL2Org(this)" alias="" L1="" L1name="" L2="" 
								L2name="" L3="" L3name="" L4="" L4name="" L2placeholder="人员选择" value=""  type="text">
						</td>
						
					</tr>
					<tr class="cre_tr">
						<td class="twin">
							<label class="layui-form-label">单<span class="center-span">
							</span>号：</label>
							<input id="work_id" class="form-control import" type="text" >
						</td>
						<td align="center">
							<label class="layui-form-label label-center">招聘状态：</label>
								<select id="roStatus" class="form-control import import-center" >
									<option value="">请选择招聘状态</option>
								</select>
						</td>
						<td>
							<label class="layui-form-label">职位类别：</label> 
								<select id="category" class="form-control import">
							<option value="">请选择职位类别</option></select>
								
						</td>
						
					</tr>
					<tr>
						<td align="center" colspan="6" class="td-btn">
							<button class="btn btn-warning upd left" type="button" onclick="updateOrderInfo()">修改工单</button>
							<button class="btn btn-warning upd" type="button" data-toggle="modal" data-target="#myModal">创建工单</button>
							<button class="btn btn-warning btn-center" type="button" id="select" onclick="queryOrder()">搜索</button>
							<span style="display: inline-block; width: 25px;"></span>
							<button type="button" class="btn btn-default btn-center"  onclick="isReset()">重置</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">创建工单</h4>
				</div>
				<div id="addrecuitOrder" class="modal-body">
					<label class="layui-form-label set">招聘负责人：</label><input class="form-control" id="recName" onclick="getRecruiterName(this)" value=""  type="text">				
					<label class="layui-form-label set">部门：</label><input class="form-control getStaff" id="department" onclick="choiceOrg(this)" alias="" L1="" L1name="" L2="" L2name="" L3="" L3name="" L4="" L4name="" L2placeholder="人员选择" value=""  type="text">				
					<label class="layui-form-label set">需求职位：</label>
						<select id="addposition" class="form-control" >
							<option vlaue="">请选择部门</option>
						</select>
					<div class="found_type">
						<label class="layui-form-label post set">职位类别：</label>
						<select id="addcategory" class="form-control" onchange="findPeriod(this)" >
								<option value="">请选择职位类别</option>
						</select>
 
					</div>
					<div style="display:flex; height: 48px;">
						<div class="rec_fair">
							<label class="layui-form-label post set">招聘周期（天）：</label>
							<select id="recruitPeriod" class="form-control import end" >
									<option value="">请选择招聘周期</option> 
									<option value="15">15</option> 
									<option value="30">30</option> 
									<option value="45">45</option> 
									<option value="60">60</option> 
									<option value="90">90</option> 
							</select>
						</div>
						<div class="rec_fair">
							<label class="layui-form-label post set">职位属性：</label>
							<select id="catAttribute" class="form-control import end" >
									<option value="">请选择职位属性</option>
									<option value="1">计时</option> 
									<option value="2">计件</option> 
							</select>
						</div>
						<div class="rec_fair">
							<label class="layui-form-label post set">所属公司：</label>
							<select id="company" class="form-control import end" >
									<option value="">请选择所属公司</option>
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="addOrder()">创建</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<div class="div_modal">
		<table id="demo" height="100%" lay-filter="test">
		</table>
		<!-- 分页条 -->
		<div id="pagingToolbar"></div>
	</div>
	
<!-- 	招聘负责人-->
	<script type="text/html" id="recruiterName">
	{{
		(d.recruiterName==null)?'<span class="glyphicon glyphicon-search"></span><a class="recru-color" id="empname" onclick="selectRecruiterName(this)" data='+d.id+' empid='+d.recruiterId+'>'+"请选择"+'</a>':(d.roStatus==1)?('<span class="glyphicon glyphicon-search"></span><a onclick="selectRecruiterName(this)" id="empname" class="title"  title="'+d.recruiterName+'" data="'+d.id+'" empid="'+d.recruiterId+'">'+d.recruiterName+'</a>'):'<div class="title" title="'+d.recruiterName+'">'+d.recruiterName+'</div>'
	}}
	</script>
<!-- 	工单号-->
	<script type="text/html" id="isMannulId-html">
	{{
		(d.isMannulId==1)?'<span>'+d.recuitOrderId+'</span><span class="layui-badge">口头</span>':'<span>'+d.recuitOrderId+'</span>'	
	}}
	</script>
<!-- 	职位类别 -->
	<script type="text/html" id="jobCategory">
		{{'<div class="title" title="'+Positioncatagory(d.jobCategory)+'">'+Positioncatagory(d.jobCategory)+'</div>'}}
	</script>
<!-- 	招聘周期 -->
	<script type="text/html" id="periodDay">
		{{d.recruitPeriod}}天
	</script>
<!-- 	转换日期格式 -->
	<script type="text/html" id="tempTime">
		<div>{{getDate(d.auditTime)}}</div>
	</script> 
<!-- 	OA流程号 -->
	<script type="text/html" id="tempProcessId">
		<div>{{(d.oaProcessId==null)?'':d.oaProcessId}}</div>
	</script> 
<!-- 	招聘状态 -->
	<script type="text/html" id="tempRostatus">
		{{(d.roStatus!=1)?'<div>'+rostatus(d.roStatus)+'</div>':rostatus(d.roStatus)}}
	</script> 
<!-- 	是否实习生 -->
	<script type="text/html" id="tempIntern">
		{{(d.isIntern==1)?"是":"否"}}
	</script>
<!-- 	是否难招 -->
	<script type="text/html" id="tempDifficult">
		{{(d.isDifficult==1)?"是":"否"}}
	</script>
<!-- 	操作 -->
	<script type="text/html" id="tempOperation">
	{{#  if(d.roStatus != 1){ }}
   		<div class="oper">取消招聘</div>
  	{{#  } else { }}
    <a class="box" onclick="updateRoState('{{d.recuitOrderId}}','{{d.id}}')">取消招聘</a> 
  	{{#  } }}
	</script>
<!-- 部门 -->
	<script type="text/html" id="tempOrgName">
		{{'<div class="title" title="'+formatDepartment(d.l1Name,d.l2Name,d.l3Name,d.l4Name)+'">'+formatDepartment(d.l1Name,d.l2Name,d.l3Name,d.l4Name)+'</div>'}}
	</script>
<!-- 职位-->
	<script type="text/html" id="tempPostion">
		{{'<div class="title" title="'+d.position+'">'+d.position+'</div>'}}
	</script>
<!-- 所属公司-->
	<script type="text/html" id="tempCompany">
		{{'<div class="title" title="'+contverCompany(d.company)+'">'+contverCompany(d.company)+'</div>'}}
	</script>
	<script type="text/javascript">
    	//回车键触发点击事件
		$("body").keydown(function() {
			var event=arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异
             if (event.keyCode == "13") {//keyCode=13是回车键
                 $("#select").click();
             }
         });
                
    </script>
</body>
</html>