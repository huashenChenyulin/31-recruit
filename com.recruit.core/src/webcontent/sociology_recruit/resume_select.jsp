<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-03-14 17:23:31
  - Description:
-->
<head>
<title>简历查看</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />


		<!-- 查看简历CSS -->
		<link rel="stylesheet" type="text/css"
			href="<%= request.getContextPath() %>/task/sociology_recruit/css/resumes_select.css">
		
		
		<%
				//简历id
				String resumeId = request.getParameter("resumeId");
				
			 %>
		
	<style type="text/css">
		
		.table tbody tr td,.table tbody tr th,.table tfoot tr td,.table tfoot tr th,.table thead tr td,.table thead tr th
			{
			border-top: 0px solid;
		}
		
		.table thead tr th {
			border-bottom: 1px solid #ddd;
			color: #565656;
		}
		
		tbody {
			color: #656565;
		}
		
		#print {
			float: right;
			margin-right: 10px;
			margin-top: 5px;
			color: #e28603;
		}
		
		#print:HOVER {
			cursor: pointer;
		}
		
		.caption {
			background-color: #f9f9f9;
		}
		
		.caption2 {
			background-color: #fff;
		}
		
		#docDemoTabBrief {
			width: 92%;
			float: left;
			margin-left: 4%;
		}
		
		/* 照片 */
		#content-top-left .samp_img{
			float: left; margin-right: 10px;width: 150px; height: 150px;border: 1px solid #dedcdc;
		}
		.panel-heading {
		    padding: 5px 15px;
		   
		}
		.assess{
			height:200px;
		}
		.assesstext{
		    display: inline-block;
		    font-size: 16px;
		    margin-bottom: 8px;
		}
</style>
</head>
<body id="pdf" style="float: left; width: 100%;">
	<div id="main">
		<!-- 左布局 -->
		<div id="main-left">
			<div id="content-top">
				<!-- 头部的个人信息 -->
				<div id="content-top-left">
					<samp class="samp_img" style="">
						<img class="img" src="" style="width: 150px; height: 150px;">
					</samp>
					<table class="table" style="float: left; width: 200px;">
						<caption id="name"></caption>
						<tbody>
							<tr>
								<td><samp id="sex"></samp> <samp>|</samp> <samp id="degree"></samp></td>
							</tr>
							<tr>
								<td id="english_level"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="content-top-right">
					<table class="table">
						<tbody>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/date.png">
									</samp> <samp id="birthDate"></samp><samp> |</samp> <samp id="birthDate-age"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/address.png">
									</samp> <samp id="address"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/phone.png">
									</samp> <samp id="phone"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/email.png">
									</samp> <samp id="email"></samp></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- <a id="print" class="glyphicon glyphicon-save" onclick="downPDF()">下载PDF</a> -->
				
			</div>
			<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" id="docDemoTabBrief">
			  <ul class="layui-tab-title">
			    <li class="layui-this">简历信息</li>
			    <li>面试评价</li>
			   
			  </ul>
			    <div class="layui-tab-content" style="height: 100px;">
			    <div class="layui-tab-item layui-show">

			<!-- 基本信息 -->
			<div id="main-table">
				<div id="content-centre">
				<table class="table">
					<caption class="caption">基本信息</caption>

					<thead>
						<tr>
							<th>毕业院校</th>
							<th>专业</th>
							<th>毕业时间</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="college"></td>
							<td id="major"></td>
							<td id="graduationDate"></td>
						</tr>
					</tbody>

					<thead>
						<tr>
							<th>身份证号码</th>
							<th>户口所在地</th>
							<th>身高(cm)/体重(kg)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="identityNo"></td>
							<td id="birthplace"></td>
							<td id="heightAndWeight"></td>
						</tr>
					</tbody>

					<thead>
						<tr>
							<th>紧急联系人姓名</th>
							<th>与紧急联系人关系</th>
							<th>紧急联系人电话</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="emergencyContact"></td>
							<td id="emergencyRelationship"></td>
							<td id="emergencyPhone"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- 申请职位-->
			<div id="centent-bottom">
				<table class="table">
					<caption class="caption">申请职位</caption>
					<thead>
						<tr>
							<th colspan="3">申请职位</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="position" colspan="3"></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th colspan="3">期望工作城市(1)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="expected-address-One" colspan="3"></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th>期望薪资</th>
							<th>得知招聘信息的渠道</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="expected-salary"></td>
							<td id="recruit-noticed-channel"></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th>是否服从工作职位的调配</th>
							<th>是否服从工作城市的调配</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="isPositionArrange"></td>
							<td id="is-workplace-arrange"></td>
						</tr>
					</tbody>
				</table>
				</div>
			

				<!-- 个人履历 -->
				<div id="centent-bottom2">
					<table class="table" id="personalResume">
	
					</table>
					<table class="table" id="recruitSchoolPractice">
	
					</table>
					<table class="table" id="recruitWorkExperience">
	
					</table>
				 </div>
			</div> 
		</div>
		<div class="assess" id="assess" > <!-- 评估板 -->
		
		</div> 
	</div>
</div>
		</div><!-- main-table -->
		<!-- 右布局 -->
		<div id="main-right">
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="preview"
					name="preview" onclick="preview2()">下载简历附件</button>
			</div>
			<div id="resumes">
				<p style="font-weight: bold; margin-top: 3%;">附件：</p>
				<div id="file_upload"></div>
			</div>
			<div id="hr">
				<hr>
			</div>
	
		</div>
	</div>

	<script type="text/javascript">
		var resumeID=<%=resumeId %>;
		
		var name;
		
		$(function(){
			
			layui.use('element', function(){
				  var $ = layui.jquery
				  ,element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
				  
				  element.on('tab(docDemoTabBrief)', function(data){
				  	//tap名称
					var innerText=data.elem.context.innerText;
					if(innerText!="简历信息"){
						$("#main-table").hide();//隐藏简历信息
						$("#assess").show();//显示评估信息
						 selectTemplate();//候选人评估信息
						
					}else{
						$("#main-table").show();//显示简历信息
						$("#main-left").css("height","100%");
						$("#assess").hide();//隐藏评估信息
						
					}
				  });
		  
		});
			//查询个人信息
			 selectResume();
		})
		
		var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	
		//是否服从工作职位的调配
		 function is_positionArrange(i){
			var isPositionArrange="";
		 	if(i==1){
		 		isPositionArrange="是";
		 	}else{
		 		isPositionArrange="否";
		 	}
		 	return isPositionArrange;
		 } 	
		 //是否服从工作城市的调配
		 function is_workplaceArrange(i){
			var workplaceArrange="";
		 	if(i==1){
		 		workplaceArrange="是";
		 	}else{
		 		workplaceArrange="否";
		 	}
		 	return workplaceArrange;
		 } 	
		 //得知招聘信息的渠道
		 function recruitNoticedChannel(i){
			var recruitNoticedChannel="";
		 	if(i==1){
		 		recruitNoticedChannel="索菲亚官网";
		 	}else if(i==2){
		 		recruitNoticedChannel="宣讲会";
		 	}else if(i==3){
		 		recruitNoticedChannel="微信推送";
		 	}else if(i==4){
		 		recruitNoticedChannel="招聘网站";
		 	}else if(i==5){
		 		recruitNoticedChannel="其他";
		 	}
		 	return recruitNoticedChannel;
		 }
		 //性别判断
         function sexstatus(status){
         	var sexstatus= "";
         	if(status == 1){
         		sexstatus="男";
         	}else if(status == 2){
         		sexstatus="女";
         	}else if(status == 3){
         		sexstatus="未知";
         	}
    		return sexstatus;
         }
         
         //判断学历
         function educationstatus(status){
         	var educationstatus= "";
         	if(status == 01){
         		educationstatus="博士研究生（博士后）";
         	}else if(status == 02){
         		educationstatus="博士研究生";
         	}else if(status == 03){
         		educationstatus="硕士研究生";
         	}else if(status == 04){
         		educationstatus="大学本科";
         	}else if(status == 05){
         		educationstatus="大专学历";
         	}else if(status == 06){
         		educationstatus="中专学历";
         	}else if(status == 07){
         		educationstatus="高中学历（职高）";
         	}else if(status == 08){
         		educationstatus="高中学历（技校）";
         	}else if(status == 09){
         		educationstatus="高中学历";
         	}else if(status == 10){
         		educationstatus="初中学历";
         	}else if(status == 11){
         		educationstatus="小学学历";
         	}else if(status == 12){
         		educationstatus="文盲或半文盲";
         	}else if(status == 13){
         		educationstatus="无学历证书";
         	}
         	
    		return educationstatus;
         }
		 //英语等级
		 function englishLevel(i){
			var englishLevel="";
		 	if(i==01){
		 		englishLevel="大学英语四级";
		 	}else if(i==02){
		 		englishLevel="大学英语六级";
		 	}else if(i==03){
		 		englishLevel="专业四级";
		 	}else if(i==04){
		 		englishLevel="专业八级";
		 	}else if(i==05){
		 		englishLevel="无";
		 	}
		 	return englishLevel;
		 }
		 //返回年龄
         function getAge(birthdate){
         	var returnAge; 
       		if(birthdate!=null && birthdate!="" ){
 		   
 		    var strBirthdayArr=birthdate.split("-");  
 		    var birthYear = strBirthdayArr[0];  
 		
 		    var d = new Date();  
 		    var nowYear = d.getFullYear();  
 		
 		    if(nowYear == birthYear)  
 		    {  
 		        returnAge = 0;//同年 则为0岁  
 		    }  
 		    else  
 		    {  
 		        var ageDiff = nowYear - birthYear ; //年之差  
 		        if(ageDiff > 0)  
 		        {  
 		        	returnAge =ageDiff;
 		        }  
 		        else  
 		        {  
 		            returnAge = -1;//返回-1 表示出生日期输入错误 晚于今天  
 		        }  
 		    }  
 		  }else{
 		  		returnAge="0";
 		  }
 		    return returnAge+" 岁";//返回周岁年龄  
 		         
          
          }
		
		 //查询个人信息
		 function selectResume(){
		 	//var id='113753';
		 	var json={
		 		resumeId:resumeID
		 	}
		 	$.ajax({
		 		url:"com.recruit.sociology_recruit.sociology_recruit.queryResume.biz.ext",
		 		type:"post",
		 		data:json,
		 		success:function(data){
		 			//简历表
		 			var resume=data.resume;
		 			//职位表
		 			var position=data.position;
		 			//附件表
		 			var resume_axccessory=data.resume_axccessory;
		 			//在校实践表
		 			var resume_school=data.resume_school;
		 			//社会实践表
		 			var resume_work=data.resume_work;
		 			
		 			console.log(position);
		 			if(resume[0].resumePictureUrl!=null && resume[0].resumePictureUrl!=""){
		 				var src="<%= request.getContextPath() %>/"+resume[0].resumePictureUrl;
			 			//个人照片
			 			$("#content-top-left img").attr("src",src);
			 			$(".img").show();
		 			}else{
		 				$(".img").hide();
		 			}
		 			
		 			name=resume[0].candidateName;
		 			
		 			//名字
		 			$("#name").text(resume[0].candidateName);
		 			//性别
		 			$("#sex").text(sexstatus(resume[0].gender));
		 			//学历
		 			$("#degree").text(educationstatus(resume[0].degree));
		 			
		 			//出生日期
		 			$("#birthDate").text(resume[0].birthDate);
		 			$("#birthDate-age").text(getAge(resume[0].birthDate));
		 			//现居地
		 			$("#address").text(resume[0].address);
		 			//电话
		 			$("#phone").text(resume[0].candidatePhone);
		 			//邮箱
		 			$("#email").text(resume[0].candidateMail);
		 			
		 			//身份证
		 			$("#identityNo").text(resume[0].identityNo);
		 			//户口所在地
		 			$("#birthplace").text(resume[0].birthplace);
		 			
	 				var weig=resume[0].weight;//体重
	 				var heig=resume[0].height;//身高
		 			if(resume[0].height=="" || resume[0].height==null){
		 				//身高
		 				heig="";
		 			}
					if(resume[0].weight=="" || resume[0].weight==null){
		 				//体重
		 				weig ="";
		 			}
	 				//身高体重
	 				$("#heightAndWeight").text(heig+"/"+weig);
		 			
		 			//紧急联系人
		 			$("#emergencyContact").text(resume[0].emergencyContact);
		 			//联系人关系
		 			$("#emergencyRelationship").text(resume[0].emergencyRelationship);
		 			//联系人电话
		 			$("#emergencyPhone").text(resume[0].emergencyPhone);
		 			//毕业学校
		 			$("#college").text(resume[0].college);
		 			//专业
		 			$("#major").text(resume[0].major);
		 			//英语等级
		 			$("#english_level").text(englishLevel(resume[0].englishLevel));
		 			//毕业时间
		 			$("#graduationDate").text(resume[0].graduationDate);
		 			
		 			
		 			//职位
		 			if(position.length>0){
		 			
			 			var html="";
			 			//循环职位
			 			for(var i=0;i<position.length;i++){
			 				html+=position[i].position_name+"，";
			 			}
			 			html=html.substring(0,html.length-1);
			 			$("#position").text(html);
		 			}
		 			//期望工作城市
		 			if(resume[0].exceptedWorkAreaA==null || resume[0].exceptedWorkAreaA==""){
		 				
		 			}else{
		 				var exceptedWorkAreaA=resume[0].exceptedWorkAreaA.replace(/,/g,"，");
			 			//期望工作城市
				 		$("#expected-address-One").text(exceptedWorkAreaA);
			 			
		 			}
		 			
		 			
		 			
		 			//期望薪资
		 			$("#expected-salary").text(resume[0].expectedSalary);
		 			//得知招聘信息的渠道
		 			if(resume[0].recruitNoticedChannelName!="" && resume[0].recruitNoticedChannelName!=null){
		 				$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel)+"—"+resume[0].recruitNoticedChannelName);
		 			}else{
			 			$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel));
		 			}
		 			//是否服从工作职位的调配
		 			$("#isPositionArrange").text(is_positionArrange(resume[0].isPositionArrange));
		 			//是否服从工作城市的调配
		 			$("#is-workplace-arrange").text(is_workplaceArrange(resume[0].isWorkplaceArrange));
		 			
		 			
		 			//个人履历 start
		 			var html="";
		 			html+='<caption class="caption">个人履历</caption>';
		 			html+='<thead><tr><th>是否有补考或重修记录</th><th>是否受过处分</th></tr></thead>';
		 			html+='<tbody><tr>';
		 			if(resume[0].retestTimes==null || resume[0].retestTimes==""){
		 				html+='<td id="is-retest">无</td>';
		 			}else{
		 				html+='<td id="is-retest">'+resume[0].retestTimes+' 次</td>';
		 			}
		 			if(resume[0].punishmentTimes==null || resume[0].punishmentTimes==""){
		 				html+='<td id="is-punished">无</td>';
		 			}else{
		 				html+='<td id="is-punished">'+resume[0].punishmentTimes+' 次</td>';
		 			}
		 			html+='</tr></tbody>';
		 			$("#personalResume").append(html);
		 			//end
		 			
		 			//个人履历之在校实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html2="<caption class='caption2'>在校实践</caption>" 
			 			for(var i=0;i<resume_school.length;i++){
			 			
			 				var startTime=resume_school[i].startTime;//开始时间
			 				var endTime=resume_school[i].endTime;//结束时间
			 				var practiceName=resume_school[i].practiceName;//单位名称
			 				var practiceDetails=resume_school[i].practiceDetails;//实习内容
			 				
			 				if(startTime==null || startTime=="" || startTime=="null"){
			 					startTime="";
			 				}
			 				if(endTime==null || endTime=="" || endTime=="null"){
			 					endTime="";
			 				}
			 				if(practiceName==null || practiceName=="" || practiceName=="null"){
			 					practiceName="";
			 				}
			 				if(practiceDetails==null || practiceDetails=="" || practiceDetails=="null"){
			 					practiceDetails="";
			 				}
			 				
			 				html2+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				
			 				html2+='<td id="">'+startTime+' ~  '+endTime+'</td>';
			 				html2+='<td id="">'+practiceName+'</td>';
			 				html2+='</tr></tbody>';
			 				html2+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="" colspan="2">'+practiceDetails+'</td>';
			 				html2+='</tr></tbody>';
			 			}
			 			$("#recruitSchoolPractice").append(html2);
		 				
		 			}
		 			//end
		 			
		 			//个人履历之社会实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html3="<caption class='caption2'>社会实践</caption>" 
			 			for(var i=0;i<resume_work.length;i++){
			 			
			 				var startTime=resume_work[i].startTime;//开始时间
			 				var endTime=resume_work[i].endTime;//结束时间
			 				var experienceName=resume_work[i].experienceName;//单位名称
			 				var experienceContent=resume_work[i].experienceContent;//实习内容
			 				
			 				if(startTime==null || startTime=="" || startTime=="null"){
			 					startTime="";
			 				}
			 				if(endTime==null || endTime=="" || endTime=="null"){
			 					endTime="";
			 				}
			 				if(experienceName==null || experienceName=="" || experienceName=="null"){
			 					experienceName="";
			 				}
			 				if(experienceContent==null || experienceContent=="" || experienceContent=="null"){
			 					experienceContent="";
			 				}
			 				
			 				html3+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="">'+startTime+' ~  '+endTime+'</td>';
			 				html3+='<td id="">'+experienceName+'</td>';
			 				html3+='</tr></tbody>';
			 				html3+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="" colspan="2">'+experienceContent+'</td>';
			 				html3+='</tr></tbody>';
			 			}
			 			$("#recruitWorkExperience").append(html3);
		 			}
		 			//end
		 			
		 			
		 			//附件
		 			//判断如果returnAcc对象长度小于等于0时不做操作
  				 	if(resume_axccessory.length <= 0){
						
					}else{
						for(var i=0;i<resume_axccessory.length;i++){
							var accessoryUrl1=resume_axccessory[i].accessoryUrl;//简历地址
							var accessoryUrl=basePath+resume_axccessory[i].accessoryUrl;//拼接附件路径
							var accessoryName=resume_axccessory[i].accessoryName;//简历名称
							//把简历放在所对应的DIV里面
							$("#file_upload").append("<p id='recruitAcc"+resume_axccessory[i].id+"' class='recruitAcc'>"
									+"<a href="+accessoryUrl+"  target='_blank'  title='"+accessoryName+"'>"+accessoryName+"</a></p>");
							resumeUrl+=accessoryUrl1+",";
							resumeName+=accessoryName+",";
						}
					}
					
					
		 		}
		 	})
		 }
		 
		 //点击下载附件需要进行的方法
    	function preview2(){
    		if(resumeUrl=="" || resumeUrl==null){
    			
    		}else{
    			resumeUrl=resumeUrl.substring(0,resumeUrl.length-1);//去掉最后一个字符
				resumeName=resumeName.substring(0,resumeName.length-1);//去掉最后一个字符
    			resumeUrl2=resumeUrl.split(",");//转换成数组
    			resumeName2=resumeName.split(",");//转换成数组
    			//循环url
    			for(var i=0;i<resumeUrl2.length;i++){
    				//循环文件名
    				for(var j=i;j<resumeName2.length;j++){
    					//简历下载
    					download(resumeUrl2[i],resumeName2[j]);
    					break;
    				}
    			}
    			
    		}
    	}
    	
    	//简历下载
    	function download(href, title) {
		    const a = document.createElement('a');
		    href=basePath+href;
		    a.setAttribute('href', href);
		    a.setAttribute('download', title);
		    a.click();
		};
		
		/*
		 * 下载PDF
		 */
		 function downPDF(){
		
			$("#print").hide();
    		$("#main-right").hide();
    		$("#main-left").css("width","100%");
    		$("#content-top-left caption").css("margin-top","0px"); 
    		$("#content-top-left").css("width","60%");
    		$(".caption").css('color','#000');
    		
    		$("#recruitSchoolPractice caption, #recruitWorkExperience caption").css('color','#000');
    		$("#pdf").css("font-size","18px");
    		$("#content-top-left caption").css("font-size","34px");
    		$(".caption").css("font-size","24px");
    		$("#recruitSchoolPractice caption, #recruitWorkExperience caption").css("font-size","20px");
    		 
    		$("#main-left").css("background","url(<%= request.getContextPath() %>/campus/society_recruit/img/索菲亚家居水印.png)");
    		$("#main-left").css("background-position","center center");
    		$("#main-left").css("background-repeat","repeat-y"); 
    		
    		$(".caption").removeClass("caption");
    		$(".caption2").removeClass("caption2");
    		
			//要转成PDF的标签的范围。
		    html2canvas($('#pdf'), {  
			    onrendered: function(canvas) {           
			        
			        var contentWidth = canvas.width;
	                  var contentHeight = canvas.height;
	
	                  //一页pdf显示html页面生成的canvas高度;
	                  var pageHeight = contentWidth / 592.28 * 841.89;
	                  //未生成pdf的html页面高度
	                  var leftHeight = contentHeight*1.5-20;
	                  //pdf页面偏移
	                  var position = 0;
	                  //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
	                  var imgWidth = 595.28;
	                  var imgHeight = 592.28/contentWidth * contentHeight;
	
	                  var pageData = canvas.toDataURL('image/png');
	
	                  var pdf = new jsPDF('p', 'pt', 'a4');
	
	                  //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
	                  //当内容未超过pdf一页显示的范围，无需分页
	                  if (leftHeight < pageHeight) {
	                      pdf.addImage(pageData, 'JPEG', -20, 0, imgWidth+40, imgHeight*1.5-20);
	                  } else {
	                      while(leftHeight > 0) {
	                          pdf.addImage(pageData, 'JPEG', -20, position, imgWidth+40, imgHeight*1.5-20)
	                          leftHeight -= pageHeight;
	                          position -= 841.89;
	                          //避免添加空白页
	                          if(leftHeight > 0) {
	                              pdf.addPage();
	                          }
	                      }
	                  }
			        pdf.save(name +"-"+ new Date().getTime() + '.pdf');
			        setTimeout(function(){  //使用  setTimeout（）方法设定定时1000毫秒
						location.reload();//页面刷新
					});  
			    }  
			});  
		} 
		
		//查询评估表
		function selectTemplate(){
			$.ajax({
	  			url:"com.recruit.sociology_recruit.sociology_recruit.queryTemplateRecord.biz.ext",
	  			type:"POST",
	  			datatype: "json",
	  			data:{
	  				resume_id:resumeID
	  			},
	  			success:function(data){
	  				console.log(data);
	  				$(".assess").text("");
	  				var htmlHead ="";
	  				var htmlinterview ="";
	  				var count =0; //标记总数量
	  				for(var i=0;i<data.data.length;i++){//循环候选人数据集
	  					for(var k=0;k<data.RecruitTemplateRecord.length;k++){//循环候选人评估数据
	  						if(data.data[i].id == data.RecruitTemplateRecord[k].candidate_id){//判断候选人的工单有存在面试评估表
	  							htmlHead ="<div  id=assess_"+data.data[i].id+" ><span class='assesstext'>工单号："+data.data[i].recuitOrderId+"</span></div>";
	  							$(".assess").append(htmlHead);//添加工单文本
	  							count++;
	  						  break;
	  						}
	  					}
	  				}
	  				for(var j=0;j<data.interviews.length;j++){
	  					htmlinterview ="<div style='width: 460px;margin-bottom: 10px;' id=interview_"+data.interviews[j].id+" ><span class=''>面试官："+data.interviews[j].interviewerName+"</span><span style='display: inline-block;float: right;'>面试时间："+getDateTime(data.interviews[j].actualInterviewTime)+"</span></div>";
	  					$("#assess_"+data.interviews[j].id).append(htmlinterview);
	  				}
	  				$(data.RecruitTemplateRecord).each(function(i,obj){
	  					var url="";
	  					var html='<div class="panel panel-info" style="width: 460px;margin-bottom: 10px;">';
	  					html+='<div class="panel-heading">';
	  					html+='<h3 class="panel-title">面试评估表</h3>';
	  					html+='</div>';
	  					html+='<div class="panel-body">';
	  						url=server_context+"/process/recruit_template/template_update.jsp?id="+obj.id+"&entry=1&sign=g"; 
	  						html+='<span>人资总分：<span>'+obj.hr_score+'</span></span><span style="margin-left: 100px;">部门总分：<span>'+obj.org_score+'</span></span><a style="float: right;color:#337dbc;margin-right: 10px;"href="'+url+'"  target="_Blank">查看</a></div>';
	  					html+='</div>';
	  					$("#assess_"+obj.candidate_id).append(html);
	  					
				  	})
					$("#main-left").css("height",365+data.RecruitTemplateRecord.length*82+25*count+data.interviews.length*21);//左页面的高度
				  		
	  			}
	  		});
		}
	</script>
</body>
</html>
