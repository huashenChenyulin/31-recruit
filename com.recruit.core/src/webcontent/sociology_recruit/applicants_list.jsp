<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
	<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-10-15 08:52:08
  - Description:
-->
<head>
<!-- （社招列表） -->
<title>应聘者列表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.css">
    
    <%
    	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
    	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
     %>
    <style type="text/css">
	 /*报表的数据高度*/
    .layui-table-view{
		height: 323px!important;
	}
	.layui-table-main{
		height: 283px!important;
	}
	.title {
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	.table1 td{
		line-height: 3;
	}
	.table1 label{
		width: 85px;
	}
	
	/* 查看简历 */
	.fa{
		cursor: pointer;
		color: #f0ad4e;
		font-size: 18px!important;
	}
	.table-striped th,.table-striped td{
		text-align: center;
	}
	
</style>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="display:none;">
			  <legend>社招列表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;" class="table1">
					<tr>
						<td>
							<label>姓名：</label>
							<input class="form-control" id="candidate_name" style="width: 170px;">
								
						</td>
						<td>
							<label>学历：</label>
							<select id="degreeText" class="form-control" style="width: 170px;">
								
							</select>
						</td>
						<td>
				      		<label>申请职位：</label>  
					        <input class="form-control" id="position_name" style="width: 170px;">
						</td>
						<td>
				      		<label style="width:114px;">所属公司：</label>  
					        <select id=companyz_name class="form-control" style="width: 170px;">
					        </select>
						</td>
						
						
					</tr>
					<tr>
						<td>
							<label>意向工作地：</label> 
							<input id="excepted_work_area" class="form-control" data="" style="width: 170px;">
						</td>
						<td>
							<label>现居地：</label> 
							<input id="address" class="form-control" data="" style="width: 170px;">
						</td>
						<td>
				      		<label>工作年限：</label>  
					        <input class="form-control" id="allDayText" style="width: 170px;">
						</td>
						<td>
				      		<label style="width:114px;">上一家公司名称：</label>  
					        <input class="form-control" id="experience_name" style="width: 170px;">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<span style="display: inline-block;width: 25px;"></span>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr2()">重置</button>
							<!-- <button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>  -->
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
			
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	
	
	<!-- 学历-->
	<script type="text/html" id="degree">
		{{ '<div class="title" title="'+educationstatus(d.degree)+'">'+educationstatus(d.degree)+'</div>' }}
	</script>
	
	<!-- 年龄-->
	<script type="text/html" id="birth_date">
		{{ '<div class="title" title="'+getAge(d.birth_date)+'">'+getAge(d.birth_date)+'</div>' }}
	</script>
	

	
	<!-- 所属公司 -->
	<script type="text/html" id="company_id">
		{{ '<div class="title" title="'+convertCompanyId(d.company_id)+'">'+convertCompanyId(d.company_id)+'</div>' }}
	</script>
	
	<!-- 操作-->
	<script type="text/html" id="operation">
		<i class="fa fa-address-card-o" title="查看简历" onclick="query_resume('{{d.resume_id}}')" ></i>
		<span style="font-size: 20px;margin-left: 10px;margin-right: 10px;">|</span>
		<i class="fa fa-reorder" title="查看所在工单" onclick="candidate_open('{{d.resume_id}}')" ></i>
		<span style="font-size: 20px;margin-left: 10px;margin-right: 10px;">|</span>
		<i class="fa fa-plus" title="添加到我的工单" onclick="addmyorder('{{d.resume_id}}')" ></i>
		
	</script>
	
<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 5; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var companyId ="";
	
	//招聘负责人id
	var empid=<%=empid %>;
	//招聘负责人姓名
	var empname="<%=empname %>";
	
	$(function(){
	 	
	 	//根据招聘负责人查询所属公司
		selectCompany();
	 	//默认查询报表数据
	 	init_table();
	 	
	});
	
	
	function init_table(candidate_name,degreeText,position_name,companyz_name,excepted_work_area,address,allDayText,experience_name){
		console.log(candidate_name+degreeText+position_name+companyz_name+excepted_work_area+address+allDayText+experience_name);
		var company_id ="";
		if(companyz_name!=null&&companyz_name!=""){
			company_id = companyz_name;
		}else{
			company_id=companyId;
		}
		var json = {
		"curr":page,
		"limit":limit,
		"company_id":company_id, //所属公司
		"candidate_name":candidate_name, //姓名
		"degreeText":degreeText, //学历
		"position_name":position_name, //职位
		"excepted_work_area":excepted_work_area, //意向工作地
		"address":address, //现居地
		"allDayText":allDayText, //工作年限
		"experience_name":experience_name //上一家公司名称
		};
       	$.ajax({
       		url: 'com.recruit.sociology_recruit.sociology_recruit.querySociologyApplicantsList.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.applicants_list
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                 {field:'company_id', width:'13%', title: '所属公司',align:'center',templet:'#company_id',total:true}
		                ,{field:'candidate_name', width:'8%', title: '姓名',align:'center',total:true}
		                ,{field:'gender', width:'8%', title: '性别',align:'center',total:true}
		                ,{field:'birth_date', width:'8%',title: '年龄' ,align:'center',templet:'#birth_date',total:true}
		                ,{field:'candidate_phone', width:'12%', title: '电话',align:'center',templet:'#',sort: true ,total:true}
		                ,{field:'candidate_mail', width:'14%', title: '邮箱',align:'center',templet:'#',sort: true ,total:true}
		                ,{field:'degree', width:'15%', title: '学历',align:'center',templet:'#degree' ,total:true}
		                ,{field:'position_name', width:'15%', title: '申请职位',align:'center',templet:'#' ,total:true}
		                ,{field:'address', width:'15%', title: '现居地',align:'center' ,total:true}
		                ,{field:'excepted_work_area', width:'15%', title: '意向工作地',align:'center' ,total:true}
		                ,{field:'allDay', width:'8%', title: '工作年限',align:'center',templet:'#allDay',total:true}
		                ,{field:'experience_name', width:'15%', title: '上一家公司名称',align:'center' ,total:true}
		                ,{field: 'detailTotalScores', title: '操作',width:'12%',unresize:true,templet:'#operation',event:'setSign',fixed:'right',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						
					}
					,total:true
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
		    	
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count
				    ,total:true
				    ,curr : page
				    ,limit:limit
				    ,limits : [5, 10, 20, 30, 50,data.PageCond.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				     	 init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	var candidate_name =$("#candidate_name").val();//姓名
    	var degreeText =$("#degreeText").val();//学历
    	var position_name =$("#position_name").val();//申请职位
    	var companyz_name =$("#companyz_name").val();//所属公司
    	var excepted_work_area =$("#excepted_work_area").val();//意向工作地
    	var address =$("#address").val();//现居地
    	var allDayText =$("#allDayText").val();//工作年限
    	var experience_name =$("#experience_name").val();//上一家工作名称
    	init_table(candidate_name,degreeText,position_name,companyz_name,excepted_work_area,address,allDayText,experience_name);
    }
  
  	//查看简历
  function query_resume(id){
	  //跳转查看简历
	  window.open("<%= request.getContextPath() %>/task/sociology_recruit/resume_select.jsp?resumeId="+id);
  }
  
  	
  	//加载添加工单框
	function addmyorder(resume_id){
   		event.stopPropagation();//阻止事件冒泡即可
		
		layer.open({
			title: '添加到我的工单',
			type:1,
			content: '<div id="content_top" style="margin-right: 25px;">'+
				'<label style="margin-left: 22px;margin-top: 20px;">公司</label>'+
				'<select class="form-control" id="company" name="company" style="width: 230px;margin-left: 70px;margin-top: -29px;">'+
				'<option></option>'+
				'</select>'+
				'</div>'+
				'<div id="content_centre" style="margin-right: 25px;">'+
				'<label style="margin-left: 22px;margin-top: 20px;">职位</label>'+
				'<select class="form-control" id="positions" name="positions" style="width: 230px;margin-left: 70px;margin-top: -29px;">'+
				'<option></option>'+
				'</select>'+
				'</div>'+
				'<div id="content_set">'+
				'<label style="margin-left: 22px;margin-top: 20px;">单号</label>'+
				'<select class="form-control" id="supplier" name="supplier" style="width: 230px;margin-left: 70px;margin-top: -29px;">'+
				
				'</select></div>'
			,btn: ['添加','关闭']
			,yes: function(index, layero){
				// 通过ID查询当前人才所在工单
				var orderid=$("#supplier").val();//工单号
		  		 selectOrderByid(resume_id,orderid);
		  		 
		  		 
		    }
		});
		//根据招聘负责人查询所属公司
		selectCompany();
		
		//所属公司值改变事件
		$("#company").change(function(){
			// 查询职位
			select_position($(this).val());
		})
		
		//职位值改变事件
		$("#positions").change(function(){
			//根据职位查询工单
			officeId($(this).val());
		})
 	}
  	
  	//查询已在工单弹出框
	function candidate_open(id){
   		event.stopPropagation();//阻止事件冒泡即可
		
		layer.open({
			title: '查询已在工单'
			,type:1
			,area:['475px','340px']
			,content: '<table class="table table-striped" style="margin-top: 10px">'+
						'<thead>'+
							'<tr>'+
								'<th>已在工单列表</th>'+
								'<th>招聘负责人</th>'+
								'<th>候选人状态</th>'+
							'</tr>'+
						'</thead>'+
						'<tbody id="tt">'+
							
						'</tbody>'+
					 '</table>'
			,btn: ['关闭']
		});
		
		//查询已在工单列表
		select_candidate(id);
 	}
  	
  	//查询已在工单列表
	function select_candidate(id){
		var json={
			resume_id:id,
		};
		$.ajax({
			url:"com.recruit.sociology_recruit.sociology_recruit.queryCandidate.biz.ext",
  			type:"POST",
  			data:json,
  			success:function(data){
  			
  				var candidate="";
  				if(data.result.length>0){
	  				//循环拼接工单号、招聘负责人、目前所在环节
	  				for(var i=0;i<data.result.length;i++){
		  				var candidateStatus= candidatestatus(data.result[i].candidateStatus);
		  				//工单号
		  				var recuitOrderId = data.result[i].recuitOrderId;
	  					candidate+="<tr><td>"+recuitOrderId+"</td>"; 
	  					candidate+="<td>"+data.result[i].recruiterName+"</td>";
	  					candidate+="<td>"+candidateStatus+"</td</tr>"; 
	  				}
  				}else{
  					candidate+='<tr><td colspan="3" style="color: #999;">无</td></tr>';
  				}
  				
  				$("#tt").append(candidate);
  			}
  		});
	}
  	
	//根据招聘负责人查询所属公司
	function selectCompany(){
		var cTime=document.getElementById("degreeText");//清除下拉框内容
			cTime.options.length=0;
		var json={
			empId:empid
		}
		$.ajax({
			url:"com.recruit.sociology_recruit.sociology_recruit.queryCompanyByEmpId.biz.ext",
  			type:"POST",
  			data:json,
  			async:false,
 			success:function(data){
 				companyId ="";
  				$("#companyz_name")[0].length=1;
  				//循环输出值并拼接到对应的位置
  				for(var i=0;i<data.company.length;i++){
  					companyId += data.company[i].company_id +',';
  					$("#company").append("<option value='"+data.company[i].company_id+"'>"+data.company[i].company_name+"</option>"); 
  					
  					//搜索栏所属公司
  					$("#companyz_name").append("<option value='"+data.company[i].company_id+"'>"+data.company[i].company_name+"</option>"); 
  				}
  				//学历循环拼接赋值
					$("#degreeText").append("<option value=''></option>");
				for (var i = 0; i < data.education.length; i++) {
					$("#degreeText").append(
						"<option value='"+data.education[i].dictID+"'>"
								+ data.education[i].dictName
								+ "</option>");
					}
  					companyId = companyId.substring(0,companyId.length-1);
  			}
  		});
	}
  	
  //查询职位
	function select_position(company){
	var cTime=document.getElementById("positions");//清除下拉框内容
			cTime.options.length=0;
		var json={
				empid:empid,//当前登录人id
				company:company//所属公司编号
		};
		$.ajax({
			url:"com.recruit.talent.talent.selectAllPosition.biz.ext",
  			type:"POST",
  			data:json,
  			success:function(data){
				$("#positions").append("<option value=''></option>"); 
  				if(data.result.length!=0){
	  				//循环输出值并拼接到对应的位置
	  				for(var i=0;i<data.result.length;i++){
	  					$("#positions").append("<option>"+data.result[i].position+"</option>"); 
	  				}
  				}else{
	  				$("#positions").append("<option value=''>此公司没有职位，请重新选择</option>"); 
  				}
  			}
  		});
	}
	
	//根据职位查询工单
	function officeId(position){
		var cTime=document.getElementById("supplier");//清除下拉框内容
			cTime.options.length=0;
		if(position!=""&&position!=null){
			var json={
				position:position,
			};
			$.ajax({
				url:"com.recruit.talent.talent.selectOfficeId.biz.ext",
	  			type:"POST",
	  			data:json,
	  			success:function(data){
	  				var result =data['result'];
	  			//循环输出值并拼接到对应的位置
	  				for(var i=0;i<result.length;i++){
	  					$("#supplier").append("<option>"+result[i].recuitOrderId+"</option>"); 
	  				}
	  			}
	  		});
  		}
	}
	
	
	//通过ID查询当前人才所在工单
	 function selectOrderByid(resume_id,orderid){
		 validator();
		//非空判断
 		if(isPass==false){
 			layer.msg(isPassMsg);
 			return;
 		}
		var json1 = {
				resume_id:resume_id,//简历id
				orderid:orderid //工单号
			};
			$.ajax({
				url:"com.recruit.sociology_recruit.sociology_recruit.selectORderexist.biz.ext",
				type:'POST',
				data:json1,
				success:function(data){
					//标识是否已经添加到工单，1已添加，其他未添加
					if(data.data.length != 0){
						layer.msg('已加入工单，请勿重复操作');
						return;
					}else{
						//添加到我的工单
			    		insertCandidate(resume_id,orderid);
				 	}
				 }
			});
	  }
	
	//添加到工单表
	function insertCandidate(resume_id,orderid){
		var json={
			orderid : orderid,//工单id
			resumeId : resume_id//简历id
		};
		$.ajax({
			url:"com.recruit.sociology_recruit.sociology_recruit.addCandidate.biz.ext",
  			type:"POST",
  			data:json,
  			success:function(data){
	  			if(data.MSG_TYPE!="E"){
	  				layer.closeAll();
	  				layer.msg('添加成功');
	  			}else{
	  				layer.msg('添加失败，请联系管理员');
	  			}
  			}
		});
	}
	
	var isPass=true;//标识是否通过验证
	var isPassMsg="";//非空验证提示输出
	
   	//失去焦点验证
   	function validator(){
   		isPassMsg="";
   		isPass=true;
   		//公司
  		if($('#company').val()==null || $('#company').val()=="" || $('#company').val()==0){
           	$("#company").css("border","1px solid red");
           	isPassMsg+="请选择公司";
           	isPass=false;
           	return;
         }else{
           	$("#company").css("border","1px solid #ccc");
         }
   		//职位
  		if($('#positions').val()==null || $('#positions').val()=="" || $('#positions').val()==0){
           	$("#positions").css("border","1px solid red");
           	isPassMsg+="请选择职位";
           	isPass=false;
           	return;
         }else{
           	$("#positions").css("border","1px solid #ccc");
         }
   		
   		//工单号
  		if($('#supplier').val()==null || $('#supplier').val()=="" ){
           	$("#supplier").css("border","1px solid red");
           	isPassMsg+="请选择工单";
           	isPass=false;
           	return;
         }else{
           	$("#supplier").css("border","1px solid #ccc");
         }
   	} 
  
     //报表的重置方法
    function resetRecr2(){
    	document.getElementById("form1").reset();
    	init_table();
    }
    
    //所属公司转换文本
    function convertCompanyId (companyId){
     //所属公司转换文本值
    	var value ="";
    	 //查询所属公司
      	 $.ajax({
      		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
      		 type:'POST',
      		 cache: false,
      		 async:false,
      		 success:function(data){
      		 	 var array = companyId.split(",");
      		 	 for(var k=0;k<array.length;k++){
      			 	for(var i=0;i<data.data.length;i++){
      			 		if(data.data[i].companyId==array[k]){
      			 			value += data.data[i].companyName+",";
      			 			break;
      			 		}
      			 	}
      			}
      		}
      	 });
      	 value = value.substring(0,value.length-1);
      	 return value;
    }
    
    
    
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>