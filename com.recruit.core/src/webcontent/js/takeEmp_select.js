	var abandonEmpId =parent.abandonEmpId;//获取当前进来的招聘负责人ID
	
	//加载事件
    window.onload=function() {
    	var json = {"abandonEmpId":abandonEmpId};
      		search(json);
      	if(parent.updateBool==true){//为true更改招聘负责人页面显示备注信息
      		$(".p-text").show();
      	}
    };
    
     //重置信息
    function reset(){
        $(".import").each(function(i,obj){
            obj.value="";
        });
    }
    
    //赋值负责人信息
    function backPrice(){
    	var parameter = $("#emp").val();//emp：JSON格式
    	return parameter;//返回给父页面
    }
    
    //查询信息
    function searchEmp(){
	    var empid = $("#empid").val();//招聘负责人ID
	    var empname = $("#empname").val();//招聘负责人姓名
	    var json = {"empid":empid,"empname":empname,"abandonEmpId":abandonEmpId};
	    	search(json);//查询方法
    }
    
	//查询
    function search(json){
    	 $.ajax({
           	url: basePath+'com.primeton.task.taskoperation.selectHrEmp.biz.ext',
			type:'POST',
			cache: false,
			data:json,
			success:function(data){
				var emp = {};
				layui.use('table', function(){
           			var table = layui.table;
				  	//第一个实例
				 	table.render({
					   id: 'idTest'
					   ,elem: '#demoEmp'
					   ,height: 280 //高度
					   ,data:data.data //数据集
					   ,page: true //开启分页
					   ,layout: [ 'prev', 'page', 'next','skip']
					   ,limit:5//分页显示条数
					   ,limits:[5]
					   ,cols: [[ //表头
					     {field: 'empid', title: 'ID',event: 'setSign',width:'30%',sort: true}
					     ,{field: 'empname', title: '招聘负责人姓名',event: 'setSign',width:'30%'}
					     ,{field: 'orgname', title: '所属部门',event: 'setSign',templet:'#tempOrgname'}
					   ]]
				 	   ,done:function(res, curr, count) {
				 		   var data = res.data;
							$('.layui-table-body tr').each(function() {
								var dataindex = $(this).attr('data-index');
								var idx = 0;
								for ( var item in data) {
									if (dataindex == idx) {
										$(this).dblclick(function() {//双击某一行事件
											datas =data[item];//获取当前选中的数据
											emp.id = datas.empid;//获取招聘负责人ID
											emp.name = datas.empname;//获取招聘负责人姓名
											var json = JSON.stringify(emp);//转换JSON格式
											var objJson =$.parseJSON(json);
											if(parent.updateBool==true){//为true表示为修改招聘负责人点进来
												current =parent.current;//获取父层当前点击的元素
												parent.updateRecruiterName(current,objJson);//调用父层修改招聘负责人方法
											}else{
												$(parent.current).val(objJson.name);
						                    	$(parent.current).attr("data",objJson.id);
						                    	parent.layer.closeAll();
											}
										});
										break;
									}
									idx++;
								}
							});
						} 
				  	});
					table.on('tool(test)', function(obj){
						var data = obj.data;
							if(obj.event === 'setSign'){
								emp.id = data.empid;//获取招聘负责人ID
								emp.name = data.empname;//获取招聘负责人姓名
								var str = JSON.stringify(emp);//转换JSON格式
								$("#emp").val(str);
						    }
					});
				});
			}
   	 	});
    }
    
    //回车事件
    document.onkeydown = function(e){
        var ev = document.all ? window.event : e;
        if(ev.keyCode==13) {
        	searchEmp();//查询
        }
    };
  
    