		var parentdata =parent.datas;//获父页面转来的值
		var isMannulId =parentdata.isMannulId;//1、口头单0、系统单
		var recuit_orderId="";//已关联工单号
		console.log(parentdata.company);
		// 页面加载
		window.onload=function(){
			$("#id").val(parentdata.id);//id
			$("#recruiterName").text(parentdata.recruiterName);//招聘负责人
			$("#roStatus").text(rostatus(parentdata.roStatus));//招聘状态
			$("#offerTime").text(getDate(parentdata.offerTime));//发送OFF时间
			$("#surplusPeriod").text(parentdata.surplusPeriod+"天");//剩余招聘周期
			$("#orgName").text(formatDepartment(parentdata.l1Name,parentdata.l2Name,parentdata.l3Name,parentdata.l4Name));//部门
			$("#orgName").attr("title",formatDepartment(parentdata.l1Name,parentdata.l2Name,parentdata.l3Name,parentdata.l4Name));//悬浮部门值
			$("#position").text(parentdata.position);//招聘职位
			$("#mainReason").val(parentdata.mainReason);//招聘原因
			$("#auditTime").text(getDate(parentdata.auditTime));//审批日期
			$("#oaProcessId").text(parentdata.reqOaprocessid);//OA流程号
			$("#recruitPeriod").val(parentdata.recruitPeriod);//招聘周期
			$("#isDifficult").val(parentdata.isDifficult);//是否难招
			$("#isIntern").val(parentdata.isIntern);//是否实习生
			$("#categoryAttribute").val(parentdata.categoryAttribute);//职位属性
			
			var json={orderId:parentdata.recuitOrderId};//工单号
			 $.ajax({
          		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
          		 type:'POST',
          		 cache: false,
          		 async:false,
          		 success:function(data){
          			 for(var i=0;i<=data.data.length;i++){
          				 if(parentdata.company==data.data[i].companyId){
          					$("#company").text(data.data[i].companyName);//所属公司
          					$("#company").attr("title",data.data[i].companyName);//所属公司
          					 break;
          				 }
          			 }
          		 }
          	 });
			loadCandidate(json);//加载候选人
			
			//加载职位类别数据
			 $.ajax({
				//调用查询职位类别逻辑流
	          	url: basePath+'com.primeton.task.taskoperation.selectCategory.biz.ext',
				type:'POST',
				cache: false,
				async:false,
				success:function(data){
					for(var i=0;i<data.data.length;i++){//循环职位类别值
						//追加职位类别给下拉框
						$("#jobCategory").append('<option value="'+data.data[i].dictID+'">'+data.data[i].dictName+'</option>');
					}
				}
	         });
			 
			 //加载已关联工单数据
			 $.ajax({
				 //调用查询工单逻辑流
				 url: basePath+'com.primeton.task.taskoperation.selectOrder.biz.ext',
				 type:'POST',
				 data:{"relevance_order_id":parentdata.relevanceOrderId},
				 cache: false,
				 async:false,
				 success:function(data){
					 $("#relevanceauditTime").text(getDate(data.data[0].auditTime));
					 $("#relevance_order_id").attr("dataId",(data.data[0].id));
				 }
			 });
			 if(isMannulId==0){//系统单隐藏已关联元素
				 $("#relevance_order_id").css("color","#333333");//隐藏已关联
				 $("#relevance_order_id").css("cursor","context-menu");//隐藏已关联
				 $("#relevance_order_id").css("text-decoration","none");//隐藏已关联
				 $("#relevance_order_id").removeAttr("onclick");//隐藏已关联
			 }
			 
			 $("#jobCategory").val(parentdata.jobCategory);//职位类别
			 if(null==parentdata.relevanceOrderId||parentdata.relevanceOrderId==""){//关联工单为空
				 if(isMannulId==0){//系统单数据为空则显示空
					 $("#relevance_order_id").text("");//关联工单
					 $(".rec_span").hide();//隐藏审批日期
				 }else{
					 $("#relevance_order_id").text("请选择");//关联工单
					 $(".rec_span").hide();//隐藏审批日期
				 }
			 }else{
				 $("#relevance_order_id").attr("data",parentdata.relevanceOrderId);//关联工单
				 $("#relevance_order_id").text(parentdata.relevanceOrderId);//关联工单
			 }
		};
		
		//选择关联工单
		function relevanceOrder(oneself){
			recuit_orderId =$(oneself).attr("data");//获取当前点击工单号
			var layer = layui.layer;
		  	 layer.open({
			    area: ['84.3%', '97%'],
				type: 2,
				shade: 0.1,
				closeBtn: false,
				resize:false, //是否允许拉伸
				title:'选择关联工单',
				closeBtn: false,//不显示关闭图标
			    btn: ['选择', '关闭'],
				content: [basePath+'task/relevanceOrder_select.jsp','no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
               yes: function(index,lay){
					var iframeWin = window[lay.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
	                var ids = iframeWin.backPrice();//调用子页面的方法，得到子页面返回的ids 
	               	var json = $.parseJSON(ids);
               	  layer.open({
				        type: 1
				        ,offset:'auto'
				        ,id: 'layerDemo'
				        ,closeBtn: false
				        ,resize:false //是否允许拉伸
				        ,content: '<div style="padding: 20px 50px 0px 50px;">工单号为：'+json.recuitOrderId+'</div><div style="padding: 15px 60px;">关联后不可取消，是否确认关联？</div>'
				        ,btn:['确定','关闭']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index,layero){
	                  		$(oneself).text(json.recuitOrderId); //改变当前点击a标签的text
	                  		$(".rec_span").show();//显示审批日期
	                  		$("#relevanceauditTime").text(getDate(json.auditTime)); //改变工单后的span标签的text
	                  		$(oneself).attr("data",json.recuitOrderId); //改变当前点击a标签的text
	                  			var jsonStr ="";
	                  			if(recuit_orderId!=""){//已有关联工单
	                  				var orderId =$("#relevance_order_id").attr("dataId");//获取原关联工单ID
	                  					jsonStr = {"new_Id":json.id,"old_Id":parentdata.id,"orderId":orderId};
	                  					$("#relevance_order_id").attr("dataId",json.id); //改变工单后的a标签的dataId
	                  			}else{
	                  				jsonStr = {"new_Id":json.id,"old_Id":parentdata.id};
	                  				$("#relevance_order_id").attr("dataId",json.id); //改变工单后的a标签的dataId
	                  			}
	                  				var result=	updaterelevanceOrder(jsonStr); //修改关联工单
	                  				layer.closeAll();//关闭
	                 			 if(result){
	                   				layer.msg("关联成功");
	                   			}else{
	                   				$("#relevanceauditTime").text(""); //改变工单后的span标签的text
	                   				$(".rec_span").hide();//隐藏审批日期
	    	                  		$(oneself).attr("data",""); //改变当前点击a标签的值为空
	    	                  		$(oneself).text(""); //改变当前点击a标签的值为空
	    	                  		layer.msg("关联失败");
	                   			}
				        }
					        ,btn2: function(index,layero){
					        	layer.close(index);//关闭
					        }
		      			}); 
		      		
					}
			});
		}
		 //修改关联工单
        function updaterelevanceOrder(json){
        	var result =false;
             $.ajax({
             	url: basePath+'com.primeton.task.taskoperation.updateRelevanceOrder.biz.ext',
				type:'POST',
				data:json,
				async:false,
				dataType: 'json',
				success:function(data){
					if(data.MSGTYPE!="E"){
						result=true;
					}else{
						result=false;
					}
				}
             });
             return result;
        }
		
		//加载候选人
		 function loadCandidate(json){
	     	$.ajax({
	           url: basePath+'com.primeton.task.taskoperation.queryCandidate.biz.ext',
					type:'POST',
					cache: false,
					data:json,
					success:function(data){
						layui.use('table', function(){
	           			var table = layui.table;
					 	  //第一个实例
						  table.render({
						    id: 'idTest'
						    ,elem: '#demoCandid'
						    ,height: 265
						    ,data:data.data
						    ,page: true //开启分页
						    ,limits: [4, 10, 20]
						    ,limit: 4
						    ,cols: [[ //表头
						      {field: 'recruitResume', title: '候选人姓名',width:'100',templet:'#tempResume'}
						      ,{field: 'recruitChannels', title: '渠道',width:'130'}
						      ,{field: 'recruitSuppliers', title: '供应商',width:'130'}
						      ,{field: 'createdTime', title: '添加候选人日期',width:'128',templet:'<div>{{getDate(d.createdTime)}}</div>'}
						      ,{field: 'offerTime', title: '发送Offer日期',width:'120',templet:'<div>{{getDate(d.offerTime)}}</div>'}
						      ,{field: 'recruitProcessId', title: '当前所在环节',width:'114',templet:'<div>{{Abandonmentstatus(d.recruitProcessId)}}</div>'}
						      ,{field: 'candidateStatus', title: '候选人状态',width:'100',templet:'<div>{{candidatestatus(d.candidateStatus)}}</div>'}
						      ,{field: 'abandonDetails', title: '放弃原因描述',width:'236',templet:'#tempabandonDetails'}
						      ,{field: 'abandonReason', title: '具体放弃情况',width:'450',templet:'#tempabandonReason'}
						    ]]
						  });
						});
					}
	   	 	});
	    }
                
         //获取值返回给父页面修改
		 function returnVal(){
				var id=$("#id").val();//id
		        var recruitPeriod=$("#recruitPeriod").val();//招聘周期
		        var isDifficult=$("#isDifficult").val();//是否难招
				var isIntern=$("#isIntern").val();//是否实习生
				var jobCategory=$("#jobCategory").val();//职位类别
				var mainReason=$("#mainReason").val();//招聘原因
				var categoryAttribute=$("#categoryAttribute").val();//职位属性
				var data={"id":id,"recruitPeriod":recruitPeriod,"isDifficult":isDifficult,"isIntern":isIntern,"auditTime":parent.datas.auditTime,"jobCategory":jobCategory,"mainReason":mainReason,"categoryAttribute":categoryAttribute};
					return data;
         }
                
         //过滤null值
		 function filtrationNull(price){
              	if(price==null||price=='null'){
              		return '';
                }else{
                	return price;
                }
         }
      