		var pageIndex = 1;//当前页数
    	var count;//总页数
    	var limit =7;//显示条数
    	var datas;//获取当前单击/双击选中的值
    	var bool =false;//用来标识是否单击到表格
    	var abandonEmpId ="";//获取当前点击招聘负责人ID
    	var updateBool =false;//用来隐藏选择招聘负责人页面的备注（元素）
    	var current  ="";//点击更改招聘负责人（自身）
        $('#myModal').modal('hide');
        $('#selectemp').modal('hide');	
        	layui.use('laydate', function(){
		  		var laydate = layui.laydate;
		  	//执行一个laydate实例
		 		laydate.render({
		   	 		elem: '#startTime' //指定元素
		  		});
		   		laydate.render({
		    		elem: '#endTime' //指定元素
		  		});
			});
        // 初始化内容	
	    window.onload=function() {
	     	//初始化招聘状态
	     	var html ="";
		     	html+="<option value='1'>招聘中</option>";
		     	html+="<option value='2'>待入职</option>";
		     	html+="<option value='3'>成功入职</option>";
		     	html+="<option value='4'>取消招聘</option>";
		     	html+="<option value='5'>候选人放弃</option>";
		     	html+="<option value='6'>更改招聘负责人</option>";
		     	html+="<option value='7'>已关联</option>";
	        $("#roStatus").append(html);
	        
	       //初始化查询职位类别
          	 $.ajax({
          		url: basePath+'com.primeton.task.taskoperation.selectCategory.biz.ext',
				type:'POST',
				cache: false,
				success:function(data){
					 for(var i=0;i<data.data.length;i++){
						$("#category").append('<option value="'+data.data[i].dictID+'">'+data.data[i].dictName+'</option>');
						$("#addcategory").append('<option value="'+data.data[i].dictID+'">'+data.data[i].dictName+'</option>');
					 }
				}
         	 });
          	 
          	 //初始化查询所属公司
          	 $.ajax({
          		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
          		 type:'POST',
          		 cache: false,
          		 success:function(data){
          			 for(var i=0;i<data.data.length;i++){
          				 $("#company").append('<option value="'+data.data[i].companyId+'">'+data.data[i].companyName+'</option>');
          			 }
          		 }
          	 });
         	 search();//调用查询
          }; 
          
          //添加工单
          function addOrder(){
			var recName = $("#recName").val(); //招聘负责人
			var recruiterId = $("#recName").attr("data"); //招聘负责人ID
			var department = $("#department").val(); //部门
			var addposition = $("#addposition").val(); //需求职位
			var addcategory = $("#addcategory").val(); //职位类别
			var recruitPeriod =$("#recruitPeriod").val();//招聘周期
			var catAttribute =$("#catAttribute").val();//职位属性
			var company =$("#company").val();//所属公司
			var l1 = $("#department").attr("l1"); //中心编号
			var l2 = $("#department").attr("l2"); //部门编号
			var l3 = $("#department").attr("l3"); //科组编号
			var l4 = $("#department").attr("l4"); //科室编号
			var l1name = $("#department").attr("l1name"); //中心名称
			var l2name = $("#department").attr("l2name"); //部门名称
			var l3name = $("#department").attr("l3name"); //科组名称
			var l4name = $("#department").attr("l4name"); //科室名称
				l1name=(l1name!="null")?l1name:null;
				l2name=(l2name!="null")?l2name:null;
				l3name=(l3name!="null")?l3name:null;
				l4name=(l4name!="null")?l4name:null;
				//验证数据
				if(recName==""){
					layer.msg("请选择招聘负责人");
					return;
				}else if(department==""){
					layer.msg("请选择部门");
					return;
				}else if(addposition==""){
					layer.msg("请选择需求职位");
					return;
				}else if(addcategory==""){
					layer.msg("请选择职位类别");
					return;
				}else if(recruitPeriod==""){
					layer.msg("请选择招聘周期");
					return;
				}else if(catAttribute==""){
					layer.msg("请选择职位属性");
					return;
				}else if(company==""){
					layer.msg("请选择所属公司");
					return;
				}
			var json = {recName:recName,recruiterId:recruiterId,l1:l1,l2:l2,l3:l3,l4:l4,addorgcentre:l1name,addorgbranch:l2name,addorgoffice:l3name,addorgsubject:l4name,isVerbal:1,recruitPeriod:recruitPeriod,addposition:addposition,addcategory:addcategory,catAttribute:catAttribute,company:company};
				layer.open({
			        type: 1
			        ,offset:'auto' 
				    ,id: 'layerDemo'
					,resize:false //是否允许拉伸
					,content: '<div style="padding: 20px 75px 0px;">是否创建工单？</div>'
					,closeBtn: false//不显示关闭图标
					,btn:['确定','关闭']
					,btnAlign: 'c' //按钮居中
					,shade: 0.2 //不显示遮罩
					,yes: function(){
					 	$.ajax({
					 		url: basePath+"com.primeton.task.taskoperation.addOrderInfo.biz.ext",
							type: "post",
							data:json,
							cache: false,
							dataType: 'json',
							success: function (json) {
								var mark = json.mark;//返回表示保存成功
							    if(mark != "E"){//判断是否保存成功
								    //清空数据
								   	$('#myModal').modal('hide');
								    $("#recName").val("");//招聘负责人
								    $("#department").val("");//部门
								    var cTime=document.getElementById("addposition");
								    	cTime.options.length=0;//职位
								    $("#addposition").append("<option  value=''>请选择部门</option>");
								    $("#addcategory").val("");//职位类别
								    $("#recruitPeriod").val("");//周期
								    $("#company").val("");//所属公司
								    	isReset();//清空所有查询条件
								  	 	search(1);//添加成功后刷新表格
								  	 	layer.closeAll();
								  	 	layer.msg("创建成功");
								 }else{
								    	layer.msg("创建失败");
								 }
							}
					    }); 
					 }
					 ,btn2: function(index,layero){
						 layer.close(index);//关闭页面
					 }
				});
			}
          
          	//职位类别联动招聘周期
          	function findPeriod(oneself){
          		var categoryID =$(oneself).val();//获取职位类别值
          		$.ajax({
              		url: basePath+'com.primeton.task.taskoperation.queryPeriod.biz.ext',
    				type:'POST',
    				data:{"categoryID":categoryID},
    				cache: false,
    				success:function(data){
    					$("#recruitPeriod").val(data.data);//赋值招聘周期
    				}
             	 });
          		
          	}
			
			//修改工单
			function updateOrderInfo(){
				if(bool){//判断是否点击表格
					if(datas.isMannulId==1){//是否是口头单
						updateOrder(datas.recuitOrderId+'<span class="layui-badge">口头</span>');
					}else{
						updateOrder(datas.recuitOrderId);
					}
				}else{
					 layer.open({//打开子页面
					        type: 1
					        ,offset:'auto' 
					        ,id: 'layerDemo'
					        ,resize:false //是否允许拉伸
					        ,content: '<div style="padding: 20px 75px 0px;">请选中一条数据！</div>'
					        ,btn:['确定']
					        ,closeBtn: false//不显示关闭图标
					        ,btnAlign: 'c' //按钮居中
					        ,shade: 0 //不显示遮罩
					        ,yes: function(){
					        	layer.closeAll();//关闭页面
					        }
					 });
				}
			}
			
			
			//按照条件查询
			function queryOrder(){
				var start_time = $("#startTime").val();//审批开始时间
		   		var end_time = $("#endTime").val();//审批结束时间
		   		var start = new Date(start_time);
		   		var end = new Date(end_time);
		   		if(start>end){//结束时间要大于开始时间
		   			layer.open({
				        type: 1
				        ,offset:'auto' 
				        ,id: 'layerDemo'
				        ,closeBtn: false
				        ,resize:false //是否允许拉伸
				        ,content: '<div style="padding: 20px 75px;">结束日期必须大于开始日期！</div>'
				        ,btn:['确定']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index,layero){
							layer.close(index);
				        }
		      		});
		   		}else{
		   			search();//查询
		   		}
			}
			
			//分页条
			function laypage(count){
				layui.use(['laypage', 'layer'], function(){
					  var laypage = layui.laypage
					  ,layer = layui.layer;
					  laypage.render({
					    elem: 'pagingToolbar'
					    ,count: count
					    ,curr:pageIndex
					    ,limit:limit
					    ,limits:[7,10,20,30,50]
					    ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
					    ,jump: function(obj,first){
					    	pageIndex = obj.curr;//获取当前页
					    	limit = obj.limit;//当前条数
					    	if(!first){
					    		search();//查询
					    	}
					    }
		  			});
		  		});
			}
	        		
			//查询信息
			function search(num){
				//查询条件
		   		var start_time = $("#startTime").val();//审批开始时间
		   		var end_time = $("#endTime").val();//审批结束时间
		    	var work_id = $("#work_id").val();//单号
		    	var L1 = $("#orgName").attr("L1");//中心
		    	var L2 = $("#orgName").attr("L2");//部门
				var recruiterName = $("#recruiter_name").val();//招聘负责人姓名
				var category = $("#category option:selected").val();//获取职位类别的值
				var roStatus = $("#roStatus").val();//招聘状态  
	        	var json = {"auditTime":start_time,"endTime":end_time,"workid":work_id,"L1":L1,"L2":L2,"recruiter_name":recruiterName,"category":category,"roStatus":roStatus,"pageIndex":pageIndex,"limit":limit};
	       	 	if(num != null){//不为空查询全部信息
	        		json = {"auditTime":null,"endTime":null,"workid":null,"L1":null,"L2":null,"recruiter_name":null,"category":null,"roStatus":null,"roStatus":null,"pageIndex":pageIndex,"limit":limit};		
	        	} 
	        	$.ajax({
					url : basePath+"com.primeton.task.taskoperation.selectRecruitOrder.biz.ext",
					type : "POST",
					data:json,
					cache:false,//不显示关闭图标
					async:true,
					success : function(data) {
						var result=data.data;//工单数据
							count =data.page.count ;//总记录数
							laypage(count);//赋值总记录数给分页条
							layui.use('table', function(){
						var table = layui.table ,form = layui.form;
						  //展示已知数据
							  table.render({
								   	 elem: '#demo'
								    ,cols: [[ //标题栏
								       {field: 'recruiterName', title: '招聘负责人',width:'100',unresize:true,templet:'#recruiterName',event:'setSign'}
								      ,{field: 'recuitOrderId', title: '工单号',width:'242',unresize:true,templet:'#isMannulId-html',event:'setSign',sort: true}
								      ,{field: 'l1', title: '部门',width:'350',unresize:true,templet:'#tempOrgName',event:'setSign'}
								      ,{field: 'position', title: '职位',width:'236',unresize:true,templet:'#tempPostion',event:'setSign'}
								      ,{field: 'jobCategory', title: '职位类别',width:'246',unresize: true,templet:'#jobCategory',event:'setSign'}
								      ,{field: 'company', title: '所属公司',width:'246',unresize: true,templet:'#tempCompany',event:'setSign'}
								      ,{field: 'recruitPeriod', title: '招聘周期',width:'90',unresize:true,templet:'#periodDay',event:'setSign'}
								      ,{field: 'auditTime', title: '审批日期',width:'107',unresize:true,templet:'#tempTime',event:'setSign',sort: true}
								      ,{field: 'oaProcessId', title: 'OA流程号',width:'100',unresize:true,templet:'#tempProcessId',event:'setSign'}
								      ,{field: 'isIntern', title: '是否实习生',width:'100',unresize:true,templet:'#tempIntern',event:'setSign'}
								      ,{field: 'isDifficult', title: '是否难招',width:'95',unresize:true,templet:'#tempDifficult',event:'setSign'}
								      ,{field: 'roStatus', title: '招聘状态',width:'132',unresize:true,templet:'#tempRostatus',event:'setSign'}
								      ,{title: '操作',width:'100',unresize:true,templet:'#tempOperation',event:'setSign'}
								    ]]
								    ,id:"idTest"
								    ,data: result 
								    ,height:355
								    ,limit: limit //显示的数量
								    ,done:function(res, curr, count) {
										var data = res.data;
										$('.layui-table-body tr').each(function() {
											var dataindex = $(this).attr('data-index');
											var idx = 0;
											for ( var item in data) {
												if (dataindex == idx) {
													$(this).dblclick(function() {//双击某一行事件
														datas =data[item];//获取当前选中的数据
														if(datas.isMannulId==1){//是否是口头单
															updateOrder(datas.recuitOrderId+'<span class="layui-badge">口头</span>');
														}else{
															updateOrder(datas.recuitOrderId);
														}
													});
													break;
												}
												idx++;
											}
										});
									} 
						  		 });
						  		 table.on('tool(test)', function(obj){//单击数据表格单击事件
						  		 	 if(obj.event === 'setSign'){
									     datas = obj.data;//获取整行值转给全局变量
									     bool=true;//用来标识是否点击到表格
						  		 	 }
								 });
						 });
					}
			});
	}
				//取消招聘
				function updateRoState(recuitOrderId,id){
					  layer.open({
					        type: 1
					        ,offset:'auto' 
					        ,id: 'layerDemo'
					        ,content: '<div style="padding: 20px 75px;">是否取消招聘？</div>'
					        ,closeBtn: false
					        ,btn:['确定','关闭']
					        ,btnAlign: 'c' //按钮居中
					        ,shade: 0 //不显示遮罩
 					        ,resize:false //是否允许拉伸
					        ,closeBtn: false//不显示关闭图标
					        ,yes: function(){
					        	$.ajax({
				                 	url: basePath+'com.primeton.task.taskoperation.updateOrderState.biz.ext',
									type:'POST',
									data:{"recuitOrderId":recuitOrderId,"id":id},
									dataType: 'json',
									success:function(data){
										if(data.result!="E"){
											search();//修改成功后刷新表格
				                  			layer.closeAll(); 
											layer.msg("取消招聘成功");
											
										}else{
											layer.closeAll(); 
											layer.msg("取消招聘失败");
										}
									}
				                 });
					        }
					        ,btn2: function(){
					          layer.closeAll();//关闭页面
					        }
					  }); 
				}
			
                //修改招聘负责人
          		function selectRecruiterName(oneself){
          			current =oneself;//赋值自身（子页面获取）
          			abandonEmpId =$(oneself).attr("empid");//获取当前点击招聘负责人ID
          			updateBool =true;
	                layui.use('layer', function(){	
	 				 var layer = layui.layer;
					  	 layer.open({
						    area: ['770px', '540px'],
							type: 2,
							shade: 0.8,
							resize:false, //是否允许拉伸
							closeBtn: false,//关闭窗口按钮
							title:'更改招聘负责人',
							closeBtn: false,//不显示关闭图标
						    btn: ['选择', '关闭'],
							content: [basePath+'task/takeEmp_select.jsp','no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
		                    yes: function(index,lay){ 
								var iframeWin = window[lay.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
				                var ids = iframeWin.backPrice();//调用子页面的方法，得到子页面返回的ids
				                if(ids==""){
				               		layer.msg("请选择负责人");
				               		return;
				               	}
				               	var json = $.parseJSON(ids);//转换JSON格式
				               		updateBool =false;
				               		updateRecruiterName(oneself,json);
	       					}
					  	 	,btn2: function(index,layero){
					        	layer.close(index);
					        	updateBool =false;
					  	 	}
						}); 
					}); 	   
                };
                
                //修改招聘负责人
                function updateRecruiterName(oneself,json){
                	 layer.open({
					        type: 1
					        ,offset:'auto' 
					        ,id: 'layerDemo'
					        ,resize:false //是否允许拉伸 
					        ,closeBtn: false //关闭窗口按钮
					        ,content: '<div style="padding: 22px 75px 5px;">是否确定更改招聘负责人？</div>'
					        ,btn:['确定','关闭']
					        ,btnAlign: 'c' //按钮居中
					        ,shade: 0 //不显示遮罩
					        ,yes: function(index,layero){
		                  			$(oneself).text(json.name); //改变当前点击a标签的text
		                  		var Workorderid = $(oneself).attr("data");//获取到当前点击工单id
		                    	var emp = {empid:json.id,empname:json.name,workorderid:Workorderid};
		                   		var result=	updateWorkorder(emp); //修改负责人
		                 			 layer.closeAll(); //关闭所有层 
		                 			 if(result){
		                   				layer.msg("修改成功");
		                   			}else{
		                   				layer.msg("修改失败");
		                   			}
		                 			 	updateBool =false;
					        }
						    ,btn2: function(index,layero){
						        	layer.close(index);
						    }
                	 }); 
                }
                
                 //修改工单数据
          		function updateOrder(orderId){
	                layui.use('layer', function(){	
	 				 var layer = layui.layer;
					  	 layer.open({
						    area: ['78%', '99%'],
							type: 2,
							shade: 0.8,
					        resize:false, //是否允许拉伸
							closeBtn: false,//不显示关闭图标
							title:'<span class="span-title"></span>'+orderId,//标题
						    btn: ['保存', '关闭'],
							content: [basePath+'task/order_update.jsp','no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
		                    yes: function(index,lay){
			                		 layer.open({
							        type: 1
							        ,offset:'auto'
							        ,id: 'layerDemo'
							        ,closeBtn: false
							        ,resize:false //是否允许拉伸
							        ,content: '<div style="padding: 20px 75px 0px;">是否保存工单信息？</div>'
							        ,btn:['确定','关闭']
							        ,btnAlign: 'c' //按钮居中
							        ,shade: 0 //不显示遮罩
							        ,yes: function(index,layero){
										var iframeWin = window[lay.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
			                			var data = iframeWin.returnVal();//调用子页面的方法，得到子页面返回的ids
			                			var json={workorderid:data.id,isDifficult:data.isDifficult,isIntern:data.isIntern,isRecruitPeriod:data.recruitPeriod,sign:"update",auditTime:data.auditTime,jobCategory:data.jobCategory,mainReason:data.mainReason,categoryAttribute:data.categoryAttribute};
				                   		var result=	updateWorkorder(json); //修改工单数据
				                   			bool=false;//清除点击标识
				                 			layer.closeAll(); //关闭所有层 
				                 			if(result){
				                   				layer.msg("保存成功");
				                   			}else{
				                   				layer.msg("保存失败");
				                   			}
							        }
							        ,btn2: function(index,layero){
							        	layer.close(index);
							        }
					      		}); 
	       					}
					  	 	,btn2: function(index,layero){
					        	search();
					        	layer.close(index);
					        }
						}); 
					}); 	   
                };
                
                //修改工单或照片负责人
                function updateWorkorder(emp){
                	var result =false;
	                 $.ajax({
	                 	url: basePath+'com.primeton.task.taskoperation.updatetask.biz.ext',
						type:'POST',
						data:emp,
						async:false,
						dataType: 'json',
						success:function(data){
							if(data.result!="E"){
								result=true;
								search();
							}else{
								result=false;
							}
						}
	                 });
	                 return result;
                }
		        
				//重置信息
                function isReset(){
                	$("#orgName").val("");//清空部门
                	$("#orgName").attr("l1","");//清空中心值
	                $("#orgName").attr("l2","");//清空部门值
	                $("#recruiter_name").val("");//清空招聘负责人
	                $("#recruiter_name").attr("data","");//清空招聘负责人ID
	                	$(".import").each(function(i,obj){
	                		obj.value="";
	                	});
                }
                
                //加载职位	-->L2:部门id
		        function loadPosition(L2){
		        	var html="";
			         $.ajax({
		                 	url: basePath+'com.primeton.task.taskoperation.queryNeedPosition.biz.ext',
							type:'POST',
							data:{orgId:L2},
							async:false,
							dataType: 'json',
							success:function(data){
								var datas =data.ArrayOfT_DATA_OUTPUT.ZSTI2038_OUT;
								var error =data.MSG_TYPE;
								if(error=="E"){//接口异常
									 layer.open({//打开子页面
								        type: 1
								        ,offset:'auto' 
								        ,id: 'layerDemo'
									    ,resize:false //是否允许拉伸
								        ,content: '<div style="padding: 20px 75px;">'+data.MSG_TEXT+'</div>'
								        ,btn:['确定']
								        ,closeBtn: false//不显示关闭图标
								        ,btnAlign: 'c' //按钮居中
								        ,shade: 0 //不显示遮罩
								        ,yes: function(index){
								        	layer.close(index);
										var cTime=document.getElementById("addposition");//清除下拉框内容
											cTime.options.length=0;
								        	$("#addposition").append("<option  value=''>"+data.MSG_TEXT+"</option>");
								        }
								 	});
								}else{
									//添加工单页面加载的职位
									var cTime=document.getElementById("addposition");
										cTime.options.length=0;//清除下拉框内容
									if(typeof(datas[0].STEXT)!="undefined"){
										
										//by函数接受一个成员名字符串做为参数
										//并返回一个可以用来对包含该成员的对象数组进行排序的比较函数
										var by = function(name) {
										    return function(o, p) {
										        var a, b;
										        if (typeof o === "object" && typeof p === "object" && o && p) {
										            a = o[name];
										            b = p[name];
										            if (a === b) {
										                return 0;
										            }
										            if (typeof a === typeof b) {
										                return a < b ? -1 : 1;
										            }
										            return typeof a < typeof b ? -1 : 1;
										        } else {
										            throw ("error");
										        }
										    };
										};
										datas.sort(by("STEXT"));
										
										//将对象数组进行计算数量并去重
										var result = [], hash = {};
										var j=0;
									    for (var i = 0; i<datas.length; i++) {
									        var elem = datas[i].STEXT; 
									        j+=1;
									        if (!hash[elem]) {
								        		j=1;
								        		datas[i].num = j; //添加num这个属性 
								                result.push(datas[i]);
									            hash[elem] = true;
									        }else{
									        	//将数组最后的一个对象属性num更新为最新
									        	result[result.length-1].num=j;
									        }
									    }
										
										for(var i=0;i<result.length;i++){
												html+="<option  value="+result[i].STEXT+">"+result[i].STEXT+"（"+result[i].num+"）</option>";
										}
									}else{
												html+="<option  value=''>此部门没有职位，请重新选择</option>";
									}
										$("#addposition").append(html);
								}
							}
		                 });
		        }
		        
		        //部门控件
		     	function choiceOrg(idName){
					layer.open({
						type: 2,
						title: '部门选择',
						content: basePath+'coframe/tools/plugIn/selectorg.jsp',
						area: ['420px', '500px'],
						closeBtn: false,
				        resize:false, //是否允许拉伸
						btn: ['确定','关闭'],
						yes: function(index, layero){ 
							    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
							    var a=iframeWin.getData();
							    var L1="";
							    var L1name="";
							    var L2="";
							    var L2name="";
							    var L3="";
							    var L3name="";
							    var L4="";
							    var L4name="";
							    var alias="";
							    var allName="";
							    $(a).each(function(i,obj){
							    		L1+=obj.L1id;
							    		L1name+=obj.L1name;
							    		L2+=obj.L2id;
							    		L2name+=obj.L2name;
							    		L3+=obj.L3id;
							    		L3name+=obj.L3name;
							    		L4+=obj.L4id;
							    		L4name+=obj.L4name;
							    		alias+=obj.alias;
							    		allName+=obj.aliasName;
							    		
							    });
							    	$(idName).val(allName);
							    	$(idName).attr("alias",alias); 
						  			$(idName).attr("L1",L1); 
						  			$(idName).attr("L1name",L1name); 
						  			$(idName).attr("L2",L2); 
						  			$(idName).attr("L2name",L2name); 
						  			$(idName).attr("L3",L3); 
						  			$(idName).attr("L3name",L3name); 
						  			$(idName).attr("L4",L4); 
						  			$(idName).attr("L4name",L4name);
						  			layer.close(layer.index);//关闭当前页面
						  			var orgID="";
						  			if(L4!=""){//获取最低那层部门
						  				orgID =L4;
						  			}else if(L3!=""){
						  				orgID =L3;
						  			}else if(L2!=""){
						  				orgID =L2;
						  			}else if(L1!=""){
						  				orgID =L1;
						  			}
						  			loadPosition(orgID);//点击确定按钮时加载需求职位
						},
						btn2: function(index, layero){
							   layer.close(layer.index);
							  }    
					});
		     	}
		     	//回车事件
		     	document.onkeydown = function(e){
		            var ev = document.all ? window.event : e;
		            if(ev.keyCode==13) {
		            	queryOrder();//查询
		            }
		        };
		        
		        //所属公司转换文本值
		        function contverCompany(companyId){
		        	var value ="";
		        	 //查询所属公司
		          	 $.ajax({
		          		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
		          		 type:'POST',
		          		 cache: false,
		          		 async:false,
		          		 success:function(data){
		          			 for(var i=0;i<=data.data.length;i++){
		          				 if(companyId==data.data[i].companyId){
		          					 value = data.data[i].companyName;
		          					 break;
		          				 }
		          			 }
		          		 }
		          	 });
		          	 return value;
		        }
				