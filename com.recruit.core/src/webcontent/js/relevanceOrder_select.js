	var pageIndex = 1;//当前页数
	var count;//总页数
	var limit =3;//显示条数
	var recuit_orderId =parent.recuit_orderId;//获取当前单击工单号
	var ordernum = parent.ordernum;
	var sort =parent.sort;//获取父层是否是我的任务关联工单
	if(recuit_orderId==""){//已关联工单为请选择则进来
		recuit_orderId =null;
	}
	//加载事件
	window.onload=function() {
		var json;
		if(sort!=null){//标识是否是我的任务进来
			limit=5;
			json= {"sort":sort,"pageIndex":pageIndex,"recuit_orderId":ordernum,"limit":limit};//用sort标识是已关联查询页面进去
		}else{
			json = {"sort":"relevance","recuit_orderId":recuit_orderId,"pageIndex":pageIndex,"limit":limit};
		}	
			search(json);
	};
	
	 //查询信息
    function searchOrder(){
	    var recruiterId = $("#recruiterId").val();//招聘负责人ID
	    var recruiter_name = $("#recruiter_name").val();//招聘负责人姓名
	    var oaProcessId = $("#oaProcessId").val();//OA流程号
	    var json;
	    if(sort!=null){//标识是否是我的任务进来
	    	json= {"recruiterId":recruiterId,"recruiter_name":recruiter_name,"recuit_orderId":recuit_orderId,"oaProcessId":oaProcessId,"sort":sort,"pageIndex":pageIndex,"limit":limit};
	    }else{
	    	json= {"recruiterId":recruiterId,"recruiter_name":recruiter_name,"recuit_orderId":recuit_orderId,"oaProcessId":oaProcessId,"sort":"relevance","pageIndex":pageIndex,"limit":limit};
	    }
	    	search(json);//查询方法
    }
    
	//查询
    function search(json){
    	 $.ajax({
           	url: basePath+'com.primeton.task.taskoperation.selectRecruitOrder.biz.ext',
			type:'POST',
			cache: false,
			data:json,
			success:function(data){
				count =data.page.count ;//总记录数
				laypage(count);//赋值总记录数给分页条
				var json = {};
				layui.use('table', function(){
           			var table = layui.table;
				  	//第一个实例
				 	table.render({
					   id: 'idTest'
					   ,elem: '#demoOrder'
					   ,height: 174
					   ,data:data.data
					   ,limit: limit //显示的数量
					   ,cols: [[ //表头
					     {field: 'recruiterId', title: '招聘负责人工号',event: 'setSign',width:'18%'}
					     ,{field: 'recruiterName', title: '招聘负责人姓名',event: 'setSign',width:'17%'}
					     ,{field: 'recuitOrderId', title: '工单号',event: 'setSign',width:'26%',sort: true}
					     ,{field: 'auditTime', title: '审批日期',event: 'setSign',width:'17%',sort: true,templet:'#tempTime'}
					     ,{field: 'reqOaprocessid', title: 'OA流程号',event: 'setSign',width:'22.7%'}
					     ,{field: 'org', title: '部门',event: 'setSign',width:'22.3%',templet:'#tempDepartmen'}
					     ,{field: 'position', title: '职位',event: 'setSign',width:'22.3%' ,templet:'#tempPostion'}
					     ,{field: 'jobCategory', title: '职位类别',event: 'setSign',width:'22.3%',templet:'#tempjobCategory'}
					     ,{field: 'rostatus', title: '当前状态',event: 'setSign',width:'12%',templet:'<div>{{rostatus(d.roStatus)}}</div>'}
					     ]]
				  	});
					table.on('tool(test)', function(obj){
						var data = obj.data;
							if(obj.event === 'setSign'){
								json.id = data.id;//工单号ID
								json.recuitOrderId = data.recuitOrderId;//获取工单号
								json.auditTime = data.auditTime;//获取审批日期
								var jsonstr = JSON.stringify(json);//转换JSON格式
								$("#orderId").val(jsonstr);//赋值工单号
						    }
					});
				});
			}
   	 	});
    }
    
  //赋值工单信息
    function backPrice(){
    	var parameter = $("#orderId").val();//emp：JSON格式
    	return parameter;//返回给父页面
    }
    
    //分页条
	function laypage(count){
		layui.use(['laypage', 'layer'], function(){
			  var laypage = layui.laypage
			  ,layer = layui.layer;
			  laypage.render({
			    elem: 'pagingToolbar'
			    ,count: count
			    ,curr:pageIndex
			    ,limit:limit
			    ,limits:[3,10,20,30,50]
			    ,layout: ['count', 'prev', 'page', 'next']
			    ,jump: function(obj,first){
			    	pageIndex = obj.curr;//获取当前页
			    	limit = obj.limit;//当前条数
			    	if(!first){
			    		searchOrder();//查询
			    	}
			    }
  			});
  		});
	}
    
	//回车事件
 	document.onkeydown = function(e){
        var ev = document.all ? window.event : e;
        if(ev.keyCode==13) {
        	searchOrder();//查询
        }
    };