package com.primeton.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class FormatDate {

	@Bizlet("时间天数加一")
	/**
	 * 
	 * @param time结束时间
	 * @return
	 */
	public String convertDate(String time) {
		Date date =null;
		String endDate="";
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
		try {
			date = sdf.parse(time);
			Calendar calendar = Calendar.getInstance();  
			calendar.setTime(date);  
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			date = calendar.getTime();
			endDate= sdf.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		return endDate;
	}

}
