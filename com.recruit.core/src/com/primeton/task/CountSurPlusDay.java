package com.primeton.task;

import java.util.Date;

import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class CountSurPlusDay {
	/**
	 * @param newDate 现在时间
	 * @param auditTime 审核时间
	 * @param recruiPeriod 招聘周期
	 * @return 计算公式 招聘周期 -(现在时间 - 审核时间)/(24*60*60*1000);单位（天）
	 * @author 唐能
	 */
	@Bizlet("")
	public static long countPeriod(Date newDate, Date auditTime,
			String recruiPeriod) {
		if (auditTime == null) {
			auditTime = new Date();
		}
		long day = (newDate.getTime() - auditTime.getTime())
				/ (24 * 60 * 60 * 1000);
		long countPeriod = Long.parseLong(recruiPeriod) - day;
		return countPeriod;
	}
}
