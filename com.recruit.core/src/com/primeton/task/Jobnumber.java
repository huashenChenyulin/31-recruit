/**
 * 
 */
package com.primeton.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.eos.system.annotation.Bizlet;

/**
 * @author Administrator
 * @date 2018-03-15 15:38:41
 * 
 */
@Bizlet("工单号")
public class Jobnumber {
	@Bizlet("工单号")
	public String show(Integer num, String orderIdSort) {
		String number = num.toString();
		String str = null;
		if (number.length() < 3) {

			int val = 3 - number.length();

			for (int i = 0; i < val; i++) {

				number = "0" + number;
			}

		}
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		String time = format.format(date);
		// 判断是OA创建的工单还是自建
		if (orderIdSort.equals("OA")) {
			str = "RS_" + time + number;
		} else {
			str = "RZ_" + time + number;
		}
		System.out.println("工单号=" + str);
		return str;
	}

	@Bizlet("创建一个数组")
	public int[] returnarray(int length) {
		int[] result = new int[length];
		return result;
	}
	
}
