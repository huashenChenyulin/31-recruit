/**
 * 
 */
package com.recruit.reports;

import java.util.ArrayList;
import java.util.List;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author 唐能
 * @date 2018-05-15 16:08:59
 *
 */
@Bizlet("")
public class mergeObject {

	/**
	 * @param RecruitOrderId 操作推进环节的工单号
	 * @param OfferDataObject  发送offer的工单号
	 * @param RecruitCandidatePool 同步人才库的工单号
	 * @return
	 * @author 唐能
	 */
	@Bizlet("")
	public static List mergeRecruitOrderId(DataObject[] RecruitOrderId,DataObject[] OfferDataObject, DataObject[] RecruitCandidatePool,DataObject[] addDataObject) {
		// 合并获取日报表每天操作的任务工单号
		List list  = new ArrayList();
		//获取操作推进环节的工单号，放入list中
		for (DataObject dataObject : RecruitOrderId) {
			String recuitOrderId = (String) dataObject.get("recruitOrderId");
			if(!list.contains(recuitOrderId)) {
				list.add(recuitOrderId);
			}
		}
		//获取发送offer的工单号，放入list中
		for (DataObject dataObject : OfferDataObject) {
			String recuitOrderId = (String) dataObject.get("recuitOrderId");
			if(!list.contains(recuitOrderId)) {
				list.add(recuitOrderId);
			}
		}
		//获取同步人才库的工单号，放入list中
		for (DataObject dataObject : RecruitCandidatePool) {
			String recuitOrderId = (String) dataObject.get("recuitOrderId");
			if(!list.contains(recuitOrderId)) {
				list.add(recuitOrderId);
			}
		}
		
		//获取添加候选人的工单号，放入list中
				for (DataObject dataObject : addDataObject) {
					String recuitOrderId = (String) dataObject.get("recuitOrderId");
					if(!list.contains(recuitOrderId)) {
						list.add(recuitOrderId);
					}
				}
		return list;
	}

}
