package com.recruit.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

public class DepartmentReportsDeal {
	@Bizlet("")
	public List<Map<String, String>> getdepartment(DataObject[] department,String interviewerid) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		int i ;
		int j ;
		for(i = 0 ;i < department.length ; i++){
			String position = department[i].getString("position");
			String recruiter_name = department[i].getString("recruiter_name");
			String recruiter_id =department[i].getString("recruiter_id");
			String recruit_order_id =department[i].getString("recruit_order_id");
			String candidate_id =department[i].get("candidate_id").toString();
			String link =department[i].getString("link");
			String L1 =department[i].getString("L1");
			String L1_name =department[i].getString("L1_name");
			String L2 =department[i].getString("L2");
			String L2_name =department[i].getString("L2_name");
			//拆分部门面试官
			String interviewer_id = department[i].getString("interviewer_id");
			String interviewer_name = department[i].getString("interviewer_name");
			String[] interidArray = interviewer_id.split("\\,");
			String[] internameArray = interviewer_name.split("\\,");
				for(j =0;j < interidArray.length ; j++){
					if(interviewerid == "" || interviewerid == null || interviewerid.equals(interidArray[j])){
						Map<String, String> map = new HashMap<String, String>();
						String[] L1Array = new String[interidArray.length];
						String[] L1_nameArray = new String[interidArray.length];
						String[] L2Array = new String[interidArray.length];
						String[] L2_nameArray = new String[interidArray.length];
						if(L1 == "" || L1 == null){
						}else{
							L1Array = L1.split("\\,");
							L1_nameArray = L1_name.split("\\,");
							
						}
						if(L2 == "" || L2 == null){
						}else{
							L2Array = L2.split("\\,");
							L2_nameArray = L2_name.split("\\,");
						}
						map.put("position", position);
						map.put("interviewer_id", interidArray[j]);
						map.put("interviewer_name", internameArray[j]);
						map.put("recruiter_name", recruiter_name);
						map.put("recruiter_id", recruiter_id);
						map.put("recruit_order_id", recruit_order_id);
						map.put("candidate_id", candidate_id);
						map.put("link", link);
						map.put("L1", L1Array[j]);
						map.put("L1_name", L1_nameArray[j]);
						map.put("L2", L2Array[j]);
						map.put("L2_name", L2_nameArray[j]);
						list.add(map);
					}
				}
			
		}
		return list;
	}
	public static void main(String[] args) {
		// TODO 自动生成的方法存根

	}

}
