/**
 * 
 */
package com.recruit.reports;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;







import java.util.Locale;

import java_cup.internal_error;

import com.eos.system.annotation.Bizlet;
import com.mysql.fabric.xmlrpc.base.Data;
import com.sun.jmx.snmp.Timestamp;

/**
 * @author 唐能
 * @date 2018-05-08 09:45:31
 *
 */
@Bizlet("时间字段处理")
public class DateUtil {

	/**
	 * @param date 开始日期结束日期拼接字段
	 * @return 时间字段转换为时间数组
	 * @author 唐能
	 */
	@Bizlet("时间字段转换为时间数组")
	public static Date[] splitDateString(String date) {
		String[] dateArray = date.split("~");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date[] dates = new Date[2];
		for (int i = 0; i < dateArray.length; i++) {
			String string = dateArray[i];
			try {
				dates[i] = sdf.parse(string);
				if (i == dateArray.length - 1) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(dates[i]);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					dates[i] = calendar.getTime();
				}
				System.out.println(dates[i]);
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		return dates;
	}

	@Bizlet("拆分时间字段为数组")
	public static String[] DateString(String date) {
		String[] dateArray = date.split("~");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dates = new Date();

		for (int i = 0; i < dateArray.length; i++) {
			String string = dateArray[i];
			try {
				dates = sdf.parse(string);
				dateArray[i] = sdf.format(dates);
				if (i == dateArray.length - 1) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(dates);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					dates = calendar.getTime();
					dateArray[i] = sdf.format(dates);
				}
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		return dateArray;
	}

	public static void main(String[] args) {
		/*splitDateString("2018-01-02~2018-01-03");*/
		/*dateYear("2018-05-15");*/
		date();
	}

	/**
	 * @return
	 */
	@Bizlet("获取当前年度")
	public static int year() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	/**
	 * @param  部门拼接
	 * @return
	 * @author zengjunjie
	 */
	@Bizlet("")
	public static List orgcentreSplicing(String l1, String l2, String l3,
			String l4, String l1_name, String l2_name, String l3_name,
			String l4_name) {
		List list = new ArrayList();
		String org = "";
		String orgName = "";
		if (l2 != null || l2 != "") {
			org += "." + l1 + "." + l2;
		}
		if (l3 != null || l3 != "") {
			org += "." + l3;
		}
		if (l4 != null || l4 != "") {
			org += "." + l4 + ".";
		}
		if (l2_name != null || l2_name != "") {
			orgName += l2_name;
		}
		if (l3_name != null || l3_name != "") {
			orgName += "/" + l3_name;
		}
		if (l4_name != null || l4_name != "") {
			orgName += "/" + l4_name;
		}
		list.add(org);
		list.add(orgName);

		return list;
	}

	/**
	 * @param date 传入的时间字符串 如：2018-05-15
	 * @return 如果传入时间为 null 获取当前系统时间的年份
	 */
	@Bizlet("获取时间的年份")
	public static int dateYear(String date) {
		Calendar cal = Calendar.getInstance();
		Date datetime = new Date();
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				datetime = sdf.parse(date);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			cal.setTime(datetime);
		}
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	/**
	 * @param 传入的时间字符串 如：2018-05-15
	 * @return 如果传入时间为 null 获取当前系统时间的月份
	 */
	@Bizlet("获取时间的月份")
	public static int dateMonth(String date) {
		Calendar cal = Calendar.getInstance();
		Date datetime = new Date();
		if (date != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				datetime = sdf.parse(date);
			} catch (ParseException e) {

				e.printStackTrace();
			}
			cal.setTime(datetime);
		}
		int Month = cal.get(Calendar.MONTH) + 1;
		return Month;
	}

	/**
	 * @return
	 */
	@Bizlet("获取当前日期")
	public static String date() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String dateString = sdf.format(d);
		return dateString;
	}
	
	@Bizlet("Datatime转Data")
	public static String DatatimetoDate(String datatime) throws ParseException{
		SimpleDateFormat  sdfTime = new SimpleDateFormat("yyyy-MM-dd");  
		Date data =sdfTime.parse(datatime);
		String date1 = sdfTime.format(data);
		return date1;
	}
	
	@Bizlet("处理特殊字符为时间格式")
	public static String specialdata(String special) {
		String year = special.substring(0, 4);
		String month = special.substring(4, 6);
		String day = special.substring(6,8);
		String date = year+"-"+month+"-"+day;
		return date;
	}
	
	@Bizlet("判断当前环节")
	public static String recruit_process(String recruit_process_id,String candidate_status){
		String process = "";
		if("3".equals(candidate_status)){//候选人状态为已归档为放弃环节
			if("1".equals(recruit_process_id)){
				process="初筛环节";
			}else if("2".equals(recruit_process_id)){
				process="电话沟通环节";
			}else if("3".equals(recruit_process_id)){
				process="人力面试环节";
			}else if("4".equals(recruit_process_id)){
				process="部门面试环节";
			}else if("5".equals(recruit_process_id)){
				process="待入职环节";
			}else if("6".equals(recruit_process_id)){
				process="已入职环节";
			}
		}else{
			process="";
		}
		return process;
	}
	
	@Bizlet("获取年龄")
	public static String GetAge(String birthdate){
		int returnAge;
		if(birthdate!=null && !"".equals(birthdate) ){
			
			String[] strBirthdayArr=birthdate.split("-");  
			
			String birthYear = strBirthdayArr[0];  
			int birthyesar = Integer.parseInt(birthYear);
			
			Calendar now = Calendar.getInstance();  
 		    int nowYear = now.get(Calendar.YEAR);
 		    
 		    if(nowYear == birthyesar)  
 		    {  
 		        returnAge = 0;//同年 则为0岁  
 		    }  
 		    else  
 		    {  
 		        int ageDiff = nowYear - birthyesar ; //年之差  
 		        if(ageDiff > 0)  
 		        {  
 		        	returnAge =ageDiff;
 		        }  
 		        else  
 		        {  
 		            returnAge = -1;//返回-1 表示出生日期输入错误 晚于今天  
 		        }  
 		    }  
 		  }else{
 		  		returnAge=0;
 		  }
 		    return returnAge+" 岁";//返回周岁年龄  
		
		}
	
	@Bizlet("字符串转data")
	public static String getendData(String month){
		 String Month = month+"-01";
		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		  // 定义当前期间的1号的date对象
		  Date date = null;
		  try {
		    date = dateFormat.parse(Month);
		  } catch (ParseException e) {
		    e.printStackTrace();
		  } 
		 Calendar cal = Calendar.getInstance(Locale.CHINA);
		 cal.clear(Calendar.YEAR);  
		 cal.clear(Calendar.MONTH);  
		 cal.clear(Calendar.DAY_OF_MONTH);
		 cal.setTime(date);
		 int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		 String reData = month+"-"+lastDay+" 23:59:59";
		 return reData;
	}
	
}
