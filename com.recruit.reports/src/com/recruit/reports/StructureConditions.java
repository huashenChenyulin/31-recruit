package com.recruit.reports;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

public class StructureConditions {
	@Bizlet("拼接条件")
	public String getChannel(DataObject[] dicEntry){
		String conditions="";
		
		for(int i = 0 ; i<dicEntry.length;i++){
			//获取业务字典的渠道和供应商
			String dicName =  dicEntry[i].getString("DICTNAME");
			//获取渠道和供应商的序号
			String seqno =  dicEntry[i].getString("SEQNO");
			//截取序号
			seqno = seqno.substring(1);
			seqno = seqno.substring(0,seqno.length()-1);
			//替换字符
			seqno = seqno.replace(".", "and");
			conditions+="MAX(CASE h.DICTNAME WHEN '"+dicName+"' THEN h.num ELSE 0 END ) dicName"+seqno+",";
		}
	
		conditions=conditions.substring(0,conditions.length()-1);
	
		return conditions;
	}
}
