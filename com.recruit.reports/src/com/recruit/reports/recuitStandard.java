/**
 * 
 */
package com.recruit.reports;



import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author 唐能
 * @date 2018-05-16 22:41:32
 *
 */
@Bizlet("")
public class recuitStandard {

	/**
	 * @param dataObject
	 * @return
	 * @author 唐能
	 */
	@Bizlet("")
	public static List standardList(DataObject[] dataObject) {
		System.out.println(dataObject.length);
		DecimalFormat df = new DecimalFormat("#");
		List list = new ArrayList();
		for (DataObject dataObject2 : dataObject) {
			List num = new ArrayList();
			for (int i = 1; i <= 12; i++) {
				num.add(df.format(dataObject2.get(i+"MONTH")));
			}
			list.add(num);
		}
		return list;
	}

}
