package com.recruit.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

@Bizlet("生成Excel文件")
public class generateIExcelFile {
	@Bizlet("生成Excel文件")
	public String personalMainReport(String head,String sheetName,String fileName,DataObject[] dataObject,String path) {
		//文件保存路径
		String savePath = path+"/";
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(sheetName);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		//设置默认宽度、高度
		sheet.setDefaultColumnWidth((short) 25);  
		sheet.setDefaultRowHeightInPoints(20);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
		
		//实例化一个map
		Map<String,Integer> map = new HashMap<String,Integer>();
		HSSFCell cell = row.createCell((short) 0);
		JSONObject JSONHead =null;
		try {
			JSONHead = new JSONObject(head);
		} catch (JSONException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
		  Iterator iterator = JSONHead.keys();  
		  int h = 0;
		  while(iterator.hasNext()){  
		      String key = (String) iterator.next();  
		      String value=null;
			try {
				value = JSONHead.getString(key);
			} catch (JSONException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		      map.put(key, h);
		      cell = row.createCell((short) h++);
		      cell.setCellValue(value);
		      cell.setCellStyle(style);
		  }
		 
		  // 第五步，写入实体数据，
		  List list = new ArrayList();//将传入字符串转换为List集合的json对象集合
		 for (int k = 0; k < dataObject.length; k++) {
			DataObject dataObject2 = dataObject[k];
			Map temp = new HashMap();
			Iterator iteratorValue = JSONHead.keys();
			while(iteratorValue.hasNext()){  
			      String key = (String) iteratorValue.next(); 
			      Object value =  dataObject2.get(key);
			      if(value==null){
			    	  value="";
			      }
			      temp.put(key, value);
			}
				JSONObject JSONValue =null;
				JSONValue = new JSONObject(temp);
				list.add(JSONValue);
		}
		 //String  jsonObject =null;
		for (int i = 0; i < list.size(); i++) {
			JSONObject job = (JSONObject) list.get(i);
			
			row = sheet.createRow((int) i + 1);
			Iterator jobiterator = job.keys();
			Set<String> keys = map.keySet();
			for(String key : keys){
					Object value=null;
					try {
						value = job.getString(key);
					} catch (JSONException e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}
					
					//判断是否为null是转换为""
					if(value==null||value=="null"){
						value = "";
					}
					
					//如果key值是渠道，需要对value值进行转换
					if(key=="recruitChannels"||key.equals("recruitChannels")){
						if(value!=""&&value!=null){
						String recruitChannels = (String)value;
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_channel", recruitChannels);
						}
					}
					
					//如果key值是供应商，需要对value值进行转换
					if(key=="recruitSuppliers"||key.equals("recruitSuppliers")){
						if(value!=""&&value!=null){
						String recruitSuppliers = (String)value;
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_provider", recruitSuppliers);
						}
					}
					
					//如果key值是供应商，需要对value值进行转换
					if(key=="jobCategory"||key.equals("jobCategory")){
						if(value!=""&&value!=null){
						String jobCategory = (String)value;
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("category", jobCategory);
						}
					}
					
					//如果key值是学历，需要对value值进行转换
					if(key=="degree"||key.equals("degree")){
						if(value!=""&&value!=null){
						String recruitChannels = (String)value;
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("SFY_COF_DEGREE", recruitChannels);
						}
					}
					
					int j = map.get(key);
					//回填表格值
					cell =row.createCell((short) j);
					String Value = (String)value;
					cell.setCellValue(Value);
					//设置表格值水平居中
					cell.setCellStyle(style);
			}
			
		}
		// 第六步，将文件存到指定位置
		try {
			
			File cacheDir = new File(savePath);//设置目录参数
			if (cacheDir.exists()) {
				cacheDir.mkdirs();
			}
			FileOutputStream fout = new FileOutputStream(savePath+fileName+".xls");
			wb.write(fout);
			fout.close();
			/* response.getWriter().print(savePath+fileName+".xls"); */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName+".xls";
		
	}

}
