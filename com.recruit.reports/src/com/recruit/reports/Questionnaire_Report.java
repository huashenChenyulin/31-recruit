/**
 * 
 */
package com.recruit.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author zengjunjie
 * @date 2018-12-11 10:47:09
 *
 */
@Bizlet("问卷调查报表")
public class Questionnaire_Report {

	@Bizlet("题目拼接")
	public DataObject[] SplicingDataObject(DataObject[] subject){
		//定义对象数组，长度35
		DataObject[] dataObject=new DataObject[35];
		
		for(int i=0;i<subject.length;i++){
			dataObject[i]=subject[i];
		}
		//获取对象长度
		int len= subject.length;
		for(int i=len;i<35;i++){
			//数理化对象
			DataObject obj=com.eos.foundation.data.DataObjectUtil.createDataObject("com.recruit.reports.questionnaireReport.Subject");
			//赋值
			obj.set("serial_number",i+1);
			obj.set("subject","无");
			dataObject[i]=obj;
		}
		return dataObject;
		
	}
	
	@Bizlet("拆分时间字段为数组")
	public static String[] DateString(String date) {
		String[] dateArray = date.split("~");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dates = new Date();

		for (int i = 0; i < dateArray.length; i++) {
			String string = dateArray[i];
			try {
				dates = sdf.parse(string);
				dateArray[i] = sdf.format(dates);
				
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		return dateArray;
	}
	
	
	@Bizlet("生成Excel文件")
	public String personalMainReport(String head,String sheetName,String fileName,DataObject[] dataObject,String path) {
		//文件保存路径
		String savePath = path+"/";
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(sheetName);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		//设置默认宽度、高度
		sheet.setDefaultColumnWidth((short) 25);  
		sheet.setDefaultRowHeightInPoints(20);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
		
		//实例化一个map
		Map<String,Integer> map = new HashMap<String,Integer>();
		HSSFCell cell = row.createCell((short) 0);
		JSONObject JSONHead =null;
		try {
			JSONHead = new JSONObject(head);
		} catch (JSONException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
		  Iterator iterator = JSONHead.keys();  
		  int h = 0;
		  while(iterator.hasNext()){  
		      String key = (String) iterator.next();  
		      String value=null;
			try {
				value = JSONHead.getString(key);
			} catch (JSONException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		      map.put(key, h);
		      cell = row.createCell((short) h++);
		      cell.setCellValue(value);
		      cell.setCellStyle(style);
		  }
		 
		  // 第五步，写入实体数据，
		  List list = new ArrayList();//将传入字符串转换为List集合的json对象集合
		 for (int k = 0; k < dataObject.length; k++) {
			DataObject dataObject2 = dataObject[k];
			Map temp = new HashMap();
			Iterator iteratorValue = JSONHead.keys();
			while(iteratorValue.hasNext()){  
			      String key = (String) iteratorValue.next(); 
			      Object value =  dataObject2.get(key);
			      if(value==null){
			    	  value="";
			      }
			      temp.put(key, value);
			}
				JSONObject JSONValue =null;
				JSONValue = new JSONObject(temp);
				list.add(JSONValue);
		}
		 //String  jsonObject =null;
		for (int i = 0; i < list.size(); i++) {
			JSONObject job = (JSONObject) list.get(i);
			
			row = sheet.createRow((int) i + 1);
			Iterator jobiterator = job.keys();
			Set<String> keys = map.keySet();
			for(String key : keys){
					Object value=null;
					try {
						value = job.getString(key);
					} catch (JSONException e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}
					
					//判断是否为null是转换为""
					if(value==null||value=="null"){
						value = "";
					}
					
					//如果key值是对应报表的值时
					if(key=="answer_1"||key.equals("answer_1") || key=="answer_2"||key.equals("answer_2") || key=="answer_3"||key.equals("answer_3")
						 || key=="answer_4"||key.equals("answer_4") || key=="answer_5"||key.equals("answer_5") || key=="answer_6"||key.equals("answer_6")
						 || key=="answer_7"||key.equals("answer_7") || key=="answer_8"||key.equals("answer_8") || key=="answer_9"||key.equals("answer_9")
						 || key=="answer_10"||key.equals("answer_10") || key=="answer_11"||key.equals("answer_11") || key=="answer_12"||key.equals("answer_12")
						 || key=="answer_13"||key.equals("answer_13") || key=="answer_14"||key.equals("answer_14") || key=="answer_15"||key.equals("answer_15")
						 || key=="answer_16"||key.equals("answer_16") || key=="answer_17"||key.equals("answer_17") || key=="answer_18"||key.equals("answer_18")
						 || key=="answer_19"||key.equals("answer_19") || key=="answer_20"||key.equals("answer_20") || key=="answer_21"||key.equals("answer_21")
						 || key=="answer_22"||key.equals("answer_22") || key=="answer_23"||key.equals("answer_23") || key=="answer_24"||key.equals("answer_24")
						 || key=="answer_25"||key.equals("answer_25") || key=="answer_26"||key.equals("answer_26") || key=="answer_27"||key.equals("answer_27")
						 || key=="answer_28"||key.equals("answer_28") || key=="answer_29"||key.equals("answer_29") || key=="answer_30"||key.equals("answer_30")
						 || key=="answer_31"||key.equals("answer_31") || key=="answer_32"||key.equals("answer_32") || key=="answer_33"||key.equals("answer_33")
						 || key=="answer_34"||key.equals("answer_34") || key=="answer_35"||key.equals("answer_35")){
						if(value!=""&&value!=null){
							String str=(String) value;
							
							if(str.substring(str.length()-1).equals(";") || str.substring(str.length()-1)==";"){
								value=str.substring(0,str.length()-1);
							}
							/*String recruitChannels = (String)value;
							value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("SFY_COF_DEGREE", recruitChannels);*/
						}
					}
					
					int j = map.get(key);
					//回填表格值
					cell =row.createCell((short) j);
					String Value = (String)value;
					cell.setCellValue(Value);
					//设置表格值水平居中
					cell.setCellStyle(style);
			}
			
		}
		// 第六步，将文件存到指定位置
		try {
			
			File cacheDir = new File(savePath);//设置目录参数
			if (cacheDir.exists()) {
				cacheDir.mkdirs();
			}
			FileOutputStream fout = new FileOutputStream(savePath+fileName+".xls");
			wb.write(fout);
			fout.close();
			/* response.getWriter().print(savePath+fileName+".xls"); */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName+".xls";
		
	}
	
}
