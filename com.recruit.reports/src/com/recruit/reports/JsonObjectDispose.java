/**
 * 
 */
package com.recruit.reports;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import com.eos.system.annotation.Bizlet;

/**
 * @author 30000075
 * @date 2018-06-12 10:03:55
 *
 */
@Bizlet("JsonObject数据处理")
public class JsonObjectDispose {

	/**json 数组对象字符串 转list对象集合
	 * JsonObject 传入的json 数组对象字符串
	 */
	@Bizlet("Json数组对象字符串转list集合")
	public static List stringList(String JsonObject){
		JSONArray JsonArray = JSONArray.fromObject(JsonObject);
		List list = new ArrayList(); 
		if(JsonArray.size()>0){
			  for(int i=0;i<JsonArray.size();i++){
				  net.sf.json.JSONObject job = JsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
			    list.add(job);//得到 每个对象中的属性值
			  }
		}
		return list;
	}
	
	/**json 数组对象字符串 转list对象集合
	 * JsonObject 传入的json 数组对象字符串
	 */
	@Bizlet("Json数组对象转list集合")
	public static List stringList(JSONArray JsonObject){
		List list = new ArrayList(); 
		if(JsonObject.size()>0){
			  for(int i=0;i<JsonObject.size();i++){
				  net.sf.json.JSONObject job = JsonObject.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
			    list.add(job);//得到 每个对象中的属性值
			  }
		}
		return list;
	}
	
}
