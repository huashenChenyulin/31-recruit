package com.recruit.reports;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;*/




import java.util.Map;
import java.util.regex.Pattern;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

import com.eos.system.annotation.Bizlet;
import commonj.sdo.DataObject;
@Bizlet("")
public class CreateSimpleExcelToDisk {
	

	/**json 数组对象字符串 转list对象集合
	 * JsonObject 传入的json 数组对象字符串
	 */
	/*public static List stringList(String JsonObject){
		JSONArray JsonArray = JSONArray.fromObject(JsonObject);
		List list = new ArrayList(); 
		if(JsonArray.size()>0){
			  for(int i=0;i<JsonArray.size();i++){
				  net.sf.json.JSONObject job = JsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
			    list.add(job);//得到 每个对象中的属性值
			  }
		}
		return list;
	}*/

/**
 * 候选人才Excel数据导入
* 查询指定目录中电子表格中所有的数据
* @param file 文件完整路径
* @param sheetFile Excel工作簿名称
* @return
*/
@Bizlet("候选人Excel导入")
public static List getAllByExcelCandidate(String file,String sheetFile){
	//Excel文件链接地址
	
	file = sheetFile;
   List list=new ArrayList();
   try {
	   WorkbookSettings ws = new WorkbookSettings();
	   ws.setCellValidationDisabled(true);
   		//判断是否为数字类型值
	   Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
       Workbook rwb=Workbook.getWorkbook(new File(file),ws);
       Sheet rs=rwb.getSheet(0);//或者rwb.getSheet(0)
       int clos=rs.getColumns();//得到所有的列
       int rows=rs.getRows();//得到所有的行
       
       System.out.println(clos+" rows:"+rows);
       for (int i = 1; i < rows; i++) {
           for (int j = 0; j < 1; j++) {
           		Map map = new HashMap();
           		//获取每行所有数据
               //第一个是列数，第二个是行数
           		//默认最左边编号也算一列 所以这里得j++
	           	String recuitOrderId = rs.getCell(j++, i).getContents();//工单号
	           	if(recuitOrderId!=null&&recuitOrderId!=""){
	           	String candidateName=rs.getCell(j++, i).getContents();//人才姓名
	           	String candidatePhone=rs.getCell(j++, i).getContents();//电话
	           	String candidateMail=rs.getCell(j++, i).getContents();//邮箱
	           	
	           	String expectedPosition=rs.getCell(j++, i).getContents();//简历性质
	           	if(expectedPosition!=null&&expectedPosition!=""&&expectedPosition.contains("-")){
                  	//截取positionCatagory编码“-”前面的编码值
	           		expectedPosition = expectedPosition.substring(0, expectedPosition.indexOf('-')); 
                 }
               String recruitChannels = rs.getCell(j++, i).getContents();//渠道
               if(recruitChannels!=null&&recruitChannels!=""&&recruitChannels.contains("-")){
                  	//截取positionCatagory编码“-”前面的编码值
               	recruitChannels = recruitChannels.substring(0, recruitChannels.indexOf('-')); 
                 }else if(recruitChannels!=null&&recruitChannels!=""){
                	 DataObject [] data = com.eos.foundation.eoscommon.BusinessDictUtil.getCurrentDictInfoByType("recruit_channel");
                	 
                	 for (DataObject dataObject : data) {
						if(dataObject.getString("dictName").equals(recruitChannels)||recruitChannels==dataObject.getString("dictName")){
							recruitChannels = dataObject.getString("dictID");
							break;
						}
					}
                 }
               String recruitSuppliers = rs.getCell(j++, i).getContents();//供应商
               if(recruitSuppliers!=null&&recruitSuppliers!=""&&recruitSuppliers.contains("-")){
                  	//截取positionCatagory编码“-”前面的编码值
               	recruitSuppliers = recruitSuppliers.substring(0, recruitSuppliers.indexOf('-')); 
                 }
               
               String gender=rs.getCell(j++, i).getContents();//性别
               if(gender!=null&&gender!=""&&gender.contains("-")){
            	   //截取positionCatagory编码“-”前面的编码值
            	   gender = gender.substring(0, gender.indexOf('-')); 
               }
               
               String degree=rs.getCell(j++, i).getContents();//学历编码
               if(degree!=null&&degree!=""&&degree.contains("-")){
            	   //截取positionCatagory编码“-”前面的编码值
            	   degree = degree.substring(0, degree.indexOf('-')); 
               }
               
               
               //将获取的Excel每行数据存入Map中
               map.put("recuitOrderId", recuitOrderId);//招聘负责人工号
               map.put("recruitChannels", recruitChannels);//渠道
               map.put("recruitSuppliers", recruitSuppliers);//供应商
               map.put("candidateName", candidateName);
               map.put("candidatePhone", candidatePhone);
               map.put("candidateMail", candidateMail);
               map.put("expectedPosition", expectedPosition);
               map.put("degree", degree);
               map.put("gender", gender);
               
               list.add(map);
	           	}
           }
       }
   } catch (Exception e) {
       // TODO Auto-generated catch block
       e.printStackTrace();
   } finally {
	   File downloadFile=new File(sheetFile);
	   downloadFile.delete();
        
   }
   return list;
}
/**
 * 批量导入中，数据已存在或者添加失败的数据整理
* @param map 已存在的候选人信息
* @param List 已存在的候选人信息集合
* @return
*/
@Bizlet("候选人批到结果返回")
public static List getAllList(Map map,List List,String stutas){
	List list=new ArrayList();
	//拥有其他结果也需放入list返回
	if(List!=null){
		list.addAll(List);
	}
	Map gather = new HashMap();
	gather.put("candidateName", map.get("candidateName"));//候选人姓名
	gather.put("candidatePhone", map.get("candidatePhone"));//候选人电话
	gather.put("recuitOrderId", map.get("recuitOrderId"));//招聘负责人工号
	
	if(map!=null){
		//判断传入人才批导放回状态，并写入map中 -1保存失败、-2人才已存在、1简历更新成功
		if(stutas=="1"||"1".equals(stutas)){
            gather.put("stutas", "简历已存在，更新成功");
		}else if(stutas=="-1"||"-1".equals(stutas)){
			gather.put("stutas", "候选人信息导入失败");
		}else if(stutas=="-2"||"-2".equals(stutas)){
			gather.put("stutas", "工单上已有该候选人");
		}else if(stutas=="-3"||"-3".equals(stutas)){
			gather.put("stutas", "请勿操作他人的工单");
		}
		list.add(gather);
	}
	return list;
}

 public static void main(String[] args) {
	 try {
		 //getAllByExcelTalents("","");
		 getAllByExcelCandidate("","");
	 } catch (Exception e) {
		 // TODO 自动生成的 catch 块
		 e.printStackTrace();
	 }
 }
}
