<%@page pageEncoding="UTF-8"%>
<%@page import="javax.servlet.ServletOutputStream"%>
<%@page import="com.eos.engine.component.ILogicComponent"%>
<%@page import="com.primeton.ext.engine.component.LogicComponentFactory"%>
<%@page import="java.io.*"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<%@page import="com.eos.web.taglib.util.*" %><%
	  //获取标签中使用的国际化资源信息
	  String fileNotExist=com.eos.foundation.eoscommon.ResourcesMessageUtil.getI18nResourceMessage("l_fileNotExist");
      Object root= com.eos.web.taglib.util.XpathUtil.getDataContextRoot("request", pageContext); 
      String localFile=(String)XpathUtil.getObjectByXpath(root,"downloadpath");
      String pathName = (String)XpathUtil.getObjectByXpath(root,"fileName");
      System.out.print(pathName);
	  byte[] buffer = new byte[512]; 
	  int size = 0; 
	  response.reset();
	  response.setContentType("application/vnd.ms-excel");
	  response.setCharacterEncoding("UTF-8");
 	  //response.setHeader("Content-disposition", "attachment;filename=\""+ java.net.URLEncoder.encode(localFile,"UTF-8") + "\"");
 	  String agent = request.getHeader("USER-AGENT");
 	  //如果是火狐浏览器编码特殊处理
 	  if(agent.toLowerCase().indexOf("firefox") > 0){
 	  //火狐浏览器自己会对URL进行一次URL转码所以区别处理
      String fileName = pathName+".xls";
 	  	System.out.println(fileName);		
 	  response.setHeader("Content-disposition", "attachment;filename="+ new String(fileName.getBytes("GB2312"),"ISO-8859-1"));
 	  }else{
 	  response.setHeader("Content-disposition", "attachment;filename=\""+ 
 	  				java.net.URLEncoder.encode(pathName+".xls","UTF-8").replace("%5B", "[")
 	  					.replace("%5D", "]").replace("%28", "(").replace("%29", ")").replace("+", "") + "\"");
 	  	}
	  ServletOutputStream os = null;
	  FileInputStream in = null;
	  //web路径
		String rootpath = request.getSession().getServletContext().getRealPath("");
	  localFile=rootpath+"/"+localFile;
	  File downloadFile=new File(localFile);
	  try {
	     os = response.getOutputStream();
	     if(downloadFile!=null&&downloadFile.exists()){
		     in = new FileInputStream(new File(localFile));
		     while ((size = in.read(buffer)) != -1) { 
		       os.write(buffer, 0, size); 
		     } 
		    out.clear();
         	out = pageContext.pushBody();
         	
	     }else{
	        out.print(fileNotExist); //"文件不存在！"
	     }
  	   } catch(Exception e) {
          e.printStackTrace();
       } finally {
      		downloadFile.delete();
            try {
             if(in!=null)in.close();
		     if(os!=null)os.close();
		     File file=new File(localFile);
		     if (file!=null&&file.isFile()&&file.exists()) {
		       file.delete();
		     }
		    //获取操作员工号
		    String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
		    //获取操作员名称
		    String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
		    if(pathName.indexOf("批导结果")==-1){
		     //记录导出日记
		    Object[] params = new Object[4];
			params[0] = empid;
			params[1] = empname;
			params[2] = "导出"+pathName+"的EXCEL文件";
			params[3] = ""+empname+"（"+empid+"） 导出了（"+pathName+"）的EXCEL文件";
			// 逻辑构件名称
			String componentName = "com.recruit.home.recruitOperateLog";
			// 逻辑流名称
			String operationName = "saveOperateLog";
			ILogicComponent logicComponent = LogicComponentFactory.create(componentName);
	        try {
				logicComponent.invoke(operationName, params);
			} catch (Throwable e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		  }
		   } catch (IOException e) {
		     e.printStackTrace();
		   }
		  
       }
%>