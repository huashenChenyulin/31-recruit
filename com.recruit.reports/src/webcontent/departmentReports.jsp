<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wj
  - Date: 2018-05-14 10:03:57
  - Description:内推奖励跟踪报页面
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>部门主要面试官名单</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>部门主要面试官名单</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>主要面试官:</label> 
							<input class="layui-input getStaff" name="recruiter_name" id="recruiter_name" onclick="getEntryStaff(this)" data="" 
							 placeholder="人员选择" value="" type="text" style="border-color: #cccccc;display:inline-block;height: 34px;width: auto;">
						</td>
						<td>
				      		<label>审批日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="~">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	init_table();
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	} 
	});
	
	
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].DICTID+'">'+data.data[i].DICTNAME+'</option>');
			 	}
			}
       });
	}
	
	function init_laydate(){
		// 审批日期
	    layui.use('laydate', function(){
		  	var laydate = layui.laydate;
		  	$("#audit_time,#offer_time,#atual_hire_date").each(function(index,element){
		  		// 执行一个laydate实例
		  		var id = element.id;
			  	laydate.render({
			    	elem: '#'+id //指定元素
			    	,type: 'month'
			    	,range:'~'
			  	});
		  	});
		});
	}
	function init_table(){
		var myDate = new Date();
		var myyear = myDate.getFullYear();
		var mymonth = myDate.getMonth()+1;
		mymonth =(mymonth<10 ? "0"+mymonth:mymonth);
		var nowMonth = myyear+"-"+mymonth;
		var dailyReportDtime = $("#atual_hire_date").val();
		if(dailyReportDtime == null || dailyReportDtime == ""){
			dailyReportDtime = nowMonth+"~"+nowMonth;
		}
		//审批时间
		var Datetime = dailyReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var startmonth = alltime[0];
		var endmonth = alltime[1];
		//招聘负责人
		var interviewerid = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":limit,"interviewerid":interviewerid,"startmonth":startmonth,"endmonth":endmonth};
       	$.ajax({
       		url: 'com.recruit.reports.departmentReports.selectDepartment.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.return_department
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'interviewer_name', title: '主要面试官姓名',align:'center'}
		                ,{field:'interviewer_id', title: '主要面试官工号',align:'center'}
		                ,{field:'position',title: '面试职位',align:'center'}
		                ,{field:'L1_name', title: '中心',align:'center'}
		                ,{field:'L2_name',  title: '部门',align:'center'}
		                ,{field:'recruiter_name',  title: '招聘负责人',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						
						/* layer.close(loading); */
					}
		        });
		    });
		  	layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.return_pg.count
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.return_pg.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      	init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 	//查询
    function doSearch(){
    	init_table();
    }
    //导出Excel表
 	function daochu(){
 	var myDate = new Date();
		var myyear = myDate.getFullYear();
		var mymonth = myDate.getMonth()+1;
		mymonth =(mymonth<10 ? "0"+mymonth:mymonth);
		var nowMonth = myyear+"-"+mymonth;
		var dailyReportDtime = $("#atual_hire_date").val();
		if(dailyReportDtime == null || dailyReportDtime == ""){
			dailyReportDtime = nowMonth+"~"+nowMonth;
		}
		//审批时间
		var Datetime = dailyReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var startmonth = alltime[0];
		var endmonth = alltime[1];
  	ExcalStats = true;
  	var interviewerid = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":count,"interviewerid":interviewerid,"startmonth":startmonth,"endmonth":endmonth};
       	$.ajax({
       		url: 'com.recruit.reports.departmentReports.selectDepartment.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				if(typeof(data.exception) == "undefined"||data.exception==null){
				
				if(ExcalStats){
	            	 //关闭导出入口
			        ExcalStats = false;
	            	//配置Excel表头内容
	            	var head = '{"interviewer_name":"主要面试官姓名","interviewer_id":"主要面试官工号","position":"面试职位","L1_name":"中心",'
	            	+'"L2_name":"L2_name","recruiter_name":"招聘负责人"}';
	            	//获取Excel表内容
	            	var jsonObject = JSON.stringify(data.return_department);
	            	//获取Excel工作簿名称
	            	var sheetName = "部门主要面试官名单";
	            	//获取Excel文件名称
	            	var fileName = "部门主要面试官名单";
	            	//保存到表单中
	            	document.getElementById("jsonObject").value = jsonObject;
 					document.getElementById("head").value = head;
		            document.getElementById("sheetName").value = sheetName;
 					document.getElementById("fileName").value = fileName; 
 					//导出Excel表
		            var form = document.getElementById("formExcel");
				    form.action = "com.recruit.reports.deriveExcel.flow";
				    form.submit(); 
				   }
				}
			
			}
		})
    }
     function resetRecr(){
    	document.getElementById("form1").reset();
    	$("#recruiter_name").attr("data","");
    	
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
