<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-15 14:28:54
  - Description:
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	.form-inline .form-control {
    width: 45%;
}
</style>
<title>招聘任务日报表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘任务日报表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>招聘负责人：</label> 
							<input id="recruiter_name" name="recruiter_name" onclick="getRecruiterName(this)"
								class="form-control" data="">
						</td>
						<td>
				      		<label>日期：</label>  
					        <input type="text" class="form-control" id="birthTime" name="birthTime" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
					       
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-defaul btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	 var ExcalStats = false;
	var datetime = new Date();
	datetime.setTime(datetime.getTime()-24*60*60*1000);
	var day = datetime.getFullYear()+"-" + (datetime.getMonth()+1) + "-" + datetime.getDate();
	
	$(function(){
	 	//时间控件
    	layui.use(['laydate','layer'], function(){
		  var laydate = layui.laydate,
		  layer = layui.layer;
		  //执行一个laydate实例
		 laydate.render({ 
			  elem: '#birthTime'
			  ,type: 'date'
			  ,range: '~' //或 range: '~' 来自定义分割字符
			});
		});
	 	
	 	init_table();
	});
	
	
	function init_table(){
		//日期
		var birthTime = $("#birthTime").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		var userid="";
		if(recruiterId =="" || recruiterId ==null  ){
			userid=null;
		}else{
			userid=recruiterId;
		}
		
		var json = {"date":birthTime,"recruiterId":userid};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.dailyReport.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.Report
		            ,height:470
		            ,limit:data.Report.length
		            ,cols: [[
		                {field:'recruiter_name', width:'10%', title: '招聘负责人',sort: true,align:'center'}
		                ,{field:'L1_name', width:'20%', title: '中心/部门/分公司',sort: true,align:'center'}
		                ,{field:'position', width:'10%', title: '职位',sort: true,align:'center'}
		                ,{field:'current_order', width:'16%', title: '当前时间段招聘中的工单数',sort: true,align:'center'}
		                
		                ,{field:'reciived_resumes', width:'10%', title: '收到简历',sort: true,align:'center'}
		                ,{field:'searched_resumes', width:'10%', title: '搜索简历',sort: true,align:'center'}
		                ,{field:'selected_resumes', width:'10%', title: '初筛简历数',sort: true,align:'center'}
		                ,{field:'phone_interview', width:'10%', title: '电话面试',sort: true,align:'center'}
		                
		                ,{field:'hr_interview', width:'12%', title: '人力资源现场面试',sort: true,align:'center'}
		                ,{field:'department_interview', width:'10%', title: '部门现场面试',sort: true,align:'center'}
		                ,{field:'included_talent_pools', width:'14%', title: '可计入人才库简历数量',sort: true,align:'center'}
		                ,{field:'offer', width:'10%', title: '录用人数',sort: true,align:'center'}
		               
		            ]]
		            ,done: function(res){
		           		CSStotal();//设置合计的css样式
						/* layer.close(loading); */
					}
		        });
		    });
			}
       });
	} 
 //查询
    function doSearch(){
    	init_table();
    }
  //
  function daochu(){
  	ExcalStats = true;
  	//日期
		var birthTime = $("#birthTime").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		var userid="";
		if( recruiterId =="" || recruiterId ==null ){
			userid=null;
		}else{
			userid=recruiterId;
		}
		
		var json = {"date":birthTime,"recruiterId":userid};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.dailyReport.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			
				if(ExcalStats){
        	 		//关闭导出入口
			        ExcalStats = false;
	            	//配置Excel表头内容
	            	var head = '{"recruiter_name":"招聘负责人","L1_name":"中心/部门/分公司","position":"职位","current_order":"当前时间段招聘中的工单数",'
	            	+'"reciived_resumes":"收到简历","searched_resumes":"搜索简历","selected_resumes":"初筛简历数","phone_interview":"电话面试",'
	            	+'"hr_interview":"人力资源现场面试","department_interview":"部门现场面试","included_talent_pools":"可计入人才库简历数量","offer":"录用人数"}';
	            	//获取Excel表内容
	            	var jsonObject = JSON.stringify(data.Report);
	            	//获取Excel工作簿名称
	            	var sheetName = "招聘任务日报表";
	            	//获取Excel文件名称
	            	var fileName = "招聘任务日报表";
	            	//保存到表单中
	            	document.getElementById("jsonObject").value = jsonObject;
 					document.getElementById("head").value = head;
		            document.getElementById("sheetName").value = sheetName;
 					document.getElementById("fileName").value = fileName; 
 					//导出Excel表
		            var form = document.getElementById("formExcel");
				    form.action = "com.recruit.reports.deriveExcel.flow";
				    form.submit(); 
				    }
			
			}
		})
    }
    
     //合计的css样式
    function CSStotal(){
    	$(".layui-table-cell").each(function(){
    		if($(this).text()=="合计"){
    			$(this).parent().parent().css("background-color","#ffe2b6");
    			$(this).parent().parent().css("font-weight","bold");
    		}
    	});
    	$(".layui-table-cell").each(function(){
    		if($(this).text()=="总计"){
    			$(this).parent().parent().css("background-color","#ffe2b6");
    			$(this).parent().parent().css("font-weight","bold");
    		}
    	});
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
