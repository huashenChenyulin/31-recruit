<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wj
  - Date: 2018-05-14 10:03:57
  - Description:内推渠道汇总表
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>内推渠道汇总表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>内推渠道汇总表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>月份:</label> 
							<div class="layui-input-inline">
						       <input type="text" class="layui-input" id="month" placeholder="yyyy-MM">
						     </div>
						</td>
						<!--<td>
				      		<label>工作日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="~">
						</td>-->
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	init_table();
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	} 
	});
	
	
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].DICTID+'">'+data.data[i].DICTNAME+'</option>');
			 	}
			}
       });
	}
	
	function init_laydate(){
		// 审批日期
	    layui.use('laydate', function(){
		  	var laydate = layui.laydate;
		  	 laydate.render({
				    elem: '#month'
				    ,type: 'month'
				  });
		});
	}
	
	
	function init_table(){
		//工作时间
		//var dailyReportDtime = $("#atual_hire_date").val();
		//获取当前月份
		var myDate = new Date();
		var myyear = myDate.getFullYear();
		var mymonth = myDate.getMonth()+1;
		mymonth =(mymonth<10 ? "0"+mymonth:mymonth);
		var nowMonth = myyear+"-"+mymonth;
		var month = $("#month").val();
		if(month == null || month == ""){
			month = nowMonth;
		}
		var json = {"curr":page,"limit":limit,"month":month};
       	$.ajax({
       		url: 'com.recruit.reports.recommendReports.selectRecommendResource.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.resourceDB
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'dictName', title: '渠道类型',align:'center'}
		                ,{field:'allresume',  title: '总计收到简历 / 份',align:'center'}
		                ,{field:'ableresume', title: '有效简历 / 份',align:'center'}
		                ,{field:'allableresume', title: '总计录用 / 份',align:'center'}
		                ,{field:'entrycriteria', title: '总计入职 / 份',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
		            	if(data.isNo == "NO"){
		            	}else{
		            	}
						/* layer.close(loading); */
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.resourceDB.length
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.resourceDB.length]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
	 //查询
    function doSearch(){
    	init_table();
    }
  function daochu(){
  	ExcalStats = true;
  	//招聘负责人
		var month = $("#month").val();
		var json = {"curr":page,"limit":count,"month":month};
       	$.ajax({
       		url: 'com.recruit.reports.recommendReports.selectRecommendResource.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"dictName":"渠道类型","allresume":"总计收到简历 / 份","ableresume":"有效简历 / 份","allableresume":"总计录用 / 份","entrycriteria":"总计入职 / 份"}';
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.resourceDB);
		            	//获取Excel工作簿名称
		            	var sheetName = "内推渠道汇总表";
		            	//获取Excel文件名称
		            	var fileName = "内推渠道汇总表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			}
			}
		});
    }
   //报表的重置方法
   function resetRecr(){
    	document.getElementById("form1").reset();
    	$("#recruiter_name").attr("data","");
    	
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
