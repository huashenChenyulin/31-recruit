<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%
//获取当前用户的角色ID
	String roleList = (String)DataContextManager.current().getMUODataContext().getUserObject().getAttributes().get("roleList");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wj
  - Date: 2018-05-14 10:03:57
  - Description:内推简历跟进表
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		    width: 43%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>内推简历跟进表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>内推简历跟进表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td style="width: 20%;">
							<label>招聘负责人:</label> 
							<input class="layui-input getStaff" name="recruiter_name" id="recruiter_name" onclick="getStaff(this)" data="" 
							  value="" type="text" style="border-color: #cccccc;display:inline-block;height: 34px;width: 60%;">
						</td>
						<td style="width: 20%;">
					       <label>所属公司:</label> 
							<select id="workplace"name="workplace" style="width: 60%;" class="form-control reset">
								<option value="">请选择公司</option>
							</select>
						</td>
						<td style="width: 25%;">
				      		<label>审批日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd" style="width: 78%;">
						</td>
						
						<td align="right" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	//获取招聘负责人角色信息
	var role = '<%=roleList %>';
	//招聘负责人角色信息转数组
	var RoleList = role.split(",");
	//是否具备招聘报表管理员权限
	var status = "false";
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	
	$(function(){
		init_laydate();// 初始化时间控件
	 	//init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	//isHaveAuthority () 判断是否具备招聘报表管理员权限
		queryCompany(isHaveAuthority ());//初始化操作员负责的所属公司下拉框
	 	init_table();
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
	 	} 
	});
	
	//判断是否具备管理权限
	function isHaveAuthority (){
		//设置查看权限
		for(var i = 0;i<RoleList.length;i++){
			if("1131"===RoleList[i]||1131===RoleList[i]){
				status = "true";
				return status;
			}
		}
	}
		//查询所属公司
	function queryCompany(status){
		var json = {"status" : status};
		$.ajax({
			url:'com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryUserCompany.biz.ext',
			type:'POST',
			data:json,
			dataType:'json',
			async:false, 
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					//动态渲染期望公司
					var option = "";
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						option+="<option value="+data.RecruitCompany[i].company_id+">"+data.RecruitCompany[i].company_name+"</option>";
					}
					$("#workplace").append(option);
				} 
			}
		});
	}
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].DICTID+'">'+data.data[i].DICTNAME+'</option>');
			 	}
			}
       });
	}
	
	function init_table(){
		//工作时间
		var ReportDtime = $("#atual_hire_date").val();
		//所属公司
		var company =  $("#workplace").val();
		var Datetime = ReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var starttime = alltime[0];
		var n = alltime[1];
   		var endtime = dateAddDays(n,1);
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":limit,"startapprovedtime":starttime,"endapprovedtime":endtime,"recommrecruiterid":recruiterId,"company":company,"status":status};
       	$.ajax({
       		url: 'com.recruit.reports.recommendReports.selectRcommendRe.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				console.log(data);
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.return_re
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'approvedata', width:'10%', title: '审批日期',sort: true,align:'center'}
		                ,{field:'suggestedRecruiterName', width:'10%', title: '招聘负责人',align:'center'}
		                ,{field:'recommenderName', width:'8%', title: '推荐人姓名',align:'center'}
		                ,{field:'recommenderPhone', width:'10%', title: '推荐人联系电话',align:'center'}
		                ,{field:'recommendedType', width:'8%', title: '内推类型',align:'center'}
		                ,{field:'recommendresource', width:'10%', title: '内推渠道类型',align:'center'}
		                //,{field:'actulalRecruiterName', width:'10%', title: '实际招聘负责人',align:'center'}
		                ,{field:'orgcentreDept', width:'28%', title: '应聘部门',align:'center'}
		                ,{field:'candicateName', width:'8%', title: '候选人姓名',align:'center'}
		                ,{field:'candicatePhone', width:'10%', title: '候选人联系电话',align:'center'}
		                ,{field:'recommendedPosition', width:'10%', title: '面试职位',align:'center'}
		                ,{field:'isSuitable', width:'9%', title: '简历是否合适',align:'center'}
		                ,{field:'recuitOrderId', width:'15%', title: '工单号',align:'center'}
		                ,{field:'recruiterName', width:'10%', title: '工单招聘负责人',align:'center'}
		                ,{field:'agreementPh', width:'10%', title: '电话面试意见',align:'center'}
		                ,{field:'agreementHr', width:'10%', title: 'HR面试意见',align:'center'}
		                ,{field:'agreementEm', width:'12%', title: '业务部门面试意见',align:'center'}
		                ,{field:'offerStatus', width:'10%', title: '是否发送offer',align:'center'}
		                ,{field:'atualHireDate', width:'10%', title: '入职日期',align:'center'}
		                ,{field:'roStatus', width:'9%', title: '是否跟进完成',align:'center'}
		                ,{field:'sfyintroducer', width:'10%', title: '转正日期',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						if(ExcalStats){
							daochuExcal('personal','dlink','内推简历跟进表');
						}
						/* layer.close(loading); */
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.return_pg.count
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.return_pg.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 	//查询
    function doSearch(){
    	init_table();
    }
    
  	function daochu(){
  		ExcalStats = true;
  		//招聘负责人
		var ReportDtime = $("#atual_hire_date").val();
		//所属公司
		var company =  $("#workplace").val();
		var Datetime = ReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var starttime = alltime[0];
		var n = alltime[1];
   		var endtime = dateAddDays(n,1);
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":count,"startapprovedtime":starttime,"endapprovedtime":endtime,"recommrecruiterid":recruiterId,"company":company,"status":status
				,head : '{"approvedata":"审批日期","suggestedRecruiterName":"招聘负责人","recommenderName":"推荐人姓名","recommenderPhone":"推荐人联系电话","recommendedType":"内推类型","recommendresource":"内推渠道类型","orgcentreDept":"应聘部门","candicateName":"候选人姓名","candicatePhone":"候选人联系电话","recommendedPosition":"面试职位","isSuitable":"简历是否合适","recuitOrderId":"工单号","recruiterName":"工单招聘负责人","agreementPh":"电话面试意见","agreementHr":"HR面试意见","agreementEm":"业务部门面试意见","offerStatus":"是否发送offer","atualHireDate":"入职日期","roStatus":"是否跟进完成","sfyintroducer":"转正日期"}'
		      ,sheetName : "内推简历跟进表"
		      ,fileName : "内推简历跟进表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createRcommendReExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		});
    }
 	//报表的重置方法
    function resetRecr(){
    	document.getElementById("form1").reset();
    	$("#recruiter_name").attr("data","");
    	
    }
    
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
