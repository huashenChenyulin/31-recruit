<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%
//获取当前用户的角色ID
	String roleList = (String)DataContextManager.current().getMUODataContext().getUserObject().getAttributes().get("roleList");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wj
  - Date: 2018-05-14 10:03:57
  - Description:内推奖励跟踪报页面
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		    width: 39%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	.layui-span{
		width: 20px;
	}
	
</style>
<title>内推奖金奖励表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>内推奖金奖励表	</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td style="width: 20%;">
							<label>推荐人:</label> 
							<input class="layui-input getStaff" name="recruiter_name" id="recruiter_name" onclick="getEntryStaff(this)" data="" 
							 placeholder="人员选择" value="" type="text" style="border-color: #cccccc;display:inline-block;height: 34px;width: 62%;">
						</td>
						<td style="width: 20%;">
					       <label>所属公司:</label> 
							<select id="workplace"name="workplace" style="width: 60%;" class="form-control reset">
								<option value="">请选择公司</option>
							</select>
						</td>
						<td style="width: 25%;">
				      		<label>奖励达到日期:</label>  
					        <input type="text"class="form-control"  id="atual_hire_date"name="atual_hire_date" style="width: 70%;" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						
						<td align="right" colspan="6" >
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	//获取招聘负责人角色信息
	var role = '<%=roleList %>';
	//招聘负责人角色信息转数组
	var RoleList = role.split(",");
	//是否具备招聘报表管理员权限
	var status = "false";
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	
	$(function(){
		init_laydate();// 初始化时间控件
		//isHaveAuthority () 判断是否具备招聘报表管理员权限
		queryCompany(isHaveAuthority ());//初始化操作员负责的所属公司下拉框
	 	//init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	init_table();
	 	/* for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	}  */
	});
	//判断是否具备管理权限
	function isHaveAuthority (){
		//设置查看权限
		for(var i = 0;i<RoleList.length;i++){
			if("1131"===RoleList[i]||1131===RoleList[i]){
				status = "true";
				return status;
			}
		}
	}
		//查询所属公司
	function queryCompany(status){
		var json = {"status" : status};
		$.ajax({
			url:'com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryUserCompany.biz.ext',
			type:'POST',
			data:json,
			dataType:'json',
			async:false, 
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					//动态渲染期望公司
					var option = "";
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						option+="<option value="+data.RecruitCompany[i].company_id+">"+data.RecruitCompany[i].company_name+"</option>";
					}
					$("#workplace").append(option);
				} 
			}
		});
	}
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].DICTID+'">'+data.data[i].DICTNAME+'</option>');
			 	}
			}
       });
	}
	
	function init_table(){
		//工作时间
		var ReportDtime = $("#atual_hire_date").val();
		var company =  $("#workplace").val();
		var startDate = "";
		var endDate = "";
		if(ReportDtime == "" || ReportDtime == null){
   		}else{
	   			var Datetime = ReportDtime.replace(/\s+/g,"");//去空格
				var alltime = Datetime.split("~");//拆分开始时间和结束时间
				var starttime = alltime[0];
				var sdata = starttime.split("-");//拆分所需要的字符
				var syear = sdata[0];
				var smonth = sdata[1];
				var sday = sdata[2];
				startDate = syear+smonth+sday;
				
				var n = alltime[1];
				var endtime = n;
				var edata = endtime.split("-");
				var eyear = edata[0];
				var emonth = edata[1];
				var eday = edata[2];
		   		endDate = eyear+emonth+eday;
   		}
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":limit,"startsfyintroducer":startDate,"endsfyintroducer":endDate,"recommenderid":recruiterId,"company":company,"status":status};
       	$.ajax({
       		url: 'com.recruit.reports.recommendReports.selectRcommendRewards.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.return_rewards
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'recruiter_name', width:'10%', title: '招聘负责人',align:'center'}
		                ,{field:'candicate_name', width:'10%', title: '姓名',align:'center'}
		                ,{field:'candicate_phone', width:'10%', title: '联系电话',align:'center'}
		                ,{field:'position', width:'15%', title: '入职岗位',align:'center'}
		                
		                ,{field:'L1_name', width:'10%', title: '入职中心',align:'center'}
		                ,{field:'L2_name', width:'10%', title: '入职部门',align:'center'}
		                ,{field:'sfybegindate', width:'10%', title: '入职时间',align:'center'}
		                ,{field:'recommender_name', width:'10%', title: '推荐人',align:'center'}
		                
		                ,{field:'recommender_id', width:'10%', title: '推荐人工号',align:'center'}
		                ,{field:'sfyintroducer', width:'10%', title: '奖励到达时间',templet: '<div>{{toDate(d.sfyintroducer)}}</div>',align:'center'}
		                ,{field:'recommended_rewards', width:'10%', title: '推荐奖励  / 元',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						/* layer.close(loading); */
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.return_pg.count
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.return_pg.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 	//查询
    function doSearch(){
    	init_table();
    }
    
    function toDate(rdate){
    	if(rdate == "" || rdate == null){
    		return "";
    	}else{
    		var year = rdate.substring(0, 4);
    		var month = rdate.substring(4, 6);
    		var day = rdate.substring(6,8);
    		var date = year+"-"+month+"-"+day;
    		return date;
    	}
    }
    
  	function daochu(){
  	ExcalStats = true;
  	//工作时间
		var ReportDtime = $("#atual_hire_date").val();
		var company =  $("#workplace").val();
		var startDate = "";
		var endDate = "";
		if(ReportDtime == "" || ReportDtime == null){
   		}else{
	   			var Datetime = ReportDtime.replace(/\s+/g,"");//去空格
				var alltime = Datetime.split("~");//拆分开始时间和结束时间
				var starttime = alltime[0];
				var sdata = starttime.split("-");//拆分所需要的字符
				var syear = sdata[0];
				var smonth = sdata[1];
				var sday = sdata[2];
				startDate = syear+smonth+sday;
				
				var n = alltime[1];
				var endtime = n;
				var edata = endtime.split("-");
				var eyear = edata[0];
				var emonth = edata[1];
				var eday = edata[2];
		   		endDate = eyear+emonth+eday;
   		}
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");	
		var json = {"curr":page,"limit":count,"startsfyintroducer":startDate,"endsfyintroducer":endDate,"recommenderid":recruiterId,"company":company,"status":status
				,head : '{"recruiter_name":"招聘负责人","candicate_name":"姓名","candicate_phone":"联系电话","position":"入职岗位",'
		            	+'"L1_name":"入职中心","L2_name":"入职部门","sfybegindate":"入职时间","recommender_name":"推荐人",'
		            	+'"recommender_id":"推荐人工号","sfyintroducer":"奖励到达时间","recommended_rewards":"推荐奖励  / 元"}'
		       ,sheetName : "内推奖金奖励表"
		      ,fileName : "内推奖金奖励表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createrecommendRewardsExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		})
    }
    function resetRecr(){
    	document.getElementById("form1").reset();
    	$("#recruiter_name").attr("data","");
    	
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
