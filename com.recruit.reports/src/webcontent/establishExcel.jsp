<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>

<%@ page import="org.codehaus.jettison.json.JSONObject"%>

<%@ page  import="java.io.FileOutputStream"%>
<%@ page  import="java.util.ArrayList"%>
<%@ page  import="java.util.Iterator"%>
<%@ page  import="java.util.List"%>
<%@ page  import="net.sf.json.JSONArray"%>
<%@ page  import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@ page  import="org.apache.poi.hssf.usermodel.HSSFCellStyle"%>
<%@ page  import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@ page  import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@ page  import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@ page  import="org.codehaus.jettison.json.JSONObject"%>
<%@ page  import="com.eos.system.annotation.Bizlet"%>
<%@ page  import="com.recruit.reports.JsonObjectDispose"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@page import="com.eos.web.taglib.util.*" %>
<%@page import="java.util.Set" %> 

<%		
		JsonObjectDispose JsonObjectDispose = new JsonObjectDispose();
		//上传保存路径
		 request.setCharacterEncoding("UTF-8");
		 response.setContentType("text/html;charset=UTF-8"); 
		//web路径
		String rootpath = request.getSession().getServletContext().getRealPath("");
		//文件保存路径
		String savePath = rootpath+"/";
		
	  Object root= com.eos.web.taglib.util.XpathUtil.getDataContextRoot("request", pageContext); 
      //Excel表头值
      String head=(String)XpathUtil.getObjectByXpath(root,"head");
      //Excel表值
      String jsonObject = (String)XpathUtil.getObjectByXpath(root,"jsonObject");
      //Excel表名称
	  String sheetName = (String)XpathUtil.getObjectByXpath(root,"sheetName");
	  //Excel文件名称
	  String fileName = (String)XpathUtil.getObjectByXpath(root,"fileName");
	  
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(sheetName);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		//设置默认宽度、高度
		sheet.setDefaultColumnWidth((short) 25);  
		sheet.setDefaultRowHeightInPoints(20);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
		
		//实例化一个map
		Map<String,Integer> map = new HashMap<String,Integer>();
		HSSFCell cell = row.createCell((short) 0);
		JSONObject JSONHead = new JSONObject(head);
		  Iterator iterator = JSONHead.keys();  
		  int h = 0;
		  while(iterator.hasNext()){  
		      String key = (String) iterator.next();  
		      String  value = JSONHead.getString(key);
		      map.put(key, h);
		      cell = row.createCell((short) h++);
		      cell.setCellValue(value);
		      cell.setCellStyle(style);
		  }  

		// 第五步，写入实体数据，
		List list = JsonObjectDispose.stringList(jsonObject);//将传入字符串转换为List集合的json对象集合
		for (int i = 0; i < list.size(); i++) {
			net.sf.json.JSONObject job = (net.sf.json.JSONObject) list.get(i);
			
			row = sheet.createRow((int) i + 1);
			Iterator jobiterator = job.keys();
			Set<String> keys = map.keySet();
			String name = "";
			for(String key : keys){
					String  value = job.getString(key);
					//判断是否为null是转换为""
					if(value==null||value=="null"){
						value = "";
					}
					
					//如果key值是渠道，需要对value值进行转换
					if(key=="recruitChannels"||key.equals("recruitChannels")){
						if(value!=""&&value!=null){
						System.out.println(key);
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_channel", value);
						}
					}
					
					//如果key值是供应商，需要对value值进行转换
					if(key=="recruitSuppliers"||key.equals("recruitSuppliers")){
						if(value!=""&&value!=null){
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_provider", value);
						}
					}
					
					//如果key值是职位类别，需要对value值进行转换
					if(key=="jobCategory"||key.equals("jobCategory")){
						if(value!=""&&value!=null){
						value = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("category", value);
						}
					}
					
					int j = map.get(key);
					//回填表格值
					cell =row.createCell((short) j);
					cell.setCellValue(value);
					//设置表格值水平居中
					cell.setCellStyle(style);
			}
			
		}
		
		String path = fileName+".xls";
		// 第六步，将文件存到指定位置
		try {
			FileOutputStream fout = new FileOutputStream(savePath+fileName+".xls");
			wb.write(fout);
			fout.close();
			/* response.getWriter().print(savePath+fileName+".xls"); */
		} catch (Exception e) {
			e.printStackTrace();
		}

 %>
 
 <form id="form1" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
</form>
			
 <script type="text/javascript">
 		document.getElementById("downloadpath").value = "<%=path%>";
 		document.getElementById("fileName").value = "<%=fileName%>";
	 	var form = document.getElementById("form1");
        form.action = "com.recruit.reports.deriveExcel.exportExcel.action";
        form.submit();
 </script>

<!-- 
  - Author(s): 唐能
  - Date: 2018-06-04 10:42:12
  - Description:
-->
