<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-05-23 10:03:57
  - Description:中心编制招聘情况报表
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		width: 43%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>中心编制和招聘情况分析表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>中心编制和招聘情况分析表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>中心/公司:</label> 
							<select id="centre"name="centre" class="form-control" style="width: 200px;"lay-verify="required">
						</select>
						</td>
						<td>
				      		<label>工作日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	</div>
		<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	
/* 	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
 */	var table;
 var ExcalStats = false;
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_select_fromDict("centre");// 初始化查询职位类别
	 	init_table();
	});
	
	
	// 初始化中心公司
	function init_select_fromDict(selectId){
		// 初始化中心公司
       	$.ajax({
       		url: 'com.recruit.reports.monthAuthorized.queryCentre.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].orgid+'">'+data.data[i].orgname+'</option>');
			 	}
			}
       });
	}
	
	function init_table(){
		//工作时间
		var date = $("#atual_hire_date").val();
		//
		var ORGID = $("#centre").val();
		var json = {/* "curr":page,"limit":limit, */"date":date,"ORGID":ORGID};
       	$.ajax({
       		url: 'com.recruit.reports.monthAuthorized.monthAuthorized.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			 
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.authorized
		            ,limit:data.authorized.length
		            ,height:480
		            ,cols: [[
		                {field:'L2Name', width:'20%', title: '部门',align:'center'}
		                ,{field:'eamount', width:'10%', title: '满编人数',sort: true,align:'right'}
		                ,{field:'oamount', width:'10%', title: '在职人数', sort: true,align:'right'}
		                ,{field:'emptyPlait', width:'10%', title: '空编人数', sort: true,align:'right'}
		                ,{field:'manpower', width:'10%', title: '人力需求数', sort: true,align:'right'}
		                ,{field:'hire', width:'10%', title: '录用人数', sort: true,align:'right'}
		                ,{field:'cancel', width:'10%', title: '取消招聘单数', sort: true,align:'right'}
		                ,{field:'awaitOffer', width:'11%', title: '等待候选人确认', sort: true,align:'right'}
		                ,{field:'abandon', width:'10%', title: '候选人放弃数', sort: true,align:'right'}
		                ,{field:'openPositions', width:'10%', title: '待招人数', sort: true,align:'right'}
		                ,{field:'supercycle', width:'16%', title: '超过招聘周期未完成工单数', sort: true,align:'right'}
		            ]]
		             
		            ,done: function(res, curr, count){
		           		CSStotal();//定义合计的css样式
						/* layer.close(loading); */
					}
		        });
		    });
		   
			}
       });
      
	} 
 //查询
    function doSearch(){
    	init_table();
    	
    }
  //
  function daochu(){
  	ExcalStats = true;
  		//工作时间
		var date = $("#atual_hire_date").val();
		//
		var ORGID = $("#centre").val();
		var json = {/* "curr":page,"limit":limit, */"date":date,"ORGID":ORGID};
       	$.ajax({
       		url: 'com.recruit.reports.monthAuthorized.monthAuthorized.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				
						if(ExcalStats){
						//关闭导出入口
				        ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"L2Name":"部门","eamount":"满编人数","oamount":"在职人数","emptyPlait":"空编人数","manpower":"人力需求数","hire":"录用人数","cancel":"取消招聘单数","awaitOffer":"等待候选人确认","abandon":"候选人放弃数","openPositions":"待招人数","supercycle":"超过招聘周期未完成工单数"}';
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.authorized);
		            	//获取Excel工作簿名称
		            	var sheetName = "中心编制和招聘情况分析表";
		            	//获取Excel文件名称
		            	var fileName = "中心编制和招聘情况分析表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			
			}
			}
		})
    }
    
    //合计的css样式
    function CSStotal(){
    	$(".layui-table-cell").each(function(){
    		if($(this).text()=="合计"){
    			$(this).parent().parent().css("background-color","#ffe2b6");
    			$(this).parent().parent().css("font-weight","bold");
    		}
    	});
    }
    
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
