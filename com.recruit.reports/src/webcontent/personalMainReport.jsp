<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): yejing
  - Date: 2018-05-02 10:03:57
  - Description:个人主报表主页面
-->
<head>
<title>招聘任务表</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
<style type="text/css">
.form-inline label {
	width: 100px;
}

.form-control {
	margin-bottom: 10px;
}
.btn {
	    margin-right: 15px;
}
.form-inline .form-control{
	    width: 47%;
}
 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘任务表</legend>

			</fieldset>
	<div class="panel panel-default">
		<div class="panel-body" style="width: 100%; height: 100%;">

			<form id="form1" class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td><label>审批日期:</label> <input type="text"
							class="form-control" id="audit_time" name="audit_time"
							placeholder="yyyy-MM-dd ~ yyyy-MM-dd"></td>

						<td><label>发offer日期:</label> <input type="text"
							class="form-control" id="offer_time" name="offer_time"
							placeholder="yyyy-MM-dd ~ yyyy-MM-dd"></td>

						<td style="width: 35%;"><label>入职日期:</label> <input type="text"
							class="form-control" id="atual_date" name="atual_date"
							placeholder="yyyy-MM-dd ~ yyyy-MM-dd"></td>
					</tr>

					<tr>
						<td><label>招聘负责人:</label> <input id="recruiter_name"
							name="recruiter_name" class="form-control" data="" type="text"
							onclick="getRecruiterName(this)"></td>

						<td><label>招聘状态:</label> <select id="ro_status"
							name="ro_status" class="form-control" >
								<option value="">请选择招聘状态</option>
						</select></td>

						<td><label>职位类别：</label> <select id="job_category"
							name="job_category" class="form-control" >
								<option value="">请选择职位类别</option>
						</select></td>
					</tr>

					<tr>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch"
								onclick="doSearch()">搜 索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重
								置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink" style="display: none;"></a>
						</td>
					</tr>

				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table class="layui-hide" id="personal"></table>
		</div>
		<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	$(function(){
		init_hiredate();// 初始化入职时间控件
		init_laydate();// 初始化时间控件
	 	init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	init_table();
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	} 
	});
	
	
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//debugger
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].dictID+'">'+data.data[i].dictName+'</option>');
			 	}
			}
       });
	}
	function init_table(){
		//审批时间
		var AuditTime = $("#audit_time").val();
		//发offer日期
		var offerTime = $("#offer_time").val();
		//入职时间
		var AtualHireDate = $("#atual_date").val();
		//招聘状态
		var roStatus = $("#ro_status").val();
		//职位类别
		var jobCategory = $("#job_category").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		var json = {"curr":page,"limit":limit,"AuditTime":AuditTime,"offerTime":offerTime,"AtualHireDate":AtualHireDate,"jobCategory":jobCategory,"roStatus":roStatus,"recruiterId":recruiterId};
       	$.ajax({
       		url: 'com.recruit.reports.mainReports.queryDailyRecuitOrder.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.RecuitOrder//传入值（对象数组）
					,limit:limit //展示条目数
		            ,height:450
		            ,cols: [[
		                {field:'recuitOrderId', width:219, title: '单号', sort: true,align:'center'}
		                ,{field:'recruiterName', width:105, title: '招聘负责人',align:'center'}
		                ,{field:'l1Name', width:185, title: '中心(L1)', sort: true,align:'center'}
		                ,{field:'l2Name', width:185, title: '部门(L2)',align:'center'}
		                
		                ,{field:'l3Name', width:185,title: '科室(L3)',align:'center'}
		                ,{field:'l4Name', width:185, title: '科组(L4)', sort: true,align:'center'}
		                ,{field:'position', width:220, title: '需求职位', sort: true,align:'center'}
		                ,{field:'jobCategory', width:290, title: '职位类别',templet: '<div>{{Positioncatagory(d.jobCategory)}}</div>',align:'center'}
		                
		                ,{field:'recruitPeriod', width:147, title: '标准招聘周期(天)',sort: true,align:'center'}
		                ,{field:'workArea', width:257, title: '工作地点', sort: true,align:'center'}
		                ,{field:'auditTime', width:140, title: '审批日期', sort: true,align:'center'}
		                ,{field:'mainReason', width:140, title: '招聘原因', sort: true,align:'center'}
		               
		                ,{field:'resignedName', width:145, title: '离职人员姓名', sort: true,align:'center'}
		                ,{field:'roStatus', width:158, title: '招聘状态', sort: true,align:'center'}
		                ,{field:'reciivedResumes', width:125, title: '收到简历数', sort: true,align:'right'}
		                ,{field:'searchedResumes', width:125, title: '搜索简历数', sort: true,align:'right'}
		                
		                ,{field:'selectedResumes', width:125, title: '初筛简历数', sort: true,align:'right'}
		                ,{field:'phoneInterview', width:125, title: '电话面试', sort: true,align:'right'}
		                ,{field:'hrInterview', width:125, title: '人力资源面试', sort: true,align:'right'}
		                ,{field:'departmentInterview', width:125, title: '部门面试', sort: true,align:'right'}
		                
		                ,{field:'includedTalentPools', width:150, title: '可计入人才库数', sort: true,align:'right'}
		                ,{field:'recruitName', width:140, title: '入职人员姓名', sort: true}
		                ,{field:'isIntern', width:120, title: '是否实习生',sort: true,align:'center'}
		                ,{field:'offerTime', width:120, title: '发offer日期', sort: true, align:'center'}
		                ,{field:'endTime', width:120, title: '待入职日期', sort: true, align:'center'}
		                
		                ,{field:'actualRecruitment_cycle', width:130, title: '实际招聘周期(天)', sort: true,align:'center'}
		                ,{field:'isStandard', width:110, title: '是否达标', sort: true,align:'center'}
		                ,{field:'atualHireDate', width:110, title: '入职日期', sort: true,align:'center'}
		                ,{field:'recruitChannels', width:110, title: '招聘渠道', sort: true,templet: '<div>{{getChannels(d.recruitChannels)}}</div>',align:'center'}
		                
		                ,{field:'recruitSuppliers', width:110, title: '供应商', sort: true,templet: '<div>{{getSuppliers(d.recruitSuppliers)}}</div>',align:'center'}
		                ,{field:'isDifficult', width:110, title: '是否难招', sort: true,align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count //总条目数
				    ,limit:limit//展示条目数
				    ,limits : [10, 20, 30, 40, 50,data.PageCond.count]//条目数选择
				    ,curr : page //当前页数
				    ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
				    ,jump: function(obj,first){
				    	count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
    
    function doSearch(){
    	init_table();
    }
      function daochu(){
  		ExcalStats = true;
  		//审批时间
		var AuditTime = $("#audit_time").val();
		//发offer日期
		var offerTime = $("#offer_time").val();
		//入职时间
		var AtualHireDate = $("#atual_date").val();
		//招聘状态
		var roStatus = $("#ro_status").val();
		//职位类别
		var jobCategory = $("#job_category").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		var json = {"curr":page,"limit":count,"AuditTime":AuditTime,"offerTime":offerTime,"AtualHireDate":AtualHireDate,"jobCategory":jobCategory,"roStatus":roStatus,"recruiterId":recruiterId
				,head : '{"recuitOrderId":"单号","recruiterName":"招聘负责人","l1Name":"中心(L1)","l2Name":"部门(L2)",'
		            	+'"l3Name":"科室(L3)","l4Name":"科组(L4)","position":"需求职位","jobCategory":"职位类别",'
		            	+'"recruitPeriod":"标准招聘周期(天)","workArea":"工作地点","auditTime":"审批日期","mainReason":"招聘原因",'
		            	+'"resignedName":"离职人员姓名","roStatus":"招聘状态","reciivedResumes":"收到简历数","searchedResumes":"搜索简历数",'
		            	
		            	+'"selectedResumes":"初筛简历数","phoneInterview":"电话面试","hrInterview":"人力资源面试","departmentInterview":"部门面试",'
		            	+'"includedTalentPools":"可计入人才库简历数量","recruitName":"入职人员姓名","isIntern":"是否实习生","offerTime":"发offer日期","endTime":"待入职日期",'
		            	
		            	+'"actualRecruitment_cycle":"实际招聘周期(天)","isStandard":"是否达标","atualHireDate":"入职日期","recruitChannels":"招聘渠道",'
		            	+'"recruitSuppliers":"供应商","isDifficult":"是否难招"}'
		      ,sheetName : "招聘任务表"
		      ,fileName : "招聘任务表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createPersonalMainReportExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		})
    }
    
    //清除招聘负责人
    function resetRecr_name(){
    	$("#recruiter_name").attr("data","");
    }
    
    function init_hiredate(){
			// 日期
		    layui.use('laydate', function(){
			  	var laydate = layui.laydate;
			  	$("#atual_date").each(function(index,element){
			  		// 执行一个laydate实例
			  		var id = element.id;
				  	laydate.render({
				    	elem: '#'+id //指定元素
				    	,range:'~'
				  	});
			  	});
			});
		}
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
