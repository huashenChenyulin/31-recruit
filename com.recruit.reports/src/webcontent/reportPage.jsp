<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-17 17:18:19
  - Description:
-->
<head>
<link href="<%= request.getContextPath() %>/reports/css/report.css?v=1.0" rel="stylesheet">
<title>报表首页</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script src="<%= request.getContextPath() %>/css/js/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">
    	#wrap,#main{
    		width: 100%;
    		float: left;
    	}
    	
    	#main-left{
    		width: 23%;
    		height: 550px;	
    		float: left;
    		margin-left: 1%;
    		box-shadow: 0 2px 2px 0 rgba(51,51,51,.2);
    border-radius: 4px;
    	}
    	#main-right,#main-center,#main-school{
    		width: 23%;
    		height: 550px;	
    		float: left;
    		margin-left: 2%;
    		box-shadow: 0 2px 2px 0 rgba(51,51,51,.2);
    		border-radius: 4px;
    	}
    	
    	/* 左、中、右部分  */
    	#left-header,#center-header,#right-header,#school-header{
    		width: 86%;
		    height: 80px;
		    margin-left: auto;
		    margin-right: auto;
		    padding-top: 20px;
    	}
    	.header{
    		float:left;
    		border: 1px solid;
    		height: 60px;
    		width: 60px;
    		border-radius: 10000px;
    		background-color: #000000;
    	}
    	.header .img{
    		margin-left: 16px;
    		margin-top: 15px;
    	}
    	.span{
    		float:left;
    		font-size: 22px;
    		margin-top: 15px;
    		margin-left: 20px;
    	}
    	
    	#left-header .header{
    		background-color: #ffd97c;
    		border-color: #ffd97c;
    	}
    	
    	#center-header .header{
    		background-color: #589bd3;
    		border-color: #589bd3;
    	}
    	
    	#right-header .header{
    		background-color: #94d6c8;
    		border-color: #94d6c8;
    	}
    	
    	#school-header .header{
    		background-color: #2F4056;
    		border-color: #2F4056;
    	}
    	
    	.hr{
    		width: 86%;
    		margin-left: auto;
		    margin-right: auto;
    		border-bottom: 2px solid;
    		margin-bottom: 10px;
    	}
    	
    	
    	.content{
    		width: 86%;
		    margin-left: auto;
		    margin-right: auto;
    	}
    	.content a{
    		color: #337dbc;
    		text-decoration:none;
    	}
    	.content a:HOVER{
    		color: #337dbc;
    		text-decoration:underline;
    	}
    	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
    </style>
</head>
<body>
<div id="wrap">
	<div id="main">
		<div id="main-left">
			<div id="left-header">
				<div class="header">
					<img class="img" src="<%= request.getContextPath() %>/reports/images/business.png">
				</div>
				<div class="span">
					<span id="business_reports"></span>
				</div>
			</div>
			<div class="hr" style="border-color: #ffd97c;"></div>
			<div class="content business_reports">
				 
			</div>
		</div>
		<div id="main-center">
			<div id="center-header">
				<div class="header">
					<img class="img" src="<%= request.getContextPath() %>/reports/images/Analysis.png">
				</div>
				<div class="span">
					<span id="analyze_reports"></span>
				</div>
			</div>
			<div class="hr" style="border-color: #589bd3;"></div>
			<div class="content analyze_reports">
				 
			</div>
		</div>
		<div id="main-right">
			<div id="right-header">
				<div class="header">
					<img class="img" style="margin-left: 14px;" src="<%= request.getContextPath() %>/reports/images/channel.png">
				</div>
				<div class="span">
					<span id="channel_number"></span>
				</div>
			</div>
			<div class="hr" style="border-color: #94d6c8;"></div>
			<div class="content channel_number">
				 
			</div>
		</div>
		
		<div id="main-school">
			<div id="school-header">
				<div class="header">
					<img class="img" style="margin-left: 14px;" src="<%= request.getContextPath() %>/reports/images/school.png">
				</div>
				<div class="span">
					<span id="school_reports"></span>
				</div>
			</div>
			<div class="hr" style="border-color: #2F4056;"></div>
			<div class="content school_reports">
				 
			</div>
		</div>
		
	</div>
</div>



	<script type="text/javascript">
	
		/* var aList = document.getElementsByTagName('a');
		for(var i=0;i<aList.length;i++){
		   aList[i].target='_blank';//定义成打开新窗口
		} */
		
		var path ="<%= request.getContextPath() %>";
		
		selectReport();
		function selectReport(){
			$.ajax({
				url: 'com.recruit.reports.menuReports.queryMenuReports.biz.ext',
				type:'POST',
				success:function(data){
					for(var i=0;i<data.reports.length;i++){
						//判断等于业务报表
						if(data.reports[i].menuCode=="business_reports"){
							$("#business_reports").text(data.reports[i].menuName);
							//根据所对应的集合查询子数据并进行循环输出
							for(var j=0;j<data.reports[i].childrenMenuTreeNodeList.length;j++){
								$(".business_reports").append("<p><a href="+path+data.reports[i].childrenMenuTreeNodeList[j].linkAction+" target='_blank' >"+data.reports[i].childrenMenuTreeNodeList[j].menuName+"</a></p>");
							}
						}
						//判断等于分析报表
						if(data.reports[i].menuCode=="analyze_reports"){
							$("#analyze_reports").text(data.reports[i].menuName);
							//根据所对应的集合查询子数据并进行循环输出
							for(var j=0;j<data.reports[i].childrenMenuTreeNodeList.length;j++){
								$(".analyze_reports").append("<p><a href="+path+data.reports[i].childrenMenuTreeNodeList[j].linkAction+" target='_blank' >"+data.reports[i].childrenMenuTreeNodeList[j].menuName+"</a></p>");
							}
						}
						//判断等于渠道数据
						if(data.reports[i].menuCode=="channel_number"){
							$("#channel_number").text(data.reports[i].menuName);
							//根据所对应的集合查询子数据并进行循环输出
							for(var j=0;j<data.reports[i].childrenMenuTreeNodeList.length;j++){
								$(".channel_number").append("<p><a href="+path+data.reports[i].childrenMenuTreeNodeList[j].linkAction+" target='_blank' >"+data.reports[i].childrenMenuTreeNodeList[j].menuName+"</a></p>");
							}
						}
						//判断等于渠道数据
						if(data.reports[i].menuCode=="campus_recruitment"){
							$("#school_reports").text(data.reports[i].menuName);
							//根据所对应的集合查询子数据并进行循环输出
							for(var j=0;j<data.reports[i].childrenMenuTreeNodeList.length;j++){
								$(".school_reports").append("<p><a href="+path+data.reports[i].childrenMenuTreeNodeList[j].linkAction+" target='_blank' >"+data.reports[i].childrenMenuTreeNodeList[j].menuName+"</a></p>");
							}
						}
					}
				}
			});
		}
    </script>
</body>
</html>