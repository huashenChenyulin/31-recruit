<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-05-02 10:03:57
  - Description:日报表页面
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		    width: 43%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>日报表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘任务日明细表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form id="form1" class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>招聘负责人：</label> 
							<input id="recruiter_name" name="recruiter_name"
								class="form-control" type="text" onclick="getRecruiterName(this)">
						</td>
						<td>
				      		<label>工作日期：</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" id="onlyForEnterSearch" type="button" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_select_fromDict("job_category","category");// 初始化查询职位类别
	 	init_table();
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	} 
	});
	
	
	// 初始化查询职位类别
	function init_select_fromDict(selectId,dicttypeid){
		// 初始化查询职位类别
   		var json = {"dicttypeid":dicttypeid};
       	$.ajax({
       		url: 'com.primeton.task.taskoperation.selectCategory.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
		 		for(var i=0;i<data.data.length;i++){
					$("#"+selectId).append('<option value="'+data.data[i].DICTID+'">'+data.data[i].DICTNAME+'</option>');
			 	}
			}
       });
	}
	
	function init_table(){
		//工作时间
		var dailyReportDtime = $("#atual_hire_date").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		var json = {"curr":page,"limit":limit,"dailyReportDtime":dailyReportDtime,"recruiterId":recruiterId};
       	$.ajax({
       		url: 'com.recruit.reports.dayRepotts.queryDayReports.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.RecuitOrder
		            ,limit:limit
		            ,height:460
		            ,cols: [[
		                {field:'recruiterName', width:'10%', title: '招聘负责人',align:'center'}
		                ,{field:'dailyReportDtime', width:'10%', title: '工作日期',sort: true,align:'center'}
		                ,{field:'recuitOrderId', width:'16%', title: '单号', sort: true,align:'center'}
		                ,{field:'POSITION', width:'16%', title: '职位', sort: true,align:'center'}
		                
		                ,{field:'reciivedResumes', width:'10%', title: '收到简历数', sort: true,align:'center'}
		                
		                ,{field:'searchedResumes', width:'10%', title: '搜索简历数', sort: true,align:'center'}
		                ,{field:'selectedResumes', width:'10%', title: '初筛简历数', sort: true,align:'center'}
		                ,{field:'phoneInterview', width:'10%', title: '电话面试数', sort: true,align:'center'}
		                ,{field:'hrInterview', width:'11%', title: '人力资源面试数', sort: true,align:'center'}
		                
		                ,{field:'departmentInterview', width:'10%', title: '部门面试数', sort: true,align:'center'}
		                ,{field:'includedTalentPools', width:'12%', title: '可计入人才库数', sort: true,align:'center'}
		                ,{field:'offer', width:'10%', title: '录用人数',sort: true,align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						/* layer.close(loading); */
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.PageCond.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	init_table();
    }
  //
  function daochu(){
  	ExcalStats = true;
  	//工作时间
		var dailyReportDtime = $("#atual_hire_date").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		//文件需生成的路径
		//页面第一次加载,招聘负责人为操作员
		var json = {"curr":page,"limit":count,"dailyReportDtime":dailyReportDtime,"recruiterId":recruiterId
				,head : '{"recruiterName":"招聘负责人","dailyReportDtime":"工作日期","recuitOrderId":"单号","POSITION":"职位","reciivedResumes":"收到简历数",'
		            	+'"searchedResumes":"搜索简历","selectedResumes":"初筛简历数","phoneInterview":"电话面试","hrInterview":"人力资源现场面试"'
		            	+',"departmentInterview":"部门现场面试","includedTalentPools":"可计入人才库简历数量","offer":"录用人数"}'
		      ,sheetName : "招聘任务日明细表"
		      ,fileName : "招聘任务日明细表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createPersonalDayReportExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		})
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
