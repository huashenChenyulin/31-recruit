<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>		
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000074
  - Date: 2018-05-17 20:25:41
  - Description:
-->
<head>
<script src="<%= request.getContextPath() %>/home/js/echarts.common.min.js"></script>
<style type="text/css">
#echart{
	width:100%;
	height:400px;
}


</style>
<title>达标率</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘达标率</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form id="form1" class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>招聘负责人:</label> 
							<input class="form-control" id="recruiter_name" onclick="getRecruiterName(this)" data=""value="" type="text">
						
						</td>
						<td>
				      		<label>选择年份:</label>  
					        <input type="text"
								class="form-control" id="atual_date" 
								name="atual_hire_date" placeholder="">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="StandardRateToHead()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							
							
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div id="echart">
	</div>
	


	<script type="text/javascript">
	var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
	layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  
	  //执行一个laydate实例
	  laydate.render({
	    elem: '#atual_date' //指定元素
	    ,type: 'year'
	    
	  });
	});
	// 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart'));
     option = {
		    title: {
				        		show:true,
						        text: '达标率',
						        left: 10,
		        				top:10,
		        				textStyle:{
						            fontSize:20,
						            color:'#FFCC66',
						            fontWeight:700
						        },
						    },
		    tooltip : {
		        trigger: 'axis'
		    },
		    legend: {
		        data:['计件','计时']
		    },
		    toolbox: {
		        show : true,
		        feature : {
		            dataView : {show: true, readOnly: false},
		            saveAsImage : {show: true}
		        }
		    },
		    calculable : true,
		    xAxis : [
		        {
		            type : 'category',
		            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value',
		            axisLabel: {
		                formatter: '{value} %'
		            },
		            max:100
		        }
		    ],
		    series : [
		        {
		            name:'计件',
		            type:'bar',
		            data:[],
		            /* label:{
		        		show:true,
		        		formatter:'{c}%'
		        		} */
		            /* markPoint : {
		                data : [
		                    {type : 'max', name: '最大值'},
		                    {type : 'min', name: '最小值'}
		                ]
		            }, */
		            /*markLine : {
		                data : [
		                    {type : 'average', name: '平均值'}
		                ]
		            }*/
		        },
		        {
		            name:'计时',
		            type:'bar',
		            data:[],
		            /* label:{
		        		show:true,
		        		formatter:'{c}%'
		        		} */
		            /* markPoint : {
		                data : [
		                    {name : '年最高', type : 'max',},
		                    {name : '年最低', type : 'min',}
		                ]
		            }, */
		            /*markLine : {
		                data : [
		                    {type : 'average', name : '平均值'}
		                ]
		            }*/
		        },{
		            "name": "总数",
		            "type": "line",
		            "stack": "总量",
		            symbolSize:10,
		            symbol:'circle',
		            "itemStyle": {
		                "normal": {
		                    "color": "#1b76b8",
		                    "barBorderRadius": 0,
		                    "label": {
		                        "show": true,
		                        "position": "top",
		                        formatter: function(p) {
		                            return p.value > 0 ? (p.value) : '';
		                        }
		                    }
		                }
		            },
		            "data": []
		        },
		    ]
		};

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);   
        
        
        $(function(){
        	$('#recruiter_name').val(''),
        	StandardRate();
			
			
        
        });
        
	//达标率
        function StandardRate(){
        	
        	$.ajax({
           		url: 'com.recruit.reports.recuitConditionReports.queryStandardIcon.biz.ext',
				type:'POST',
				data:{},
				success:function(data){
					myChart.setOption({
				        series : [
						        {
						            data:data.byThePiece,
						            
						        },
						        {
						            
						            data:data.byTime,
						            
						        },{
						            "data": data.count,
						        },
						    ]
				    }); 
				}
			});
        
        }
        
        //根据条件查询
        function StandardRateToHead(){
        	
        	$.ajax({
           		url: 'com.recruit.reports.recuitConditionReports.queryStandardIconReports.biz.ext',
				type:'POST',
				data:{
					recruiterId:$('#recruiter_name').attr('data'),
					year:$('#atual_date').val()
				},
				success:function(data){
					myChart.setOption({
				        series : [
						        {
						            data:data.byThePiece,
						            
						        },
						        {
						            
						            data:data.byTime,
						            
						        },{
						            "data": data.count,
						        },
						    ]
				    }); 
				}
			});
        
        }
    </script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>