<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-10-15 08:52:08
  - Description:
-->
<head>
<title>学生面试评价报表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
	 /*报表的数据高度*/
    .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	.title {
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	.table1 td{
		line-height: 3;
	}
	.table1 label{
		width: 85px;
	}
</style>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>学生面试评价报表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;" class="table1">
					<tr>
						<td>
							<label>招聘计划：</label>
							<a id="showApplyName" title=""> <select id="recruitPlan" class="form-control" style="width: 170px;">
								<option value="">请选择招聘计划</option>
							</select></a>
						</td>
						<td>
							<label>推荐状态：</label>
							<select id="interview_advice" class="form-control" style="width: 170px;">
								<option value="">请选择推荐状态</option>
								<option value="推荐">推荐</option>
								<option value="待定">待定</option>
								<option value="不推荐">不推荐</option>
								<option value="未到场">未到场</option>
								<option value="无">无</option>
							</select>
						</td>
						<td>
				      		<label>户口所在地：</label>  
					        <input class="form-control" id="birthplace" style="width: 170px;">
						</td>
						
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr2()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button> 
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					<tr>
						<td>
							<label>姓名：</label> 
							<input id="studentName" class="form-control" data="" style="width: 170px;">
						</td>
						<td>
							<label>学校：</label> 
							<input id="school" class="form-control" data="" style="width: 170px;">
						</td>
						<td>
				      		<label>时间：</label>  
					        <input class="form-control" id="date" style="width: 170px;">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	
	
	<!-- 学历-->
	<script type="text/html" id="degree">
		{{ '<div class="title" title="'+educationstatus(d.degree)+'">'+educationstatus(d.degree)+'</div>' }}
	</script>
	<!-- 英语等级-->
	<script type="text/html" id="englishLevel">
		{{ '<div class="title" title="'+english_levels(d.englishLevel)+'">'+english_levels(d.englishLevel)+'</div>' }}
	</script>
	<!-- 照片-->
	<script type="text/html" id="resumePictureUrl">
		{{ resumePictureUrl(d.resumePictureUrl) }}
	</script>
	<!-- 第一个选择-->
	<script type="text/html" id="position_nameOne">
		{{splitPosition_nameOne(d.positionName)}}
	</script>
	<!-- 第二个选择-->
	<script type="text/html" id="position_nameTwo">
		{{splitPosition_nameTwo(d.positionName)}}
	</script>
	<!-- 第三个选择-->
	<script type="text/html" id="position_nameThree">
		{{splitPosition_nameThree(d.positionName)}}
	</script>

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	$(function(){
	 	
	 	//默认查询报表数据
	 	init_table();
	 	//查询招聘计划
	 	selectPlan();
	 
	 	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //年月选择器
			laydate.render({
			  elem: '#date'
			  ,type: 'month'
			  ,trigger: 'click'
			});
		});
	 
	 	
	 	//下拉框悬浮提示内容
	 	$("#showApplyName").change(function(){ 

	         //获取下拉框的文本值
	
	         var checkText=$("#recruitPlan").find("option:selected").text();
	
	         //修改title值
	
	        $("#showApplyName").attr("title",checkText);
	
	   });
	 	
	});
	
	
		
	
	function init_table(){
		//时间
		var date = $("#date").val();
		//学生
		var studentName = $("#studentName").val();
		//学校
		var school = $("#school").val();
		//招聘计划
		var recruitPlan=$("#recruitPlan").val();
		//推荐状态
		var interview_advice=$("#interview_advice").val();
		//户口所在地
		var birthplace=$("#birthplace").val();
		
		var json = {"curr":page,"limit":limit,"date":date,"school":school,"studentName":studentName,"recruitPlan":recruitPlan,"interview_advice":interview_advice,"birthplace":birthplace};
       	$.ajax({
       		url: 'com.recruit.reports.studentInterviewEvaluation.queryStudentInterviewEvaluation.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.detail_evaluate
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'recruitPlanName', width:'15%', title: '招聘计划',align:'center'}
		                
		                ,{field:'candidateName', width:'8%', title: '姓名',align:'center',total:true}
		                ,{field:'gender', width:'5%', title: '性别',align:'center' ,total:true}
		                ,{field:'college', width:'10%', title: '学校' ,align:'center',total:true}
		                ,{field:'degree', width:'10%', title: '学历',align:'center',templet:'#degree' ,total:true}
		                ,{field:'major', width:'10%', title: '专业' ,align:'center',total:true}
		                
		                ,{field:'candidatePhone', width:'10%', title: '电话' ,align:'center',total:true}
		                ,{field:'candidateMail', width:'14%', title: '邮箱' ,align:'center',total:true}
		                ,{field:'retestTimes', width:'10%', title: '挂科情况/次',align:'center',sort: true ,total:true}
		                ,{field:'englishLevel', width:'10%', title: '英语等级',align:'center',templet:'#englishLevel',sort: true ,total:true}
		                ,{field:'testScores', width:'5%', title: '总分',align:'center',sort: true ,total:true}
		                
		                ,{field:'characterType', width:'10%', title: '性格类型',align:'center',sort: true ,total:true}
		                ,{field:'exceptedWorkAreaA', width:'12%', title: '期望工作地点',align:'center' ,total:true}
		                ,{field:'birthplace', width:'14%', title: '户口所在地' ,align:'center',total:true}
		                ,{field:'resumePictureUrl', width:'7%', title: '照片',align:'center' ,templet:'#resumePictureUrl',total:true}
		                
		                ,{field:'interviewerNameFive', width:'10%', title: '小组讨论面试官'  ,total:true}
		                ,{field:'evaluateFive', width:'20%', title: '小组讨论评价' ,total:true}
		                ,{field:'interviewerNameSix', width:'10%', title: '一对一面试官'  ,total:true}
		                ,{field:'evaluateSix', width:'20%', title: '一对一面试评价',total:true}
		                ,{field:'interviewerNameSeven', width:'10%', title: '部门面试面试官'  ,total:true}
		                ,{field:'evaluateSeven', width:'20%', title: '部门面试评价' ,total:true}
		                ,{field:'interviewerNameEight', width:'10%', title: '评价中心面试官'  ,total:true}
		                ,{field:'evaluateEight', width:'20%', title: '评价中心评价'  ,total:true}
		                
		                ,{field:'position_nameOne', width:'15%', title: '第一选择',templet:'#position_nameOne'  ,total:true}
		                ,{field:'position_nameTwo', width:'15%', title: '第二选择',templet:'#position_nameTwo'  ,total:true}
		                ,{field:'position_nameThree', width:'15%', title: '第三选择' ,templet:'#position_nameThree' ,total:true}
		                ,{field:'interviewAdvice', width:'10%', title: '推荐状态',align:'center',sort: true ,total:true}
		                  
		            ]]
		            ,done: function(res, curr, count){
						
					}
					,total:true
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
		    	
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count
				    ,total:true
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.PageCond.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				     	 init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	init_table();
    }
  
  //查询招聘计划
	function selectPlan(){
		$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.recruitPlan.queryRecruitPlan.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				var RecruitPlan = data.RecruitPlan;
				for(var i=0;i<RecruitPlan.length;i++){
					$("#recruitPlan").append('<option value="'+RecruitPlan[i].recruitPlanId+'" >'+RecruitPlan[i].recruitPlanName+'</option>');
			 	}
			}
		})
	}
  //导出
  function daochu(){
  	ExcalStats = true;
  		//时间
		var date = $("#date").val();
		//学生
		var studentName = $("#studentName").val();
		//学校
		var school = $("#school").val();
		//招聘计划
		var recruitPlan=$("#recruitPlan").val();
		//招聘状态
		var interview_advice=$("#interview_advice").val();
		//户口所在地
		var birthplace=$("#birthplace").val();
		
		var json = {curr:page,limit:count,date:date,school:school,studentName:studentName,recruitPlan:recruitPlan,interview_advice:interview_advice,birthplace:birthplace
				,head : '{"recruitPlanName":"招聘计划","candidateName":"姓名","gender":"性别","college":"学校",'
		            		+'"degree":"学历","major":"专业","candidatePhone":"电话","candidateMail":"邮箱",'
		            		+'"retestTimes":"挂科情况/次","englishLevel":"英语等级","testScores":"总分","characterType":"性格类型",'
		            		+'"exceptedWorkAreaA":"期望工作地","birthplace":"户口所在地","interviewerNameFive":"小组讨论面试官","evaluateFive":"小组讨论评价",'
		            		+'"interviewerNameSix":"一对一面试官","evaluateSix":"一对一面试官评价","interviewerNameSeven":"部门面试面试官","evaluateSeven":"部门面试评价",'
		            		+'"interviewerNameEight":"评价中心面试官","evaluateEight":"评价中心评价","positionName":"申请职位","interviewAdvice":"推荐状态"}'
		        ,sheetName : "学生面试评价报表"
		        ,fileName : "学生面试评价报表"
		        //web路径
		        ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.reports.studentInterviewEvaluation.excalStudentInterviewEvaluation.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			
			var downloadpath=data.downloadpath;
			var file=data.file;
			
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
					if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = downloadpath;
 						document.getElementById("fileName").value = file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
			        }
				}
			}
		}) 
    }
    //第一职位
    function splitPosition_nameOne(name){
		var html ="";
		var ss = name.split(",");
		if(ss.length>=1){
			html ='<div class="title" title="'+ss[0]+'">'+ss[0]+'</div>';
		}else{
			html ='<div class="title" title=""></div>';
		}
		return html;
    }
    //第二职位
    function splitPosition_nameTwo(name){
		var html ="";
		var ss = name.split(",");
		if(ss.length>=2){
			html ='<div class="title" title="'+ss[1]+'">'+ss[1]+'</div>';
		}else{
			html ='<div class="title" title=""></div>';
		}
		return html;
    }
    //第三职位
    function splitPosition_nameThree(name){
		var html ="";
		var ss = name.split(",");
		if(ss.length==3){
			html ='<div class="title" title="'+ss[2]+'">'+ss[2]+'</div>';
		}else{
			html ='<div class="title" title=""></div>';
		}
		return html;
    }
    //照片
    function resumePictureUrl(url){
    	var html="";
    	if(url==null || url==""){
    		html ='<div></div>';
    	}else{
		    html ='<div><img class="img" src="'+basePath+url+'" style="width: 150px;"></div>';
    	
    	}
    	
    	return html;
    }
     //报表的重置方法
	    function resetRecr2(){
	    	document.getElementById("form1").reset();
	    	init_table();
	    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>