<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-05-23 10:03:57
  - Description:中心编制招聘情况报表
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		width: 34%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>集团编制和招聘情况分析表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>集团编制和招聘情况分析表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td style=" padding-left: 34%;">
				      		<label>工作日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	</div>
	
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
<script type="text/javascript">
	
	var table;
	var ExcalStats = false;
	$(function(){
		init_laydate();// 初始化时间控件
		init_table();
	});
	
	function init_table(){
		//工作时间
		var date = $("#atual_hire_date").val();
		var json = {"date":date};
       	$.ajax({
       		url: 'com.recruit.reports.executivesReports.executivesReports.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.authorized
		            ,limit:data.authorized.length
		            ,height:480
		            ,cols: [[
		                {field:'L1Name', width:'10%', title: '中心/公司',align:'center'}
		                ,{field:'eamount', width:'10%', title: '满编人数',sort: true,align:'right'}
		                ,{field:'oamount', width:'10%', title: '在职人数', sort: true,align:'right'}
		                ,{field:'emptyPlait', width:'10%', title: '空编人数', sort: true,align:'right'}
		                ,{field:'groupOpenPositions', width:'10%', title: '待招人数', sort: true,align:'right'}
		                
		                ,{field:'manpower', width:'10%', title: '人力需求数', sort: true,align:'right'}
		                ,{field:'hire', width:'10%', title: '录用人数', sort: true,align:'right'}
		                ,{field:'openPositions', width:'10%', title: '待招人数', sort: true,align:'right'}
		                ,{field:'awaitOffer', width:'11%', title: '录用未到岗人数', sort: true,align:'right'}
		                ,{field:'supercycle', width:'16%', title: '超过招聘周期未完成工单数', sort: true,align:'right'}
		            ]]
		            ,done: function(res, curr, count){
		            
				        CSStotal();//定义合计的css样式
					}
		        });
		    });
			}
       });
	} 
	
 //查询
    function doSearch(){
    	init_table();
    }
  //
  function daochu(){
  	
  	ExcalStats = true;
  		//工作时间
		var date = $("#atual_hire_date").val();
		var json = {"date":date};
       	$.ajax({
       		url: 'com.recruit.reports.executivesReports.executivesReports.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
				        ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"L1Name":"中心/公司","eamount":"满编人数","oamount":"在职人数","emptyPlait":"空编人数","groupOpenPositions":"待招人数","manpower":"人力需求数","hire":"录用人数","openPositions":"待招人数","awaitOffer":"录用未到岗人数","supercycle":"超过招聘周期未完成工单数"}';
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.authorized);
		            	//获取Excel工作簿名称
		            	var sheetName = "集团编制和招聘情况分析表";
		            	//获取Excel文件名称
		            	var fileName = "集团编制和招聘情况分析表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			}
			}
		})
    }
    
     //合计的css样式
    function CSStotal(){
    	/* $(".laytable-cell-1-L1Name").each(function(){
    		if($(this).text()=="合计"){
    			$(this).parent().parent().css("background-color","#ffe2b6");
    			$(this).parent().parent().css("font-weight","bold");
    		}
    	}); */
    	$(".layui-table-cell").each(function(){
    		if($(this).text()=="合计"){
    			$(this).parent().parent().css("background-color","#ffe2b6");
    			$(this).parent().parent().css("font-weight","bold");
    		}
    	});
    }
   
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
