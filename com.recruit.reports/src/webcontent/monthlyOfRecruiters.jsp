<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-15 14:28:54
  - Description:
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>招聘负责人月度录用统计表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘负责人月度录用统计表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td>
							<label>招聘负责人：</label> 
							<input id="recruiter_name" name="recruiter_name" onclick="getRecruiterName(this)"
								class="form-control" data="" >
						</td>
						<td>
				      		<label>年份：</label>  
					        <select class="form-control" id="year" >
					        </select>
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	$(function(){
	 	
	 	init_table();
	 	setYear();
	});
	
	
		//设置年份选择
		function setYear() {
			//设置年份选择
			var startYear = 2018;//起始年份
			var endYear = new Date().getUTCFullYear();//终止年份（当前年份）
			var year_select = $("#year");
			var year_item;
			for (var i = startYear; i <= endYear; i++) {

				year_item = {
					id : i,
					text : i
				};
				
				if(year_item.text==endYear){
					year_select.prepend("<option selected vlaue='"+year_item.text+"'>"+year_item.text+"</option>");
				}else{
					year_select.prepend("<option vlaue='"+year_item.text+"'>"+year_item.text+"</option>");
				}
			}
		}
	
	
	
	function init_table(){
		//年份
		var year = $("#year").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		var json = {"curr":page,"limit":limit,"year":year,"recruiterId":recruiterId};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.monthlyOfRecruiters.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.Order
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'name', width:'10%', title: '招聘负责人',sort: true,align:'center'}
		                
		                ,{field:'January', width:'10%', title: '一月/人',sort: true,align:'right',total:true}
		                ,{field:'February', width:'10%', title: '二月/人',sort: true,align:'right',total:true}
		                ,{field:'March', width:'10%', title: '三月/人',sort: true,align:'right',total:true}
		                ,{field:'oneSeason', width:'10%', title: '一季度总计/人',sort: true,align:'right',total:true}
		                
		                ,{field:'April', width:'10%', title: '四月/人',sort: true,align:'right',total:true}
		                ,{field:'May', width:'10%', title: '五月/人',sort: true,align:'right',total:true}
		                ,{field:'June', width:'10%', title: '六月/人',sort: true,align:'right',total:true}
		                ,{field:'twoSeason', width:'10%', title: '二季度总计/人',sort: true,align:'right',total:true}
		                
		                ,{field:'July', width:'10%', title: '七月/人',sort: true,align:'right',total:true}
		                ,{field:'August', width:'10%', title: '八月/人',sort: true,align:'right',total:true}
		                ,{field:'September', width:'10%', title: '九月/人',sort: true,align:'right',total:true}
		                ,{field:'ThreeSeason', width:'10%', title: '三季度总计/人',sort: true,align:'right',total:true}
		                
		                ,{field:'October', width:'10%', title: '十月/人',sort: true,align:'right',total:true}
		                ,{field:'November', width:'10%', title: '十一月/人',sort: true,align:'right',total:true}
		                ,{field:'December', width:'10%', title: '十二月/人',sort: true,align:'right',total:true}
		                ,{field:'fourSeason', width:'10%', title: '四季度总计/人',sort: true,align:'right',total:true}
		                 ,{field:'oneYear', width:'10%', title: '一年总计/人',sort: true,align:'right',total:true}
		                  ,{field:'surplus_period', width:'10%', title: '超招聘周期数',sort: true,align:'right',total:true}
		            ]]
		            ,done: function(res, curr, count){
						
					}
					,total:true
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count
				    ,total:true
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	init_table();
    }
  //
  function daochu(){
  	ExcalStats = true;
  	//年份
		var year = $("#year").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		var json = {"curr":page,"limit":count,"year":year,"recruiterId":recruiterId};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.monthlyOfRecruiters.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
					if(ExcalStats){
						//关闭导出入口
						ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"name":"招聘负责人","January":"一月/人","February":"二月/人","March":"三月/人","oneSeason":"一季度总计/人",'
		            	+'"April":"四月/人","May":"五月/人","June":"六月/人","twoSeason":"二季度总计/人",'
		            	+'"July":"七月/人","August":"八月/人","September":"九月/人","ThreeSeason":"三季度总计/人",'
		            	+'"October":"十月/人","November":"十一月/人","December":"十二月/人","fourSeason":"四季度总计/人",'
		            	+'"oneYear":"一年总计/人","surplus_period":"超招聘周期数"}';
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.Order);
		            	//获取Excel工作簿名称
		            	var sheetName = "招聘负责人月度录用统计表";
		            	//获取Excel文件名称
		            	var fileName = "招聘负责人月度录用统计表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
				}
			}
		})
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
