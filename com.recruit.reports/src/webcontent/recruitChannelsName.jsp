<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000083
  - Date: 2018-05-17 16:01:16
  - Description:
-->
<head>
<style type="text/css">
	#results{
	       
	border-left:1px solid #e6e6e6;
	border-bottom:1px solid #e6e6e6;
	border-right:1px solid #e6e6e6;
    width: 98%;
    height: 485px;
    overflow: auto;
    margin-left: 1%;
    margin-right: 1%;
    margin-top: 9px;
	}
	.layui-table, .layui-table-view{
		    margin: 0px 0;
	}
	
	.layui-table{
		width: 4000px;
	}	
	#archive-pagingToolbar{
		margin-left: 1%;
	}
	hr{
		margin: -12px 0;
		background-color: rgb(221, 221, 221);
	}
	.layui-field-title{
		margin: 3px 0 0px;
	}
.panel-body{
	    padding: 11px;
}
.form-inline .form-control{
	    width: 200px;
}
#total{
	background-color:#ffe2b6;
	font-weight:bold;
}
 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>招聘负责人渠道分析表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">

</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘负责人渠道分析表</legend>

			</fieldset>
<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						
						<td style=" padding-left: 34%;">
				      		<label>工作日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date"  placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="selectResults()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	
	

<div  id ="results">
<table class="layui-table" id ='personal' >
  
  <thead>
    <tr id = "th">
      <th>招聘负责人</th>
      
    </tr> 
  </thead>
  <tbody id= "tbody">
  	<tr>
  		
  	</tr>

  </tbody>
</table> 
</div>
			<!-- 分页条 -->
<div id="archive-pagingToolbar"></div>
<hr>
<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
</form>
<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
   
	<script type="text/javascript">
	
	var state = "Initialize";
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;//初始化总条数
	var ExcalStats = false;
	var campusSum = 0;//校园
	var huanongSum = 0;//华农
	var hainanSum = 0 ; //海南
	var huanuanSum = 0 ;//华软
	var schoolRecruitSum = 0;//校招
	var guangmeiSum = 0;//广美
	var qitaSum = 0;//其他
	var qita = 0;
			
	var networkSum = 0;//网络
	var liepinSum= 0 ;//猎聘
	var jobSum = 0;//前程无忧
	var zhaopinLTDSum = 0;//智联招聘
	var  citySum = 0 ; //58同城
	var bossDirecthireSum = 0;//boos直聘
	var jjrSum = 0;//JJR
	var draghookSum = 0 ;//拉钩网
			
	var headhuntersSum = 0 ;//猎头
	var trumpSum = 0;//川普
	var youdaSum = 0 ; //优达
	var youyueSum = 0;//优越
	var weisidengSum = 0;//威斯登
			
	var internalSum = 0 ;//内部竞聘
	var internalSum1 = 0;//内部竞聘
	var InternalRecommendSum = 0 ;//内部推荐
	var RecommendOaSum = 0;//OA内推
	var internalcallSum = 0;//内部调动
	var internalcallSum1 = 0;//内部调动	
			
	//专场招聘
	var zhuangchangSum = 0;
	//东区人才市场
	var dongquSum=0;
	//宁西广场招聘会
	var ningxiSum=0;
	//退伍人才专场
	var tuiwuSum=0;
	//增城人才市场
	var zengcheng=0;
	//招工牌
	var zhaogong = 0;
	//招工牌
	var zhaogong1=0;
	//人才中介
	var zhongjie=0;
	//鼎力
	var dingli = 0;
	//红新
	var hongxin=0;
	//新塘招聘
	var xintang=0;
	//穗诚
	var huicheng=0;
	//沙埔中介
	var shapu=0;
	//引田
	var yintian=0;
	//总计
	var zonji=0;
	selectResults();
	function selectResults(){
	
		//工作时间
		var dailyReportDtime = $("#atual_hire_date").val();
		var data = {dailyReportDtime:dailyReportDtime,state:state,curr:page,limit:limit};
    		$.ajax({
       		url: 'com.recruit.reports.recruitcChannelsReports.queryRecruitChannelsName.biz.ext',
			type:'POST',
			data:data,
			success:function(data){
			$("#tbody").empty();
			if(state=="Initialize"){

			for(var i = 0;i<data.dicEntry.length;i++){
				//序号
				var seqno = data.dicEntry[i].SEQNO;
				//获取字符串从1位置开始的小数点位置
				var k = seqno.indexOf(".",1);
				//截取从字符串开始值为小数点位置的
				seqno = seqno.substring(k+1);
				/*
					动态渲染渠道和供应商
					seqno值等于空 给th加样式
					seqno值为空时为渠道，否则是供应商
				*/
				var th;
				if(seqno==""){
					th+="<th style='font-weight: 600;color:#f0ad4e'>"+data.dicEntry[i].DICTNAME+"</th>";
				}else{
					th+="<th>"+data.dicEntry[i].DICTNAME+"</th>";
				}
			}
			$("#th").append(th);
			
			$("#th").append("<th>总计</th>");
			laypage(count);
				state="";
			}else{
			
			count= data.PageCond.count;
			laypage(count);
			
			if(data.countChannels.length>0){
			
		 	for(var j  = 0 ; j<data.countChannels.length;j++){
			
				var tr = "<tr>";
					tr+="<td>"+data.countChannels[j].recruiter_name+"</td>";
					tr+="<td class = 'campusSum'>"+data.countChannels[j].dicName1+"</td>";
					tr+="<td class ='huanongSum'>"+data.countChannels[j].dicName1and1+"</td>";
					tr+="<td class = 'hainanSum'>"+data.countChannels[j].dicName1and2+"</td>";
					tr+="<td class = 'huanuanSum'>"+data.countChannels[j].dicName1and3+"</td>";
					tr+="<td class = 'schoolRecruitSum'>"+data.countChannels[j].dicName1and4+"</td>";
					tr+="<td class = 'guangmeiSum'>"+data.countChannels[j].dicName1and5+"</td>";
					tr+="<td class = 'qitaSum'>"+data.countChannels[j].dicName10+"</td>";
					tr+="<td class = 'qita'>"+data.countChannels[j].dicName10and32+"</td>";
					tr+="<td class = 'networkSum'>"+data.countChannels[j].dicName2+"</td>";
					tr+="<td class = 'liepinSum'>"+data.countChannels[j].dicName2and10+"</td>";
					tr+="<td class = 'jobSum'>"+data.countChannels[j].dicName2and11+"</td>";
					tr+="<td class = 'zhaopinLTDSum'>"+data.countChannels[j].dicName2and12+"</td>";
					tr+="<td class = 'citySum'>"+data.countChannels[j].dicName2and6+"</td>";
					tr+="<td class = 'bossDirecthireSum'>"+data.countChannels[j].dicName2and7+"</td>";
					tr+="<td class = 'jjrSum'>"+data.countChannels[j].dicName2and8+"</td>";
					tr+="<td class = 'draghookSum'>"+data.countChannels[j].dicName2and9+"</td>";
					tr+="<td class = 'headhuntersSum'>"+data.countChannels[j].dicName3+"</td>";
					tr+="<td class = 'trumpSum'>"+data.countChannels[j].dicName3and13+"</td>";
					tr+="<td class = 'youdaSum'>"+data.countChannels[j].dicName3and14+"</td>";
					tr+="<td class = 'youyueSum'>"+data.countChannels[j].dicName3and15+"</td>";
					tr+="<td class = 'weisclassengSum'>"+data.countChannels[j].dicName3and16+"</td>";
					tr+="<td class = 'InternalRecommendSum'>"+data.countChannels[j].dicName5+"</td>";
					tr+="<td class = 'RecommendOaSum'>"+data.countChannels[j].dicName5and28+"</td>";
					tr+="<td class = 'internalcallSum'>"+data.countChannels[j].dicName6+"</td>";
					tr+="<td class = 'internalcallSum1'>"+data.countChannels[j].dicName6and30+"</td>";
					tr+="<td class = 'zhuangchangSum'>"+data.countChannels[j].dicName7+"</td>";
					tr+="<td class = 'dongquSum'>"+data.countChannels[j].dicName7and23+"</td>";
					tr+="<td class = 'ningxiSum'>"+data.countChannels[j].dicName7and25+"</td>";
					tr+="<td class = 'tuiwuSum'>"+data.countChannels[j].dicName7and26+"</td>";
					tr+="<td class = 'zengcheng'>"+data.countChannels[j].dicName7and27+"</td>";
					tr+="<td class = 'zhaogong'>"+data.countChannels[j].dicName8+"</td>";
					tr+="<td class = 'zhaogong1'>"+data.countChannels[j].dicName8and31+"</td>";
					tr+="<td class = 'zhongjie'>"+data.countChannels[j].dicName9+"</td>";
					tr+="<td class = 'dingli'>"+data.countChannels[j].dicName9and17+"</td>";
					tr+="<td class = 'hongxin'>"+data.countChannels[j].dicName9and18+"</td>";
					tr+="<td class = 'xintang'>"+data.countChannels[j].dicName9and19+"</td>";
					tr+="<td class = 'huicheng'>"+data.countChannels[j].dicName9and20+"</td>";
					tr+="<td class = 'shapu'>"+data.countChannels[j].dicName9and21+"</td>";
					tr+="<td class = 'yintian'>"+data.countChannels[j].dicName9and22+"</td>";
					tr+="<td class = 'zonji'>"+data.countChannels[j].total+"</td>";
					tr+="</tr>";
				$("#tbody").append(tr);
			
			 }  
			}
	 campusSum = 0;//校园
	 huanongSum = 0;//华农
	 hainanSum = 0 ; //海南
	 huanuanSum = 0 ;//华软
	 schoolRecruitSum = 0;//校招
	 guangmeiSum = 0;//广美
	 qitaSum = 0;//其他
	 qita = 0;
			
	 networkSum = 0;//网络
	 liepinSum= 0 ;//猎聘
	 jobSum = 0;//前程无忧
	 zhaopinLTDSum = 0;//智联招聘
	 citySum = 0 ; //58同城
	 bossDirecthireSum = 0;//boos直聘
	 jjrSum = 0;//JJR
	 draghookSum = 0 ;//拉钩网
			
	 headhuntersSum = 0 ;//猎头
	 trumpSum = 0;//川普
	 youdaSum = 0 ; //优达
	 youyueSum = 0;//优越
	 weisidengSum = 0;//威斯登
			
	 InternalRecommendSum = 0 ;//内部推荐
	 RecommendOaSum = 0;//OA内推
	 internalcallSum = 0;//内部调动
	 internalcallSum1 = 0;//内部调动	
			
	//专场招聘
	 zhuangchangSum = 0;
	//东区人才市场
	 dongquSum=0;
	//宁西广场招聘会
	 ningxiSum=0;
	//退伍人才专场
	 tuiwuSum=0;
	//增城人才市场
	 zengcheng=0;
	//招工牌
	 zhaogong = 0;
	//招工牌
	 zhaogong1=0;
	//人才中介
	 zhongjie=0;
	//鼎力
	 dingli = 0;
	//红新
	 hongxin=0;
	//新塘招聘
	 xintang=0;
	//穗诚
	 huicheng=0;
	//沙埔中介
	 shapu=0;
	//引田
	 yintian=0;
	 //总计
	 zonji=0;
			if(data.countChannels.length>0){
			
			 //校园
			$('.campusSum').each(function(){
				campusSum+=parseFloat($(this).text());
			});
			//华农
			$('.huanongSum').each(function(){
				huanongSum+=parseFloat($(this).text());
			});
			//海南
			$('.hainanSum').each(function(){
				hainanSum+=parseFloat($(this).text());
			}); 
			 //华软
			$('.huanuanSum').each(function(){
				huanuanSum+=parseFloat($(this).text());
			});
			//校招
			$('.schoolRecruitSum').each(function(){
				schoolRecruitSum+=parseFloat($(this).text());
			});
			//广美
			$('.guangmeiSum').each(function(){
				guangmeiSum+=parseFloat($(this).text());
			});
			//其他
			$('.qitaSum').each(function(){
				qitaSum+=parseFloat($(this).text());
			});
			$('.qita').each(function(){
				qita+=parseFloat($(this).text());
			});
			//网络
			$('.networkSum').each(function(){
				networkSum+=parseFloat($(this).text());
			});
			//猎聘
			$('.liepinSum').each(function(){
				liepinSum+=parseFloat($(this).text());
			});
			//前程无忧
			$('.jobSum').each(function(){
				jobSum+=parseFloat($(this).text());
			});
			//智联招聘
			$('.zhaopinLTDSum').each(function(){
				zhaopinLTDSum+=parseFloat($(this).text());
			});
			//58同城
			$('.citySum').each(function(){
				citySum+=parseFloat($(this).text());
			});
			//boos直聘
			$('.bossDirecthireSum').each(function(){
				bossDirecthireSum+=parseFloat($(this).text());
			});
			//JJR
			$('.jjrSum').each(function(){
				jjrSum+=parseFloat($(this).text());
			});
			//拉钩网
			$('.draghookSum').each(function(){
				draghookSum+=parseFloat($(this).text());
			});
			//猎头
			$('.headhuntersSum').each(function(){
				headhuntersSum+=parseFloat($(this).text());
			});
			//川普
			$('.trumpSum').each(function(){
				trumpSum+=parseFloat($(this).text());
			});
			//优达
			$('.youdaSum').each(function(){
				youdaSum+=parseFloat($(this).text());
			});
			//优越
			$('.youyueSum').each(function(){
				youyueSum+=parseFloat($(this).text());
			});
			//威斯登
			$('.weisidengSum').each(function(){
				weisidengSum+=parseFloat($(this).text());
			});
			
		
			//内部推荐
			$('.InternalRecommendSum').each(function(){
				InternalRecommendSum+=parseFloat($(this).text());
			});
			//OA内推
			$('.RecommendOaSum').each(function(){
				RecommendOaSum+=parseFloat($(this).text());
			});
			//内部调动
			$('.internalcallSum').each(function(){
				internalcallSum+=parseFloat($(this).text());
			});
			//内部调动
			$('.internalcallSum1').each(function(){
				internalcallSum1+=parseFloat($(this).text());
			});
			
			
			//专场招聘
			$('.zhuangchangSum').each(function(){
				zhuangchangSum+=parseFloat($(this).text());
			});
			//东区人才市场
			$('.dongquSum').each(function(){
				dongquSum+=parseFloat($(this).text());
			});
			//宁西广场招聘会
			$('.ningxiSum').each(function(){
				ningxiSum+=parseFloat($(this).text());
			});
			//退伍人才专场
			$('.tuiwuSum').each(function(){
				tuiwuSum+=parseFloat($(this).text());
			});
			//增城人才市场
			$('.zengcheng').each(function(){
				zengcheng+=parseFloat($(this).text());
			});
			//招工牌
			$('.zhaogong').each(function(){
				zhaogong+=parseFloat($(this).text());
			});
			//招工牌
			$('.zhaogong1').each(function(){
				zhaogong1+=parseFloat($(this).text());
			});
			//人才中介
			$('.zhongjie').each(function(){
				zhongjie+=parseFloat($(this).text());
			});
			//鼎力
			$('.dingli').each(function(){
				dingli+=parseFloat($(this).text());
			});
			//红新
			$('.hongxin').each(function(){
				hongxin+=parseFloat($(this).text());
			});
			//新塘招聘
			$('.xintang').each(function(){
				xintang+=parseFloat($(this).text());
			});
			//穗诚
			$('.huicheng').each(function(){
				huicheng+=parseFloat($(this).text());
			});
			//沙埔中介
			$('.shapu').each(function(){
				shapu+=parseFloat($(this).text());
			});
			//引田
			$('.yintian').each(function(){
				yintian+=parseFloat($(this).text());
			});
			//总计
			$('.zonji').each(function(){
				zonji+=parseFloat($(this).text());
			});
			
			var sumtr = "<tr id='total'>";
				sumtr +="<td>合计</td>";
			    sumtr +="<td>"+campusSum+"</td>";
				sumtr +="<td>"+huanongSum+"</td>";
				sumtr +="<td>"+hainanSum+"</td>";
				sumtr +="<td>"+huanuanSum+"</td>";
				sumtr +="<td>"+schoolRecruitSum+"</td>";
				sumtr +="<td>"+guangmeiSum+"</td>";
				sumtr +="<td>"+qitaSum+"</td>";
				sumtr +="<td>"+qita+"</td>";
				//网络
				sumtr +="<td>"+networkSum+"</td>";
				sumtr +="<td>"+liepinSum+"</td>";
				sumtr +="<td>"+jobSum+"</td>";
				sumtr +="<td>"+zhaopinLTDSum+"</td>";
				sumtr +="<td>"+citySum+"</td>";
				sumtr +="<td>"+bossDirecthireSum+"</td>";
				sumtr +="<td>"+jjrSum+"</td>";
				sumtr +="<td>"+draghookSum+"</td>";
				
				//猎头
				sumtr +="<td>"+headhuntersSum+"</td>";
				sumtr +="<td>"+trumpSum+"</td>";
				sumtr +="<td>"+youdaSum+"</td>";
				sumtr +="<td>"+youyueSum+"</td>";
				sumtr +="<td>"+weisidengSum+"</td>";
				
				//内部
				sumtr +="<td>"+InternalRecommendSum+"</td>";
				sumtr +="<td>"+RecommendOaSum+"</td>";
				sumtr +="<td>"+internalcallSum+"</td>";
				sumtr +="<td>"+internalcallSum1+"</td>";
				
				
				sumtr +="<td>"+zhuangchangSum +"</td>";
				sumtr +="<td>"+dongquSum +"</td>";
				sumtr +="<td>"+ningxiSum +"</td>";
				sumtr +="<td>"+tuiwuSum +"</td>";
				sumtr +="<td>"+zengcheng +"</td>";
				sumtr +="<td>"+zhaogong +"</td>";
				sumtr +="<td>"+zhaogong1 +"</td>";
				sumtr +="<td>"+zhongjie +"</td>";
				sumtr +="<td>"+dingli +"</td>";
				sumtr +="<td>"+hongxin +"</td>";
				sumtr +="<td>"+xintang +"</td>";
				sumtr +="<td>"+huicheng  +"</td>";
				sumtr +="<td>"+shapu  +"</td>";
				sumtr +="<td>"+yintian  +"</td>";
				sumtr +="<td>"+zonji  +"</td>";
				sumtr +="</tr>";
				
				//专场
				
				 
				$("#tbody").append(sumtr);
			  }
			 }
			 if(ExcalStats){
				daochuExcal('personal','dlink','招聘负责人渠道分析表');
			}
			}
			
			});
		}
		

	
	
	
	function laypage(count){
	layui.use(['laypage', 'layer'], function(){	
		var laypage = layui.laypage
		 ,layer = layui.layer;
			laypage.render({
			   elem: 'archive-pagingToolbar'
			   ,count:count
			   ,curr:page
			   ,limit:limit
			   ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
			   ,jump: function(obj,first){
			     count = obj.count;
				 limit = obj.limit;
				 page = obj.curr;
			    if(!first){
			    	selectResults();
			    }
    	    }
  		 });
	  
   });	
}
 function daochu(){
  	ExcalStats = true;
  		//工作时间
		var dailyReportDtime = $("#atual_hire_date").val();
		var data = {dailyReportDtime:dailyReportDtime,state:state,curr:page,limit:limit};
       	$.ajax({
       		url: 'com.recruit.reports.recruitcChannelsReports.queryRecruitChannelsName.biz.ext',
			type:'POST',
			data:data,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
			
			
				if(ExcalStats){
				
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"recruiter_name":"招聘负责人","dicName1":"校园","dicName1and1":"华农",'
		            	+'"dicName1and2":"海南大学","dicName1and3":"华软","dicName1and4":"双选会","dicName1and5":"广美",'
		            	+'"dicName10":"其他","dicName10and32":"其他","dicName2":"网络","dicName2and10":"猎聘","dicName2and11":"前程无忧",'
		            	+'"dicName2and12":"智联招聘","dicName2and6":"58同城","dicName2and7":"boss直聘","dicName2and8":"JJR","dicName2and9":"拉勾网",'
		            	+'"dicName3":"猎头","dicName3and13":"川普","dicName3and14":"优达猎头","dicName3and15":"优越人才","dicName3and16":"威思登",'
		            	+'"dicName5":"内部推荐","dicName5and28":"OA内推","dicName6":"内部调动",'
		            	+'"dicName6and30":"内部调动","dicName7":"专场招聘","dicName7and23":"东区人才市场","dicName7and25":"宁西广场招聘会",'
		            	+'"dicName7and26":"退伍人才专场","dicName7and27":"增城人才市场","dicName8":"招工牌","dicName8and31":"招工牌","dicName9":"人才中介",'
		            	+'"dicName9and17":"鼎力","dicName9and18":"红新","dicName9and19":"新塘招聘","dicName9and20":"穗诚","dicName9and21":"沙埔中介","dicName9and22":"引田","total":"总计"}'; 
		            
		            	var total = {"recruiter_name":"合计","dicName1":campusSum,"dicName1and1":huanongSum,
		            	"dicName1and2":hainanSum,"dicName1and3":huanuanSum,"dicName1and4":schoolRecruitSum,"dicName1and5":guangmeiSum,
		            	"dicName10":qitaSum,"dicName10and32":qita,"dicName2":networkSum,"dicName2and10":liepinSum,"dicName2and11":jobSum,
		            	"dicName2and12":zhaopinLTDSum,"dicName2and6":citySum,"dicName2and7":bossDirecthireSum,"dicName2and8":jjrSum,"dicName2and9":draghookSum,
		            	"dicName3":headhuntersSum,"dicName3and13":trumpSum,"dicName3and14":youdaSum,"dicName3and15":youyueSum,"dicName3and16":weisidengSum,
		            	"dicName5":InternalRecommendSum,"dicName5and28":RecommendOaSum,"dicName6":internalcallSum,
		            	"dicName6and30":internalcallSum1,"dicName7":zhuangchangSum,"dicName7and23":dongquSum,"dicName7and25":ningxiSum,
		            	"dicName7and26":tuiwuSum,"dicName7and27":zengcheng,"dicName8":zhaogong,"dicName8and31":zhaogong1,"dicName9":zhongjie,
		            	"dicName9and17":dingli,"dicName9and18":hongxin,"dicName9and19":xintang,"dicName9and20":huicheng,"dicName9and21":shapu,"dicName9and22":yintian,"total":zonji};
		            	var recommenArray = data.countChannels;
		            	
		            	 recommenArray.push(total); 
		            	 
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(recommenArray);
		            	//获取Excel工作簿名称
		            	var sheetName = "招聘负责人渠道分析表";
		            	//获取Excel文件名称
		            	var fileName = "招聘负责人渠道分析表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			}
			}
			
		})
    }

    init_laydate();
		
    </script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
