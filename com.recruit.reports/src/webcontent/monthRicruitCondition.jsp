<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%
//获取当前用户的角色ID
	String roleList = (String)DataContextManager.current().getMUODataContext().getUserObject().getAttributes().get("roleList");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-05-02 10:03:57
  - Description:招聘情况分析表
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	label{
		margin-bottom: 0px;
	
	}
	.common{
		width: 90%!important;
	}
</style>

<title>招聘情况分析表</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
<style type="text/css">
</style>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘情况分析表</legend>

			</fieldset>
	<div class="panel panel-default">
		<div class="panel-body" style="width: 100%; height: 100%;">

			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td style="width: 6%;"><label>工作日期:</label></td>
						<td style="width: 18%">
						<input type="text"
							class="form-control common" id="audit_time" name="audit_time"
							placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						
						<td style="width: 7%;"><label>招聘负责人:</label></td>
						<td style="width: 18%">
						 <input id="recruiter_name"
							name="recruiter_name" class="form-control common" data="" type="text"
							onclick="getRecruiterName(this)">
						</td>

						 <td style="width: 6%;"><label>招聘状态:</label> </td>
						 <td style="width: 18%">
							 <select id="ro_status"
								name="ro_status" class="form-control common" >
									<option value="">请选择招聘状态</option>
							 </select>
						</td>
						<td>
							
							<button class="btn btn-warning" type="button"
								onclick="doSearch()">搜 索</button>
							<button class="btn btn-default btn-center" id="onlyForEnterSearch" type="button" onclick="resetRecr()">重
								置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink" style="display: none;"></a>
						
						</td>
					</tr>	
					
					<tr style="line-height: 4;">
						<td><label>中心L1:</label></td>
						<td>
						 <select id="L1"
							name="L1" class="form-control common" oninput=init_L2()>
								<option value="">请选择中心</option>
						</select></td>
						
						<td><label>部门L2:</label> </td>
						<td>
						<select id="L2"
							name="L2" class="form-control common">
								<option value="">请选择部门</option>
						</select>
						</td>
						<td><label>所属公司:</label> </td>
						<td>
							<select id="workplace"name="workplace" style="width: 90%;" class="form-control reset">
								<option value="">请选择公司</option>
							</select>
						</td>
					</tr> 
					

					

				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table class="layui-hide" id="personal"></table>
		</div>
		<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	<script type="text/javascript">
	//获取招聘负责人角色信息
	var role = '<%=roleList %>';
	//招聘负责人角色信息转数组
	var RoleList = role.split(",");
	//是否具备招聘报表管理员权限
	var status = "false";
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	var roStatus = [{id:1,name:"招聘中"},{id:2,name:"待入职"},{id:3,name:"已入职"},{id:4,name:"取消招聘"},{id:5,name:"候选人放弃"},{id:6,name:"修改招聘负责人"},];
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_table();
	 	queryCompany(isHaveAuthority ());
	 	for(var i=0;i<roStatus.length;i++){
					$("#ro_status").append('<option value="'+roStatus[i].id+'">'+roStatus[i].name+'</option>');
			 	} 
		init_L1();
	});
	
	//判断是否具备管理权限
	function isHaveAuthority (){
		//设置查看权限
		for(var i = 0;i<RoleList.length;i++){
			if("1131"===RoleList[i]||1131===RoleList[i]){
				status = "true";
				return status;
			}
		}
	}
		//查询所属公司
	function queryCompany(status){
		var json = {"status" : status};
		$.ajax({
			url:'com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryUserCompany.biz.ext',
			type:'POST',
			data:json,
			dataType:'json',
			async:false, 
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					//动态渲染期望公司
					var option = "";
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						option+="<option value="+data.RecruitCompany[i].company_id+">"+data.RecruitCompany[i].company_name+"</option>";
					}
					$("#workplace").append(option);
				} 
			}
		});
	}
	//初始化加载中心下拉框选择
	function init_L1(){
		$.ajax({
			url: 'com.recruit.department.queryDepartment.queryCentreDepartment.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				var L1 = data.OrgOrganization;
				for(var i=0;i<L1.length;i++){
					$("#L1").append('<option value="'+L1[i].orgid+'">'+L1[i].orgname+'</option>');
			 	} 
			
			}
		});
	}
	
	
	
	//初始化加载中心下拉框选择
	function init_L2(){
		//中心id
		var PARENTORGID = $("#L1").val();
		var json = {"PARENTORGID":PARENTORGID};
		$.ajax({
			url: 'com.recruit.department.queryDepartment.queryCentreDepartment.biz.ext',
			type:'POST',
			cache: false,
			data:json,
			success:function(data){
				var L2 = data.OrgOrganization;
				var value = $("#L2").val();
				$("#L2")[0].length=1;
				for(var i=0;i<L2.length;i++){
					$("#L2").append('<option value="'+L2[i].orgid+'">'+L2[i].orgname+'</option>');
			 	} 
			
			}
		});
	}
	
	function init_table(){
		//审批时间
		var AuditTime = $("#audit_time").val();
		//招聘状态
		var roStatus = $("#ro_status").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		
		//所属公司
		var company =  $("#workplace").val();
		
		var L1 = $("#L1").val();
		
		var L2 = $("#L2").val();
		
		var json = {"L1":L1,"L2":L2,"curr":page,"limit":limit,"date":AuditTime,"roStatus":roStatus,"recruiterId":recruiterId,"company":company,"status":status};
       	$.ajax({
       		url: 'com.recruit.reports.monthRecruitCondition.monthRicruitCondition.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.CounditionReports//传入值（对象数组）
					,limit:limit //展示条目数
		            ,height:450
		            ,cols: [[
		                {field:'recuit_order_id', width:219, title: '单号', sort: true,align:'center'}
		                ,{field:'recruiter_name', width:105, title: '招聘负责人',align:'center'}
		                ,{field:'L1_name', width:153, title: '中心(L1)', sort: true,align:'center'}
		                ,{field:'L2_name', width:153, title: '部门(L2)',align:'center'}
		                
		                ,{field:'position', width:190, title: '需求职位', sort: true,align:'center'}
		                ,{field:'jobCategory', width:290, title: '职位类别',templet: '<div>{{Positioncatagory(d.jobCategory)}}</div>',align:'center'}
		                ,{field:'recruit_period', width:147, title: '标准招聘周期',sort: true,align:'center'}
		                ,{field:'positions', width:257, title: '职位数量', sort: true,align:'center'}
		                
		                ,{field:'audit_time', width:140, title: '审批日期', sort: true,align:'center'}
		                ,{field:'reciivedResumes', width:140, title: '收到简历', sort: true,align:'right'}
		                ,{field:'searchedResumes', width:145, title: '搜索简历', sort: true,align:'right'}
		                ,{field:'selectedResumes', width:158, title: '筛选简历数', sort: true,align:'right'}
		                
		                ,{field:'phoneInterview', width:125, title: '已电话面试', sort: true,align:'right'}
		                ,{field:'hrInterview', width:135, title: '已人力资源面试', sort: true,align:'right'}
		                ,{field:'departmentInterview', width:125, title: '已部门面试', sort: true,align:'right'}
		                ,{field:'includedTalentPools', width:155, title: '可计入人才库简历', sort: true,align:'right'}
		                
		                ,{field:'cancel_status', width:125, title: '取消招聘', sort: true,align:'right'}
		                ,{field:'pending', width:125, title: '已录用人数', sort: true,align:'right'}
		                ,{field:'Pends', width:125, title: '待招人数', sort: true,align:'right'}
		                ,{field:'await', width:140, title: '等待候选人确认', sort: true ,align:'right'}
		                
		                ,{field:'abandon', width:120, title: '候选人放弃',sort: true,align:'right'}
		                ,{field:'practical', width:120, title: '实际招聘周期', align:'right'}
		            ]]
		            ,done: function(res, curr, count){
		            	
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count //总条目数
				    ,limit:limit//展示条目数
				    ,limits : [10, 20, 30, 40, 50,data.PageCond.count]//条目数选择
				    ,curr : page //当前页数
				    ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
				    ,jump: function(obj,first){
				    	count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
    
    function doSearch(){
    	init_table();
    }
      function daochu(){
  		ExcalStats = true;
  		//审批时间
		var AuditTime = $("#audit_time").val();
		//招聘状态
		var roStatus = $("#ro_status").val();
		//招聘负责人
		var recruiterId = $("#recruiter_name").attr("data");
		//所属公司
		var company =  $("#workplace").val();
		//中心
		var L1 = $("#L1").val();
		//部门
		var L2 = $("#L2").val();
		
		var json = {"L1":L1,"L2":L2,"curr":page,"limit":count,"date":AuditTime,"roStatus":roStatus,"recruiterId":recruiterId,"company":company,"status":status
       			,head : '{"recuit_order_id":"单号","recruiter_name":"招聘负责人","L1_name":"中心(L1)","L2_name":"部门(L2)",'
		            	+'"position":"需求职位","jobCategory":"职位类别","recruit_period":"标准招聘周期","positions":"职位数量",'
		            	+'"audit_time":"审批日期","reciivedResumes":"收到简历","searchedResumes":"搜索简历","selectedResumes":"筛选简历数",'
		            	+'"phoneInterview":"已电话面试","hrInterview":"已人力资源面试","departmentInterview":"已部门面试","includedTalentPools":"可计入人才库简历",'
		            	+'"cancel_status":"取消招聘","pending":"已录用人数","Pends":"待招人数","await":"等待候选人确认",'
		            	+'"abandon":"候选人放弃","practical":"实际招聘周期"}'
		      ,sheetName : "招聘情况分析表"
		      ,fileName : "招聘情况分析表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createMonthRicruitConditionExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		})
    }
    
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>