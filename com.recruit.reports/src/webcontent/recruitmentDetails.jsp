<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-18 14:45:32
  - Description:
-->
<head>
<style type="text/css">
	.form-inline .form-control{
		      width: 34%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>招聘明细表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
    
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>招聘明细报表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td style="padding-left: 34%;">
				      		<label>工作日期:</label>  
					        <input type="text" 
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date"  placeholder="yyyy-MM-dd ~ yyyy-MM-dd" >
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	
<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	$(function(){
		init_laydate();// 初始化时间控件
		init_table();
	});
	
	function init_table(){
		//工作时间
		var date = $("#atual_hire_date").val();
		
		var json = {"date":date};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.recruitmentDetails.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.authorized
		            ,limit:data.authorized.length
		            ,height:470
		            ,cols: [[
		                {field:'L1Name', width:'20%', title: '中心/公司',align:'center'}
		                ,{field:'eamount', width:'10%', title: '满编人数',sort: true,align:'right',total:true}
		                ,{field:'oamount', width:'10%', title: '在职人数', sort: true,align:'right',total:true}
		                ,{field:'emptyPlait', width:'10%', title: '空缺人数', sort: true,align:'right',total:true}
		                
		                ,{field:'pending', width:'10%', title: '待招人数(编制)', sort: true,align:'right',total:true}
		                ,{field:'departing', width:'10%', title: '离职人数', sort: true,align:'right',total:true}
		                ,{field:'manpower', width:'10%', title: '人力需求数', sort: true,align:'right',total:true}
		                ,{field:'hire', width:'10%', title: '总录用人数', sort: true,align:'right',total:true}
		                
		                ,{field:'alreadyEntry', width:'10%', title: '已入职人数', sort: true,align:'right',total:true}
		                ,{field:'awaitOffer', width:'12%', title: '录用未到岗人数', sort: true,align:'right',total:true}
		                ,{field:'abandonEntry', width:'10%', title: '候选人放弃', sort: true,align:'right',total:true}
		                ,{field:'openPositions', width:'10%', title: '待招人数(人力需求)', sort: true,align:'right',total:true}
		                ,{field:'supercycle', width:'17%', title: '超过招聘周期未完成工单数', sort: true,align:'right',total:true}
		            ]]
		            ,done: function(res){
						/* layer.close(loading); */
					}
					,total:true
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	init_table();
    }
  //
  function daochu(){
  	ExcalStats = true;
  	//工作时间
		var date = $("#atual_hire_date").val();
		
		var json = {"date":date};
       	$.ajax({
       		url: 'com.recruit.reports.monthly.recruitmentDetails.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//配置Excel表头内容
		            	var head = '{"L1Name":"中心/公司","eamount":"满编人数","oamount":"在职人数","emptyPlait":"空缺人数",'
		            	+'"pending":"待招人数(编制)","departing":"离职人数","manpower":"人力需求数","hire":"总录用人数",'
		            	+'"alreadyEntry":"已入职人数","awaitOffer":"录用未到岗人数","abandonEntry":"候选人放弃","openPositions":"待招人数(人力需求)","supercycle":"超过招聘周期未完成工单数"}';
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.authorized);
		            	//获取Excel工作簿名称
		            	var sheetName = "招聘明细报表";
		            	//获取Excel文件名称
		            	var fileName = "招聘明细报表";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			}
			}
		})
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
