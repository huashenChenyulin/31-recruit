<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-10-15 08:52:08
  - Description:
-->
<head>
<title>问卷调查报表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
	 /*报表的数据高度*/
    .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	.title {
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
	.table1 td{
		line-height: 3;
	}
	.table1 label{
		width: 85px;
	}
</style>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>问卷调查报表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;" class="table1">
					<tr>
						<td>
							<label>问卷模版：</label>
							<a id="showApplyName" title=""> <select id="recruitPlan" class="form-control" style="width: 170px;">
								
							</select></a>
						</td>
						<td>
							<label>发送时间：</label>
							<input type="text"
								class="form-control" id="date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<!-- <button class="btn btn-default btn-center" type="button" onclick="resetRecr2()">重置</button> -->
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button> 
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	

<script type="text/javascript">
	
	var page = 1;//初始化页数
	var limit = 10; //初始条目数
	var count = 0;
	var table;
	var ExcalStats = false;
	$(function(){
	 	//查询问卷模版
	 	selectTemplate();
	 	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //年月选择器
			laydate.render({
			  elem: '#date'
			  ,range:'~'
			  ,min: '2017-03-01'
  			  ,max: dateString
			});
		}); 
		
	});
	
	
		
	
	function init_table(){
		
		var date = new Date();//获取系统当前时间
		var myDate=date.toLocaleString(); //获取日期与时间
		//获取前8位
		myDate=myDate.substring(0, 10);
		myDate=myDate.replace("/", "-");
		myDate=myDate.replace("/", "-");
		
		
		//问卷模版
		var template_id=$("#recruitPlan").val();
		//时间
		var date = $("#date").val();
		if(date=="" || date==null){
			date="2017-03-01~"+myDate;
		}
		
		var json = {"curr":page,"limit":limit,"template_id":template_id,"time":date};
       	$.ajax({
       		url: 'com.recruit.reports.questionnaireReport.questionnaireReport.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				//拼接表头
				splicingHeader(data.subject);
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.reportContent
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		            	 {field:'template_name', width:'15%', title: '问卷名称',sort: true ,align:'center'}
		            	,{field:'template_time', width:'15%', title: '发送时间',sort: true ,align:'center'}
		                ,{field:'sub_time', width:'15%', title: '提交时间',sort: true ,align:'center'}
		                
		               	,{field:'answer_1', width:'15%', title: '<span title="'+data.subject[0].serial_number+'、'+data.subject[0].subject+'">'+data.subject[0].serial_number+'、'+data.subject[0].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_1)}}</div>'}
		                ,{field:'answer_2', width:'15%', title: '<span title="'+data.subject[1].serial_number+'、'+data.subject[1].subject+'">'+data.subject[1].serial_number+'、'+data.subject[1].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_2)}}</div>'}
		                ,{field:'answer_3', width:'15%', title: '<span title="'+data.subject[2].serial_number+'、'+data.subject[2].subject+'">'+data.subject[2].serial_number+'、'+data.subject[2].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_3)}}</div>'}
		                ,{field:'answer_4', width:'15%', title: '<span title="'+data.subject[3].serial_number+'、'+data.subject[3].subject+'">'+data.subject[3].serial_number+'、'+data.subject[3].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_4)}}</div>'}
		                ,{field:'answer_5', width:'15%', title: '<span title="'+data.subject[4].serial_number+'、'+data.subject[4].subject+'">'+data.subject[4].serial_number+'、'+data.subject[4].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_5)}}</div>'}
		                
		                ,{field:'answer_6', width:'15%', title: '<span title="'+data.subject[5].serial_number+'、'+data.subject[5].subject+'">'+data.subject[5].serial_number+'、'+data.subject[5].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_6)}}</div>'}
		                ,{field:'answer_7', width:'15%', title: '<span title="'+data.subject[6].serial_number+'、'+data.subject[6].subject+'">'+data.subject[6].serial_number+'、'+data.subject[6].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_7)}}</div>'}
		                ,{field:'answer_8', width:'15%', title: '<span title="'+data.subject[7].serial_number+'、'+data.subject[7].subject+'">'+data.subject[7].serial_number+'、'+data.subject[7].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_8)}}</div>'}
		                ,{field:'answer_9', width:'15%', title: '<span title="'+data.subject[8].serial_number+'、'+data.subject[8].subject+'">'+data.subject[8].serial_number+'、'+data.subject[8].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_9)}}</div>'}
		                ,{field:'answer_10', width:'15%', title: '<span title="'+data.subject[9].serial_number+'、'+data.subject[9].subject+'">'+data.subject[9].serial_number+'、'+data.subject[9].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_10)}}</div>'}
		                
		                ,{field:'answer_11', width:'15%', title: '<span title="'+data.subject[10].serial_number+'、'+data.subject[10].subject+'">'+data.subject[10].serial_number+'、'+data.subject[10].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_11)}}</div>'}
		                ,{field:'answer_12', width:'15%', title: '<span title="'+data.subject[11].serial_number+'、'+data.subject[11].subject+'">'+data.subject[11].serial_number+'、'+data.subject[11].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_12)}}</div>'}
		                ,{field:'answer_13', width:'15%', title: '<span title="'+data.subject[12].serial_number+'、'+data.subject[12].subject+'">'+data.subject[12].serial_number+'、'+data.subject[12].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_13)}}</div>'}
		                ,{field:'answer_14', width:'15%', title: '<span title="'+data.subject[13].serial_number+'、'+data.subject[13].subject+'">'+data.subject[13].serial_number+'、'+data.subject[13].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_14)}}</div>'}
		                ,{field:'answer_15', width:'15%', title: '<span title="'+data.subject[14].serial_number+'、'+data.subject[14].subject+'">'+data.subject[14].serial_number+'、'+data.subject[14].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_15)}}</div>'}
		                
		                ,{field:'answer_16', width:'15%', title: '<span title="'+data.subject[15].serial_number+'、'+data.subject[15].subject+'">'+data.subject[15].serial_number+'、'+data.subject[15].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_16)}}</div>'}
		                ,{field:'answer_17', width:'15%', title: '<span title="'+data.subject[16].serial_number+'、'+data.subject[16].subject+'">'+data.subject[16].serial_number+'、'+data.subject[16].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_17)}}</div>'}
		                ,{field:'answer_18', width:'15%', title: '<span title="'+data.subject[17].serial_number+'、'+data.subject[17].subject+'">'+data.subject[17].serial_number+'、'+data.subject[17].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_18)}}</div>'}
		                ,{field:'answer_19', width:'15%', title: '<span title="'+data.subject[18].serial_number+'、'+data.subject[18].subject+'">'+data.subject[18].serial_number+'、'+data.subject[18].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_19)}}</div>'}
		                ,{field:'answer_20', width:'15%', title: '<span title="'+data.subject[19].serial_number+'、'+data.subject[19].subject+'">'+data.subject[19].serial_number+'、'+data.subject[19].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_20)}}</div>'}
		                
		                ,{field:'answer_21', width:'15%', title: '<span title="'+data.subject[20].serial_number+'、'+data.subject[20].subject+'">'+data.subject[20].serial_number+'、'+data.subject[20].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_21)}}</div>'}
		                ,{field:'answer_22', width:'15%', title: '<span title="'+data.subject[21].serial_number+'、'+data.subject[21].subject+'">'+data.subject[21].serial_number+'、'+data.subject[21].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_22)}}</div>'}
		                ,{field:'answer_23', width:'15%', title: '<span title="'+data.subject[22].serial_number+'、'+data.subject[22].subject+'">'+data.subject[22].serial_number+'、'+data.subject[22].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_23)}}</div>'}
		                ,{field:'answer_24', width:'15%', title: '<span title="'+data.subject[23].serial_number+'、'+data.subject[23].subject+'">'+data.subject[23].serial_number+'、'+data.subject[23].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_24)}}</div>'}
		                ,{field:'answer_25', width:'15%', title: '<span title="'+data.subject[24].serial_number+'、'+data.subject[24].subject+'">'+data.subject[24].serial_number+'、'+data.subject[24].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_25)}}</div>'}
		                
		                ,{field:'answer_26', width:'15%', title: '<span title="'+data.subject[25].serial_number+'、'+data.subject[25].subject+'">'+data.subject[25].serial_number+'、'+data.subject[25].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_26)}}</div>'}
		                ,{field:'answer_27', width:'15%', title: '<span title="'+data.subject[26].serial_number+'、'+data.subject[26].subject+'">'+data.subject[26].serial_number+'、'+data.subject[26].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_27)}}</div>'}
		                ,{field:'answer_28', width:'15%', title: '<span title="'+data.subject[27].serial_number+'、'+data.subject[27].subject+'">'+data.subject[27].serial_number+'、'+data.subject[27].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_28)}}</div>'}
		                ,{field:'answer_29', width:'15%', title: '<span title="'+data.subject[28].serial_number+'、'+data.subject[28].subject+'">'+data.subject[28].serial_number+'、'+data.subject[28].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_29)}}</div>'}
		                ,{field:'answer_30', width:'15%', title: '<span title="'+data.subject[29].serial_number+'、'+data.subject[29].subject+'">'+data.subject[29].serial_number+'、'+data.subject[29].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_30)}}</div>'}
		                	
		                ,{field:'answer_31', width:'15%', title: '<span title="'+data.subject[30].serial_number+'、'+data.subject[30].subject+'">'+data.subject[30].serial_number+'、'+data.subject[30].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_31)}}</div>'}
		                ,{field:'answer_32', width:'15%', title: '<span title="'+data.subject[31].serial_number+'、'+data.subject[31].subject+'">'+data.subject[31].serial_number+'、'+data.subject[31].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_32)}}</div>'}
		                ,{field:'answer_33', width:'15%', title: '<span title="'+data.subject[32].serial_number+'、'+data.subject[32].subject+'">'+data.subject[32].serial_number+'、'+data.subject[32].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_33)}}</div>'}
		                ,{field:'answer_34', width:'15%', title: '<span title="'+data.subject[33].serial_number+'、'+data.subject[33].subject+'">'+data.subject[33].serial_number+'、'+data.subject[33].subject+'</span>' ,total:true,templet:'<div>{{deleteLast_character(d.answer_34)}}</div>'}
		                ,{field:'answer_35', width:'15%', title: '<span title="'+data.subject[34].serial_number+'、'+data.subject[34].subject+'">'+data.subject[34].serial_number+'、'+data.subject[34].subject+'</span>',total:true,templet:'<div>{{deleteLast_character(d.answer_35)}}</div>'}
		                
		            ]]
		            ,done: function(res, curr, count){
						
					}
					,total:true
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
		    	
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.PageCond.count
				    ,total:true
				    ,curr : page
				    ,limit:limit
				    ,limits : [10, 20, 30, 40, 50,data.PageCond.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				     	 init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
 //查询
    function doSearch(){
    	//默认查询报表数据
	 	init_table();
    }
    
    //查询问卷模版
	function selectTemplate(){
		$.ajax({
       		url: 'com.recruit.reports.questionnaireReport.queryTemplate.biz.ext',
			type:'POST',
			cache: false,
			async:false,
			success:function(data){
				var template = data.template_name;
				for(var i=0;i<template.length;i++){
					$("#recruitPlan").append('<option value="'+template[i].template_id+'" >'+template[i].template_name+'</option>');
			 	}
			}
		})
			//默认查询报表数据
	 		init_table();
	}
    
  var Header;
  //拼接导出Excel表头
  function splicingHeader(obj){
  	Header='{"template_name":"问卷名称","template_time":"发送时间","sub_time":"提交时间", ';
  	for(var i=0;i<obj.length;i++){
  		var j=i+1;
  		//如果没题目则不拼接
  		if(obj[i].subject=="无"){
  			
  		}else{
	  		Header+='"answer_'+j+'":"'+obj[i].serial_number+'、'+obj[i].subject+'",';
  		}
  	}
  	Header=Header.substr(0,Header.length-1);
  	Header+='}';
 	
  }
  
  //导出
  function daochu(){
  	ExcalStats = true;
  		//问卷模版
		var template_id=$("#recruitPlan").val();
		//时间
		var date = $("#date").val();
  		
		var json = {curr:page,limit:count,template_id:template_id,time:date
				,head : Header
		        ,sheetName : "问卷调查报表"
		        ,fileName : "问卷调查报表"
		        //web路径
		        ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.reports.questionnaireReport.excalExport.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			
			var downloadpath=data.downloadpath;
			var file=data.file;
			
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
					if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = downloadpath;
 						document.getElementById("fileName").value = file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
			        }
				}
			}
		}) 
    }
    
    //删除最后一个字符
    function deleteLast_character(aa){
		var html ="";
		if(aa.charAt(aa.length - 1)==";"){
			html=aa.substr(0,aa.length-1);
		}else{
			html=aa;
		}
			
		return html;
    }
    
 //报表的重置方法
    function resetRecr2(){
    	document.getElementById("form1").reset();
    	init_table();
    }
</script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>