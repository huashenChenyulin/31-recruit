<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): wj
  - Date: 2018-05-14 10:03:57
  - Description:人才简历表
-->
<head>
<link type="text/css" rel="styleSheet"
	href="<%= request.getContextPath() %>/reports/css/telentReport.css" />

<style type="text/css">
	.form-inline .form-control{
		    width: 65%;
	}
	 /*报表的数据高度*/
 .layui-table-view{
		height: 392px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
	#add_label{
		width: 15.8%;
		margin-left: 4px;
		
	}
	#label{
		margin-top: 18px;
		float: left;
	}
</style>
<title>人才简历表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    	<!-- 报表通用样式  -->
    <link rel="stylesheet" type="text/css"
		href="<%= request.getContextPath() %>/reports/css/report.css">
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>人才简历表</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
						<td width="270">
							<label>姓名:</label> 
							<input type="text"
								class="form-control" id="candidate_name" 
								name="candidate_name" >
						</td>
						<td width="300">
							<label>创建人:</label> 
							<input type="text"
								class="form-control" id="recruit_name" 
								name="recruit_name" >
						</td>
						<td width="350">
				      		<label>存入人才库日期:</label>  
					        <input type="text"
								class="form-control" id="atual_hire_date" 
								name="atual_hire_date" placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center" colspan="6">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="doSearch()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="clean()">重置</button>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
			<div id="content_label" >
			
				<div id="content_label_button">
					<label id="label">标签:</label>
					<div id="add_label">
						标签选择<i class="layui-icon">&#xe602;</i>
					</div>
					<div id="content_label2"></div>
				</div>
			</div>
		</div>
			
	</div>
	
	<!-- 标签 -->
		<div id="main-center">
				<div id="content-one_label">
					<ul class="list-group">
					</ul>
				</div>
			<div id="content-Two_label"></div>
		</div>
	
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="personal" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
<script type="text/javascript">
	var page = 1;//初始化页数
	var limit = 9; //初始条目数
	var count = 0;
	var table;
	
	$(function(){
		init_laydate();// 初始化时间控件
	 	init_table();
	 	dictionarycombox();
	});
	var dictionData;
	//获取下拉框数据字典数据
	function dictionarycombox() {
		$.ajax({
			url : "com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
			type : 'POST',
			success : function(data) {
				dictionData=data;
			}
		});
	}
		
	function getsalary(id){
		var salaryname = "";
		$(dictionData.reexpected).each(function(i,obj){
			if(id==obj.dictID){
			salaryname=obj.dictName;
			}
		});
		return salaryname;
	}
	
	function init_table(){
		//工作时间
		var ReportDtime = $("#atual_hire_date").val();
		var Datetime = ReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var starttime = alltime[0];
		var n = alltime[1];
   		var endtime = dateAddDays(n,1);
		//姓名
		var candidatename = $("#candidate_name").val();
		//创建人
		var recruit_name = $("#recruit_name").val();
		//获取标签id
		    		var obj="";
		    		for(var key1 in testMap){
						obj +=key1+",";
					} 
		    		obj=obj.substring(0, obj.length-1);
		
		var json = {"curr":page,"limit":limit,"startcreatedtime":starttime,"endcreatedtime":endtime,"candidatename":candidatename,"lableId":obj,"recruit_name":recruit_name};
       	$.ajax({
       		url: 'com.recruit.reports.talentReports.slectTalent.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
				
				layui.use('table', function(){
		       	table = layui.table;
		        table.render({
		            elem: '#personal'
		            ,data:data.return_talent
		            ,limit:limit
		            ,height:480
		            ,cols: [[
		                {field:'candidate_name', width:'11%', title: '姓名',align:'center'}
		                ,{field:'degree', width:'13%', title: '学历',align:'center'}
		                ,{field:'gender', width:'5%', title: '性别',align:'center'}
		                ,{field:'birth_date', width:'5%', title: '年龄',align:'center'}
		                ,{field:'created_time', width:'10%', title: '创建人才库日期',align:'center'}
		                ,{field:'recruiter_name', width:'10%', title: '创建人',align:'center'}
		                ,{field:'update_time', width:'12%', title: '简历更新日期',align:'center'}
		                ,{field:'candidate_phone', width:'10%', title: '联系方式',align:'center'}
		                ,{field:'excepted_work_area_a', width:'14%', title: '工作意向地',align:'center'}
		                ,{field:'address', width:'14%', title: '现居住地',align:'center'}
		                ,{field:'recuit_order_id', width:'15%', title: '单号',align:'center'}
		                ,{field:'position', width:'15%', title: '应聘职位',align:'center'}
		                ,{field:'agreement_ph', width:'11%', title: 'HR电话面试评价',align:'center'}
		                ,{field:'agreement_hr', width:'11%', title: 'HR现场面试评价',align:'center'}
		                ,{field:'agreement_em', width:'14%', title: '业务部门现场面试评价',align:'center'}
		                ,{field:'expected_salary', width:'10%', title: '期待薪资',align:'center'}
		                ,{field:'recruit_process_id', width:'10%', title: '候选人放弃环节',align:'center'}
		                ,{field:'abandon_details', width:'10%', title: '归档原因',align:'center'}
		                ,{field:'offer_status', width:'10%', title: '是否发送offer',align:'center'}
		                ,{field:'abandon_reason', width:'12%', title: '归档原因描述',align:'center'}
		                ,{field:'talent_status', width:'8%', title: '是否入职',align:'center'}
		                //,{field:'delayed_reason', width:'10%', title: '不入职的原因',align:'center'}
		            ]]
		            ,done: function(res, curr, count){
						/* if(ExcalStats){
							daochuExcal('personal','dlink','人才简历表');
						} */
						/* layer.close(loading); */
					}
		        });
		    });
		    layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
					laypage.render({
				    elem: 'demo'
				    ,count: data.return_pg.count
				    ,curr : page
				    ,limit:limit
				    ,limits : [9, 20, 30, 40, 50,data.return_pg.count]
				    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
				    ,jump: function(obj,first){
				      count = obj.count;
				      limit = obj.limit;
				      page = obj.curr;
				      if(!first){
				      init_table();
				      }
				    }
				  });
			  });
			}
       });
	} 
	
	//判断候选人放弃环节a,所在环节b,候选人状态
	function recruit_process(a,b){
		var process ="";
		if(b == 3){//候选人状态为已归档为放弃环节
			if(a == 1){
				process="初筛环节";
			}else if(a == 2){
				process="电话沟通环节";
			}else if(a == 3){
				process="人力面试环节";
			}else if(a == 4){
				process="部门面试环节";
			}else if(a == 5){
				process="待入职环节";
			}else if(a == 6){
				process="已入职环节";
			}
		}else{
			process="";
		}
		return process;
	}
	
 	//查询
    function doSearch(){
    	init_table();
    }
  function daochu(){
  		ExcalStats = true;
  		//招聘负责人
		//工作时间
		var ReportDtime = $("#atual_hire_date").val();
		var Datetime = ReportDtime.replace(/\s+/g,"");
		var alltime = Datetime.split("~");
		var starttime = alltime[0];
		var n = alltime[1];
   		var endtime = dateAddDays(n,1);
		//姓名
		var candidatename = $("#candidate_name").val();
		//创建人
		var recruit_name = $("#recruit_name").val();
		//获取标签id
		    		var obj="";
		    		for(var key1 in testMap){
						obj +=key1+",";
					} 
		    		obj=obj.substring(0, obj.length-1);
		
		var json = {"curr":page,"limit":count,"startcreatedtime":starttime,"endcreatedtime":endtime,"candidatename":candidatename,"lableId":obj,"recruit_name":recruit_name
				,head : '{"candidate_name":"姓名","degree":"学历","gender":"性别","birth_date":"年龄","created_time":"创建人才库日期","recruiter_name":"创建人","update_time":"简历更新日期","candidate_phone":"联系方式","excepted_work_area_a":"工作意向地","address":"现居住地","recuit_order_id":"单号","position":"应聘职位","agreement_ph":"HR电话面试评价","agreement_hr":"HR现场面试评价","agreement_em":"业务部门现场面试评价","expected_salary":"期待薪资","recruit_process_id":"候选人放弃环节","abandon_details":"归档原因","offer_status":"是否发送offer","abandon_reason":"归档原因描述","talent_status":"是否入职"}'
		      ,sheetName : "人才简历表"
		      ,fileName : "人才简历表"
		      //web路径
		      ,filePath :filePath
		};
       	$.ajax({
       		url: 'com.recruit.excelEntery.ExcelDerive.createTalentReportsExcel.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		});
    }
    function clean(){
			$('#candidate_name').val("");
			$('#recruit_name').val("");
			$('#atual_hire_date').val("");
			testMap={};
			var obj="#content_label2 span";
			$(obj).remove();//清除
		}
</script>
<script
	src="<%= request.getContextPath() %>/Talent/js/resume_conditional_select.js"
	type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/reports/js/enter.js" type="text/javascript"></script>
</body>
</html>
