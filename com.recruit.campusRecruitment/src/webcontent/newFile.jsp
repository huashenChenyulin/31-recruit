<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-03-14 17:23:31
  - Description:
-->
<head>
<title>简历查看</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
	
	<!-- 查看简历CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/campusRecruitment/css/resumes_select.css">
	<%
		String resumeId = request.getParameter("resumeId");
		
	 %>

<style type="text/css">
	
	
	.table tbody tr td, .table tbody tr th, .table tfoot tr td, .table tfoot tr th, .table thead tr td, .table thead tr th{
		border-top: 0px solid;
	}
	.table thead tr th{
		border-bottom: 1px solid #ddd;
		color: #565656;
	}
	tbody{
		color: #656565;
	}
	
</style>
</head>
<body>
	<div id="main">
		<!-- 左布局 -->
		<div id="main-left">
			
			<!-- 中部的求职意向信息 -->
			<div id="content-centre">
				<table class="table">
					<caption>基本信息</caption>
					
					
					
					<thead>
						<tr>
							<th>身份证号码</th>
							<th>户口所在地</th>
							<th>现居地</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="identityNo"></td>
							<td id="birthplace"></td>
							<td id="address"></td>
						</tr>
					</tbody>
					
					<thead>
						<tr>
							<th>紧急联系人姓名</th>
							<th>与紧急联系人关系</th>
							<th>紧急联系人电话</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="emergencyContact"></td>
							<td id="emergencyRelationship"></td>
							<td id="emergencyPhone"></td>
						</tr>
					</tbody>
					
					<thead>
						<tr>
							<th>身高(cm)/体重(kg)</th>
							<th>英语等级</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="heightAndWeight"></td>
							<td id="english_level"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- 尾部的-->
			<div id="centent-bottom">
				<table class="table" >
					<caption>申请职位</caption>
			
					<thead>
						<tr>
							<th>期望工作城市</th>
							<th>期望薪资</th>
							<th>得知招聘信息的渠道</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="expected-address"></td>
							<td id="expected-salary"></td>
							<td id="recruit-noticed-channel"></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th>是否服从工作职位的调配</th>
							<th>是否服从工作城市的调配</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="isPositionArrange"></td>
							<td id="is-workplace-arrange"></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<!-- 尾部的-->
			<div id="centent-bottom2">
				<table class="table" id="personalResume">
					
				</table>
				<table class="table" id="recruitSchoolPractice">
					
				</table>
				<table class="table" id="recruitWorkExperience">
					
				</table>
			</div>

		</div>
		
	<script type="text/javascript">
		
		var resumeID=<%=resumeId %>;
	
		var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	
		//是否服从工作职位的调配
		 function is_positionArrange(i){
			var isPositionArrange="";
		 	if(i==1){
		 		isPositionArrange="是";
		 	}else{
		 		isPositionArrange="否";
		 	}
		 	return isPositionArrange;
		 } 	
		 //是否服从工作城市的调配
		 function is_workplaceArrange(i){
			var workplaceArrange="";
		 	if(i==1){
		 		workplaceArrange="是";
		 	}else{
		 		workplaceArrange="否";
		 	}
		 	return workplaceArrange;
		 } 	
		 //得知招聘信息的渠道
		 function recruitNoticedChannel(i){
			var recruitNoticedChannel="";
		 	if(i==1){
		 		recruitNoticedChannel="索菲亚官网";
		 	}else if(i==2){
		 		recruitNoticedChannel="宣讲会";
		 	}else if(i==3){
		 		recruitNoticedChannel="微信推送";
		 	}else if(i==4){
		 		recruitNoticedChannel="招聘网站";
		 	}else if(i==5){
		 		recruitNoticedChannel="其他";
		 	}
		 	return recruitNoticedChannel;
		 }
		 //英语等级
		 function englishLevel(i){
			var englishLevel="";
		 	if(i==01){
		 		englishLevel="大学英语四级";
		 	}else if(i==02){
		 		englishLevel="大学英语六级";
		 	}else if(i==03){
		 		englishLevel="专业四级";
		 	}else if(i==04){
		 		englishLevel="专业八级";
		 	}else{
		 		englishLevel="无";
		 	}
		 	return englishLevel;
		 }
		  	
		 selectResume();
		 function selectResume(){
		 	var id='113753';
		 	var json={
		 		resumeId:resumeID
		 	}
		 	$.ajax({
		 		url:"com.recruit.campusRecruitment.campusManagement.resume.queryResume.biz.ext",
		 		type:"post",
		 		data:json,
		 		success:function(data){
		 			//简历表
		 			var resume=data.resume;
		 			//职位表
		 			var position=data.position;
		 			//附件表
		 			var resume_axccessory=data.resume_axccessory;
		 			//在校实践表
		 			var resume_school=data.resume_school;
		 			//社会实践表
		 			var resume_work=data.resume_work;
		 			

		 			//现居地
		 			$("#address").text(resume[0].address);
		 			
		 			//身份证
		 			$("#identityNo").text(resume[0].identityNo);
		 			//户口所在地
		 			$("#birthplace").text(resume[0].birthplace);
		 			
	 				var weig=resume[0].weight;//体重
	 				var heig=resume[0].height;//身高
		 			if(resume[0].height=="" || resume[0].height==null){
		 				//身高
		 				heig="";
		 			}
					if(resume[0].weight=="" || resume[0].weight==null){
		 				//体重
		 				weig ="";
		 			}
	 				//身高体重
	 				$("#heightAndWeight").text(heig+"/"+weig);
		 			
		 			//紧急联系人
		 			$("#emergencyContact").text(resume[0].emergencyContact);
		 			//联系人关系
		 			$("#emergencyRelationship").text(resume[0].emergencyRelationship);
		 			//联系人电话
		 			$("#emergencyPhone").text(resume[0].emergencyPhone);
		 			//英语等级
		 			$("#english_level").text(englishLevel(resume[0].englishLevel));
		 			//毕业时间
		 			$("#graduationDate").text(resume[0].graduationDate);
		 			
		 			//期望工作城市
		 			$("#expected-address").text(resume[0].exceptedWorkAreaA);
		 			//期望薪资
		 			$("#expected-salary").text(resume[0].expectedSalary);
		 			//得知招聘信息的渠道
		 			if(resume[0].recruitNoticedChannelName!="" && resume[0].recruitNoticedChannelName!=null){
		 				$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel)+"—"+resume[0].recruitNoticedChannelName);
		 			}else{
			 			$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel));
		 			}
		 			//是否服从工作职位的调配
		 			$("#isPositionArrange").text(is_positionArrange(resume[0].isPositionArrange));
		 			//是否服从工作城市的调配
		 			$("#is-workplace-arrange").text(is_workplaceArrange(resume[0].isWorkplaceArrange));
		 			
		 			//个人履历 start
		 			var html="";
		 			html+='<caption>个人履历</caption>';
		 			html+='<thead><tr><th>是否有补考或重修记录</th><th>是否受过处分</th></tr></thead>';
		 			html+='<tbody><tr>';
		 			if(resume[0].retestTimes==null || resume[0].retestTimes==""){
		 				html+='<td id="is-retest">无</td>';
		 			}else{
		 				html+='<td id="is-retest">'+resume[0].retestTimes+' 次</td>';
		 			}
		 			if(resume[0].punishmentTimes==null || resume[0].punishmentTimes==""){
		 				html+='<td id="is-punished">无</td>';
		 			}else{
		 				html+='<td id="is-punished">'+resume[0].punishmentTimes+' 次</td>';
		 			}
		 			html+='</tr></tbody>';
		 			$("#personalResume").append(html);
		 			//end
		 			
		 			//个人履历之在校实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html2="<caption>在校实践</caption>" 
			 			for(var i=0;i<resume_school.length;i++){
			 				html2+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="">'+resume_school[i].startTime+' ~  '+resume_school[i].endTime+'</td>';
			 				html2+='<td id="">'+resume_school[i].practiceName+'</td>';
			 				html2+='</tr></tbody>';
			 				html2+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="" colspan="2">'+resume_school[i].practiceDetails+'</td>';
			 				html2+='</tr></tbody>';
			 			}
			 			$("#recruitSchoolPractice").append(html2);
		 				
		 			}
		 			//end
		 			
		 			//个人履历之社会实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html3="<caption>社会实践</caption>" 
			 			for(var i=0;i<resume_work.length;i++){
			 				html3+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="">'+resume_work[i].startTime+' ~  '+resume_work[i].endTime+'</td>';
			 				html3+='<td id="">'+resume_work[i].experienceName+'</td>';
			 				html3+='</tr></tbody>';
			 				html3+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="" colspan="2">'+resume_work[i].experienceContent+'</td>';
			 				html3+='</tr></tbody>';
			 			}
			 			$("#recruitWorkExperience").append(html3);
		 			}
		 			//end
		 			
		 		}
		 	})
		 }
	</script>
</body>
</html>
