<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
	<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
	<%
		String id = request.getParameter("id");
	 %>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-08-08 22:00:52
  - Description:
-->
<head>
<title>新增发布职位</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">
	.layui-form-label {
    width: 110px;
    
}
.layui-form{
	margin-right: 6%;
}
.layui-method{
	width: 212px !important;
}
.layui-form-item{
	margin-bottom: 0px;
}
.layui-form-text{
	margin-bottom:20px;
}
.btn-submit{
	margin-bottom: 20px;
}
</style>	
</head>
<body>
<form class="layui-form"   lay-filter="example" >
<div class="layui-form-item" style="display: none;">
    <label class="layui-form-label">id:</label>
    <div class="layui-input-block">
      <input type="text" name="id"  autocomplete="off" placeholder="请输入标题" class="layui-input">
    </div>
  </div>
	<div class="layui-form-item">
	<div class="layui-inline">
    <label class="layui-form-label">职位名称:</label>
    <div class="layui-input-block">
      <input type="text" id="positionName" name="positionName" lay-verify="required" placeholder="请输入职位名称" autocomplete="off" class="layui-input layui-method">
    </div>
    </div>
    
    <div class="layui-inline">
	    <label class="layui-form-label">是否发布:</label>
	    <div class="layui-input-block">
	      <input type="radio" id="isVaild" name="isVaild" value="1" title="发布"checked>
	      <input type="radio" id="isVaild" name="isVaild" value="2" title="不发布">
	    </div>
	  </div>
  </div>
  </div>
  
  <div class="layui-form-item">
  <div class="layui-inline">
      <label class="layui-form-label">工作地点:</label>
      <div class="layui-input-block">
        <input type="text" name="company" id="company" class="layui-input layui-method">
      </div>
    </div>
	
	  <div class="layui-inline">
    <label class="layui-form-label">项目类别:</label>
    <div class="layui-input-block">
      <select id="degreeRequirement" name="degreeRequirement" lay-verify="required">
        <option value="1" selected="">菲腾项目</option>
        <option value="2">TTD项目</option>
      </select>
    </div>
   </div>
  
  <div class="layui-form-item">
	  <div class="layui-inline">
	    <label class="layui-form-label">招聘类别:</label>
	    <div class="layui-input-block">
	      <select id="recruitType" name="recruitType" lay-verify="required">
	        <option value="1" selected="">校园招聘</option>
	        <option value="2">社会招聘</option>
	      </select>
	    </div>
    </div>
    
    <div class="layui-inline">
	    <label class="layui-form-label">职位类别:</label>
	    <div class="layui-input-block">
	      <select id="positionType" name="positionType" lay-verify="required">
	        <option value="">请选择职位类别</option>
	      </select>
	    </div>
	  </div>
  </div>
  
 <!--  <div class="layui-form-item">
  	<div class="layui-inline">
      <label class="layui-form-label">开始日期:</label>
      <div class="layui-input-block">
        <input type="text" name="starDate" id="starDate"  placeholder="yyyy-MM-dd" class="layui-input layui-method" lay-verify="required">
      </div>
    </div>
    
    <div class="layui-inline">
      <label class="layui-form-label">结束日期:</label>
      <div class="layui-input-block">
        <input type="text" name="endDate" id="endDate"  placeholder="yyyy-MM-dd" class="layui-input layui-method" lay-verify="endDate">
      </div>
    </div>
  </div> -->
  
    <!--  <div class="layui-inline">
	    <label class="layui-form-label">工作地点:</label>
	    <div class="layui-input-block">
	      <select id="workplace" name="workplace" >
	        <option value="">请选择工作地点</option>
	      </select>
	    </div>
	  </div> -->
	  
	  <div class="layui-inline">
      <label class="layui-form-label">专业要求:</label>
      <div class="layui-input-block">
        <input type="text" name="major" id="major" class="layui-input layui-method">
      </div>
    </div>
       <div class="layui-inline">
     	 <label class="layui-form-label">所属公司:</label>
		      <div class="layui-input-block">
		     	 <select id="workplace"name="workplace" class="form-control" lay-verify="required">
		     	 	<option value=''>请选择所属公司</option>
		     	 </select>
		      </div>
       </div>
  
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">发展方向:</label>
    <div class="layui-input-block">
      <textarea name="career" placeholder="请输入内容" class="layui-textarea"></textarea>
    </div>
  </div>
  
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">职位描述:</label>
    <div class="layui-input-block">
      <textarea name="responsibilities"  placeholder="请输入内容" class="layui-textarea"></textarea>
    </div>
  </div>
  
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">职位要求:</label>
    <div class="layui-input-block">
      <textarea name="requirements" placeholder="请输入内容" class="layui-textarea"></textarea>
    </div>
  </div>
  
	 <div class="layui-form-item btn-submit">
    <div class="layui-input-block" align="center">
      <button class="layui-btn" lay-submit lay-filter="position">立即提交</button>
    </div>
  </div>
</form>	
	<script type="text/javascript">
	$(function(){
		init_positionType();// 初始化职位类别下拉框控件
	 	queryCompany();//查询所属公司
	 	init_table();
	});
	
	function init_table(){
	var id = "<%=id %>";
	if( id ==null || id == "null" ||<%=id %>==""){
		//新增初始化
		addTable();
	}else{
		//编辑初始化
		updateTable(id)
		}
	 }
	 
	 function updateTable(id){
	 
	 	var json = {id:id};
		$.ajax({
  			url:"com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryPosition.biz.ext",
  			type:"POST",
  			data:json,
  			success:function(data){
  			
				layui.use(['form', 'layedit', 'laydate'], function(){
				  var form = layui.form
				  ,layer = layui.layer
				  ,layedit = layui.layedit
				  ,laydate = layui.laydate;
				  
				  /* //初始化开始日期
				  laydate.render({
				    elem: '#starDate'
				    ,min: getNowFormatDate()
				  });
				  
				  //自定义验证
				    form.verify({
				    //验证结束时间大于开始时间
					  endDate: function(value, item){ //value：表单的值、item：表单的DOM对象
					    var starDate = $("#starDate").val();
					    if(value < starDate ){
					    	return "结束日期必须大于等于开始日期！";
					    }
					  }
					});
					
				  //初始化结束日期
				  laydate.render({
				    elem: '#endDate'
				    ,min: getNowFormatDate()
				  }); */
				  
		  		 $("input[name=isVaild][value=1]").attr("checked", data.RecruitPosition[0].isVaild == 1 ? true : false);
                 $("input[name=isVaild][value=2]").attr("checked", data.RecruitPosition[0].isVaild == 2 ? true : false);
                 form.render();
			  //表单初始赋值
				   form.val('example', {
				    "id": data.RecruitPosition[0].id // "name": "value"
				    ,"positionName": data.RecruitPosition[0].positionName
				    ,"positionType": data.RecruitPosition[0].positionType
				    
				    ,"degreeRequirement": data.RecruitPosition[0].degreeRequirement
				    ,"workplace": data.RecruitPosition[0].workplace
				    /* ,"starDate": data.RecruitPosition[0].starDate
				    ,"endDate": data.RecruitPosition[0].endDate */
				    //,"like[write]": true //复选框选中状态
				    ,"responsibilities": data.RecruitPosition[0].responsibilities
				    ,"recruitType" : data.RecruitPosition[0].recruitType
				    
				    ,"requirements": data.RecruitPosition[0].requirements
				    ,"career": data.RecruitPosition[0].career
				    ,"company":data.RecruitPosition[0].company
				    ,"major":data.RecruitPosition[0].major
				  }) 
			  
			  
			  //监听提交
			  form.on('submit(position)', function(obj){
			  
			  		submit(obj);
			 	 });
			  })
  			}
  		})
	 
	 }
	 
	 function addTable(){
		layui.use(['form', 'layedit', 'laydate'], function(){
		  var form = layui.form
		  ,layer = layui.layer
		  ,layedit = layui.layedit
		  ,laydate = layui.laydate
		  ,$ = layui.jquery
		  ;
		  
		  /* //初始化开始日期
		  laydate.render({
		    elem: '#starDate'
		    ,min: getNowFormatDate()
		  });
		  
		  //初始化结束日期
		  laydate.render({
		    elem: '#endDate'
		    ,min: getNowFormatDate()
		  }); */
		  //监听提交
		  form.on('submit(position)', function(data){
		  	submit(data);
		  });
		//自定义验证
	    /* form.verify({
	    //验证结束时间大于开始时间
		  endDate: function(value, item){ //value：表单的值、item：表单的DOM对象
		    var starDate = $("#starDate").val();
		    if(value < starDate ){
		    	return "结束日期必须大于等于开始日期！";
		    }
		  }
		});  */
	});
}
//获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
	  //提交				
	 function submit(obj){
	 	 $.ajax({
			  			url:"com.recruit.campusRecruitment.campusManagement.RecruitPosition.insertPosition.biz.ext",
			  			type:"POST",
			  			data:obj.field,
			  			async:false,
			  			success:function(data){
			  				close();
			  				}
		  			});
	 }
	 
	  //初始化职位类别
	function init_positionType(){
	
	$.ajax({
  			url:"com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryPositionType.biz.ext",
  			type:"POST",
  			success:function(data){
  			//获取职位类别信息
  				var positionType = data.positionType;
  				//初始化职位类别下拉框信息
  				for(var i=0;i<positionType.length;i++){
					$("#positionType").append('<option value="'+positionType[i].dictID+'">'+positionType[i].dictName+'</option>');
			 	}
			 	//获取职位工作类别
			 	var position_workplace = data.position_workplace;
  				//初始化职位类别下拉框信息
//   				for(var i=0;i<position_workplace.length;i++){
// 					$("#workplace").append('<option value="'+position_workplace[i].dictName+'">'+position_workplace[i].dictName+'</option>');
// 			 	}
  			}
  			});
	
	}
	//查询所属公司
	function queryCompany(){
		$.ajax({
			url:'com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryUserCompany.biz.ext',
			type:'POST',
			dataType:'json',
			async:false, 
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					//动态渲染期望公司
					var option = "";
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						option+="<option value="+data.RecruitCompany[i].company_id+">"+data.RecruitCompany[i].company_name+"</option>";
					}
					$("#workplace").append(option);
				} 
			}
		});
	}
	
	function close(){
	       var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.layer.close(index);
	}
    </script>
</body>
</html>