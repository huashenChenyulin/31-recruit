<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
<%
String starDate = request.getParameter("starDate");
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-11-15 08:44:04
  - Description:
-->
<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
    	.layui-form-label {
		    width: 110px;
		    
		}
    .layui-input{
    	width: 80%;
    }
    select {
    width: 80%;
    height: 38px;
}
    </style>
</head>
<body>
 <div class="layui-form-item">
  	 <div class="layui-inline">
      <label class="layui-form-label">招聘年度:</label>
      <div class="layui-input-block">
        <input type="text" name="recruitYear" id="recruitYear"  placeholder="yyyy" class="layui-input" lay-verify="required|number">
      </div>
     </div>
     <div class="layui-inline">
	    <label class="layui-form-label">招聘周期:</label>
	    <div class="layui-input-block">
	      <select id="recruitTope" name="recruitTope" lay-verify="required">
	      	<option value="" selected="">请选择招聘周期</option>
	        <option value="1">春招</option>
	        <option value="2">秋招</option>
	      </select>
	    </div>
	  </div>
    </div>
<script type="text/javascript">
	
	$(function(){
		//初始化时间控件
		init_date();
	});
	
	//获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
    
    //初始化时间
    function init_date(){
		layui.use(['laydate'], function(){
		  var laydate = layui.laydate;
		  
		  //初始化年度控件
		  laydate.render({
		    elem: '#recruitYear'
		    ,type: 'year'
		  });
	 });
	
	}
</script>
</body>
</html>