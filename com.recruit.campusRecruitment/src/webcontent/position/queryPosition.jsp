<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-08-08 15:19:01
  - Description:
-->
<head>
<title>职位管理页面</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">
#select tbody tr td{
	padding: 6px;
}
	.layui-form-label {
    width: 110px;
    
}
.span-btn{
	display: inline-block;
    width: 25px;
}
.add-span-btn{
	display: inline-block;
    width: 3px;
}
.layui-btn-warm{
	height: 28px;
	line-height: 24px;
}
</style>    
</head>

<body>

<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table id ="select" style="width: 100%; height: 100%;" >
					<tr align="center">
						<td>
							<label>职位类别:</label> 
							<select id="positionType"name="positionType" class="form-control" style="width: 80%;"">
							<option value="">请选择职位类别</option>
						</select>
						</td>
						<td>
							<label>学历要求:</label> 
							<select id="degreeRequirement"name="degreeRequirement" class="form-control" style="width: 80%;"">
							<option value="">请选择学历要求</option>
						</select>
						</td>
						<td>
							<label>发布状态:</label> 
							<select id="isVaild"name="isVaild" class="form-control" style="width: 80%;">
							<option value="">请选择发布状态</option>
							<option value="1">发布</option>
							<option value="2">不发布</option>
						</select>
						</td>
					</tr>
					
					<tr align="center">
						<td>
							<label>招聘类别:</label> 
							<select id="recruitType"name="recruitType" class="form-control" style="width: 80%;">
								<option value="">请选择招聘类别</option>
								<option value="1">校园招聘</option>
								<option value="2">社会招聘</option>
							</select>
						</td>
						<td>
							<label>工作地点:</label> 
							<input type="text" class="form-control" id="company"
										name="company" style="width: 80%;">
						</td>
						
						<td>
							<label>职位名称:</label> 
							<input type="text" class="form-control" id="positionName"
										name="positionName" style="width: 80%;">
						</td>
					</tr>
					<tr >
						<td>
						<span class="add-span-btn"></span>
						<button class="btn btn-warning" type="button"onclick="add()">新增职位</button>
						</td>
						<td align="center">
							<button class="btn btn-warning" type="button"onclick="selectPosition()">搜索</button>
							<span class="span-btn"></span>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- <div class="panel-body" style="width: 100%; height: 100%;">
		<button class="btn btn-warning" type="button" id="addPosition" onclick="addPosition()">新增职位</button>
	</div> -->
<table class="layui-hide" id="position" lay-filter="positionEvent"></table>
<!-- event: 'setSign'
 -->
<script type="text/html" id="checkboxTpl">
  <input type="checkbox" name="{{d.starDate}}" id="{{d.id}}"  value="{{d.id}}" title="发布" lay-filter="lockDemo" {{(d.isVaild==1)? 'checked' : ''}}  }>
</script>
<script type="text/html" id="update">
 <button class="layui-btn layui-btn-warm" onclick="updatePosition({{d.id}})" >编辑</button>
</script>
<script type="text/html" id="workplace">
	{{convertWorkPlace(d.workplace)}}
</script>
<script>
var degreeRequirement =[{id:'1',name:"菲腾项目"},{id:'2',name:"TTD项目"}]
$(function(){
		init_positionType();// 初始化职位类别下拉框控件
	 	init_table();
	 	
	 	// 初始化学历要求下拉框控件
	 	for(var i=0;i<degreeRequirement.length;i++){
					$("#degreeRequirement").append('<option value="'+degreeRequirement[i].id+'">'+degreeRequirement[i].name+'</option>');
			 	}
	});
//页数
var page;

function init_table(){
		//学历要求
		var degreeRequirement = $("#degreeRequirement").val();
		//职位类别
		var positionType = $("#positionType").val();
		//招聘类别
		var recruitType = $("#recruitType").val();
		//发布状态
		var isVaild = $("#isVaild").val();
		//职位名称
		var positionName = $("#positionName").val();
		//工作地点
		var company = $("#company").val();
		var json = {positionName:positionName,positionType:positionType,degreeRequirement:degreeRequirement,isVaild:isVaild,recruitType:recruitType,company:company};
       	$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryPosition.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			 
				layui.use('table', function(){
				var table = layui.table
  					,form = layui.form;	
			        table.render({
			            elem: '#position'
			            ,data:data.RecruitPosition
			            ,height:380
			            ,cols: [[
			           	 {field:'positionName', width:'15%', title: '职位名称', align:"center"}
			            ,{field:'positionType', width:'15%', title: '职位类别', align:"center",templet: '<div>{{position(d.positionType)}}</div>',}
			            ,{field:'recruitType', width:'10%',align:"center", title: '招聘类别',templet:"<div>{{(d.recruitType==1)? '校园招聘':(d.recruitType==2)?'社会招聘':''}}</div>" }
			            ,{field:'degreeRequirement', width:'10%',align:"center", title: '学历要求',templet:"<div>{{(d.degreeRequirement==1)? '菲腾项目':(d.degreeRequirement==2)?'TTD项目':''}}</div>" }
			           /*  ,{field:'starDate', width:'9%', title: '职位开始日期',align:"center"}
			            ,{field:'endDate', width:'9%', title: '职位结束日期',align:"center"} */
			            ,{field:'company', width:'15%', title: '工作地点',align:"center"}
			            ,{field:'workplace', width:'15%', title: '所属公司',align:"center",templet:'#workplace'}
			            ,{field:'isVaild', title:'是否发布', width:'10%', align:"center",templet: '#checkboxTpl', unresize: true}
			            ,{field:'lock', title:'编辑', width:'10%', templet: '#update', align:"center"}
			            ]]
			            ,page:{
			            	//设置起始页
			            	 curr:page
			            }
			           ,done:function(res,curr,count){
			           		//赋值页数
              				page=curr;
            			}
			            ,limit:7
			            ,limits:[7,20,30,40,50,60,70,80,90]
				        });
					//监听锁定操作
					  form.on('checkbox(lockDemo)', function(obj){
					  	var status;
					  	
					  	if(obj.elem.checked){
					  		status = 1;
					  		
					  	}else{
					  		status = 2;
					  	}
					  	var json ={status:status,id:obj.value};
					  	publish(json);
					    //layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);
					  });
			       });
		  		}
		    });
	}
	//发布事件
	function publish(json){
		$.ajax({
  			url:"com.recruit.campusRecruitment.campusManagement.RecruitPosition.udatePosition.biz.ext",
  			type:"POST",
  			data:json,
  			async:false,
  			success:function(data){
  				init_table();
  			}
  		})
	
	}
	//编辑
	function updatePosition(id){
			var url = '<%= request.getContextPath() %>/campusRecruitment/position/addPosition.jsp?id='+id;
			layer.open({
				type: 2,
				title: '编辑职位',
				content: url,
				area: ['830px', '520px'],
				end: function(layero, index){ 
				  init_table();
				}    
			});
	}
	//新增职位
	function add(){
		var url = '<%= request.getContextPath() %>/campusRecruitment/position/addPosition.jsp';
			layer.open({
				type: 2,
				title: '新增职位',
				content: url,
				area: ['830px', '520px'],
				end: function(layero, index){ 
				  init_table();
				}    
			});
	}
	//搜索
	function selectPosition(){
		page =1;
		init_table();
		
	} 
	//初始化职位类别
	function init_positionType(){
	$.ajax({
  			url:"com.recruit.campusRecruitment.campusManagement.RecruitPosition.queryPositionType.biz.ext",
  			type:"POST",
  			success:function(data){
  				var positionType = data.positionType;
  				for(var i=0;i<positionType.length;i++){
					$("#positionType").append('<option value="'+positionType[i].dictID+'">'+positionType[i].dictName+'</option>');
			 	}
  			}
  			});
	
	}
	//转换期望公司的值
	function convertWorkPlace(val){
		//查询期望公司
		var html ="";
		$.ajax({
			url:'com.recruit.company.queryCompany.queryCompany.biz.ext',
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					//清空元素
					//动态渲染期望公司
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						if(val==data.RecruitCompany[i].companyId){
							value = data.RecruitCompany[i].companyName;
							html = "<div>"+value+"</div>";
						}
					}
				}
			}
		});
		return html;
	}
    //提示框   content提示内容
    function open(content){
		layui.use('layer', function(){ 
		
  			var $ = layui.jquery, layer = layui.layer; 
  			layer.open({
			  title: '信息'
			 ,content: '<div style="text-align: center;">'+content+'</div>'
			 ,btnAlign: 'c' //按钮居中
			 ,offset:'auto'
			 ,closeBtn: false//不显示关闭图标
			 ,end: function(layero, index){ 
				  //init_table();
				}    
			});
		});
	}
</script>
</body>
</html>