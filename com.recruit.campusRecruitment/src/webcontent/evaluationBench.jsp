<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<%@page import="com.eos.access.http.security.config.HttpSecurityConfig"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%
	//获取招聘计划id
	String workbenchUrl = request.getParameter("workbenchUrl");
	String contextPath = StringUtil.htmlFilter(request.getContextPath());
	String URL = null;
	String recruit_id = null;
	String planId = null;
	if(workbenchUrl!=null&&workbenchUrl!=""){
		workbenchUrl = workbenchUrl.replace(" ", "+");
		try{
		//解密招聘计划id
		URL = new String(com.eos.foundation.common.utils.CryptoUtil.decryptByDES(workbenchUrl,null));
		//截取url第一个参数
		recruit_id = URL.substring(URL.indexOf("=")+1,URL.lastIndexOf("&"));  
		//截取url第二个参数
		planId = URL.substring(URL.lastIndexOf("=")+1);
		if(recruit_id!=null&&recruit_id!=""){
		//加密操作员
		recruit_id = com.eos.foundation.common.utils.CryptoUtil.encryptByDES(recruit_id,null);
		}
		//加密招聘计划
		planId =com.eos.foundation.common.utils.CryptoUtil.encryptByDES(planId,null);
		//截取url链接
		URL = contextPath +"/"+URL.substring(0,URL.indexOf("?"));
		//拼接链接地址
		//URL = URL+"?recruit_id="+recruit_id+"&planId="+planId;
		
		}catch(Exception e){
			URL = contextPath+"/coframe/auth/login/login.jsp";
		}
	}else{
		URL = contextPath+"/coframe/auth/login/login.jsp";
	}
	
	//String IP= request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
   	//协议跳转
   	//URL = IP+contextPath+"/"+URL;
   /* 	response.setStatus(302);//向浏览器发送302状态码
	response.setHeader("location",URL);//发送响应头Location为所给路径 */
	//response.sendRedirect(URL);
	
 %>
<!-- 
  - Author(s): 唐能
  - Date: 2018-09-14 16:06:10
  - Description:
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>面试工作台</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body>
		<!-- 用form标签跳转 -->
		<form id="form" action="" method="post" style="display: none;">
			<input type="text" style="display:none;" name="recruit_id" id ="recruit_id" value="">
			<input type="text" style="display:none;" name="planId" id ="planId" value="">
			
		</form>
<script src="<%= request.getContextPath() %>/css/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
		$("#form").attr("action","<%=URL %>");
		$("#recruit_id").val("<%= recruit_id%>");
		$("#planId").val("<%= planId%>");
		$("#form").submit();
	</script>
</body>
</html>