<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-09-17 11:57:33
  - Description:
-->
<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
</head>
<body>

	
	<p style="font-weight: bold;">Dear：</p>
	<p>您好。您即将有一场<samp style="font-weight: bold;"> 面试名称 </samp> 面试安排，具体事宜如下</p> 
	<p>时间：<samp style="font-weight: bold;">时间</samp></p> 
	<p>地点：<samp style="font-weight: bold;">地点</samp></p> 
	
	 <br> 
	 <p>请提前20分钟到达面试现场，并携带好电脑或其他电子设备，通过登陆以下链接进入面试工作台开始评价。</p> 
	 <p>面试工作台：<samp style="font-weight: bold;">链接</samp></p> 
	 <p>页面最下方附候选人信息，请注意查收。</p> 
	 <br> 
	 <p>Best Regards & Thanks</p> 
	 <p>索菲亚人力资源部</p> 
	 <p>索菲亚家居股份有限公司（股票代码：002572）</p> 
	 <p>公司总部：广州市增城新塘镇宁西工业园</p> 
	 <p>营销中心：广州市天河区体育东路创展中心西座</p> 
	 <p>公司网址：<a href="http://www.sfygroup.com" target="_blank" style="color: blue;margin-right: 15px;">www.sfygroup.com</a> 
	   衣柜体验网： <a href="https://www.suofeiya.com" target="_blank" style="color: blue;">www.suofeiya.com.cn</a></p> 
	 <p><img src="http://hr-admin-test.sfygroup.com:9284/default/resume/索菲亚图标1530154211298.png"></p> 
	 <br> 
	 <p class="MsoNormal" align="left"><b>候选人信息：</b></p>
	 <table style="margin-left: 20px;border-collapse: collapse;font-size: 14px">
		<tbody><tr style="background: #9dc3e5">
			<td style="border:1px solid;padding: 8px;width: 100px" contenteditable="false">候选人姓名</td>
			<td style="border:1px solid;padding: 8px;width: 100px" contenteditable="false">学校</td>
			<td style="border:1px solid;padding: 8px;width: 150px" contenteditable="false">专业</td>
			<td style="border:1px solid;padding: 8px;width: 150px" contenteditable="false">笔试成绩</td>
			<td style="border:1px solid;padding: 8px;width: 150px" contenteditable="false">申请岗位</td>
		</tr>
		<tr>
			<td style="border:1px solid;padding: 8px">time</td>
			<td style="border:1px solid;padding: 8px">name</td>
			<td style="border:1px solid;padding: 8px">position</td>
			<td style="border:1px solid;padding: 8px">org</td>
			<td style="border:1px solid;padding: 8px" contenteditable="false">
				<a id="template" target="_blank" style="cursor: pointer;" href="url">请点击打开</a>
			</td>
		</tr>
	</tbody></table>
<p class="MsoNormal" align="left"><b><br></b></p>
<p class="MsoNormal" align="left"><b><br></b></p>


	<script type="text/javascript">
    </script>
</body>
</html>