<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): chenhaijiang
  - Date: 2018-09-12 15:53:05
  - Description:
-->
<head>
<title>淘汰页面</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <style type="text/css">
    #main{
    	padding:10px;
    }
    </style>
</head>
<body>
	<div id="main">
		<div class="">
			<span id="" style="color:#606060;">淘汰原因:</span>
		 <select  class="form-control" id="backOut" style="margin-left: 67px;width: 64%;display: inline-block;">
		 
	    </select>
		</div>
		
		<div style="margin: 10px 0px;">
			<span id="" style="float: left;color:#606060;">具体淘汰情况描述:</span>
			<textarea class="form-control out_interview_text" style="width: 64%;margin-left: 15px;float: left;" placeholder="" rows="5" style="margin-top:5px"></textarea>
		</div>
	</div>


	<script type="text/javascript">
	$(function(){
		$.ajax({
  			url:"com.recruit.campusProcessInfo.campusProcessInfo.queryCauseOfElimination.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{},
  			success:function(data){
  				$(data.cause_of_elimination).each(function(i,obj){
  						$("#backOut").append("<option value='"+obj.dictName+"'>"+obj.dictName+"</option>");
  					
  				})
  				
  			}
		});
	})
    </script>
</body>
</html>