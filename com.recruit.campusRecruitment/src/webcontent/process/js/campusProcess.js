		var jurisdiction = false; //默认当前登入人没有操作权限
		var processNum = ""; //整个流程环节所在人数数据集
		var checkdata = ""; //复选框的数据
		var currentStep = ""; //当前所在环节
		var phase_codeName ="";//获取环节名称
		var numCandidate =""; //应聘环节的总人数
		var result =""; //数据集
		var listHeight =0;
		var myindex =""//加载提示
		var layer ="";
		var clickdata ="";//单选教室或者面试时间的值
		var statusBool =true;//标识淘汰按钮样式
		var pageIndex = 1;//当前页数
    	var count =0;//总页数
    	var limit =50;//显示条数
		$(function(){
			layui.use(['layer', 'form'], function(){
				layer = layui.layer
				,form = layui.form;
			});
			queryCompany();//初始化期待公司数据
			queryData();//初始化所在环节人数和数据
			if(degreeType==2){//TTD项目
				$(".middle_written").hide();//隐藏笔试题框
				$(".middle").css("width","25%");//调整小框宽度
			}
			//设置标题值
			$("#topTitle").text(recruitPlanName);
//			$(".line-btn").hide();//隐藏按钮（转发面试官、推送、淘汰、发送邮件）
			$(".interview ").hide();//隐藏转派面试官
			$(".weed  ").hide();//隐藏淘汰
			$(".push").hide();//隐藏推送环节按钮
			$(".btns-span").hide();//显示按钮间隔span标签
			$(".btn-left").css("margin-right","0px");//按钮间隔span标签
			$("#message").hide();//隐藏淘汰按钮
			$(".accredit").css("margin-left","20px");
			
			
			//授权招聘负责人
			var array =recruiterId.split(",");
			for(var i=0;i<array.length;i++){
				if(array[i]==empCode||empCode==1){//判断当前登入员是否有操作权限
					jurisdiction =true;//才操作流程
				}
			}
			if(jurisdiction==false){//没有权限
				$(".line-btn").hide();//隐藏按钮（转发面试官、推送、淘汰、发送邮件）
			}
			
			//学生笔试标题事件
			$("#studentTest").click(function(){
				$(".headline").removeClass("vray-border");
				$(this).addClass("vray-border");//标题底线
				$(".middle").removeClass("middle_v2");//移除小框的样式
				$("#currentStep1").addClass("middle_v2");//默认第一个框
//				$(".line-btn").hide()//隐藏按钮列表
				$(".interview ").hide();//隐藏转派面试官
				$(".weed  ").hide();//隐藏淘汰
				$(".push").hide();//隐藏推送环节按钮
				$(".btns-span").hide();//显示按钮间隔span标签
				$(".btn-left").css("margin-right","0px");//按钮间隔span标签
				$("#message").hide();//隐藏淘汰按钮
				$(".accredit").css("margin-left","20px");
				currentStep = "";
				if(degreeType==2){//TTD项目
					$(".middle_written").hide();//隐藏笔试题框
					$(".middle").css("width","25%");//调整小框宽度
				}
				//替换文本
				$(".link1").text("应聘");
				$(".link2").text("性格测试");
				$(".link3").text("逻辑测试");
				$(".link4").text("笔试");
				$(".link5").text("面试邀约");
				//替换流程环节
				$("#currentStep1").attr("data","");//应聘没有环节
				$("#currentStep2").attr("data","1");
				$("#currentStep3").attr("data","2");
				$("#currentStep4").attr("data","3");
				$("#currentStep5").attr("data","4");
				queryData();//替换流程环节所在人数
					currentStep = $("#currentStep1").attr("data");//获取当前所在环节
				//释放按钮
					$(".diction").css("cursor","pointer");
		  			$(".diction").css("background-color","black");
		  			$(".interview").attr("onclick","forwardInterviewer()");//指派面试官
		  			$(".email").attr("onclick","sendEmail()");//发送通知
		  			$(".weed").attr("onclick","weedOut()");//淘汰
		  			$(".push").attr("onclick","pushProcess()");//推送
		  			$(".diction").attr("title","");
		  			//指派面试官和发送通知
		  			$(".interview,.email").css({
		  				"background":"white",
		  				"border":"1px solid #ecbf10",
		  				"color":"#ecbf10"
		  			});
		  			$(".push").css("background","#33AB9F").css("color","white");//推送
		  			$(".weed").css("background","#FF784E").css("color","white");//淘汰
		  		//清空搜索条件
					$(".import").each(function (index,obj){
						obj.value ="";
					})
			});
			
			//现场面试标题事件
			$("#localeInterview").click(function(){
				if(jurisdiction==true){
					$(".line-btn").show();//显示按按钮（发送通知、转发面试官、推送、淘汰）
					$(".interview ").show();//显示转派面试官
					$(".email  ").show();//显示发送通知
					$(".weed  ").show();//显示淘汰
					$(".push").show();//显示推送环节按钮
					$(".btns-span").show();//显示按钮间隔span标签
					$(".bot-content").css("height","828px");//主体宽度
					$("#message").show();//显示淘汰按钮
					$(".select").css("margin-left","0px");//显示搜索按钮
//					$(".btn-left").css("margin-right","23px");//按钮间隔span标签
					$(".email").css("margin-right","0px");
					$(".email").css("margin-left","0px");
					
				}
				$(".headline").removeClass("vray-border");
				$(this).addClass("vray-border");//标题底线
				$(".middle_written").show();//显示笔试题框
				$(".middle").css("width","20%");//调整小框宽度
				$(".middle").removeClass("middle_v2");//移除小框的样式
				$("#currentStep1").addClass("middle_v2");//默认第一个框
				//替换文本
				$(".link1").text("小组讨论");
				$(".link2").text("一对一面试");
				$(".link3").text("部门面试");
				$(".link4").text("评估中心");
				$(".link5").text("offer");
				//替换流程环节
				$("#currentStep1").attr("data","5");
				$("#currentStep2").attr("data","6");
				$("#currentStep3").attr("data","7");
				$("#currentStep4").attr("data","8");
				$("#currentStep5").attr("data","9");
					currentStep = $("#currentStep1").attr("data");//获取当前所在环节
					phase_codeName = $("#currentStep1").children().next().text();//获取环节名称
				//清空搜索条件
				$(".import").each(function (index,obj){
					obj.value ="";
				})
				queryData();//初始流程环节所在人数
			});
			
			//返回按钮事件
			$("#isback").click(function(){
				window.location.href =basePath +"campusRecruitment/recruitPlan/queryRecruitPlanManagement.jsp?curr="+curr;
			})
			
			//复选框绑定一个点击事件
			$(".checkBox").click(function(){
				//获取自定义属性值
				var check = $(this).attr("data-check");
				if(check=="check"){//查看不淘汰学生
					//移除class元素
					$(this).removeClass("background-img");
					//替换自定义属性值
					$(this).attr("data-check","false");
					statusBool =false;//不淘汰
				}else{//查看淘汰学生
					statusBool =true;//淘汰
					//添加class元素
					$(this).addClass("background-img");
					//替换自定义属性值
					$(this).attr("data-check","check");
				}
				hideProcessStatus();//隐藏或者显示学生
			})
			
			//追加导航内容
			$.ajax({
				url : basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryUserRecruitPlan.biz.ext",
				type : "POST",
				cache:false,
				async:false,
				success : function(data) {
					var htmlInput ="";
					for(var i=0;i<data.RecruitPlan.length;i++){
						if(data.RecruitPlan[i].recruitPlanId == recruitPlanId){
							htmlInput +='<input type="checkbox" lay-filter="plan" value="'+data.RecruitPlan[i].recruitPlanId+'"  name="planCheckbox" title="'+data.RecruitPlan[i].recruitPlanName+'" checked>';
						}else{
							htmlInput +='<input type="checkbox" lay-filter="plan" value="'+data.RecruitPlan[i].recruitPlanId+'"  name="planCheckbox" title="'+data.RecruitPlan[i].recruitPlanName+'">';
						}
					}
					$("#planName").append(htmlInput);
					layui.use('form', function(){
						  var form = layui.form;
						  form.on('checkbox(plan)', function(data){
							  var checkedValue="";
							  var recruitPlanName="";
							  $("input[name='planCheckbox']:checked").each(function(){
				                	checkedValue+=$(this).val()+",";
				                	recruitPlanName+=$(this).attr("title")+",";
							  })
				            	checkedValue = checkedValue.substring(0,checkedValue.length-1);
							  	recruitPlanName = recruitPlanName.substring(0,recruitPlanName.length-1);
							  	recruitPlanId = checkedValue; //赋值计划id--多个计划id
							  	jurisdiction =true;//给权限当前用户
							  	$(".line-btn").show();//显示按钮（转发面试官、推送、淘汰、发送邮件）
							  	queryData()//加载数据
							  	$("#topTitle").text(recruitPlanName);
							  	$("#topTitle").attr("title",recruitPlanName);
							});        
						}); 
				
				}
			})
			
			//计划名称移入事件
			$(document).ready(function(){
			  $(".left-navigation").mouseover(function(){
				  $(".left-content").animate({left:'0px'});//内容望右移200
				  $(".left-navigation").hide();//导航栏右移200
				  $("#right-content").addClass("right-content");
			  });
			});
			
			//左边导航内容移出的事件
			$(document).ready(function(){
				$("#right-content").mouseover(function(){
				   $(".left-content").animate({left:'-300px'});//内容望左移200
				   $(".left-navigation").show();//导航栏右移200
				   $("#right-content").removeClass("right-content");
				});
			});
		});
		
		//工作台按钮事件
		function iswork(){
			$("#workPage").attr("href",basePath+"campusRecruitment/process/workBench.jsp?planId="+woorkRecruit_id+"&recruit_id="+woorkEmpCode);
			document.getElementById("workPage").click();
		}
		
		//点击小方框事件
		function clickProcess(obj){
			myindex = layer.load('我正在加载数据哦', { time: 0 });//加载条
			currentStep = $(obj).attr("data");//获取当前所在环节
			phase_codeName = $(obj).children().next().text();//获取环节名称
			$(".middle").removeClass("middle_v2");//移除小框的样式
			$(obj).addClass("middle_v2");
			statusBool = true;//标识淘汰学生
			//添加class元素
			$(".checkBox").addClass("background-img");
			//替换自定义属性值
			$(".checkBox").attr("data-check","check");
				if(jurisdiction==true){//有权限
					if(currentStep>4){//第五以后的环节
						$(".line-btn").show();//显示按按钮（发送通知、转发面试官、推送、淘汰）
						$(".interview").show();//显示转派面试官
						$(".email").show();//显示发送通知
						$(".weed").show();//显示淘汰
						$(".push").show();//显示推送环节按钮
						$(".btns-span").show();//显示按钮间隔span标签
						$("#message").show();//显示淘汰按钮
						$(".select").show();//显示搜索按钮
					}else{//第4前环节
						$(".interview ").hide();//隐藏转派面试官
						$(".weed").hide();//隐藏淘汰
						$(".btns-span").hide();//隐藏按钮间隔span标签
						$(".push").hide();//隐藏推送环节按钮
						$(".line-btn").show();//显示按钮列表
						$(".email").show();//显示发送通知
						$(".select").show();//显示搜索按钮
						$(".select").css("margin-left","20px");//显示搜索按钮
						$("#message").hide();//隐藏淘汰按钮
						$(".btn-left").css("margin-right","0px");//按钮间隔span标签
						if(currentStep>=4){//面试邀约环节
							$(".push").show();//显示推送环节按钮
							$(".push").css("margin-left","0px");//显示推送环节按钮
							$(".btn-left").css("margin-right","23px");//按钮间隔span标签
						}
					}
			}else{
				$(".push").hide();//隐藏推送按钮
				$(".line-btn").hide();//隐藏按钮列表
			}
			if(currentStep==9){//offer环节
				$(".push").hide();//隐藏推送环节按钮
			}
			//清空搜索条件
			$(".import").each(function (index,obj){
				obj.value ="";
			})
			queryData();//加载数据
		}
		var layer ="";
		layui.use(['layer', 'form'], function(){
			 layer = layui.layer
			,form = layui.form;
		});
		
		//加载面试邀约前的环节数据数据 currentStep:当前环节，name：学生姓名，school:学生学校，major:专业，exceptedCompany:期望公司，processStatus：学生状态：（1、招聘2、淘汰）
		function getData(currentStep,name,school,major,exceptedCompany,processStatus){
			if(recruitPlanId==""){
				recruitPlanId = 0;
			}
			var json ={
					"recruit_plan_id":recruitPlanId, //招聘计划id
					"current_step":currentStep, //当前所在环节
					"studentName":name, //学生姓名
					"college":school, //学校
					"major":major, //专业
					"exceptedCompany":exceptedCompany, //期望公司
					"process_status":processStatus, //流程状态
					"pageIndex":pageIndex, //分页当前页
					"limit":limit //分页显示条数
					};
			$.ajax({
				url : basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryInformation.biz.ext",
				type : "POST",
				data:json,
				cache:false,
				async:false,
				success : function(data) {
					if(currentStep>=1){//环节大于1,性格及后面的环节数据集
						result = data.studentlist;//数据集
					}else{//应聘环节数据集
						result = data.applicant;//数据集
					}
					count =data.page.count ;//总记录数
					laypage(count);//赋值总记录数给分页条
					layui.use('table', function(){
						var table = layui.table ,form = layui.form;
						  //展示已知数据
							  table.render({
								   	 elem: '#idTest'
								    ,cols: [[ //标题栏
								       {type:'checkbox',width:'3%',align:'center'}
								      ,{field: 'candidateName', title: '姓名',width:'9.6%',unresize:false,templet:'#candidateName',event:'setSign'}
								      ,{field: 'college', title: '学校',width:'12.8%',unresize:false,templet:'#college',event:'setSign'}
								      ,{field: 'positionName', title: '申请职位',width:'14%',unresize:false,templet:'#positionName',event:'setSign'}
								      ,{field: 'major', title: '专业',width:'10.7%',unresize:false,templet:'#major',event:'setSign'}
								      ,{field: 'graduationDate', title: '毕业时间',width:'9%',unresize:false,templet:'#graduationDate',event:'setSign'}
								      ,{field: 'candidatePhone', title: '联系方式',width:'9.9%',unresize:false,event:'setSign'}
								      ,{field: 'characterType', title: '性格类型',width:'8%',unresize: false,event:'setSign'}
								      ,{field: 'testScores', title: '评测分数',width:'8%',unresize:false,templet:'#tempProcessId',event:'setSign',align:"center"}
								      ,{field: 'examNumber', title: '笔试次数',width:'7.8%',unresize:false,templet:'#',event:'setSign',align:"center"}
								      ,{field: 'rests_plan_name', title: '全部计划名称',width:'10%',unresize:false,templet:'#rests_plan_name',event:'setSign'}
								      ,{field: 'rests_current_step', title: '全部计划环节',width:'10%',unresize:false,templet:'#rests_current_step',event:'setSign'}
								      ,{field: 'recruitPlanName', title: '计划名称',width:'9.6%',unresize:false,templet:'#tempPlanName',event:'setSign'}
								      ,{field: 'resumeid', title: '简历Id',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'processId', title: '流程Id',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'candidateMail', title: '邮箱',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'processStatus', title: '流程状态',width:'',unresize:true,templet:'#processStatus',event:'setSign'}
								      ,{field: 'plan_degree_type', title: '学历类别',width:'',unresize:true,templet:'#',event:'setSign'}
								    ]]
								    ,id:"idTest"
								    ,data: result 
								    ,height:169
								    ,limit:result.length
								    ,done:function(res, curr, count) {
								    	layer.close(myindex);//加载条
								    	checkdata ="";//清除已选复选框值
								    	
								    	/* 页面已弄成分页，暂时注释掉自适应高度功能
								    	listHeight = result.length*39;//列表的数据*它的高度 =列表内容的总高度
										if(currentStep>=4){//环节大于4
											if(jurisdiction==true){//有权限者
												$(".bot-content").css("height",listHeight+205+61);//设置主要内容的div高度
											}else{
												$(".bot-content").css("height",listHeight+205);//设置主要内容的div高度
											}
										}else{
											$(".bot-content").css("height",listHeight+205);//设置主要内容的div高度
										}
										changeHeight();//绘制列表的高度
										*/
								    	if(result.length!=0){//数据集不为空
								    		listHeight = result.length*39;//数据的数量*它的高度 =列表内容的总高度
								    	}else{ //默认数据集为空的高度
								    		listHeight =110;
								    	}
								    	
								    	$(".bot-content").css("height",listHeight+205+50+10);//设置主要内容的div高度
											if(currentStep==4){//面试邀约环节，没有横轴滚动条
												if(jurisdiction==true){//有权限者
													$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
													$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
													$(".bot-content").css("height",listHeight+205+50+10+61);//设置主要内容的div高度
												}else{//没有权限，不用计算按钮列表的高度
													$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层 296：是固定死的高度
												}
											}else{//默认环节前四个的高度
												$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层
												$(".layui-table-body.layui-table-main").css("height",listHeight+20);//列表里面层
											}
								    	
								    	$(".layui-table-view .layui-table").css("width","100%");
								    	//隐藏列
								    	$(".layui-table-box").find("[data-field='resumeid']").css("display","none"); //简历ID   
								    	$(".layui-table-box").find("[data-field='processId']").css("display","none"); //流程ID
								    	$(".layui-table-box").find("[data-field='candidateMail']").css("display","none"); //邮件
								    	$(".layui-table-box").find("[data-field='processStatus']").css("display","none"); //流程状态
								    	$(".layui-table-box").find("[data-field='plan_degree_type']").css("display","none"); //学历类别
								    	if(currentStep !=""){//不是签到环节显示     全部计划名称、全部环节名称字段
								    		$(".layui-table-box").find("[data-field='rests_plan_name']").css("display","none"); //全部计划名称
								    		$(".layui-table-box").find("[data-field='rests_current_step']").css("display","none"); //全部环节名称
								    	}else{//签到环节
								    		$(".layui-table-box").find("[data-field='duration']").css("display","none"); //隐藏总时长
								    		$(".layui-table-box").find("[data-field='characterType']").css("display","none"); //隐藏性格类型
								    		$(".layui-table-box").find("[data-field='testScores']").css("display","none"); //隐藏评测分数
								    		$(".layui-table-box").find("[data-field='examNumber']").css("display","none"); //隐藏笔试次数
								    	}
									} 
						  		 });
						  	
							 //复选框事件
							 table.on('checkbox', function(obj){
								  var checkdatas = table.checkStatus('idTest');
								  	  checkdata =checkdatas;//勾选复选框的值
								  var bool =true;//标识是否有淘汰者;
								  if(checkdata.data.length==0){//未选择复选框
									  	$(".diction").css("cursor","pointer");
									  	$(".interview").attr("onclick","forwardInterviewer()");//指派面试官
									  	$(".email").attr("onclick","sendEmail()");//发送通知
									  	$(".weed").attr("onclick","weedOut()");//淘汰
									  	$(".push").attr("onclick","pushProcess()");//推送
							  			$(".diction").attr("title","");
							  			//指派面试官和发送通知
							  			$(".interview,.email").css({
							  				"background":"white",
							  				"border":"1px solid #ecbf10",
							  				"color":"#ecbf10"
							  			});
							  			$(".push").css("background","#33AB9F").css("color","white");//推送
							  			$(".weed").css("background","#FF784E").css("color","white");//淘汰
								  }
							 });
					})
				}
			})
		}
		
		//加载列表的单选选中复选框
		 $(document).on("click",".layui-table-body table.layui-table tbody tr",function(){
				var obj = event ? event.target : event.srcElement;
				var tag = obj.tagName;
				var checkbox = $(this).find("td div.laytable-cell-checkbox div.layui-form-checkbox I");
				if(checkbox.length!=0){
					if(tag == 'DIV') {
						checkbox.click();
			        }
				}
				
			});
		$(document).on("click","td div.laytable-cell-checkbox div.layui-form-checkbox",function(e){
			e.stopPropagation();
		});
		
		
		//加载小组讨论及后面环节的数据 currentStep:当前环节，name：学生姓名，school:学生学校，major:专业，exceptedCompany:期望公司，processStatus：学生状态：（1、招聘2、淘汰）
		function getData2(currentStep,name,school,major,exceptedCompany,processStatus){
			if(recruitPlanId==""){
				recruitPlanId = 0;
			}
			var json ={
					"recruit_plan_id":recruitPlanId, //招聘计划id
					"current_step":currentStep, //当前所在环节
					"studentName":name, //学生姓名
					"college":school, //学校
					"major":major, //专业
					"exceptedCompany":exceptedCompany, //期望公司
					"process_status":processStatus, //流程状态
					"pageIndex":pageIndex, //分页当前页
					"limit":limit //分页显示条数
					};
			$.ajax({
				url : basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryInformation.biz.ext",
				type : "POST",
				data:json,
				cache:false,//不显示关闭图标
				async:false,
				success : function(data) {
					result = data.studentlist;//数据集
					count =data.page.count ;//总记录数
					laypage(count);//赋值总记录数给分页条
					layui.use('table', function(){
						
						var table = layui.table ,form = layui.form;
						  //展示已知数据
							  table.render({
								   	 elem: '#idTest'
								    ,cols: [[ //标题栏
								       {type:'checkbox',width:'3%',align:'center'}
								      ,{field: 'candidateName', title: '姓名',width:'9.6%',unresize:false,templet:'#candidateName',event:'setSign'}
								      ,{field: 'college', title: '学校',width:'12.8%',unresize:false,templet:'#college',event:'setSign'}
								      ,{field: 'candidatePhone', title: '联系方式',width:'9.9%',unresize:false,event:'setSign'}
								      ,{field: 'isCall', title: '通知状态',width:'7.8%',unresize:false,templet:'#isCall',event:'setSign',align:'left'}
								      ,{field: 'interviewTime', title: '面试时间',width:'12%',unresize:false,templet:'#interviewTime',event:'editor',align:'left'}
								      ,{field: 'classroom', title: '教室',width:'9%',unresize:false,templet:'#classroom',event:'editor'}
								      ,{field: 'recruiterId', title: '面试官Id',width:'',unresize:false,templet:'#',event:'setSign'}
								      ,{field: 'recruiterName', title: '面试官姓名',width:'22.5%',unresize:false,templet:'#recruiterName'}
								      ,{field: 'resumeid', title: '简历Id',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'processId', title: '流程Id',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'candidateMail', title: '邮箱',width:'',unresize:true,templet:'#',event:'setSign'}
								      ,{field: 'processStatus', title: '流程状态',width:'',unresize:true,templet:'#processStatus',event:'setSign'}
								      ,{field: 'exceptedCompany', title: '期望公司',width:'17%',unresize:false,templet:'#exceptedCompany',event:'setSign'}
								      ,{field: 'rests_plan_name', title: '全部计划名称',width:'10%',unresize:false,templet:'#rests_plan_name',event:'setSign'}
								      ,{field: 'rests_current_step', title: '全部计划环节',width:'10%',unresize:false,templet:'#rests_current_step',event:'setSign'}
								      ,{field: 'recruitPlanName', title: '计划名称',width:'9.6%',unresize:false,templet:'#tempPlanName',event:'setSign'}
								      ,{field: 'detailInterviewAdvice', title: '面试评价',width:'7.4%',unresize:true,templet:'#detailInterviewAdvice',event:'setSign',align:'left',fixed:'right'}
								      ,{field: 'detailTotalScores', title: '分数',width:'7%',unresize:true,templet:'#detailTotalScores',event:'setSign',fixed:'right'}
								      ,{field: 'operation', title: '简历',width:'5%',unresize:true,templet:'#operation',event:'setSign',fixed:'right',align:'center'}
								      ,{field: 'plan_degree_type', title: '学历类别',width:'5%',unresize:true,templet:'#',event:'setSign',fixed:'right',align:'center'}
								    ]]
								    ,id:"idTest"
								    ,data: result 
								    ,height:169
								    ,limit:result.length
								    ,done:function(res, curr, count) {
								    	layer.close(myindex);//加载条
								    	checkdata ="";//清除已选复选框值
								    	$("th").each(function(index,obj){
								    		//面试官姓名渲染背景图片
								    		if($(obj).attr("data-field")=="recruiterName"){
								    			$(obj).css({
								    			"background":"url("+basePath+"campusRecruitment/process/img/update-icon.png) no-repeat",
								    			"background-position":"86px"
								    			});
								    		//面试时间渲染背景图片
								    		}else if($(obj).attr("data-field")=="interviewTime"){
								    			$(obj).css({
								    				"background":"url("+basePath+"campusRecruitment/process/img/update-icon.png) no-repeat",
								    				"background-position":"73px"
								    			});
								    		//面试教室渲染背景图片
								    		}else if($(obj).attr("data-field")=="classroom"){
								    			$(obj).css({
								    				"background":"url("+basePath+"campusRecruitment/process/img/update-icon.png) no-repeat",
								    				"background-position":"45px"
								    			});
								    			
								    		}
								    	})
								    	/* 页面已弄成分页，暂时注释掉自适应高度功能
								    	listHeight = result.length*39;//列表的数据*它的高度 =列表内容的总高度
										if(currentStep>=4){//环节大于4
											if(jurisdiction==true){//有权限者
												$(".bot-content").css("height",listHeight+205+61);//设置主要内容的div高度
											}else{
												$(".bot-content").css("height",listHeight+205);//设置主要内容的div高度
											}
										}else{
											$(".bot-content").css("height",listHeight+205);//设置主要内容的div高度
										}
										changeHeight();//绘制列表的高度
										*/
								    	if(result.length!=0){//数据集不为空
								    		listHeight = result.length*39;//数据的数量*它的高度 =列表内容的总高度
								    	}else{ //默认数据集为空的高度
								    		listHeight =110;
								    	}
											if(jurisdiction==true){//有权限者
												$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+61);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
												$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
												$(".bot-content").css("height",listHeight+205+18+61+10+50);//主要内容的div
											}else{//没有权限，不用计算按钮列表的高度
												$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38+20);//列表外面层
												$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
												$(".bot-content").css("height",listHeight+205+18+10+50);//主要内容的div
											}
								    	
								    	//状态为4的学生（淘汰）加背景颜色标记
								    	$(".weedBck").parents("tr").css("background-color","rgba(158, 237, 237, 0.37)")
								    	$(".weedOut").parents("tr").css("background-color","rgba(158, 237, 237, 0.37)")
								    	$(".layui-table-view .layui-table").css("width","100%");
								    	//隐藏列
								    	$(".layui-table-box").find("[data-field='resumeid']").css("display","none"); //简历ID   
								    	$(".layui-table-box").find("[data-field='processId']").css("display","none"); //流程ID
								    	$(".layui-table-box").find("[data-field='candidateMail']").css("display","none"); //邮件
								    	$(".layui-table-box").find("[data-field='processStatus']").css("display","none"); //流程状态
								    	$(".layui-table-box").find("[data-field='recruiterId']").css("display","none"); //面试官ID
								    	$(".layui-table-box").find("[data-field='plan_degree_type']").css("display","none"); //面试官ID
									} 
						  		 });
							  
							//单击数据表格单击事件
						  	table.on('tool(test)', function(obj){
					  		 	 if(obj.event === 'editor'){
								     clickdata = obj.data;//获取整行值转给全局变量
								     if(clickdata.recruiterId!=""&&clickdata.recruiterId!=null){
								     layui.use('layer', function(){	
											var layer = layui.layer;
											layer.open({
												area: ['426px', '430px'],
												type: 2,
												shade: 0.5,
												resize:false, //是否允许拉伸
												closeBtn: false,//关闭窗口按钮
												title:'修改面试时间与地点',
												closeBtn: false,//不显示关闭图标
											    btn: ['确定', '关闭'],
												content: [basePath+'campusRecruitment/process/updateInterviewerAndClass.jsp','no'],
												yes: function(index,lay){
													var body = layer.getChildFrame('body', index);
												  	var classroom=body.find('#classroom').val(); //地点
												  	var interview_time=body.find('#interview_time').val(); //面试时间
												  	var email=body.find('#email').attr("data-check"); //是否发送邮件
												  	var sms=body.find('#sms').attr("data-check"); //是否发送短信
												  	var mailTemplateId =0;//是否发送邮件
												  	var is_send_SMS =0;//是否发送短信
												  	//指派面试官邮件标题（计划名称+当前环节名称）
												  	var mailTitle = recruitPlanName+"("+phase_codeName+")";
												  	if(email=="check"){//发送邮件
												  		mailTemplateId =1;//是否发送邮件
												  	}
												  	if(sms=="check"){//发送短信
												  		is_send_SMS =1;
												  	}
												    if(interview_time==""){
												  		layer.msg("请选择面试时间");
												  		return;
												  	}else if(classroom==""){
												  		layer.msg("请选择面试地点");
												  		return;
												  	}
													layer.open({
												        type: 1
												        ,offset:'auto' 
												        ,id: 'layerDemo'
												        ,content: '<div style="padding: 20px 75px;">是否确定修改？</div>'
												        ,closeBtn: false
												        ,btn:['确定','关闭']
												        ,btnAlign: 'c' //按钮居中
												        ,shade: 0 //不显示遮罩
													    ,resize:false //是否允许拉伸
												        ,closeBtn: false//不显示关闭图标
												        ,yes: function(){
														  	$.ajax({
													  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.updateTimeANDPlace.biz.ext",
													  			type:"POST",
													  			datatype: "json",
													  			data:{
													  				recruit_id:clickdata.resumeid,//简历id
													  				recruit_plan_id:clickdata.recruitPlanId,//招聘计划id
													  				process_id:clickdata.processId,//流程id
													  				phase_code:currentStep,//流程环节
													  				phase_codeName:phase_codeName,//环节名称
													  				recruiter_id:clickdata.recruiterId,//招聘人id
													  				recruiter_name:clickdata.recruiterName,//招聘人姓名
													  				classroom:classroom,//分配教室
													  				interviewTime:interview_time,//面试时间
													  				is_Email:mailTemplateId,//是否发送邮件
													  				is_send_SMS:is_send_SMS,//是否发送短信
													  				mailTitle:mailTitle,//邮件标题
													  				studentName:clickdata.candidateName//学生姓名
													  			},
													  			success:function(data){
													  				if(data.msg_type>0){
													  					queryData();//初始流程环节所在人数,加载数据
													  					layer.closeAll();//关闭页面
													  					layer.msg('修改成功');
													  				}else{
													  					layer.closeAll();//关闭页面
													  					layer.msg('修改失败');
													  				}
													  			}
														  	});
														}
													 	,btn2: function(index){
												          layer.close(index);//关闭页面
												        }
													});
										        }
										        ,btn2: function(){
										          layer.closeAll();//关闭页面
										        }
										    });
								     });
								   }else{
									   layer.msg('没有面试官不可编辑教室和时间！');
								   }
					  		 	 }
					  		 	 if(obj.event === 'setSign'){
//					  		 		 alert("单击行");//查看学生简历
					  		 	 }
					  		 	 
							})
							
							//复选框的事件
							 table.on('checkbox', function(obj){
								  var checkdatas = table.checkStatus('idTest');
								  	  checkdata =checkdatas;//勾选复选框的值
								  var bool =true;//标识是否有淘汰者;
								  if(checkdata.data.length==0){//未选择复选框
									  	$(".diction").css("cursor","pointer");
									  	$(".interview").attr("onclick","forwardInterviewer()");//指派面试官
									  	$(".email").attr("onclick","sendEmail()");//发送通知
									  	$(".weed").attr("onclick","weedOut()");//淘汰
									  	$(".push").attr("onclick","pushProcess()");//推送
							  			$(".diction").attr("title","");
							  			//指派面试官和发送通知
							  			$(".interview,.email").css({
							  				"background":"white",
							  				"border":"1px solid #ecbf10",
							  				"color":"#ecbf10"
							  			});
							  			$(".push").css("background","#33AB9F").css("color","white");//推送
							  			$(".weed").css("background","#FF784E").css("color","white");//淘汰
								  }
								  	for(var i=0;i<checkdata.data.length;i++){
								  		if(checkdata.data[i].processStatus==4){//状态为淘汰
								  			$(".diction").css({
								  				"cursor":"not-allowed",
								  				"background-color":"#ccc",
								  				"color":"white",
								  				"border":"1px solid white"
								  				});
								  			$(".diction").removeAttr("onclick");
								  			$(".diction").attr("title","淘汰学生不可操作");
								  			bool =false;
								  		}else{//可操作
								  			$(".diction").css("cursor","pointer");
								  			$(".diction").css("background-color","black");
								  			$(".interview").attr("onclick","forwardInterviewer()");//指派面试官
								  			$(".email").attr("onclick","sendEmail()");//发送通知
								  			$(".weed").attr("onclick","weedOut()");//淘汰
								  			$(".push").attr("onclick","pushProcess()");//推送
								  			$(".diction").attr("title","");
								  			//指派面试官和发送通知
								  			$(".interview,.email").css({
								  				"background":"white",
								  				"border":"1px solid #ecbf10",
								  				"color":"#ecbf10"
								  			});
								  			$(".push").css("background","#33AB9F").css("color","white");//推送
								  			$(".weed").css("background","#FF784E").css("color","white");//淘汰
								  		}
								  	}
							 });
					})
				}
			})
		}
		
		
		//推送下一个环节
		function pushProcess(){
			if(checkedBool()){
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,content: '<div style="padding: 20px 75px;">是否将学生推到下一个环节 ？</div>'
			        ,closeBtn: false
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
				    ,resize:false //是否允许拉伸
			        ,closeBtn: false//不显示关闭图标
			        ,yes: function(){
			        	var resume_id =""; //简历ID 
					  	var process_id =""; //流程ID 
					  	var examNumber =""; //考试次数
					  	var planId =""; //计划id
					  	for(var i=0;i<checkdata.data.length;i++){
					  		if(checkdata.data.length-1!=i){
					  			resume_id += checkdata.data[i].resumeid + ","; //简历id
					  			process_id += checkdata.data[i].processId + ","; //流程id
					  			examNumber += checkdata.data[i].examNumber + ","; //考试次数
					  			planId += checkdata.data[i].recruitPlanId + ","; //计划id
					  		}else{
					  			resume_id += checkdata.data[i].resumeid;//简历id
					  			process_id += checkdata.data[i].processId;//流程id
					  			examNumber += checkdata.data[i].examNumber;//考试次数
					  			planId += checkdata.data[i].recruitPlanId;//计划id
					  		}
					  	}
						$.ajax({
				  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.updateCurrentStep.biz.ext",
				  			type:"POST",
				  			datatype: "json",
				  			data:{
				  				resume_id:resume_id,//简历id
				  				recruit_plan_id:planId,//招聘计划id
				  				process_id:process_id, //流程ID
				  				current_step:currentStep,//流程环节
				  				examNumber:examNumber//考试次数
				  			},
				  			success:function(data){
				  				if(data.success!="-1"){
				  						queryData();//重新加载数据
				  						layer.closeAll();//关闭页面
				  						layer.msg('推送成功');
				  				}else{
				  					layer.closeAll();//关闭页面
				  					layer.msg('推送失败');
				  				}
				  			}
					  	});
			        }
			        ,btn2: function(){
			          layer.closeAll();//关闭页面
			        }
			  });
			}else{
				layer.msg("请选择学生");
			}
		}
		
		//指派面试官
		function forwardInterviewer(){
			var boolBtn =false; //防止用户触发两次按钮
			if(checkedBool()){
			layui.use('layer', function(){	
				var layer = layui.layer;
				layer.open({
					area: ['426px', '430px'],
					type: 2,
					shade: 0.5,
					resize:false, //是否允许拉伸
					closeBtn: false,//关闭窗口按钮
					title:'转发面试官',
					closeBtn: false,//不显示关闭图标
				    btn: ['确定', '关闭'],
					content: [basePath+'campusRecruitment/process/interviewer.jsp','no'],
					yes: function(index,lay){
						if(boolBtn==false){//防止用户提交多次
							boolBtn =true;
						var body = layer.getChildFrame('body', index);
						var recruiter_id=body.find('#interviewer').attr("data"); //面试官id
					  	var recruiter_name=body.find('#interviewer').val(); //面试官姓名
					  	var classroom=body.find('#classroom').val(); //地点
					  	var interview_time=body.find('#interview_time').val(); //面试时间
					  	var email=body.find('#email').attr("data-check"); //是否发送邮件
					  	var sms=body.find('#sms').attr("data-check"); //是否发送短信
					  	var is_Email =0;//是否发送邮件
					  	var is_send_SMS =0;//是否发送短信
					  	if(email=="check"){//发送邮件
					  		is_Email =1;//是否发送邮件
					  	}
					  	if(sms=="check"){//发送短信
					  		is_send_SMS =1;
					  	}
					  	var resume_id =""; //简历ID 
					  	var process_id =""; //流程ID 
					  	var candName ="";//学生姓名
					  	var planId ="";//计划id
					  	if(recruiter_name==""){
					  		layer.msg("请选择面试官");
					  		return;
					  	}else if(interview_time==""){
					  		layer.msg("请选择面试时间");
					  		return;
					  	}else if(classroom==""){
					  		layer.msg("请选择面试地点");
					  		return;
					  	}
					  	for(var i=0;i<checkdata.data.length;i++){
					  		if(checkdata.data.length-1!=i){
					  			resume_id += checkdata.data[i].resumeid + ","; //简历id
					  			process_id += checkdata.data[i].processId + ","; //流程id
					  			candName += checkdata.data[i].candidateName+",";//学生姓名
					  			planId += checkdata.data[i].recruitPlanId+",";//计划id
					  		}else{
					  			resume_id += checkdata.data[i].resumeid;//简历id
					  			process_id += checkdata.data[i].processId;//流程id
					  			candName += checkdata.data[i].candidateName;//学生姓名
					  			planId += checkdata.data[i].recruitPlanId;//计划id
					  		}
					  	}
					  	//指派面试官邮件标题（计划名称+当前环节名称）
					  	var mailTitle = recruitPlanName+"("+phase_codeName+")";
						  	$.ajax({
					  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.updateRecruiterName.biz.ext",
					  			type:"POST",
					  			datatype: "json",
					  			data:{
					  				resume_id:resume_id,//简历id
					  				recruit_plan_id:planId,//招聘计划id
					  				process_id:process_id, //流程ID
					  				phase_code:currentStep,//流程环节
					  				recruiter_id:recruiter_id,//面试官Id
					  				recruiter_name:recruiter_name,  //面试官姓名
					  				classroom:classroom,//分配教室
					  				interview_time:interview_time,//面试时间
					  				phase_codeName:phase_codeName,//环节名称
					  				mailTitle:mailTitle,//邮件标题
					  				is_Email:is_Email,//是否发送邮件
					  				is_send_SMS:is_send_SMS,//是否发送短信
					  				candName:candName//学生姓名
					  			},
					  			success:function(data){
					  				if(data.success!="-1"){
					  					queryData();//重新加载数据
					  					layer.closeAll();//关闭页面
					  					layer.msg('转发成功');
					  				}else{
					  					layer.closeAll();//关闭页面
					  					layer.msg('转发失败');
					  				}
					  			}
						  	});
						}
					}
			        ,btn2: function(){
			          layer.closeAll();//关闭页面
			          boolBtn =true;
			        }
			    });
			})
			}else{
				layer.msg("请选择学生");
			}
			
		}
		
		//发送邮件
		function sendEmail(){
			if(checkedBool()){
				var candidateMail ="";//邮箱
				var candidatePhone ="";//电话
				var candidateName ="";//名称
				var positionName ="";//申请职位
				var emailType  =0;//模板类型编号
				var resumeid  ="";//简历id
				var plan_degree_type  ="";//学历类别
				var planId  ="";//计划id
				for(var i=0; i<checkdata.data.length;i++){
					if(checkdata.data.length -1!=i){
						candidateMail +=checkdata.data[i].candidateName+"<"+checkdata.data[i].candidateMail+">,";//拼接邮箱
						candidatePhone += checkdata.data[i].candidatePhone + ","; //电话
						candidateName += checkdata.data[i].candidateName + ","; //名称
						resumeid += checkdata.data[i].resumeid + ","; //简历id
						planId += checkdata.data[i].recruitPlanId+",";//计划id
						plan_degree_type += checkdata.data[i].plan_degree_type + ","; //学历类别
						if(checkdata.data[i].positionName==""){//申请职位
							positionName +=null+",";//申请职位
						}else{
							positionName += checkdata.data[i].positionName + ","; //申请职位
						}
					}else{
						candidateMail +=checkdata.data[i].candidateName+"<"+checkdata.data[i].candidateMail+">";//拼接邮箱
						candidatePhone +=checkdata.data[i].candidatePhone;//拼接电话
						candidateName +=checkdata.data[i].candidateName;//名称
						resumeid +=checkdata.data[i].resumeid;//简历id
						plan_degree_type +=checkdata.data[i].plan_degree_type;//学历类别
						planId += checkdata.data[i].recruitPlanId;//计划id
						if(checkdata.data[i].positionName==""){//申请职位
							positionName +=null;//申请职位
						}else{
							positionName +=checkdata.data[i].positionName;//申请职位
						}
					}
				}
				if(currentStep==5||currentStep==6){
					emailType =1;
				}else if(currentStep==7){
					emailType =2;
				}else if(currentStep==8){
					emailType =3;
				}
//				if(currentStep==""||currentStep==null){//签到环节
//					positionName =null;
//				}
				console.log(positionName);
				if(currentStep=='9'){
					parent.openEmail(candidateMail,candidateName,positionName,candidatePhone,emailType,planId,"1",resumeid,plan_degree_type);//发送邮件，调用父层
				}else{
					parent.openEmail(candidateMail,candidateName,positionName,candidatePhone,emailType,planId,"true",resumeid,plan_degree_type);//发送邮件，调用父层
				}
			}else{
				layer.msg("请选择学生");
			}
		}
		
		//修改通知状态,interviewTime:面试时间，classroom：面试教室
		function updateInform(interviewTime,classroom){
			if(currentStep>=4){//前四个环节不改通知状态
				var resume_id =""; //简历ID 
			  	var process_id =""; //流程ID
				for(var i=0; i<checkdata.data.length;i++){
					if(checkdata.data.length -1!=i){
						resume_id += checkdata.data[i].resumeid + ","; //简历id
						process_id += checkdata.data[i].processId + ","; //流程id
					}else{
						resume_id += checkdata.data[i].resumeid;//简历id
						process_id += checkdata.data[i].processId;//流程id
					}
				}
				$.ajax({
		  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.updateIsNotice.biz.ext",
		  			type:"POST",
		  			datatype: "json",
		  			data:{
		  				resume_id:resume_id,//简历id
		  				recruit_plan_id:recruitPlanId,//招聘计划id
		  				process_id:process_id, //流程ID
		  				phase_code:currentStep,//流程环节
		  				classroom:classroom,//面试教室
		  				interviewTime:interviewTime//面试时间
		  			},
		  			success:function(data){
		  					queryData();//重新加载数据
		  			}
			  	});
			}
		}
		
		
		//是否已勾选复选框，true：已选择，false：未选择
		function checkedBool(){
			var checkBool = false;
			if(checkdata ==""){
				checkBool =false;
			}else if(checkdata.data.length==0){
				checkBool =false;
			}else {
				checkBool =true;
			}
			return checkBool;
		}
		
		
		//淘汰事件
		function weedOut(){
			if(checkedBool()){
				layui.use('layer', function(){	
					var layer = layui.layer;
					layer.open({
						area: ['426px', '330px'],
						type: 2,
						shade: 0.5,
						resize:false, //是否允许拉伸
						closeBtn: false,//关闭窗口按钮
						title:'淘汰学生',
						closeBtn: false,//不显示关闭图标
					    btn: ['确定', '关闭'],
						content: [basePath+'campusRecruitment/process/weedOut.jsp','no'],
						yes: function(index,lay){
							var body = layer.getChildFrame('body', index);
							var Reason=body.find('#backOut').val(); //淘汰类型
						  	var Details=body.find('.out_interview_text').val(); //淘汰情况描述
						  	var resume_id =""; //简历ID 
						  	var process_id =""; //流程ID 
						  	for(var i=0;i<checkdata.data.length;i++){
						  		if(checkdata.data.length-1!=i){
						  			resume_id += checkdata.data[i].resumeid + ","; //简历id
						  			process_id += checkdata.data[i].processId + ","; //流程id
						  		}else{
						  			resume_id += checkdata.data[i].resumeid;//简历id
						  			process_id += checkdata.data[i].processId;//流程id
						  		}
						  	}
						  	$.ajax({
					  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.updateProcessStatus.biz.ext",
					  			type:"POST",
					  			datatype: "json",
					  			data:{
					  				resume_id:resume_id,//简历id
					  				recruit_plan_id:recruitPlanId,//招聘计划id
					  				process_id:process_id, //流程ID
					  				current_step:currentStep,//流程环节
					  				reason:Details,//淘汰原因
					  				cause:Reason  //淘汰类别
					  			},
					  			success:function(data){
					  				if(data.success!="-1"){
					  					layer.msg('淘汰成功');
					  					queryData();//重新加载数据
					  				}else{
					  					layer.msg('淘汰失败');
					  				}
					  			}
						  	});
						  	layer.closeAll();//关闭页面
				        }
				        ,btn2: function(){
				          layer.closeAll();//关闭页面
				        }
				    });
				})
			}else{
				layer.msg("请选择学生");
			}
		}
		
		
		//查询流程环节所在人数和列表--location：区分标识环节，name：学生姓名，school:学校，major：专业，exceptedCompany：期待公司
		function processCount(location,name,school,major,exceptedCompany){
			if(statusBool==true){
				var	processStatus =1;//查询不淘汰学生
			}else{
				var	processStatus ="";//查询淘汰和在招学生
			}
			$.ajax({
				url : basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryStudentListNum.biz.ext",
				type : "POST",
				data:{"recruit_plan_id":recruitPlanId},
				cache:false,//不显示关闭图标
				async:true,
				success : function(data) {
						numCandidate =data.numCandidate;//应聘环节的总人数
					if(data.processInfo.length!=0){
						processNum =data.processInfo[0];//其他各个环节的总人数
					}else{//没有总人数
						processNum.numCharacter =null;
						processNum.numLogic =null;
						processNum.numWrittenTest =null;
						processNum.numInvite =null;
					}
					if(location==1){//刷新前4个环节
						$(".middle_num1").text(numCandidate);//赋值应聘环节所在人数
						$(".middle_num2").text(processNum.numCharacter==null?0:processNum.numCharacter);//赋值性格环节所在人数
						$(".middle_num3").text(processNum.numLogic==null?0:processNum.numLogic);//赋值逻辑环节所在人数
						$(".middle_num4").text(processNum.numWrittenTest==null?0:processNum.numWrittenTest);//赋值笔试环节所在人数
						$(".middle_num5").text(processNum.numInvite==null?0:processNum.numInvite);//赋值面试邀约环节所在人数
							//加载学生笔试的数据
							getData(currentStep,name,school,major,exceptedCompany,processStatus);//加载数据
					}else{//刷新后4个环节的人数
						$(".middle_num1").text(processNum.numGroup==null?0:processNum.numGroup);//赋值小组讨论环节所在人数
						$(".middle_num2").text(processNum.numOneToOne==null?0:processNum.numOneToOne);//赋值一对一环节所在人数
						$(".middle_num3").text(processNum.numOrg==null?0:processNum.numOrg);//赋值部门环节所在人数
						$(".middle_num4").text(processNum.numEvaluation==null?0:processNum.numEvaluation);//赋值评估中心所在人数
						$(".middle_num5").text(processNum.numOffer==null?0:processNum.numOffer);//赋值offer环节所在人数
							//加载现场面试的数据
							getData2(currentStep,name,school,major,exceptedCompany,processStatus);//加载数据
					}
					
				}
			});
		}
		
		//淘汰者添加图标
		function ConvetName(name,processStatus){
			var html ="";
			if(processStatus==4){
				html ='<div class="title" title="'+name+'">'+name+'<span class="layui-badge emliminate" >淘汰</span></div>';
			}else{
				html ='<div class="title" title="'+name+'">'+name+'</div>';
			}
			return html;
			
		}
		
		//替换图标,str：面试官反馈，InterviewOrg
		function replaceIcon(str,InterviewOrg){
			if(str!=null){
			var array = str.split(",");
			var Interview = InterviewOrg.split(",");//面试官的工号
			var html ="";
			for(var i=0;i<array.length;i++){
				if(array[i]==1){//通过
//					html += '<a href ="'+basePath+'campusRecruitment/process/workBench.jsp?planId='+recruitPlanId+'&recruit_id='+Interview[i]+'" target="_blank" ><span class="layui-badge layui-bg-green title" title="推荐">荐</span></a>';
					html += '<span class="layui-badge layui-bg-green title" title="推荐">荐</span>';
				}else if(array[i]==2){//待定
//					html += '<a href ="'+basePath+'campusRecruitment/process/workBench.jsp?planId='+recruitPlanId+'&recruit_id='+Interview[i]+'" target="_blank" ><span class="layui-badge layui-bg-blue title" title="待定">待</span></a>';
					html += '<span class="layui-badge layui-bg-blue title" title="待定">待</span>';
				}else if(array[i]==3){//不推荐
//					html += '<a href ="'+basePath+'campusRecruitment/process/workBench.jsp?planId='+recruitPlanId+'&recruit_id='+Interview[i]+'" target="_blank" ><span class="layui-badge title" title="放弃">汰</span></a>';
					html += '<span class="layui-badge title" title="放弃">汰</span>';
				}else if(array[i]==4){//未到场
//					html += '<a href ="'+basePath+'campusRecruitment/process/workBench.jsp?planId='+recruitPlanId+'&recruit_id='+Interview[i]+'" target="_blank" ><span class="layui-badge layui-bg-black title" title="未到场">未</span></a>';
					html += '<span class="layui-badge layui-bg-black title" title="未到场">未</span>';
				}
			}
				return html;
			}else{
				return '';
			}
		}
		
		//根据列表的内容去定义整体的高度（提升体验感） -->页面已经弄成分页，暂时没有调到此函数
		function changeHeight(){
				//监听浏览器有没有滚动条
				if(document.body.scrollHeight > (window.innerHeight || document.documentElement.clientHeight)==false){
					if(currentStep>=4){//第5、6、7、8、9环节进来
						if(currentStep==4){//面试邀约环节，没有横轴滚动条
							if(jurisdiction==true){//有权限者
								$(".layui-form.layui-border-box.layui-table-view").css("height",296-61);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
								$(".layui-table-body.layui-table-main").css("height",296+18-61);//列表里面层（横轴滚动条）
							}else{//没有权限，不用计算按钮列表的高度
								$(".layui-form.layui-border-box.layui-table-view").css("height",296);//列表外面层 296：是固定死的高度
								$(".layui-table-body.layui-table-main").css("height",296+18-61);//列表里面层（横轴滚动条）
							}
						}else{//第五环节及后面，带有横轴滚动条
							if(jurisdiction==true){//有权限者
								$(".layui-form.layui-border-box.layui-table-view").css("height",296-61);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
								$(".layui-table-body.layui-table-main").css("height",296-61-38);//列表里面层（横轴滚动条）
							}else{//没有权限，不用计算按钮列表的高度
								$(".layui-form.layui-border-box.layui-table-view").css("height",296-61+38+20);//列表外面层
								$(".layui-table-body.layui-table-main").css("height",296+18-61);//列表里面层（横轴滚动条）
							}
						}
					}else{
						//小于4环节的列表高度已经固定好了296
						$(".layui-form.layui-border-box.layui-table-view").css("height",296);//列表外面层 296：是固定死的高度
						$(".layui-table-body.layui-table-main").css("height",296-18-20);//列表里面层（横轴滚动条）
					}
					$(".bot-content").css("height",460);//主要内容的div
					
				}else{//浏览器有滚动条
						//默认环节前四个的高度
						$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层
						$(".layui-table-body.layui-table-main").css("height",listHeight+20);//列表里面层
					if(currentStep>=4){//第4、5、6、7、8、9环节进来
						if(currentStep==4){//面试邀约环节，没有横轴滚动条
							if(jurisdiction==true){//有权限者
								$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
								$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
							}else{//没有权限，不用计算按钮列表的高度
								$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38);//列表外面层 296：是固定死的高度
							}
						}else{//第五环节及后面，带有横轴滚动条
							if(jurisdiction==true){//有权限者
								$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+61);//列表外面层 296：是固定死的高度，60是减去按钮列表的高度
								$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
								$(".bot-content").css("height",listHeight+205+18+61);//主要内容的div
							}else{//没有权限，不用计算按钮列表的高度
								$(".layui-form.layui-border-box.layui-table-view").css("height",listHeight+38+20);//列表外面层
								$(".layui-table-body.layui-table-main").css("height",listHeight+18);//列表里面层（横轴滚动条）
								$(".bot-content").css("height",listHeight+205+18);//主要内容的div
							}
						}
					}
					
				}
			
			}
		
		//隐藏淘汰学生
		function hideProcessStatus(){
			if(statusBool ==true){//淘汰
				 var name =$("#name").val();//获取姓名
     			 var school =$("#school").val();//获取学校
     			 var major =$("#major").val();//获取专业值
     			 if(currentStep==4){//面试邀约
     				 processCount(1,name,school,major);
     			 }else{//小组讨论之后的环节
     				 processCount(2,name,school,major);
     			 }
			}else{//不淘汰
				 var name =$("#name").val();//获取姓名
     			 var school =$("#school").val();//获取学校
     			 var major =$("#major").val();//获取专业值
     			 if(currentStep==4){//面试邀约
     				 processCount(1,name,school,major);
     			 }else{//小组讨论之后的环节
     				 processCount(2,name,school,major);
     			 }
			}
			
		}
		
		
		//绘制面试评估分
		function replaceTotalScores(detailTotalScores,processStatus){
			var html ="";
			if(processStatus==4){//淘汰者背景页面变色
				html ='<div class="title weedOut" title="'+detailTotalScores+'">'+detailTotalScores+'</div>';
			}else{
				html ='<div class="title" title="'+detailTotalScores+'">'+detailTotalScores+'</div>';
			}
			return html;
		}
		
		
		//搜索事件
		function queryData(){
			layui.use(['layer', 'form'], function(){
				layer = layui.layer
				,form = layui.form;
				myindex = layer.load();//加载条
			});
			 var name =$("#name").val();//获取姓名
 			 var school =$("#school").val();//获取学校
 			 var major =$("#major").val();//获取专业值
 			 var exceptedCompany =$("#company").val();//获取期望公司值
 			 if(currentStep<=4){//面试邀约
 				 processCount(1,name,school,major,exceptedCompany);
 			 }else{//小组讨论之后的环节
 				 processCount(2,name,school,major,exceptedCompany);
 			 }
		}
		
		//回车键触发点击事件
		$("body").keydown(function() {
			var event=arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异
             if (event.keyCode == "13") {//keyCode=13是回车键
            	 queryData();//查询
             }
         });
		
		//绘制面试官
		function converdRecruiterName(recruiterName,recruiterId,processStatus,resumeid,processId,studentName){
			
			var html = "";
			if(processStatus!=4){//不等于淘汰
				if(recruiterName!=""&&recruiterName!=null){
				var arrayName = recruiterName.split(",");//面试官姓名
				var arrayID = recruiterId.split(",");//面试官id
				    html ="<div class='title'>";
				for(var i =0;i<arrayName.length;i++){
					html+="<span class='recrSpan '  id='"+arrayID[i]+"' onmouseout = 'removeicon(this)' onmouseover='addicon(this)'>"+arrayName[i]+"<span class='glyphicon glyphicon-remove icon' id='icon"+i+"' style='display:none;'  onclick=\"delrecruiterName('"+arrayID[i]+"','"+arrayName[i]+"','"+recruiterId+"','"+recruiterName+"','"+resumeid+"','"+processId+"','"+studentName+"')\" data='0'></span></span>";
				}
					html +="</div>";
				}
			}else{
				html ="<div class='title' title='"+recruiterName+"'>"+recruiterName+"</div>";
			}
			return html;
			
		}
		//悬浮追加父层事件
		function addicon(self){
			$(self).parent().attr("onmouseover","moveicon(this)");
		}
		//移除追加父层事件
		function removeicon(self){
			$(self).parent().attr("onmouseout","mouseouticon(this)");
		}
		//添加图标
		function moveicon(self){
			$(self).children().children(".icon ").show();//显示图标
		}
		//移除图标
		function mouseouticon(self){
			$(self).children().children(".icon ").hide();//隐藏图标
		}
		
		
		//删除面试官 recruiterId:面试官id,recruiterName:面试官姓名,recruiterIdArray:未拆分的面试官id,
	    //       recruiterNameArray:未拆分的面试官姓名,resumeid:学生简历id，processId：流程id,studentName:学生名称
		function delrecruiterName(recruiterId,recruiterName,recruiterIdArray,recruiterNameArray,resumeid,processId,studentName){
			layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content: '<div style="padding: 20px 75px;">是否删除“'+recruiterName+'”面试官 ？</div>'
		        ,closeBtn: false
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
			    ,resize:false //是否允许拉伸
		        ,closeBtn: false//不显示关闭图标
		        ,yes: function(){
		        	var idArray = recruiterIdArray.split(",");
		        	var nameArray = recruiterNameArray.split(",");
		        	var recruiter_id ="";//面试官id
		        	var recruiter_name ="";//面试官姓名
	
		        	var planNameCodeName = recruitPlanName+"("+phase_codeName+")";//拼接招聘计划+环节名称
		        	for(var i=0;i<idArray.length;i++){
		        		if(recruiterId!=idArray[i]){//判断等于删除的面试官
		        				recruiter_id += idArray[i]+",";//拼接面试官id
		        				recruiter_name += nameArray[i]+",";//拼接面试官名称
		        		}
		        	}
		        	recruiter_id= recruiter_id.substring(0,recruiter_id.length-1);
		        	recruiter_name= recruiter_name.substring(0,recruiter_name.length-1);
					$.ajax({
			  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.deleteRecruiterName.biz.ext",
			  			type:"POST",
			  			datatype: "json",
			  			data:{
			  				resume_Id:resumeid,//简历id
			  				phase_code:currentStep,//流程环节
			  				recruiter_id:recruiter_id,//面试官id
			  				recruiter_name:recruiter_name,  //面试官姓名
			  				selected_recruiter_id:recruiterId,  //选中面试官id
			  				processId:processId,  //流程id
			  				recruitPlanId:recruitPlanId,  //计划id
			  				planNameCodeName:planNameCodeName,//拼接招聘计划+环节名称
			  				studentName:studentName //学生名称
			  			},
			  			success:function(data){
			  				if(data.success!="-1"){
			  					layer.msg('删除成功');
			  					queryData();//初始流程环节所在人数
			  				}else{
			  					layer.msg('删除失败');
			  				}
			  			}
				  	});
		        	layer.closeAll();//关闭页面
		        }
			 	,btn2: function(){
		          layer.closeAll();//关闭页面
		        }
			});
		}
		
		//查看操作{
		function Ahref(self,planId,studentId,recruitId,ProcessId){
			$(self).attr("href",basePath+"campusRecruitment/resumeList/resume_select.jsp?recruitPlanId="+planId+"&resumeId="+studentId+"&phase_code="+currentStep+"&ProcessId="+ProcessId+"");
			$(self).click();
		}
		
		//查询期望公司
		function queryCompany(){
			$.ajax({
				url:'com.recruit.campusRecruitment.campusManagement.recruitPlan.queryExpectedCompany.biz.ext',
				type:'POST',
				dataType:'json',
				success:function(data){
					//返回数据长度大于0
					if(data.Company.length>0){
						//清空元素
						$("#company").empty();
						//动态渲染期望公司
						var option = "<option value=''>请选择期望公司</option>";
						for(var i = 0 ;i<data.Company.length;i++){
							option+="<option value="+data.Company[i].dictName+">"+data.Company[i].dictName+"</option>";
						}
						$("#company").append(option);
					} 
				}
			});
		}
		
		//分页条
		function laypage(count){
			layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
				  laypage.render({
				    elem: 'pagingToolbar'
				    ,count: count
				    ,curr:pageIndex
				    ,limit:limit
				    ,limits:[50,100,150,200]
				    ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
				    ,jump: function(obj,first){
				    	pageIndex = obj.curr;//获取当前页
				    	limit = obj.limit;//当前条数
				    	if(!first){
				    		queryData();//查询
				    	}
				    }
	  			});
	  		});
		}

		
		
