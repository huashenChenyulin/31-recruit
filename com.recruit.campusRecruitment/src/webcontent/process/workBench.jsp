<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<%@include file="/common/common.jsp"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000074
  - Date: 2018-09-11 10:10:56
  - Description:
-->
<head>
<%		
		//获取参数
		String recruitId = request.getParameter("recruit_id");
		//参数解密
		if(recruitId!=null&&recruitId!=""){
		recruitId = recruitId.replace(" ", "+");
		recruitId = new String(com.eos.foundation.common.utils.CryptoUtil.decryptByDES(recruitId,null));
		} 
		String planId = request.getParameter("planId");
		//参数解密
		 if(planId!=null&&planId!=""){
		 planId = planId.replace(" ", "+");
		planId = new String(com.eos.foundation.common.utils.CryptoUtil.decryptByDES(planId,null));
		} 
 %>
<title>面试工作台</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/campusRecruitment/process/reta/layui.css" />
    <style type="text/css">
    	.content{
    		padding: 5px 20px;
			width: 1360px;
			height: 100%;
			background-color: #f5f8fa;
			position: absolute;
			left: 0;
			right: 0;
			margin: 0 auto;
    	}
    	.top{
    		width: 100%;
			height: 61px;
			background-color: #e4ebf0;
			float: left;
    	}
    	.left{
    		float: left;
			width: 386px;
			overflow: scroll;
			position: absolute;
			top: 111px;
			bottom: 0;
    	
    	}
    	.right{
    		float: left;
			margin-left: 60px;
			overflow: scroll;
			position: absolute;
			top: 66px;
			bottom: 0;
			left: 368px;
			width: 912px;
    	
    	}
    	.bottom{
    	
    	}
    	.layui-colla-content {
		    padding: 0px;
		}
    	.item{
    		width: 100%;
			height: 100px;
			border-bottom: 1px solid #e6e6e6;
			position: relative;
			cursor: pointer;
    	}
    	.item-action{
    		border-left: 4px solid #0078ad;
    	}
    	.man{
    		width:80px;
    		height:80px;
    		background: url(img/man.png) no-repeat;
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.woman{
    		width:80px;
    		height:80px;
    		background: url(img/woman.png) no-repeat;
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.human{
    		width:80px;
    		height:80px;
    		
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.st-data{
    		float: left;
    		height: 80px;
  			margin: 10px 8px;
    	}
    	.st-data-item{
    		line-height: 26px;
    		max-height: 26px;
			overflow: hidden;
			width: 230px;
    	}
    	.st-btn{
    		position: absolute;
			top: 13px;
			right: 100px;;
    	}
    	.st-btn-tn{
    		position: absolute;
			top: 12px;
			right: 2px;
			font-size: 10px;
    	}
    	.st-btn-hz{
    		position: absolute;
			top: 40px;
			right: 5px;
			font-size: 10px;
    	}
    	.st-btn-em{
    		position: absolute;
			top: 65px;
			right: 5px;
			font-size: 10px;
    	}
    	.st-information{
    		width: 100%;
			height: 138px;
			background-color: white;
			float: left;
			padding: 10px 25px;
    	}
    	.student-name{
    		line-height: 36px;
			font-size: 30px;
			height: 36px;
			color: #666;
			margin-bottom: 5px;
    	}
    	.student-data{
    		float: left;

			line-height: 26px;
			
			font-size: 14px;
			
			height: 26px;
			
			color: #666;
			
			width: 100%;
    	}
    	.student-data1{
    		float: left;
			width: 30%;
    	}
    	.student-data2{
    		float: left;
			width: 30%;
    	}
    	.student-data3{
    		float: left;
			height: 100%;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
    	}
    	.layui-btn{
    		margin: 2px 26px 2px 0 !important;
			width: 68px!important;
    	}
    	.layui-btn-sm {
		
		    height: 24px !important;
		    line-height: 24px !important;
		    padding: 0 10px !important;
		    font-size: 14px !important;
		
		}
		.student-details{
			width: 100%;
			background-color: white;
			float: left;
			padding: 0px 25px 25px 25px;
			margin-top: 5px;
		}
		.reta-name{
			height: 38px;
			line-height: 38px;
			float: left;
			width: 14%;
			color: #666;
		}
		.reta{
			float: left;
		}
		.layui-rate {
		    padding: 7px 5px 8px 0;
		}
    	.evaluate{
    		float: left;
    		width: 100%;
    		margin: 2px 0px;
    	}
    	.evaluate-item{
    		float: left;
			width: 100%;
			height: 122px;
			background-color: #f5f8fa;
			border: 1px solid #e4ebf0;
			padding: 10px 15px;
			margin: 6px 0px;
    	}
    	.interviewer-name{
    		font-size: 13px;
			margin-right: 5px;
			width: 100px;
			float: left;
			height: 23px;
			line-height: 23px;
			font-weight: bold;
    	}
    	.interviewer-time{
    		float: right;
			color: #666;
			font-size: 12px;
    	}
    	.evaluate-item-tn{
    		width: 100%;
    		float: left;
    		margin-bottom: 2px;
    	}
    	.evaluate-item-state-green{
    		color: #009688;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-red{
    		color: #ff5722;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-blue{
    		color: #1e9fff;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-gray{
    		color: #666666;
    		margin-left: 5px;
    	}
    	.evaluate-item-num{
    		margin-left: 15px;
    	}
    	.evaluate-item-text{
    		height: 50px;
			margin-top: 10px;
			color: #666;
			overflow: hidden;
			border-bottom: 1px dotted #bfb7b7;
			font-size: 12px;
    	}
    	.hrs{
    		height: 2px;
			border: none;
		    border-top-color: currentcolor;
		    border-top-style: none;
		    border-top-width: medium;
			border-top: 4px dotted #e4ebf0;
			margin: 20px 0;
    	}
    	.table tbody tr td, .table tbody tr th, .table tfoot tr td, .table tfoot tr th, .table thead tr td, .table thead tr th{
			border-top: 0px solid;
		}
		.table thead tr th{
			border-bottom: 1px solid #ddd;
			color: #565656;
		}
		tbody{
			color: #656565;
		}
		#content-centre{
			float: left;
			height: 100%;
			width: 100%;
		}
		#centent-bottom{
			float: left;
			height: 100%;
			width: 100%;
		}
		#centent-bottom2{
			float: left;
			height: 100%;
			width: 100%;
		}
		.checkBoxs{
			
		}
		.st-data-name{
			font-size: 16px;
			color: #2f2f2f;
		}
		.stButton{
			width: 386px;
			height: 45px;
			border: 1px solid #f0eef0;
			position: absolute;
			top: 66px;
			background: #fff;
		}
		.btn-group{
			
		}
		.title-name{
			font-size: 20px;
			font-weight: bold;
			height: 36px;
			line-height: 36px;
			color: #f0ad4e;
			margin-left: 10px;
			text-align: center;
		}
		.title-i{
			font-size: 11px;
			height: 18px;
			line-height: 18px;
			color: #eea236;
			margin-left: 10px;
			text-align: center;
		}
		.dropdown-menu{
			min-width: 90px;
		}
		.st-data-s{
			white-space: nowrap;
    		text-overflow: ellipsis;
    		overflow: hidden;
		}
		.st-data-s{
			white-space: nowrap;
    		text-overflow: ellipsis;
    		overflow: hidden;
		}
		.leftMeun{
			    float: left;
			    width: 300px;
			    position: absolute;
			    top: 0;
			    bottom: 0;
			    background-color: #f5f8fa;
			    border-right: 1px solid #dbd8d8;
			    left: 0px; 
			    display: none;
			    padding-left: 20px;
		}
		.meunPlay{
				animation: indexNewPlay 200ms;
				-webkit-animation: indexNewPlay 200ms; /* Safari and Chrome */
				
				animation-timing-function:linear;
				-webkit-animation-timing-function:linear; /* Safari and Chrome */
				
				animation-iteration-count:1;
				-webkit-animation-iteration-count:1; /*Safari and Chrome*/
		}
		.meunLi{
			height: 46px;
			line-height: 46px;
			padding: 0px 10px;
			border-bottom: 1px solid #e0dddd;
			font-size: 16px;
			color: #5c5c5c;
			
		}
		.leftMeun-mini{
		float: left;
		width: 15px;
		position: absolute;
		top: 200px;
		background-color: #d9e8f3;
		border-right: 1px solid #d1caca;
		left: 0;
		height: 200px;
		border-top-right-radius: 100px;
		border-bottom-right-radius: 100px;
		font-size: 12px;
		color: #787878;
		padding: 50px 0px;
		}
		.leftMeun-zezao{
			float: left;
			width: 800px;
			position: absolute;
			left: 300px;
			bottom: 0;
			top: 0;
			display: none;
		}
		.nameSelectButton{
			float: left;
			width: 210px;
			position: absolute;
			top: 111px;
			left: 190px;
			display: none;
		}
		.classroom-time{
			position: absolute;
			top: 0px;
			right: 10px;
			font-size: 10px;
		}
		.layui-form-checkbox span {
		    width: 230px;
		}
		.layui-form-checkbox i {
		    height: 30px !important;
		}
		@keyframes indexNewPlay{
			0%{
				        -webkit-transform: translate3d(-300px,0,0) scale(1,1);
				        -moz-transform: translate3d(-300px,0,0) scale(1,1);
				        -ms-transform: translate3d(-300px,0,0) scale(1,1);
				        -o-transform: translate3d(-300px,0,0) scale(1,1);
				        transform: translate3d(-300px,0,0) scale(1,1);
				    }
				    50%{
				        -webkit-transform: translate3d(-150px,0,0) scale(1,1);
				        -moz-transform: translate3d(-150px,0,0) scale(1,1);
				        -ms-transform: translate3d(-150px,0,0) scale(1,1);
				        -o-transform: translate3d(-150px,0,0) scale(1,1);
				        transform: translate3d(-150px,0,0) scale(1,1);
				    }
				    100%{
				        -webkit-transform: translate3d(0,0,0) scale(1,1);
				        -moz-transform: translate3d(0,0,0) scale(1,1);
				        -ms-transform: translate3d(0,0,0) scale(1,1);
				        -o-transform: translate3d(0,0,0) scale(1,1);
				        transform: translate3d(0,0,0) scale(1,1);
				    }
		}
				
		@-webkit-keyframes indexNewPlay  /* Safari and Chrome */
				{
				0%{
				        -webkit-transform: translate3d(0,0,0) scale(1,1);
				        -moz-transform: translate3d(0,0,0) scale(1,1);
				        -ms-transform: translate3d(0,0,0) scale(1,1);
				        -o-transform: translate3d(0,0,0) scale(1,1);
				        transform: translate3d(0,0,0) scale(1,1);
				    }
				    50%{
				        -webkit-transform: translate3d(150px,0,0) scale(1,1);
				        -moz-transform: translate3d(150px,0,0) scale(1,1);
				        -ms-transform: translate3d(150px,0,0) scale(1,1);
				        -o-transform: translate3d(150px,0,0) scale(1,1);
				        transform: translate3d(150px,0,0) scale(1,1);
				    }
				    100%{
				        -webkit-transform: translate3d(300,0,0) scale(1,1);
				        -moz-transform: translate3d(300,0,0) scale(1,1);
				        -ms-transform: translate3d(300,0,0) scale(1,1);
				        -o-transform: translate3d(300,0,0) scale(1,1);
				        transform: translate3d(300,0,0) scale(1,1);
				    }
				}
    </style>
   
	<script type="text/javascript" src="<%=request.getContextPath()%>/campusRecruitment/process/reta/rate.js"></script>
</head>
<body>
<div class="content">
	<div class="top">
		<p class="title-name">面试工作台</p>
		<p class="title-i">面试官的推荐/不推荐/待定/未到场等结果会记录在学生的校招流程上，便于HR进行后面的面试安排</p>
	</div>
	<div class="stButton">
			<input type="checkbox" name="checkboxAll" onclick="checkboxOnclick(this)" style="margin-top: 16px;margin-left: 6px;"/>
			全选（共<span id="stCount"></span>人）
			<div class="btn-group" style="float: right;margin-top: 5px;margin-right: 5px;">
			    <div class="btn-group">
			      <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown" style="border:1px solid #e3e3e3;">标记通话<span class="caret"></span></button>
			      <ul class="dropdown-menu" role="menu">
			        <li><a href="#" onclick="getTheCheckBoxValue('1')">已通话</a></li>
			        <li><a href="#" onclick="getTheCheckBoxValue('2')">未接通</a></li>
			      </ul>
			    </div>
				<button type="button" class="btn " style="border:1px solid #e3e3e3;" onclick="sendStEmail()">发通知</button>
				<button type="button" class="btn " style="border:1px solid #e3e3e3;" data="off" onclick="nameSelectButtonShow(this)" >搜索</button>
		     </div>
		</div>
	<div class="left">
		
		<!-- <div class="layui-collapse"> -->
		 <!-- <div class="layui-colla-item">
		    <h2 class="layui-colla-title">教室A</h2>
		    <div class="layui-colla-content layui-show">
		    	<div class="item item-action">
		    		<div class="man"></div>
		    		<div class="st-data">
		    			<div class="st-data-item st-data-name">陈玉麟</div>
		    			<div class="st-data-item st-data-i">男 | 24 | 大专</div>
		    			<div class="st-data-item st-data-s">华南理工大学 | 计算机软件</div>
		    		</div>
		    		<div class="st-btn"><span class="layui-badge layui-bg-green">推荐</span></div>
		    	</div>
		    	<div class="item">
		    		<div class="woman"></div>
		    		<div class="st-data">
		    			<div class="st-data-item st-data-name">刘永欣</div>
		    			<div class="st-data-item st-data-i">女 | 24 | 大专</div>
		    			<div class="st-data-item st-data-s">华南理工大学 | 计算机软件</div>
		    		</div>
		    		<div class="st-btn"><span class="layui-badge layui-bg-green">推荐</span></div>
		    	</div>
		    </div>
		  </div> -->
		  <!-- <div class="layui-colla-item">
		    <h2 class="layui-colla-title">教室B</h2>
		    <div class="layui-colla-content">
				<div class="item"></div>
				<div class="item"></div>
				<div class="item"></div>
				<div class="item"></div>
				<div class="item"></div>
			</div>
		  </div> -->
		<!-- </div> -->
	
	</div>
	<!-- 姓名搜索框 -->
	<div class="nameSelectButton">
				<div class="input-group  ">
					<input type="text" class="form-control" id="selectStname">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" onclick="selectStName()">
							<i class="layui-icon" style="font-size: 14px;">&#xe615;</i>
						</button>
					</span>
				</div>
	</div>			
				
	<div class="right">
		<div class="st-information">
			<div class="information-left" style="width: 90%;float: left;">
				<div class="student-name"></div>
				<div class="student-data ">
					<div class="student-data1"><span id="student-data-gender"></span><span id="student-data-degree"></span><span id="student-data-birthDate"></span></div><div class="student-data2" id="student-data-phone"></div><div class="student-data1">当前流程：<span id="student-data-step"></span></div>
				</div>
				<div class="student-data">
					<div class="student-data1" id="student-data-college"></div><div class="student-data2" id="student-data-email"></div><div class="student-data3" style="width: 300px;"><span id="student-data-classroom"></span> | <span id="student-data-time"></span></div>
				</div>
				<div class="student-data">
					<div class="student-data3">应聘职位：<span id="student-data-zw" title=""></span></div>
				</div>
			</div>
			<div class="information-right" style="width: 10%;float: left;padding-left: 15px;">
				<button class="layui-btn  layui-btn-sm" id="btn-tj">推荐</button>
				<button class="layui-btn layui-btn-normal  layui-btn-sm" id="btn-dd">待定</button>
				<button class="layui-btn layui-btn-danger  layui-btn-sm" id="btn-btj">不推荐</button>
				<button class="layui-btn layui-btn-primary  layui-btn-sm" id="btn-wdc">未到场</button>
			</div>
		</div>
		<div class="student-details">
			<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
			  <ul class="layui-tab-title">
			    <li >简历信息</li>
			    <li class="layui-this">评价</li>
			  </ul>
			  <div class="layui-tab-content" style="height: 100px;">
			    <div class="layui-tab-item ">
			    	<!-- 简历信息 -->
			    	<div id="content-centre">
						<table class="table">
							<caption>基本信息</caption>
							
							
							
							<thead>
								<tr>
									<th>身份证号码</th>
									<th>户口所在地</th>
									<th>现居地</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="identityNo"></td>
									<td id="birthplace"></td>
									<td id="address"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>紧急联系人姓名</th>
									<th>与紧急联系人关系</th>
									<th>紧急联系人电话</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="emergencyContact"></td>
									<td id="emergencyRelationship"></td>
									<td id="emergencyPhone"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>身高(cm)/体重(kg)</th>
									<th>英语等级</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="heightAndWeight"></td>
									<td id="english_level"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- 尾部的-->
					<div id="centent-bottom">
						<table class="table" >
							<caption>申请职位</caption>
					
							<thead>
								<tr>
									<th>期望工作城市</th>
									<th>期望薪资</th>
									<th>得知招聘信息的渠道</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="expected-address"></td>
									<td id="expected-salary"></td>
									<td id="recruit-noticed-channel"></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th>是否服从工作职位的调配</th>
									<th>是否服从工作城市的调配</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="isPositionArrange"></td>
									<td id="is-workplace-arrange"></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- 尾部的-->
					<div id="centent-bottom2">
						<table class="table" id="personalResume">
							
						</table>
						<table class="table" id="recruitSchoolPractice">
							
						</table>
						<table class="table" id="recruitWorkExperience">
							
						</table>
					</div>
			    </div>
			    <div class="layui-tab-item layui-show">
			    	<!-- 评价 -->	
			    	<div class="evaluate">
			    		<div class="reta-name">适应力:</div>
			    		<div id="reta1" class="reta"></div>
			    		<input name="title" id="reta1-evaluate" lay-verify="title" autocomplete="off" placeholder="请输入评分原因" class="layui-input" type="text" style="float: left; width: 60%;margin-left: 2px">
			    		 
			    	</div>
			    	<div class="evaluate">
			    		<div class="reta-name">提供和接受反馈:</div>
			    		<div id="reta2" class="reta"></div>
			    		<input name="title" id="reta2-evaluate" lay-verify="title" autocomplete="off" placeholder="请输入评分原因" class="layui-input" type="text" style="float: left; width: 60%;margin-left: 2px" onpropertychange="OnPropChanged(event)">
			    		 
			    	</div>
			    	<div class="evaluate">
			    		<div class="reta-name">创新:</div>
			    		<div id="reta3" class="reta"></div>
			    		<input name="title" id="reta3-evaluate" lay-verify="title" autocomplete="off" placeholder="请输入评分原因" class="layui-input" type="text" style="float: left; width: 60%;margin-left: 2px" onpropertychange="OnPropChanged(event)">
			    		 
			    	</div>
			    	<div class="evaluate">
			    		<div class="reta-name">分析和解决问题:</div>
			    		<div id="reta4" class="reta"></div>
			    		<input name="title" id="reta4-evaluate" lay-verify="title" autocomplete="off" placeholder="请输入评分原因" class="layui-input" type="text" style="float: left; width: 60%;margin-left: 2px;" onpropertychange="OnPropChanged(event)">
			    		 
			    	</div>
			    	<div class="evaluate">
			    		<div class="reta-name">影响他人:</div>
			    		<div id="reta5" class="reta"></div>
			    		<input name="title" id="reta5-evaluate" lay-verify="title" autocomplete="off" placeholder="请输入评分原因" class="layui-input" type="text" style="float: left; width: 60%;margin-left: 2px " onpropertychange="OnPropChanged(event)">
			    		 
			    	</div>
			    	 <textarea placeholder="请输入综合评价" class="layui-textarea" style="width: 90%;float: left;margin: 5px 0px 10px 0px;" onpropertychange="OnPropChanged(event)"></textarea>
			    	 <button class="layui-btn layui-btn-primary" style="width: 100px !important;border-radius: 4px;background: #f0ad4e;color: #fff;border-color: #eea236;" onclick="updateStEvaluate('1')">保存</button>
			    	 <div class="hrs"></div>
			    	 <div id="evaluate-content" style="float: left;width: 100%;">
			    	
			    	 </div>
			    </div>
			  </div>
			</div> 
		</div>
		    
	</div>
	
	
	
</div>
<!-- 左侧栏 -->
	<div class="leftMeun">
		<p style="font-size: 20px;font-weight: bold;margin-top: 20px;">校招计划</p>
		<span style="font-size: 11px;margin-bottom: 10px;float: left;">学生栏所显示的信息包括以下勾选的校招计划</span>
		<!-- <div class="meunLi" style="color: white;background-color: #1e9fff;">华南理工沸腾项目</div>
		<div class="meunLi">华南理工沸腾项目</div>
		<div class="meunLi">华南理工沸腾项目</div>
		<div class="meunLi">华南理工沸腾项目</div> -->
		
	</div>
	
	<div class="leftMeun-mini">
		筛选校招计划	
	</div>
	
	<div class="leftMeun-zezao">
		
	</div>
	
	
	<script type="text/javascript">
	var layer;
	layui.use('layer', function(){
	  layer = layui.layer;
	  
	});   
	          
	/* layui.use('element', function(){
		  var element = layui.element;
		  element.on('tab(docDemoTabBrief)', function(data){
			  console.log(this); //当前Tab标题所在的原始DOM元素
			  console.log(data.index); //得到当前Tab的所在下标
			  console.log(data.elem); //得到当前的Tab大容器
			});
		  
		}); */
		
	var planId="<%=planId %>";	
	var interviewerId="<%=recruitId %>";/* 10111529  10112757*/
	var stId="";
	var evaluateId="";
	var stProcessId="";
	var stInterviewStep="";
	
	var stRate1=0;
	var stRate2=0;
	var stRate3=0;
	var stRate4=0;
	var stRate5=0;
	
	var phases_id ="";//环节id
  	
  	
  	var changets="";//已编辑状态
  	var element;
  	$(function(){
  			getPlan();
  			 
  			
			
			
			$("#reta1-evaluate,#reta2-evaluate,#reta3-evaluate,#reta4-evaluate,#reta5-evaluate,.layui-textarea").bind("input propertychange",function(event){
			       changets="1";
			});
  	
  	})
  	
  	function getClassroom(planIds,stName){
  		$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryClassroom.biz.ext",
			type:"POST",
			data:{
				planId:planIds,
				recruiterId:interviewerId,
				resumeName:stName
			},
			success:function(data){
				var stCount=0;
				/* console.log(data); */
				$(".left").html("");
				var html="<div class='layui-collapse'>";
				
				
				$(data.classroomResumes).each(function(i,obj){
					html+="<div class='layui-colla-item'>";
				    html+="<h2 class='layui-colla-title'>"+obj.classroom+"<div class='classroom-time'>"+getDateTimes(obj.time)+"</div></h2>";
				    if(i=="0"){
				    	html+="<div class='layui-colla-content layui-show'>";
				    }else{
				    	html+="<div class='layui-colla-content'>";
				    }
				    
				    	$(obj.resume).each(function(j,obj2){
				    		stCount++;
				    		if(i=="0"&&j=="0"){html+="<div class='item item-action' onclick='changeStudent("+obj2.resume_id+","+obj2.process_id+",this,"+obj2.phase_code+")'>";}else{html+="<div class='item' onclick='changeStudent("+obj2.resume_id+","+obj2.process_id+",this,"+obj2.phase_code+")'>";}
				    		html+="<input type='checkbox' value='"+JSON.stringify(obj2)+"' name='sProblem' class='checkBoxs' style='float: left;margin-top: 40px;margin-left: 6px;'>"
				    		if(obj2.resume_picture_url!=""&obj2.resume_picture_url!=null){
				    			var url="<%=request.getContextPath()%>/"+obj2.resume_picture_url;
				    			html+="<div class='human' style='background: url("+url+") no-repeat;background-size: 100% 100%'></div>";
				    		}else{
				    			if(obj2.gender=="女"){
					    			html+="<div class='woman'></div>";
					    		}else{
					    			html+="<div class='man'></div>";
				    		}
				    		}
				    		
				    		
				    		html+="<div class='st-data'>";
				    		html+="<div class='st-data-item st-data-name'>"+obj2.candidate_name+"</div>";
				    		html+="<div class='st-data-item st-data-i'>"+getStData(obj2.gender)+""+getStData(obj2.birth_date)+""+obj2.degree+"</div>";
				    		html+="<div class='st-data-item st-data-s'>"+obj2.college+" | "+obj2.major+"</div>";
				    		html+="</div>";
				    		if(obj2.suggest=="推荐"){
				    			html+="<div class='st-btn'><span class='layui-badge layui-bg-green'>推荐</span></div>";
				    		}else if(obj2.suggest=="待定"){
				    			html+="<div class='st-btn'><span class='layui-badge layui-bg-blue'>待定</span></div>";
				    		}else if(obj2.suggest=="不推荐"){
				    			html+="<div class='st-btn'><span class='layui-badge'>不推荐</span></div>";
				    		}else if(obj2.suggest=="未到场"){
				    			html+="<div class='st-btn'><span class='layui-badge layui-bg-black'>未到场</span></div>";
				    		}else{
				    			html+="<div class='st-btn'></div>";
				    		}
				    		
				    		html+="<div class='st-btn-tn'>"+getstep(obj2.phase_code)+"</div>";
				    		if(obj2.is_call=="1"){
				    			html+="<div class='st-btn-hz'><span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #6db2e3;'></span></div>";
				    		}
				    		else if(obj2.is_call=="2"){
				    			html+="<div class='st-btn-hz'><span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #ff5722;'></span></div>";
				    		}else{
				    			html+="<div class='st-btn-hz'></div>";
				    		}
				    		
				    		if(obj2.is_mailAndSMS=="1"){
				    			html+="<div class='st-btn-em'><span class='glyphicon glyphicon-envelope' style='font-size: 13px;color: #6db2e3;'></span></div>";
				    		}else{
				    			html+="<div class='st-btn-em'></div>";
				    		}	
				    		html+="</div>";
				    	})
				    html+="</div>";	
				    html+="</div>";	
				})
				html+="</div>";	
				$(".left").append(html);
				$("#stCount").text(stCount);
				layui.use('element', function(){
					//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
					element = layui.element;
					element.init();
				  
				});
				
				getStudent(data.classroomResumes[0].resume[0].resume_id,data.classroomResumes[0].resume[0].recruit_plan_id,data.classroomResumes[0].resume[0].process_id);
				planId=data.classroomResumes[0].resume[0].recruit_plan_id;
				badge=$($(".item")[0]).children(".st-btn");//记住第一个学生的dom元素
				/* $($(".item")[0]).children(".checkBoxs").prop("checked",true); */
				stId=data.classroomResumes[0].resume[0].resume_id;//记录学生ID全局变量
				stProcessId=data.classroomResumes[0].resume[0].process_id;
				stInterviewStep=data.classroomResumes[0].resume[0].phase_code;
				selectStEvaluate(data.classroomResumes[0].resume[0].resume_id,data.classroomResumes[0].resume[0].process_id,data.classroomResumes[0].resume[0].phase_code);
				getEvaluate();
				selectResume();
				$("input[name='sProblem']").click(function(evt){
							// 阻止冒泡  
			    evt.stopPropagation();  
			})  
			}
			});
  	
  	
  	
  	}
  	
  	//查询面试官所负责的计划
  	function getPlan(){
  		$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryWorkbenchPlan.biz.ext",
			type:"POST",
			data:{
				interviewerId:interviewerId
			},
			success:function(data){
			var html="";
				if(planId=="no"){
					 html+="<form class='layui-form'>";
					 html+="<div class='layui-form-item'>"; 
					 var id="";
					 $(data.planWorkbench).each(function(i,obj){
					 	if(i=="0"){
					 		id=obj.recruitPlanId;
					 		planId=obj.recruitPlanId;
					 		html+="<input name='planCheckbox' lay-filter='plan' value='"+obj.recruitPlanId+"' title='"+obj.recruitPlanName+"' type='checkbox' checked>"
					 	}else{
					 		html+="<input name='planCheckbox' lay-filter='plan' value='"+obj.recruitPlanId+"' title='"+obj.recruitPlanName+"' type='checkbox'>"
					 	}
					 	
					 });
					 html+="</div>";
					 html+="</form>";
					 getClassroom(id,"");
				
				}else{
					 html+="<form class='layui-form'>";
					 html+="<div class='layui-form-item'>"; 
					 $(data.planWorkbench).each(function(i,obj){
					 	if(obj.recruitPlanId==planId){
					 		html+="<input name='planCheckbox' lay-filter='plan' value='"+obj.recruitPlanId+"' title='"+obj.recruitPlanName+"' type='checkbox' checked>"
					 	}else{
					 		html+="<input name='planCheckbox' lay-filter='plan' value='"+obj.recruitPlanId+"' title='"+obj.recruitPlanName+"' type='checkbox'>"
					 	}
					 	
					 });
					 html+="</div>";
					 html+="</form>";
					 getClassroom(planId,"");
				}   
				 
				 $(".leftMeun").append(html);
				 layui.use('form', function(){
				  var form = layui.form;
				  form.on('checkbox(plan)', function(data){
					  /* console.log(data.elem.checked); //是否被选中，true或者false
					  console.log(data.value);  */
					  var checkedValue="";
					  $("input[name='planCheckbox']:checked").each(function(){
		                    
		                	checkedValue+=$(this).val()+",";
		            })
		            checkedValue = checkedValue.substring(0,checkedValue.length-1);
		            console.log(checkedValue);
		            getClassroom(checkedValue,"");
					});        
				 
				}); 
			}
			});
  	
  	}
  	//切换学生
  	var badge;
  	function changeStudent(resumeId,processId,obj,phaseCode){
  		if(changets=="1"){
  			layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,content: '<div style="padding: 20px 75px;">您还未保存评价，是否保存</div>'
			        ,closeBtn: false
			        ,btn:['保存','不保存']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
				    ,resize:false //是否允许拉伸
			        ,closeBtn: false//不显示关闭图标
			        ,yes: function(){
			        	updateStEvaluate('1');
			        	layer.closeAll();//关闭页面
			        	return;
			        }
			        ,btn2: function(){
			          changets="0";
			          layer.closeAll();//关闭页面
			          changeStudent(resumeId,processId,obj,phaseCode);
			        }
			  });
  		}else{
  			$(".item").removeClass("item-action");
  			var value=$(obj).children(".checkBoxs").val();
  			value=JSON.parse(value);
  			planId=value.recruit_plan_id;//转换当前学生的计划ID，后续调用的方法都需要这个ID
	  		 getStudent(resumeId,planId,processId);
	  		$(obj).addClass("item-action");
	  		badge=$(obj).children(".st-btn");
	  		/* $(obj).children(".checkBoxs").prop("checked",true); */
	  		stProcessId=processId;
	  		stId=resumeId;
	  		stInterviewStep=phaseCode;
	  		selectStEvaluate(resumeId,processId,phaseCode);
	  		getEvaluate();
	  		selectResume();
  		}
  		
  	}
  	//查询学生个人信息
  	function getStudent(resumeId,planId,processId){
  		$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryWorkbenchResume.biz.ext",
			type:"POST",
			data:{
				resumeId:resumeId,
				planId:planId,
				processId:processId
			},
			success:function(data){
				/* console.log(data); */
				$(".student-name").text(data.workbenchResume[0].candidate_name);
				$("#student-data-gender").text(getStData(data.workbenchResume[0].gender));
				$("#student-data-birthDate").text(data.workbenchResume[0].birth_date);
				$("#student-data-degree").text(getStData(data.workbenchResume[0].degree));
				$("#student-data-phone").html("<span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #6db2e3;'></span>"+data.workbenchResume[0].candidate_phone);
				$("#student-data-zw").text(data.workbenchResume[0].position_name);
				$("#student-data-zw").attr("title",data.workbenchResume[0].position_name);
				$("#student-data-college").text(data.workbenchResume[0].college+" | "+data.workbenchResume[0].major);
				$("#student-data-email").html("<span class='glyphicon glyphicon-envelope' style='font-size: 13px;color: #6db2e3;'></span>"+data.workbenchResume[0].candidate_mail);
				$("#student-data-classroom").text(data.workbenchResume[0].classroom);
				$("#student-data-time").text(getDateTimes(data.workbenchResume[0].interview_time));
				$("#student-data-step").text(getstep(data.workbenchResume[0].current_step));
				$("#btn-tj").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'1')");
				$("#btn-dd").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'2')")
				$("#btn-btj").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'3')")
				$("#btn-wdc").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'4')")
				stProcessId=data.workbenchResume[0].process_id;
  				stId=data.workbenchResume[0].resume_id;
  				stInterviewStep=data.workbenchResume[0].current_step;
				
			}
			});
  	
  	}
  	//查询当前评价，回填
  	function selectStEvaluate(resumeId,processId,interviewStep){
  		$.ajax({
			url:"com.recruit.workbench.campusWorkbench.querycCampusInterviewDetail.biz.ext",
			type:"POST",
			data:{
				recruitPlanId:planId,
				resumeId:resumeId,
				processId:processId,
				interviewStep:interviewStep,
				interviewerId:interviewerId
			},
			success:function(data){
				/* console.log(data); */
				evaluateId=data.detail.id;
				stRate1=data.detail.acceptabilityPoints;
				$("#reta1-evaluate").val(data.detail.acceptabilityPointsEvaluate); 
				
				stRate2=data.detail.feedbackPoints;
				$("#reta2-evaluate").val(data.detail.feedbackPointsEvaluate); 
				
				stRate3=data.detail.innovationPoints;
				$("#reta3-evaluate").val(data.detail.innovationPointsEvaluate); 
				
				stRate4=data.detail.analyzingAbility;
				$("#reta4-evaluate").val(data.detail.analyzingAbilityEvaluate); 
				
				stRate5=data.detail.affectionPoints;
				$("#reta5-evaluate").val(data.detail.affectionPointsEvaluate); 
				$(".layui-textarea").val(data.detail.evaluate);
				layui.use(['rate'], function(){
				  var rate = layui.rate;
				  //基础效果
				  rate.render({
				    elem: '#reta1'
				    ,value:stRate1
				    ,choose: function(value){
					    stRate1=value;
					    changets="1";
					  }
				  })
				  rate.render({
				    elem: '#reta2'
				    ,value:stRate2
				    ,choose: function(value){
					    stRate2=value;
					    changets="1";
					  }
				  })
				  rate.render({
				    elem: '#reta3'
				    ,value:stRate3
				    ,choose: function(value){
					    stRate3=value;
					    changets="1";
					  }
				  })
				  rate.render({
				    elem: '#reta4'
				    ,value:stRate4
				    ,choose: function(value){
					    stRate4=value;
					    changets="1";
					  }
				  })
				  rate.render({
				    elem: '#reta5'
				    ,value:stRate5
				    ,choose: function(value){
					    stRate5=value;
					    changets="1";
					  }
				  })
			  	})
			}
			});
  	
  	}
  	
  	function getStData(o){
  		var i=""
  		if(o!=""&&o!=null){
  			i=o+" | "
  		}
  		return i;
  	}
  	function getstep(o){
  		var i=""
  		if(o=="5"){
  			i="小组讨论"
  		}
  		if(o=="6"){
  			i="一对一面试"
  		}
  		if(o=="7"){
  			i="部门面试"
  		}
  		if(o=="8"){
  			i="评估中心"
  		}
  		if(o=="9"){
  			i="offer"
  		}
  		return i;
  	}
  	
  	function getDateTimes(date){
        	 var curTime;
		      if(date == null || date == ""){
		        	curTime = "";
		      }else{
			        var oldTime = (new Date(''+date+'')).getTime();
					curTime = new Date(oldTime).format("yyyy-MM-dd hh:mm");
			  }
		      
		      return curTime;
        	
        }
        //标记学生
        function changeOpinion(resumeId,processId,phaseCode,suggest){
        var value="";
        if(suggest=="1"){
						value="推荐"		    			
					 }else if(suggest=="2"){
						value="待定"			 				
					    			
					}else if(suggest=="3"){
						value="不推荐"			 				
					}else if(suggest=="4"){
						value="未到场"	
					}
        layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,content: '<div style="padding: 20px 75px;">确认标记为'+value+'</div>'
			        ,closeBtn: false
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
				    ,resize:false //是否允许拉伸
			        ,closeBtn: false//不显示关闭图标
			        ,yes: function(){
			        	$.ajax({
							url:"com.recruit.workbench.campusWorkbench.UpdateCampusPhaseOpinion.biz.ext",
							type:"POST",
							data:{
								recruitPlanId:planId,
								resumeId:resumeId,
								processId:processId,
								phaseCode:phaseCode,
								suggest:suggest,
								recruiterId:interviewerId
							},
							success:function(data){
								if(suggest=="1"){
								    			$(badge).html("<span class='layui-badge layui-bg-green'>推荐</span>");
								 }else if(suggest=="2"){
								 				$(badge).html("<span class='layui-badge layui-bg-blue'>待定</span>");
								    			
								 }else if(suggest=="3"){
								 				$(badge).html("<span class='layui-badge'>不推荐</span>");
								 }else if(suggest=="4"){
								 				$(badge).html("<span class='layui-badge layui-bg-black'>未到场</span>");
								 }
								 layer.closeAll();//关闭页面
							}
							});
			        
			          }
			        ,btn2: function(){
			          layer.closeAll();//关闭页面
			        }
			  });
        
        	
        }
        
        
        //保存评价
        function updateStEvaluate(i){
        	$.ajax({
			url:"com.recruit.workbench.campusWorkbench.updateCampusInterviewDetail.biz.ext",
			type:"POST",
			data:{
				acceptabilityPoints:stRate1,//适应力分
				acceptabilityPointsEvaluate:$("#reta1-evaluate").val(),
				feedbackPoints:stRate2,//提供与反馈
				feedbackPointsEvaluate:$("#reta2-evaluate").val(),
				innovationPoints:stRate3,//创新分
				innovationPointsEvaluate:$("#reta3-evaluate").val(),
				analyzingAbility:stRate4,//分析和解决能分 
				analyzingAbilityEvaluate:$("#reta4-evaluate").val(),
				affectionPoints:stRate5,//影响他人
				affectionPointsEvaluate:$("#reta5-evaluate").val(),
				totalScores:stRate1+stRate2+stRate3+stRate4+stRate5,//总分
				evaluate:$(".layui-textarea").val(),
				id:evaluateId,
				
				
				
			},
			success:function(data){
				if(i=="1"&&data.out1=="成功"){
					layer.msg("保存成功");
					changets="0";
				}else{
					layer.msg("保存失败");
				}
			}
			});
        }
        
        
        function getEvaluate(){
        
        	$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryCampusInterviewDetail.biz.ext",
			type:"POST",
			data:{
				interviewerId:interviewerId,
				interviewStep:stInterviewStep,
				recruitPlanId:planId,
				resumeId:stId,
				processId:stProcessId
			},
			success:function(data){
				/* console.log(data); */
				 $("#evaluate-content").html("");
				var html="";
				$(data.interviewDetail).each(function(i,obj){
					html+="<div class='evaluate-item'>";
					html+="<div class='evaluate-item-tn'>";
					html+="<span class='interviewer-name'>"+obj.interviewer_name+"的评价</span><span class='layui-badge-rim' style='float: left;'>"+getstep(obj.interview_step)+"</span><span class='interviewer-time'>"+getDateTimes(obj.interview_time)+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn'>";
					if(obj.suggest=="推荐"){
						html+="<span class='layui-badge-dot layui-bg-green'></span>";
						html+="<span class='evaluate-item-state-green'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="待定"){
						html+="<span class='layui-badge-dot layui-bg-blue'></span>";
						html+="<span class='evaluate-item-state-blue'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="不推荐"){
						html+="<span class='layui-badge-dot'></span>";
						html+="<span class='evaluate-item-state-red'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="未到场"){
						html+="<span class='layui-badge-dot layui-bg-black'></span>";
						html+="<span class='evaluate-item-state-gray'>"+obj.suggest+"</span>";
					}else{
						html+="<span class='layui-badge-dot layui-bg-gray'></span>";
						html+="<span class='evaluate-item-state-gray'>未评价</span>";
					}
					html+="<span class='evaluate-item-num'>总分："+obj.total_scores+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'>"+isEmpty(obj.evaluate)+"</div>";
					html+="</div>";
				})
				$("#evaluate-content").append(html);
				
			}
			});
        }
        
        //标记通话 1.已通话 2.未接通  。获取checkbox
        function getTheCheckBoxValue(i){
		        
		var checkedNum = $("input[name='sProblem']:checked").length;
		if (checkedNum > 0) {
			var test = $("input[name='sProblem']:checked");
		var checkBoxValue = ""; 
		test.each(function(){
			var val=JSON.parse($(this).val());
			checkBoxValue+=val.phases_id+",";
		})
		checkBoxValue = checkBoxValue.substring(0,checkBoxValue.length-1);
			$.ajax({
			url:"com.recruit.workbench.campusWorkbench.updateCampusProcessPhase.biz.ext",
			type:"POST",
			data:{
				phaseId:checkBoxValue,
				callStatus:i
			},
			success:function(data){
				test.each(function(){
					if(i=="1"){
						$(this).parent().find(".st-btn-hz").html("<span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #6db2e3;'></span>");
					}
					if(i=="2"){
						$(this).parent().find(".st-btn-hz").html("<span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #ff5722;'></span>");
					}
					
				});
				layer.msg("标记成功");
			}
			});
		    
		} else {
		    layer.msg("至少选中一个");
		}
		
		
		}
		//发送通知
		function sendStEmail(){
		var test = $("input[name='sProblem']:checked");
		var mailValue = ""; 
		var phoneValue = ""; 
		var nameValue = "";
		var phases_id="";
		test.each(function(){
			var val=JSON.parse($(this).val());
			mailValue+=val.candidate_name+"<"+val.candidate_mail+">"+",";
			phoneValue+=val.candidate_phone+",";
			nameValue+=val.candidate_name+",";
			phases_id +=val.phases_id+",";//环节id
		})
		mailValue = mailValue.substring(0,mailValue.length-1);
		phoneValue = phoneValue.substring(0,phoneValue.length-1); 
		nameValue= nameValue.substring(0,nameValue.length-1);
		phases_id= phases_id.substring(0,phases_id.length-1);
		
			openEmail(mailValue,nameValue,phoneValue,planId,'work',0);
		}
		
		//修改通知状态,interviewTime:面试时间，classroom：面试教室
		function updateInformWork(interviewTime,classroom){
			$.ajax({
				url:"com.recruit.workbench.campusWorkbench.updateCampusProcessPhase.biz.ext",
				type:"POST",
				data:{
					phaseId:phases_id,
					type:"email",
					interviewTime:interviewTime,
					classroom:classroom
				},
				success:function(data){
					if(data.out1!="成功"){
						layer.msg("保存通知状态失败");
					}
				}
			});
		}
		
		
		
		function checkboxOnclick(checkbox){
 
			if ( checkbox.checked == true){
			 	checkall();
			}else{
				clearall();
			}
		}
		//全选
		function checkall(){
	        var hobby = $("input[name='sProblem']");
	        var i;
	        for(i = 0;i < hobby.length;++i){
	            hobby[i].checked=true;
	        }
	        
	 
	    }
	    //全不选
    	function clearall(){
	        var hobby = $("input[name='sProblem']");
	        var i;
	        for(i= 0;i < hobby.length;++i){
	            hobby[i].checked=false;
	        }
	 
	    }
        
        
        //简历信息
        var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	
		//是否服从工作职位的调配
		 function is_positionArrange(i){
			var isPositionArrange="";
		 	if(i==1){
		 		isPositionArrange="是";
		 	}else{
		 		isPositionArrange="否";
		 	}
		 	return isPositionArrange;
		 } 	
		 //是否服从工作城市的调配
		 function is_workplaceArrange(i){
			var workplaceArrange="";
		 	if(i==1){
		 		workplaceArrange="是";
		 	}else{
		 		workplaceArrange="否";
		 	}
		 	return workplaceArrange;
		 } 	
		 //得知招聘信息的渠道
		 function recruitNoticedChannel(i){
			var recruitNoticedChannel="";
		 	if(i==1){
		 		recruitNoticedChannel="索菲亚官网";
		 	}else if(i==2){
		 		recruitNoticedChannel="宣讲会";
		 	}else if(i==3){
		 		recruitNoticedChannel="微信推送";
		 	}else if(i==4){
		 		recruitNoticedChannel="招聘网站";
		 	}else if(i==5){
		 		recruitNoticedChannel="其他";
		 	}
		 	return recruitNoticedChannel;
		 }
		 //英语等级
		 function englishLevel(i){
			var englishLevel="";
		 	if(i==01){
		 		englishLevel="大学英语四级";
		 	}else if(i==02){
		 		englishLevel="大学英语六级";
		 	}else if(i==03){
		 		englishLevel="专业四级";
		 	}else if(i==04){
		 		englishLevel="专业八级";
		 	}else{
		 		englishLevel="无";
		 	}
		 	return englishLevel;
		 }
		  	
		 
		 function selectResume(){
		 	var json={
		 		resumeId:stId
		 	}
		 	$.ajax({
		 		url:"com.recruit.campusRecruitment.campusManagement.resume.queryResume.biz.ext",
		 		type:"post",
		 		data:json,
		 		success:function(data){
		 			//简历表
		 			var resume=data.resume;
		 			//职位表
		 			var position=data.position;
		 			//附件表
		 			var resume_axccessory=data.resume_axccessory;
		 			//在校实践表
		 			var resume_school=data.resume_school;
		 			//社会实践表
		 			var resume_work=data.resume_work;
		 			

		 			//现居地
		 			$("#address").text(resume[0].address);
		 			
		 			//身份证
		 			$("#identityNo").text(resume[0].identityNo);
		 			//户口所在地
		 			$("#birthplace").text(resume[0].birthplace);
		 			
	 				var weig=resume[0].weight;//体重
	 				var heig=resume[0].height;//身高
		 			if(resume[0].height=="" || resume[0].height==null){
		 				//身高
		 				heig="";
		 			}
					if(resume[0].weight=="" || resume[0].weight==null){
		 				//体重
		 				weig ="";
		 			}
	 				//身高体重
	 				$("#heightAndWeight").text(heig+"/"+weig);
		 			
		 			//紧急联系人
		 			$("#emergencyContact").text(resume[0].emergencyContact);
		 			//联系人关系
		 			$("#emergencyRelationship").text(resume[0].emergencyRelationship);
		 			//联系人电话
		 			$("#emergencyPhone").text(resume[0].emergencyPhone);
		 			//英语等级
		 			$("#english_level").text(englishLevel(resume[0].englishLevel));
		 			//毕业时间
		 			$("#graduationDate").text(resume[0].graduationDate);
		 			
		 			//期望工作城市
		 			$("#expected-address").text(resume[0].exceptedWorkAreaA);
		 			//期望薪资
		 			$("#expected-salary").text(resume[0].expectedSalary);
		 			//得知招聘信息的渠道
		 			if(resume[0].recruitNoticedChannelName!="" && resume[0].recruitNoticedChannelName!=null){
		 				$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel)+"—"+resume[0].recruitNoticedChannelName);
		 			}else{
			 			$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel));
		 			}
		 			//是否服从工作职位的调配
		 			$("#isPositionArrange").text(is_positionArrange(resume[0].isPositionArrange));
		 			//是否服从工作城市的调配
		 			$("#is-workplace-arrange").text(is_workplaceArrange(resume[0].isWorkplaceArrange));
		 			
		 			//个人履历 start
		 			var html="";
		 			html+='<caption>个人履历</caption>';
		 			html+='<thead><tr><th>是否有补考或重修记录</th><th>是否受过处分</th></tr></thead>';
		 			html+='<tbody><tr>';
		 			if(resume[0].retestTimes==null || resume[0].retestTimes==""){
		 				html+='<td id="is-retest">无</td>';
		 			}else{
		 				html+='<td id="is-retest">'+resume[0].retestTimes+' 次</td>';
		 			}
		 			if(resume[0].punishmentTimes==null || resume[0].punishmentTimes==""){
		 				html+='<td id="is-punished">无</td>';
		 			}else{
		 				html+='<td id="is-punished">'+resume[0].punishmentTimes+' 次</td>';
		 			}
		 			html+='</tr></tbody>';
		 			$("#personalResume").html("");
		 			$("#personalResume").append(html);
		 			//end
		 			
		 			//个人履历之在校实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
		 				$("#recruitSchoolPractice").html("");
			 			var html2="<caption>在校实践</caption>" 
			 			for(var i=0;i<resume_school.length;i++){
			 				html2+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="">'+resume_school[i].startTime+' ~  '+resume_school[i].endTime+'</td>';
			 				html2+='<td id="">'+resume_school[i].practiceName+'</td>';
			 				html2+='</tr></tbody>';
			 				html2+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="" colspan="2">'+resume_school[i].practiceDetails+'</td>';
			 				html2+='</tr></tbody>';
			 			}
			 			$("#recruitSchoolPractice").append(html2);
		 				
		 			}
		 			//end
		 			
		 			//个人履历之社会实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
		 				$("#recruitWorkExperience").html("");
			 			var html3="<caption>社会实践</caption>" 
			 			for(var i=0;i<resume_work.length;i++){
			 				html3+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="">'+resume_work[i].startTime+' ~  '+resume_work[i].endTime+'</td>';
			 				html3+='<td id="">'+resume_work[i].experienceName+'</td>';
			 				html3+='</tr></tbody>';
			 				html3+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="" colspan="2">'+resume_work[i].experienceContent+'</td>';
			 				html3+='</tr></tbody>';
			 			}
			 			$("#recruitWorkExperience").append(html3);
		 			}
		 			//end
		 			
		 		}
		 	})
		 }
		 
		 
		 var toMail; //收件人邮箱
		var invitationTime;//邀约时间
		var recruitPlan_id;//招聘计划id
		var classroom;//面试地点
		var phoneNumber;//手机号码
		var toName;//收件人姓名
		var msgType;//短信模板类型
		var success;//发送邮件返回值
		//打开发送邮件(收件人邮箱，，短信参数，状态用来标识是否更改通知状态（true为是false为否）)
		function openEmail(mail,name,phone,recruitPlanId,state,type){
			toMail = mail;//收件人邮箱
			phoneNumber = phone;//手机号码
			//招聘计划id
			recruitPlan_id = recruitPlanId;
			toName=name;
			msgType=type;
			layer.open({
            	area: ['87%', '96%'],
				type: 2,
				shade:0.5,//不显示遮罩
				title:'发送邮件',
				resize:false, //是否允许拉伸
				content: [server_context+'campusRecruitment/mail/group_hair_email.jsp','no'],	//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
				end: function(layero, index){
					//发送邮箱成功，修改工作台通知状态
					if(success==1 && state=="work"){
						//执行面试工作台子页面修改通知状态
						updateInformWork(invitationTime,classroom);
						//清除发送邮箱返回值
						success="";
						var test = $("input[name='sProblem']:checked");
						test.each(function(){
							
								$(this).parent().find(".st-btn-em").html("<span class='glyphicon glyphicon-envelope' style='font-size: 13px;color: #6db2e3;'></span>");
							
							
							
						});
					}
				}   
			});
		}
		
		$(".leftMeun-mini").on({
					mouseover : function(){
						$(".leftMeun").addClass("meunPlay") ;
						$(".leftMeun").show();
						$(".leftMeun-zezao").show();
						$(".leftMeun-mini").hide();
					} ,
					
				}) ;
				
		$(".leftMeun-zezao").on({
					mouseover : function(){
						$(".leftMeun").hide();
						$(".leftMeun-mini").show();
						$(".leftMeun-zezao").hide();
						
					} 
					
				}) ;		

		function nameSelectButtonShow(obj){
				
				if($(obj).attr("data")=="off"){
					$(obj).addClass("active");
					$(".nameSelectButton").show();
					$(obj).attr("data","on");
				}else{
					$(".nameSelectButton").hide();
					$(obj).attr("data","off");
					$(obj).removeClass("active");
				}
				
			
			
		}
		//单个查询学生
		function selectStName(){
			var name=$("#selectStname").val();
			getClassroom(planId,name);
			layui.use('element', function(){
					//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
				  
				});
		
		}
		
    </script>
</body>
</html>
