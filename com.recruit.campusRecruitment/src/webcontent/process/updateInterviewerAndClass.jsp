<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): chenhaijiang
  - Date: 2018-09-13 14:36:40
  - Description:
-->
<head>
<title>面试官</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<%=request.getContextPath() %>/campusRecruitment/process/js/jquery-editable-select.min.js?v=1.2"></script>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/campusRecruitment/process/css/jquery-editable-select.min.css?v=1.2">
    <style type="text/css">
     #main{
    	padding:10px;
    }
    .layui-laydate{
    	top:0px!important;
    }
/*     下拉框 */
	.es-list{
		left:93.8906px!important;
	}
	#message{
			margin-left:86px;
			margin-bottom: 3px;
	}
	#message label{
		margin-left: 10px;
		margin-right: 32px;
		font-weight: 500;
	}
	/*复选框样式*/
	.checkBox{
		border: 1px solid #ccc;
		border-radius:2px;
		height: 16px;
		width: 16px;
		float: left;
		margin-top: 2px;
	}
	.background-img{
		 border-color: rgb(89,164,254);
		 background-color: rgb(89,164,254); 
		 background-image: url(<%= request.getContextPath() %>/css/layui/images/face/yes.png);
			 background-repeat: no-repeat;
			 background-size: 14px;
   			 background-position: center;	
	}
	
	/* 时间控件 */
	#layui-laydate1 .laydate-time-list>li:nth-child(1) {
		width: 50%;
	}

	#layui-laydate1 .laydate-time-list>li:nth-child(2) {
		width: 50%;
	}
		.laydate-time-list>li:nth-child(3) {
    display:none !important;
	}
	#layui-laydate1 .laydate-time-list ol li {
		width: 100%;
		padding-left: 57px;
	}
	
    </style>
</head>
<body>
	<div id="main">
		<div style="margin: 10px 0px;">
			<span id="" style="float: left;color:#606060;">面试时间:</span>
			<input type="text" class="form-control" id="interview_time" data  placeholder="yyyy-MM-dd HH:mm:ss"  style="margin-left: 24px;width: 64%;display: inline-block;">
			
		</div>
		<div style="margin: 25px 0px 18px 0px;">
			<span id="" style="float: left;color:#606060;">面试地点:</span>
			 <select  class="form-control" id="classroom" placeholder="请输入面试地点" style="margin-left: 24px;width: 64%;display: inline-block;">
	    	</select>
		</div>
		 <div id="message">
				<div class="checkBox " id="email"></div>
				<div class="font" style="margin-bottom: 10px;"><label>发送邮件通知面试官</label></div>
				<div class="checkBox " id="sms"></div>
				<div class="font"><label>发送短信通知面试官</label></div>
			</div>
	</div>


	<script type="text/javascript">
	layui.use(['layer', 'form'], function(){
	  var layer = layui.layer
	  ,form = layui.form;
	});
	layui.use('laydate', function(){
 		var laydate = layui.laydate;
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#interview_time', //指定元素
	    	type:'datetime'
		  });
	});
	
	$(function(){
		$("#interview_time").val(parent.clickdata.interviewTime+":00");//面试时间
		
		$.ajax({
  			url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryPlanClassroom.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{"recruitPlanId":parent.recruitPlanId},
  			success:function(data){
  				$(data.DataObject).each(function(i,obj){
  					$("#classroom").append("<option value='"+obj.classroom+"'>"+obj.classroom+"</option>");
  				})
  				$('#classroom').editableSelect({ //开启下拉框可编辑
		    		effects: 'slide' 
				});
  				$("#classroom").val(parent.clickdata.classroom);//面试地点
  				
  			}
		});
	})
	
	//复选框绑定一个点击事件
	$(".checkBox").click(function(){
		//获取自定义属性值
		var check = $(this).attr("data-check");
		if(check=="check"){
			//移除class元素
			$(this).removeClass("background-img");
			//替换自定义属性值
			$(this).attr("data-check","false");
		}else{
			//添加class元素
			$(this).addClass("background-img");
			//替换自定义属性值
			$(this).attr("data-check","check");
		}
	})
	
	
	
    </script>
</body>
</html>