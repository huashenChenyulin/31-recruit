<%@page import="com.eos.system.Constants"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.eos.access.http.security.config.HttpSecurityConfig"%>
<%@page import="com.eos.system.utility.StringUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<%@page pageEncoding="UTF-8"%>

<%
	String userId =  DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	
	String recruit_id = null;
	if(userId!=null||userId!="null"){
	//加密操作员
	recruit_id = com.eos.foundation.common.utils.CryptoUtil.encryptByDES(userId,null);
	
	}
	//加密招聘计划
	String planId =com.eos.foundation.common.utils.CryptoUtil.encryptByDES("no",null);
	String contextPath = StringUtil.htmlFilter(request.getContextPath());
	String url = null;
	if(userId!=null||userId!="null"){
	url = contextPath +"/campusRecruitment/process/workBench.jsp?planId="+planId+"&recruit_id="+recruit_id;
	}else{
	//操作员id不存在跳转到登入页面
	 url = contextPath + "/coframe/auth/login/login.jsp";
	
	}
	
	//协议跳转
	HttpSecurityConfig securityConfig = new HttpSecurityConfig();
   	boolean isOpenSecurity = securityConfig.isOpenSecurity();
   	if(isOpenSecurity){
   		boolean isAllInHttps = securityConfig.isAllInHttps();
   		if(!isAllInHttps){
   			String ip = securityConfig.getHost();
   			String http_port = securityConfig.getHttp_port();
   			url = "http://" + ip + ":" + http_port + url;
			String serverType = HttpSecurityConfig.INSTANCE.getServerType();
			System.out.println(serverType);
			if(!(StringUtils.equals(serverType, Constants.SERVER_TYPE_WEBLOGIC) || StringUtils.equals(serverType, Constants.SERVER_TYPE_WEBSPHERE))){
				String sessionID = session.getId();
				Cookie cookie = new Cookie("JSESSIONID", sessionID);
				cookie.setPath(contextPath);
				response.addCookie(cookie);
	   		}
		}
   	}
   	
	response.sendRedirect(url); 
%>
