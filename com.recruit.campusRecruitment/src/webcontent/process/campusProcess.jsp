<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<% 
	//招聘计划Id
	String recruitPlanId =request.getParameter("recruitPlanId");
	//招聘计划名称
	String recruitPlanName =request.getParameter("recruitPlanName");
	//项目类别
	String degreeType =request.getParameter("degreeType");
	//招聘负责人id
	String recruiterId =request.getParameter("recruiterId");
	/*获取当前人员id */ 
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	/*获取上一个页面的当前页数 */ 
	String curr =request.getParameter("curr");
	String  woorkRecruit_id =null;
	String  woorkEmpCode =null;
	if(recruitPlanId!=""&&recruitPlanId!=null){
		/* 对招聘计划ID加密 */
		  woorkRecruit_id =com.eos.foundation.common.utils.CryptoUtil.encryptByDES(recruitPlanId,null);
	}
	if(empCode!=""&&empCode!=null){
		/* 对招聘人员ID加密 */
		  woorkEmpCode =com.eos.foundation.common.utils.CryptoUtil.encryptByDES(empCode,null);
	}
 %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): chenhaijiang
  - Date: 2018-09-11 11:11:03
  - Description:
-->
<head>
<title>校招流程</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="<%=request.getContextPath() %>/campusRecruitment/process/css/campusProcess.css?v=2.1" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/campusRecruitment/process/js/campusProcess.js?v=4.1"></script>
</head>
    <style type="text/css">
    .background-img{
		 border-color: rgb(89,164,254);
		 background-color: rgb(89,164,254); 
		 background-image: url(<%= request.getContextPath() %>/css/layui/images/face/yes.png);
		 background-repeat: no-repeat;
		 background-size: 14px;
   		 background-position: center;	
		}
    </style>
<body>
	<!-- 主体页面 -->
	<div class="main">
		<!-- 顶部 -->
		<div class="top-head" align="center">
			<div class="top-left" >
				<button type="button" id="isback" class="btn back"><span style="margin-right: 5px;" class="glyphicon glyphicon-arrow-left"></span>返回</button>
			</div>
			<div class="top-right">
				<button type="button" id="iswoork" onclick="iswork()" class="btn back">工作台<span style="margin-left: 5px;" class="glyphicon glyphicon-arrow-right"></span></button>
			</div>
			<div class="top-center" align="center">
				<font class="font-left">计划名称：</font>
				<font class="font-center" id="topTitle">网站应聘（沸腾）</font>
			</div>
		</div>
		<!-- 中间标题-->
		<div class="cen-headline">
			<div class="headline vray-border" id="studentTest">
				学生笔试
			</div>
			<div class="headline" id="localeInterview">
				现场面试
			</div>
		</div>
		<!--主要内容 -->
		<div class="bot-content">
			<!-- 五个框 -->
			<div class="content_middle">
				<div class="middle middle_v2" id="currentStep1"  onclick="clickProcess(this)" data="">
					<div class="middle_numble middle_num1">0</div><div class="middle_link link1">应聘</div>
				</div>
				<div class="middle" id="currentStep2" onclick="clickProcess(this)" data="1">
					<div class="middle_numble middle_num2">0</div><div class="middle_link link2">性格测试</div>
				</div>
				<div class="middle" id="currentStep3" onclick="clickProcess(this)" data="2">
					<div class="middle_numble middle_num3">0</div><div class="middle_link link3">逻辑测试</div>
				</div>
				<div class="middle middle_written" id="currentStep4" onclick="clickProcess(this)" data="3">
					<div class="middle_numble middle_num4">0</div><div class="middle_link link4">笔试</div>
				</div>
				<div class="middle" id="currentStep5" style="border-width: 0px;" onclick="clickProcess(this)" data="4">
					<div class="middle_numble middle_num5">0</div><div class="middle_link link5">面试邀约</div>
				</div>
			</div>
			
			<!-- 按钮列 -->
			<div class="line-btn">
				<input id="name" class="form-control import" type="text" placeholder="请输入姓名" >
				<input id="school" class="form-control import" type="text" placeholder="请输入学校">
				<input id="major" class="form-control import" type="text" placeholder="请输入专业">
				<select id="company" name="testScores" class="form-control import">
						</select>
				<div id="message">
					<div class="checkBox background-img" data-check="check" id=""></div>
					<div class="font" style="margin-bottom: 10px;float: left;"><label>隐藏已淘汰</label></div>
				</div>
				<div class="btn  select accredit" onclick="queryData()">搜索</div>
				<span class="btns-span diction"></span>
				<div class="btn btn-right weed accredit diction" onclick="weedOut()">淘汰</div>
				<div class="btn btn-right push accredit diction" onclick="pushProcess()">推送下一环节</div>
				<div class="btn btn-left interview accredit diction " onclick="forwardInterviewer()">指派面试官</div>
				<div class="btn  btn-left accredit  email diction" onclick="sendEmail()">发送通知</div>
				<span class="btns-span diction"></span>
			</div>
			
			<!-- 列表内容 -->
			<div class="div_modal">
				<table id="idTest"  height="100%" lay-filter="test">
				</table>
				<!-- 分页条 -->
				<div id="pagingToolbar"></div>
			</div>
			<a href="" id="workPage" target="_blank" style="display:block;"></a><!-- 跳转工作台页面 -->
		</div>
		<!-- 左边列表导航 -->
		<div >
			<div class="left-navigation" id="left-navigation">
				计划名称
			</div>
		</div>
		<!-- 导航内容 -->
		<div class="left-content" >
			<div class="left-text">
				<font class="planFont">计划名称</font>
			 <form class='layui-form'>
				 <div class="layui-form-item">
				    <div class="layui-input-block" id="planName">
				    </div>
				  </div>
			</form>
			</div>
			<!-- 右边的内容，空白 -->
			<div class="right-content" id="right-content">
			</div>
		</div>
		
			
	</div>
	<!-- 姓名-->
	<script type="text/html" id="candidateName">
		{{ConvetName(d.candidateName,d.processStatus)}}
	</script>
	<!-- 学校-->
	<script type="text/html" id="college">
		{{'<div class="title" title="'+d.college+'">'+d.college+'</div>'}}
	</script>
	<!-- 专业-->
	<script type="text/html" id="major">
		{{'<div class="title" title="'+d.major+'">'+d.major+'</div>'}}
	</script>
	<!-- 毕业时间-->
	<script type="text/html" id="graduationDate">
		{{'<div class="title" title="'+getDate(d.graduationDate)+'">'+getDate(d.graduationDate)+'</div>'}}
	</script>
	<!-- 申请职位-->
	<script type="text/html" id="positionName">
		{{'<div class="title" title="'+d.positionName+'">'+d.positionName+'</div>'}}
	</script>
	<!-- 面试官姓名-->
	<script type="text/html" id="recruiterName">
		{{'<div >'+converdRecruiterName(d.recruiterName,d.recruiterId,d.processStatus,d.resumeid,d.processId,d.candidateName)+'</div>'}}
	</script>
	<!-- 面试时间-->
	<script type="text/html" id="interviewTime">
		{{'<div  class="title" title="'+d.interviewTime+'">'+d.interviewTime+'</div>'}}
	</script>
	<!-- 分配教室-->
	<script type="text/html" id="classroom">
		{{'<div class="title" title="'+d.classroom+'">'+d.classroom+'</div>'}}
	</script>
	<!-- 期望公司-->
	<script type="text/html" id="exceptedCompany">
		{{'<div class="title" title="'+d.exceptedCompany+'">'+d.exceptedCompany+'</div>'}}
	</script>
	<!-- 全部计划名称-->
	<script type="text/html" id="rests_plan_name">
		{{'<div class="title" title="'+d.rests_plan_name+'">'+d.rests_plan_name+'</div>'}}
	</script>
	<!-- 全部计划环节-->
	<script type="text/html" id="rests_current_step">
		{{'<div class="title" title="'+d.rests_current_step+'">'+d.rests_current_step+'</div>'}}
	</script>
	<!-- 计划名称-->
	<script type="text/html" id="tempPlanName">
		{{'<div class="title" title="'+d.recruitPlanName+'">'+d.recruitPlanName+'</div>'}}
	</script>
	<!-- 面试官反馈-->
	<script type="text/html" id="detailInterviewAdvice">
		{{'<div >'+replaceIcon(d.detailInterviewAdvice,d.recruiterId)+'</div>'}}
	</script>
	<!-- 面试评估分-->
	<script type="text/html" id="detailTotalScores">
		{{'<div >'+replaceTotalScores(d.detailTotalScores,d.processStatus)+'</div>'}}
	</script>
	<!-- 操作-->
	<script type="text/html" id="operation">
		<a onclick="Ahref(this,<%=recruitPlanId%>,'{{d.resumeid}}',<%=empCode%>,'{{d.processId}}')" target="_blank">查看</a>
	</script>
	<!-- 环节-->
	<script type="text/html" id="processStatus">
    {{#  if(d.processStatus == 4){ }}
   		<div class="weedBck">{{d.processStatus}}</div>
  	{{#  } else { }}
   		<div >{{d.processStatus}}</div>
  	{{#  } }}
	</script>
	<!-- 已通知-->
	<script type="text/html" id="isCall">
    {{#  if(d.isCall == 1&&d.isMailandsms!=1){ }}
		<div class="title" title="已电话通知" ><i class="fa fa-phone" style="font-size:20px;color:#00a1ff;margin-top: 4px;"></i></div>
	  {{# } if(d.isCall == 2&&d.isMailandsms!=1){ }}
		<div class="title" title="未接电话" ><i class="fa fa-phone" style="font-size:20px;color:#ea2440;margin-top: 4px;"></i></div>
  	{{#  } if(d.isMailandsms == 1&&d.isCall != 1&&d.isCall != 2) { }}
		<div class="title" ><i class="fa fa-phone" style="font-size:20px;color:transparent;margin-top: 4px;margin-right: 10px;"></i><i class="fa fa-envelope" title="已发送邮件和短信" style="font-size:20px;color:#01a1ff;margin-top: 4px;"></i></div>
  	{{#  } if(d.isCall == 1&&d.isMailandsms == 1) {}}
		<div class="title"><i  title="已电话通知" class="fa fa-phone" style="font-size:20px;margin-right: 10px;margin-top: 4px;color:#00a1ff;"></i><i  title="已发送邮件和短信" class="fa fa-envelope" style="font-size:20px;color:#01a1ff"></i></div>
	{{#  } if(d.isCall == 2&&d.isMailandsms == 1) { }}
		<div class="title"><i  title="未接电话" class="fa fa-phone" style="font-size:20px;margin-right: 10px;margin-top: 4px;color:#ea2440;"></i><i  title="已发送邮件和短信" class="fa fa-envelope" style="font-size:20px;color:#01a1ff"></i></div>
	{{#  } }}
	</script>	
<script type="text/javascript">
	//招聘计划Id
	var recruitPlanId ="<%=recruitPlanId %>";
	//招聘计划名称
	var recruitPlanName ="<%=recruitPlanName %>";
	//项目类别
	var degreeType ="<%=degreeType %>";
	//招聘负责人id
	var recruiterId ="<%=recruiterId %>";
	//当前登入员id
	var empCode ="<%=empCode %>";
	//获取上一个页面的当前页数
	var curr ="<%=curr %>";
	/* 对人员工号加密 */
	var woorkEmpCode ="<%=woorkEmpCode %>";
	/* 对招聘计划ID加密 */
	var woorkRecruit_id ="<%=woorkRecruit_id %>";
</script>
	

</body>
</html>