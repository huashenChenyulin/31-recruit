<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<%@include file="/common/common.jsp"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000074
  - Date: 2018-09-11 10:10:56
  - Description:
-->
<head>
<%		
		//获取参数
		String recruitId = request.getParameter("recruit_Id");
		String studentId = request.getParameter("student_id");
		
		String planId = request.getParameter("planId");
		String ProcessId = request.getParameter("ProcessId");
 %>
<title>评价详情</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/campusRecruitment/process/reta/layui.css" />
    <style type="text/css">
    	.content{
    		padding: 5px 20px;
			width: 1345px;
			height: 100%;
			background-color: #f5f8fa;
			position: absolute;
			left: 0;
			right: 0;
			margin: 0 auto;
    	}
    	.top{
    		width: 100%;
			height: 61px;
			background-color: #e4ebf0;
			float: left;
    	}
    	.left{
    		float: left;
			width: 386px;
			overflow: scroll;
			position: absolute;
			top: 111px;
			bottom: 0;
    	
    	}
    	.right{
    		float: left;
			width: 100%;
			text-align: center;
    	
    	}
    	.bottom{
    	
    	}
    	.layui-colla-content {
		    padding: 0px;
		}
    	.item{
    		width: 100%;
			height: 100px;
			border-bottom: 1px solid #e6e6e6;
			position: relative;
			cursor: pointer;
    	}
    	.item-action{
    		border-left: 4px solid #0078ad;
    	}
    	.man{
    		width:80px;
    		height:80px;
    		background: url(img/man.png) no-repeat;
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.woman{
    		width:80px;
    		height:80px;
    		background: url(img/woman.png) no-repeat;
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.human{
    		width:80px;
    		height:80px;
    		
			background-size: 100% 100%;
			border-radius: 40px;
			margin: 10px 8px;
			float: left;
    	}
    	.st-data{
    		float: left;
    		height: 80px;
  			margin: 10px 8px;
    	}
    	.st-data-item{
    		line-height: 26px;
    		max-height: 26px;
			overflow: hidden;
			width: 230px;
    	}
    	.st-btn{
    		position: absolute;
			top: 13px;
			right: 100px;;
    	}
    	.st-btn-tn{
    		position: absolute;
			top: 12px;
			right: 2px;
			font-size: 10px;
    	}
    	.st-btn-hz{
    		position: absolute;
			top: 40px;
			right: 5px;
			font-size: 10px;
    	}
    	.st-btn-em{
    		position: absolute;
			top: 65px;
			right: 5px;
			font-size: 10px;
    	}
    	.st-information{
    		width: 100%;
			height: 138px;
			background-color: white;
			float: left;
			padding: 10px 25px;
    	}
    	.student-name{
    		line-height: 36px;
			font-size: 30px;
			height: 36px;
			color: #666;
			margin-bottom: 5px;
    	}
    	.student-data{
    		float: left;

			line-height: 26px;
			
			font-size: 14px;
			
			height: 26px;
			
			color: #666;
			
			width: 100%;
    	}
    	.student-data1{
    		float: left;
			width: 30%;
			text-align:left;
    	}
    	.student-data2{
    		float: left;
			width: 30%;
			text-align:left;
    	}
    	.student-data3{
    		float: left;
			width: 40%;
			height: 100%;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			text-align:left;
    	}
    	.layui-btn{
    		margin: 2px 26px 2px 0 !important;
			width: 68px!important;
    	}
    	.layui-btn-sm {
		
		    height: 24px !important;
		    line-height: 24px !important;
		    padding: 0 10px !important;
		    font-size: 14px !important;
		
		}
		.student-details{
			width: 100%;
			background-color: white;
			float: left;
			padding: 0px 25px 25px 25px;
			margin-top: 5px;
		}
		.reta-name{
			height: 38px;
			line-height: 38px;
			float: left;
			width: 14%;
			color: #666;
		}
		.reta{
			float: left;
		}
		.layui-rate {
		    padding: 7px 5px 8px 0;
		}
    	.evaluate{
    		float: left;
    		width: 100%;
    		margin: 2px 0px;
    	}
    	.evaluate-item{
    		float: left;
			width: 100%;
			height: 100%;
			background-color: #f5f8fa;
			border: 1px solid #e4ebf0;
			padding: 10px 15px;
			margin: 6px 0px;
    	}
    	.interviewer-name{
    		font-size: 13px;
			margin-right: 5px;
			width: 100px;
			float: left;
			height: 23px;
			line-height: 23px;
			font-weight: bold;
    	}
    	.interviewer-time{
    		float: right;
			color: #666;
			font-size: 12px;
    	}
    	.evaluate-item-tn{
    		width: 100%;
    		float: left;
    		margin-bottom: 2px;
    	}
    	.evaluate-item-state-green{
    		color: #009688;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-red{
    		color: #ff5722;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-blue{
    		color: #1e9fff;
    		margin-left: 5px;
    	}
    	.evaluate-item-state-gray{
    		color: #666666;
    		margin-left: 5px;
    	}
    	.evaluate-item-num{
    		margin-left: 15px;
    	}
    	.evaluate-item-text{
    		height: 100%;
			margin-top: 10px;
			padding-bottom:10px;
			color: #666;
			overflow: hidden;
			border-bottom: 1px dotted #bfb7b7;
			font-size: 12px;
    	}
    	.hrs{
    		height: 2px;
			border: none;
		    border-top-color: currentcolor;
		    border-top-style: none;
		    border-top-width: medium;
			border-top: 4px dotted #e4ebf0;
			margin: 20px 0;
    	}
    	.table tbody tr td, .table tbody tr th, .table tfoot tr td, .table tfoot tr th, .table thead tr td, .table thead tr th{
			border-top: 0px solid;
		}
		.table thead tr th{
			border-bottom: 1px solid #ddd;
			color: #565656;
		}
		tbody{
			color: #656565;
		}
		#content-centre{
			float: left;
			height: 100%;
			width: 100%;
		}
		#centent-bottom{
			float: left;
			height: 100%;
			width: 100%;
		}
		#centent-bottom2{
			float: left;
			height: 100%;
			width: 100%;
		}
		.checkBoxs{
			
		}
		.st-data-name{
			font-size: 16px;
			color: #2f2f2f;
		}
		.stButton{
			width: 386px;
			height: 45px;
			border: 1px solid #f0eef0;
			position: absolute;
			top: 66px;
			background: #fff;
        }
		.btn-group{
		}
		.title-name{
			font-size: 20px;
			font-weight: bold;
			height: 36px;
			line-height: 36px;
			color: #f0ad4e;
			margin-left: 10px;
			text-align: center;
			margin-top:14px;
		}
		.title-i{
			font-size: 11px;
			height: 18px;
			line-height: 18px;
			color: #eea236;
			margin-left: 10px;
			text-align: center;
		}
		.dropdown-menu{
			min-width: 90px;
		}
		.st-data-s{
			white-space: nowrap;
    		text-overflow: ellipsis;
    		overflow: hidden;
		}
		.st-data-s{
			white-space: nowrap;
    		text-overflow: ellipsis;
    		overflow: hidden;
		}
		.evaluate-bottom{
			border-bottom:0px dotted #bfb7b7;
		}
		.evaluate-item-text font{
			font-weight: bold;
			float: left;
		}
		.evaluate-item-text span{
			width:92%;
			display:inline-block;
		}
</style>   
	<script type="text/javascript" src="<%=request.getContextPath()%>/campusRecruitment/process/reta/rate.js"></script>
</head>
<body>
<div class="content">
	<div class="top">
		<p class="title-name">面试评价</p>
		
	</div>
	<div class="right">
		<div class="st-information">
			<div class="information-left" style="width: 100%;float: left;">
				<div class="student-name"></div>
				<div class="student-data ">
					<div class="student-data1"><span id="student-data-gender"></span><span id="student-data-degree"></span><span id="student-data-birthDate"></span></div><div class="student-data2" id="student-data-phone"></div><div class="student-data3">应聘职位：<span id="student-data-zw" title=""></span></div>
				</div>
				<div class="student-data">
					<div class="student-data1" id="student-data-college"></div><div class="student-data2" id="student-data-email"></div><div class="student-data3" ><span id="student-data-classroom"></span> | <span id="student-data-time"></span></div>
				</div>
				<div class="student-data">
					<div class="student-data1">当前流程：<span id="student-data-step"></span></div>
				</div>
			</div>
			
		</div>
		<div class="student-details">
			<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
			  <ul class="layui-tab-title">
			    <li >简历信息</li>
			    <li class="layui-this">评价</li>
			  </ul>
			  <div class="layui-tab-content" style="height: 100px;">
			    <div class="layui-tab-item ">
			    	<!-- 简历信息 -->
			    	<div id="content-centre">
						<table class="table">
							<caption>基本信息</caption>
							
							
							
							<thead>
								<tr>
									<th>身份证号码</th>
									<th>户口所在地</th>
									<th>现居地</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="identityNo"></td>
									<td id="birthplace"></td>
									<td id="address"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>紧急联系人姓名</th>
									<th>与紧急联系人关系</th>
									<th>紧急联系人电话</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="emergencyContact"></td>
									<td id="emergencyRelationship"></td>
									<td id="emergencyPhone"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>身高(cm)/体重(kg)</th>
									<th>英语等级</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="heightAndWeight"></td>
									<td id="english_level"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- 尾部的-->
					<div id="centent-bottom">
						<table class="table" >
							<caption>申请职位</caption>
					
							<thead>
								<tr>
									<th>期望工作城市</th>
									<th>期望薪资</th>
									<th>得知招聘信息的渠道</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="expected-address"></td>
									<td id="expected-salary"></td>
									<td id="recruit-noticed-channel"></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th>是否服从工作职位的调配</th>
									<th>是否服从工作城市的调配</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="isPositionArrange"></td>
									<td id="is-workplace-arrange"></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- 尾部的-->
					<div id="centent-bottom2">
						<table class="table" id="personalResume">
							
						</table>
						<table class="table" id="recruitSchoolPractice">
							
						</table>
						<table class="table" id="recruitWorkExperience">
							
						</table>
					</div>
			    </div>
			    <div class="layui-tab-item layui-show">
			    	<!-- 评价 -->	
			    	 <div id="evaluate-content" style="float: left;width: 100%;">
			    	
			    	 </div>
			    </div>
			  </div>
			</div> 
		</div>
		    
	</div>
	
</div>

	<script type="text/javascript">
	var layer;
	layui.use('layer', function(){
	  layer = layui.layer;
	});   
	layui.use('element', function(){
					//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
				  
				});           
	/* layui.use('element', function(){
		  var element = layui.element;
		  element.on('tab(docDemoTabBrief)', function(data){
			  console.log(this); //当前Tab标题所在的原始DOM元素
			  console.log(data.index); //得到当前Tab的所在下标
			  console.log(data.elem); //得到当前的Tab大容器
			});
		  
		}); */
		
	var planId="<%=planId %>";	/* 招聘计划id */
	
	var studentId="<%=studentId %>";/* 简历id 114364 */
	var interviewerId="<%=recruitId %>"; /* 当前登录人id 10112665*/
	var stId="";
	var evaluateId="";
	var stProcessId="<%=ProcessId %>";/* 流程id 1484 */
	var stInterviewStep="";	//当前环节
	
	var stRate1=0;
	var stRate2=0;
	var stRate3=0;
	var stRate4=0;
	var stRate5=0;
	
	var phases_id ="";//环节id
  	var stCount=0;
  	$(function(){
  		getStudent(studentId,planId,stProcessId);
		getEvaluate();	
			
  	
  	})
  	
  	
  	//切换学生
  	var badge;
  	function changeStudent(resumeId,processId,obj,phaseCode){
  		$(".item").removeClass("item-action");
  		 getStudent(resumeId,planId,processId);
  		$(obj).addClass("item-action");
  		badge=$(obj).children(".st-btn");
  		/* $(obj).children(".checkBoxs").prop("checked",true); */
  		stProcessId=processId;
  		stId=resumeId;
  		stInterviewStep=phaseCode;
  		selectStEvaluate(resumeId,processId,phaseCode);
  		getEvaluate();
  		selectResume();
  	}
  	//查询学生个人信息
  	function getStudent(resumeId,planId,processId){
  		$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryWorkbenchResume.biz.ext",
			type:"POST",
			data:{
				resumeId:resumeId,
				planId:planId,
				processId:processId
			},
			success:function(data){
				/* console.log(data); */
				$(".student-name").text(data.workbenchResume[0].candidate_name);
				$("#student-data-gender").text(getStData(data.workbenchResume[0].gender));
				$("#student-data-birthDate").text(data.workbenchResume[0].birth_date);
				$("#student-data-degree").text(getStData(data.workbenchResume[0].degree));
				$("#student-data-phone").html("<span class='glyphicon glyphicon-earphone' style='font-size: 13px;color: #6db2e3;margin-right:10px;'></span>"+data.workbenchResume[0].candidate_phone);
				$("#student-data-zw").text(data.workbenchResume[0].position_name);
				$("#student-data-zw").attr("title",data.workbenchResume[0].position_name);
				$("#student-data-college").text(data.workbenchResume[0].college+" | "+data.workbenchResume[0].major);
				$("#student-data-email").html("<span class='glyphicon glyphicon-envelope' style='font-size: 13px;color: #6db2e3;margin-right:10px;'></span>"+data.workbenchResume[0].candidate_mail);
				$("#student-data-classroom").text(data.workbenchResume[0].classroom);
				$("#student-data-time").text(getDateTimes(data.workbenchResume[0].interview_time));
				$("#student-data-step").text(getstep(data.workbenchResume[0].current_step));
				$("#btn-tj").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'1')");
				$("#btn-dd").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'2')")
				$("#btn-btj").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'3')")
				$("#btn-wdc").attr("onclick","changeOpinion("+data.workbenchResume[0].resume_id+","+data.workbenchResume[0].process_id+","+data.workbenchResume[0].current_step+",'4')")
				stProcessId=data.workbenchResume[0].process_id;
  				stId=data.workbenchResume[0].resume_id;
  				stInterviewStep=data.workbenchResume[0].current_step;
				selectResume();
			}
			});
  	
  	}
  	
  	
  	function getStData(o){
  		var i=""
  		if(o!=""&&o!=null){
  			i=o+" | "
  		}
  		return i;
  	}
  	function getstep(o){
  		var i=""
  		if(o=="5"){
  			i="小组讨论"
  		}
  		if(o=="6"){
  			i="一对一面试"
  		}
  		if(o=="7"){
  			i="部门面试"
  		}
  		if(o=="8"){
  			i="评估中心"
  		}
  		if(o=="9"){
  			i="offer"
  		}
  		return i;
  	}
  	
  	function getDateTimes(date){
        	 var curTime;
		      if(date == null || date == ""){
		        	curTime = "";
		      }else{
			        var oldTime = (new Date(''+date+'')).getTime();
					curTime = new Date(oldTime).format("yyyy-MM-dd hh:mm");
			  }
		      
		      return curTime;
        	
        }
        
        
        
        
        function getEvaluate(){
        
        	$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryCampusInterviewDetail.biz.ext",
			type:"POST",
			data:{
				interviewerId:interviewerId,
				interviewStep:stInterviewStep,
				recruitPlanId:planId,
				resumeId:studentId,
				processId:stProcessId
			},
			success:function(data){
				console.log(data);
				 $("#evaluate-content").html("");
				var html="";
				$(data.interviewDetail).each(function(i,obj){
					html+="<div class='evaluate-item'>";
					html+="<div class='evaluate-item-tn'>";
					html+="<span class='interviewer-name'>"+obj.interviewer_name+"的评价</span><span class='layui-badge-rim' style='float: left;'>"+getstep(obj.interview_step)+"</span><span class='interviewer-time'>"+getDateTimes(obj.interview_time)+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn'>";
					if(obj.suggest=="推荐"){
						html+="<span class='layui-badge-dot layui-bg-green'></span>";
						html+="<span class='evaluate-item-state-green'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="待定"){
						html+="<span class='layui-badge-dot layui-bg-blue'></span>";
						html+="<span class='evaluate-item-state-blue'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="不推荐"){
						html+="<span class='layui-badge-dot'></span>";
						html+="<span class='evaluate-item-state-red'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="未到场"){
						html+="<span class='layui-badge-dot layui-bg-black'></span>";
						html+="<span class='evaluate-item-state-gray'>"+obj.suggest+"</span>";
					}else{
						html+="<span class='layui-badge-dot layui-bg-gray'></span>";
						html+="<span class='evaluate-item-state-gray'>未评价</span>";
					}
					html+="<span class='evaluate-item-num'>总分："+obj.total_scores+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>适应力：</font><span>"+isEmpty(obj.acceptability_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>提供和接受反馈：</font><span>"+isEmpty(obj.feedback_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>创新：</font><span>"+isEmpty(obj.innovation_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>分析和解决问题：</font><span>"+isEmpty(obj.analyzing_ability_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>影响他人：</font><span>"+isEmpty(obj.affection_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text evaluate-bottom'><font>总评价：</font><span>"+isEmpty(obj.evaluate)+"</span></div>";
					html+="</div>";
				})
				$("#evaluate-content").append(html);
				
			}
			});
        }
        
		
		
		
		
		
		
		function checkboxOnclick(checkbox){
 
			if ( checkbox.checked == true){
			 	checkall();
			}else{
				clearall();
			}
		}
		//全选
		function checkall(){
	        var hobby = $("input[name='sProblem']");
	        var i;
	        for(i = 0;i < hobby.length;++i){
	            hobby[i].checked=true;
	        }
	        
	 
	    }
	    //全不选
    	function clearall(){
	        var hobby = $("input[name='sProblem']");
	        var i;
	        for(i= 0;i < hobby.length;++i){
	            hobby[i].checked=false;
	        }
	 
	    }
        
        
        //简历信息
        var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	
		//是否服从工作职位的调配
		 function is_positionArrange(i){
			var isPositionArrange="";
		 	if(i==1){
		 		isPositionArrange="是";
		 	}else{
		 		isPositionArrange="否";
		 	}
		 	return isPositionArrange;
		 } 	
		 //是否服从工作城市的调配
		 function is_workplaceArrange(i){
			var workplaceArrange="";
		 	if(i==1){
		 		workplaceArrange="是";
		 	}else{
		 		workplaceArrange="否";
		 	}
		 	return workplaceArrange;
		 } 	
		 //得知招聘信息的渠道
		 function recruitNoticedChannel(i){
			var recruitNoticedChannel="";
		 	if(i==1){
		 		recruitNoticedChannel="索菲亚官网";
		 	}else if(i==2){
		 		recruitNoticedChannel="宣讲会";
		 	}else if(i==3){
		 		recruitNoticedChannel="微信推送";
		 	}else if(i==4){
		 		recruitNoticedChannel="招聘网站";
		 	}else if(i==5){
		 		recruitNoticedChannel="其他";
		 	}
		 	return recruitNoticedChannel;
		 }
		 //英语等级
		 function englishLevel(i){
			var englishLevel="";
		 	if(i==01){
		 		englishLevel="大学英语四级";
		 	}else if(i==02){
		 		englishLevel="大学英语六级";
		 	}else if(i==03){
		 		englishLevel="专业四级";
		 	}else if(i==04){
		 		englishLevel="专业八级";
		 	}else{
		 		englishLevel="无";
		 	}
		 	return englishLevel;
		 }
		  	
		 
		 function selectResume(){
		 	var json={
		 		resumeId:stId
		 	}
		 	$.ajax({
		 		url:"com.recruit.campusRecruitment.campusManagement.resume.queryResume.biz.ext",
		 		type:"post",
		 		data:json,
		 		success:function(data){
		 			//简历表
		 			var resume=data.resume;
		 			//职位表
		 			var position=data.position;
		 			//附件表
		 			var resume_axccessory=data.resume_axccessory;
		 			//在校实践表
		 			var resume_school=data.resume_school;
		 			//社会实践表
		 			var resume_work=data.resume_work;
		 			

		 			//现居地
		 			$("#address").text(resume[0].address);
		 			
		 			//身份证
		 			$("#identityNo").text(resume[0].identityNo);
		 			//户口所在地
		 			$("#birthplace").text(resume[0].birthplace);
		 			
	 				var weig=resume[0].weight;//体重
	 				var heig=resume[0].height;//身高
		 			if(resume[0].height=="" || resume[0].height==null){
		 				//身高
		 				heig="";
		 			}
					if(resume[0].weight=="" || resume[0].weight==null){
		 				//体重
		 				weig ="";
		 			}
	 				//身高体重
	 				$("#heightAndWeight").text(heig+"/"+weig);
		 			
		 			//紧急联系人
		 			$("#emergencyContact").text(resume[0].emergencyContact);
		 			//联系人关系
		 			$("#emergencyRelationship").text(resume[0].emergencyRelationship);
		 			//联系人电话
		 			$("#emergencyPhone").text(resume[0].emergencyPhone);
		 			//英语等级
		 			$("#english_level").text(englishLevel(resume[0].englishLevel));
		 			//毕业时间
		 			$("#graduationDate").text(resume[0].graduationDate);
		 			
		 			//期望工作城市
		 			$("#expected-address").text(resume[0].exceptedWorkAreaA);
		 			//期望薪资
		 			$("#expected-salary").text(resume[0].expectedSalary);
		 			//得知招聘信息的渠道
		 			if(resume[0].recruitNoticedChannelName!="" && resume[0].recruitNoticedChannelName!=null){
		 				$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel)+"—"+resume[0].recruitNoticedChannelName);
		 			}else{
			 			$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel));
		 			}
		 			//是否服从工作职位的调配
		 			$("#isPositionArrange").text(is_positionArrange(resume[0].isPositionArrange));
		 			//是否服从工作城市的调配
		 			$("#is-workplace-arrange").text(is_workplaceArrange(resume[0].isWorkplaceArrange));
		 			
		 			//个人履历 start
		 			var html="";
		 			html+='<caption>个人履历</caption>';
		 			html+='<thead><tr><th>是否有补考或重修记录</th><th>是否受过处分</th></tr></thead>';
		 			html+='<tbody><tr>';
		 			if(resume[0].retestTimes==null || resume[0].retestTimes==""){
		 				html+='<td id="is-retest">无</td>';
		 			}else{
		 				html+='<td id="is-retest">'+resume[0].retestTimes+' 次</td>';
		 			}
		 			if(resume[0].punishmentTimes==null || resume[0].punishmentTimes==""){
		 				html+='<td id="is-punished">无</td>';
		 			}else{
		 				html+='<td id="is-punished">'+resume[0].punishmentTimes+' 次</td>';
		 			}
		 			html+='</tr></tbody>';
		 			$("#personalResume").html("");
		 			$("#personalResume").append(html);
		 			//end
		 			
		 			//个人履历之在校实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
		 				$("#recruitSchoolPractice").html("");
			 			var html2="<caption>在校实践</caption>" 
			 			for(var i=0;i<resume_school.length;i++){
			 				html2+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="">'+resume_school[i].startTime+' ~  '+resume_school[i].endTime+'</td>';
			 				html2+='<td id="">'+resume_school[i].practiceName+'</td>';
			 				html2+='</tr></tbody>';
			 				html2+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="" colspan="2">'+resume_school[i].practiceDetails+'</td>';
			 				html2+='</tr></tbody>';
			 			}
			 			$("#recruitSchoolPractice").append(html2);
		 				
		 			}
		 			//end
		 			
		 			//个人履历之社会实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
		 				$("#recruitWorkExperience").html("");
			 			var html3="<caption>社会实践</caption>" 
			 			for(var i=0;i<resume_work.length;i++){
			 				html3+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="">'+resume_work[i].startTime+' ~  '+resume_work[i].endTime+'</td>';
			 				html3+='<td id="">'+resume_work[i].experienceName+'</td>';
			 				html3+='</tr></tbody>';
			 				html3+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="" colspan="2">'+resume_work[i].experienceContent+'</td>';
			 				html3+='</tr></tbody>';
			 			}
			 			$("#recruitWorkExperience").append(html3);
		 			}
		 			//end
		 			
		 		}
		 	})
		 }
		 
		 
		 var toMail; //收件人邮箱
		var invitationTime;//邀约时间
		var recruitPlan_id;//招聘计划id
		var classroom;//面试地点
		var phoneNumber;//手机号码
		var toName;//收件人姓名
		var msgType;//短信模板类型
		var success;//发送邮件返回值
		//打开发送邮件(收件人邮箱，，短信参数，状态用来标识是否更改通知状态（true为是false为否）)
		function openEmail(mail,name,phone,recruitPlanId,state,type){
			toMail = mail;//收件人邮箱
			phoneNumber = phone;//手机号码
			//招聘计划id
			recruitPlan_id = recruitPlanId;
			toName=name;
			msgType=type;
			layer.open({
            	area: ['87%', '96%'],
				type: 2,
				shade:0.5,//不显示遮罩
				title:'发送邮件',
				resize:false, //是否允许拉伸
				content: [server_context+'campusRecruitment/mail/group_hair_email.jsp','no'],	//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
				end: function(layero, index){
					//发送邮箱成功，修改工作台通知状态
					if(success==1 && state=="work"){
						//执行面试工作台子页面修改通知状态
						updateInformWork(invitationTime,classroom);
						//清除发送邮箱返回值
						success="";
						var test = $("input[name='sProblem']:checked");
						test.each(function(){
							
								$(this).parent().find(".st-btn-em").html("<span class='glyphicon glyphicon-envelope' style='font-size: 13px;color: #6db2e3;'></span>");
							
							
							
						});
					}
				}   
			});
		}
    </script>
</body>
</html>
