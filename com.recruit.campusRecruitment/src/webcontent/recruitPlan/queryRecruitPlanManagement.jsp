<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@include file="/common/common.jsp"%>	
<%
	String URL = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_code_url","recruit_code_url");
	String PC_URL = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_code_url","recruit_code_url_PC");
	String WX_URL = com.eos.foundation.eoscommon.BusinessDictUtil.getDictName("recruit_code_url","recruit_code_url_WX");
 	//获取流程传过来的页数
 	String curr = request.getParameter("curr");
 	//获取当前的姓名
	String userName = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	//获取当前用户的工号
	String userCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	//获取当前用户的角色ID
	String roleList = (String)DataContextManager.current().getMUODataContext().getUserObject().getAttributes().get("roleList");
 %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-08-08 15:19:01
  - Description:
-->
<head>
<title>招聘计划管理页面</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<%= request.getContextPath() %>/campusRecruitment/qrcode/jquery.qrcode.min.js?v=1.0"></script>
    
<style type="text/css">
.span-btn{
	display: inline-block;
    width: 25px;
}
.layui-btn-warm{
	height: 28px;
	line-height: 24px;
	color: rgb(255, 184, 0);
	background: #fff;
	border:1px solid;
	border-color: rgb(255, 184, 0);
}
.layui-btn-warm:hover{
	color: rgb(255, 184, 0);
}
.layui-table-tips-main{
	display: none;
}
.layui-table-tips-c{
	display: none;
}
.btn-reset{
	margin-left: 15px;
	
}
</style>
</head>
<body>

<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
					<tr>
					<td style="width: 30%;">
							<label>招聘负责人:</label> 
							<input disabled="disabled" type="text" class="form-control" id="recruitName" name="recruitName" style="width: 72%;">
						</td>
						
						<td style="width: 30%;">
							<label>计划名称:</label> 
							<input type="text" class="form-control reset" id="recruitPlanName"name="recruitPlanName" style="width: 72%;">
						</td>
						
						
						<td style="width: 30%;" >
							<label>项目类别:</label> 
							<select id="degreeType"name="degreeType" style="width: 78%;" class="form-control reset">
							<option value="">请选择项目类别</option>
						</select>
						</td>
						<td>
							<button class="btn btn-warning btn-center btn-reset" type="button" id="onlyForEnterSearch" onclick="selectPosition()">搜索</button>
						</td>
					</tr>
					
					<tr>
						<td style="width: 30%;padding-top:2px">
							<label>招聘年度 &nbsp&nbsp&nbsp:</label> 
							<input type="text" class="form-control reset" id="recruitYear"name="recruitYear" style="width: 72%;">
						</td>
						
						<td style="width: 30%;padding-top:2px;" >
							<label>招聘周期:</label> 
							<select id="recruitTope"name="recruitTope" style="width: 72%;" class="form-control reset">
							<option value="">请选择招聘周期</option>
							<option value="1">春招</option>
							<option value="2">秋招</option>
						</select>
					
					   <td style="width: 30%;padding-top:2px;">
							<label>发布状态:</label> 
							<select id="isVaild"name="isVaild" class="form-control reset" style="width: 78%;">
							<option value="1">发布</option>
							<option value="2">已下架</option>
						</select>
						</td>
						<td>
							<button class="btn btn-default btn-center btn-reset" type="button" onclick="onReset()">重置</button>
							<!-- <a id="dlink"  style="display:none;"></a> -->
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel-body" style="width: 100%; height: 100%;">
		<button class="btn btn-warning" type="button" id="addPosition" onclick="addPosition()">新增招聘计划</button>
		<button class="btn btn-warning" type="button" id="btn-update" onclick="">编辑招聘计划</button>
		<button class="btn btn-warning" type="button" id="updateIsVaild" onclick="updateIsVaild()">批量下架</button>
	</div>
<table class="layui-hide" id="position" lay-filter="positionEvent"></table>
<!-- event: 'setSign'
 -->
<script type="text/html" id="checkboxTpl">
  <input type="checkbox" id="{{d.recruitPlanId}}"  value="{{d.recruitPlanId}}" title="发布" lay-filter="lockDemo" {{(d.isVaild==1)? 'checked' : ''}}  }>
</script>
<script type="text/html" id="select">
 <button class="layui-btn layui-btn-warm" onclick="entranceProcess({{d.recruitPlanId}},{{d.degreeType}},'{{d.recruitPlanName}}','{{d.recruiterId}}','{{d.recruitYear}}','{{d.recruitTope}}')" >进入</button>
</script>
<script type="text/html" id="recruitName">
<div style="text-overflow: ellipsis;overflow: hidden;" title="{{d.recruitPlanName}}">{{d.recruitPlanName}}</div>
</script>
<script type="text/html" id="recruitTopeInto">
  {{#  if(d.recruitTope === 1){ }}
    <span style="color: #52b694;">春招 </span>
  {{#  } else if(d.recruitTope === 2){ }}
	<span style="color: #f0ad4e;">秋招</span>
  {{#  } else { }}
    <span style="color: #FF5511;"><button class="btn btn-warning" type="button" onclick="updateRecruitPlan('{{d.recruitPlanId}}')">编辑</button></span>
  {{#  } }}
</script>


<script type="text/html" id="Generate">

	<div id="download" style="display:none;" ></div> 
	<button class="layui-btn layui-btn-warm" onclick="downloadFile('{{d.recruitPlanName}}','{{d.degreeType}}','{{d.recruitPlanId}}')" >下载二维码</button>

</script>
<script >
//查看招聘计划最大权限用户(sysadmin、危蕾、陈晓彤可查看所有计划)
var users = ["1","11001935","11017205"];
//获取招聘负责人角色信息
var role = '<%=roleList %>';
//招聘负责人角色信息转数组
var RoleList = role.split(",");
//获取招聘负责人姓名
var userName = '<%=userName %>';
//获取当前登录人的工号
var userCode = <%=userCode %>;
if(userName!="" && userName!='null' && userName!=null){
	//赋值招聘负责人
	$("#recruitName").val(userName);
}

//重置
function onReset(){
	$(".reset").val("");
}

var degreeType =[{id:'1',name:"菲腾项目"},{id:'2',name:"TTD项目"}];
var URL = "<%=URL %>";;
var PC_URL = "<%=PC_URL %>";
var WX_URL = "<%=WX_URL %>";
$(function(){
//下架按钮默认不可见
$("#updateIsVaild").hide();
	 	init_table();//初始化招聘计划数据
	 	// 初始化学历要求下拉框控件
	 	for(var i=0;i<degreeType.length;i++){
					$("#degreeType").append('<option value="'+degreeType[i].id+'">'+degreeType[i].name+'</option>');
		}
		layui.use(['laydate'], function(){
				  var laydate = layui.laydate;
		//初始化年度选择器
		  laydate.render({
		    elem: '#recruitYear'
		    ,type: 'year'
		  });
	   });
		//编辑计划按钮绑定一个点击事件
		$("#btn-update").click(function(){
			//编辑招聘计划
			updateRecruitPlan(recruitPlanId);
		});
	});


//进入招聘计划流程(招聘计划id,项目类别,招聘计划名称,招聘负责人id)
function entranceProcess(recruitPlanId,degreeType,recruitPlanName,recruiterId,recruitYear,recruitTope){
	//跳转流程页
	 window.location.href=basePath+"/campusRecruitment/process/campusProcess.jsp?recruitPlanId="+recruitPlanId+"&degreeType="+degreeType+"&recruitPlanName="+recruitPlanName+"&recruiterId="+recruiterId+"&curr="+page+""; 
}
//全局招聘计划id
var recruitPlanId;
//页数
var page;
//条目数
var limit = 7;
//获取流程返回的页数
var currentPage  = <%=curr %>
if(currentPage!="" && currentPage!=null){
	page =currentPage;
}
function init_table(){
		//学历要求
		var degreeType = $("#degreeType").val();
		//招聘负责人
		var recruitName = $("#recruitName").val();
		
		//招聘年度
		var recruitYear = $("#recruitYear").val();
		//招聘周期
		var recruitTope = $("#recruitTope").val();
		//debugger
		//设置查看权限
		for(var i = 0;i<RoleList.length;i++){
			if("1021"==RoleList[i]||1021==RoleList[i]){
				recruitName="";
				$("#updateIsVaild").show();
				break;
			}
		}
		//招聘计划名称
		var recruitPlanName = $("#recruitPlanName").val();
		//发布状态
		var isVaild = $("#isVaild").val();
		var json = {recruitPlanName:recruitPlanName,recruitName:recruitName,degreeType:degreeType,isVaild:isVaild,recruitTope:recruitTope,recruitYear:recruitYear};
       	$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.recruitPlan.queryRecruitPlan.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			if(limit < 7 ){
				limit=7;
			}
				layui.use('table', function(){
				var table = layui.table
  					,form = layui.form;	
			        table.render({
			            elem: '#position'
			            ,data:data.RecruitPlan
			            ,limit: limit
			            ,cols: [[
			           	 {field:'recruitYear',sort: true,event: 'setSign', width:'10%', title: '招聘年度',align:"center" }
			            ,{field:'recruitTope', sort: true,event: 'setSign',width:'10%', title: '招聘周期',align:"center",templet:"#recruitTopeInto"}		    
			            ,{field:'recruitPlanName',sort: true,event: 'setSign', width:'21%', title: '招聘计划名称' ,templet:'#recruitName'}
			            ,{field:'recruiterName', sort: true,event: 'setSign',width:'21%', title: '招聘负责人',align:"center"}		    
			            ,{field:'degreeType', sort: true,event: 'setSign',width:'10%',align:"center", title: '项目类别',templet:"<div>{{(d.degreeType==1)? '菲腾项目':(d.degreeType==2)?'TTD项目':''}}</div>" }
			            ,{field:'location',sort: true,event: 'setSign', width:'10%', title: '招聘地点',align:"center"}
			            ,{field:'supportLevel',sort: true,event: 'setSign', width:'10%', title: '协办方配合度', align:"center"}
			            ,{field:'remark',sort: true,event: 'setSign', width:'30%', title: '备注', align:"center"}
			            
			            /* ,{field:'startedTime', sort: true,event: 'setSign',width:'10%', title: '开始日期',align:&quot;center&quot;}
			            ,{field:'endTime', sort: true,event: 'setSign',width:'10%', title: '结束日期',align:&quot;center&quot;} */
			            //,{field:'lock',fixed:'right', title:'是否发布', width:'8%', align:"center",templet: '#checkboxTpl', unresize: true} 
			            ,{field:'update', sort: true,fixed:'right',title:'查看', width:'8%', templet: '#select', align:"center"}
			            ,{field:'GenerateCode',sort: true,fixed:'right', title:'下载二维码', width:'10%', templet: '#Generate', align:"center"}
			            ]]
			            ,limits:[7,20,30,40,50,60,70,80,90]
			            ,page:{
			            	//设置起始页
			            	 curr:page
			            }
			           ,done:function(res,curr,count){
			           		limit = res.data.length;
			           		//赋值页数
              				page=curr;
            			}
			            
				        });
				       	//监听单选行事件
				         table.on('tool(positionEvent)', function(obj){
				         	//赋值选择行的招聘计划id给全局的招聘计划id
						 	recruitPlanId = obj.data.recruitPlanId;
						 });
				       	
					//监听锁定操作
					  /* form.on('checkbox(lockDemo)', function(obj){
					  	var status;
					  	
					  	if(obj.elem.checked){
					  		status = 1;
					  	}else{
					  		status = 2;
					  	}
					  	
					  	var json ={status:status,recruitPlanId:obj.value};
					  	publish(json);
					    //layer.tips(this.value + ' ' + this.name + '：'+ obj.elem.checked, obj.othis);
					  }); */
			       });
		  		}
		    });
	}
	//下架
	function soldOut(json){
	
		$.ajax({
	  			url:"com.recruit.campusRecruitment.campusManagement.recruitPlan.udateRecruitPlan.biz.ext",
	  			type:"POST",
	  			data:json,
	  			async:false,
	  			success:function(data){
		  			if(typeof(data.exception) == "undefined"||data.exception==null){
		  				open(data.out1,true);
		  				//init_table();
		  			}
	  			}
  			})
	}
	//编辑招聘计划
	function updateRecruitPlan(recruitPlanId){
		//判断全局的招聘计划id不为空
		if(recruitPlanId!="" && recruitPlanId!=null){
			//弹出编辑计划页面
			var url = '<%= request.getContextPath() %>/campusRecruitment/recruitPlan/addRecruitPlan.jsp?id='+recruitPlanId;
			layer.open({
				type: 2,
				title: '修改招聘计划',
				content: url,
				area: ['780px', '550px'],
				end: function(layero, index){ 
				  //清空全局的招聘计划id
				  recruitPlanId="";
				  init_table();
				}    
			}); 
		}else{
		//提示框
			layer.open({
					type: 1
					,offset:'auto' 
					,id: 'layerDemo'
					,resize:false //是否允许拉伸
					,content: '<div style="padding: 20px 75px 5px;">请选中一条数据！</div>'
					,btn:['确定']
					,closeBtn: false//不显示关闭图标
					,btnAlign: 'c' //按钮居中
					,shade: 0 //不显示遮罩
					,yes: function(){
					    layer.closeAll();//关闭页面
				}
			});
		 }
	 }
	
	//新增职位
	function addPosition(){
		
			layer.open({
				type: 2,
				title: '新增招聘计划',
				content: '<%= request.getContextPath() %>/campusRecruitment/recruitPlan/addRecruitPlan.jsp',
				area: ['780px', '550px'],
				end: function(layero, index){ 
				  init_table();
				}    
				
			});
	}
	//搜索
	function selectPosition(){
		page = 1;
		init_table();
	} 
	
	function download(src) {
			    var $a = document.createElement('a');
			    $a.setAttribute("href", src);
			    $a.setAttribute("download", "");
 
 
			    var evObj = document.createEvent('MouseEvents');
			    evObj.initMouseEvent( 'click', true, true, window, 0, 0, 0, 0, 0, false, false, true, false, 0, null);
			    $a.dispatchEvent(evObj);
	}
	
	//提示框   content提示内容
    function open(content,status){
		layui.use('layer', function(){ 
		
  			var $ = layui.jquery, layer = layui.layer; 
  			layer.open({
			  title: '信息'
			 ,content: '<div style="text-align: center;">'+content+'</div>'
			 ,btnAlign: 'c' //按钮居中
			 ,offset:'auto'
			 ,closeBtn: false//不显示关闭图标
			 ,end: function(layero, index){ 
				  if(status){
				  	init_table();
				  }
				}    
			});
		});
	}
	function updateIsVaild(){
		var DateURL = '<%= request.getContextPath() %>/campusRecruitment/position/popupDate.jsp';
	  		layer.open({
	        type: 2
	        ,title: "下架" //显示标题栏
	        ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
	        ,btn: ['确认','取消']
	        ,closeBtn: false//不显示关闭图标
	        ,area: ['350px', '470px']
	        ,btnAlign: 'c'
	        ,content: DateURL
	        ,yes: function(index, layero){
	        	var recruitYear = layero.find("iframe")[0].contentWindow.$("#recruitYear").val();
	        	var recruitTope = layero.find("iframe")[0].contentWindow.$("#recruitTope").val();
	        	if(recruitYear!=null && recruitYear!="" && recruitTope != null && recruitTope!="" &&recruitYear!="1111" &&recruitYear!=1111){
	        	
		        	var json ={recruitYear : recruitYear,recruitTope : recruitTope};
		        	close(index);
		        	soldOut(json);
        		}else{
        			open("招聘年度和招聘周期不能为空",false);
        		
        		}
        	}
 		});
	}
	function close(index){
			layer.closeAll();//关闭页面
	}
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/campusRecruitment/recruitPlan/js/queryRecruitPlanManagement.js?v=1.1"></script>
</body>
</html>
