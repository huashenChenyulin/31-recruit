//生成二微码
function downloadFile(recruitPlanName,degreeType,recruitPlanId ){
	//二维码链接内容 
	var url = URL +"?degreeType="+degreeType+"&recruitPlanId="+recruitPlanId;
 //默认使用Canvas生成，并显示到图片   
 var qrcode= $('#download').qrcode({
    render: "canvas", // 渲染方式有table方式（IE兼容）和canvas方式
    width: 260, //宽度
    height: 260, //高度
    text: url, //内容
    typeNumber: -1,//计算模式
    correctLevel: 2,//二维码纠错级别
    background: "#ffffff",//背景颜色
    foreground: "#000000"  //二维码颜色
});
 var canvas=qrcode.find('canvas').get(0);  
 
 
 //图片导出为 png 格式
var type = 'png';
var imgData = canvas.toDataURL(type);

/**
 * 获取mimeType
 * @param  {String} type the old mime-type
 * @return the new mime-type
*/
var _fixType = function(type) {
   type = type.toLowerCase().replace(/jpg/i, 'jpeg');
   var r = type.match(/png|jpeg|bmp|gif/)[0];
   return 'image/' + r;
}; 

// 加工image data，替换mime type
imgData = imgData.replace(_fixType(type),'image/octet-stream');

/**
 * 在本地进行文件保存
 * @param  {String} data     要保存到本地的图片数据
 * @param  {String} filename 文件名
 */
var saveFile = function(data, filename){
    var save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
    save_link.href = data;
    save_link.download = filename;

    var event = document.createEvent('MouseEvents');
    event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    save_link.dispatchEvent(event);
};

// 下载后的文件名
var filename = recruitPlanName+"."+ type;
// download
saveFile(imgData,filename);
init_table();
}