<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<%@include file="/common/common.jsp"%>
	<%	
		//rootpath在/common/common.jsp中的web路径
		//文件保存路径
		String savePath = rootpath+"/QRCode/";
		String id = request.getParameter("id");
	 %>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-08-08 22:00:52
  - Description:
-->
<head>
<title>新增</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">
	.layui-form-label {
    width: 110px;
    
}
#recruitYear[readonly]{  
    background-color: #efefef;  
} 
.layui-disabled{  
    background-color: #efefef;  
}  
.layui-form{
	margin-right: 6%;
}

.layui-textarea{
	min-height: 62px;
}
.layui-unselect{
	width: 182px;
}
.div-btn{
	width: 100%;
	text-align: center;
}
#recruit-name{
	width: 182px!important;
}

</style>	
</head>
<body>
<form class="layui-form"   lay-filter="example" >
<div class="layui-form-item" style="display: none;">
    <label class="layui-form-label">id:</label>
    <div class="layui-input-block">
      <input type="text" name="recruitPlanId" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
    </div>
  </div>
 <div class="layui-form-item">
	    <div class="layui-inline">
	    <label class="layui-form-label">计划名称:</label>
	    <div class="layui-input-block">
	      <input type="text" id="recruitPlanName" name="recruitPlanName" lay-verify="required" placeholder="请输入职位名称" autocomplete="off" class="layui-input">
	    </div>
	  </div>
	  <div class="layui-inline">
	    <label class="layui-form-label">项目类别:</label>
	    <div class="layui-input-block">
	      <select id="degreeType" name="degreeType" lay-verify="required">
	        <option value="1" selected="">菲腾项目</option>
	        <option value="2">TTD项目</option>
	      </select>
	    </div>
	  </div>
  </div>
  
  <div class="layui-form-item">
  	 <div class="layui-inline">
      <label class="layui-form-label">招聘年度:</label>
      <div class="layui-input-block">
        <input type="text" name="recruitYear" id="recruitYear"  placeholder="yyyy" class="layui-input" lay-verify="required|number">
      </div>
     </div>
     <div class="layui-inline">
	    <label class="layui-form-label">招聘周期:</label>
	    <div class="layui-input-block">
	      <select id="recruitTope" name="recruitTope" lay-verify="required">
	      	<option value="" selected="">请选择招聘周期</option>
	        <option value="1">春招</option>
	        <option value="2">秋招</option>
	      </select>
	    </div>
	  </div>
    </div>
  
  	<!-- <div class="layui-form-item">
  	 <div class="layui-inline">
      <label class="layui-form-label">开始日期:</label>
      <div class="layui-input-block">
        <input type="text" name="startedTime" id="startedTime"  placeholder="yyyy-MM-dd" class="layui-input" lay-verify="required">
      </div>
     </div>
      <div class="layui-inline">
	      <label class="layui-form-label">结束日期:</label>
	      <div class="layui-input-block">
	        <input type="text" name="endDate" id="endDate"  placeholder="yyyy-MM-dd" class="layui-input" lay-verify="endDate">
	      </div>
   	 </div>
    </div> -->
  
   <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">宣讲地点:</label>
      <div class="layui-input-inline" style="width: 100px;">
        <input type="text" name="location_province" placeholder="省" autocomplete="off" class="layui-input" lay-verify="required">
      </div>
      <div class="layui-form-mid">-</div>
      <div class="layui-input-inline" style="width: 100px;">
        <input type="text" name="location_city" placeholder="市" autocomplete="off" class="layui-input" lay-verify="required">
      </div>
    </div>
  </div>
  
   <div class="layui-form-item">
    <div class="layui-inline">
	      <label class="layui-form-label">招聘负责人:</label>
	      <div class="layui-input-block">
	        <input class="layui-input getStaff" autocomplete="off" name = "recruiterName" id="recruit-name" onclick="parent.parent.getStaff(this)" placeholder="人员选择" value="" style="width: 160px;float:left" type="text" lay-verify="required">
	      </div>
   	 </div>
  </div>
  
    <div class="layui-form-item">
    <label class="layui-form-label">是否发布:</label>
    <div class="layui-input-block">
      <input type="radio" id="isVaild" name="isVaild" value="1" title="发布"checked>
      <input type="radio" id="isVaild" name="isVaild" value="2" title="下架">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">学校配合度:</label>
    <div class="layui-input-block">
      <input type="text" id="supportLevel" name="supportLevel" lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">备注:</label>
    <div class="layui-input-block">
      <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
    </div>
  </div>
	<div class="div-btn">
      <button class="layui-btn" lay-submit lay-filter="position">提交</button>
   </div>
</form>	
	<script type="text/javascript">
	$(function(){
	 	init_table();
	});
	
	function init_table(){
	var id = "<%=id %>";
	if( id ==null || id == "null" ||<%=id %>==""){
		//新增初始化
		addTable();
	}else{
		//编辑初始化
		updateTable(id)
		}
	 }
	 
	 function updateTable(id){
	 
	 	var json = {recruitPlanId:id};
		$.ajax({
  			url:"com.recruit.campusRecruitment.campusManagement.recruitPlan.queryRecruitPlan.biz.ext",
  			type:"POST",
  			data:json,
  			success:function(data){
  			 $("#recruit-name").attr("data",data.RecruitPlan[0].recruiterId);
				layui.use(['form', 'layedit', 'laydate'], function(){
				  var form = layui.form
				  ,layer = layui.layer
				  ,layedit = layui.layedit
				  ,laydate = layui.laydate;
				  
				 /*  //初始化开始日期
				  laydate.render({
				    elem: '#startedTime'
				    ,min: getNowFormatDate()
				  });
				  
				 //初始化结束日期
				  laydate.render({
				    elem: '#endDate'
				    ,min: getNowFormatDate()
				  });
				   */
		  		 $("input[name=isVaild][value=1]").attr("checked", data.RecruitPlan[0].isVaild == 1 ? true : false);
                 $("input[name=isVaild][value=2]").attr("checked", data.RecruitPlan[0].isVaild == 2 ? true : false);
                 form.render();
                 
			  //表单初始赋值
				   form.val('example', {
				    "recruitPlanId": data.RecruitPlan[0].recruitPlanId // "name": "value"
				    //招聘计划名称
				    ,"recruitPlanName": data.RecruitPlan[0].recruitPlanName
				    //学历类别.1本科2大专
				    ,"degreeType": data.RecruitPlan[0].degreeType
				    //招聘地点(省)
				    ,"location_province": data.RecruitPlan[0].location.substring(0,data.RecruitPlan[0].location.indexOf("-"))
				    //招聘地点(市)
				    ,"location_city": data.RecruitPlan[0].location.substring(data.RecruitPlan[0].location.indexOf("-")+1,data.RecruitPlan[0].location.lenght)
				    /* //招聘开始时间
				    ,"startedTime": data.RecruitPlan[0].startedTime
				    //招聘结束时间
				    ,"endDate": data.RecruitPlan[0].endTime */
				    //招聘年
				    ,"recruitYear": data.RecruitPlan[0].recruitYear
				    //招聘周期
				    ,"recruitTope": data.RecruitPlan[0].recruitTope
				    
				    //协办方配合度
				    ,"supportLevel": data.RecruitPlan[0].supportLevel
				    //备注
				    ,"remark": data.RecruitPlan[0].remark
				    //招聘负责人
				    ,"recruiterName":data.RecruitPlan[0].recruiterName
				  }) 
				  
				   if(data.RecruitPlan[0].recruitPlanId==1||data.RecruitPlan[0].recruitPlanId==2||data.RecruitPlan[0].recruitPlanId=="1"||data.RecruitPlan[0].recruitPlanId=="2"){
		             $("#recruitYear").attr("readOnly","true");
		             $("#recruitTope").attr("readOnly","true");
		             $("#recruitTope").attr("disabled","disabled");
                 }else{
					  //初始化年度选择器
					  laydate.render({
					    elem: '#recruitYear'
					    ,type: 'year'
					    
					  });
				  };
			  form.render();
			  //自定义验证
			    form.verify({
			    //验证结束时间大于开始时间
				  endDate: function(value, item){ //value：表单的值、item：表单的DOM对象
				    var startedTime = $("#startedTime").val();
				    if(value!=''&&value!=null){
				    	if(value < startedTime ){
				    	return "结束日期必须大于等于开始日期！";
				    }
				    }
				    
				  }
				}); 
			  //监听提交
			  form.on('submit(position)', function(obj){
			  		submit(obj);
			 	 });
			  })
  			}
  		})
	 
	 }
	 
	 function addTable(){
		layui.use(['form', 'layedit', 'laydate'], function(){
		  var form = layui.form
		  ,layer = layui.layer
		  ,layedit = layui.layedit
		  ,laydate = layui.laydate
		  ,$ = layui.jquery
		  ;
		  
		  //年选择器
		  laydate.render({
		    elem: '#recruitYear'
		    ,type: 'year'
		  });
		  
		  /* //初始化开始日期
		  laydate.render({
		    elem: '#startedTime'
		    ,min: getNowFormatDate()
		  });
		  
		 //初始化结束日期
		  laydate.render({
		    elem: '#endDate'
		    ,min: getNowFormatDate()
		  });
		   */
		  //监听提交
		  form.on('submit(position)', function(data){
		  	submit(data);
		  });
		  /* //自定义验证
		    form.verify({
		    //验证结束时间大于开始时间
			  endDate: function(value, item){ //value：表单的值、item：表单的DOM对象
			    var startedTime = $("#startedTime").val();
			    if(value < startedTime ){
			    	return "结束日期必须大于等于开始日期！";
			    }
			  }
			});*/  
			
		}); 
	 
}
	function open(content){
		layui.use('layer', function(){ 
		
  			var $ = layui.jquery, layer = layui.layer; 
  			layer.open({
			  title: '信息'
			 ,content: '<div style="text-align: center;">'+content+'</div>'
			 ,btnAlign: 'c' //按钮居中
			 ,offset:'auto'
			 ,closeBtn: false//不显示关闭图标
			});
		});
	}
	//获取当前时间，格式YYYY-MM-DD
    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }
	  //提交				
	 function submit(obj){
	 	var json = obj.field;
	 	json['savePath']="<%=savePath %>";
	 	//获取招聘负责人的id
	 	json['recruiterId'] = $("#recruit-name").attr("data");
	 	 $.ajax({
			  			url:"com.recruit.campusRecruitment.campusManagement.recruitPlan.insertRecruitPlan.biz.ext",
			  			type:"POST",
			  			data:json,
			  			async:false,
			  			success:function(data){
			  				close();
			  				}
		  			}); 
	 }
	function close(){
	       var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.layer.close(index);
	}
    </script>
</body>
</html>