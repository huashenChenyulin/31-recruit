<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000083
  - Date: 2018-09-11 14:40:35
  - Description:
-->
<head>
<title>群发邮件</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<%=request.getContextPath() %>/campusRecruitment/process/js/jquery-editable-select.min.js?v=1.2"></script>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/campusRecruitment/process/css/jquery-editable-select.min.css?v=1.2">
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
	<script src="<%= request.getContextPath() %>/campusRecruitment/mail/js/group_hair_email.js?v=1.0"></script>
   	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
   	<!-- 查询当前登录人的电话 -->
   	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js?v=1.0"></script>
  
   <style type="text/css">
   		#main{
   			overflow-y:auto;
   			max-height:560px;
   		}
   		#main samp{
   			color: red;
   		}
   		#main .mail{
   			width: 97.6%;
   			margin-left: 14px;
   		}
   		#main .left-content{
   			width: 20%;
   			height: 200px;
   			float: left;
   		}
   		.input{
   			margin-left: 14px;
   		}
   		#main .right-content{
   			width: 76.3%;
   			height: 100%;
   			float: left;
   			margin-left: 28px;
   		}
   		#main label {
			margin-left: 15px;
			margin-top: -1px;
		}
		.layui-layer-title{
			padding: 0 51px 0 16px;
		}
		
		#message{
			margin-left:14px;
			width: 100%;
			margin-bottom: 3px;
		}
		#message label{
			margin-left: 10px;
		}
		/*复选框样式*/
		.checkBox{
			border: 1px solid #ccc;
			border-radius:2px;
			height: 16px;
			width: 16px;
			float: left;
		}
		.background-img{
			 border-color: rgb(89,164,254);
			 background-color: rgb(89,164,254); 
			 background-image: url(<%= request.getContextPath() %>/css/layui/images/face/yes.png);
			 background-repeat: no-repeat;
			 background-size: 14px;
   			 background-position: center;	
		}
		/*按钮样式*/
		.div-btn{
			width: 100%;
			float: left;
		}
		.div-btn #send{
			width: 100px;
   			height: 34px;
    		font-size: 15px;
    		margin: 16px 0px 16px 0px;
		}
		/*附件样式*/
		#uploader .queueList{
			margin-left:14px;
			width: 100%;
		}
		#uploader .statusBar{
			width:100%;
			margin-left: 14px;
		}
		#uploader .filelist{
			margin-left: 14px;
		}
		#uploader .filelist li{
			width: 100%;
		}
		#uploader .statusBar .btns{
			left: 0px;
		}
		#uploader .statusBar .btns .uploadBtn{
			margin-left: 24px;
		}
		#upload-list{
			width:100%;
			margin-left: 12px;
		}
		#upload-list .files{
			width: 100%;
		}
		#upload-list .files .afile{
			width: 170px;
		}
		.es-list{
			margin-left: 14px;
		}
		#layui-laydate1 .laydate-time-list>li:nth-child(1) {
		width: 50%;
	}

	#layui-laydate1 .laydate-time-list>li:nth-child(2) {
		width: 50%;
	}
		.laydate-time-list>li:nth-child(3) {
    display:none !important;
}
#layui-laydate1 .laydate-time-list ol li {
		width: 100%;
		padding-left: 57px;
	}
		
  </style>
</head>
<body>
<div id="main">
<div class="org-form-group  form-group">
    <label for="name">收件人邮箱</label><samp>*</samp>
    <input type="text" class="form-control mail" id= "toMail" name ="toMail" autocomplete="off" disabled="disabled" > 
</div>
	 <div class="org-form-group form-group  ccMail">
    <label for="name">抄送人邮箱</label>
    <input type="text" class=" form-control mail" id= "ccMail" >
</div>
<div class="form-group group bccMail">
    <label for="name">密送人邮箱</label>
    <input type="text" class="form-control mail" id="bccMail" >
</div>
 <div style="margin-bottom: 15px;margin-top: -10px;margin-left: 14px;">
		<a class="addCC" data="on">添加抄送人</a><a class="addBCC" data="on">添加密送人</a>
</div>


<div class ="left-content">

<div class="form-group">
    <label for="name">邀约时间</label><samp>*</samp>
    <input type="text"  autocomplete="off" class="form-control input" id="invitationTime" name="invitationTime" placeholder="请输入邀约时间" onblur="">
</div>

<div class="form-group interviewAddress">
    <label for="name">面试地点</label>
    <select  class="form-control input" name="classroom" id="classroom" placeholder="请输入面试地点" >
	</select>
</div>

	<div class="form-group group">
    <label for="name">选择邮件模板</label><samp>*</samp>
    <select class="form-control input" id = "templateName" name = "templateName"  onchange="" >
      
    </select>
  </div>
  
  <div class="form-group group">
    <label for="name">主题</label><samp>*</samp>
    <input type="text" class="form-control input" id="title" >
  </div>
  <div id="message">
				<div class="checkBox " data-check="false"></div>
				<div class="font"><label>是否发送短信</label></div>
			</div>
  <!-- 渲染附件 -->
   <div id="upload-list" >
  	     
  	</div>
 
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、pdf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			          
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
		
			    
  </div> 
  
  <div class ="right-content">
	 <textarea class="layui-textarea" id="textarea" style="display: none;" >  </textarea>
</div>

<div class="div-btn" align="center">
	<button class="btn btn-warning btn-save" id="send">发送</button>
</div>
</div>
	<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
	<script type="text/javascript">
    	
    </script>
</body>
</html>