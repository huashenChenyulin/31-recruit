var mail = parent.toMail;//获取父页面的收件人邮箱
var name = parent.toName;//获取收件人姓名
var resumeId = parent.toResumeId;//获取收件人简历id
var degreeType = parent.toDegreeType;//获取收件人学历类别
var position = parent.toPosition;//获取收件人应聘的岗位
var phone = parent.phoneNumber;//获取父页面的手机号码
var recruitPlanId = parent.recruitPlan_id;//获取招聘计划id
var type = parent.msgType;//获取短信类型
var state =  parent.successState;//获取标识是否更改通知状态或区分哪个功能调用发送邮件
if(mail=="undefined"){
	mail="";
}
if(name=="undefined"){
	name="";
}
if(position=="undefined"){
	position="";
}
if(phone=="undefined"){
	phone="";
}
if(resumeId=="undefined"){
	resumeId="";
}
if(degreeType=="undefined"){
	degreeTyp="";
}
if(recruitPlanId=="undefined"){
	recruitPlanId="";
}
$(function(){
	//手机号码为空隐藏发送短信
	if(phone==null || phone=="" || phone==undefined){
		$("#message").hide();
		$(".checkBox ").attr("data-check","false");
		/*$(".interviewAddress").remove();*/
	}
	
	if(state=="resumeList"){
		$('#classroom').editableSelect({ //开启下拉框可编辑
   		 filter:false,
   		 effects: 'fade',
   		 duration: 200,
		});	
		//输入事件
		$('#classroom').on('input', function (e) {
			//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
			selectTempNameContent($("#templateName option:selected").text(),$(this).val(),$("#invitationTime").val(),$("#templateName").val()); 
			return false;
	     });
		
	}else{
		//根据招聘计划id查询招聘地点
		queryAddress();
	}
	
	
	//父页面的收件人邮箱不等于空
	if(mail!="" && mail!=null){
		//赋值收件人邮箱
		$("#toMail").val(mail);
		$("#toMail").attr("title",mail);
	}
	//查询校招邮件模板
	selectMailTemplate();
	//发送绑定一个点击事件
	$("#send").click(function(){
		//发送邮件方法
		send();
	})
	//复选框绑定一个点击事件
	$(".checkBox").click(function(){
		//获取自定义属性值
		var check = $(this).attr("data-check");
		if(check=="check"){
			//移除class元素
			$(this).removeClass("background-img");
			//替换自定义属性值
			$(this).attr("data-check","false");
		}else{
			//添加class元素
			$(this).addClass("background-img");
			//替换自定义属性值
			$(this).attr("data-check","check");
		}
	})
	//选中事件
	$('#templateName').on('change', function (e) {
		//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
		selectTempNameContent($("#templateName option:selected").text(),"","",$("#templateName").val()); 
	    return false;
	});
	
});
//初始化富文本编辑器
var index;
var layedit;
layui.use('layedit', function(){
layedit = layui.layedit
,$ = layui.jquery;
layedit.set({
     uploadImage: {
     url:basePath+'interpolation/file_uploadserver.jsp'
     ,type: 'post'        
     }
   });
   //构建一个默认的编辑器
  index= layedit.build('textarea',{
  	  height: 350 //设置编辑器高度
  });
});  

//查询校招邮件模板(06校招模板编码)
function selectMailTemplate(){
	var json;
	//resumeList 为校招简历列表调用发送邮件 增加一个邮件模板类型 ,true是卡片传值过来的
	if(state=="resumeList"||state=="true"){
		json = {"type":"06,07"};
	}else if(state=="1"){
		json = {"type":"06,08"};
	}else{
		json = {"type":"06"};
	}
	$.ajax({
		url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
		dataType:'json',
		data:json,
		success:function(data){
			//返回的数据长度要大于0
			if(data.mailTemplate.length>0){
				//清空元素
				$("#templateName").empty();
				//动态添加下拉框
				var option = "<option></option>";
				for(var i = 0;i<data.mailTemplate.length;i++){
					option += "<option value='"+data.mailTemplate[i].id+"'>"+data.mailTemplate[i].templateName+"</option>";	
				}
				$("#templateName").append(option);
			}
		}
	});
}

var mail_template_id="";
//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
function selectTempNameContent(tempName,classroom,time,tempId){
	mail_template_id=tempId;
	if(tempName==undefined || tempName=="" || tempName==null){
		tempName=$("#templateName").val();
	}
	if(tempName=="【正式】校招通关失败通知" || tempName =="【正式】面试失败通知"){
		//隐藏发送短信
		$("#message").hide();
		$(".checkBox ").attr("data-check","false");
	}else if(phone!=null && phone!="" && phone!=undefined && $(".checkBox ").attr("data-check")=="check"){
		//显示发送短信
		$("#message").show();
		$(".checkBox ").attr("data-check","check");
	}
	if(classroom==undefined || classroom=="" || classroom==null){
		classroom =  $("#classroom").val();
	}
	if(time==undefined || time=="" || time==null){
		//获取邀约时间
		time = $("#invitationTime").val();
	}
	//邮件模板名称不等于空才进行查询
	if(tempName!="" && tempName!=null){
	//templateName:模板名称 interviewPlace:面试地点  time:邀约时间  tag：标识更换模板中的标识  state：标识为是发送功能的模板替换 prot：ip地址 ,当前登录人手机号码
	var json = {"templateName":tempName,"interviewPlace":classroom,"time":time,"tag":"replace","state":"send","prot":basePath,"userPhone":userPhone,"userMail":userMail};
	$.ajax({
		url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
		dataType:'json',
		data:json,
		success:function(data){
			//赋值主题
			$("#title").val(data.mailTemplate[0].title);
			//给富文本编辑器赋值
  			layedit.setContent(index,data.updatePortTemplate); 
		}
	});
  }else{
	  //清空主题值
	  $("#title").val("");
	  //清空模板内容
	  layedit.setContent(index,""); 
  }
}

//根据招聘计划id查询面试地点
function queryAddress(){
	
	$.ajax({
		url:basePath+"com.recruit.campusProcessInfo.campusProcessInfo.queryPlanClassroom.biz.ext",
		type:"POST",
		datatype: "json",
		data:{"recruitPlanId":recruitPlanId},
		success:function(data){
			$(data.DataObject).each(function(i,obj){
					$("#classroom").append("<option  value='"+obj.classroom+"'>"+obj.classroom+"</option>");
				
			})
			
			$('#classroom').editableSelect({ //开启下拉框可编辑
    		 filter:false,
    		 effects: 'fade',
    		 duration: 200,
    		 
			});	
			//输入事件
			$('#classroom').on('input', function (e) {
					//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
					selectTempNameContent($("#templateName option:selected").text(),$(this).val(),$("#invitationTime").val(),$("#templateName").val()); 
					return false;
	         });
			//选中事件
			$('#classroom').on('select.editable-select', function (e) {
				//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
				selectTempNameContent($("#templateName option:selected").text(),$(this).val(),$("#invitationTime").val(),$("#templateName").val()); 
 			    return false;
 			});
		
	 }
  });
}


//发送邮件
function send(){
	//存放提示信息
	var tips;
	//获取面试地点
	var place = $("#classroom").val();
	if(place==undefined || place!="" && place!=null){
		tips = "<div style='padding: 20px 75px 0px;'>是否发送？</div>";
	}else{
		tips = "<div style='padding: 20px 75px 0px;'>面试地点未填,是否发送？</div>";
	}
	//非空验证格式验证
	validator();
	//等于false验证不通过
	if(isPass==false){
		layer.msg(arr[0]);
		return;
	}
	//获取附件元素的值
	var isFileUplod =$(".imgWrap").text();
	// 值不等于null
	if(isFileUplod!=null && isFileUplod!=""){
		layer.msg("附件未上传");
	}else{
	//提示框
	layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content:tips
		        ,closeBtn: false//不显示关闭图标
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,yes: function(index){
		         layer.close(index);//关闭页面
		         //发送邮件内容
		         sendContent();
		        }
		        ,btn2: function(index,layero){
		         layer.close(index);//关闭页面
		        }
	});
   }
}

//成功或失败回调
var success;
//发送邮件内容
function sendContent(){
	//获取邀约时间
	var invitationTime =  $("#invitationTime").val();
	//获取面试地点
	var classrooms = $("#classroom").val();
	//拼接内容
	var spellingContent = "邀约时间："+invitationTime+"；面试地点："+classrooms+"";
	
	//获取收件人邮箱
	var toMail = replaceMail($("#toMail").val());
	//获取抄送人
	var ccMail = replaceMail($("#ccMail").val());
	//获取密送人
	var bccMail =replaceMail($("#bccMail").val());
	
	//获取邮件主题
	var title = $("#title").val();
	//获取邮件内容
	var content = layedit.getContent(index);
	//获取邮件模板名称
	var tempName = $("#templateName option:selected").text();
	//附件地址
	var filesUrl="";
	//迭代元素
	$(".afile").each(function(i,obj){
		//赋值附件地址
		filesUrl+= filePath+"/"+getUrl($(obj).attr("href"))+",";
	});
	filesUrl = filesUrl.substring(0,filesUrl.length-1);
	var json ={"toMail":toMail,"bccMail":bccMail,"ccMail":ccMail,
				"toName":name,"title":title,"content":content,"filesUrl":filesUrl,
				"toPosition":position,"toResumeId":resumeId,"toDegreeType":degreeType,
				"tempName":tempName,"tempId":mail_template_id,"spellingContent":spellingContent};
	//简历列表调用发送邮件增加一个招聘计划id参数
	if(state=="resumeList"||state=="true"){
		json["toRecruitPlanId"] = recruitPlanId;
	}
	$.ajax({
		url:'com.recruit.campusProcessInfo.campusProcessInfo.sendMail.biz.ext',
		type:'POST',
		dataType:'json',
		data:json,
		async:false,
		success:function(data){
			//获取邀约时间
			var invitationTime =  $("#invitationTime").val();
			//获取面试地点
			var classrooms = $("#classroom").val();
			//获取是否发送邮件的值
	        var checkBox = $(".checkBox").attr("data-check");
	        
			if(data.success==1){
				 
		         //等于check表示发送
		         if(checkBox=="check"){
		        	//发送短信(面试时间，面试地点，手机号码，标识是否更新通知状态)
		        	 var sendMsg = sendMessage(invitationTime,classrooms,phone,type);
		        	 if(sendMsg!="SUCCESS"){
		        		//赋值父层的结果值为1
		        		parent.success = 1;
		        		parent.layer.closeAll();
		        		parent.layer.msg("邮件发送成功,短信发送失败");
		        	 }else{
		        		parent.success=data.success;
		        		parent.layer.closeAll();
		 				parent.layer.msg("发送成功");
		        	 }
		         }else{
		        	 	parent.success=data.success;
		        	 	parent.layer.closeAll();
		        	 	parent.layer.msg("发送成功");
		         }
				//获取邀约时间给父层邀约时间赋值
				parent.invitationTime = invitationTime;
				//获取面试地点给父层面试地点赋值
				parent.classroom = classrooms;
				
				
			}else{
				
				 //等于check表示发送短信
		         if(checkBox=="check"){
		        	//发送短信(面试时间，面试地点，手机号码，标识是否更新通知状态)
		        	 var sendMsg = sendMessage(invitationTime,classrooms,phone,type);
		        	 if(sendMsg!="SUCCESS"){
		        		parent.layer.closeAll();
		        		parent.layer.msg("邮件发送失败,短信发送失败");
		        	 }else{
		        		//赋值父层的结果值为1
		        		parent.success=1;
		        		parent.layer.closeAll();
		 				parent.layer.msg("邮件发送失败,短信发送成功");
		        	 }
		         }else{
		        	 	parent.layer.msg("发送失败");
		         }
				
			}
		}
	});
}


//发送短信(时间，地点，手机号码，短信模板类型)
function  sendMessage(time,classroom,phone,type){
		var msg ="";
		var json = {"time":time,"location":classroom,"phone":phone,"type":type};
		$.ajax({
			url:'com.recruit.campusProcessInfo.campusProcessInfo.sendMessage.biz.ext',
			dataType:'json',
			data:json,
			async:false,
			success:function(data){
				//解析返回值json字符串
				var json = JSON.parse(data.success);
				if(json.code==0 && json.msg=="SUCCESS"){
					parent.successMsg=1;
				}else{
					parent.successMsg=-1;
				}
				msg =json.msg;
			}
		});
		return msg;
	
	
}

//加载时间控件
layui.use('laydate', function(){
	  var date =new Date();
	  var dateTime =date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+" 09:00:00";
	  var time = layui.laydate;
	  //执行一个laydate实例
	  time.render({
	    elem: '#invitationTime',
	    min: dateString,
	    type:'datetime',
	    value:new Date(dateTime),
	    done: function(value, date, endDate){
	       //邮件模板名称
		   var templateName =  $("#templateName option:selected").text();
		   //获取面试地点
		   var classroom = $("#classroom").val();
		   if(templateName!=null && templateName!=""){
			   //根据模板名称去查询模板内容(模板名称)
			   selectTempNameContent(templateName,classroom,value,$("#templateName").val());
		   }
  		} 
	  });
});
//标识是否通过验证
var isPass=true;
//非空验证提示输出
var isPassMsg="";
//邮箱正则
var regEmail=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
//非空验证个格式验证
function validator(){
	isPassMsg="";
	isPass=true;
	arr=[];
	//获取收件人邮箱
	var toMail = $("#toMail").val();
	//获取抄送人
	var ccMail = $("#ccMail").val();
	//获取密送人
	var bccMail = $("#bccMail").val();
	//获取邀约时间
	var invitationTime = $("#invitationTime").val();
	
	//获取邮件模板名称
	var templateName = $("#templateName").val();
	//获取主题
	var title = $("#title").val();
	//收件人邮箱包含中文逗号分号替换成英文的
	toMail = replaceSymbol(toMail);
	//抄送人邮箱不为空（并包含中文逗号分号替换成英文的）
	if(ccMail!="" && ccMail!=null){
		ccMail = replaceSymbol(ccMail);
		$("#ccMail").val(ccMail);
		//邮箱格式验证
		regMail(ccMail,"#ccMail");
	}
	//密送人邮箱不为空（并包含中文逗号分号替换成英文的）
	if(bccMail!="" && bccMail!=null){
		bccMail = replaceSymbol(bccMail);
		$("#bccMail").val(bccMail);
		//邮箱格式验证
		regMail(bccMail,"#bccMail");
	}
	
	$("#toMail").val(toMail);
	//收件人邮箱验证
	if(toMail=="" || toMail==null){
		$("#toMail").css("border","1px solid red");
		isPassMsg+="请输入收件人邮箱,";
      	isPass=false;
	}else if(toMail!="" && toMail!=null){
		//邮箱格式验证
		regMail(toMail,"#toMail");
	}else{
		$("#toMail").css("border","1px solid #ccc");
	}
	
	
	
	//邀约时间验证
	if(invitationTime=="" || invitationTime==null){
		$("#invitationTime").css("border","1px solid red");
		isPassMsg+="请输入邀约时间,";
      	isPass=false;
	}else{
		$("#invitationTime").css("border","1px solid #ccc");
	}
	/*if(phone!=null && phone!="" && phone!=undefined){
	//获取面试地点
	var classroom = $("#classroom").val();
	//面试地点验证
	if(classroom=="" || classroom==null){
		$("#classroom").css("border","1px solid red");
		isPassMsg+="请输入面试地点,";
      	isPass=false;
	}else{
		$("#classroom").css("border","1px solid #ccc");
	}
 }*/
	//邮件模板名称验证
	if(templateName=="" || templateName==null){
		$("#templateName").css("border","1px solid red");
		isPassMsg+="请选择邮件模板,";
      	isPass=false;
	}else{
		$("#templateName").css("border","1px solid #ccc");
	}
	//主题验证
	if(title=="" || title==null){
		$("#title").css("border","1px solid red");
		isPassMsg+="请输入主题,";
      	isPass=false;
	}else{
		$("#title").css("border","1px solid #ccc");
	}
	arr=isPassMsg.split(",");
}
//邮箱格式验证
function regMail(mail,element){
	//将收件人字符串转换成数组
	mail= mail.split(",");
	for(var i = 0;i<mail.length;i++){
		//截取邮箱的位置
		var start = mail[i].indexOf("<")+1;
		var End = mail[i].indexOf(">");
		var subMail;
		if(start!=0){
			subMail = mail[i].substring(start,End);
		}else{
			subMail = mail[i];
		}
		//邮箱格式验证
		if(!regEmail.test(subMail)){
			$(""+element+"").css("border","1px solid red");
			isPassMsg+=mail[i]+"邮箱有误，请输入正确的邮箱格式,";
			isPass=false;
		}else{
			$(""+element+"").css("border","1px solid #ccc");
		}
	}
}
//替换符号
function replaceSymbol(val){
	val = val.replace("，",",");
	val = val.replace("；",",");
	return val;
}
//去除邮箱的中文字还有大于或小于号
function replaceMail(val){
	if(val!=null && val!=""){
		var reg=/[\u4E00-\u9FA5]/g;
		val=val.replace(reg,'');
		val=val.replace(new RegExp("<","g"),'');
		val=val.replace(new RegExp(">","g"),'');
	}
	return val;
}