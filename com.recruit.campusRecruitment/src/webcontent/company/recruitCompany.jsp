<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-22 20:42:07
  - Description:
-->
<head>
<title>公司负责人配置</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <%@include file="/common/common.jsp" %>
    <style type="text/css">
    .layui-nav{
        background-color: #FFF;
    	color:black;
    }
    .layui-nav .layui-nav-item a {
    color: black;
	}
	.layui-nav .layui-nav-item a:hover {
   
  	color: black !important; 
 	background-color:#F0F0F0; 
	}
	
    </style>
</head>
<body>

<div class="layui-side">
    <div class="layui-side-scroll">
      
		<ul class="layui-nav layui-nav-tree site-demo-nav">
	  
		  <li class="layui-nav-item layui-nav-itemed">
		  
		    <dl id = "demo1">
		    &nbsp;
		    </dl>
		  </li>
		</ul>
	</div>
</div>

<div class="layui-body layui-tab-content site-demo site-demo-body">
    <button type="button" id="insert" onclick="insertCompanyEmp(this)" data class="btn btn-primary btn-query returnEvent_select"   data-complete-text="Loading finished">添加</button>
	<button type="button" id="delete"  onclick="deleteCompanyEmp()" class="btn btn-primary btn-query returnEvent_select"   data-complete-text="Loading finished">删除</button>
	<table class="layui-hide" id="company" lay-filter="companyEvent"></table>
</div>
	<script type="text/javascript">
	
	/*
	 定义全局变量
	 */
	 //公司id
	 var companyId ="" ;
	 //公司负责人中间id
	 var recruit_company_emp_id="";
	 var EMPNAME ="";
	 $(function() {
	 	//初始化加载页面数据
	 	queryCompany();
	 });
	
	
	//查询所属公司
	function queryCompany(){
		$.ajax({
			url:'com.recruit.company.queryCompany.queryCompany.biz.ext',
			type:'POST',
			dataType:'json',
			success:function(data){
				//返回数据长度大于0
				if(data.RecruitCompany.length>0){
					var option="" ;
					//动态渲染所属公司
					for(var i = 0 ;i<data.RecruitCompany.length;i++){
						option+="<dd class='dd"+data.RecruitCompany[i].companyId+"'><a href='javascript:void(0);' onclick='queryCompanyResponsible("+data.RecruitCompany[i].companyId+")'"+">"+data.RecruitCompany[i].companyName+"</a></dd>";
					}
					$("#demo1").append(option);
					queryCompanyResponsible(data.RecruitCompany[0].companyId);
				} 
			}
		});
	}
	/* 查询公司负责人信息
	 *	id参数 公司id编码
	 */
	 var oldClass="" ;
	function queryCompanyResponsible(id){
		//初始化首条选中不改变旧dd的class样式
		if(oldClass!=""){
			$(oldClass).css("background","#FFF");
			$(oldClass).children().css("color","black");
		}
		//选中行样式变化
		var ddClass = ".dd"+id;
		oldClass = ddClass;
		
		$(ddClass).css("background","#BEBEBE");
		$(ddClass).children().css("color","#blank"); 
		
		
		//将公司id赋值给全局公司id
		companyId = id;
		var json = {id:id};
		$.ajax({
			url:'com.recruit.company.queryCompany.queryResponsible.biz.ext',
			type:'POST',
			dataType:'json',
			data:json,
			success:function(data){
			 	layui.use('table', function(){
				var table = layui.table
  					,form = layui.form;	
			        table.render({
			            elem: '#company'
			            ,data:data.Responsible
			            ,height:500
			            ,cols: [[
			           	 {field:'EMPNAME', width:'11%', title: '姓名', align:"center",event:'setSign'}
			           	,{field:'EMPID', width:'10%', title: '工号', align:"center",event:'setSign'}
			            ,{field:'sfyofficephone', width:'10%', title: '公司电话', align:"center",event:'setSign'}
			            ,{field:'sfypersonalphone', width:'10%',align:"center", title: '手机号码',event:'setSign' }
			            ,{field:'sfyofficemail', width:'15%',align:"center", title: '电子邮件' ,event:'setSign'}
			            ,{field:'ORGNAME', width:'20%', title: '所属部门',align:"center",event:'setSign'}
			            ,{field:'posiname', width:'12%', title: '职位',align:"center",event:'setSign'}
			            ,{field:'EMPSTATUS', width:'12%', title: '人员状态',align:"center",event:'setSign'}
			            ]]
			            ,page: true
			            ,limit:10
			            ,limits:[10,20,30,40,50,60,70,80,90]
				        });
			         	table.on('tool(companyEvent)', function(obj){
				  		 	 if(obj.event === 'setSign'){
				  		 	 //赋值选择行的id给全局的id
						 	 recruit_company_emp_id = obj.data.id;
						 	 EMPNAME = obj.data.EMPNAME;
				  		 	 }
				  		  });
				   });
			}
		})
	
	}
	function deleteCompanyEmp(){
		if(recruit_company_emp_id==""){
			open("请选中一条数据！");
		return;
		}
		layui.use('layer', function(){ 
  			var $ = layui.jquery, layer = layui.layer; 
  			layer.open({
			  title: '信息'
			  ,content: '<div style="text-align: center;">是否确定删除负责人：'+EMPNAME+'!</div>'
			  ,offset:'auto'
			  ,closeBtn: false//不显示关闭图标
			  ,btnAlign: 'c' //按钮居中
			 ,btn: ['确定', '取消']
			  ,yes: function(index, layero){
			  	var json = {id:recruit_company_emp_id};
			    $.ajax({
					url:'com.recruit.company.queryCompany.deleteCompanyEmp.biz.ext',
					type:'POST',
					dataType:'json',
					data:json,
					success:function(data){
						
						if(data.out1=="成功"){
							open("删除成功！");
							queryCompanyResponsible(companyId);
						}else{
							open("删除失败！");
							queryCompanyResponsible(companyId);
						}
					}
				})
			  }
			}); 
  			
  		})
		
	}
	function open(content){
		layui.use('layer', function(){ 
		
  			var $ = layui.jquery, layer = layui.layer; 
  			layer.open({
			  title: '信息'
			 ,content: '<div style="text-align: center;">'+content+'</div>'
			 ,btnAlign: 'c' //按钮居中
			 ,offset:'auto'
			 ,closeBtn: false//不显示关闭图标
			});
		});
	}
	
	function insertCompanyEmp(obj){
		layer.open({
				type: 2,
				title: '人员选择',
				content: server_context+'/coframe/tools/plugIn/selecthrorg.jsp?empId=""',
				area: ['1128px', '570px'],
				btn: ['确定', '取消'],
				yes: function(index, layero){ 
					debugger;
					var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
					    var a=iframeWin.getData();
					    var list="";
					    var list2=""
					    $(a).each(function(i,obj){
					    	if(i=="0"){
					    		list+=obj.username;
					    		list2+=obj.empid;
					    	}else{
					    		list+=","+obj.username;
					    		list2+=","+obj.empid;
					    	}
					    	
					    });
					    layer.close(layer.index);
					    if(list2!=""){
					    	var json = {empid:list2,companyId:companyId};
							$.ajax({
								url:'com.recruit.company.queryCompany.addCompanyEmp.biz.ext',
								type:'POST',
								dataType:'json',
								data:json,
								success:function(data){
									
									if(data.out1=="成功"){
									open("添加成功！");
									queryCompanyResponsible(companyId);
								}else{
									open("添加失败！");
									queryCompanyResponsible(companyId);
								}
								
								}
							});
					    }
				},
				btn2: function(index, layero){
					   layer.close(layer.index);
					  } 
			})
	}
    </script>
</body>
</html>