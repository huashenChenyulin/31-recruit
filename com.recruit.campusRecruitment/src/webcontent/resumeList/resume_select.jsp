<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-03-14 17:23:31
  - Description:
-->
<head>
<title>简历查看</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
	
	<!-- 查看简历CSS -->
	<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/campusRecruitment/css/resumes_select.css">
	
	<script type="text/javascript" src="<%= request.getContextPath() %>/campusRecruitment/resumeList/js/html2canvas.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/campusRecruitment/resumeList/js/jsPdf.debug.js"></script>
	
	<%
		//简历id
		String resumeId = request.getParameter("resumeId");
		//招聘计划id
		String recruitPlanId = request.getParameter("recruitPlanId");
		//流程id
		String ProcessId = request.getParameter("ProcessId");
		//环节编码
		String phase_code = request.getParameter("phase_code");
		
		
		String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
		String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	 %>

<style type="text/css">
	
	
	.table tbody tr td, .table tbody tr th, .table tfoot tr td, .table tfoot tr th, .table thead tr td, .table thead tr th{
		border-top: 0px solid;
	}
	.table thead tr th{
		border-bottom: 1px solid #ddd;
		color: #565656;
	}
	tbody{
		color: #656565;
	}
	
	
	#print{
		float: right;
		margin-right: 10px;
    	margin-top: 5px;
    	color: #e28603;
	}
	#print:HOVER{
		cursor: pointer;
	}
	.SFYName{
		float: right;
		margin-right: 10px;
    	margin-top: 5px;
    	display: none;
	}
	
	.caption{
		background-color: #f9f9f9;
	}
	.caption2{
		background-color: #fff;
	}
	
	#content-centre, #centent-bottom, #centent-bottom2{
		width: 100%;
	    float: left;
	    margin-left: 0px;
	}
	
	#docDemoTabBrief{
		width: 92%;
	    float: left;
	    margin-left: 4%;
	}
	
	/* 面试评价 */
	.evaluate-item{
		float: left;
		width: 100%;
		height: 100%;
		background-color: #f5f8fa;
		border: 1px solid #e4ebf0;
		padding: 10px 15px;
		margin: 6px 0px;
	}
	.evaluate-item-tn{
		width: 100%;
		float: left;
		margin-bottom: 2px;
	}
	.evaluate-item-state-green{
		color: #009688;
		margin-left: 5px;
	}
	.evaluate-item-state-red{
		color: #ff5722;
		margin-left: 5px;
	}
	.evaluate-item-state-blue{
		color: #1e9fff;
		margin-left: 5px;
	}
	.evaluate-item-state-gray{
		color: #666666;
		margin-left: 5px;
	}
	.evaluate-item-num{
		margin-left: 15px;
	}
	.evaluate-item-text{
		height: 100%;
		margin-top: 10px;
		padding-bottom:10px;
		color: #666;
		overflow: hidden;
		border-bottom: 1px dotted #bfb7b7;
		font-size: 12px;
	}
	.evaluate-bottom{
		border-bottom:0px dotted #bfb7b7;
	}
	.evaluate-item-text font{
		font-weight: bold;
		float: left;
	}
	/* 面试评价人姓名 */
	.interviewer-name{
		font-size: 13px;
		margin-right: 5px;
		width: 100px;
		float: left;
		height: 23px;
		line-height: 23px;
		font-weight: bold;
	}
	.interviewer-time{
		float: right;
		color: #666;
		font-size: 12px;
	}
</style>
</head>
<body id="pdf" style="float: left;width: 100%;">
	<div id="main">
		<!-- 左布局 -->
		<div id="main-left">
			<div id="content-top">
				<!-- 头部的个人信息 -->
				<div id="content-top-left">
					<samp  style="float: left;margin-right: 10px;"><img class="img" src="" style="width: 150px;height: 150px;"></samp>
					<table class="table" style="float: left;width: 200px;">
						<caption id="name"></caption>
						<tbody>
							<tr>
								<td><samp id="sex"></samp> <samp>|</samp> <samp
										id="degree"></samp></td>
							</tr>
							<tr>
								<td id="english_level"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="content-top-right">
					<table class="table">
						<tbody>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/date.png">
									</samp> <samp id="birthDate"></samp><samp> |</samp> <samp id="birthDate-age"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/address.png">
									</samp> <samp id="address"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/phone.png">
									</samp> <samp id="phone"></samp></td>
							</tr>
							<tr>
								<td><samp>
										<img
											src="<%= request.getContextPath() %>/Talent/images/email.png">
									</samp> <samp id="email"></samp></td>
							</tr>
						</tbody>
					</table>
				</div>
				<a id="print" class="glyphicon glyphicon-save" onclick="downPDF()">下载PDF</a>
				<samp class="SFYName"></samp>
			</div>
			
			<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" id="docDemoTabBrief">
			  <ul class="layui-tab-title">
			    <li class="layui-this">简历信息</li>
			    <li>面试评价</li>
			   
			  </ul>
			  <div class="layui-tab-content" style="height: 100px;">
			    <div class="layui-tab-item layui-show">
					
							
					<!-- 基本信息 -->
					<div id="content-centre">
						<table class="table">
							<caption class="caption">基本信息</caption>
							
							<thead>
								<tr>
									<th>毕业院校</th>
									<th>专业</th>
									<th>毕业时间</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="college"></td>
									<td id="major"></td>
									<td id="graduationDate"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>身份证号码</th>
									<th>户口所在地</th>
									<th>身高(cm)/体重(kg)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="identityNo"></td>
									<td id="birthplace"></td>
									<td id="heightAndWeight"></td>
								</tr>
							</tbody>
							
							<thead>
								<tr>
									<th>紧急联系人姓名</th>
									<th>与紧急联系人关系</th>
									<th>紧急联系人电话</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="emergencyContact"></td>
									<td id="emergencyRelationship"></td>
									<td id="emergencyPhone"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- 申请职位-->
					<div id="centent-bottom">
						<table class="table" >
							<caption class="caption">申请职位</caption>
							<thead>
								<tr>
									<th>第一申请职位</th>
									<th>第二申请职位</th>
									<th>第三申请职位</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="one-apply-position"></td>
									<td id="two-apply-position"></td>
									<td id="three-apply-position"></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th>期望工作城市(1)</th>
									<th>期望工作城市(2)</th>
									<th>期望工作城市(3)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="expected-address-One"></td>
									<td id="expected-address-Two"></td>
									<td id="expected-address-Three"></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th>期望工作城市(4)</th>
									<th>期望薪资</th>
									<th>得知招聘信息的渠道</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="expected-address-Four"></td>
									<td id="expected-salary"></td>
									<td id="recruit-noticed-channel"></td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th>是否服从工作职位的调配</th>
									<th>是否服从工作城市的调配</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td id="isPositionArrange"></td>
									<td id="is-workplace-arrange"></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<!-- 个人履历 -->
					<div id="centent-bottom2">
						<table class="table" id="personalResume">
							
						</table>
						<table class="table" id="recruitSchoolPractice">
							
						</table>
						<table class="table" id="recruitWorkExperience">
							
						</table>
					</div>
					
					
				</div>
			    <div class="layui-tab-item">
			    
			    	<!-- 评价 -->	
			    	 <div id="evaluate-content" style="float: left;width: 100%;">
			    	
			    	 </div>
					
				</div>
			    
			  </div>
			</div> 
			

		</div>
		
		<!-- 右布局 -->
		<div id="main-right">
			<div id="content_button">
				<button type="button" class="btn btn-warning" id="preview"
					name="preview" onclick="preview2()">下载简历附件</button>
			</div>
			<div id="resumes">
				<p style="font-weight: bold;margin-top: 3%;">
					附件：
				</p>
				<div id="file_upload">
					
				</div>
			</div>
			<div id="hr">
				<hr>
			</div>
	
		</div>
		
	</div>
	
	<script type="text/javascript">
	
		var resumeID=<%=resumeId %>;
		//面试官id
		var interviewerId=<%=empid %>;
		//招聘计划id
		var recruitPlanId=<%=recruitPlanId %>;
		//流程id
		var ProcessId=<%=ProcessId %>;
		//环节编码
		var phase_code=<%=phase_code %>;
		
		var name;
		
		
		layui.use('element', function(){
		  var $ = layui.jquery
		  ,element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
		  
		  element.on('tab(docDemoTabBrief)', function(data){
		  	//tap名称
			var innerText=data.elem.context.innerText;
			if(innerText!="简历信息"){
				$("#print").hide();
			}else{
				$("#print").show();
			}
		  });
		  
		});
		
		$(function(){
			
			//查询个人信息
			 selectResume();
			 
			 getEvaluate();
		})
	
		var resumeUrl="";//接收查询后的简历地址
		var resumeName="";//接收查询后的简历名
		var resumeUrl2=new Array();//简历地址改为数组
		var resumeName2=new Array();//简历名改为数组
	
		//是否服从工作职位的调配
		 function is_positionArrange(i){
			var isPositionArrange="";
		 	if(i==1){
		 		isPositionArrange="是";
		 	}else{
		 		isPositionArrange="否";
		 	}
		 	return isPositionArrange;
		 } 	
		 //是否服从工作城市的调配
		 function is_workplaceArrange(i){
			var workplaceArrange="";
		 	if(i==1){
		 		workplaceArrange="是";
		 	}else{
		 		workplaceArrange="否";
		 	}
		 	return workplaceArrange;
		 } 	
		 //得知招聘信息的渠道
		 function recruitNoticedChannel(i){
			var recruitNoticedChannel="";
		 	if(i==1){
		 		recruitNoticedChannel="索菲亚官网";
		 	}else if(i==2){
		 		recruitNoticedChannel="宣讲会";
		 	}else if(i==3){
		 		recruitNoticedChannel="微信推送";
		 	}else if(i==4){
		 		recruitNoticedChannel="招聘网站";
		 	}else if(i==5){
		 		recruitNoticedChannel="其他";
		 	}
		 	return recruitNoticedChannel;
		 }
		 //英语等级
		 function englishLevel(i){
			var englishLevel="";
		 	if(i==01){
		 		englishLevel="大学英语四级";
		 	}else if(i==02){
		 		englishLevel="大学英语六级";
		 	}else if(i==03){
		 		englishLevel="专业四级";
		 	}else if(i==04){
		 		englishLevel="专业八级";
		 	}else if(i==05){
		 		englishLevel="无";
		 	}
		 	return englishLevel;
		 }
		 //环节
		 function getstep(o){
	  		var i=""
	  		if(o=="5"){
	  			i="小组讨论"
	  		}
	  		if(o=="6"){
	  			i="一对一面试"
	  		}
	  		if(o=="7"){
	  			i="部门面试"
	  		}
	  		if(o=="8"){
	  			i="评估中心"
	  		}
	  		if(o=="9"){
	  			i="offer"
	  		}
	  		return i;
	  	}
	  	//时间
	  	function getDateTimes(date){
        	 var curTime;
		      if(date == null || date == ""){
		        	curTime = "";
		      }else{
			        var oldTime = (new Date(''+date+'')).getTime();
					curTime = new Date(oldTime).format("yyyy-MM-dd hh:mm");
			  }
		      
		      return curTime;
        	
        }
		  
		 //查询个人信息
		 function selectResume(){
		 	//var id='113753';
		 	var json={
		 		resumeId:resumeID
		 	}
		 	$.ajax({
		 		url:"com.recruit.campusRecruitment.campusManagement.resume.queryResume.biz.ext",
		 		type:"post",
		 		data:json,
		 		success:function(data){
		 			//简历表
		 			var resume=data.resume;
		 			//职位表
		 			var position=data.position;
		 			//附件表
		 			var resume_axccessory=data.resume_axccessory;
		 			//在校实践表
		 			var resume_school=data.resume_school;
		 			//社会实践表
		 			var resume_work=data.resume_work;
		 			
		 			if(resume[0].resumePictureUrl!=null && resume[0].resumePictureUrl!=""){
		 				var src="<%= request.getContextPath() %>/"+resume[0].resumePictureUrl;
			 			//个人照片
			 			$("#content-top-left img").attr("src",src);
			 			$(".img").show();
		 			}else{
		 				$(".img").hide();
		 			}
		 			
		 			name=resume[0].candidateName;
		 			
		 			//名字
		 			$("#name").text(resume[0].candidateName);
		 			//性别
		 			$("#sex").text(sexstatus(resume[0].gender));
		 			//学历
		 			$("#degree").text(educationstatus(resume[0].degree));
		 			
		 			//出生日期
		 			$("#birthDate").text(resume[0].birthDate);
		 			$("#birthDate-age").text(getAge(resume[0].birthDate));
		 			//现居地
		 			$("#address").text(resume[0].address);
		 			//电话
		 			$("#phone").text(resume[0].candidatePhone);
		 			//邮箱
		 			$("#email").text(resume[0].candidateMail);
		 			
		 			//身份证
		 			$("#identityNo").text(resume[0].identityNo);
		 			//户口所在地
		 			$("#birthplace").text(resume[0].birthplace);
		 			
	 				var weig=resume[0].weight;//体重
	 				var heig=resume[0].height;//身高
		 			if(resume[0].height=="" || resume[0].height==null){
		 				//身高
		 				heig="";
		 			}
					if(resume[0].weight=="" || resume[0].weight==null){
		 				//体重
		 				weig ="";
		 			}
	 				//身高体重
	 				$("#heightAndWeight").text(heig+"/"+weig);
		 			
		 			//紧急联系人
		 			$("#emergencyContact").text(resume[0].emergencyContact);
		 			//联系人关系
		 			$("#emergencyRelationship").text(resume[0].emergencyRelationship);
		 			//联系人电话
		 			$("#emergencyPhone").text(resume[0].emergencyPhone);
		 			//毕业学校
		 			$("#college").text(resume[0].college);
		 			//专业
		 			$("#major").text(resume[0].major);
		 			//英语等级
		 			$("#english_level").text(englishLevel(resume[0].englishLevel));
		 			//毕业时间
		 			$("#graduationDate").text(resume[0].graduationDate);
		 			
		 			//期望工作城市
		 			if(resume[0].exceptedWorkAreaA==null || resume[0].exceptedWorkAreaA==""){
		 				
		 			}else{
		 				var expected_address = resume[0].exceptedWorkAreaA.split(",");//面试官id
		 			
			 			if(expected_address.length<=0){
			 				
			 			}else{
			 				//第一申请职位
				 			$("#expected-address-One").text(expected_address[0]);
				 			if(expected_address.length>=2){
				 				//第二申请职位
				 				$("#expected-address-Two").text(expected_address[1]);
				 			}
							if(expected_address.length>=3){
				 				//第三申请职位
				 				$("#expected-address-Three").text(expected_address[2]);
				 			}
				 			if(expected_address.length==4){
				 				//第三申请职位
				 				$("#expected-address-Four").text(expected_address[3]);
				 			}
			 			}
		 			}
		 			
		 			
		 			//$("#expected-address").text(resume[0].exceptedWorkAreaA);
		 			//期望薪资
		 			$("#expected-salary").text(resume[0].expectedSalary);
		 			//得知招聘信息的渠道
		 			if(resume[0].recruitNoticedChannelName!="" && resume[0].recruitNoticedChannelName!=null){
		 				$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel)+"—"+resume[0].recruitNoticedChannelName);
		 			}else{
			 			$("#recruit-noticed-channel").text(recruitNoticedChannel(resume[0].recruitNoticedChannel));
		 			}
		 			//是否服从工作职位的调配
		 			$("#isPositionArrange").text(is_positionArrange(resume[0].isPositionArrange));
		 			//是否服从工作城市的调配
		 			$("#is-workplace-arrange").text(is_workplaceArrange(resume[0].isWorkplaceArrange));
		 			
		 			if(position.length<=0){
		 				
		 			}else{
		 				//第一申请职位
			 			$("#one-apply-position").text(position[0].position_name);
			 			if(position.length>=2){
			 				//第二申请职位
			 				$("#two-apply-position").text(position[1].position_name);
			 			}
						if(position.length==3){
			 				//第三申请职位
			 				$("#three-apply-position").text(position[2].position_name);
			 			}
		 			}
 					
		 			
		 			//个人履历 start
		 			var html="";
		 			html+='<caption class="caption">个人履历</caption>';
		 			html+='<thead><tr><th>是否有补考或重修记录</th><th>是否受过处分</th></tr></thead>';
		 			html+='<tbody><tr>';
		 			if(resume[0].retestTimes==null || resume[0].retestTimes==""){
		 				html+='<td id="is-retest">无</td>';
		 			}else{
		 				html+='<td id="is-retest">'+resume[0].retestTimes+' 次</td>';
		 			}
		 			if(resume[0].punishmentTimes==null || resume[0].punishmentTimes==""){
		 				html+='<td id="is-punished">无</td>';
		 			}else{
		 				html+='<td id="is-punished">'+resume[0].punishmentTimes+' 次</td>';
		 			}
		 			html+='</tr></tbody>';
		 			$("#personalResume").append(html);
		 			//end
		 			
		 			//个人履历之在校实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html2="<caption class='caption2'>在校实践</caption>" 
			 			for(var i=0;i<resume_school.length;i++){
			 			
			 				var startTime=resume_school[i].startTime;//开始时间
			 				var endTime=resume_school[i].endTime;//结束时间
			 				var practiceName=resume_school[i].practiceName;//单位名称
			 				var practiceDetails=resume_school[i].practiceDetails;//实习内容
			 				
			 				if(startTime==null || startTime=="" || startTime=="null"){
			 					startTime="";
			 				}
			 				if(endTime==null || endTime=="" || endTime=="null"){
			 					endTime="";
			 				}
			 				if(practiceName==null || practiceName=="" || practiceName=="null"){
			 					practiceName="";
			 				}
			 				if(practiceDetails==null || practiceDetails=="" || practiceDetails=="null"){
			 					practiceDetails="";
			 				}
			 				
			 				html2+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				
			 				html2+='<td id="">'+startTime+' ~  '+endTime+'</td>';
			 				html2+='<td id="">'+practiceName+'</td>';
			 				html2+='</tr></tbody>';
			 				html2+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html2+='<tbody><tr>';
			 				html2+='<td id="" colspan="2">'+practiceDetails+'</td>';
			 				html2+='</tr></tbody>';
			 			}
			 			$("#recruitSchoolPractice").append(html2);
		 				
		 			}
		 			//end
		 			
		 			//个人履历之社会实践 start
		 			if(resume_school.length<=0){
		 				
		 			}else{
			 			var html3="<caption class='caption2'>社会实践</caption>" 
			 			for(var i=0;i<resume_work.length;i++){
			 			
			 				var startTime=resume_work[i].startTime;//开始时间
			 				var endTime=resume_work[i].endTime;//结束时间
			 				var experienceName=resume_work[i].experienceName;//单位名称
			 				var experienceContent=resume_work[i].experienceContent;//实习内容
			 				
			 				if(startTime==null || startTime=="" || startTime=="null"){
			 					startTime="";
			 				}
			 				if(endTime==null || endTime=="" || endTime=="null"){
			 					endTime="";
			 				}
			 				if(experienceName==null || experienceName=="" || experienceName=="null"){
			 					experienceName="";
			 				}
			 				if(experienceContent==null || experienceContent=="" || experienceContent=="null"){
			 					experienceContent="";
			 				}
			 				
			 				html3+='<thead><tr><th>起始时间</th><th>单位名称</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="">'+startTime+' ~  '+endTime+'</td>';
			 				html3+='<td id="">'+experienceName+'</td>';
			 				html3+='</tr></tbody>';
			 				html3+='<thead><tr><th colspan="2">实习内容</th></tr></thead>';
			 				html3+='<tbody><tr>';
			 				html3+='<td id="" colspan="2">'+experienceContent+'</td>';
			 				html3+='</tr></tbody>';
			 			}
			 			$("#recruitWorkExperience").append(html3);
		 			}
		 			//end
		 			
		 			
		 			//附件
		 			//判断如果returnAcc对象长度小于等于0时不做操作
  				 	if(resume_axccessory.length <= 0){
						
					}else{
						for(var i=0;i<resume_axccessory.length;i++){
							var accessoryUrl1=resume_axccessory[i].accessoryUrl;//简历地址
							var accessoryUrl=basePath+resume_axccessory[i].accessoryUrl;//拼接附件路径
							var accessoryName=resume_axccessory[i].accessoryName;//简历名称
							//把简历放在所对应的DIV里面
							$("#file_upload").append("<p id='recruitAcc"+resume_axccessory[i].id+"' class='recruitAcc'>"
									+"<a href="+accessoryUrl+"  target='_blank'  title='"+accessoryName+"'>"+accessoryName+"</a></p>");
							resumeUrl+=accessoryUrl1+",";
							resumeName+=accessoryName+",";
						}
					}
					
					
		 		}
		 	})
		 }
		 
		 //面试评价
		 function getEvaluate(){
        
        	$.ajax({
			url:"com.recruit.workbench.campusWorkbench.queryCampusInterviewDetail.biz.ext",
			type:"POST",
			data:{
				interviewerId:interviewerId,
				interviewStep:phase_code,
				recruitPlanId:recruitPlanId,
				resumeId:resumeID,
				processId:ProcessId
			},
			success:function(data){
				
				 $("#evaluate-content").html("");
				var html="";
				$(data.interviewDetail).each(function(i,obj){
					html+="<div class='evaluate-item'>";
					html+="<div class='evaluate-item-tn'>";
					html+="<span class='interviewer-name'>"+obj.interviewer_name+"的评价</span><span class='layui-badge-rim' style='float: left;'>"+getstep(obj.interview_step)+"</span><span class='interviewer-time'>"+getDateTimes(obj.interview_time)+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn'>";
					if(obj.suggest=="推荐"){
						html+="<span class='layui-badge-dot layui-bg-green'></span>";
						html+="<span class='evaluate-item-state-green'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="待定"){
						html+="<span class='layui-badge-dot layui-bg-blue'></span>";
						html+="<span class='evaluate-item-state-blue'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="不推荐"){
						html+="<span class='layui-badge-dot'></span>";
						html+="<span class='evaluate-item-state-red'>"+obj.suggest+"</span>";
					}
					else if(obj.suggest=="未到场"){
						html+="<span class='layui-badge-dot layui-bg-black'></span>";
						html+="<span class='evaluate-item-state-gray'>"+obj.suggest+"</span>";
					}else{
						html+="<span class='layui-badge-dot layui-bg-gray'></span>";
						html+="<span class='evaluate-item-state-gray'>未评价</span>";
					}
					html+="<span class='evaluate-item-num'>总分："+obj.total_scores+"</span>";
					html+="</div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>适应力：</font><span>"+isEmpty(obj.acceptability_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>提供和接受反馈：</font><span>"+isEmpty(obj.feedback_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>创新：</font><span>"+isEmpty(obj.innovation_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>分析和解决问题：</font><span>"+isEmpty(obj.analyzing_ability_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text'><font>影响他人：</font><span>"+isEmpty(obj.affection_points_evaluate)+"</span></div>";
					html+="<div class='evaluate-item-tn evaluate-item-text evaluate-bottom'><font>总评价：</font><span>"+isEmpty(obj.evaluate)+"</span></div>";
					html+="</div>";
				})
				$("#evaluate-content").append(html);
				
			}
			});
        }
		 
		 
		
		 //点击下载附件需要进行的方法
	    	function preview2(){
	    		if(resumeUrl=="" || resumeUrl==null){
	    			
	    		}else{
	    			resumeUrl=resumeUrl.substring(0,resumeUrl.length-1);//去掉最后一个字符
					resumeName=resumeName.substring(0,resumeName.length-1);//去掉最后一个字符
	    			resumeUrl2=resumeUrl.split(",");//转换成数组
	    			resumeName2=resumeName.split(",");//转换成数组
	    			//循环url
	    			for(var i=0;i<resumeUrl2.length;i++){
	    				//循环文件名
	    				for(var j=i;j<resumeName2.length;j++){
	    					//简历下载
	    					download(resumeUrl2[i],resumeName2[j]);
	    					break;
	    				}
	    			}
	    			
	    		}
	    	}
	    	
	    	//简历下载
	    	function download(href, title) {
			    const a = document.createElement('a');
			    href=basePath+href;
			    a.setAttribute('href', href);
			    a.setAttribute('download', title);
			    a.click();
			};
			
		 	//打印
    	/* function printPage(){
    		$("#print").hide();
    		$("#main-right").hide();
    		$("#main-left").css("width","100%");
    		$("#content-top-left caption").css("margin-top","0px"); 
    		$("#content-top-left img").css("width","120px"); 
    		$("#content-top-left img").css("height","140px"); 
    		$("#content-top-left").css("width","62%"); 
    		window.print();
    		setTimeout(function(){  //使用  setTimeout（）方法设定定时1000毫秒
				location.reload();//页面刷新
			});
    	}  */
    	
    	
    	/*
		 * 下载PDF
		 */
		 function downPDF(){
		
			$("#print").hide();
    		$("#main-right").hide();
    		$(".layui-tab-title").hide();
    		$("#main-left").css("width","100%");
    		$("#content-top-left caption").css("margin-top","0px"); 
    		$("#content-top-left").css("width","60%");
    		$(".caption").css('color','#000');
    		
    		$("#recruitSchoolPractice caption, #recruitWorkExperience caption").css('color','#000');
    		$("#pdf").css("font-size","18px");
    		$("#content-top-left caption").css("font-size","34px");
    		$(".caption").css("font-size","24px");
    		$("#recruitSchoolPractice caption, #recruitWorkExperience caption").css("font-size","20px");
    		 
    		$("#main-left").css("background","url(<%= request.getContextPath() %>/campusRecruitment/css/索菲亚家居水印.png)");
    		$("#main-left").css("background-position","center center");
    		$("#main-left").css("background-repeat","repeat-y"); 
    		
    		$(".caption").removeClass("caption");
    		$(".caption2").removeClass("caption2");
    		
    		$(".SFYName").show();
    		$(".SFYName").text("索菲亚家居:<%=empname %>");
    		
    		<%-- //添加水印
    		watermark("索菲亚家居：<%=empname %>"); --%>
    		
			//要转成PDF的标签的范围。
		    html2canvas($('#pdf'), {  
			    onrendered: function(canvas) {           
			        
			        var contentWidth = canvas.width;
	                  var contentHeight = canvas.height;
	
	                  //一页pdf显示html页面生成的canvas高度;
	                  var pageHeight = contentWidth / 592.28 * 841.89;
	                  //未生成pdf的html页面高度
	                  var leftHeight = contentHeight*1.5-20;
	                  //pdf页面偏移
	                  var position = 0;
	                  //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
	                  var imgWidth = 595.28;
	                  var imgHeight = 592.28/contentWidth * contentHeight;
	
	                  var pageData = canvas.toDataURL('image/png');
	
	                  var pdf = new jsPDF('p', 'pt', 'a4');
	
	                  //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
	                  //当内容未超过pdf一页显示的范围，无需分页
	                  if (leftHeight < pageHeight) {
	                      pdf.addImage(pageData, 'JPEG', -20, 0, imgWidth+40, imgHeight*1.5-20);
	                  } else {
	                      while(leftHeight > 0) {
	                          pdf.addImage(pageData, 'JPEG', -20, position, imgWidth+40, imgHeight*1.5-20)
	                          leftHeight -= pageHeight;
	                          position -= 841.89;
	                          //避免添加空白页
	                          if(leftHeight > 0) {
	                              pdf.addPage();
	                          }
	                      }
	                  }
			        pdf.save(name +"-"+ new Date().getTime() + '.pdf');
			        setTimeout(function(){  //使用  setTimeout（）方法设定定时1000毫秒
						location.reload();//页面刷新
					});  
			    }  
			});  
		} 

		
		
		
		/* function watermark(settings) {
		    //默认设置
		    var defaultSettings={
		        watermark_txt:settings,
		        watermark_x:20,//水印起始位置x轴坐标
		        watermark_y:20,//水印起始位置Y轴坐标
		        watermark_rows:20,//水印行数
		        watermark_cols:20,//水印列数
		        watermark_x_space:200,//水印x轴间隔
		        watermark_y_space:200,//水印y轴间隔
		        watermark_color:'#e4e4e4',//水印字体颜色
		        watermark_alpha:0.4,//水印透明度
		        watermark_fontsize:'15px',//水印字体大小
		        watermark_font:'微软雅黑',//水印字体
		        watermark_width:210,//水印宽度
		        watermark_height:80,//水印长度
		        watermark_angle:15//水印倾斜度数
		    };
		    //采用配置项替换默认值，作用类似jquery.extend
		    if(arguments.length===1&&typeof arguments[0] ==="object" )
		    {
		        var src=arguments[0]||{};
		        for(key in src)
		        {
		            if(src[key]&&defaultSettings[key]&&src[key]===defaultSettings[key])
		                continue;
		            else if(src[key])
		                defaultSettings[key]=src[key];
		        }
		    }
		 
		    var oTemp = document.createDocumentFragment();
		 
		    //获取页面最大宽度
		    var page_width = Math.max(document.body.scrollWidth,document.body.clientWidth);
		    var cutWidth = page_width*0.0150;
		    var page_width=page_width-cutWidth;
		    //获取页面最大高度
		    var page_height = document.body.offsetHeight;
		    //var page_height = document.body.clientHeight+document.body.scrollTop;
		    //如果将水印列数设置为0，或水印列数设置过大，超过页面最大宽度，则重新计算水印列数和水印x轴间隔
		    if (defaultSettings.watermark_cols == 0 || (parseInt(defaultSettings.watermark_x + defaultSettings.watermark_width *defaultSettings.watermark_cols + defaultSettings.watermark_x_space * (defaultSettings.watermark_cols - 1)) > page_width)) {
		        defaultSettings.watermark_cols = parseInt((page_width-defaultSettings.watermark_x+defaultSettings.watermark_x_space) / (defaultSettings.watermark_width + defaultSettings.watermark_x_space));
		        defaultSettings.watermark_x_space = parseInt((page_width - defaultSettings.watermark_x - defaultSettings.watermark_width * defaultSettings.watermark_cols) / (defaultSettings.watermark_cols - 1));
		    }
		    //如果将水印行数设置为0，或水印行数设置过大，超过页面最大长度，则重新计算水印行数和水印y轴间隔
		    if (defaultSettings.watermark_rows == 0 || (parseInt(defaultSettings.watermark_y + defaultSettings.watermark_height * defaultSettings.watermark_rows + defaultSettings.watermark_y_space * (defaultSettings.watermark_rows - 1)) > page_height)) {
		        defaultSettings.watermark_rows = parseInt((defaultSettings.watermark_y_space + page_height - defaultSettings.watermark_y) / (defaultSettings.watermark_height + defaultSettings.watermark_y_space));
		        defaultSettings.watermark_y_space = parseInt(((page_height - defaultSettings.watermark_y) - defaultSettings.watermark_height * defaultSettings.watermark_rows) / (defaultSettings.watermark_rows - 1));
		    }
		    var x;
		    var y;
		    for (var i = 0; i < defaultSettings.watermark_rows; i++) {
		        y = defaultSettings.watermark_y + (defaultSettings.watermark_y_space + defaultSettings.watermark_height) * i;
		        for (var j = 0; j < defaultSettings.watermark_cols; j++) {
		            x = defaultSettings.watermark_x + (defaultSettings.watermark_width + defaultSettings.watermark_x_space) * j;
		 
		            var mask_div = document.createElement('div');
		            mask_div.id = 'mask_div' + i + j;
		            mask_div.className = 'mask_div';
		            ///三个节点
		            var span = document.createElement('div');
		            span.appendChild(document.createTextNode(defaultSettings.watermark_txt))
		            mask_div.appendChild(span);
		            //设置水印div倾斜显示
		            mask_div.style.webkitTransform = "rotate(-" + defaultSettings.watermark_angle + "deg)";
		            mask_div.style.MozTransform = "rotate(-" + defaultSettings.watermark_angle + "deg)";
		            mask_div.style.msTransform = "rotate(-" + defaultSettings.watermark_angle + "deg)";
		            mask_div.style.OTransform = "rotate(-" + defaultSettings.watermark_angle + "deg)";
		            mask_div.style.transform = "rotate(-" + defaultSettings.watermark_angle + "deg)";
		            mask_div.style.visibility = "";
		            mask_div.style.position = "absolute";
		            mask_div.style.left = x + 'px';
		            mask_div.style.top = y + 'px';
		            mask_div.style.overflow = "hidden";
		            mask_div.style.zIndex = "-99";
		            mask_div.style.pointerEvents='none';//pointer-events:none  让水印不遮挡页面的点击事件
		            //mask_div.style.border="solid #eee 1px";
		            mask_div.style.opacity = defaultSettings.watermark_alpha;
		            mask_div.style.fontSize = defaultSettings.watermark_fontsize;
		            mask_div.style.fontFamily = defaultSettings.watermark_font;
		            mask_div.style.color = defaultSettings.watermark_color;
		            mask_div.style.textAlign = "center";
		            mask_div.style.width = defaultSettings.watermark_width + 'px';
		            mask_div.style.height = defaultSettings.watermark_height + 'px';
		            mask_div.style.display = "block";
		            oTemp.appendChild(mask_div);
		        };
		    };
		    document.body.appendChild(oTemp);
		} */
		
	</script>
</body>
</html>
