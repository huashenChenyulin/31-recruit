<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 唐能
  - Date: 2018-08-08 15:19:01
  - Description:
-->
<head>
<title>校招人员信息</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">
.span-btn{
	display: inline-block;
    width: 25px;
}
.form-inline td{
	line-height: 3;
}
#send{
	width: 82px;
	height: 34px;
	margin-left: 15px;
    margin-top: 10px;
    display: none;
}
.layui-btn-warm{
	height: 28px;
	line-height: 24px;
	color: rgb(255, 184, 0);
	background: #fff;
	border:1px solid;
	border-color: rgb(255, 184, 0);
}
.layui-btn-warm:hover{
	color: rgb(255, 184, 0);
}
.panel-body{
	padding: 15px 0px 11px 15px;
}
.layui-icon-ok{
	margin-top: 6px;
}
.title{
	white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
}

</style>
</head>
<body>

<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" style="width: 100%; height: 100%;">
				<table style="width: 100%; height: 100%;">
				<tr>
						<td style="width: 24%">
							<label>招聘计划:</label> 
							<select id="recruitPlanName"name="recruitPlanName" class="form-control" style="width: 70%;">
							<option value="">请选择招聘计划</option>
							</select>
						</td>
						<td>
						<span class="span-btn"></span>
						</td>
						<td >
							<label>学校:</label> 
							<input type="text" class="form-control" id="college"name="college" style="width: 70%;">
						</td>
						<td>
						<span class="span-btn"></span>
						</td>
						<td >
							<label>姓名:</label> 
							<input type="text" class="form-control" id="name"name="name" style="width:70%;">
						</td>
						<td >
							<label>期望工作地:</label> 
							<input type="text" class="form-control" id="exceptedWorkArea"name="exceptedWorkAreaA" style="width:70%;">
						</td>
					</tr>
					
					<tr>
						<td >
							<label>项目类型:</label> 
							<select id="degreeType"name="degreeType" class="form-control" style="width: 70%;">
							<option value="">请选择项目类型</option>
							<option value="菲腾项目">菲腾项目</option>
							<option value="TTD项目">TTD项目</option>
						</select>
						</td>
						<td>
						<span class="span-btn"></span>
						</td>
						<td >
							<label>专业:</label> 
							<input type="text" class="form-control" id="major"name="major" style="width: 70%;">
						</td>
						<td>
						<span class="span-btn"></span>
						</td>
						<td >
							<label>总分:</label> 
							<select id="testScores"name="testScores" class="form-control" style="width: 70%;">
							<option value="60">请选择是否及格</option>
							<option value="71">及格</option>
							<option value="49">不及格</option>
						</select>
						</td>
						<td style="width: 26%">
							<label>期望公司:</label> 
							<select id="company"name="company" class="form-control" style="width: 70%;margin-left: 14px;">
						</td>
					</tr>
					<tr >
					
						<td colspan="6" align="center" style="padding-left: 15px;">
							<button class="btn btn-warning" type="button" id="onlyForEnterSearch" onclick="selectResume()">搜索</button>
							<span class="span-btn"></span>
							<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							<span class="span-btn"></span>
							<button class="btn btn-export" type="button" onclick="daochu()">导出Excel</button>
							<!-- <a id="dlink"  style="display:none;"></a> -->
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<button class="btn btn-warning" type="button" id="send" onclick="">发送邮件</button>
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="downloadpath" name="downloadpath"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
<table class="layui-hide" id="position" lay-filter="positionEvent"></table>
<!-- event: 'setSign'
 -->
<script type="text/html" id="checkboxTpl">
  <input type="checkbox" name="lock" value="{{d.recruitPlanId}}" title="发布" lay-filter="lockDemo" {{(d.isVaild==1)? 'checked' : ''}}  }>
</script>
<script type="text/html" id="query">
 <button class="layui-btn layui-btn-warm" onclick="queryResume({{d.resumeid}},{{d.recruitPlanId}},{{d.processId}},{{d.currentStepId}})" >查看</button>
</script>

<script type="text/html" id="positionName">
	<div class='title' title ='{{Isnull(d.positionName)}}'>{{Isnull(d.positionName)}}</div>
</script>
<script type="text/html" id="rests_plan_name">
	<div class='title' title ='{{Isnull(d.rests_plan_name)}}'>{{Isnull(d.rests_plan_name)}}</div>
</script>
<script type="text/html" id="rests_current_step">
	<div class='title' title ='{{Isnull(d.rests_current_step)}}'>{{Isnull(d.rests_current_step)}}</div>
</script>

<!-- 学历-->
	<script type="text/html" id="degree">
		{{ '<div class="title" title="'+educationstatus(d.degree)+'">'+educationstatus(d.degree)+'</div>' }}
	</script>

<script >

$(function(){
	 	// 初始化招聘计划下拉框控件
	 	selectPlan();
	 	//查询期望公司
	 	queryCompany();
	 	$("#send").click(function(){
	 		//发送邮件
	 		sendEmail();
	 	});
	 	
	});
var checkdata = ""; //复选框的数据
var studentNmae="";//学生姓名
var toResumeId="";//简历id
var toRecruitPlanId="";//招聘计划id
var toDegreeCode ="";//学历类别
var _cur_page="";
function init_table(){
		//招聘计划名称
		var recruitPlanName = $("#recruitPlanName").val();
		//学校
		var college = $("#college").val();
		//发布状态
		var name = $("#name").val();
		//项目类型
		var degreeType = $("#degreeType").val();
		//专业
		var major = $("#major").val();
		//总分
		var testScores = $("#testScores").val();
		//获取期望工作地点
		var exceptedWorkArea = $("#exceptedWorkArea").val();
		//获取期望公司
		var company = $("#company").val();
		var json = {company:company,exceptedWorkArea:exceptedWorkArea,recruitPlanName:recruitPlanName,college:college,name:name,degreeType:degreeType,major:major,testScores:testScores};
       	$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.campusRemuse.queryRemuse.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			console.log(data);
				layui.use('table', function(){
				var table = layui.table
  					,form = layui.form;	
			        table.render({
			            elem: '#position'
			            ,data:data.Resume
			            ,height:342
			            ,limit: 6
			            ,cols: [[
			             {type: 'checkbox', fixed: 'left'}
			           	,{field:'recruitPlanName', width:'18%', title: '招聘计划', align:"left"}
			           	,{field:'recruiterName', width:'11%', title: '计划负责人', align:"center"}
			           	,{field:'candidateName', width:'10%', title: '姓名', align:"center"}
			           	,{field:'college', width:'10%', title: '学校', align:"center"}
			           	,{field:'degree', width:'10%', title: '学历',templet:'#degree', align:"center"}
			           	,{field:'major', width:'10%', title: '专业', align:"center"}
			           	,{field:'positionName', width:'18%', title: '申请职位', align:"left" ,templet:'#positionName'}
			           	,{field:'exceptedWorkAreaA', width:'14%', title: '期望工作地', align:"center"}
			           	,{field:'exceptedCompany', width:'14%', title: '期望公司', align:"center"}
			            ,{field:'gender', width:'7%', title: '性别', align:"center"}
			            ,{field:'candidatePhone', width:'12%', title: '电话', align:"center"}
			            ,{field:'candidateMail', width:'13%', title: '邮箱', align:"center"}
			            ,{field:'degreeType', width:'10%',align:"center", title: '项目类型' }
			            ,{field:'currentStep', width:'10%', title: '当前通关环节',align:"center"}
			            ,{field:'rests_plan_name', width:'10%', title: '全部计划名称',align:"center",templet:'#rests_plan_name'}
			            ,{field:'rests_current_step', width:'10%', title: '全部计划环节',align:"center",templet:'#rests_current_step'}
			            ,{field:'startTime', width:'15%', title: '测试开始时间',align:"center"}
			            ,{field:'testTime', width:'15%', title: '测试完结时间',align:"center"}
			            ,{field:'testScores', width:'10%', title: '测试总分',align:"center"}
			            ,{field:'characterType', width:'10%', title: '性格类型', align:"center"}
			            ,{field:'query',fixed:'right', title:'查看', width:'10%', templet: '#query', align:"center"}
			            ]]
			            ,page: true
			            ,limits:[6,20,30,40,50,60,70,80,90]
				        ,done: function(res, curr, count){
							  _cur_page =curr;
						  }
				        });
				        table.on('checkbox', function(obj){
				        
				        	//获取当前页数据
				        	var allCheckdata=table.cache['position'];
				        	//判断是否全选，是否选中
				        	if(obj.type=="all" && obj.checked==true){
				        	  //循环遍历当前数据
				        	  	for(var i=0;i<allCheckdata.length;i++){
				        	  		
				        	  		//获取名字+邮箱
				        	  		var nameANDMail=allCheckdata[i].candidateName+"<"+allCheckdata[i].candidateMail+">,";
				        	  		//获取姓名
				        	  		var name=allCheckdata[i].candidateName+",";
				        	  		//获取简历id
				        	  		var resumeId = allCheckdata[i].resumeid+",";
				        	  		//获取招聘计划id
				        	  		var recruitPlanId = allCheckdata[i].recruitPlanId+",";
				        	  		//获取学历类别
				        	  		var degreeCode =  allCheckdata[i].degreeCode+",";
				        	  		//判断 checkdata  参数是否包含 nameANDMail
				        	  		if(checkdata.indexOf(nameANDMail)== -1){
				        	  			
				        	  			checkdata+=nameANDMail;
				        	  			studentNmae+=name;
				        	  			toResumeId+=resumeId;
				        	  			toRecruitPlanId+=recruitPlanId;
				        	  			toDegreeCode+=degreeCode;
				        	  		}
				        	  		
				        	  	}
				        	  //判断是否全选，是否取消选中
				        	}else if(obj.type=="all" && obj.checked==false){
				        		
				        		//循环遍历当前数据
				        		for(var i=0;i<allCheckdata.length;i++){
				        	  		//获取名字+邮箱
				        	  		var nameANDMail=allCheckdata[i].candidateName+"<"+allCheckdata[i].candidateMail+">,";
				        	  		//获取姓名
				        	  		var name=allCheckdata[i].candidateName+",";
				        	  		//获取简历id
				        	  		var resumeId = allCheckdata[i].resumeid+",";
				        	  		//获取招聘计划id
				        	  		var recruitPlanId = allCheckdata[i].recruitPlanId+",";
				        	  		//获取学历类别
				        	  		var degreeCode =  allCheckdata[i].degreeCode+",";
	
				        	  		//删除对应的数据
				        	  		checkdata = checkdata.split(nameANDMail).join("");
						        	studentNmae=checkdata.split(name).join("");
						        	toResumeId =checkdata.split(resumeId).join("");
						        	toRecruitPlanId=checkdata.split(recruitPlanId).join("");
						        	toDegreeCode = checkdata.split(degreeCode).join("");
				        	  	}
				        	}else{
				        	
						        if(obj.checked==false){
						        	checkdata = checkdata.split(obj.data.candidateName+"<"+obj.data.candidateMail+">,").join("");
						        	studentNmae=checkdata.split(obj.data.candidateName).join("");
						        	toResumeId = checkdata.split(obj.data.resumeid).join("");
						        	toRecruitPlanId = checkdata.split(obj.data.recruitPlanId).join("");
						        	toDegreeCode = checkdata.split(obj.data.degreeCode).join("");
						        }else{
						        	checkdata+=obj.data.candidateName+"<"+obj.data.candidateMail+">,";
						        	studentNmae+=obj.data.candidateName+",";
						        	toResumeId+=obj.data.resumeid+",";
						        	toRecruitPlanId+=obj.data.recruitPlanId+",";
						        	toDegreeCode+=obj.data.degreeCode+",";
						        }
				        	}
						});
			       });
		  		}
		    });
	}
	
		//发送邮件
		function sendEmail(){
			if(checkedBool()){
				var candidateMail ="";
				var candidateName="";
				var candidateResumeId="";
				var candidateRecruitPlanId="";
				var candidateDegreeCode="";
				/* for(var i=0;i<checkdata.data.length;i++){
			  		if(checkdata.data.length-1!=i){
			  			candidateMail +=checkdata.data[i].candidateName+"<"+ checkdata.data[i].candidateMail + ">,"; //拼接邮箱
			  		}else{
			  			candidateMail +=checkdata.data[i].candidateName+"<"+ checkdata.data[i].candidateMail +">";//邮箱
			  		}
			  	} */
			  	candidateMail = checkdata.substr(0, checkdata.length - 1);
			  	candidateName = studentNmae.substr(0, studentNmae.length - 1);
			  	candidateResumeId = toResumeId.substr(0, toResumeId.length - 1);
			  	candidateRecruitPlanId = toRecruitPlanId.substr(0, toRecruitPlanId.length - 1);
			  	candidateDegreeCode = toDegreeCode.substr(0, toDegreeCode.length - 1);
			  	//加载数据表格
			  	init_table();
			  	checkdata="";
			  	studentNmae="";
			  	toResumeId="";
			  	toRecruitPlanId="";
			  	toDegreeCode="";
				parent.openEmail(candidateMail,candidateName,null,null,null,candidateRecruitPlanId,"resumeList",candidateResumeId,candidateDegreeCode);//发送邮件，调用父层
			}else{
				layer.msg("请选择学生");
			}
			
		}
		
	//是否已勾选复选框，true：已选择，false：未选择
		function checkedBool(){
			var checkBool = false;
			if(checkdata ==""){
				checkBool =false;
			}else {
				checkBool =true;
			}
			return checkBool;
		}
	//搜索
	function selectResume(){
		//显示发送邮件按钮
		$("#send").show();
		init_table();
	} 
	
	//查询招聘计划
	function selectPlan(){
		$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.recruitPlan.queryRecruitPlan.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				var RecruitPlan = data.RecruitPlan;
				for(var i=0;i<RecruitPlan.length;i++){
					$("#recruitPlanName").append('<option value="'+RecruitPlan[i].recruitPlanName+'">'+RecruitPlan[i].recruitPlanName+'</option>');
			 	}
			}
		})
	}
	//查看简历详情
	function queryResume(id,recruitPlanId,ProcessId,phase_code){
	
		window.open("<%= request.getContextPath() %>"+"/campusRecruitment/resumeList/resume_select.jsp?resumeId="+id+"&recruitPlanId="+recruitPlanId+"&ProcessId="+ProcessId+"&phase_code="+phase_code);
	}
//导出Excal
function daochu(){
  		ExcalStats = true;
  		//招聘计划名称
		var recruitPlanName = $("#recruitPlanName").val();
		//学校
		var college = $("#college").val();
		//发布状态
		var name = $("#name").val();
		//项目类型
		var degreeType = $("#degreeType").val();
		//专业
		var major = $("#major").val();
		//总分
		var testScores = $("#testScores").val();
		//获取期望工作地点
		var exceptedWorkArea = $("#exceptedWorkArea").val();
		//获取期望公司
		var company = $("#company").val();
		var json = {company:company,exceptedWorkArea:exceptedWorkArea,recruitPlanName:recruitPlanName,college:college,name:name,degreeType:degreeType,major:major,testScores:testScores
					,head : '{"recruitPlanName":"招聘计划","recruiterName":"计划负责人","candidateName":"姓名","college":"学校","degree":"学历",'
		            	+'"major":"专业","positionName":"申请职位","exceptedWorkAreaA":"期望工作地","exceptedCompany":"期望公司","gender":"性别","candidatePhone":"电话","candidateMail":"邮箱",'
		            	+'"degreeType":"项目类型","currentStep":"当前通关环节","rests_plan_name":"全部计划名称","rests_current_step":"全部计划环节","startTime":"测试开始时间","testTime":"测试结束时间",'
		            	+'"testScores":"总分","characterType":"性格类型","recruit_noticed_channel":"招聘信息的渠道","recruit_noticed_channel_Name":"招聘信息的渠道详情"}'
		      ,sheetName : "校招简历表"
		      ,fileName : "校招简历表"
		      //web路径
		      ,filePath :filePath
		
		
		};
       	$.ajax({
       		url: 'com.recruit.campusRecruitment.campusManagement.campusRemuse.ExcalRemuse.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){
			//判断是否报错
			if(typeof(data.exception) == "undefined"||data.exception==null){
				if(ExcalStats){
		            	 //关闭导出入口
				        ExcalStats = false;
		            	//保存到表单中
		            	document.getElementById("downloadpath").value = data.downloadpath;
 						document.getElementById("fileName").value = data.file;
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.Excel.flow";
				        form.submit(); 
				        }
				} 
			}
		})
    }
 function Isnull(val){
	if(val==null){
		val="";
	}
	return val;
}

//查询期望公司
function queryCompany(){
	$.ajax({
		url:'com.recruit.campusRecruitment.campusManagement.recruitPlan.queryExpectedCompany.biz.ext',
		type:'POST',
		dataType:'json',
		success:function(data){
			//返回数据长度大于0
			if(data.Company.length>0){
				//清空元素
				$("#company").empty();
				//动态渲染期望公司
				var option = "<option value=''>请选择期望公司</option>";
				for(var i = 0 ;i<data.Company.length;i++){
					option+="<option value="+data.Company[i].dictName+">"+data.Company[i].dictName+"</option>";
				}
				$("#company").append(option);
			} 
		}
	});
}
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/campusRecruitment/recruitPlan/js/queryRecruitPlanManagement.js?v=1.0"></script>
</body>
</html>