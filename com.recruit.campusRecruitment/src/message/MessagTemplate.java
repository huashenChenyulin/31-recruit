package message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.eos.system.annotation.Bizlet;

/*短信模板*/
public class MessagTemplate {
	//返回值
	String msg ;
	
	/**
	 * @param type 模板类型（01一面邀约   02二面邀约  03 评估中心邀约  04面试安排to面试官）
	 * @param time 时间
	 * @param location 位置
	 * @param intvName 环节名称
	 * @return
	 * @throws ParseException 
	 */
	@Bizlet
	public String messagTemplate(int type,String time,String location,String intvName) throws ParseException{
		//时间显示转换
		time = strTimeTrunDate(time);
		if(type==0){
			msg ="【索菲亚全屋定制】您好，索菲亚邀您 "+time+"前往"+location+"参加测评。详细信息请查看邮件，若有疑问，请通过邮件上的联系方式与我们联系。";
		}if(type==1){
			
			msg ="【索菲亚全屋定制】您好，索菲亚邀您 "+time+"前往"+location+"参加初试。详细信息请查看邮件，若有疑问，请通过邮件上的联系方式与我们联系。";
		}else if(type==2){
			
			int year = getCurrentYear();
			msg ="【索菲亚全屋定制】您好，恭喜您通过了索菲亚集团"+year+"届校园招聘的初试，现诚邀您于"+time+"前往"+location+"参加复试，详细信息请查看邮件，若有疑问，请通过邮件上的联系方式与我们联系，期待您更好的表现。";
		}else if(type==3){
			
			int year = getCurrentYear();
			msg ="【索菲亚全屋定制】您好，恭喜您通过了索菲亚集团"+year+"届校园招聘的复试，现诚邀您于"+time+"前往"+location+"参加评估中心测评。详细信息请查看邮件，若有其他疑问，可通过邮件上的联系方式与我们联系，期待您更好的表现。";
		}else if(type==4){
			
			msg ="【索菲亚全屋定制】您好，您于"+time+"在"+location+"将有一场“"+intvName+"”安排，请提前20分钟到达现场，并携带好电脑或其他电子设备，通过登陆工作台开始评价，详细信息请查看邮件，如有任何疑问，请咨询人力资源部，谢谢！";
		}else if(type==5){
			
			msg ="【索菲亚全屋定制】您好，您的“"+intvName+"”安排已调整到"+time+"在"+location+"，请提前20分钟到达现场，并携带好电脑或其他电子设备，通过登陆工作台开始评价，详细信息请查看邮件，如有任何疑问，请咨询人力资源部，谢谢！";
		}
		return msg;
		
		
	}
	//获取当前年份
	public static int getCurrentYear(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Date date = new Date();
        return Integer.parseInt(sdf.format(date))+1;
	}
	//字符串时间转时间
	public String strTimeTrunDate(String time) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Date date=format.parse(time);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		return format1.format(date);
	}
}
