/**
 * 
 */
package com.recruit.upload;

import java.io.*;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.eos.system.annotation.Bizlet;

/**
 * @author zengjunjie
 * @date 2018-09-06 14:42:10
 *
 */
@Bizlet("")
public class upload {

	@Bizlet("")
	/**
	 * base64码转换为文件流
	 * @param base64Code 加密结果
	 * @param targetPath	文件路径
	 * @param catalogue		存储路径
	 * @throws Exception
	 */
	public static String decoderBase64File(String base64Code, String targetPath,String catalogue){
		
		String rum="";
		try {
		    File file = new File(catalogue);
		    //如果文件夹不存在,就新建一个
		    if(!file.exists()){
		    	file.mkdirs();
		    }
		    
	        File f =new File(targetPath);
	        //获取文件名加后缀
	        String fileName=f.getName();
	        
	        File absolutePath = new File(catalogue, fileName);
	        //如果文件不存在,就新建一个
	        if(!absolutePath.exists()){
	            try {
	            	absolutePath.createNewFile();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	        byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
	        FileOutputStream out = new FileOutputStream(absolutePath);
	        out.write(buffer);
	        //关闭
	        out.close();
	        
	        rum="成功";
	        return rum;
		} catch (Exception e) {
			rum="失败";
			return rum;
		}
        
	}
	
	
	
	/*public static String encodeBase64File(String path) throws Exception {
        File file = new File(path);
        FileInputStream inputFile = new FileInputStream(file);
        byte[] buffer = new byte[(int) file.length()];
        inputFile.read(buffer);
        inputFile.close();
        return new BASE64Encoder().encode(buffer);
 
    }
	
	public static void main(String[] args) {
		upload t = new upload();
		try {
		String ret = t.encodeBase64File("D:/123.png");
		System.err.println(ret);
		t.decoderBase64File(ret, "D:/999.png", "E:/MySql");
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	}*/
	
}

