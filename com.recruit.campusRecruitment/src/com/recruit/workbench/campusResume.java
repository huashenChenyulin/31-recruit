/**
 * 
 */
package com.recruit.workbench;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author 唐能
 * @date 2018-09-11 22:54:59
 *
 */
@Bizlet("")
public class campusResume {

	/**
	 * @param classroom 教室组
	 * @param resume 学生组
	 * @return
	 * @author 唐能
	 */
	@Bizlet("")
	public static List groupResume(DataObject[] resume) {
		
		List<String> list = new ArrayList<String>();
		List<Map> mapist = new ArrayList<Map>();
		for (DataObject campusResume : resume) {
			String classResume = (String) campusResume.get("classroom");
			Timestamp time = (Timestamp) campusResume.get("interview_time");
			System.out.println(time);
			list.add(time+","+classResume);
		}
		//list去重
		 HashSet<String> h = new HashSet<String>(list);   
		 list.clear();   
		 list.addAll(h);
		 //排序
		 Collections.sort(list); 
		 //分组教室学生
		 for (int i = 0; i < list.size(); i++) {
			 Map<String, Object> map = new HashMap<String, Object>();
			 String className = (String) list.get(i);
			 List<DataObject> listDataObject = new ArrayList<DataObject>();
				if (className != null && className != "" && className != "null") {
					for (DataObject campusResume : resume) {
						String classResume = (String) campusResume.get("classroom");
						Timestamp classtime = (Timestamp) campusResume.get("interview_time");
						if (className.equals(classtime+","+classResume)|| (classtime+","+classResume) == className) {
							listDataObject.add(campusResume);
						}
					}
					String[] classNames = className.split(",");
					map.put("classroom", classNames[1]);
					map.put("time", classNames[0]);
					map.put("resume", listDataObject);
					mapist.add(map);
				}
		}
		 
		/*for (DataObject dataObject : classroom) {
			String className = (String) dataObject.get("classroom");
			List<DataObject> list = new ArrayList<DataObject>();
			if (className != null && className != "" && className != "null") {
				for (DataObject campusResume : resume) {
					String classResume = (String) campusResume.get("classroom");
					if (className.equals(classResume)
							|| classResume == className) {
						list.add(campusResume);
					}
				}
				dataObject.set("resume", list);
			}
		}*/
		
		return mapist;
	}
	
	/**
	 * 
	 * @param phaseId 环节id 如：245,246,247
	 * @param phase 环节对象。空对象用来接收传递对象值
	 * @param callStatus 通话状态 ； 1：已通话、2：未接通
	 * @return
	 */
	@Bizlet("标记已通话")
	public static DataObject[] phaseDataObject(String phaseId,
			DataObject[] phase, int callStatus) {
		String[] phaseIds = phaseId.split(",");//环节id
		DataObject[] dateObject = new DataObject[phaseIds.length];
		for (int i = 0; i < phaseIds.length; i++) {
			dateObject[i] = com.eos.foundation.data.DataObjectUtil.createDataObject("com.recruit.workbench.workbench.RecruitCampusProcessPhase");
			dateObject[i].set("id", phaseIds[i]);
			dateObject[i].set("isCall", callStatus);
		}
		return dateObject;
	}

	/**
	 * 修改发送邮件通知状态 
	 * @param phaseId 环节id 如：245,246,247
	 * @param phase 环节id 如：245,246,247
	 * @return
	 */
	@Bizlet("标记已发送邮件")
	public static DataObject[] phaseDataEmail(String phaseId, DataObject[] phase,String interviewTime,String classroom) {
		String[] phaseIds = phaseId.split(",");//环节id
		DataObject[] dateObject = new DataObject[phaseIds.length];
		for (int i = 0; i < phaseIds.length; i++) {
			dateObject[i] = com.eos.foundation.data.DataObjectUtil.createDataObject("com.recruit.workbench.workbench.RecruitCampusProcessPhase");
			dateObject[i].set("id", phaseIds[i]);
			dateObject[i].set("interviewTime", interviewTime);
			dateObject[i].set("classroom", classroom);
			dateObject[i].set("isMailandsms",1);
		}
		return dateObject;
	}
}
