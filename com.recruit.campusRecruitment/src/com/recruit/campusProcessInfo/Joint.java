/**
 * 
 */
package com.recruit.campusProcessInfo;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.eos.common.mail.MailFactory;
import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author zengjunjie
 * @date 2018-08-29 10:51:57
 *
 */
@Bizlet("")
public class Joint {
	
	
	/**
	 * 
	 * @param url 链接
	 * @param to 收件人邮箱
	 * @param name 姓名
	 * @param time 时间
	 * @param place 地点
	 * @param interviewName 面试名称
	 * @param obj
	 * @return 
	 * @throws EmailException
	 */
	@Bizlet("")
	//拼接hrml ：校招通关邀请
	public String jointHTML(String to,String mailContent,String title,DataObject[] obj) throws EmailException {
		
		HtmlEmail mail = MailFactory.getHtmlEmailInstance();
		
		//定义发送内容  HTML格式
		String html="<html><body> <div style='font-size: 14px; font-family: '微软雅黑';line-height: 25px;'>";
		html+=mailContent;
		html+="<br>";
		html+="<p class='MsoNormal' align='left'><b>候选人信息：</b></p>";
		html+="<table style='margin-left: 20px;border-collapse: collapse;font-size: 14px'>";
		html+="<tbody><tr style='background: #9dc3e5'>";
		html+="<td style='border:1px solid;padding: 8px;width: 100px' contenteditable='false'>候选人姓名</td>";
		html+="<td style='border:1px solid;padding: 8px;width: 130px' contenteditable='false'>学校</td>";
		html+="<td style='border:1px solid;padding: 8px;width: 150px' contenteditable='false'>专业</td>";
		html+="<td style='border:1px solid;padding: 8px;width: 80px' contenteditable='false'>笔试成绩</td>";
		html+="<td style='border:1px solid;padding: 8px;width: 250px' contenteditable='false'>申请岗位</td>";
		html+="</tr>";
		
		for(int i=0;i<obj.length;i++){
				
				html+="<tr>";
				html+="<td style='border:1px solid;padding: 8px;' contenteditable='false' >"+obj[i].get("candidateName")+"</td>";
				html+="<td style='border:1px solid;padding: 8px;' contenteditable='false' >"+obj[i].get("college")+"</td>";
				html+="<td style='border:1px solid;padding: 8px;' contenteditable='false' >"+obj[i].get("major")+"</td>";
				html+="<td style='border:1px solid;padding: 8px;' contenteditable='false' >"+obj[i].get("testScores")+"</td>";
				html+="<td style='border:1px solid;padding: 8px;' contenteditable='false' >"+obj[i].get("positionName")+"</td>";
				html+="</tr>";
			
		}
		
		html+="</tbody></table>";
		html+="<p class='MsoNormal' align='left'><b><br></b></p>";
		html+="<p class='MsoNormal' align='left'><b><br></b></p>";
		
		html+="</div></body></html>";
		
		
		
		//发送邮件方法
		String resumes = "发送成功";
		try {
			//设置SMTP服务器地址
			mail.setHostName("192.168.2.172");
			//如果需要用户验证，设定用户名和密码
			mail.setAuthentication("campus", "xiaozhao");
			mail.setSmtpPort(25);
			//设定收件人邮件地址
			mail.addTo(to);
			//设定发送者邮件地址
			mail.setFrom("campus@sfygroup.com");
			// 字符编码集的设置
			mail.setCharset("utf-8");
			//设定主题
			mail.setSubject(title);
			//设定消息内容
			mail.setHtmlMsg(html);
			
			mail.send();
		} catch (Exception e) {
			resumes="发送失败";
		}
		return resumes;
		
	}
	
	public String sendMail(String html,String to,String subject){
		
		HtmlEmail mail = MailFactory.getHtmlEmailInstance();
		/*//设置SMTP服务器地址
		mail.setHostName("mail.sfygroup.com");
		//如果需要用户验证，设定用户名和密码
		mail.setAuthentication("campus", "xiaozhao");
		//设定收件人邮件地址
		mail.addTo(to);
		//设定发送者邮件地址
		mail.setFrom("campus@sfygroup.com");
		// 字符编码集的设置
		mail.setCharset("utf-8");
		//设定主题
		mail.setSubject(subject);
		//设定消息内容
		mail.setHtmlMsg(html);

		mail.send();*/
		
		String obj = "发送成功";
		try {
			//设置SMTP服务器地址
			mail.setHostName("192.168.2.172");
			//如果需要用户验证，设定用户名和密码
			mail.setAuthentication("campus", "xiaozhao");
			mail.setSmtpPort(25);
			//设定收件人邮件地址
			mail.addTo(to);
			//设定发送者邮件地址
			mail.setFrom("campus@sfygroup.com");
			// 字符编码集的设置
			mail.setCharset("utf-8");
			//设定主题
			mail.setSubject(subject);
			//设定消息内容
			mail.setHtmlMsg(html);
			
			mail.send();
		} catch (Exception e) {
			obj="发送失败";
		}
		return obj;
	}
	
	/*public static void main(String[] args) throws EmailException {
		
	
			sendMail("123", "1169021458@qq.com", "测试");
		
	}*/
	
	
}
