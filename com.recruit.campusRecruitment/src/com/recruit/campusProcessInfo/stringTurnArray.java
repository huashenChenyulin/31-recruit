/**
 * 
 */
package com.recruit.campusProcessInfo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author zengjunjie
 * @date 2018-09-12 16:30:27
 *
 */
@Bizlet("")
public class stringTurnArray {

	@Bizlet("")
	//字符串转数组
	public  List stringTurnJSONObject(String obj1) {
		
		JSONArray json = JSONArray.fromObject(obj1);
		List list =new ArrayList();
		if(json.size()>0){
			  for(int i=0;i<json.size();i++){
			
			    JSONObject job = json.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
			    list.add(job.toString());
			   
			  }
		}
		return list;
	}
	@Bizlet("")
	public Map jsonStrToJava(String objectStr){
	    //1、使用JSONObject
	    JSONObject jsonObject=JSONObject.fromObject(objectStr);
	    
	    Map map=new HashMap();
	    
	    Iterator<String> it = jsonObject.keys(); 
	    
	    while(it.hasNext()){
		    // 获得key
		    String key = it.next(); 
		    String value = jsonObject.getString(key);
		    map.put(key, value);
	    }

		return map;
	}
	
	/**
	 * 追加面试官
	 * @param obj 数据对象
	 * @param recruiter_id 面试官id
	 * @param recruiter_name 面试官姓名
	 * @return
	 */
	@Bizlet("")
	public DataObject appendString(DataObject obj,String recruiter_id,String recruiter_name){
		
		String[] recruiterId=recruiter_id.split(",");//面试官id
		String[] recruiterName=recruiter_name.split(",");//面试官姓名
		
		//获取对象中的面试官id和姓名
		String rec_id=(String)obj.get("recruiterId");
		String rec_name=(String)obj.get("recruiterName");
		
		for(int i=0;i<recruiterId.length;i++){
			//判断recruiterId参数是否包含recruiter_id参数
			if(rec_id.contains(recruiterId[i])){
				
			}else{
				rec_name+=","+recruiterName[i];
				rec_id+=","+recruiterId[i];
			}
			
		}
		obj.set("recruiterId", rec_id);
		obj.set("recruiterName", rec_name);
		
		return obj;
	}
	
	/**
	 * 转换成对象数组
	 * @param resume_id 简历di
	 * @param process_id 流程id
	 * @param recruit_plan_id 招聘计划id
	 * @param phase_code 环节编码
	 * @param recruiter_id 面试官id
	 * @param recruiter_name 面试官名字
	 * @param sign  标记
	 * @return
	 */
	@Bizlet("")
	public DataObject[] stringTurnDataObject(String resume_id,String process_id,String recruit_plan_id,
					String phase_code,String recruiter_id,String recruiter_name,DataObject[] obj){
		
		
		for(int i=0;i<obj.length;i++){
			String recruiterId=(String) obj[i].get("recruiterId");
			
			if(recruiterId!=null && recruiterId!=""){
				//判断recruiterId参数是否包含recruiter_id参数
				if(recruiterId.contains(recruiter_id)){
					
					String resumeIds=Integer.toString((Integer)obj[i].get("resumeId"));
					resume_id = resume_id.replaceAll(resumeIds, "");
					
					String processIds=Integer.toString((Integer)obj[i].get("processId"));
					process_id = process_id.replaceAll(processIds, "");
				}
				
			}
		}
		
		String[] processId=process_id.split(",");//流程id
		String[] resumeId=resume_id.split(",");//简历id
		String[] recruitPlanId=recruit_plan_id.split(",");//招聘计划id
		resumeId = replaceNull(resumeId);
		processId = replaceNull(processId);
		
		DataObject[] obj1= new DataObject[resumeId.length];
		//循环赋值到对象数组
		for(int i=0;i<resumeId.length;i++){
			
			//判断如果简历id不等于空则进行添加数据
			if(resumeId[i]!=null && resumeId[i]!=""&&!"".equals(resumeId[i])){
				obj1[i]=com.eos.foundation.data.DataObjectUtil.createDataObject("com.recruit.campusProcessInfo.campusProcess.RecruitCampusInterviewDetail");
				obj1[i].set("resumeId", resumeId[i]);
				obj1[i].set("processId", processId[i]);
				obj1[i].set("recruitPlanId", recruitPlanId[i]);
				obj1[i].set("interviewStep", phase_code);
				obj1[i].set("interviewerId", recruiter_id);
				obj1[i].set("interviewerName", recruiter_name);
				//获取对象主键
				com.eos.foundation.database.DatabaseExt.getPrimaryKey(obj1[i]);
				
			}
			
		}
			
		return obj1;
		
	}
	@Bizlet("")
	public List stringTurnList(String recruiter_id,String recruiter_name){
		
		String[] recruiterId=recruiter_id.split(",");
		String[] recruiterName=recruiter_name.split(",");
		List list =new ArrayList();
		
		for(int i=0;i<recruiterId.length;i++){
			Map map=new HashMap();
			map.put("recruiterId", recruiterId[i]);
			map.put("recruiterName", recruiterName[i]);
			list.add(map);
		}
		
		return list; 
		 
	}
	/**
	 * 字符串转换成list
	 * @param to_mail 收件人邮箱
	 * @param to_name 收件人姓名
	 */
	@Bizlet("")
	public List stringturnLists(String to_mail,String to_name,String to_position,
				String to_resumeId,String to_degreeType,String to_recruitPlanId){
		
		String[] toMail={};
		String[] toName={};
		String[] toPosition={};
		String[] toResumeId={};
		String[] toDegreeType={};
		String[] toRecruitPlanId={};
		//收件人邮箱不为空
		if(to_mail!="" && to_mail!=null){
			toMail=to_mail.split(",");
		}
		//收件人姓名不为空
		if(to_name!="" && to_name!=null){
			toName=to_name.split(",");
		}
		//收件人职位不为空
		if(to_position!="" && to_position!=null){
			toPosition=to_position.split(",");
		}
		//收件人简历id不为空
		if(to_resumeId!="" && to_resumeId!=null){
			toResumeId=to_resumeId.split(",");
		}
		//收件人学历类别不为空
		if(to_degreeType!="" && to_degreeType!=null){
			toDegreeType=to_degreeType.split(",");
		}
		//收件人招聘计划id不为空
		if(to_recruitPlanId!="" && to_recruitPlanId!=null){
			toRecruitPlanId=to_recruitPlanId.split(",");
		}
		List list =new ArrayList();
		for(int i=0;i<toMail.length;i++){
			Map map=new HashMap();
			//收件人邮箱不为空
			if(to_mail!="" && to_mail!=null){
				map.put("toMail", toMail[i]);
			}
			//收件人姓名不为空
			if(to_name!="" && to_name!=null){
				map.put("toName", toName[i]);
			}
			//收件人职位不为空
			if(to_position!="" && to_position!=null){
				map.put("toPosition", toPosition[i]);
			}
			//收件人简历id不为空
			if(to_resumeId!="" && to_resumeId!=null){
				map.put("toResumeId", toResumeId[i]);
			}
			//收件人学历类别不为空
			if(to_degreeType!="" && to_degreeType!=null){
				map.put("toDegreeType", toDegreeType[i]);
			}
			//收件人招聘计划id不为空
			if(to_recruitPlanId!="" && to_recruitPlanId!=null){
				map.put("toRecruitPlanId", toRecruitPlanId[i]);
			}
			list.add(map);
		}
		return list;
	}
	
	/**
	 * 转换成对象数组
	 * @param resume_id 简历di
	 * @param process_id 流程id
	 * @param recruit_plan_id 招聘计划id
	 * @param phase_code 环节编码
	 * @param examNumber 当前的测试次数
	 * @param sign  标记
	 * @return
	 */
	@Bizlet("")
	public DataObject[] stringTurnDataObject2(String resume_id,String process_id,String recruit_plan_id,
					String phase_code,String examNumber){
		
		//转成数组
		String[] resumeId=resume_id.split(",");//简历id
		String[] processId=process_id.split(",");//流程id
		String[] examNumbers=examNumber.split(",");//测试次数
		String[] recruitPlanId=recruit_plan_id.split(",");//测试次数
		
		//获取当前时间
		Date date2 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
		
		//String格式转换为Date格式
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		String str = sdf.format(date2);
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();   
		}
		
		DataObject[] obj1= new DataObject[resumeId.length];
		
			//循环赋值到对象数组
			for(int i=0;i<resumeId.length;i++){
				obj1[i]=com.eos.foundation.data.DataObjectUtil.createDataObject("com.recruit.campusProcessInfo.campusProcess.RecruitCampusProcessPhase");
				obj1[i].set("resumeId", resumeId[i]);
				obj1[i].set("processId", processId[i]);
				obj1[i].set("recruitPlanId", recruitPlanId[i]);
				obj1[i].set("phaseCode", phase_code);
				obj1[i].set("startTime", date);
				obj1[i].set("examNumber", examNumbers[i]);
				//获取对象主键
				com.eos.foundation.database.DatabaseExt.getPrimaryKey(obj1[i]);
			}
			
		return obj1;
		
	}
	
	//删除数组空元素
	private String[] replaceNull(String[] str){
	    //用StringBuffer来存放数组中的非空元素，用“;”分隔
	       StringBuffer sb = new StringBuffer();
	       for(int i=0; i<str.length; i++) {
	           if("".equals(str[i])) {
	               continue;
	           }
	           sb.append(str[i]);
	           if(i != str.length - 1) {
	               sb.append(",");
	           }
	       }
	       //用String的split方法分割，得到数组
	       str = sb.toString().split(",");
	       return str;
	}
	
	
	/**
	 * 判断是否为空
	 * @param resume_id  简历id
	 * @param recruit_plan_id  计划id
	 * @param process_id  流程id
	 * @param phase_code  环节
	 * @return
	 */
	@Bizlet("")
	public String isNull(String resume_id,String recruit_plan_id,String process_id,String phase_code){
		String i="";
		
		if(resume_id=="" || resume_id==null){
			i="失败";
			
		}else if(recruit_plan_id=="" || recruit_plan_id==null){
			i="失败";
			
		}else if(process_id=="" || process_id==null){
			i="失败";
			
		}else if(phase_code=="" || phase_code==null){
			i="失败";
			
		}
		
		return i;
		
		
	}
}
