<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>
<%
	/*获取当前人员id */ 
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	/*获取当前人员姓名 */ 
	String empName = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
 	/*获取职位*/
 	String current = request.getParameter("current");
 	//获取明细关闭的状态
 	String currentState =  request.getParameter("currentState");
 	//获取明细返回的已归档的页数
 	String archivedPage =  request.getParameter("archivedPage");
 	//获取当前任务返回的查询状态
 	String state =  request.getParameter("state");
 	//获取流程返回的已归档的页数
 	String selPage =  request.getParameter("selPage");
 	String candidate_name = request.getParameter("candidate_name");//当前任务候选人姓名
	String recuit_order_id = request.getParameter("recuit_order_id");//当前任务工单号
	String startTime = request.getParameter("startTime");//当前任务开始时间
	String endTime = request.getParameter("endTime");//当前任务结束时间
	System.out.println("selPage="+selPage);
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<%= request.getContextPath() %>/myTask/css/task_select.css" rel="stylesheet">
<meta charset="utf-8">
<title>layui</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<style type="text/css">
	.div-position0{
		border-left:3px solid;
    	border-color:#eea236;
	}
	#batch-template,#test4{
		color: #e28603;
	   	margin-left: 22px;
	    font-size: 12px;
	} 
	#batch-template,#test4:HOVER {
		cursor: pointer;
	}
	.div_button{
	    text-align: center;
   		margin-top: 10px;
	} 
</style>
<body>
	
	<div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">创建工单</h4>
				</div>
				<div id="addrecuitOrder" class="modal-body">
					<label class="layui-form-label set">部门：</label>
					<input class="form-control long getStaff" id="department" onclick="choiceOrg(this)" alias="" L1="" L1name="" L2="" L2name="" L3="" L3name="" L4="" L4name="" L2placeholder="人员选择" value=""  type="text">				
					<label class="layui-form-label set">需求职位：</label>
						<select id="addposition" class="form-control long" >
							<option vlaue="">请选择部门</option>
						</select>
					<div class="found_type">
						<label class="layui-form-label post set">职位类别：</label>
						<select id="addcategory" class="form-control long" onchange="findPeriod(this)"  >
								<option value="">请选择职位类别</option>
						</select>
					</div>
					<div style="display:flex; height: 48px;">
						<div class="rec_fair">
							<label class="layui-form-label post set">招聘周期（天）：</label>
							<select id="recruitPeriod" class="form-control long import end" >
									<option value="">请选择招聘周期</option> 
									<option value="15">15</option> 
									<option value="30">30</option> 
									<option value="45">45</option> 
									<option value="60">60</option> 
									<option value="90">90</option> 
							</select>
						</div>
						<div class="rec_fair">
							<label class="layui-form-label post set">职位属性：</label>
							<select id="catAttribute" class="form-control long import end" >
									<option value="">请选择职位属性</option>
									<option value="1">计时</option> 
									<option value="2">计件</option> 
							</select>
						</div>
						<div class="rec_fair">
							<label class="layui-form-label post set">所属公司：</label>
							<select id="company" class="form-control import end" >
									<option value="">请选择所属公司</option>
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="addOrder()">创建</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
	
		</div>
	
	</div>
		<!--导航栏  -->
		<ul id="myTab" class="nav nav-tabs">
			<li class="active" id='recruitment' onclick="recruitment()"><a href=""
				data-toggle="tab" class="a">当前任务</a></li>

			<li class="archive"  id ="archiveTask" onclick="archiveTask()"><a href=""
				data-toggle="tab" class="a">已归档任务</a></li>
			
			
			<div id="tab-button">
			<div class="btn-group btn-sorting">
				<button  type="button"  class="btn btn-default btn-center btn-up" onclick="sortUp()">剩余周期 
				<img class="btn-up-img" id="top" src="<%=request.getContextPath()%>/css/layui/images/face/whiteTop.png"> </button>
				<button type="button" class="btn btn-default btn-center btn-down" onclick="sortDown()"  >审批日期
				<img class="btn-down-img" id="bottom" src="<%=request.getContextPath()%>/css/layui/images/face/bottom.png">
				
				</button>
				<img id="whitebottom" src="<%=request.getContextPath()%>/css/layui/images/face/whiteBottom.png">
			</div>
			<!--批导模板按钮  -->
			
			<a class="glyphicon glyphicon-open" id="test4">信息批导</a>
				
			<a id="batch-template" class="glyphicon glyphicon-save" 
					href = "<%=request.getContextPath()%>/myTask/候选人批导模板.xls">模版下载</a>
					
			<%-- <a id="a-batch-template" href = "<%=request.getContextPath()%>/myTask/候选人批导模板.xls">
			<button type="button"  id="batch-template"  class="btn btn-default" onclick="">模板下载
			</button>
			</a>
			<button type="button" class="layui-btn layui-btn-primary file " id="test4">候选人批导</button> --%>
			<button class="btn btn-warning right" type="button" data-toggle="modal" data-target="#myModal">创建工单</button>
		  
		  </div>
		  
		</ul>
		<!-- 当前任务 -->
		<div id="content-recruitment">
			
			<!-- 显示职位内容 -->
			<div id = "main-left">
			
			</div>
			
			<!-- 显示工单内容 -->
			<div id = "main-right">
				<div class="sel_div">
					<font class="font-text">候选人姓名：</font>
					<input type="text" class="form-control import top_text" id="candidate_name" >
					<font class="font-text">单号：</font>
					<input type="text" class="form-control import top_text" id="recuit_order_id" >
					<font class="font-text">部门：</font>
					<input type="text" class="form-control import top_text" id="org_name" 
						onmouseover="selbox($(this))" onclick="getOrg(this)" data="" alias="" placeholder="部门选择">
					<font class="font-text">审批日期：</font>
					<input type="text" class="startTime import" id="present_startTime" style="width:12.5%;margin-right:1%;" name="birthTime" placeholder="yyyy-MM-dd">
					<font class="font-text block">至</font>
					<input type="text" class="startTime import" id="present_endTime" style="width:12.5%;margin-right:1%;" name="birthTime" placeholder="yyyy-MM-dd">
					<br>
					<div class="div_button">
					
						<button class="btn btn-warning top_btn " type="button" onclick="selOrders()">查询</button>
						<span class="span_block"></span>
						<button type="button" class="btn btn-default btn-center" onclick="isReset()">重置</button>
					</div>
				</div>
				<div id="main-right-table" style="margin-top: 35px;">
					
				</div>
			</div>
				<!-- 分页条 -->
			<div id="pagingToolbar"></div> 
		</div>
		
			
		
		

		<!-- 已归档任务 -->
		 <div class="content_archive">
			<div class="search">
					职位：<input type="text" class="form-control import" id="position" >
					状态：<select class="form-control import"id="oddState" >
						<option></option>
						<option value="3">已入职</option>
						<option value="4">取消招聘</option>
						<option value="5">候选人放弃</option>
						<option value="6">修改负责人</option>
						<option value="7">已关联</option>
					</select>
					部门：
					<input type="text" class="form-control import top_text" id="org_name_v2" 
						onmouseover="selbox($(this))" onclick="getOrg(this)" data="" alias="" placeholder="部门选择">
						
					审批日期：<input type="text" class="startTime import" id="startTime"
						name="birthTime" placeholder="yyyy-MM-dd"> 
					<span id= "to">至</span>
					<input
						type="text" class="endTime import" id="endTime" name="birthTime"
						placeholder="yyyy-MM-dd"> 
						
					<div class="div_button">
						<button type="button" onclick="archive()" class="btn btn-primary"
							id="search" data-complete-text="Loading finished">搜索</button>
						<button type="button" class="btn btn-default btn-center" onclick="isReset()">重置</button>
					</div>	
					
				</div>
			<div id="archive-list"></div>
			<!--  分页条-->
			<div id="archive-pagingToolbar"></div>
		</div>  

		</div>
		<!--Excel导出  -->
	<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>

	<script type="text/javascript">
	
	
	var empCode ='<%=empCode %>';//获取当前登录人ID
	var empName ='<%=empName %>';//获取当前登录人姓名
	var tagPosition = '<%=current%>';//获取流程点击返回过来的职位
	var currentState = <%=currentState %>;//获取工单明细点击关闭过来的状态
	var archivedTaskPage = '<%=archivedPage %>';//获取工单明细返回已归档任务的当前页数
	var state =false;
		if(<%=state %>!=null){
			state = '<%=state %>';//获取当前任务返回的查询状态
		}
	var pro_candidate_name ='<%=candidate_name %>';//当前任务候选人姓名
	var pro_recuit_order_id ='<%=recuit_order_id %>';//当前任务工单号
	var pro_startTime = '<%=startTime %>';//当前任务开始时间
	var pro_endTime ='<%=endTime %>';//当前任务结束时间
	var orderPage =1;//起始页数
	var selPage =<%=selPage %>;
		if(selPage!=null){ //流程返回的页数
			orderPage = selPage;
		}
	//加载layui
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;  
		});  
		
   
	//点击工单明细跳转页面的方法
	function clikDetail(obj){
	
		var url="/myTask/task_details.jsp?recuitOrderId="+obj+"&page="+archivePage;
	    parent.goToUrl(url);
	} 
	
 	layui.use('upload', function(){
  		var $ = layui.jquery
  		,upload = layui.upload;
  		upload.render({ //允许上传的文件后缀
	    elem: '#test4'
	    ,url: '<%=request.getContextPath()%>/reports/ExcelFile_uploadserver.jsp'
	    ,accept: 'file' //普通文件
	    ,exts: 'xls' //只允许上传压缩文件
	    ,done: function(res){
	      
	      if(res.msg=="成功"){
	      var json = {"filePath":res.data.filePath};
	      $.ajax({
       		url: 'com.recruit.excelEntery.ExcelEntery.ExcelCandidate.biz.ext',
			type:'POST',
			data:json,
			cache: false,
			success:function(data){ 
				if(typeof(data.exception) == "undefined"||data.exception==null){
				if(data.alreadyExist!=null){
		            	//配置Excel表头内容
		            	var head = '{"stutas":"保存状态说明","recuitOrderId":"工单号","candidateName":"姓名","candidatePhone":"电话"}';
		            	/* ,"candidateMail":"邮箱","expectedPosition":"简历性质","recruitChannels":"招聘渠道","recruitSuppliers":"供应商",'
		            	+'"gender":"性别","degree":"学历" */
		            	//获取Excel表内容
		            	var jsonObject = JSON.stringify(data.alreadyExist);
		            	//获取Excel工作簿名称
		            	var sheetName = "候选人批导结果";
		            	//获取Excel文件名称
		            	var fileName = "候选人批导结果";
		            	//保存到表单中
		            	document.getElementById("jsonObject").value = jsonObject;
 						document.getElementById("head").value = head;
		            	document.getElementById("sheetName").value = sheetName;
 						document.getElementById("fileName").value = fileName; 
 						//导出Excel表
		            	var form = document.getElementById("formExcel");
				        form.action = "com.recruit.reports.deriveExcel.flow";
				        form.submit(); 
				        }
			        }
				
				}
			})
	     
	      }
	    }
	  });
	}) 
</script>
<script src="<%=request.getContextPath()%>/myTask/js/task_select.js?v=1.0"type="text/javascript"></script>
</body>
</html>
