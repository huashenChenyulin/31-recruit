<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>
<%
	String recuitOrderId = request.getParameter("recuitOrderId");
	//获取已归档任务的当前页数
	String archivedPage = request.getParameter("page");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-13 08:34:28
  - Description:
-->
<head>
<link href="<%= request.getContextPath() %>/myTask/css/task_details.css" rel="stylesheet">
<title>工单明细</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">

</style>
</head>
<body>
	<div id="wrap">
		<div id="head">
		
		<span id = "title">工单明细</span>
			 
			<button type="button"onclick="shutDown()"class="btn btn-primary" data-complete-text="Loading finished">关闭</button>
		
		</div>
		
		<div id="main">
			<div id="header">
				<table class="table table-condensed  headTable">
					<tbody>
						<tr>
							<td>工单号：<span id="recuitOrderId"></span></td>
							<td>招聘负责人：<span id="recruiterName"></span></td>
							<td>部门：<span id="department"></td>
						</tr>
						<tr>
							<td>职位：<span id="position"></span></td>
							<td>职位类别：<span id="jobCategory"></span></td>
							<td>招聘周期：<span id="recruitPeriod"></span>天
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<table class="table table-condensed tabCondensed">
				<thead>
					<tr>
						<th>姓名</th>
						<th>年龄</th>
						<th>渠道</th>
						<th>供应商</th>
						<th>学历</th>
						<th>性别</th>
						<th style="width: 16%;">不通过/放弃环节</th>
						<th style="width: 35%;">原因</th>
					</tr>
				</thead>
				<tbody id="tbody">
						
					
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
	//已归档任务的页数
	var archivedPage = '<%=archivedPage%>';
			
	//查询工单明细
	$(function(){
			var recuitOrderId = '<%=recuitOrderId %>';//单号
			var data = {recuitOrderId:recuitOrderId};
		$.ajax({
			url:'com.recruit.process.process.queryPigeonholeFlew.biz.ext',
			data:data,
			type:'post',
			dataType:'json',
			success: function (data){
			
			
				
				$("#recuitOrderId").html(data.recruitOddNumbers.recuitOrderId);//单号
				$("#recruiterName").html(data.recruitOddNumbers.recruiterName);//招聘负责人
				//部门
				
				
				var department = formatDepartment(data.recruitOddNumbers.l1Name,data.recruitOddNumbers.l2Name,data.recruitOddNumbers.l3Name,data.recruitOddNumbers.l4Name);
				$("#department").html(department);
				$("#position").html(data.recruitOddNumbers.position);//职位
				$("#jobCategory").html(data.recruitOddNumbers.jobCategory);//职位类别
				$("#recruitPeriod").html(data.recruitOddNumbers.recruitPeriod);//招聘周期
				
				
				//判断recruitCandidate不能为空
				if(data.recruitOddNumbers.recruitCandidate !=null && data.recruitOddNumbers.recruitCandidate.length>0){
					for(var i = 0 ;i<data.recruitOddNumbers.recruitCandidate.length;i++){
					
						 //年龄：把查询出的日期值传入getAge（）方法里面并返回结果
						 var age = getAge(data.recruitOddNumbers.recruitCandidate[i].recruitResume.birthDate);
						 //性别:把查询出的性别类型代码传入sexstatus（）方法里面并返回结果
						 var gender = sexstatus(data.recruitOddNumbers.recruitCandidate[i].recruitResume.gender);
						 //学历:把查询出的学历类型代码传入educationstatus（）方法里面并返回结果
						 var degree = educationstatus(data.recruitOddNumbers.recruitCandidate[i].recruitResume.degree);
						 //当前所在环节:把查询出的当前所在环节类型代码传入Abandonmentstatus（）方法里面并返回结果
						 var status=  Abandonmentstatus(data.recruitOddNumbers.recruitCandidate[i].recruitProcessId);
						 //recruitChannels（渠道名称）
						 var recruitChannels = getChannels(data.recruitOddNumbers.recruitCandidate[i].recruitChannels);
						// recruitSuppliers（供应商名称）
						 var recruitSuppliers = getSuppliers(data.recruitOddNumbers.recruitCandidate[i].recruitSuppliers);
						 //candidateName（姓名 ）abandonReason（具体放弃情况）
						 
						 var abandonReason = data.recruitOddNumbers.recruitCandidate[i].abandonReason;
						 if(abandonReason==null || abandonReason==""){
						 	 abandonReason = "";
						 }
						 var tr ="<tr class='change' onclick='callParent("+data.recruitOddNumbers.recruitCandidate[i].id+")'>";
							 tr+="<td>"+data.recruitOddNumbers.recruitCandidate[i].recruitResume.candidateName+"</td>";
							 tr+="<td>"+age+"</td>";
							 tr+="<td>"+recruitChannels+"</td>";
							 tr+="<td>"+recruitSuppliers+"</td>";
							 tr+="<td>"+degree+"</td>";
							 tr+="<td>"+gender+"</td>";
							 tr+="<td>"+status+"</td>";
							 if(data.recruitOddNumbers.recruitCandidate[i].recruitProcessId=="6"){
							 tr+="<td><div id ='ru'>入职</div></td>";
							 }else{
							 tr+="<td>"+abandonReason+"</td>"; 
							 }
							 tr+="</tr>";
						$("#tbody").append(tr);
					
					}
				  }else{
				
				  	var tr="<tr><td colspan='8'><div style='text-align: center;margin-top: 1%;'>暂无数据！</div></td></tr>";
				  	$("#tbody").append(tr);
				  }
				}
			});
	});
	
	function callParent(id){
		parent.getTalentData(id);
	}
	function shutDown(){
	
		var url="/myTask/task_select.jsp?currentState='close'&archivedPage="+archivedPage+"";
    	parent.goToUrl(url);
	}
	
	</script>
</body>
</html>