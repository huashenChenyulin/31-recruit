	
	//status标识  true为招聘中  false为已归档
	var status;
//	var state =false;//标识当前任务查询是否被点击：false，已点查询按钮为：true
	//标识初始化
	var Initialize =true;
	var tagePage =false;
	$(function(){
		
		//判断状态是否从工单明细点击关闭过来的
		if(currentState=="close"){
			
			$("#archiveTask").click();
		}else{
			//初始化查询职位
			selectPositions();
			 //初始化查询职位类别
			findPositionCategory();
			 //初始化查询所属公司
         	 $.ajax({
         		 url: basePath+'com.primeton.task.taskoperation.selectCompany.biz.ext',
         		 type:'POST',
         		 cache: false,
         		 success:function(data){
         			 for(var i=0;i<data.data.length;i++){
         				 $("#company").append('<option value="'+data.data[i].companyId+'">'+data.data[i].companyName+'</option>');
         			 }
         		 }
         	 });
			
		}
	});
	//查询职位类别
	function findPositionCategory(){
		 $.ajax({
	     		url: basePath+'com.primeton.task.taskoperation.selectCategory.biz.ext',
				type:'POST',
				cache: false,
				success:function(data){
					//数据长度要大于0
					if(data.data.length>0){
					//动态给职位类别下拉框赋值
					 for(var i=0;i<data.data.length;i++){
						$("#addcategory").append('<option value="'+data.data[i].dictID+'">'+data.data[i].dictName+'</option>');
					 }
				   }
				}
	    	});
	}
	
		
		
	
	//职位类别联动招聘周期
  	function findPeriod(oneself){
  		var categoryID =$(oneself).val();//获取职位类别值
  		$.ajax({
      		url: basePath+'com.primeton.task.taskoperation.queryPeriod.biz.ext',
			type:'POST',
			data:{"categoryID":categoryID},
			cache: false,
			success:function(data){
				$("#recruitPeriod").val(data.data);//赋值招聘周期
			}
     	 });
  		
  	}

	//添加工单
	function addOrder(){
		var department = $("#department").val(); //部门
		var addposition = $("#addposition").val(); //需求职位
		var addcategory = $("#addcategory").val(); //职位类别
		var recruitPeriod =$("#recruitPeriod").val();//招聘周期
		var catAttribute =$("#catAttribute").val();//职位属性
		var company =$("#company").val();//所属公司
		var l1 = $("#department").attr("l1"); //中心编号
		var l2 = $("#department").attr("l2"); //部门编号
		var l3 = $("#department").attr("l3"); //科组编号
		var l4 = $("#department").attr("l4"); //科室编号
		var l1name = $("#department").attr("l1name"); //中心名称
		var l2name = $("#department").attr("l2name"); //部门名称
		var l3name = $("#department").attr("l3name"); //科组名称
		var l4name = $("#department").attr("l4name"); //科室名称
			l1name=(l1name!="null")?l1name:null;
			l2name=(l2name!="null")?l2name:null;
			l3name=(l3name!="null")?l3name:null;
			l4name=(l4name!="null")?l4name:null;
			//验证数据
			if(department==""){
				layer.msg("请选择部门");
				return;
			}else if(addposition==""){
				layer.msg("请选择需求职位");
				return;
			}else if(addcategory==""){
				layer.msg("请选择职位类别");
				return;
			}else if(recruitPeriod==""){
				layer.msg("请选择招聘周期");
				return;
			}else if(catAttribute==""){
				layer.msg("请选择职位属性");
				return;
			}else if(company==""){
				layer.msg("请选择所属公司");
				return;
			}
		var json = {recName:empName,recruiterId:empCode,l1:l1,l2:l2,l3:l3,l4:l4,addorgcentre:l1name,addorgbranch:l2name,addorgoffice:l3name,addorgsubject:l4name,isVerbal:1,recruitPeriod:recruitPeriod,addposition:addposition,addcategory:addcategory,catAttribute:catAttribute,company:company};
			layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,resize:false //是否允许拉伸
			        ,content: '<div style="padding: 20px 75px 0px;">是否创建工单？</div>'
			        ,closeBtn: false//不显示关闭图标
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0.2 //不显示遮罩
			        ,yes: function(){
			        	 $.ajax({
							url: basePath+"com.primeton.task.taskoperation.addOrderInfo.biz.ext",
							type: "post",
							data:json,
							cache: false,
							dataType: 'json',
							success: function (json) {
					    			var mark = json.mark;//返回表示保存成功
					    			if(mark != "E"){//判断是否保存成功
					    			//清空数据
					   				$('#myModal').modal('hide');
					    			$("#recName").val("");//招聘负责人
					    			$("#department").val("");//部门
					    			var cTime=document.getElementById("addposition");
					    				cTime.options.length=0;//职位
					    			$("#addposition").append("<option  value=''>请选择部门</option>");
					    			$("#addcategory").val("");//职位类别
					    			$("#recruitPeriod").val("");//周期
					    			$("#catAttribute").val("");//职位属性
					    			
					  				 	layer.closeAll();
					  				 	layer.msg("创建成功");
					  				 	tagState = "";//状态用来判断是否添加样式  为空是不添加
					  				 	//创建工单成功后存储需求职位
					  				 	tagPosition = addposition;
					  				 	//调用查询职位的方法
					  				 	selectPositions();
					  				 	
					  				 	
						    		}else{
						    			layer.msg("创建失败");
						    		}
							}
			    		}); 
			        }
			        ,btn2: function(index,layero){
			         layer.close(index);//关闭页面
			        }
			});
	}
	
	 //加载职位	-->L2:部门id
    function loadPosition(L2){
    	var html="";
         $.ajax({
             	url: basePath+'com.primeton.task.taskoperation.queryNeedPosition.biz.ext',
				type:'POST',
				data:{orgId:L2},
				async:false,
				dataType: 'json',
				success:function(data){
					var datas =data.ArrayOfT_DATA_OUTPUT.ZSTI2038_OUT;
					var error =data.MSG_TYPE;
					if(error=="E"){//接口异常
						 layer.open({//打开子页面
					        type: 1
					        ,offset:'auto' 
					        ,id: 'layerDemo'
						    ,resize:false //是否允许拉伸
					        ,content: '<div style="padding: 20px 75px;">'+data.MSG_TEXT+'</div>'
					        ,btn:['确定']
					        ,closeBtn: false//不显示关闭图标
					        ,btnAlign: 'c' //按钮居中
					        ,shade: 0 //不显示遮罩
					        ,yes: function(index){
					        	layer.close(index);
							var cTime=document.getElementById("addposition");//清除下拉框内容
								cTime.options.length=0;
					        	$("#addposition").append("<option  value=''>"+data.MSG_TEXT+"</option>");
					        }
					 	});
					}else{
							//添加工单页面加载的职位
							var cTime=document.getElementById("addposition");
								cTime.options.length=0;//清除下拉框内容
							if(typeof(datas[0].STEXT)!="undefined"){
								
								//by函数接受一个成员名字符串做为参数
								//并返回一个可以用来对包含该成员的对象数组进行排序的比较函数
								var by = function(name) {
								    return function(o, p) {
								        var a, b;
								        if (typeof o === "object" && typeof p === "object" && o && p) {
								            a = o[name];
								            b = p[name];
								            if (a === b) {
								                return 0;
								            }
								            if (typeof a === typeof b) {
								                return a < b ? -1 : 1;
								            }
								            return typeof a < typeof b ? -1 : 1;
								        } else {
								            throw ("error");
								        }
								    };
								};
								datas.sort(by("STEXT"));
								
								//将对象数组进行计算数量并去重
								var result = [], hash = {};
								var j=0;
							    for (var i = 0; i<datas.length; i++) {
							        var elem = datas[i].STEXT; 
							        j+=1;
							        if (!hash[elem]) {
						        		j=1;
						        		datas[i].num = j; //添加num这个属性 
						                result.push(datas[i]);
							            hash[elem] = true;
							        }else{
							        	//将数组最后的一个对象属性num更新为最新
							        	result[result.length-1].num=j;
							        }
							    }
								
								for(var i=0;i<result.length;i++){
										html+="<option  value="+result[i].STEXT+">"+result[i].STEXT+"（"+result[i].num+"）</option>";
								}
							}else{
										html+="<option  value=''>此部门没有职位，请重新选择</option>";
							}
								$("#addposition").append(html);
					}
				}
             });
    }
	
	//部门控件
 	function choiceOrg(idName){
		layer.open({
			type: 2,
			title: '部门选择',
			content: basePath+'coframe/tools/plugIn/selectorg.jsp',
			area: ['420px', '500px'],
			closeBtn: false,
	        resize:false, //是否允许拉伸
			btn: ['确定','关闭'],
			yes: function(index, layero){ 
				    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
				    var a=iframeWin.getData();
				    var L1="";
				    var L1name="";
				    var L2="";
				    var L2name="";
				    var L3="";
				    var L3name="";
				    var L4="";
				    var L4name="";
				    var alias="";
				    var allName="";
				    $(a).each(function(i,obj){
				    		L1+=obj.L1id;
				    		L1name+=obj.L1name;
				    		L2+=obj.L2id;
				    		L2name+=obj.L2name;
				    		L3+=obj.L3id;
				    		L3name+=obj.L3name;
				    		L4+=obj.L4id;
				    		L4name+=obj.L4name;
				    		alias+=obj.alias;
				    		allName+=obj.aliasName;
				    		
				    });
				    	$(idName).val(allName);
				    	$(idName).attr("alias",alias); 
			  			$(idName).attr("L1",L1); 
			  			$(idName).attr("L1name",L1name); 
			  			$(idName).attr("L2",L2); 
			  			$(idName).attr("L2name",L2name); 
			  			$(idName).attr("L3",L3); 
			  			$(idName).attr("L3name",L3name); 
			  			$(idName).attr("L4",L4); 
			  			$(idName).attr("L4name",L4name);
			  			layer.close(layer.index);//关闭当前页面
			  			var orgID="";
			  			if(L4!=""){//获取最低那层部门
			  				orgID =L4;
			  			}else if(L3!=""){
			  				orgID =L3;
			  			}else if(L2!=""){
			  				orgID =L2;
			  			}else if(L1!=""){
			  				orgID =L1;
			  			}
			  			loadPosition(orgID);//点击确定按钮时加载需求职位
			},
			btn2: function(index, layero){
				   layer.close(layer.index);
				  }    
		});
 	}

 	//获取需要修改单号的值  num标识id recuitOrderId 工单号
	function modifyOfficeId(obj,num,tag,auditTime,priod,recuitOrderId){
		
		var id =$(obj).next().val();//获取下个节点单号id的值
		var isDifficult = $("#btnIsdifficult"+num).html();//是否难招
		var isIntern = $("#btnIsintern"+num).html();//是否实习生
		var roStatus = $("#btnRoStatus"+num).html();//单号当前状态
		//tag 标识 1代表是否难招  2代表是否实习生 3代表当前状态
			if(tag==1){
				isDifficult = $(obj).html();
			}else if(tag==2){
				isIntern = $(obj).html();
			}else if(tag==3){
			//修改当前状态
				
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,title:'提示'
			        ,content: '<div style="padding: 20px 49px 8px 45px;">单号为：'+recuitOrderId+'</div>'+
			        		  '<div style="text-align: center;">是否取消招聘？</div>'
			        		 
			        ,closeBtn: false//不显示关闭图标
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
			        ,yes: function(index){
			        	roStatus = $(obj).html();//单号当前状态
			        	tagState = "";//标识是否添加样式  空为否
			 			if(roStatus=="取消招聘"){
							roStatus=4;
						}
			 			var currentTime = getCurrentTime();//当前时间
			 			
						var data = {id:id,isDifficult:isDifficult,isIntern:isIntern,roStatus:roStatus,auditTime:auditTime,priod:priod,currentTime:currentTime,orderId:recuitOrderId};
						//执行修改的方法
						updateRecuitOrder(data,tag,num,obj);
			    		layer.close(index);
			        }
			        ,btn2: function(index,layero){
			        	layer.close(index);//关闭页面
			        }
			});
				
		}
		 	//是否难招
			if(isDifficult=="是"){
				isDifficult=1;
			}else if(isDifficult=="否"){
				isDifficult=0;
			}
			//是否实习生
			if(isIntern=="是"){
				isIntern=1;
			}else if(isIntern=="否"){
				isIntern=0;
			} 
			//单号当前状态
			if(roStatus=="招聘中"){
		  		roStatus=1;
			}else if(roStatus=="待入职"){
		   		roStatus=2;
			}
			//当前状态不执行修改  只执行是否难招 (是否实习生) 修改
			if(tag!=3){
				var data = {id:id,isDifficult:isDifficult,isIntern:isIntern,roStatus:roStatus};
				updateRecuitOrder(data,tag,num,obj);
			}		 
	}
	//修改单号信息 
	function updateRecuitOrder(data,tag,num,obj){
		 $.ajax({
					url:'com.recruit.myTask.myTask.updateRecuitOrder.biz.ext',
					type: 'post',	
					data:data,   		
					dataType: 'json',
					success: function (data) {
							//回调data.success等于1代表成功
						 	if(data.success==1){
						 		// tag 标识 1代表是否难招  2代表是否实习生 3代表当前状态
						 		if(tag==1){
						 			$("#btnIsdifficult"+num).html($(obj).html());
						 		}else if(tag==2){
						 			$("#btnIsintern"+num).html($(obj).html());
						 		}else if(tag==3){
						 			//执行根据职位查询工单方法
						 			selectOrder(storagePosition);
						 		}		
							} 
					}
			});  
	}
 

	
	


	
	//点击导航栏的当前任务触发方法
	function recruitment(){
			//显示当前任务内容
			$("#content-recruitment").show();
			//隐藏已归档内容
    		$(".content_archive").hide();
    		//显示导航上的按钮
  		  	$("#tab-button").show();
			//工单明细点击关闭的状态要等于close
			if(currentState=="close"){
				//查询职位
				selectPositions();
				//清空工单明细点击关闭的状态值
				currentState="";
			}
			
	}
	//点击导航栏的已归档任务触发方法
	function archiveTask(){
		
		  //查询已归档任务
		  archive();
		  //隐藏导航上的按钮
		  $("#tab-button").hide();
		  //隐藏当前任务内容
		  $("#content-recruitment").hide();
		  
		  //显示已归档内容
		  $(".content_archive").show();
		  //替换导航栏的当前任务、已归档任务的class元素
		  $("#archiveTask").attr("class","active");
		  $("#recruitment").attr("class","archive");
		  
	}
	


	//加载时间控件
	layui.use('laydate', function(){
		  var startTime = layui.laydate;
		  var endTime= layui.laydate;
		  //执行一个laydate实例
		  startTime.render({
		    elem: '#startTime'
		  });
		  endTime.render({
		    elem: '#endTime'
		  });
	});
	
	//加载当前任务时间控件
	layui.use('laydate', function(){
		  var startTime = layui.laydate;
		  var endTime= layui.laydate;
		  //执行一个laydate实例
		  startTime.render({
		    elem: '#present_startTime'
		  });
		  endTime.render({
		    elem: '#present_endTime'
		  });
	});
	
	
	//获取当前时间
	function getCurrentTime(){
		var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    //拼接时间
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
		    return currentdate;
	}
	
    
   //当前任务
    //状态标识
    var status="";
	//用来每次点击职位存储
	var storagePosition;

	//按招聘周期排序
function sortUp(){
	//改变招聘周期按钮的样式和图片
	$(".btn-up").css("color","#fff");
	$(".btn-up").css("background-color","#f0ad4e");
	$(".btn-up").css("border-color","#f0ad4e");
	$(".btn-up").css("outline","none");
	$(".btn-up-img").attr("src",basePath+"css/layui/images/face/top.png");
	
	//改变审批日期按钮的样式和图片
	$(".btn-down").css("color","#8a8a8a");
	$(".btn-down").css("background-color","#fff");
	$(".btn-down").css("border-color","#ccc");
	$(".btn-down-img").attr("src",basePath+"css/layui/images/face/bottom.png");
	
	tagState="";
	status = true;
	selectOrder(storagePosition);
}
//按审批日期排序
function sortDown(){
	//改变审批日期按钮的样式和图片
	$(".btn-down").css("color","#fff");
	$(".btn-down").css("background-color","#f0ad4e");
	$(".btn-down").css("border-color","#f0ad4e");
	$(".btn-down").css("outline","none");
	$(".btn-down-img").attr("src",basePath+"css/layui/images/face/whiteBottom.png");
	
	//改变招聘周期按钮的样式和图片
	$(".btn-up").css("color","#8a8a8a");
	$(".btn-up").css("background-color","#fff");
	$(".btn-up").css("border-color","#ccc");
	$(".btn-up-img").attr("src",basePath+"css/layui/images/face/whiteTop.png");
	
	tagState="";
	status = false;
	selectOrder(storagePosition);
}

//查询职位
function selectPositions(){
$.ajax({
		url: 'com.recruit.process.process.queryRecruitPosition.biz.ext',
		type:'POST',
		cache: false,
		success:function(data){
			//数据长度要大于0
			if(data.Positions.length>0){
				
				//对元素清空
				$("#main-left").empty();
				
				//循坏拼接显示职位
				for(var i = 0 ; i<data.Positions.length;i++){
					
					var divli ="" ;
						if(tagPosition!="null"){
							divli +="<div class = 'div-content' id='' onclick='selectOrder(\""+data.Positions[i].position+"\",this,false)'>";
						}else{
							divli +="<div class = 'div-content div-position"+i+"' id='' onclick='selectOrder(\""+data.Positions[i].position+"\",this,false)'>";
						}
						divli += "<div class='div-li'>";
						divli += "<div class='mail-left-position' title="+data.Positions[i].position+">"+data.Positions[i].position+"</div><span class='glyphicon glyphicon-chevron-right'></span>";
						divli +="</div>";
						divli += "</div>";
					$("#main-left").append(divli);
					
					
				}
				//流程点击返回职位不能为空
				if(tagPosition!="null"){
					//遍历元素
					$(".mail-left-position").each(function(){
						//判断每个元素的值是否等于返回的值相同，相同会绑定一个点击事件
						 if($(this).text()==tagPosition){
							//给class 元素添加样式
							
							 $(this).parent().parent().click();
							 $(this).parent().parent().css("border-left","3px solid");
							 $(this).parent().parent().css("border-color","#eea236");
							
						 }
					});
				}else{
					//给class 元素添加样式
					$(".div-position0").css("border-left","3px solid");
					$(".div-position0").css("border-color","#eea236");
				}
				if(tagPosition=="" || tagPosition=="null" &&state == false){//第一次进来
					//查询根据职位查询工单的方法
					selectOrder(data.Positions[0].position,"",false);
				}
				
			}else{
				//对左边职位元素清空
				$("#main-left").empty();
				//对右边工单元素清空
				$("#main-right-table").empty();
				//对分页条清空
				$("#pagingToolbar").empty();
			}
		}
	 });
	
}

var num = 0;//标识id
//标识是否初始化 true为是 false 为否
var tagState= true;

var orderLimit = 5;//显示条数
var orderCount;//总条数
var candidate_name ="";//候选人姓名
var recuit_order_id ="";//单号
var org_name="";//部门
var startTime ="";//审批开始时间
var endTime ="";//审批结束时间
//根据职位查询工单
function selectOrder(position,obj,postState){//postState:标识当前任务查询工单状态，清空职位值 
	if(tagState==true){
		tagState="style";
	}else{
		if(tagState == "style"){
			//给当前元素加id 加样式
			$(".div-content").css("border-left","0px solid");
			$(obj).attr("id","positions");
			$(obj).css("border-left","3px solid");
			$(obj).css("border-color","#eea236");
		
		}else{
			tagState="style";
		}
	}
		//获取部门
		org_name=$("#org_name").val();
		storagePosition = position;
		candidate_name = candidate_name.replace(/\s+/g,"");
		recuit_order_id = recuit_order_id.replace(/\s+/g,"");
		org_name=org_name.replace(/\s+/g,"");
	if(postState==true||typeof(postState)=="undefined"){//当前任务查询功能清空职位条件
		position ="";
	}else{//清空当前任务搜索框值
		isReset();
		candidate_name="";
		recuit_order_id ="";
		startTime ="";
		endTime ="";
		if(state=="true"){//流程传回来的查询状态
			state =true;
		}else{
			state =false;
		}
	}
	if(state==true&&postState==false){//有查询条件
		position ="";
		candidate_name = pro_candidate_name;
		recuit_order_id = pro_recuit_order_id;
		startTime = pro_startTime;
		endTime = pro_endTime;
		$("#candidate_name").val(candidate_name);
		$("#recuit_order_id").val(recuit_order_id);
		$("#present_startTime").val(startTime);
		$("#present_endTime").val(endTime);
	}
	if(tagePage ==true&&state==false){//点击分页
		position = storagePosition;
		tagePage =false;
	}
	var data =
			{
				"candidate_name":candidate_name,
				"recuit_order_id":recuit_order_id,
				"org_name":org_name,
				"startTime":startTime,
				"endTime":endTime,
				"position":position,
				"status":status,
				"limit":orderLimit,
				"curr":orderPage
			};
	$.ajax({
	url: 'com.recruit.process.process.queryProcess.biz.ext',
	type:'POST',
	data:data,
	cache: false,
	success:function(data){
		org_name="";
		//数据长度要大于0
		if(data.RecruitOrder.length>0){
			orderCount=data.PageCond.count;
			currentLaypages(orderCount);
		//清空id 以为的内容
		$("#main-right-table").empty();
		$("#pagingToolbar").show();//显示分页
		for(var i = 0 ; i < data.RecruitOrder.length;i++){
		 num++;
		//审核时间
		var auditTime= data.RecruitOrder[i].audit_time;
		//审批时间不能为空  并进行截取10位
       if(auditTime!=null){
          auditTime = auditTime.substring(0,10);
       }
       //职位类别
     	var jobCategory = Positioncatagory(data.RecruitOrder[i].job_category);
     	//工作地点
     	var workArea =  data.RecruitOrder[i].work_area;
       if(workArea==null){
       	workArea="无";
       }
       //是否难招 
		var isDifficult = orresult(data.RecruitOrder[i].is_difficult);
		//是否实习生
		var isIntern = orresult(data.RecruitOrder[i].is_intern);
		
		//工单当前状态 1招聘中 2待入职 
  		 if(data.RecruitOrder[i].ro_status==1){
  		 	 data.RecruitOrder[i].ro_status="招聘中";
  		 }else if(data.RecruitOrder[i].ro_status==2){
  		 	 data.RecruitOrder[i].ro_status="待入职";
  		 }
  		 //招聘原因
  		 var mainReason = data.RecruitOrder[i].main_reason;
  		 if(mainReason==null){
  			mainReason="无";
  		 }
  		 //部门 
  		 var orgName = formatDepartment(data.RecruitOrder[i].L1_name,data.RecruitOrder[i].L2_name,data.RecruitOrder[i].L3_name,data.RecruitOrder[i].L4_name);
		
  		//职位属性
  		 var categoryAttribute  = data.RecruitOrder[i].category_attribute;
  		 //职位属性 等于1 为计时  2为计件
  		 if(categoryAttribute == 1){
  			categoryAttribute = "计时";
  		 }else if(categoryAttribute == 2){
  			categoryAttribute = "计件";
  		 }
  		 //	已关联工单
  		var relevanceOrderId = data.RecruitOrder[i].relevance_order_id;
  		
  		
  		//oa流程号
  		var oaProcessId =  data.RecruitOrder[i].oaProcess_id;
  		if(oaProcessId == null){
  			oaProcessId="无";
  		}
  		 
  		 //拼接工单信息
		var order = "<div class='panel panel-default' style='display: block;'>";
		order+="<div class='panel-heading'>";
		//判断是否口头单还是oa单 （0是oa 1是口头）
		if(data.RecruitOrder[i].is_mannul_id ==1){
			order+="<h3 class='panel-title'>单号："+data.RecruitOrder[i].recuit_order_id+"<span class='layui-badge' style='position: absolute;margin-left: 4px;'>口头</span>";
		}else if(data.RecruitOrder[i].is_mannul_id ==0){
			order+="<h3 class='panel-title'>单号："+data.RecruitOrder[i].recuit_order_id+"";
		}
		
		order+="<button type='button' id='button' class='btn btn-warning' onclick='entranceProcess(\""+data.RecruitOrder[i].recuit_order_id+"\",\""+data.RecruitOrder[i].position+"\")'>进入流程</button>";
		order+="</h3></div><table class='table'  style='table-layout:fixed;'>";
		order+="<tbody>";
		order+="<tr>";
		order+="<td class='org scroll padding' style='padding-top: 1%;'>部门：<span title="+orgName+">"+orgName+"</span></td>";
		order+="<td id= 'auditTime'>审批日期："+auditTime+"</td>";
		order+="<td class='scroll' id='jobCategory'>职位类别：<span title='"+jobCategory+"'>"+jobCategory+"</span>";
		order+="</td>";
		//判断剩余招聘周期是否小于0 小于0 则标红
		if(data.RecruitOrder[i].surplus_period<0){
		 order+="<td style='width: 180px'>剩余周期：<span style='color:red'>"+data.RecruitOrder[i].surplus_period+"</span>天</td>";
		}else{
		 order+="<td style='width: 180px'>剩余周期："+data.RecruitOrder[i].surplus_period+"天</td>";
		}
		order+="<td style='width: 228px;'>职位属性："+categoryAttribute+"</td>";
		order+="</tr>";
		
	
		order+="<tr>";
		order+="<td id = 'order-position' class='scroll padding' >职位：<span title="+data.RecruitOrder[i].position+">"+data.RecruitOrder[i].position+"</span></td>";
		
		order+="<td>招聘负责人："+data.RecruitOrder[i].recruiter_name+"</td>";
		order+="<td>招聘周期："+data.RecruitOrder[i].recruit_period+"天</td>";
		order+="<td id= 'mainReason' class='scroll'>招聘原因：<span title='"+mainReason+"'>"+mainReason+"</span></td>";
		//关联工单等于空并且工单是oa单
		if(relevanceOrderId == null && data.RecruitOrder[i].is_mannul_id ==1){
			order+="<td class='scroll'>已关联工单：<span class='span' id='title"+i+"' title='请选择'><a  id='relevance_order_id"+i+"' class='relevance_order_id' onclick = 'relevanceOrder(this,"+data.RecruitOrder[i].id+",\""+data.RecruitOrder[i].recuit_order_id+"\","+i+")'>请选择</a></span></td>";
		}else if(relevanceOrderId != null && data.RecruitOrder[i].is_mannul_id ==1){
			order+="<td class='scroll'>已关联工单：<span class='span' title='"+relevanceOrderId+"'>"+relevanceOrderId+"</span></td>";
		}else{
			if(relevanceOrderId == null){
	  			relevanceOrderId="无";
	  		}
			order+="<td class='scroll'>已关联工单："+relevanceOrderId+"</td>";
		}
		order+="</tr>";
		
		
	
		order+="<tr>";
		order+="<td class='padding'>OA流程号："+oaProcessId+"</td>";
		order+="<td style='padding-top: 1%;' class='scroll'>工作地点：<span title = '"+workArea+"'>"+workArea+"</span></td>";
		order+="<td>是否实习：";
	   	order+="<div class='btn-group'>";
	   	order+="<button type='button' id='btnIsintern"+num+"' class='btn button btn-default btnIsdifficult downMenu'>"+isIntern+"</button>";
	   	order+="<button type='button' class='btn btn-default dropdown-toggle downMenu' data-toggle='dropdown'>";
	   	order+="<span class='caret'></span>";
		order+= "</button>";
		order+="<ul class='dropdown-menu'  role='menu'>";
		order+="<li><a  onclick='modifyOfficeId(this,"+num+",2)'>是</a><input type='hidden' value='"+data.RecruitOrder[i].id+"'></li>";
		order+="<li><a  onclick='modifyOfficeId(this,"+num+",2)'>否</a><input type='hidden' value='"+data.RecruitOrder[i].id+"'></li>";
		order+="</ul>";
		order+="</div>";
		order+="</td>";
	
		order+="<td>是否难招：<div class='btn-group'>";
		order+="<button type='button' id='btnIsdifficult"+num+"' class='btn button btn-default btnIsdifficult downMenu'>"+isDifficult+"</button>";
		order+="<button type='button' class='btn btn-default dropdown-toggle downMenu' data-toggle='dropdown' aria-expanded='false'>";
		order+="<span class='caret'></span></button>";
		order+="<ul class='dropdown-menu' role='menu'>";
		order+="<li>";
		order+="<a onclick='modifyOfficeId(this,"+num+",1)'>是</a>";
		order+="<input type='hidden' value='"+data.RecruitOrder[i].id+"'>";
		order+="</li>";
		order+="<li>";
		order+="<a onclick='modifyOfficeId(this,"+num+",1)'>否</a><input type='hidden' value='"+data.RecruitOrder[i].id+"'>";
		order+="</li>";
		order+="</ul>";
		order+="</div>";
		order+="</td>";
		
		order+="<td>当前状态：<div class='btn-group'>";
		order+="<button type='button' id='btnRoStatus"+num+"' class='btn button btn-default downMenu'>"+data.RecruitOrder[i].ro_status+"</button>";
		if(data.RecruitOrder[i].ro_status!="待入职"){
		
		order+="<button type='button' class='btn btn-default dropdown-toggle downMenu' data-toggle='dropdown'>";
		order+="<span class='caret'></span></button>";
		order+="<ul class='dropdown-menu' role='menu'>";
		order+="<li>";
		order+="<a onclick='modifyOfficeId(this,"+num+",3,\""+data.RecruitOrder[i].audit_time+"\","+data.RecruitOrder[i].recruit_period+",\""+data.RecruitOrder[i].recuit_order_id+"\")'>取消招聘</a>";
		order+="<input type='hidden' value='"+data.RecruitOrder[i].id+"'></li>";
		order+="</ul>";
		order+="</div>";
		order+="</td> ";
		}
		
		
		order+="</tr>";
		order+="</tbody>";
		order+="</table>";
		order+="</div>";
		order+="</div>";
		$("#main-right-table").append(order);
	}
		
		//动态给渲染职位赋初始值
		$("#main-left").css("min-height",507);
		//获取渲染工单区域的div高度
		var height = $("#content-recruitment").height();
		//初始化赋值div长度
		if(Initialize==true){
			//判断渲染工单长度大于等于三
			if(data.RecruitOrder.length>=3){
				//动态给渲染职位div赋值长度
				$("#main-left").css("min-height",height+50);
			}
			
			Initialize=false;
		}else{
				//动态给渲染职位赋初高度
				$("#main-left").css("min-height",height-14);
		}
		
	}else{
		tagState="";
		tagPosition="null";
		//查询职位
		selectPositions();
		$("#main-right-table").empty();
		$("#main-right-table").append("<div class='message_text'>此条件没有工单！</div>");
		$("#pagingToolbar").hide();//隐藏分页
	}
  }
	
});
	
}


//候选人姓名查询工单
function selOrders(){
	candidate_name = $("#candidate_name").val();
	recuit_order_id = $("#recuit_order_id").val();//单号
	org_name=$("#org_name").val();//部门
	startTime = $("#present_startTime").val();//审批开始时间
	endTime = $("#present_endTime").val();//审批结束时间
	//结束时间不能小于开始时间
	if(endTime<startTime && endTime!=""){
		//提示信息
		layer.open({
	        type: 1
	        ,offset:'auto' 
	        ,title:'提示'
	        ,id: 'layerDemo'
	        ,content: '<div style="padding:14px 20px 0px 34px">结束日期必须大于开始日期！</div>'
	        ,closeBtn: false//不显示关闭图标
	        ,btn:['确定']
	        ,btnAlign: 'c' //按钮居中
	        ,shade: 0 //不显示遮罩
	        ,yes: function(index){
	        	layer.close(index);
	        } 
	});
	}else{
		state =true;
		selectOrder(storagePosition,"",state);//查询工单
	}
	//查询职位
//	selectPositions();
	
}

function dianji(){

	$(".ji").slideToggle();
}


//当前任务分页条
function currentLaypages(count){
layui.use(['laypage', 'layer'], function(){	
	var laypage = layui.laypage
	 ,layer = layui.layer;
		laypage.render({
		   elem: 'pagingToolbar'
		   ,count:count
		   ,curr:orderPage
		   ,limit:orderLimit
		   ,layout: [ 'prev', 'page', 'next','skip']
		   ,jump: function(obj,first){
		    orderPage = obj.curr;//获取当前页面
		    if(!first){
		    	//执行查询已归档任务方法
		    	tagState="";
		    	tagePage =true;
		    	selectOrder(storagePosition);
		    }
	    }
	});
 });
	
}


//查询所有已归档的职位对应的工单信息
var archivePage =1;//起始页数
//判断工单明细返回的页数不等于空
if(archivedTaskPage!="" && archivedTaskPage!="null"){
	//工单明细返回的当前页数赋值给起始页数
	archivePage=archivedTaskPage;
}
var limit = 10;//显示条数
var count;//总条数
function archive(){
	var startTime = $("#startTime").val();//开始时间
	var endTime = $("#endTime").val();//结束时间
	//结束时间不能小于开始时间
	if(endTime<startTime && endTime!=""){
		//提示信息
		layer.open({
	        type: 1
	        ,offset:'auto' 
	        ,title:'提示'
	        ,id: 'layerDemo'
	        ,content: '<div style="padding:14px 20px 0px 34px">结束日期必须大于开始日期！</div>'
	        ,closeBtn: false//不显示关闭图标
	        ,btn:['确定']
	        ,btnAlign: 'c' //按钮居中
	        ,shade: 0 //不显示遮罩
	        ,yes: function(index){
	        	layer.close(index);
	        } 
	});
	}else{
	var oddState=$("#oddState").val();//状态
	var position = $("#position").val();//职位
	var org_name=$("#org_name_v2").val();//部门
	$.ajax({
		url:'com.recruit.process.process.queryPigeonholeProcess.biz.ext',
  		type: 'post',
  		data:{position:position,org_name:org_name,pigeonholeDataOld:startTime,pigeonholeDataNew:endTime,oddState:oddState,limit:limit,curr:archivePage},	   		
		dataType: 'json',
		success: function (data) {
			//获取总条数
			count=data.PageCond.count;
			//执行分页方法 传入一个参数
			laypage(count);
			//元素清空
			$("#archive-list").empty();
			//数据长度要大于0
			if(data.Positions.length>0){
			for(var i = 0;i<data.Positions.length;i++){
				
				//是否难招
				var isDifficult = orresult(data.Positions[i].is_difficult);
				//是否难招
				
				//是否实习生
				var isIntern = orresult(data.Positions[i].is_intern);
 	 	 			//roStatus工单当前状态 3已入职 4已取消 5候选人放弃 6修改负责人
		 	 	 	if(data.Positions[i].ro_status==3){
		 	 	 		data.Positions[i].ro_status="已入职";
		 	 	 	}else if(data.Positions[i].ro_status==4){
		 	 	 		data.Positions[i].ro_status="取消招聘";
		 	 	 	}else if(data.Positions[i].ro_status==5){
		 	 	 		data.Positions[i].ro_status="候选人放弃";
		 	 	 	}else if(data.Positions[i].ro_status==6){
		 	 	 		data.Positions[i].ro_status="修改负责人";
		 	 	 	}else if(data.Positions[i].ro_status==7){
		 	 	 		data.Positions[i].ro_status="已关联";
		 	 	 	}
		 	 	 	var jobCategory = Positioncatagory(data.Positions[i].job_category);//职位类别
		 	 	 	
        		var auditTime= data.Positions[i].audit_time;//审核时间
        		if(auditTime!=null){
					auditTime = auditTime.substring(0,10);
				}
        		var workArea =  data.Positions[i].work_area;//工作地点
				if(workArea==null){
					workArea="无";
				}
				//招聘原因
				var mainReason = data.Positions[i].main_reason;
				if(mainReason==null){
					mainReason="无";
				}
				 //部门 
		  		 var orgName = formatDepartment(data.Positions[i].L1_name,data.Positions[i].L2_name,data.Positions[i].L3_name,data.Positions[i].L4_name);
				
		  		//职位属性
		  		 var categoryAttribute  = data.Positions[i].category_attribute;
		  		 //职位属性 等于1 为计时  2为计件
		  		 if(categoryAttribute == 1){
		  			categoryAttribute = "计时";
		  		 }else if(categoryAttribute == 2){
		  			categoryAttribute = "计件";
		  		 }
		  		 //已关联工单
		  		var relevanceOrderId = data.Positions[i].relevance_order_id;
		  		if(relevanceOrderId == null){
		  			relevanceOrderId="无";
		  		}
		  		//oa流程号
		  		var oaProcessId =  data.Positions[i].oaProcess_id;
		  		if(oaProcessId == null){
		  			oaProcessId="无";
		  		}
				//循环拼接显示工单的内容
				var main="<div class='panel panel-default archive-panel'>";
					main+="<div class='panel-heading'>";
					//判断是否口头单还是oa单 （0是oa 1是口头）
					if(data.Positions[i].is_mannul_id ==1){
						main+="<h3 class='panel-title'>单号："+data.Positions[i].recuit_order_id+"<span class='layui-badge' style='position: absolute;margin-left: 4px;'>口头</span> <button type='button' id='button' class='btn btn-warning' onclick='clikDetail(\""+data.Positions[i].recuit_order_id+"\")'>单号明细</button></h3>";
					}else if(data.Positions[i].is_mannul_id ==0){
						main+="<h3 class='panel-title'>单号："+data.Positions[i].recuit_order_id+" <button type='button' id='button' class='btn btn-warning' onclick='clikDetail(\""+data.Positions[i].recuit_order_id+"\")'>单号明细</button></h3>";
						
					}
					main+="</div>";
					main+="<table class='table' style='table-layout:fixed;'>";
					main+="<tr>";
					main+="<td class='scroll padding' >部门：<span title="+orgName+">"+orgName+"</span></td>";
					main+="<td id='archive-auditTime'>审批日期："+auditTime+"</td>";
					main+="<td class='scroll' id='archivep-jobCategory' >职位类别：<span title='"+jobCategory+"'>"+jobCategory+"</span></td>";
					if(data.Positions[i].surplusPeriod<0){
		 			    main+="<td style='width: 180px'>剩余招聘周期：<span style='color:red'>"+data.Positions[i].surplus_period+"</span>天</td>";
		 			}else{
		 			    main+="<td style='width: 180px'>剩余招聘周期："+data.Positions[i].surplus_period+"天</td>";
		 			}   
					main+="<td style='width: 228px;'>职位属性："+categoryAttribute+"</td>";   
		 			main+="</tr>";
		 			    
		 			main+="<tr>";
		 			main+="<td id='archive-position' class='scroll padding'>职位：<span title="+data.Positions[i].position+">"+data.Positions[i].position+"</span></td>";
		 			main+="<td>招聘负责人："+data.Positions[i].recruiter_name+"</td>";
		 			main+="<td>招聘周期："+data.Positions[i].recruit_period+"天</td>";
		 			main+="<td id='archive-mainReason' class='scroll'>招聘原因：<span title='"+mainReason+"'>"+mainReason+"</span></td>";
		 			main+="<td class='scroll'>已关联工单：<span title='"+relevanceOrderId+"'>"+relevanceOrderId+"</span></td>";
		 			main+="</tr>";
		 			
		 			main+="<tr>";
		 			main+="<td class='padding'>OA流程号："+oaProcessId+"</td>";
		 			main+="<td class='scroll'>工作地点：<span title = '"+workArea+"'>"+workArea+"</span></td>";
 		 			main+="<td>是否实习生：";
 		 			main+="<div class='btn-group'>";
 		 			main+="<button type='button' id='btnInternship' class='btn button btn-default btn-button downMenu'>"+isIntern+"</button>";
 		 			main+=" </button>";
 		 			main+="</div>";
 		 			main+="</td>";
 		 			
 		 			main+="<td >是否难招：";
		 			main+="<div class='btn-group'>";
		 			main+="<button type='button' id='btnAttract' class='btn button btn-default btn-box btn-button downMenu'>"+isDifficult+"</button>";
 		 			main+=" </button>";
 		 			main+="</div>";
 		 			main+="</td>";
 		 			
 		 			main+="<td>当前状态：";
 		 			main+="<div class='btn-group'>";
 		 			main+="<button type='button' id='btnstate' class='btn button btn-default btn-button downMenu'>"+data.Positions[i].ro_status+"</button>";
 		 			main+="</button>";
 		 			main+="</div>";
 		 			main+="</td>";
 		 			main+="</tr>";	
 		 			main+="</table>";
 		 			main+="</div>";
		 		  
				$("#archive-list").append(main);
				
			}
		  }
		}
		
	})
}
}

//已归档任务分页条
/*
* count:总条数
* curr：当前页
* limit：显示条数
* */
function laypage(count){
layui.use(['laypage', 'layer'], function(){	
	var laypage = layui.laypage
	 ,layer = layui.layer;
		laypage.render({
		   elem: 'archive-pagingToolbar'
		   ,count:count
		   ,curr:archivePage
		   ,limit:limit
		   ,layout: [ 'prev', 'page', 'next','skip']
		   ,jump: function(obj,first){
		    archivePage = obj.curr;//获取当前页面
		    if(!first){
		    	//执行查询已归档任务方法
		    	archive();
		    }
	    }
		 });
  
});
	
}

//重置
function isReset(){

$(".import").each(function(i,obj){
      obj.value="";
});
}

	//回车事件
	document.onkeydown = function(e){
		var ev = document.all ? window.event : e;
		if(ev.keyCode==13) {
			archive();//执行查询已归档的任务的方法
		}
	};

var sort;//用来区分查询
var id;//工单id
var ordernum;//当前工单号
//选择关联工单 num区分元素id标识
function relevanceOrder(oneself,orderId,orderNum,num){
	ordernum = orderNum;
	id=orderId;
	sort = "curr";
	recuit_orderId =$(oneself).attr("data");//获取当前点击工单号
	var layer = layui.layer;
  	 layer.open({
	    area: ['84.3%', '90%'],
		type: 2,
		shade: 0.1,
		closeBtn: false,
		resize:false, //是否允许拉伸
		scrollbar:false,
		title:'选择关联工单',
		closeBtn: false,//不显示关闭图标
	    btn: ['选择', '关闭'],
		content: [basePath+'task/relevanceOrder_select.jsp?isCurrentTask=yes','no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
       yes: function(index,lay){
			var iframeWin = window[lay.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
            var ids = iframeWin.backPrice();//调用子页面的方法，得到子页面返回的ids 
           	var json = $.parseJSON(ids);
       	  layer.open({
		        type: 1
		        ,offset:'auto'
		        ,id: 'layerDemo'
		        ,closeBtn: false
		        ,resize:false //是否允许拉伸
		        ,content: '<div style="padding: 20px 50px 0px 50px;">工单号为：'+json.recuitOrderId+'</div><div style="padding: 15px 60px;">关联后不可取消，是否确认关联？</div>'
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,yes: function(index,layero){
		        	
              		$(oneself).text(json.recuitOrderId); //改变当前点击a标签的text
              		$(".rec_span").show();//显示审批日期
              		$("#relevanceauditTime").text(getDate(json.auditTime)); //改变工单后的span标签的text
              		$(oneself).attr("data",json.recuitOrderId); //改变当前点击a标签的text
              			var jsonStr ="";
              			
              				
              				var orderId =$("#relevance_order_id").attr("dataId");//获取原关联工单ID
              					jsonStr = {"new_Id":json.id,"old_Id":id,"orderId":orderId};
              					$("#relevance_order_id").attr("dataId",json.id); //改变工单后的a标签的dataId
              					$("#title"+num).attr("title",json.recuitOrderId); //改变工单后的span悬浮值
              					$("#relevance_order_id"+num).attr("onclick",""); 
                  				$("#relevance_order_id"+num).css("text-decoration","none");
              			
              				var result=	updaterelevanceOrder(jsonStr); //修改关联工单
              				layer.closeAll();//关闭
             			 if(result){
               				layer.msg("关联成功");
               			}else{
               				$("#relevanceauditTime").text(""); //改变工单后的span标签的text
               				$(".rec_span").hide();//隐藏审批日期
	                  		$(oneself).attr("data",""); //改变当前点击a标签的值为空
	                  		$(oneself).text(""); //改变当前点击a标签的值为空
	                  		layer.msg("关联失败");
               			}
		        }
			        ,btn2: function(index,layero){
			        	layer.close(index);//关闭
			        }
      			}); 
      		
			}
	});
}

//修改关联工单
function updaterelevanceOrder(json){
	var result =false;
     $.ajax({
     	url: basePath+'com.primeton.task.taskoperation.updateRelevanceOrder.biz.ext',
		type:'POST',
		data:json,
		async:false,
		dataType: 'json',
		success:function(data){
			if(data.MSGTYPE!="E"){
				result=true;
			}else{
				result=false;
			}
		}
     });
     return result;
}
//点击进入流程跳转流程的方法
function entranceProcess(order,position){
	window.location.href=basePath+"process/process_home.jsp?position="+ encodeURIComponent(position) +
		"&order=" + order + "&selPage=" + orderPage+ "&state=" + state+ "&candidate_name=" + candidate_name+
		"&recuit_order_id=" + recuit_order_id + "&startTime=" + startTime+ "&endTime=" + endTime;
}

    
