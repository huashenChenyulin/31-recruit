<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%@include file="/common/common.jsp" %>
<%
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-11 15:27:15
  - Description:
-->
<head>
 <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
   	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
  
   	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
<title>邮件模板创建</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    

<style type="text/css">
		
	.form-control{
		width: 88%;
		
	}
	textarea{
		resize : none;
	}
	.form{
		padding-left: 22px;
    	padding-top: 15px;
		width: 20%;
		float: left;
		
	}
	#LAY_demo1{
		float: left;
	}
	#content{
		margin-top: 15px;
		width: 80%;
		float: left;
	}
	#LAY_layedit_1{
		height: 418px!important;
	}
	.btn-save{
		margin-left: 44%;
		margin-top: 8px;
		
	}
	.layui-layedit{
		width: 99%;
	}
	.btn-attachments{
		height: 28px;
		font-size: 12px;
		line-height: 31px;
		width: 86%;
		background: rgb(230, 230, 230);
		color: black;
	}
	.btn-attachments:hover{
		color: black;
	}
	.file{
		height: 30px;
		background: #f9f9f9;
		line-height: 30px;
		width: 86%;
	}
	.file:hover{
		background: #dbedff;
	}
	.file .x1006{
		 font-size: 12px!important;
		 color: black!important;
		 float: right;
		 cursor: pointer;
	}
	 
	  
	#startUpload{
		display: none;
	}
	#LAY_demo1 img{
		height: 2px;
		width: 2px;
	}
	samp{
		color: red;
	}
</style>  
</head>
<body>
<div>
	<form id="form" class="form">
  <div class="form-group">
    <label for="name">标题</label><samp>*</samp>
    <input type="text" class="form-control" id="title"  onblur="validator()"  name = "title" placeholder="请输入标题">
  </div>
  
  <div class="form-group">
    <label for="name">模板类型</label><samp>*</samp>
    <select class="form-control" id = "type" name = "type"  onblur="validator()">
    </select>
  </div>
  
  <div class="form-group">
    <label for="name">模板名称</label><samp>*</samp>
    <textarea class="form-control" rows="3" id="templateName" name = "templateName"  onblur="validator()"></textarea>
  </div>
  <div id="upload-list" >
  	       
  	       
  </div>
 
			   
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
  
</div> 
</form>
<div id ="content">
	<textarea class="layui-textarea" id="LAY_demo1" style="display: none">  
	
</textarea> 
<button class="btn btn-warning btn-save" id="save" onclick="save()">保存</button>
			
</div>

  
	
</div>
<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>

<script type="text/javascript">

var prot = '<%=rooturl %>';//端口

var state ;//状态


//富文本编译器初始化
	var index;
	var layedit;
  	

 </script>
  <script src="<%= request.getContextPath() %>/mail/js/template_add.js?v=1.0"></script>

</body>
</html>