<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-27 08:22:59
  - Description:
-->
<head>
<title>所有模板</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  
    <style type="text/css">
    
    a{
  
    	    color: #f0ad4e;
    }
    a:hover{
    	    color: #f0ad4e;
    }
  
    #content{
    	border: 1px solid #ddd;
    	margin-left: auto;
    	margin-right: auto;
    	width: 86%;
    	
    }
    .table{
    	margin-bottom: 3px;
    }
    .btn-save{
     	margin-bottom: 4px;
    	margin-top: 7px;
   		margin-left: 7%;
    }
 
    </style>
</head>
<body>
<button class="btn btn-warning btn-save" onclick="addTemplate()">添加模板</button>
<div id ="content">

<table class="table" >
  <thead>
    <tr>
      <th>邮件模板名称</th>
      <th>类型</th>
      <th>操作</th>
    </tr>
  </thead>
  <tbody id= "tbody" >
   
  </tbody>
</table>

</div>
	<script type="text/javascript">
	$(function(){
		//查询邮件模板
		selectAllTemplate();
		//初始化layer
		var layer;
		layui.use('layer', function(){
		  	layer = layui.layer;
		});  
	});
	//查询邮件模板
	function selectAllTemplate(){
		$.ajax({
	  		url:'com.recruit.mail.mail.selectAllTemplate.biz.ext',
	  		type:'POST',
	  		dataType: 'json',
	  		success:function(data){
	  		//数据长度要大于0
	  		if(data.allTemplate.length>0){
	 			//清空元素以为所有的内容
  				$("#tbody").empty();
  				for(var i = 0; i<data.allTemplate.length;i++){
  				//tyoe 为邮件类型
  				var type = typeTurntypeName(data.allTemplate[i].type);
  					/* if(type==01){
  						type="节日问候";
  					}else if (type == 02){
  						type="邀约";
  					}else if (type == 03){
  						type="通知";
  					}else if (type == 04 ){
  						type="发送offer";
  					}else if (type == 05 ){
  						type="计划部门面试";
  					}else if (type == 06 ){
  						type="校招";
  					} */
  					var tr = "<tr><td>"+data.allTemplate[i].templateName+"</td><td>"+type+"</td><td><a href = '#' onclick='editorTemplate("+data.allTemplate[i].id+")'>编辑</a>&nbsp;&nbsp;<a href = '#' onclick='deleteTemplate("+data.allTemplate[i].id+",\""+data.allTemplate[i].templateName+"\")'>删除</a></td></tr>";
  					$("#tbody").append(tr);
  				}
  			  }
  			}
   		});
	}
   		
   		//修改邮件模板
   		//tempId 为邮件模板id
   		var tempId="";
   		function editorTemplate(id){
   			    tempId =id;
   			   //跳转页面
				layui.use('layer', function(){	
	 				 var layer = layui.layer;
					  	 layer.open({
						    area: ['1200px', '580px'],
							type: 2,
							shade: 0.8,
							title:'修改邮件模板',
							content: ['template_add.jsp','no'], 
							cancel:function(){
								tempId="";
							}
						}); 
					}); 
					
				}
		
		//删除邮件模板
		function deleteTemplate(id,templateName){
			var tempId = id;
			var data = {tempId:tempId};
			layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,content: '<div style="padding: 12px 39px 0px 34px;">是否删除模板为'+templateName+'？</div>'
		        ,closeBtn: false//不显示关闭图标
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,yes: function(index){
		        		 $.ajax({
					  		url:'com.recruit.mail.mail.deleteTemplate.biz.ext',
					  		type:'POST',
					  		data:data,
					  		dataType: 'json',
					  		success:function(data){
					  		//返回值success 为1是成功 否则为失败
	  							if(data.success==1){
	  							   layer.close(index);//关闭页面
	  								layer.msg("删除成功");
	  								//查询邮件模板
	  								selectAllTemplate();
	  								
	  							}else{
	  								layer.msg("删除失败");
	  							}
	  					}
			 	 		
			  		});
		        }
		        ,btn2: function(index,layero){
		         layer.close(index);//关闭页面
		        }
		});
			
	}
	
	//点击添加邮件模板（跳转）	
   function addTemplate(){
  		
   		layui.use('layer', function(){	
	 				 var layer = layui.layer;
					  	 layer.open({
						    area: ['1200px', '580px'],
							type: 2,
							shade: 0.8,
							title:'添加邮件模板',
							content: ['template_add.jsp','no'], 
						}); 
					}); 	
				
   }
  //全局存储邮件模板的类型
  var templateTypes;
  //获取业务字典邮件模板类型
  (function templateType(){
	$.ajax({
		url:'com.recruit.mail.mail.selectMailTemplateType.biz.ext',
		type:'POST',
		dataType:'json',
		success:function(data){
			//赋值给全局存储邮件模板的类型
			templateTypes =data.TemplateType;
		}
	});
})();

//邮件模板编码转邮件模板名称
function typeTurntypeName(val){
	var typeName;
	for(var i = 0 ;i<templateTypes.length;i++){
		if(val==templateTypes[i].dictID){
			typeName = templateTypes[i].dictName;
			break;
		}
	}
	return typeName;
}

    </script>
</body>
</html>