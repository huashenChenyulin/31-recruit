$(function(){
	//初始化查询邮件模板  03为通知
	selectNameMailModel("03,08");
	//查询待入职候选人
	selectCandidate();
	$("#send").click(function() {
		validator();
		//非空判断
		if(isPass==false){
			layer.msg(arr[0]);
			return;
		}
		//设置群发
      	IsMultiple = "true";
      	//标识是待入职人员群发
      	stateTag = "groupOf";
      	//发送邮件
      	sendMail();
	  });
	
	//选中事件
	$('#templateName').on('change', function (e) {
		//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
		selectNameMailModel($("#templateName option:selected").text()); 
	    return false;
	});
});

var isPass=true;//标识是否通过验证
var isPassMsg="";//非空验证提示输出
var arr=[];
//失去焦点验证
function validator(){
	isPassMsg="";
	isPass=true;
	arr=[];
	
	 ///收件人邮箱
	 
	  if($('#toMail').val()==null || $('#toMail').val()==""){
      	$("#toMail").css("border","1px solid red");
      	isPassMsg+="请输入收件人邮箱,";
      	isPass=false;
      }else{
      	$("#toMail").css("border","1px solid #ccc");
      }
	
	  //邮件模板
	  if($('#templateName option:selected').text()==null || $('#templateName option:selected').text()=="" ){
      	$("#templateName").css("border","1px solid red");
      	isPassMsg+="请输入邮件模板,";
      	isPass=false;
      }else{
      	$("#templateName").css("border","1px solid #ccc");
      }
	  //主题
	  if($('#title').val()==null || $('#title').val()=="" ){
	     $("#title").css("border","1px solid red");
	     isPassMsg+="请输入主题,";
	     isPass=false;
	  }else{
	     $("#title").css("border","1px solid #ccc");
	  }	
	 arr=isPassMsg.split(",");
} 



//点击部门群发事件
function orgMultiple(){
	//根据邮件模板类型查询邮件模板名称 03为通知类型
	selectNameMailModel("03,08");
	//隐藏群发邮件内容
 	$("#multiple-mail").hide();
 	//显示部门群发内容
 	$("#org-multiple-mail").show();
 }
 //点击群发邮件
 function multipleMail(){
	 //显示群发邮件内容
 	$("#multiple-mail").show();
 	//显示群发邮件内容
 	$("#org-multiple-mail").hide();
 }
 //查询待入职候选人
function selectCandidate(){
	//开始时间
	var startTime = $("#startTime").val();
	//结束时间
	var endTime = $("#endTime").val();
	var startDate = new Date(startTime);//开始时间
	var endDate = new Date(endTime);//结束时间
	if(startDate>endDate){
		layer.msg("结束时间必须大于开始时间");
		return;
	}
	var data = {startTime:startTime,endTime:endTime};
	$.ajax({
		url:'com.recruit.mail.mail.selectCandidate.biz.ext',
    		type:'POST',
    		dataType: 'json',
    		data:data,
    		success:function(data){
    		//将返回结果赋值给result
    		var result = data.candidate;
    			
    layui.use('table', function(){
  	var table = layui.table;
  
  //渲染数据表格
  /*
   * limit:显示条数
   * data：数据集
   * */
  table.render({
    elem: '#LAY_table_user'
    ,cols: [[
      {checkbox: true, fixed: true}
      ,{field:'candidate_name', title: '姓名', width:100, sort: true, fixed: true}
      ,{field:'planned_hire_date', title: '计划入职时间', width:120}
      ,{title: '部门', width:305 , templet:'#tempOrgName' }
      ,{field:'position', title: '职位', width:180}
      ,{field:'recuit_order_id', title: '工单号', width:200}
      ,{field:'recruiter_name', title: '负责人', width:100}
      ,{field:'candidate_mail', title: '邮箱', width:215}
     
    ]]
    ,id: 'LAY_table_user'
    ,limit:5
    ,page: true
    ,height: 315
    ,data:result
  });
});
    			
 }
		
});
	
}


/*var positionJson={} ;
var positionKey="";
var positionValue="";



//收件人
var toMail={} ;
var value="";
var text="";*/

var checkdata = ""; //复选框的数据
var studentNmae="";//学生姓名
var toResumeId="";//简历id
layui.use('table', function(){
  var table = layui.table;
  //监听表格复选框选择
  table.on('checkbox', function(obj){
	  
	/* //type 为all 是全选  checked 为true是选中
  	if(obj.type =="all" && obj.checked ==true){
  		//获取复选框选中的值
  		var checkStatus = table.checkStatus('LAY_table_user')
      	,data = checkStatus.data;
  		
  		//姓名跟邮箱
      	for(var i  = 0 ;i<data.length;i++){
     	 value =data[i].candidate_name;
	     text =data[i].candidate_mail;
	     var num=0;
	     for(var key in toMail){
	    	 if(key == value){
	    		 num++;
	    	 }
	     }
		if(num == 0){
    		 toMail[value] = text;
    		 var text2="";
    		 for(var key in toMail){
	  			text2+=key+"<"+toMail[key]+">,";
	  		} 
	     }
     	}
      	$("#toMail").val(text2.substring(0,text2.length-1));
      	
      	//姓名跟职位
      	for(var i  = 0 ;i<data.length;i++){
        	 positionKey =data[i].candidate_name;
        	 positionValue =data[i].position;
	   	     var num2=0;
	   	     for(var key in positionJson){
	   	    	 if(key == positionKey){
	   	    		 num2++;
	   	    	 }
	   	     }
	   		if(num2 == 0){
	       		 positionJson[positionKey] = positionValue;
	       		 var position3="";
	       		 for(var key in positionJson){
	       			position3+=positionJson[key]+",";
	   	  		} 
	       		toBeHiredPosition=position3;
	       		console.log(toBeHiredPosition);
	   	     }
        }
      	
     	
     	  
     //type 为one 是单选  checked 为true是选中
  	} else if(obj.type =="one"  && obj.checked==true){
  		 //当前行的姓名赋值给value
  		 value =obj.data.candidate_name;
  		 //当前行的邮箱赋值给text
	     text =obj.data.candidate_mail;
	     var num=0;
	     
	     for(var key in toMail){
	    	 if(key == value){
	    		 num++;
	    	 }
	     }
	    //num值等于0
		if(num == 0){
			//toMail 键为value（为姓名） 值为text（邮箱）
    		 toMail[value] = text;
    		 var text2="";
    		 for(var key in toMail){
	  			text2+=key+"<"+toMail[key]+">,";
	  		}
    		 
    		 $("#toMail").val(text2.substring(0,text2.length-1));
	     }
		
		 //当前行的姓名赋值给value
		positionKey =obj.data.candidate_name;
		 //当前行的职位赋值给value
   	 	positionValue =obj.data.position;
  	     var num2=0;
  	     for(var key in positionJson){
  	    	 if(key == positionKey){
  	    		 num2++;
  	    	 }
  	     }
  	     //num值等于0
  		if(num2 == 0){
  		//positionJson 键为positionKey（为姓名） 值为positionValue（职位）
      		 positionJson[positionKey] = positionValue;
      		 var position3="";
       		 for(var key in positionJson){
       			position3+=positionJson[key]+",";
   	  		} 
       		toBeHiredPosition=position3;
       		console.log(toBeHiredPosition);
  	     }
		//type 为one 是单选  checked 为false是取消选中
  	}else if(obj.type =="one" && obj.checked==false){
  		//删除邮箱的
  		delete toMail[obj.data.candidate_name];
  		//删除职位的
  		delete positionJson[obj.data.candidate_name];
  		var obj2="";
  		for(var key in toMail){
  			obj2+=key+"<"+toMail[key]+">,";
  		}
  		var position3="";
  		for(var key in positionJson){
  			position3+=positionJson[key]+",";
  		}
  		toBeHiredPosition=position3;
  		console.log(toBeHiredPosition);
  		$("#toMail").val(obj2.substring(0,obj2.length-1));
  	//type 为all 是全选  checked 为false是取消选中
  	}else if(obj.type =="all" && obj.checked ==false){
  		toMail={};
  		positionJson={};
  		toBeHiredPosition="";
  		$("#toMail").val("");
  	}
  	*/
	//获取当前页数据
  	var allCheckdata=table.cache['LAY_table_user'];
  	//判断是否全选，是否选中
  	if(obj.type=="all" && obj.checked==true){
  	  //循环遍历当前数据
  	  	for(var i=0;i<allCheckdata.length;i++){
  	  		
  	  		//获取名字+邮箱
  	  		var nameANDMail=allCheckdata[i].candidate_name+"<"+allCheckdata[i].candidate_mail+">,";
  	  		//获取姓名
  	  		var name=allCheckdata[i].candidate_name+",";
  	  		//获取简历id
  	  		var resumeId = allCheckdata[i].resume_id+",";
  	  		//获取职位
  	  		var position = allCheckdata[i].position+","
  	  		//判断 checkdata  参数是否包含 nameANDMail
  	  		if(checkdata.indexOf(nameANDMail)== -1){
  	  			
  	  			checkdata+=nameANDMail;
  	  			studentNmae+=name;
  	  			toResumeId+=resumeId;
  	  			toBeHiredPosition+=position;
  	  			
  	  		$("#toMail").val(checkdata.substring(0,checkdata.length-1));
  	  		}
  	  		
  	  	}
  	  //判断是否全选，是否取消选中
  	}else if(obj.type=="all" && obj.checked==false){
  		
  		//循环遍历当前数据
  		for(var i=0;i<allCheckdata.length;i++){
  	  		//获取名字+邮箱
  	  		var nameANDMail=allCheckdata[i].candidate_name+"<"+allCheckdata[i].candidate_mail+">,";
  	  		//获取姓名
  	  		var name=allCheckdata[i].candidate_name+",";
  	  		//获取简历id
  	  		var resumeId = allCheckdata[i].resume_id+",";
  	  		//获取职位
  	  		var position = allCheckdata[i].position+","

  	  		//删除对应的数据
  	  		checkdata = checkdata.split(nameANDMail).join("");
	        	studentNmae=checkdata.split(name).join("");
	        	toResumeId =checkdata.split(resumeId).join("");
	        	toBeHiredPosition=checkdata.split(position).join("");
	        	$("#toMail").val(checkdata);
  	  	}
  	}else{
  	
	        if(obj.checked==false){
	        	checkdata = checkdata.split(obj.data.candidate_name+"<"+obj.data.candidate_mail+">,").join("");
	        	$("#toMail").val(checkdata);
	        	studentNmae=checkdata.split(obj.data.candidate_name).join("");
	        	toResumeId = checkdata.split(obj.data.resume_id).join("");
	        	toBeHiredPosition= checkdata.split(obj.data.position).join("");
	        }else{
	        	checkdata+=obj.data.candidate_name+"<"+obj.data.candidate_mail+">,";
	        	$("#toMail").val(checkdata.substring(0,checkdata.length-1));
	        	studentNmae+=obj.data.candidate_name+",";
	        	toResumeId+=obj.data.resume_id+",";
	        	toBeHiredPosition+= obj.data.position+",";
	        }
  	}
  });
  
  });


//用来存储点击时间控件的时间
var tagTime = "";
//用来存储邮件模板名称
var tagName = "";
//根据邮件模板类型查询邮件模板
 function selectNameMailModel(parameterVal){
	 
	 //获取邮件模板名称
	 tagName = parameterVal;
	 //获取存储的控件时间
	 var invitationTime = tagTime;
 	if(parameterVal==""){
 			//清空元素的值
 			$("#title").val("");
 			layedit.setContent(index,"");
 			$(".files").empty();
 	}else{
 	
	if(parameterVal=="03,08"){
		data= {type:parameterVal};
	}else{
		//用来区分可以替换模板标识
		var tag = "replace";
		//标识发送的需要使用替换
		state="send";
  		data = {templateName:parameterVal,prot:prot,time:invitationTime,state:state,name:name,position:position,tag:tag
  				,userMail:userMail,userPhone:userPhone};
	}
 	
 	
 	$.ajax({
			url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  			type:'POST',
  			data:data,
  			dataType: 'json',
  		success:function(data){
	  		if(data.mailTemplate.length>0){
	  
  			if(parameterVal=="03,08"){
  				//动态为邮件模板赋值
  				$("#templateName option").remove();
  	  			if(parameterVal=="03,08"){
  	  				//动态为邮件模板赋值
  	  				var option="";
  	  				option =('<option value=""></option>');
  	  					for(var i = 0;i<data.mailTemplate.length;i++){
  	  					 option += ("<option value='"+data.mailTemplate[i].id+"'>"+data.mailTemplate[i].templateName+"</option>");
  	  					}
  	  					$("#templateName").append(option);
  	  			}
  					
  			}else{
  				//清空元素的值
				$("#title").val("");
	  			$(".files").empty();
  		
	  			//邮件主题不等于空
  			if(data.mailTemplate[0].title!=null){
  				//赋值邮件主题
  				$("#title").val(data.mailTemplate[0].title);
  			}
  			//访问地址
  		 	var fileUrl = data.mailTemplate[0].fileUrl;
  		 	//根据附件地址渲染附件
  		 	showFile(fileUrl);
  		 	//赋值富文本编辑邮件正文
  			layedit.setContent(index,data.updatePortTemplate);
  		 }
  		}
  	  }
	});
  }
 }
//加载时间控件
layui.use('laydate', function(){
	  var startTime = layui.laydate;
	  var endTime= layui.laydate;
	  //执行一个laydate实例
	  startTime.render({
	    elem: '#startTime'
	  });
	  endTime.render({
	    elem: '#endTime'
	  });
});

//重置
function isReset(){
	$(".import").each(function(i,obj){
          obj.value="";
    });
}


//初始值默认false 不调用查询邮件模板，为true的话调用查询邮件模板
var initial = false;
//加载时间控件
layui.use('laydate', function(){
	  var time = layui.laydate;
	  //执行一个laydate实例
	  time.render({
	    elem: '#invitationTime',
	    min: dateString,
	    type:'datetime',
	    done: function(value, date, endDate){
  		if(value!=null && value!==""){
  			//时间
			tagTime = value;
    			if(tagName!=""){
    				//根据邮件名称查询邮件模板
    				selectNameMailModel(tagName);
    			}
  			$("#templateName").attr("disabled",false);
  		}
	} 
  });
});