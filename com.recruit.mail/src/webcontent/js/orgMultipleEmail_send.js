$(function(){
	//根据邮件模板类型去查询邮件模板名称
	selectNameMailModel("03,08");
	
	
	$("#send").click(function () {//非submit按钮点击后进行验证，如果是submit则无需此句直接验证  
	       
		validator();
		//非空判断
		if(isPass==false){
			layer.msg(arr[0]);
			return;
		}
		IsMultiple = "true";
		//标识是部门群发
		stateTag="orgMutiple";
		//发送邮件模板
    	sendMail();
		
    });
	
	//选中事件
	$('#templateName').on('change', function (e) {
		//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
		selectNameMailModel($("#templateName option:selected").text()); 
	    return false;
	});
});
var isPass=true;//标识是否通过验证
var isPassMsg="";//非空验证提示输出
var arr=[];
//失去焦点验证
function validator(){
	//debugger
	isPassMsg="";
	isPass=true;
	arr=[];
	
	 ///收件人邮箱
	 
	  if($('#toMail').val()==null || $('#toMail').val()==""){
      	$("#toMail").css("border","1px solid red");
      	isPassMsg+="请输入收件人邮箱,";
      	isPass=false;
      }else{
      	$("#toMail").css("border","1px solid #ccc");
      	toResumeId=$("#toMail").attr("empid");
      }
	
	
	  //邮件模板
	  if($("#templateName option:selected").text()==null || $("#templateName option:selected").text()=="" ){
      	$("#templateName").css("border","1px solid red");
      	isPassMsg+="请输入邮件模板,";
      	isPass=false;
      }else{
      	$("#templateName").css("border","1px solid #ccc");
      }
	  //主题
	  if($('#title').val()==null || $('#title').val()=="" ){
	     $("#title").css("border","1px solid red");
	     isPassMsg+="请输入主题,";
	     isPass=false;
	  }else{
	     $("#title").css("border","1px solid #ccc");
	  }	
	 arr=isPassMsg.split(",");
} 

//查询邮件模板
 function selectNameMailModel(parameterVal){
 
 	if(parameterVal==""){
		//清空
 			$("#title").val("");
 			layedit.setContent(index,"");
 			$(".files").empty();
 	}else{
 	
 
	if(parameterVal=="03,08"){
		data= {type:parameterVal};
	}else{
		//用来区分可以替换模板标识
		var tag = "replace";
		//标识发送的需要使用替换
		state="send";
  		data = {templateName:parameterVal,prot:prot,state:state,name:name,position:position,
  				userMail:userMail,userPhone:userPhone,tag:tag};
	}
 	
 	$.ajax({
			url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  			type:'POST',
  			data:data,
  			dataType: 'json',
  		success:function(data){
  		
  			if(data.mailTemplate.length>0){
  				
  				
  			//parameterVal值为03 是  03类型编码 为通知类型 
  			if(parameterVal=="03,08"){
  				//动态邮件模板下拉框赋值
  					for(var i = 0;i<data.mailTemplate.length;i++){
	  			var option = ("<option value='"+data.mailTemplate[i].id+"'>"+data.mailTemplate[i].templateName+"</option>");
	  			$("#templateName").append(option);
	  			
  			}
  					
  		
  			}else{
			$("#title").val("");
  			$(".files").empty();
  		
  			if(data.mailTemplate[0].title!=null){
  				$("#title").val(data.mailTemplate[0].title);
  				
  			}
  				//访问地址
  		 	var fileUrl = data.mailTemplate[0].fileUrl;
  		 	//渲染附件
  		 	showFile(fileUrl);
  			
  			layedit.setContent(index,data.updatePortTemplate);
  		  }
  		 }
  		}
	 });
   }
 }
 
 