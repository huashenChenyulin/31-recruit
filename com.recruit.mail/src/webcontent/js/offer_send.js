/*//查询offer类型的邮件模板 (04是发送offer类型)*/
selectOfferTemplat("04");
function selectOfferTemplat(val){
	var type = val;//04 代表 offer的类型
	var data={type:type};
		$.ajax({
			url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  			type:'POST',
  			data:data,
  			dataType: 'json',
  			success:function(data){
  			//返回数据的长度要大于0
  			if(data.mailTemplate.length>0){
  		
  			//循环给offer邮件模板赋值
  				var option ='<option value=""></option>';
  			for(var i = 0;i<data.mailTemplate.length;i++){
  					option+= "<option>"+data.mailTemplate[i].templateName+"</option>";
  			}
  			$("#templateName").append(option);
  		  }
  		}
	});
}
//用来存储邮件模板名称
var tagName = "";
//用来存储点击时间控件的时间
var tagTime = "";
//根据offer邮件名称去查询邮件模板
function selectNameMailModel(parameterVal){
	var tag = "replace";
	tagName = parameterVal;
	var hireDate = tagTime;
 	if(parameterVal==""){
 			$("#title").val("");
 			layedit.setContent(index,"");
 			$("#upload-list").empty();
 	}else{
 	
  		data = {templateName:parameterVal,prot:prot,state:state,name:name,
  				position:position,tag:tag,time:hireDate,userMail:userMail,userPhone:userPhone};
 	$.ajax({
			url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  			type:'POST',
  			data:data,
  			dataType: 'json',
  		success:function(data){
			$("#title").val("");
  			$("#upload-list").empty();
  			//返回数据title值不能为空
  			if(data.mailTemplate[0].title!=null){
  				$("#title").val(data.mailTemplate[0].title);
  				
  			}
  				//访问地址
  		 	//附件地址
  			var filesUrl = data.mailTemplate[0].fileUrl;
  			//渲染附件
  			showFile(filesUrl);
  			layedit.setContent(index,data.updatePortTemplate);
  		}
  		
	});
	}
 }
var isPass=true;//标识是否通过验证
var isPassMsg="";//非空验证提示输出
var arr=[];
//失去焦点验证
function validator(){
	isPassMsg="";
	isPass=true;
	arr=[];
	
	 //候选人邮箱
	  var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
	  if($('#toMail').val()==null || $('#toMail').val()=="" || !email.test($("#toMail").val())){
      	$("#toMail").css("border","1px solid red");
      	isPassMsg+="请输入正确的邮箱格式-如：XXX@qq.com";
      	isPass=false;
      }else{
      	$("#toMail").css("border","1px solid #ccc");
      }
	  //计划入职时间
	  if($('#plan-hire-date').val()==null || $('#plan-hire-date').val()=="" ){
      	$("#plan-hire-date").css("border","1px solid red");
      	isPassMsg+="请输入邀约时间,";
      	isPass=false;
      }else{
      	$("#plan-hire-date").css("border","1px solid #ccc");
      }
	  //offer邮件模板
	  if($('#templateName').val()==null || $('#templateName').val()=="" ){
      	$("#templateName").css("border","1px solid red");
      	isPassMsg+="请输入邮件模板类型,";
      	isPass=false;
      }else{
      	$("#templateName").css("border","1px solid #ccc");
      }
	  //主题
	  if($('#title').val()==null || $('#title').val()=="" ){
	     $("#title").css("border","1px solid red");
	     isPassMsg+="请输入主题,";
	     isPass=false;
	  }else{
	     $("#title").css("border","1px solid #ccc");
	  }	
	 arr=isPassMsg.split(",");
} 

//点击触发发送方法
function send(){
	validator();
	//非空判断
	if(isPass==false){
		layer.msg(arr[0]);
		return;
	}
	//标识状态类型
	stateTag = "offer";
	//发送邮件
	sendMail();
   }
	

//初始值默认false 不调用查询邮件模板，为true的话调用查询邮件模板
var initial = false;
//加载时间控件
layui.use('laydate', function(){
	  var startTime = layui.laydate;
	  var endTime= layui.laydate;
	  //执行一个laydate实例
	  startTime.render({
	    elem: '#plan-hire-date',
	    min: dateString,
	    type:'datetime',
	    done: function(value, date, endDate){
    		if(value!=null && value!==""){
	    			//时间
	    			tagTime = value;
	    			if(tagName!=""){
	    				//根据邮件名称查询邮件模板
	    				selectNameMailModel(tagName);
	    				
	    			}
    			$("#templateName").attr("disabled",false);
    		}
  		} 
	    
	  });
	
});

