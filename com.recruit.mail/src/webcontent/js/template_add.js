
var tempId = parent.tempId;//获取副层的模板id
$(function(){
	layui.use('layedit', function(){
	    layedit = layui.layedit
	  	,$ = layui.jquery;
	  	 layedit.set({
	   		      uploadImage: {
	                url:server_context+'/interpolation/file_uploadserver.jsp' //接口url
	                ,type: 'post'//默认post
	            }
	        });
	   
	  //构建一个默认的编辑器
	  index= layedit.build('LAY_demo1');
	  //查询邮件模板
	  selectTemplate();
	  //动态赋值邮件模板类型
	  templateType();
	});  
	
});
var isPass=true;//标识是否通过验证
var isPassMsg="";//非空验证提示输出
var arr=[];
//失去焦点验证
function validator(){
	isPassMsg="";
	isPass=true;
	arr=[];
	
	  //标题
	  if($('#title').val()==null || $('#title').val()=="" ){
      	$("#title").css("border","1px solid red");
      	isPassMsg+="请输入标题,";
      	isPass=false;
      }else{
      	$("#title").css("border","1px solid #ccc");
      }
	  //模板类型
	  if($('#type').val()==null || $('#type').val()=="" ){
      	$("#type").css("border","1px solid red");
      	isPassMsg+="请输入模板类型,";
      	isPass=false;
      }else{
      	$("#type").css("border","1px solid #ccc");
      }
	  //模板名称
	  if($('#templateName').val()==null || $('#templateName').val()=="" ){
	     $("#templateName").css("border","1px solid red");
	     isPassMsg+="请输入模板名称,";
	     isPass=false;
	  }else{
	     $("#templateName").css("border","1px solid #ccc");
	  }	
	 arr=isPassMsg.split(",");
} 

//保存
function save(){
	validator();
	//非空判断
if(isPass==false){
	layer.msg(arr[0]);
	return;
}
//debugger
	//tempId 值为空时，为新增 否则为编辑
	if(tempId==null || tempId==""){
		state="save";
    	var title = $("#title").val();//标题
    	var type = $("#type").val();//模板类型
    	var templateName = $("#templateName").val();//模板名称
    	//附件
    	var filesUrl="";
    	var state1 =$(".imgWrap").text() ;
    	//遍历附件地址
    	 $(".afile").each(function(i,obj){
   			filesUrl+= getUrl($(obj).attr("href"))+",";
   		  	
  		 });
    	 //state1值 不等于空为  （判断附件是否上传）
  		 if(state1!=null && state1!=""){
   	   		layer.msg("附件未上传");
   	   }else{
   	   
   		 //去除附件url的最后一个逗号
		filesUrl = filesUrl.substring(0,filesUrl.length-1);
    	var content = layedit.getContent(index);//模板内容
    	
     	var data = {title:title,templateName:templateName,type:type,content:content,fileUrl:filesUrl,rootUrl:prot,state:state};
    	$.ajax({
    		url:'com.recruit.mail.mail.addMailTemplate.biz.ext',
    		type:'POST',
    	   	data:data,
    		success:function(data){
    			//out值 等于 1 为成功 否则失败
    			if(data.out==1){
    				//调用父层的查询邮件模板加载
    				parent.selectAllTemplate();
    				parent.layer.closeAll();
	  				parent.layer.msg("保存成功");
	  				
    			}else{
    				layer.msg("保存失败");
    			}	
    			
    		}
    		
    	});
       }
    }else{
    	//修改邮件模板
    	var upFilesUrl="";
    	var state1 =$(".imgWrap").text() ;
    	$(".afile").each(function(i,obj){
    	
   				upFilesUrl+= getUrl($(obj).attr("href"))+",";
   		  	
  		 });
  		 if(state1!=null && state1!=""){
   	   		layer.msg("附件未上传");
   	   }else{
    	upFilesUrl = upFilesUrl.substring(0,upFilesUrl.length-1);
    	
    	state = "save";
   	    var upTitle = $("#title").val();//标题
    	var upType = $("#type").val();//模板类型
    	var upTemplateName = $("#templateName").val();//模板名称
    	var upContent = layedit.getContent(index);//模板内容
    	var data ={templateName:upTemplateName,type:upType,content:upContent,title:upTitle,id:tempId,port:prot,state:state,fileUrls:upFilesUrl};
    	$.ajax({
		url:'com.recruit.mail.mail.updateTemplat.biz.ext',
	  		type:'POST',
	  		dataType: 'json',
	  		data:data,
	  		success:function(data){
	  			if(data.success ==1){
	  				
	  				///清空父层的邮件模板id
	  				parent.tempId="";
	  				//调用父层的查询邮件模板加载
    				parent.selectAllTemplate();
	  				parent.layer.closeAll();
	  				parent.layer.msg("保存成功");
	  			}else{
	  				layer.msg("保存失败");
	  			}
	  	}
	  });
	  }
    }
   
}
//根据模板id查询相对应模板 tempId值不为空则编辑
function selectTemplate(){
	//tempId 模板id 值不能为空
	if(tempId!=null && tempId!=""){
		state = "update";
		var data = {tempId:tempId,state:state,prot:prot};
		
		$.ajax({
			url:'com.recruit.mail.mail.selectAllTemplate.biz.ext',
		  		type:'POST',
		  		data:data,
		  		dataType: 'json',
		  		success:function(data){
		  			$("#title").val(data.allTemplate[0].title);
		  			$("#type").val(data.allTemplate[0].type);
		  			$("#templateName").val(data.allTemplate[0].templateName);
		  		
	  		 	//附件地址
	  			var filesUrl = data.allTemplate[0].fileUrl;
	  			 //渲染附件
	  			showFile(filesUrl);
	  			//给富文本编辑器赋值
		  		layedit.setContent(index,data.TemplateRepTag);
	  			}
	  		});
	}
	
}

//动态赋值邮件模板类型
function templateType(){
	$.ajax({
		url:'com.recruit.mail.mail.selectMailTemplateType.biz.ext',
		type:'POST',
		dataType:'json',
		success:function(data){
			$("#type").empty();
			//返回数据的长度要大于0
			if(data.TemplateType.length>0){
				//动态赋值邮件模板类型
				var option = "<option></option>";
				for(var i = 0 ;i<data.TemplateType.length;i++){
					option+="<option value="+data.TemplateType[i].dictID+">"+data.TemplateType[i].dictName+"</option>";
				}
				$("#type").append(option);
			}
		}
	});
}


