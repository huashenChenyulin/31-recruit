//根据文件名截取附件地址
  	function getUrl(obj){
  		var configPath="resume/";
  		//获取文件名在第几位
  		var index=obj.lastIndexOf(configPath);
  		//截取附件地址
  	   	obj=obj.substring(index,obj.length);
  	    return obj;
    }
  	
  	 //截取附件名
    function getFiles(obj){
    	var files = new Array();
    	if(obj!=""&&obj!=null){
    		var objFiles= new Array(); //定义一数组 
    		//字符串转换数组
    		objFiles=obj.split(",");
  	    for(var i = 0;i<objFiles.length;i++){
  	   		var file  = objFiles[i];
  	   		//获取/的位置
  	   		var pos = file.lastIndexOf("/");
  	   		//截取/后面的内容
  	   		var subFile  = file.substring(pos+1);
  	   		//获取.的位置
  	   		var pos1 = subFile.lastIndexOf(".");
  	   		//截取. 后面的内容
  	   		var subSuffix=subFile.substring(pos1);
  	   		
  	   		files[i] = (subFile.substring(pos1-13,subFile)+subSuffix); 	
  	    } 
     }
     return files;  
  }

 var j = 0;//标识id
 //渲染附件
 function showFile(filesUrl){
    var fileUrl =  new Array();
    //判断附件地址不能为null
    	if(filesUrl!=""&&filesUrl!=null){
    	 	fileUrl=filesUrl.split(",");
    	 }
    	 	if(fileUrl.length>0){
    	 		//附件名称
    	 		var filesName = getFiles(filesUrl);
    	 		//循环渲染附件
    	 		for(var i = 0 ;i<filesName.length;i++){
    	 		var accessUrl = prot+fileUrl[i]; 
    			var div =("<div class= 'files' id='div-file"+j+"'> <a class='afile' data='' id='file"+j+"'  target ='_blank' href='"+accessUrl+"' >"+filesName[i]+"</a>  <i  onclick= 'delFile("+j+")' class='layui-icon i-delete'>&#xe640;</i></div>");
    			$("#upload-list").append(div);
    			j++;
    		 }
    	}
  }
 
 
 
//发送邮件
function sendMail(sign){
	//获取元素的值
	state =$(".imgWrap").text();
	// 值不等于null
	if(state!=null && state!=""){
		layer.msg("附件未上传");
	}else{
	//提示框
	layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content: '<div style="padding: 20px 75px 0px;">是否发送？</div>'
		        ,closeBtn: false//不显示关闭图标
		        ,btn:['确定','关闭']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,yes: function(index){
		         layer.close(index);//关闭页面
		         //邮件内容
		         mailContent(sign);
		        }
		        ,btn2: function(index,layero){
		         layer.close(index);//关闭页面
		        }
	 
	});
  }
}


//是否群发
var IsMultiple;
//标识是发送是什么功能点进来的（邀约，offer，转发用人部门，候选人群发，部门群发）
var stateTag;
//用来群发赋值待入职人员职位
var toBeHiredPosition="";
//获取邮件内容并且执行发送
function mailContent(sign){
	//附件地址
	var filesUrl="";
	
	//迭代元素
	$(".afile").each(function(i,obj){
		//赋值附件地址
		filesUrl+= getUrl($(obj).attr("href"))+",";
	});
	
	var type=$("#type").val();
	var category=0;
	//判断是否问卷调查类型
	if(type=="08" || type==08){
		category=1;
	}
	
	var tempId=$("#templateName option:selected").text();
	var temp_id=$("#templateName").val();
	//判断是否问卷模版类型
	if(tempId=="面试官满意度调查" || tempId=="校园招聘学生满意度调查"){
		category=1;
	}
	 //模板内容
	var text = layedit.getContent(index);
	//主题
	var theme = $("#title").val();
	//收件人邮箱
	var toMail = $("#toMail").val();
	//抄送人
	var ccMail = $("#ccMail").val();
	//密送人
	var bccMail = $("#bccMail").val();
	//职位
	var position ="";
	//群发待入职人员职位不能为空
	if(toBeHiredPosition!=""){
		position=toBeHiredPosition;
		//去除字符串最后一个字符为（,）
		position = position.substring(0, position.length-1);
	}
	//判断是否发送邀约，发送offer点进来的
	if(stateTag=="invitation" || stateTag=="offer"){
		//赋值候选人名称
		name=name;
	}
	var data = {"text":text,"theme":theme,"filesUrl":filesUrl,
				"toMail":toMail,"IsMultiple":IsMultiple,"ccMail":ccMail,
				"bccMail":bccMail,"position":position,"state":stateTag,"name":name,"resume_id":toResumeId,"tempId":temp_id,"category":category};
	$.ajax({
		url:'com.recruit.mail.mail.sendMali.biz.ext',
		type:'POST',
		data:data,
		async: false,
		success:function(data){
			
			if(data.success=="没有用户邮箱"){
				//定时弹框
				setTimeout(function(){
					layer.open({
				        type: 1
				        ,offset:'auto' 
				        ,id: 'layerDemo'
				        ,content: '<div style="padding: 18px 30px 10px 33px;">请到【功能设置】里的【个人设置】，设置您的邮箱信息</div>'
				        		 +'<div style="text-align: center;padding: 5px 0px 0px 0px;">谢谢</div>'
				        ,closeBtn: false//不显示关闭图标
				        ,btn:['确定']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index){
				        	layer.close(index);
				        }
				        
					});
					
					
				},500);
					
		}else{
			//success 为1  代表成功
			if(data.success==1){
				//计划待入职
				var planHireDate =  $("#plan-hire-date").val();
				//状态值为offer
				if(stateTag=="offer"){
					//调用赋值的方法
					parent.OfferCallback(planHireDate);
				}
				//状态值为invitation 会执行修改候选人最新计划人力面试时间的方法
				if(stateTag=="invitation"){
					updateCandidateTime();
				}
				//关闭
				if(sign=='1'){
					parent.parent.layer.closeAll();
					
					parent.parent.layer.msg("发送成功");
				}else{
					parent.layer.closeAll();
					
					parent.layer.msg("发送成功");
				}
				
				
			}else if(data.success=="工号或密码错误"){
				setTimeout(function(){
					layer.open({
				        type: 1
				        ,offset:'auto' 
				        ,id: 'layerDemo'
				        ,content: '<div style=" padding: 11px 12px 0px 26px;">您的密码有误！请到【功能设置】里的【个人设置】</div>'
				        +'<div style="text-align: center;padding-top: 11px;">修改您的邮箱信息！</div>'
				       		 +'<div style="text-align: center;padding: 5px 0px 0px 0px;">谢谢！</div>'
				        ,closeBtn: false//不显示关闭图标
				        ,btn:['确定']
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(index){
				        	empId=empCode;
							
							layer.close(index);
				        }
					});
				},500);
				
}else if(data.success=="发送错误"){
	setTimeout(function(){
		layer.open({
	        type: 1
	        ,offset:'auto' 
	        ,id: 'layerDemo'
	        ,content: '<div style=" padding: 13px 51px 0px 46px;">您的邮箱有误或附件异常！</div>'
	        	+'<div style="text-align: center;padding-top: 11px;">请联系管理员,谢谢！</div>'
	        ,closeBtn: false//不显示关闭图标
	        ,btn:['确定']
	        ,btnAlign: 'c' //按钮居中
	        ,shade: 0 //不显示遮罩
	        ,yes: function(index){
	        	
	 		 layer.close(index);
	        }
	        
	    });
	},500);
	
  }
 }
}
});
	
}

$(function(){
	
	//添加或删除抄送人
	$(".addCC").click(function(){
	//添加
		if($(this).attr("data")=="on"){
		//改变 a 标签的值
			$(this).text("删除抄送人");
			$(this).attr("data","off");
		//显示抄送人邮箱
			$(".ccMail").css("display","block");
	//删除
		}else if($(this).attr("data")=="off"){
		//改变 a 标签的值
			$(this).text("添加抄送人");
			$(this).attr("data","on");
		//隐藏抄送人邮箱并清空值
			$(".ccMail").css("display","none");
			$("#ccMail").val("");
		}
	  });
	  //添加或删除密送人
	$(".addBCC").click(function(){
	//添加
		if($(this).attr("data")=="on"){
		//改变 a 标签的值
			$(this).text("删除密送人");
			$(this).attr("data","off");
		//显示密送人邮箱
			$(".bccMail").css("display","block");
	//删除
		}else if($(this).attr("data")=="off"){
		//改变 a 标签的值
			$(this).text("添加密送人");
			$(this).attr("data","on");
		//隐藏密送人邮箱并清空值
			$(".bccMail").css("display","none");
			$("#bccMail").val("");
		}
	  });
});