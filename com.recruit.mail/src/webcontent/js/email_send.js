

$(function(){
	//获取父层的计划人资面试时间
	var nextTime = parent.humanNextTime;
	//计划人资面试时间不等于空
	if(nextTime!=""){
		//给邀约时间赋值
		$("#invitationTime").val(nextTime);
		tagTime = nextTime;
		//邮件模板类型不禁用
		$("#type").attr("disabled",false);
	}
	//选中事件
	$('#templateName').on('change', function (e) {
		//根据模板名称去查询模板内容(模板名称,面试地点,邀约时间)
		selectNameMailModel($("#templateName option:selected").text()); 
	    return false;
	});
});


//按模板类型查询相对邮件模板   parameterVal:邮件模板类型编码
function selectTypeMailModel(parameterVal){
	
	//邮件类型为空
	if(parameterVal==""){
		//清空每一个元素的值
		$("#templateName").attr("disabled",true);
		$("#title").val("");
  		$("#upload-list").empty();
  		$("#templateName")[0].length=0;
  		layedit.setContent(index,"");
	}else{
		//清空元素一下的内容
		$("#upload-list").empty();
		
		$("#templateName").attr("disabled",false);
		
		var data = {type:parameterVal};
  		$.ajax({
  			url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  			type:'POST',
  			data:data,
  			dataType: 'json',
  			success:function(data){
  				$("#templateName")[0].length=0;
  				//返回的数据值的长度要大于0
  				if(data.mailTemplate.length>0){
  					
  					//循环为选择邮件模板类型下拉框赋值
  					for(var i = 0;i<data.mailTemplate.length;i++){
  						var option = ("<option value='"+data.mailTemplate[i].id+"'>"+data.mailTemplate[i].templateName+"</option>");
  						$("#templateName").append(option); 
  					}
  				}
  				
  				//根据模板名称去查询邮件模板
  				selectNameMailModel(data.mailTemplate[0].templateName);
  			}
  		});
	} 
} 

//用来存储邮件模板名称
var tagName = "";
//用来存储点击时间控件的时间
var tagTime = "";
//根据模板名称去查询邮件模板  parameterVal:邮件模板名称
function selectNameMailModel(parameterVal){
	//获取邮件模板名称
	tagName = parameterVal;
	//获取存储的控件时间
	var invitationTime = tagTime;
	//用来区分可以替换模板标识
	var tag = "replace";
  	data = {templateName:parameterVal,prot:prot,state:state,name:name,
  			position:position,tag:tag,time:invitationTime,userMail:userMail,userPhone:userPhone};
	$.ajax({
  		url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
  		type:'POST',
  		data:data,
  		dataType: 'json',
  		success:function(data){
  			$("#upload-list").empty();
  			//title：值不能等于null
  			if(data.mailTemplate[0].title!=null){
  				$("#title").val(data.mailTemplate[0].title);
  			}
  			//附件地址
  			var filesUrl = data.mailTemplate[0].fileUrl;
  			 //渲染附件
  			showFile(filesUrl);
  			//给富文本编辑器赋值
  			layedit.setContent(index,data.updatePortTemplate); 
  		}
  	});
}


	var isPass=true;//标识是否通过验证
	var isPassMsg="";//非空验证提示输出
	var arr=[];
	//失去焦点验证
	function validator(){
		isPassMsg="";
		isPass=true;
		arr=[];
		
		 //候选人邮箱
		  var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
		  if($('#toMail').val()==null || $('#toMail').val()=="" || !email.test($("#toMail").val())){
	      	$("#toMail").css("border","1px solid red");
	      	isPassMsg+="请输入正确的邮箱格式-如：XXX@qq.com";
	      	isPass=false;
	      }else{
	      	$("#toMail").css("border","1px solid #ccc");
	      }
		  //邀约时间
		  if($('#invitationTime').val()==null || $('#invitationTime').val()=="" ){
	      	$("#invitationTime").css("border","1px solid red");
	      	isPassMsg+="请输入邀约时间,";
	      	isPass=false;
	      }else{
	      	$("#invitationTime").css("border","1px solid #ccc");
	      }
		  //邮件模板类型
		  if($('#type').val()==null || $('#type').val()=="" ){
	      	$("#type").css("border","1px solid red");
	      	isPassMsg+="请输入邮件模板类型,";
	      	isPass=false;
	      }else{
	      	$("#type").css("border","1px solid #ccc");
	      }
		  //主题
		  if($('#title').val()==null || $('#title').val()=="" ){
		     $("#title").css("border","1px solid red");
		     isPassMsg+="请输入主题,";
		     isPass=false;
		  }else{
		     $("#title").css("border","1px solid #ccc");
		  }	
		 arr=isPassMsg.split(",");
	} 
	//触发发送按钮事件
	function send(){
		
		validator();
			//非空判断
		if(isPass==false){
			layer.msg(arr[0]);
			return;
		}
		//标识为邀约
		stateTag = "invitation";
		//发送邮件
    	sendMail();
	}
	//修改候选人最新计划人力面试时间
	function updateCandidateTime(){
		
		//候选人id
		var id = parent.candidateId;
		//邀约时间
		var invitationTime = $("#invitationTime").val();
		var json ={candidateId:id,interviewTime:invitationTime};
		$.ajax({
			url:'com.recruit.mail.mail.updateCandidate.biz.ext',
			type:'POST',
			data:json,
	  		dataType: 'json',
	  		success:function(data){
	  			if(data.success==1){
	  				layer.msg("成功");
	  			}else{
	  				layer.msg("失败");
	  			}
	  		}
		});
	}
 
	

//初始值默认false 不调用查询邮件模板，为true的话调用查询邮件模板
var initial = false;
//加载时间控件
layui.use('laydate', function(){
	  var time = layui.laydate;
	  //执行一个laydate实例
	  time.render({
	    elem: '#invitationTime',
	    min: dateString,
	    type:'datetime',
	    done: function(value, date, endDate){
	    	
    		if(value!=null && value!==""){
    				//时间
    				tagTime = value;
	    			if(tagName!=""){
	    				//根据邮件名称查询邮件模板
	    				selectNameMailModel(tagName);
	    			}
	    		
    			$("#type").attr("disabled",false);
    		}
  		} 
	    
	  });
	
});

