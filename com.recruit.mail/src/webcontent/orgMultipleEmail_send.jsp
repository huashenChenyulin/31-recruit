<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp" %>
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	String talentMail = request.getParameter("talentMail");
	String name = request.getParameter("name");
	String position = request.getParameter("position");
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/orgMultipleEmail_send.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js?v=1.0"></script>
   <style type="text/css">
   	.personnel{
	   	background:#f0ad4e;
	   	line-height: 33px;
	    width: 35px;
	    text-align: center;
   		margin-top: -2.6%;
   		border: 1px solid #ccc;
   		height: 34px;
    	float: right;
    	font-size: 24px;	
    	border-radius: 4px;
   	}
   	#sidebar{
   		    width: 20%;
   		    float: left; 
   
   	}
   	.form-control{
   		width: 88%;
   	}
   	.layui-layedit{
   		margin-left: 20%;
   	}
   	.mail{
   		width: 97%;
   	}
   	.layui-btn{
   		margin-top: 1%;
   	 	margin-left: 57%;
   	}
   	#send{
   		margin-left: 50%;
    	margin-top: 1%;
   	}
   	#title{
   		width: 100%;
   	}
   		samp{
   			color: red;
   		}
   	   </style>
</head>
<body>
<div id="form">
	<div class="org-form-group  form-group">
    <label for="name">收件人邮箱</label><samp>*</samp>
    <input type="text" class="form-control mail" id= "toMail" name ="toMail" onblur="validator()">
  	<i class="layui-icon personnel" onclick="parent.parent.getEmail(this)">&#xe770;</i>   
  	</div>
  	
  	 <div class="org-form-group form-group ccMail">
    <label for="name">抄送人邮箱</label>

    <input type="text" class=" form-control mail" id= "ccMail" onblur="validator()">
    <i class="layui-icon personnel" onclick="parent.parent.getEmail(this)">&#xe770;</i>
  </div>
  

   <div class="form-group group bccMail">
    <label for="name">密送人邮箱</label>
    <input type="text" class="form-control mail" id="bccMail" >
    <i class="layui-icon personnel" onclick="parent.parent.getEmail(this)">&#xe770;</i>
  </div>
  <div style="margin-bottom: 15px;margin-top: -10px;">
		<a class="addCC" data="on">添加抄送人</a><a class="addBCC" data="on">添加密送人</a>
	</div>
  <div class="form-group group">
    <label for="name">主题</label><samp>*</samp>
    <input type="text" class="form-control" id="title" onblur="validator()">
  </div>
  
  <div id = "sidebar">
  	 <div class="form-group group">
     
    <label for="name">选择邮件模板</label><samp>*</samp>
    <select class="form-control" id = "templateName" name = "templateName"  onchange="" onblur="validator()">
      <option value=""></option>
    </select>
  </div>
  
  	 <div id="upload-list" >
  	       
  	       
  </div>
 
		
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
  </div> 
 <textarea class="layui-textarea" id="LAY_demo1" style="display: none;" >  
	
</textarea>
  
     <button class="btn btn-warning btn-save" id="send">发送</button>
  

	
</div>


	<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
	<script type="text/javascript">
//工号
var empId;
//端口
var prot = '<%=rooturl %>';
//状态
var state = "send";//状态
//姓名
var name = '<%=name %>';
//职位
var position = '<%=position %>';

var toResumeId="";

//初始化富文本编辑器
   var index;
   var layedit;
  	layui.use('layedit', function(){
    layedit = layui.layedit
  ,$ = layui.jquery;
  
  	 layedit.set({
  	 
            uploadImage: {
                url:'<%= request.getContextPath() %>/interpolation/file_uploadserver.jsp' //接口url
                ,type: 'post'//默认post
                
            }
           
        });
   
  //构建一个默认的编辑器
  index= layedit.build('LAY_demo1');
  
   
});  


 
    	
    </script>
</body>
</html>