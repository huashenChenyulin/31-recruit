<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
	<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@include file="/common/common.jsp" %>

<%
	//通过业务字典获取端口
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	//获取邮箱
	String talentMail = request.getParameter("talentMail");
	//获取姓名
	String name = request.getParameter("name");
	//获取id
	String id = request.getParameter("id");
	//获取职位
	String position = request.getParameter("position");
	//获取当前的工号
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-06-20 16:36:35
  - Description:
-->
<head>
<title>切换栏</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
    	#myTab{
		margin-left: 1%;
	    margin-right: 1%;
	    margin-top: 1%;
	}
	.main-content{
		float: left;
	    width: 98%;
	    border-bottom: 1px solid;
	    border-left: 1px solid;
	    border-right: 1px solid;
	    margin-left: 1%;
	    margin-right: 1%;
	    border-color: #dddddd;
	    height: 500px;
	}
	.a{
    	background: #eeeeee;
    }
    </style>
</head>
<body>
<div>
	<ul id="myTab" class="nav nav-tabs">
  	
  		<li class="active" onclick="email_send()" >
	
		<a href="" data-toggle="tab" class="a">发送邮件邀请</a>
		
		</li>
		
		<li class="archive" onclick="forwardingEvaluation()">
	
		<a href="" data-toggle="tab" class="a">转发用人部门</a>
		
		</li>
		
	</ul>

<div class="main-content" id="email_send">
	<iframe id="mainframe" src="<%= request.getContextPath() %>/mail/email_send.jsp?talentMail=<%=talentMail %>&position=<%=position %>&name=<%=name %>" frameborder="0" name="main" style="width:100%;height:100%;"></iframe>
</div>

<div class="main-content" id="forwardingEvaluation">
	<iframe id="mainframe" src="<%= request.getContextPath() %>/mail/forwardingEvaluation.jsp?id=<%=id %>" frameborder="0" name="main" style="width:100%;height:100%;padding: 10px"></iframe>
</div>


</div>


	<script type="text/javascript">
		//默认执行发送邮件邀请
    	email_send();
    //发送邮件邀请
    function email_send(){
		
		$("#email_send").show();
		$("#forwardingEvaluation").hide();
	}	
    //转发用人部门
	function forwardingEvaluation(){
		
		$("#forwardingEvaluation").show();
		$("#email_send").hide();
	}
    </script>
</body>
</html>