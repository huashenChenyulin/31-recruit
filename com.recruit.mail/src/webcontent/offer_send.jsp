<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
	<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@include file="/common/common.jsp" %>
<%
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	String talentMail = request.getParameter("talentMail");
	String name = request.getParameter("name");
	String position = request.getParameter("position");
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-11 15:27:15
  - Description:
-->
<head>
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
   	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
	<link href="<%= request.getContextPath() %>/mail/css/email_send.css" rel="stylesheet"/>
	<script src="<%= request.getContextPath() %>/mail/js/offer_send.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js?v=1.0"></script>
<title>发送offer</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 
</head>
<body>
<div>
	<form id="form" class="form">
  <div class="form-group">
    <label for="name">候选人邮箱</label><samp>*</samp>
    <input type="text" class="form-control" onblur="validator()" id= "toMail" name= "toMail">
  </div>
  
  <div class="form-group">
   <label for="name">计划入职时间</label><samp>*</samp>
   
   <input type="text" class="form-control import" onblur="validator()" id="plan-hire-date"
					name="planHireDate" placeholder="请输入计划入职时间">
	 </div>  
 
  
   
   <div class="form-group">
    <label for="name">选择offer邮件模板</label><samp>*</samp>
    <select class="form-control" disabled="disabled" onblur="validator()" id = "templateName"  name= "templateName" onchange="selectNameMailModel(this.value)">
   
    </select>
  </div>
  
  <div class="form-group">
    <label for="name">邮件主题</label><samp>*</samp>
    <input type="text" class="form-control" onblur="validator()" id="title">
  </div>
<div id= "uploadFile">
<div id="upload-list" >
  	       
  	       
  </div>
    
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
  
</div> 
</form>
<div id ="content">
	<textarea class="layui-textarea" id="LAY_demo1" style="display: none">  
	
</textarea> 
<button class="btn btn-warning btn-save" id ="send" onclick="send()">发送</button>
			
</div>

  
	
</div>
<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
<script type="text/javascript">
var empId ;//工号
$("#toMail").val("<%=talentMail %>");
//端口
var prot = '<%=rooturl %>';
//状态标识
var state = "send";
//姓名
var name = '<%=name %>';
//职位
var position = '<%=position %>';


//初始化富文本编辑
   var index;
   var layedit;
  	layui.use('layedit', function(){
    layedit = layui.layedit
  ,$ = layui.jquery;
  
  	 layedit.set({
  	 
            uploadImage: {
                url:'<%= request.getContextPath() %>/interpolation/file_uploadserver.jsp' //接口url
                ,type: 'post'//默认post 
            }
        });
   
  //构建一个默认的编辑器
  index= layedit.build('LAY_demo1');


});  
  
   
	
 
 </script>
</body>
</html>
