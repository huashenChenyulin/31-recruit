<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
	<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@include file="/common/common.jsp" %>
<%
	//通过业务字典获取端口
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	//获取邮箱
	String talentMail = request.getParameter("talentMail");
	//获取姓名
	String name = request.getParameter("name");
	//获取id
	String id = request.getParameter("id");
	//获取职位
	String position = request.getParameter("position");
	//简历id
	String resume_id = request.getParameter("resume_id");
	//获取当前的工号
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-11 15:27:15
  - Description:
-->
<head>
<title>邮件模板创建</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
 	<link href="<%= request.getContextPath() %>/mail/css/email_send.css" rel="stylesheet"/>
 	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
 	<script src="<%= request.getContextPath() %>/mail/js/email_send.js?v=1.0"></script>
 	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js?v=1.0"></script>
	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
 <style type="text/css">
    	#myTab{
		margin-left: 1%;
	    margin-right: 1%;
	    margin-top: 1%;
	}
	.main-content{
		float: left;
	    width: 98%;
	    border-bottom: 1px solid;
	    border-left: 1px solid;
	    border-right: 1px solid;
	    margin-left: 1%;
	    margin-right: 1%;
	    border-color: #dddddd;
	    height: 500px;
	}
	.a{
    	background: #eeeeee;
    }
    </style>
</head>
<body>
<div>
	<ul id="myTab" class="nav nav-tabs">
  	
  		<li class="active" id="emailSend" onclick="email_send()" >
	
		<a href="#" data-toggle="tab" class="a">发送邮件邀请</a>
		
		</li>
	<%
	if(request.getParameter("id")!=null){
	%>	
		<li class="" id="evaluation" onclick="forwardingEvaluation()">
		<a href="#" class="a">转发用人部门</a>
		</li>
	<%
	}
	%>
	</ul>
<div class="main-content" id="email_send">
	<form id="form" class="form">
  <div class="form-group">
    <label for="name">候选人邮箱</label><samp>*</samp>
    <input type="text" class="form-control" id= "toMail" name= "toMail" onblur="validator()">
  </div>
  
  <div class="form-group">
    <label for="name">邀约时间</label><samp>*</samp>
    <input type="text" class="form-control " id="invitationTime"
					name="invitationTime" placeholder="请输入邀约时间" onblur="validator()">
   
  </div>
  
  
  <div class="form-group">
    <label for="name">选择邮件模板类型</label><samp>*</samp>
    <select class="form-control" onblur="validator()" id = "type" name = "type"  disabled="disabled" onchange="selectTypeMailModel(this.value)" >
  	 <option value=""></option>
  	 <option value="02">邀约</option>
  	 <option value="03">通知</option>
  	 <option value="08">调查问卷型</option>
    </select>
  </div>
   <div class="form-group">
    <label for="name">选择邮件模板</label>
    <select class="form-control" onblur="validator()" id = "templateName"  disabled="disabled"  onchange="">
  	</select>
  </div>
  
  <div class="form-group">
    <label for="name">主题</label><samp>*</samp>
    <input type="text" onblur="validator()" class="form-control" name= "title" id="title">
  </div>
  
  <div>
  
  <div id= "uploadFile">
  <div id="upload-list" >
  	       
  	       
  </div>
 
			   
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <!-- <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div> -->
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
				</div>
  
</div> 
</div>
</form>
<div id ="content">
	<textarea class="layui-textarea" id="LAY_demo1" style="display: none">  
	
</textarea> 
<button class="btn btn-warning btn-save" id="send" onclick="send()">发送</button>	
</div>
</div>
<%
if(request.getParameter("id")!=null){
%>

<div class="main-content" id="forwardingEvaluation">
	<iframe id="mainframe" src="<%= request.getContextPath() %>/mail/forwardingEvaluation.jsp?id=<%=id %>" frameborder="0" name="main" style="width:100%;height:100%;padding: 10px"></iframe>
</div>
<%
}
%>

</div>
<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
<script type="text/javascript">

var empId ;//工号

//给候选人邮箱赋值
$("#toMail").val("<%=talentMail%>");
//端口
var prot = '<%=rooturl %>';
//状态标识
var state = "send";
//姓名
var name = '<%=name %>';
//职位
var position = '<%=position %>';

var toResumeId=<%=resume_id %>;
//当前工号
var empCode = <%=empCode %>;

//初始化富文本编辑器
var index;
var layedit;
  	layui.use('layedit', function(){
    layedit = layui.layedit
  	,$ = layui.jquery;
  	 layedit.set({
   		   uploadImage: {
                url:'<%= request.getContextPath() %>/interpolation/file_uploadserver.jsp' //接口url
                ,type: 'post'//默认post
            }
        });
   
  //构建一个默认的编辑器
  index= layedit.build('LAY_demo1');
});  

	//默认执行发送邮件邀请
    email_send();
    //发送邮件邀请
    function email_send(){
		
		$("#email_send").show();
		$("#forwardingEvaluation").hide();
	}	
    //转发用人部门
	function forwardingEvaluation(){
		
		if(parent.TemplateRecord==false){
		//清除所有li的class属性
			$("#myTab li").removeAttr("class"); 
		//给id为emailSend的li添加class名为active
			$("#emailSend").addClass("active");
			layer.msg("请先添加面试评估");
			return;
		}
		//清除所有li的class属性
		$("#myTab li").removeAttr("class");
		//给id为evaluation的li添加class名为active
		$("#evaluation").addClass("active");
		$("#forwardingEvaluation").show();
		$("#email_send").hide();
	}
	

 </script>
</body>
</html>
