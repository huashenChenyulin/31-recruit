<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp" %>
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%
	
	String id = request.getParameter("id");
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
   	
	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js"></script>
	
   <style type="text/css">
   body{
   	padding: 10px;
   }
   		   	.personnel{
   	background:#f0ad4e;
   	line-height: 33px;
    width: 35px;
    text-align: center;
   		margin-top: -3%;
   		border: 1px solid #ccc;
   		 height: 34px;
   	
    	float: right;
    	font-size: 24px;	
    	border-radius: 4px;
   	}
   	#sidebar{
   		    width: 20%;
   		    float: left; 
   
   	}
   	.form-control{
   		width: 88%;
   	}
   	.layui-layedit{
   		margin-left: 20%;
   	}
   	.mail{
   		width: 97%;
   	}
   	.layui-btn{
   		margin-top: 1%;
   	 	margin-left: 57%;
   	}
   	#send{
   		margin-left: 50%;
    	margin-top: 1%;
   	}
   	#title{
   		width: 97%;
   	}
   	#org-multiple-mail {
   		    min-height: 519px;
   	}
   	#LAY_demo1{
   		height:228px!important;
   		}
   		samp{
   			color: red;
   		}
   		
   	   </style>
</head>
<body>
<div id="form">
	
	<div class="org-form-group  form-group">
    <label for="name">收件人邮箱</label><samp>*</samp>
    <div>
	    <input type="text" class="form-control mail" id= "toMail" name ="toMail" onblur="validator()">
	  	<i class="layui-icon personnel" onclick="getEmail(this)" onblur="validator()">&#xe770;</i> 
  	</div>  
  	</div>
  	
  	 <div class="org-form-group form-group ccMail">
     <label for="name">抄送人邮箱</label>
	 <div>
	    <input type="text" class=" form-control mail" id= "ccMail">
	    <i class="layui-icon personnel" onclick="getEmail(this)">&#xe770;</i>
    </div>  
  	</div>
  	
  	 <div class="org-form-group form-group bccMail">
     <label for="name">密送人邮箱</label>
	 <div>
	    <input type="text" class=" form-control mail" id= "bccMail">
	    <i class="layui-icon personnel" onclick="getEmail(this)">&#xe770;</i>
    </div>  
  	</div>
  <div style="margin-bottom: 15px;margin-top: -10px;">
		<a class="addCC" data="on">添加抄送人</a><a class="addBCC" data="on">添加密送人</a>
	</div>
  <div class="form-group group">
    <label for="name">主题</label><samp>*</samp>
    <input type="text" class="form-control" id="title" onblur="validator()">
  </div>
  
  <div id = "sidebar">
  	 <div class="form-group group">
    
    <label for="name">选择邮件模板</label><samp>*</samp>
    <select class="form-control" id = "templateName" name = "templateName"  onchange="selectNameMailModel(this.value)" onblur="validator()">
      <option value=""></option>
    </select>
      <div class="form-group" style="margin-top: 15px;">
   <label for="name">计划面试时间</label><samp>*</samp>
   
   <input type="text" class="form-control import"  id="plan-hire-date"
					name="planHireDate" placeholder="请输入计划面试时间" onblur="validator()">
	 </div>
  </div>
  
  	 <div id="upload-list" >
  	       
  	       
  </div> 
 
		
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
  </div> 
 <textarea class="layui-textarea" id="LAY_demo1" style="display: none;height:228px;" >  
	
</textarea>
  
     <button class="btn btn-warning btn-save" id="send" onclick="send()">发送</button>
  
</div>



	<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
	<script type="text/javascript">
	//端口
	var prot = '<%=rooturl %>';
		var id='<%=id %>';
		//日期
		var date2="";
		//时间
		var time="";
		//主题
		var title="";
		//面试评估表地址
		var url="";
		//候选人
		var candidate_name="";
		//应聘部门
		var org="";
		//应聘职位
		var position="";
		
		//初始化富文本编辑器
		   var index;
		   var layedit;
		  	layui.use('layedit', function(){
		    layedit = layui.layedit
		  ,$ = layui.jquery;
		  
		  	 layedit.set({
		  	 
		            uploadImage: {
		                url:'<%= request.getContextPath() %>/interpolation/file_uploadserver.jsp' //接口url
		                ,type: 'post'//默认post
		                
		            }
		           
		        });
		   
		  //构建一个默认的编辑器
		  index= layedit.build('LAY_demo1');
		  layedit.setContent(index,"");
		     //查询面试评估模版
			  selectTemplate(id);
			   //查询候选人姓名，招聘部门，招聘职位
			  selectCandidateAndOrder(id);  
		}); 
		
		
		 
		
		   //初始值默认false 不调用查询邮件模板，为true的话调用查询邮件模板
		var initial = false;
		//加载时间控件
		layui.use('laydate', function(){
			  var startTime = layui.laydate;
			  var endTime= layui.laydate;
			  //执行一个laydate实例
			  startTime.render({
			    elem: '#plan-hire-date',
			    min: dateString,
			    type:'datetime',
			    done: function(value, date){
			
			    	//赋值日期时间
			    	time=value;
			    	  
			    	//截取日期
			    	 date2=value.substring(0, value.indexOf(' '));
			    	 if(time!=null){
			    		selectNameMailModel(tempName);
			    	}
			    	//非空验证
			    	validator();
				}
			    
			  });
			
		}); 	
		
		 //用来存储邮件模板的名称
		 var tempName="";
		 selectNameMailModel('05');
		 //查询邮件模板
		 function selectNameMailModel(parameterVal){
		 	if(parameterVal==""){
				//清空
		 			$("#title").val("");
		 			layedit.setContent(index,"");
		 			$(".files").empty();
		 	}else{
				if(parameterVal=="05"){
					data= {type:parameterVal};
				}else{
					var state = "send";
					var tag = "replace";
					tempName=parameterVal;
			  		data = {templateName:parameterVal,userMail:userMail,
			  				prot:prot,userPhone:userPhone,tag:tag,state:state};
				}
			 	
			 	$.ajax({
						url:'com.recruit.mail.mail.selectMailTemplate.biz.ext',
			  			type:'POST',
			  			data:data,
			  			dataType: 'json',
			  		success:function(data){
			  			//parameterVal值为05 是  05类型编码为 计划部门面试
			  			if(parameterVal=="05"){
			  				//动态邮件模板下拉框赋值
			  				for(var i = 0;i<data.mailTemplate.length;i++){
				  				var option = ("<option>"+data.mailTemplate[i].templateName+"</option>");
				  				$("#templateName").append(option);
			  				}
			  			}else{
							$("#title").val("");
				  			$(".files").empty();
				  		
				  			if(data.mailTemplate[0].title!=null){
				  				title=data.mailTemplate[0].title;
				  				$("#title").val(date2+title);
			  				}else{
			  					title="";
			  				}
			  			//访问地址
			  		 	var fileUrl = data.mailTemplate[0].fileUrl;
			  		 	if(fileUrl!=null && fileUrl!=""){
			  		 		//渲染附件
			  		 		showFile(fileUrl);
			  		 	}else{
			  		 	
			  		 	}
			  		 	
			  		 	//邮件模版
			  			var content =  data.updatePortTemplate;
			  			//赋值面试评估表地址，时间，候选人，应聘部门，应聘职位
			  			content = content.replace("url",url);
			  			content = content.replace("time",time);
			  			content = content.replace("name",candidate_name);
			  			content = content.replace("org",org);
			  			content = content.replace("position",position);
			  			layedit.setContent(index,content);
			  			
			  			}
			  		}
				});
			}
		 }
		 
		 //查询面试评估模版
    	function selectTemplate(id){
    		var json={
    			candidateId:id
    		}
    		$.ajax({
    			url:'com.recruit.mail.mail.selectTemplate.biz.ext',
			  	type:'POST',
    			data:json,
    			async : false,
    			success:function(data){
    				if(data.success=="-1"){
    					return;
    				}
    				//赋值url
    				var temId=data.template[0].id;
    				
    				var json2={
    					id:temId,
    					entry:'false'
    				}
    				//des加密，对id与标识进行加密
    				$.ajax({
		    			url:'com.recruit.mail.mail.encryptByDES.biz.ext',
					  	type:'POST',
		    			data:json2,
		    			async : false,
		    			success:function(data){
		    				url=basePath+'process/recruit_template/template_update.jsp?id='+data.id2+'&entry='+data.enrty2+'&sign=false';
    					}
    				});
    				
    				
    			}
    		});
    	}
    	
    	 //查询候选人信息
    	function selectCandidateAndOrder(id){
    		var json={
    			candId:id
    		}
    		$.ajax({
    			url:'com.recruit.mail.mail.selectCandidateAndOrder.biz.ext',
			  	type:'POST',
    			data:json,
    			async : false,
    			success:function(data){
    				//赋值候选人，应聘部门，应聘职位
    				position=data.candidate[0].position;
    				org=data.candidate[0].org;
    				candidate_name=data.candidate[0].candidate_name;
    			}
    		});
    	}
		 
    	var isPass=true;//标识是否通过验证
	var isPassMsg="";//非空验证提示输出
	var arr=[];
	//失去焦点验证
	function validator(){
		isPassMsg="";
		isPass=true;
		arr=[];
		
		 //候选人邮箱
		  var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
		  if($('#toMail').val()==null || $('#toMail').val()=="" || !email.test($("#toMail").val())){
	      	$("#toMail").css("border","1px solid red");
	      	isPassMsg+="请输入正确的邮箱格式-如：XXX@qq.com";
	      	isPass=false;
	      }else{
	      	$("#toMail").css("border","1px solid #ccc");
	      }
	      //主题
		  if($('#title').val()==null || $('#title').val()=="" ){
		     $("#title").css("border","1px solid red");
		     isPassMsg+="请输入主题,";
		     isPass=false;
		  }else{
		     $("#title").css("border","1px solid #ccc");
		  }	
		  //面试时间
		  if($('.import').val()==null || $('.import').val()=="" ){
	      	$(".import").css("border","1px solid red");
	      	isPassMsg+="请选择计划面试时间,";
	      	isPass=false;
	      }else{
	      	$(".import").css("border","1px solid #ccc");
	      }
		  //邮件模板类型
		  if($('#templateName').val()==null || $('#templateName').val()=="" ){
	      	$("#templateName").css("border","1px solid red");
	      	isPassMsg+="请选择邮件模版";
	      	isPass=false;
	      }else{
	      	$("#templateName").css("border","1px solid #ccc");
	      }
		  
		 arr=isPassMsg.split(",");
	} 
	
	//触发发送按钮事件
	function send(){
		
		validator();
			//非空判断
		if(isPass==false){
			layer.msg(arr[0]);
			return;
		}
		//状态标识（转发用人部门）
		stateTag="org";
		//发送邮件
    	sendMail('1');
	}
	
    	
    </script>
</body>
</html>