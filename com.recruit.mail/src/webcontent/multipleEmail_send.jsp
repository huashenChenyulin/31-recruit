<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp" %>
<%@ page import="com.eos.data.datacontext.DataContextManager"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%
	String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
	String talentMail = request.getParameter("talentMail");
	String name = request.getParameter("name");
	String position = request.getParameter("position");
	String empCode = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-05-05 10:44:46
  - Description:
-->
<head>
<title>群发邮件</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="<%= request.getContextPath() %>/mail/css/multipleEmail_send.css" rel="stylesheet"/>
 	 <link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
	<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
	<script src="<%= request.getContextPath() %>/mail/js/mailCommunal.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/multipleEmail_send.js?v=1.0"></script>
	<script src="<%= request.getContextPath() %>/mail/js/userInformation.js?v=1.0"></script>
	
	<style type="text/css">
		#multiple-mail,#org-multiple-mail{
	   	    float: left;
		    width: 98%;
		    border-bottom: 1px solid;
		    border-left: 1px solid;
		    border-right: 1px solid;
		    margin-left: 1%;
		    margin-right: 1%;
		    border-color: #dddddd;
	   	}
	   	#org-multiple-mail{
	   		height: 500px;
	   	}
	
	</style>
</head>
<body>
<div>
	<div id = "nav">
		<ul id="myTab" class="nav nav-tabs">
  	
  		<li class="active" onclick="multipleMail()" >
	
		<a href="" data-toggle="tab" class="a">待入职人员邮件群发</a>
		
		</li>
		
		
		<li class="archive" onclick="orgMultiple()" >
	
		<a href="" data-toggle="tab" class="a">部门群发</a>
		
		</li>
		</ul>
	</div>
		
<div id = "multiple-mail">
		<div id= "screening-nvb">
	 <label for="name" class="time import">计划入职时间</label><input type="text" class="form-control import" id="startTime"
					name="birthTime" placeholder="yyyy-MM-dd"> 
					<span class = "between">至</span>
				<input
					type="text" class="form-control import" id="endTime" name="birthTime"
					placeholder="yyyy-MM-dd"> 
					
		<button class="btn btn-warning btn-query" onclick="selectCandidate()" >查询</button>
	
		<button type="button" class="btn btn-default btn-center" onclick="isReset()">重置</button>
	
	<table class="layui-hide"  id="LAY_table_user" lay-filter="user"></table>

<div id="form" class="form">
  <div class="form-group">
    <label for="name">收件人邮箱</label><samp>*</samp>
    <input type="text" class="form-control mail" id= "toMail" name= "toMail" onblur="validator()">
  </div>
   <div class="form-group ccMail">
    <label for="name">抄送人邮箱</label>

    <input type="text" class=" form-control mail" id= "ccMail" onblur="validator()">
  </div>

   <div class="form-group group bccMail">
    <label for="name">密送人邮箱</label>
    <input type="text" class="form-control mail" id="bccMail" >
  </div>
  <div style="margin-bottom: 15px;margin-top: -10px;">
		<a class="addCC" data="on">添加抄送人</a><a class="addBCC" data="on">添加密送人</a>
	</div>
    <div class="form-group group">
    <label for="name">主题</label><samp>*</samp>
    <input type="text" class="form-control" id="title" onblur="validator()">
  </div>
  
   
  <div class = "sidebar_a">
  
  <div class="form-group">
    <label for="name">邀约时间</label><samp>*</samp>
    <input type="text" class="form-control " id="invitationTime" 
	name="invitationTime" placeholder="请输入邀约时间" onblur="validator()">
  </div>
  
     <div class="form-group group">
    <label for="name">选择邮件模板</label><samp>*</samp>
    <select class="form-control" id = "templateName" disabled="disabled" name = "templateName"  onchange="" onblur="validator()">
      <option value=""></option>
    </select>
  </div>
  

   <div id="upload-list" >
  	       
  	       
  </div>
 
			   
  <!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			            <div class="progress">
			                <span class="text">0%</span>
			                <span class="percentage"></span>
			            </div>
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
			    </div>
  

  
  </div>
   <textarea class="layui-textarea"  id="LAY_demo1" style="display: none">  
	
</textarea>

  <button class="btn btn-warning" id="send">发送</button>

	  </div>
</div>
</div>


<!-- 部门群发 -->
<div id = "org-multiple-mail" style = "display: none;">
  <iframe src="<%= request.getContextPath() %>/mail/orgMultipleEmail_send.jsp"frameborder="0" name="main" style="width:100%;height:100%;padding:10px 1px 10px 10px;"></iframe>

</div>

</div>
	
	
<!-- 部门 -->
	<script type="text/html" id="tempOrgName">
		{{formatDepartment(d.L1_name,d.L2_name,d.L3_name,d.L4_name)}}
	</script>
<script src="<%= request.getContextPath() %>/mail/js/resumeupload.js?v=1.0"></script>
<script type="text/javascript">
//工号
var empId ;
//端口
var prot = '<%=rooturl %>';
//状态标识
var state = "send";
//姓名
var name = '<%=name %>';
//职位
var position = '<%=position %>';
//当前工号
var empCode = <%=empCode %>;

//初始化富文本编辑
   var index;
   var layedit;
  	layui.use('layedit', function(){
    layedit = layui.layedit
  ,$ = layui.jquery;
  
  	 layedit.set({
            uploadImage: {
                url:'<%= request.getContextPath() %>/interpolation/file_uploadserver.jsp' //接口url
                ,type: 'post'//默认post
                
            }
           
        });
   
  //构建一个默认的编辑器
  index= layedit.build('LAY_demo1');

});  
   
  
</script>
	
</body>
</html>
