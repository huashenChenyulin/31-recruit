/**
 * 
 */
package com.recruit.publicMail;

import java.util.Date;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.eos.system.annotation.Bizlet;

/**
 * @author hjd
 * @date 2018-07-19 17:59:41
 * @param host 邮箱服务器
 * @param protococ 邮件发送协议
 * @param port 邮件服务器默认端口
 * @param loginName 登录名
 * @param password 密码
 * @param sendMail 发送人邮箱
 * @param toMail 收件人
 * @param subject 邮件主题
 * @param content 邮件内容
 * @return success 返回值
 */
@Bizlet("")
public class SendEmail {
	@Bizlet("发送索菲亚outlook邮件")
	public static String sendEmail(String host,String protococ,String port,String loginName,String password,
			   String sendMail,String toMail,String subject,String content){
		//失败或成功返回值
		String success = "";
		try {
		//调用发送邮件方法
		sendMail(host,protococ,port,loginName,password,sendMail,toMail,subject,content);
		success="发送成功";
		}catch(AuthenticationFailedException e){
			success="用户名或密码错误";
			e.printStackTrace();
		}catch(AddressException e){
			success="发送人或收件人邮箱有误";
			e.printStackTrace();
		}catch(SendFailedException e){
			success="收件人邮箱有误";
			e.printStackTrace();
		}
		catch(MessagingException e){
			success="发送异常";
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
	
	public static void sendMail(String host,String protococ,String port,String loginName,String password,
						   String sendMail,String toMail,String subject,String content) throws Exception{
		
			//连接邮件服务器的参数配置
			Properties prop = new Properties();
			//设置用户的认证方式
			prop.setProperty("mail.smtp.auth", "true");
			//设置传输协议
			prop.setProperty("mail.transport.protocol", protococ);
			//设置发件人的SMTP服务器地址
	        prop.setProperty("mail.smtp.host", host);
	        //设置邮件服务器默认端口
	        prop.setProperty("mail.smtp.port", port);
	        
	        //创建定义整个应用程序所需的环境信息的 Session 对象
	        Session session = Session.getInstance(prop);
	        
	        //创建邮件的实例对象
	        Message msg = getMimeMessage(session,sendMail,toMail,subject,content);
	       
	        //根据session对象获取邮件传输对象Transport
	        Transport ts = session.getTransport(protococ);
	        //使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
	        ts.connect(host,loginName,password);
	        /*发送邮件，并发送到所有收件人地址，message.getAllRecipients() 
	                   获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人*/
	        ts.sendMessage(msg,msg.getAllRecipients());
	        //关闭邮件连接
	        ts.close();	
	}
	
	public static MimeMessage getMimeMessage(Session session,String sendMail,String toMail,String subject,String content) throws Exception, MessagingException{
		//创建一封邮件的实例对象
        MimeMessage msg = new MimeMessage(session);
        
        //设置发件人邮箱
        msg.setFrom(new InternetAddress(sendMail));
        //设置收件人邮箱
        msg.setRecipients(Message.RecipientType.TO, toMail);
        //抄送给发送人邮箱
        msg.setRecipients(Message.RecipientType.CC,sendMail);
        //设置邮件主题
        msg.setSubject(subject,"UTF-8");
        //设置邮件正文
        msg.setContent(content, "text/html;charset=UTF-8");
        //设置邮件的发送时间,默认立即发送
        msg.setSentDate(new Date());
        return msg;
	}
}
