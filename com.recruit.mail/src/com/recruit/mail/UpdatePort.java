package com.recruit.mail;

import java.io.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.eos.system.annotation.Bizlet;

/**
 * @param rootUrl 访问端口
 * @param text 邮件正文
 * @param state 状态
 * @param name 名称
 * @param position 职位
 * @param time 时间
 * @param userName 当前用户名称
 * @param userPhone 当前用户号码
 * @param userMail 当前用户邮箱
 * @param interviewPlace 面试地点
 * @return templateName 返回已修改过的邮件正文;
 */

@Bizlet("修改邮件正文的标识")
public class UpdatePort {
	@Bizlet("")
	public static String UpdatePort(String rootUrl,String text,String state,String name,
									String position,String time,String interviewPlace,String userName,String userPhone,String userMail,
									String dataTime,String positionName,String URL){
		//返回邮件正文
		String templateName="";
	
		//年
		String years = "";
		//月
		String month = "";
		//日
		String day = "";
		//星期
		String week = "";
		//具体时间
		String specificTime = "";
		//判断时间不能为空
		if(time!=null){
			
			List<String> timeList;
			String []arr =new String[3];
			//截取时间 前10位
			String subTime = time.substring(0,10);
			//截取时间 11位以下
			specificTime = time.substring(11);
		try {
			//时间转星期
			week = dateToWeek(subTime);
			//拆分年月日
			timeList = getSeparatedTime(subTime);
			
			for(int i = 0 ; i <timeList.size();i++){
				arr[i] =timeList.get(i);
			}
			//赋值年
			years = arr[0];
			//赋值月
			month = arr[1];
			//赋值天
			day = arr[2];
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
	   }
		//邮件正文等于空
		if(text!=null){
			templateName=text;
		}
		//状态为send 代表是发送
		if(state.equals("send")){
			//名称等于空 
			if(name!=null && name!="" && !name.equals("null")){
				
				templateName = templateName.replaceAll("\\{[ 姓名 ^}]*\\}",name);
				templateName = templateName.replaceAll("\\{[ 职位 ^}]*\\}",position);
			}
			//替换邮件正文的标识
			if( userName!=null && userName!="" && userName!="null" ){
				templateName = templateName.replaceAll("\\{[ 负责人 ^}]*\\}",userName);
			}
			if(userPhone!=null && userPhone!="" && userPhone!="null" ){
				templateName = templateName.replaceAll("\\{[ 号码 ^}]*\\}",userPhone);
			}
			if(userMail!=null && userMail!="" && userMail!="null" ){
				templateName = templateName.replaceAll("\\{[ 邮箱 ^}]*\\}",userMail);
			}
			if(interviewPlace!=null && interviewPlace!="" && interviewPlace!="null"  ){
				templateName = templateName.replaceAll("\\{[ 地点 ^}]*\\}",interviewPlace);
			}
			if(dataTime!=null && dataTime!="" && dataTime!="null"  ){
				templateName = templateName.replaceAll("\\{[ 时间 ^}]*\\}",dataTime);
			}
			if(positionName!=null && positionName!="" && positionName!="null"  ){
				templateName = templateName.replaceAll("\\{[ 面试名称 ^}]*\\}",positionName);
			}
			if(URL!=null && URL!="" && URL!="null"  ){
				templateName = templateName.replaceAll("\\{[ 链接 ^}]*\\}",URL);
			}
			if(rootUrl!=null && rootUrl!="" && rootUrl!="null"){
				templateName = templateName.replaceAll("port/",rootUrl);
			}
				
			//时间不等于空
			if(time!=null){
				templateName = templateName.replaceAll("\\{[ 年 ^}]*\\}",years);
				templateName = templateName.replaceAll("\\{[ 月 ^}]*\\}",month);
				templateName = templateName.replaceAll("\\{[ 日 ^}]*\\}",day);
				templateName = templateName.replaceAll("\\{[ 星期 ^}]*\\}",week);
				templateName = templateName.replaceAll("\\{[ 具体时间 ^}]*\\}",specificTime);		
			}
		//state为save 代表保存
		}else if(state.equals("save")){
			
			templateName = templateName.replaceAll(rootUrl,"port/");
			
		//state为update 代表为修改
		}else if (state.equals("update")){
			templateName = templateName.replaceAll("port/",rootUrl);
		
		}	
		
		return templateName;
		
	}
	
	/*拆分 年 月 日 方法*/
	public static List<String> getSeparatedTime(String time) throws ParseException{
		
		List<String> septime = java.util.Arrays.asList(time.split("\\-"));
		return septime;
		
	} 
	/*日期转星期方法*/
	public static String dateToWeek(String datetime) {  
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");  
        String[] weekDays = { "日", "一", "二", "三", "四", "五", "六" };  
        //获得一个日历  
        Calendar cal = Calendar.getInstance(); 
        Date datet = null;  
        try {  
            datet = f.parse(datetime);  
            cal.setTime(datet);  
        } catch (ParseException e) {  
            e.printStackTrace();  
        }  
        //指示一个星期中的某天。 
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1; 
        if (w < 0)  
            w = 0;  
        return weekDays[w];  
    }  
	
	
	
	
}
