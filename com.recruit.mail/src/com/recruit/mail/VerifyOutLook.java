package com.recruit.mail;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;

import com.eos.system.annotation.Bizlet;
/**
 * @param host 邮箱服务器
 * @param protococ 邮件发送协议 
 * @param port 邮件服务器默认端口
 * @param loginName 登录名
 * @param password 密码
 * @return 返回状态status 为true（成功）为false（失败）;
 */
public class VerifyOutLook {
	private EmailParam param;
	@Bizlet("验证用户和密码是否正确")
	public String VerifySfyOutLook(String host,String protococ,String port,String loginName,String password)throws Exception{
		
		String status = "true";
		if(param==null){
			param = new EmailParam();
			param.setHost(host);
			param.setProtococ(protococ);
			param.setPort(port);
			param.setLoginName(loginName);
			param.setPassword(password);
		}
		
		//系统属性
		Properties prop = new Properties();
        prop.setProperty("mail.smtp.host", param.getHost());
        prop.setProperty("mail.transport.protocolz", param.getProtococ());
        prop.setProperty("mail.smtp.port", param.getPort());
        prop.setProperty("mail.smtp.auth", "true");

        //创建session
        Session session = Session.getInstance(prop);
       
        //通过session得到transport对象
        //使用邮箱的用户名和密码连上邮件服务器，发送邮件时，
        //发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证
        
		try {
			Transport ts = session.getTransport(param.getProtococ());
			ts.connect(param.getHost(), param.getLoginName(),param.getPassword());
			status = "true";
		} catch (NoSuchProviderException e) {
			status = "false";
			e.printStackTrace();
		} catch (MessagingException e) {
			status = "false";
			e.printStackTrace();
		}
       
      
      
		
		return status;
	}

}
