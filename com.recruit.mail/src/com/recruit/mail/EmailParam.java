package com.recruit.mail;

import com.eos.system.annotation.Bizlet;

/**
 * @param host 邮箱服务器
 * @param protococ 邮件发送协议 
 * @param port 邮件服务器默认端口
 * @param userName 用户名
 * @param loginName 登录名
 * @param password 密码
 */
@Bizlet("")
public class EmailParam {

	private String host;
	private String protococ;
	private String port;
	private String userMail;
	private String loginName;
	private String password;
	
	
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getProtococ() {
		return protococ;
	}
	public void setProtococ(String protococ) {
		this.protococ = protococ;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUserMail() {
		return userMail;
	}
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


	
	

	

}
