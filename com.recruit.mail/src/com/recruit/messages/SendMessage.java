package com.recruit.messages;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.eos.foundation.data.DataObjectUtil.convertDataObjects;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.eos.system.annotation.Bizlet;


public class SendMessage {
	//唯一标识
	public static String apikey = "4b490abf8367a8270ade6693b650f590";
	//请求URL
	public static String messageUrl = "http://sms.suofeiya.com.cn/api/v1/sms/single_send";
	//短信运营商认证信息
	public static String service_key = "3499a17ddd51ff045f1de5ffb8ca8ced";
	//短信运营商
	public static String service_provider = "YunPian";
	
	//发送短信公用方法
	@Bizlet("短信发送")
	public String batchSend( String msg, String phone,String username,String type) {
		Map<String, Object> params = new HashMap<String, Object>();// 请求参数集合
		JSONObject object = new JSONObject();
		object.put("key", service_key);
		String service_auth = object.toString();
		String Msg ="【索菲亚全屋定制】亲爱的"+username+"，索菲亚祝您身体健康，节日愉快！"+msg+"！";
		params.put("api_key", apikey);
		params.put("phone", phone);
		params.put("msg", Msg);
		params.put("type", type);
		params.put("service_provider", service_provider);
		params.put("service_auth", service_auth);
		return post(messageUrl,params);// 请自行使用post方式请求,可使用Apache HttpClient
	}
	
	
	// post方法提交
	public String post(String url, Map<String, Object> param) {
		HttpPost postMethod = null;
		HttpResponse response = null;
		//返回结果
		String result = "";
		postMethod = new HttpPost(url);
		if (param != null) {
			List<NameValuePair> paramList = new ArrayList<NameValuePair>();
			for (String key : param.keySet()) {
				paramList.add(new BasicNameValuePair(key, (String) param
						.get(key)));
			}
			// 模拟表单
			UrlEncodedFormEntity entity = null;
			try {
				entity = new UrlEncodedFormEntity(paramList, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			postMethod.setEntity(entity);
		}

		try {
			response = getClient().execute(postMethod);// 执行请求，获取HttpResponse对象

			int statuscode = response.getStatusLine().getStatusCode();// 根据相应码处理URI重定向
			if (statuscode == 200) {
				HttpEntity en = response.getEntity();
				result = EntityUtils.toString(en, "UTF-8");
				System.out.println(result);

			} else {
				System.out.println("发送失败");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			releaseConnection(postMethod);// 释放连接,无论成功与否都需要释放连接
		}

		return result;
	}

	//获取HttpClient对象
	public  HttpClient getClient() {
		HttpClient client = new DefaultHttpClient();// 获取HttpClient对象
		return client;
	}
	
	//释放连接
	private void releaseConnection(HttpRequestBase request) {
		if (request != null) {
			request.releaseConnection();
		}
	}
	
	public static void main(String[] args) {
		SendMessage sendmessage = new SendMessage();
		sendmessage.batchSend("节日愉快", "18617352946", "危蕾","1");
	}
	
	
	

}
