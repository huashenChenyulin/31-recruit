package com.recruit.messages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class Sendmeaaage {
	 private static String URI_SEND_SMS =
		        "https://sms.yunpian.com/v2/sms/single_send.json";
	public static String sendSms(String apikey, String text, 
	        String mobile) throws IOException {
	        Map < String, String > params = new HashMap < String, String > ();
	        params.put("apikey", apikey);
	        params.put("text", text);
	        params.put("mobile", mobile);
	        return post(URI_SEND_SMS, params);
	    }
	
	 public static String post(String url, Map < String, String > paramsMap) {
	        HttpClient client = new DefaultHttpClient();// 获取HttpClient对象
	        String responseText = "";
	        HttpResponse response = null;
	        try {
	            HttpPost method = new HttpPost(url);
	            if (paramsMap != null) {
	                List < NameValuePair > paramList = new ArrayList <
	                    NameValuePair > ();
	                for (Map.Entry < String, String > param: paramsMap.entrySet()) {
	                    NameValuePair pair = new BasicNameValuePair(param.getKey(),
	                        param.getValue());
	                    paramList.add(pair);
	                }
	                method.setEntity(new UrlEncodedFormEntity(paramList,"UTF-8"));
	            }
	            response = client.execute(method);
	            HttpEntity entity = response.getEntity();
	            if (entity != null) {
	                responseText = EntityUtils.toString(entity, "UTF-8");
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            try {
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	        return responseText;
	    }
	
	
	public static void main(String[] args) {
		Sendmeaaage sendmeaaage = new Sendmeaaage();
		try {
			sendmeaaage.sendSms("3499a17ddd51ff045f1de5ffb8ca8ced","【索菲亚全屋定制】亲爱的林瑞琪，祝您生日快乐！", "18926168554");
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
