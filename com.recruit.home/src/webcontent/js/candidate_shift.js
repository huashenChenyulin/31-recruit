		var pathName = document.location.pathname;
		var index = pathName.substr(1).indexOf("/");
		var contextPath = pathName.substr(0, index + 1);// 获取服务器IP和端口
		var pageIndex = 1;//当前页数
		var count;//总页数
		var limit =4;//显示条数
		var raw_orderId =parent.orderID;//当前候选人工单号
		var rawPostion=decodeURIComponent(parent.talentPosition);//decodeURIComponent：解码当前职位
		
		var isBatch = parent.isBatch;//是否为批量
		//数据表格长度
		var height;
		//工单号列的长度
		var orderIdWidth;
		//职位列的长度
		var positionWidth;
		//部门列的长度
		var orgWidth;
		//剩余周期列的长度
		var periodWidth;
		//招聘状态列的长度
		var roStatusWidth;
		//判断是否是批量转移 yes为是 no为否
		if(isBatch=="yes"){
		    height=289;
		    orderIdWidth='30%';
		    positionWidth='23%';
		    orgWidth='19.2%';
		    periodWidth='16%';
		    roStatusWidth='12%';
		}else{
			height=196;
			orderIdWidth='22%';
			positionWidth='27%';
			orgWidth='31.2%';
			periodWidth='12%';
			roStatusWidth='8%';
		}
		
		// 页面加载事件
		window.onload = function() {
			//查询
			var json = {"raw_orderId":raw_orderId,"rawPosition":rawPostion,"pageIndex":pageIndex,"limit":limit};
				search(json);
			
		};
		
		function searchOrder(){
			var orderId =$("#recruiterId").val();//工单号
			var position =$("#recruitPosition").val();//职位
			var json = {"orderId":orderId,"raw_orderId":raw_orderId,"rawPosition":rawPostion,"pageIndex":pageIndex,"limit":limit};
			search(json);
		}
		
		//查询关联工单信息
	    function search(json){
	    	 $.ajax({
	           	url: contextPath+'/com.recruit.home.updateCandidate.queryPosition.biz.ext',
				type:'POST',
				cache: false,
				data:json,
				success:function(data){
					count =data.page.count ;//总记录数
					laypage(count);//赋值总记录数给分页条
					var json = {};
					layui.use('table', function(){
	           			var table = layui.table;
					  	//第一个实例
					 	table.render({
						   id: 'idTest'
						   ,elem: '#demoOrder'
						   ,height: height
						   ,data:data.data
						   ,limit: limit //显示的数量
						   ,cols: [[ //表头
						     {field: 'recuit_order_id', title: '工单 号',event: 'setSign',width:orderIdWidth,templet:'#tempOrderId'}
						     ,{field: 'position', title: '职位',event: 'setSign',width:positionWidth,templet:'#tempPosition'}
						     ,{title: '部门',event: 'setSign',width:orgWidth,templet:'#tempOrgName'}
						     ,{field: 'surplus_period', title: '剩余招聘周期',event: 'setSign',width:periodWidth,sort: true,templet:'#tempPeriod'}
						     ,{field: 'ro_status', title: '招聘状态',event: 'setSign',width:roStatusWidth,templet:'#tempRostatus'}
						   ]]
					  	});
						table.on('tool(test)', function(obj){
							var data = obj.data;
								if(obj.event === 'setSign'){
									json.recuitOrderId = data.recuit_order_id;//获取工单号
									var jsonstr = JSON.stringify(json);//转换JSON格式
									$("#orderId").val(jsonstr);//赋值工单号
							    }
						});
					});
				}
	   	 	});
	    }

		
		// 返回工单号给父层调用，封装JSON格式
		function resultJson() {
			var recuit_order_id = $("#orderId").val();// 获取工单号
			return recuit_order_id;// 返回JSON格式
		}
		
		 //分页条
		function laypage(count){
			layui.use(['laypage', 'layer'], function(){
				  var laypage = layui.laypage
				  ,layer = layui.layer;
				  laypage.render({
				    elem: 'pagingToolbar'
				    ,count: count
				    ,curr:pageIndex
				    ,limit:limit
				    ,limits:[3,10,20,30,50]
				    ,layout: ['count', 'prev', 'page', 'next']
				    ,jump: function(obj,first){
				    	pageIndex = obj.curr;//获取当前页
				    	limit = obj.limit;//当前条数
				    	if(!first){
				    		searchOrder();//查询
				    	}
				    }
	  			});
	  		});
		}
		//回车事件
     	document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {
            	searchOrder();//查询
            }
        };