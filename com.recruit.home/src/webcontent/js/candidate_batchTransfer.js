$(function(){
	//初始化根据工单号查询工单下的候选人
	selectCandiate();
	
});
//当前页数
var currPage = 1;
//显示条数
var limits = 8;
//根据工单号查询工单下的候选人
function selectCandiate(){
	//调用父层的工单号
	var orderId = parent.orderID;
	var json = {"orderId":orderId,"curr":currPage,"limit":limits};
	$.ajax({
		url:basePath+'com.recruit.home.updateCandidate.queryCandidate.biz.ext',
		type:'post',
		dataType:'json',
		data:json,
		async:false,
		success: function (data) {
			//渲染数据表格
			layui.use('table', function(){
			var table = layui.table;
			table.render({
				    elem: '#candiate-data'
				    ,height: 355
				    ,data:data.candidate
				    ,limit:limits
				    ,cols: [[
				      {type:'checkbox'}
				      ,{field:'id', width:'15%', title: 'id'}
				      ,{field:'candidate_name', width:'20%', title: '姓名'}
				      ,{field:'gender', width:'18%', title: '性别',templet: '<div>{{sexstatus(d.gender)}}</div>'}
				      ,{field:'candidate_phone',  width:'27%', title: '手机号码'}
				      ,{field:'recruit_process_id', width:'25%', title: '当前面试环节',templet: '<div>{{Abandonmentstatus(d.recruit_process_id)}}</div>'}
				      
				    ]],
				    //数据表格渲染回调
					done: function(res, curr, count){
						//隐藏id列
						$("[data-field='id']").css('display','none');
						$("[data-field='candidate_name']").css('height','39px');
					}
				    ,page: false
				});
			});
			//加载分页条
			pageToolbar(data.pageCond.count);
		}
	});	
}

var candidateJson={};
var key="";
var value="";
var text="";
//候选人姓名
var candidateName="";
//监听表格复选框选择
layui.use('table', function(){
	  var table = layui.table;
	  table.on('checkbox', function(obj){
		  var checkStatus = table.checkStatus('candiate-data')
	      	,data = checkStatus.data;
		  //type等于one 为单选  checked 等于true为选中
		 if(obj.type =="one"  && obj.checked==true){
			 //调用构造json方法
			 structureJson(data);
		 }else if(obj.type =="one" && obj.checked==false){
			  //调用删除json里面的数据方法
			 deleteJson(obj.data.candidate_name);
		  }
		 //type 为all 是全选  checked 为true是选中
		 if(obj.type =="all" && obj.checked ==true){
			 //调用构造json方法
			 structureJson(data);
		 //type 为all 是全选  checked 为false是取消选中
		 }else if(obj.type =="all" && obj.checked ==false){
			 //清空值
			 candidateJson={};
			 text="";
			 candidateName="";
		 }
			 //去掉字符串最后一个逗号
	    	 text=text.substring(0, text.length-1);
	    	 candidateName=candidateName.substring(0, candidateName.length-1);
	  });
});

//构造一个json
function structureJson(data,type){
	for(var i = 0;i<data.length;i++){
		//赋值参数候选人姓名
		key =data[i].candidate_name;
		//赋值参数候选人id
	    value =data[i].id;
	    //赋值json key为姓名  value 为值
	   candidateJson[key] = value;
	}
   	 	text="";
   	 	candidateName="";
   	 	for(var k in candidateJson){
   		//拼接json的值
   	 	text+=candidateJson[k]+",";
   	 	candidateName+=k+",";
   	 }
}
//根据键去删除json里的数据
function deleteJson(key){
	//删除json里面的数据
	  delete candidateJson[key];
	  text="";
	  candidateName="";
	  for(var k in candidateJson ){
		  text+=candidateJson[k]+",";
		  candidateName+=k+",";
	  }
}
//返回候选人id给父层调用
function getCandidateId(){
	return text;
}
//返回候选人姓名给父层调用
function getCandidateName(){
	return candidateName;
}
//展现候选人分页条
function pageToolbar(count){
layui.use(['laypage', 'layer'], function(){	
	var laypage = layui.laypage
	 ,layer = layui.layer;
		laypage.render({
		   elem: 'pageToolbar'
		   ,count:count//总条数
		   ,curr:currPage//当前页
		   ,limit:limits//显示条数
		   ,limits:[8, 20, 30, 40, 50]
		   ,layout: [ 'count','prev', 'page', 'next','limit',]
		   ,jump: function(obj,first){
			count = obj.count;
			currPage = obj.curr;//获取当前页面
			limits = obj.limit;//显示条数
		    if(!first){
		    	//清空候选人id
		    	text="";
		    	//根据工单号查询工单下的候选人
		    	selectCandiate();
		    }
	    }
	});
 });
	
}