
/* 该js为整个头部通用js，
 * 整个系统页面由头部页面包裹模块子页面（即iframe），
 * 包括流程模态的页面，称为模态层，该层为流程操作的弹出层（模态层写在头部页面是因为可以提供各个模块调取打开流程，并且模态层打开时布局是铺满，）
 * 其他模块功能会调用该js内的方法以便刷新流程页的数据
 * */
$(function() {
			
	 //打开发送邮件页面
    function opendMail(){
   	 alert(1);
    }	
			getMenu();//加载菜单数据
			getDiction();//加载渠道数据
			checkEmail();//检查用户有没有邮箱帐号
			
			//推送按钮下拉操作
			$(".drop-down-btn").click(function(){
		    		if($(this).attr("data")=="no"){
		    			$(".rc-trigger-popup").css("display","block");
		    			$(this).attr("data","off");
		    		}else if($(this).attr("data")=="off"){
		    			$(".rc-trigger-popup").css("display","none");
		    			$(this).attr("data","no");
		    			
		    		}});
			//下拉切换	
				$("ul#selectcont").on("click","li",function(){      
				var n=$(this).attr("date");
				var o=$(this).text();
			    $(".pushProcess").attr("onclick","pushProcess(\'"+n+"\')"); 
			    $(".pushProcess").text(o);
			    $(".rc-trigger-popup").css("display","none");});	
			});
	
	//查询数据字典
	function getDiction(){
		$.ajax({
			url:"com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
			type:"POST",
			data:{
			},
			success:function(data){
				Diction=data;
			}
			});
	}
		//加载菜单
		function getMenu(){
			$.ajax({
					url:"com.recruit.home.queryMenu.queryMenu.biz.ext",
					type:"POST",
					datatype: "json",
					data:{
					},
					success:function(data){
						 var html='';
						 $(data.menus).each(function(index,obj){
							 //菜单html拼接
							 html+=' <li class="layui-nav-item" lay-unselect><a href="javascript:goToUrl(\''+obj.linkAction+'\');" >'+obj.menuName+'</a>';
							 //判断是否有子页面
							 if(obj.childrenMenuTreeNodeList!=null){
								 //遍历的元素需要<dl class="layui-nav-child">包裹
								 if(obj.menuName=="报表"){}else{
								 html+='<dl class="layui-nav-child">';
								 }
							 }
							 $(obj.childrenMenuTreeNodeList).each(function(i,obj2){
								 if(obj.menuName=="报表"){}else{
									 html+='<dd><a href="#" onclick="goToUrl(\''+obj2.linkAction+'\')">'+obj2.menuName+'</a></dd>';
								 }
								 
								 
							 })
							 if(obj.childrenMenuTreeNodeList!=null||obj.childrenMenuTreeNodeList!="null"){
								 if(obj.menuName=="报表"){}else{
								 html+='</dl>';
								 }
							 }
							 html+='</li>';
							 
						 })
						 var url=server_context+'/coframe/auth/login/logout.jsp';//注销页面
						 var url2=server_context+'/coframe/auth/login/index.jsp';//行政页面
						 //注销、返回行政，用户名显示
						 html+='<li class="layui-nav-item" lay-unselect=""><a href="javascript:goToUrlForWord();">工作台</a></li>';
						 html+='<li class="layui-nav-item" lay-unselect=""><a href="javascript:goToUrl(\'#\');"><i class="layui-icon">&#xe612;</i> '+empname+'<span class="layui-nav-more"></span></a><dl class="layui-nav-child"><dd><a href="'+url2+'" target="_top">平台主页</a></dd><dd><a href="'+url+'" target="_top">注销</a></dd></dl></li>';
						 $(".recruit_menu").append(html);
						 //lay菜单控件绑定
						 layui.use('element', function(){
							  var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
							  
							});
					}
					});
			
		}
		
		
		
		
		//编辑候选人
		function candidateUpdata(){
			layer.open({
				type: 2,
				title: '编辑',
				content: server_context+'/process/candidate_update.jsp?candidateId='+candidateId,
				area: ['1210px', '420px'],
				cancel: function(index, layero){ 
					   var iframeWin = window[layero.find('iframe')[0]['name']]; 
					  iframeWin.historyGo();
				},   
				end: function(index, layero){ 
					layer.close(index);
					getTalentData(candidateId);
				}    
			});
		
		}
		function sendEmail(){
			if(checkmail=="false"){
				layer.msg('首次发送邮件，请先到【功能设置】-【个人设置】配置邮箱！');
				return;
			}
			if(talentEmail==""||talentEmail==null){
				layer.msg('当前候选人无邮箱地址，请编辑后重试');
			}else{
				layer.open({
					type: 2,
					title: '发送邮件通知',
					content: server_context+'/mail/email_send.jsp?talentMail='+talentEmail+'&position='+talentPosition+'&name='+talentName+'&id='+candidateId+'&resume_id='+talentResumeId,
					area: ['1210px', '600px'],
					end: function(index, layero){ 
						
					}    
				});
			}
			
		
		}
		function sendOffer(){
			if(checkmail=="false"){
				layer.msg('首次发送邮件，请先到【功能设置】-【个人设置】配置邮箱！');
				return;
			}
			if(talentEmail==""||talentEmail==null){
				layer.msg('当前候选人无邮箱地址，请编辑后重试');
			}else{
				layer.open({
					type: 2,
					title: '发送offer',
					content: server_context+'/mail/offer_send.jsp?talentMail='+talentEmail+'&position='+talentPosition+'&name='+talentName+'&resume_id='+talentResumeId,
					area: ['1210px', '600px'],
					end: function(index, layero){ 
						
					}    
				});
			}
		}
		//offer发送成功后回调
		function OfferCallback(date){
			$.ajax({
				url:"com.recruit.process.sendMail.sendOffer.biz.ext",
				type:"POST",
				datatype: "json",
				data:{
					id:candidateId,
					recuitOrderId:orderID,
					plannedHireDate:date
				},
				success:function(data){
					layer.close(layer.index);
					getTalentData(candidateId);//重新加载候选人页
				}
				});
		}
		
		
		/* 加载候选人信息 */	
		var candidateId="";//候选人ID
		var linkId="";//当前环节
		var orderID="";//当前候选人工单ID
		var offerstat="0";//当前候选人offer状态
		var talentData;//候选人实体
		var talentStatus;//当前候选人归档状态
		var talentEmail="";//候选人邮箱
		var talentPosition="";//当前招聘职位
		var talentName="";
		var talentResumeId="";//简历ID
		var talentID="";//人才库ID
		var healthStatus=""//体检情况
		var talentDataChannels=0//当前工单职位类别
		var humanNextTime='';
		var jobCategory=""
		function getTalentData(num) {
			$(".loadEffect").show();//加载滚动条
			$.ajax({
  			url:"com.recruit.process.process.queryCandidate.biz.ext",
  			type:"POST",
  			data:{
  				recruiterId:num,
  			},
  			success:function(data){
  				//全局变量赋值
  				talentData=data.recruitCandidate;
  				candidateId=data.recruitCandidate.id;
  				linkId=data.recruitCandidate.recruitProcessId;
  				orderID=data.recruitCandidate.recuitOrderId;
  				offerstat=data.recruitCandidate.offerStatus;
  				talentStatus=data.recruitCandidate.candidateStatus;
  				talentEmail=data.recruitCandidate.recruitResume.candidateMail;
  				talentPosition=encodeURIComponent(data.recuitOrder.position);
  				talentDataChannels=data.recruitCandidate.recruitChannels;
  				talentName=data.recruitCandidate.recruitResume.candidateName;
  				talentResumeId=data.recruitCandidate.recruitResume.id;
  				talentID=data.recruitCandidate.talentId;
  				humanNextTime=getDateTime(data.recruitCandidate.interviewTime);
  				jobCategory=data.recuitOrder.jobCategory;
  				//以下加载前清除上一次数据，防止叠加html元素
  				/*$(".human_interview").remove();*/
  				$("#addCandidate").remove();
  				$("#addhealthReport").remove();
  				$("li.processLi").removeClass("active");
				$("#processState").addClass("active");
				$(".interview").remove();//清楚掉总揽面试元素
  				$(".department_interview").remove();//清楚部门面试情况html元素
  				$(".healthReport").remove();//清除待入职面试情况html元素
  				/* 候选人信息赋值 */
  				$(".candidateName").text(data.recruitCandidate.recruitResume.candidateName);
  				$("#channels").text(getChannels(data.recruitCandidate.recruitChannels));
  				$("#suppliers").text(getSuppliers(data.recruitCandidate.recruitSuppliers));
  				if(sexstatus(data.recruitCandidate.recruitResume.gender)==""){}else{
  					$("#talent-sex").text(sexstatus(data.recruitCandidate.recruitResume.gender)+" |");
  				}
  				
  				$("#talent-old").text(" "+getAge(data.recruitCandidate.recruitResume.birthDate)+" |");
  				$("#talent-Education").text(educationstatus(data.recruitCandidate.recruitResume.degree));
  				$("#talent-mail").text(data.recruitCandidate.recruitResume.candidateMail);
  				$("#talent-moneny").text(getReexpected(data.recruitCandidate.recruitResume.expectedSalary));
  				$("#talent-phone").text(data.recruitCandidate.recruitResume.candidatePhone);
  				$(".positionName").text("职位："+data.recuitOrder.position);
  				/* 面试赋值 */
  				/* link为当前环节 */
  				$(".interviewSituation").css("display","none");
  				var first_object=data.recruitCandidate.recruitInterviewProcess;
  				var ObjLenght=0;
  					//面试情况赋值开始遍历（以下根据环节判断）
  					$(first_object).each(function(index,obj){
  						if(obj.link=="1"){
  							$(".first_interview_text").val(obj.interviewEvaluation);
  							$(".addfirst").attr("onclick","addFirst(\'"+obj.id+"\')");
  						}else if(obj.link=="2"){
  							$("#human_nextTime").val(getDateTime(data.recruitCandidate.interviewTime));
  							$("#department_nextTime").val(getDateTime(data.recruitCandidate.sectionTime));
  							$(".phone_interview_text").val(obj.interviewEvaluation);
  							$(".addphone").attr("onclick","addFirst(\'"+obj.id+"\')");
  						}else if(obj.link=="3"){
  							$(".addhuman").attr("onclick","addheman(\'"+obj.id+"\')");  
  							$(".getStaff").attr("data",obj.interviewerId);
  							$(".getStaff").val(obj.interviewerName);
  							$("#actual_interview_time").val(getDateTime(obj.actualInterviewTime));//实际时间
  							$("#planned_interview_time").val(getDateTime(data.recruitCandidate.sectionTime));//计划时间
  							$(".human-form-control").val(obj.interviewEvaluation);
  						}else if(obj.link=="4"){
  							var thead="";
  							var processId=obj.id;
  								ObjLenght++;
  								thead='<div class="department_interview interviewSituation">';
  								thead+='<span class="layui-badge-rim" style="float: left;">第'+ObjLenght+'轮</span>';
  								thead+='<button  class="btn btn-default addDepartment" style="float: right;" onclick="addDepartment('+obj.id+')">保存</button>';
  								thead+='<div id="department_interview_input" style="margin: 30px 0px 10px 0px;">';
  								thead+='<span style="float:left;line-height: 38px;">实际部门面试时间</span>';
  								thead+='<input class="layui-input dataTime" id="actual_department_time'+obj.id+'" placeholder="yyyy-MM-dd HH:mm:ss" value="'+getDateTime(obj.actualInterviewTime)+'" style="width: 160px;float:left;margin-left: 5px;" type="text">';
  								thead+='<span class="score_title" style="float:left;line-height: 38px;width: 50px;margin-left: 80px;">面试官</span>';
  								thead+='<input class="layui-input bumen_Staff'+obj.id+'" id="" onclick="getStaff(this)" data="'+isEmpty(obj.interviewerId)+'" placeholder="人员选择" value="'+isEmpty(obj.interviewerName)+'"  style="width: 160px;float:left" type="text">';
  								thead+='</div>';
  								thead+='<div style="float:left;">';
  								
  								thead+='</div> ';
  								thead+='<textarea class="form-control department-control'+obj.id+'" rows="4" style="margin-top: 80px;" placeholder="面试评价" value="" >'+isEmpty(obj.interviewEvaluation)+'</textarea>';
  								thead+='</div>';
  								$(".talent_situation").append(thead);
								for (var i=0;i<ObjLenght;i++){
										var a="#actual_department_time"+obj.id;
										var b="#planned_department_time"+obj.id;
									    laydate.render({
										    elem: a //指定元素
										    ,type: 'datetime'
										    ,format: 'yyyy-MM-dd HH:mm:ss'
										  });
										  
										 
									    laydate.render({
										    elem: b //指定元素
										    ,type: 'datetime'
										    ,format: 'yyyy-MM-dd HH:mm:ss'
										  });
									}
  								
  						}else if(obj.link=="5"){
  							$("#entry_interview_time").val(data.recruitCandidate.plannedHireDate)
  							$(".addpending").attr("onclick","addPending(\'"+obj.id+"\')");  
  							$(".pending_interview_text").val(data.recruitCandidate.delayedReason);
  							$("#pending_nextTime").val(data.recruitCandidate.delayedDate);
  							if(data.recruitCandidate.isDelayed=="1"){
  								$(".isPending").text("是");
  								isDelayed="1";
  								$(".pending-box").show();
  							}else{
  								$(".isPending").text("否");
  								$(".pending-box").hide();
  								isDelayed="0";
  								
  							}
  							healthStatus="";
  							$(data.recruitCandidate.recruitHealthReport).each(function(k,o){
  								if(healthReportStatus(o.healthReportStatus)=="合格"){
  									healthStatus=healthReportStatus(o.healthReportStatus);//检验体检是否合格
  								}
  								var thead="";
  									thead+='<div class="healthReport interviewSituation">';
  									thead+='<span style="float:left;">'+o.healthReportName+'</span><span style="float:right;color: #337dbc;">'+healthReportStatus(o.healthReportStatus)+'</span>';
  									thead+='</div>';
  									$(".talent_situation").append(thead);
  							})
  								
  							
  						}
  					})
  				//添加面试按钮在部门环节出现
  				if(linkId=="4"){var thead2='<button class="layui-btn layui-btn-primary" id="addCandidate" style="width: 100%;margin: 10px 0px 20px 0px;display:none" onclick="addCandidate()"><i class="layui-icon">&#xe654;</i>添加新一轮面试</button>';
					$(".talent_situation").append(thead2);//添加面试按钮
					}
  				if(linkId=="5"){var thead2='<button class="layui-btn layui-btn-primary" id="addhealthReport" style="width: 100%;margin: 10px 0px 20px 0px;display:none" onclick="sendHealth()"><i class="layui-icon">&#xe654;</i>发送体检报告到EHS</button>';
					$(".talent_situation").append(thead2);//添加面试按钮
					}		
  				//下一个环节ID赋值，push按钮
  				$(".rc-trigger-popup").css("display","none");
  				$(".drop-down-btn").attr("data","no");
  				var nextpush=Number(linkId)+1;
  				$(".pushProcess").attr("onclick","pushProcess(\'"+nextpush+"\')");
  				$(".pushProcess").text("推进至下一环节");
  				$(".pushProcess").css("background-color","#337dbc");
				$(".drop-down-btn").css("background-color","#337dbc");
  				if(nextpush=="6"){
  					$(".pushProcess").text("入职");
  				}
  				if(nextpush=="7"){
  					$(".pushProcess").attr("onclick","");
  					$(".pushProcess").text("已入职");
  				}
  				bsStep(linkId);//环节进度条
  				
  				//发送offer按钮显示，在部门环节才显示，即linkid=4
  				if(linkId=="4"){
  					/*if(data.recuitOrder.offerStatus=="1"||data.recuitOrder.offerStatus=="2"){
  						$("#sendOffer").attr("disabled",'disabled');
	  					}else{
	  						$("#sendOffer").removeAttr("disabled");
	  					}*/
  						$("#sendOffer").show();
  						$("#sendOffer").css("width","60%");
  						$("#sendEmail").css("width","40%");
  					}else{
  						$("#sendOffer").hide();$("#sendEmail").css("width","100%");}
  				//迁移候选人按钮在待入职环节不出现
  				if(linkId=="5"){
  					$("#qianyiButton").hide();
  					$("#rencaiButton").css("width","50%");
  					$("#abandonButton").css("width","50%");
  				}else{
  					$("#qianyiButton").show();
  					$("#rencaiButton").css("width","35%");
  					$("#abandonButton").css("width","50%");
  				}
  				//页面环节面试情况html元素显示 
  				if(linkId=="1"){
  					$(".first_interview").show();
  				}else if(linkId=="2"){
  					$(".phone_interview").show();
  				}else if(linkId=="3"){
  					$(".human_interview").show();
  				}else if(linkId=="4"){
  					$(".department_interview").show();
  					$("#addCandidate").show();
  				}else if(linkId=="5"){
  					$(".pending_interview").show();
  					$(".healthReport").show();
  					$("#addhealthReport").show();
  				}
  				$(".talent-detail-modal").show();
  				
  				/* 环节下拉赋值 */
  				$(".icon-btn-drop-down-cont").text("");
  				var mycars=new Array("推进到初筛","推进到电话环节","推进到人资环节","推进到部门环节");
  				$(mycars).each(function(i,o){
			        if((i+1)>linkId){
			        	$(".icon-btn-drop-down-cont").append("<li class='drop-down-item' date='"+(i+1)+"'><span>"+o+"</span></li>");
			        	
			        }
			    });
  				selectTemplate();//候选人评估信息
  				$(".loadEffect").hide();//加载条消失
  				FileShow(talentStatus);//处理候选人为归档时。。。
  				
  				
  			}
  			});
			
			}
			
			
		/* 总览信息 */
		function talentDataShow(){
			var jsp='<div class="interview first_data"><div class="interview1_left interview_info "><b>初筛</b></div></div>'
				+'<div class="interview phone_data"><div class="interview2_left interview_info "><b>电话面试</b></div></div>'
				+'<div class="interview human_data"><div class="interview2_left interview_info "><b>人资面试</b></div></div>'
				+'<div class="interview"><div class="interview3_left interview_info "><b>部门面试</b></div><div class="interview3_right bumen_data" sytle="min-height: 150px;"></div></div>'
				+'<div class="interview waiting_data"><div class="interview5_left interview_info"><b>待入职</b></div></div>'
				+'<div class="interview fangqi_data"><div class="interview5_left interview_info"><b>放弃原因</b></div></div>'
			$(".talent_situation").append(jsp); 
			/* link为当前环节，以下各环节总揽数据查询赋值 */
			$.ajax({
	  			url:"com.recruit.process.process.queryCandidate.biz.ext",
	  			type:"POST",
	  			data:{
	  				recruiterId:candidateId,
	  			},
	  			success:function(data){
	  				talentData=data.recruitCandidate;
	  				var Lenght=0;
	  				$(talentData.recruitInterviewProcess).each(function(index,obj){
	  					if(obj.link=="1"){
	  						var html="";
	  						html+='<p class="interview1_right">'+isEmpty(obj.interviewEvaluation)+'</p>';
	  						$(".first_data").append(html);
	  					}
	  					if(obj.link=="2"){
	  						var html="";
	  						html+='';
	  						html+='<div style="float: left;width: 80%;min-height: 150px;">';
	  						html+='<p style="max-width: 600px;float: right;width: 100%;"><span class="field">电话沟通时间：</span><span>'+getDateTime(obj.endTime)+'</span></p>';
	  						html+='<p style="max-width: 600px;float: right;width: 100%;"><span class="field">计划人资面试时间：</span><span>'+getDateTime(talentData.interviewTime)+'</span></p>';
	  						html+='<p class="interview2_right" style=""><span class="field">计划部门面试时间：</span><span>'+isEmpty(talentData.sectionTime)+'</span><span><hr>'+isEmpty(obj.interviewEvaluation)+'</span></p>';
	  						html+='</div>';
	  						html+='';				
	  						$(".phone_data").append(html);				
	  									
	  					}
	  					if(obj.link=="3"){
	  						var html="";
	  						html+='';
	  						html+='<div style="float: left;width: 80%;min-height: 150px;">';
	  						html+='<p style="max-width: 600px;float: right;width: 100%;"><span class="field">实际面试时间：</span><span>'+getDateTime(obj.actualInterviewTime)+'</span></p>';
	  						html+='<p style="max-width: 600px;float: right;width: 100%;"><span class="field">面试官：</span><span>'+isEmpty(obj.interviewerName)+'</span></p>';
	  						html+='<p class="interview2_right" style=""><span class="field">部门面试时间：</span><span>'+getDateTime(talentData.sectionTime)+'</span><span><hr>'+isEmpty(obj.interviewEvaluation)+'</span></p>';
	  						html+='</div>';
	  						html+='';				
	  						$(".human_data").append(html);			
	  					}
	  					
	  					if(obj.link=="4"){
	  						
	  						var html="";
	  						Lenght++;
	  							
	  							html+='';
	  							html+='<span class="layui-badge layui-bg-gray">第'+Lenght+'轮</span><div style="">实际面试时间：<span>'+getDateTime(obj.actualInterviewTime)+'</span><br></div>';
	  							html+='<p style="max-width: 600px;float: right;width: 100%;">面试官：<span>'+isEmpty(obj.interviewerName)+'</span></p>';
	  							html+='<span style="float: left;margin-top: 5px;">评价：'+isEmpty(obj.interviewEvaluation)+'</span><hr>';					        
	  							html+='';
	  						
	  						$(".bumen_data").append(html);
	  					}
	  					if(obj.link=="5"){
	  						var i=talentData.recruitHealthReport.length;
	  						var HealthReportName=""
	  						var HealthStatus="";//体检报告状态
	  						var HealthReason=""//体检描述
	  						if(i==0){
	  						}else{
	  							HealthReportName=talentData.recruitHealthReport[i-1].healthReportName;
	  							HealthStatus="（"+talentData.recruitHealthReport[i-1].healthReportStatus+"）";
		  						HealthReason=talentData.recruitHealthReport[i-1].healthFeedbackReason;
	  						}
	  						var html='<div style="float: left;width: 100%;max-width: 600px;">';
	  						html+='<p style="max-width: 600px;float: left;width: 100%;"><span class="field">计划入职时间：</span><span>'+isEmpty(talentData.plannedHireDate)+'</span></p>';
	  						html+='<p class="interview1_right">延期时间：<span>'+isEmpty(talentData.delayedDate)+'</span><br>'+isEmpty(talentData.delayedReason)+'<br><br>'+isEmpty(HealthReportName)+''+healthReportStatus(HealthStatus)+'<br>体检反馈:'+isEmpty(HealthReason)+'</p></div>';				
	  						$(".waiting_data").append(html); 
	  					}
	  				})
	  				
	  				var html='<div style="float: left;width: 100%;max-width: 600px;">';
						html+='<p style="max-width: 600px;float: left;width: 100%;"><span class="field">放弃原因：</span><span>'+isEmpty(talentData.abandonReason)+'</span></p>';
						html+='<p class="interview1_right">具体描述：<span>'+isEmpty(talentData.abandonDetails)+'</span></p></div>';				
						$(".fangqi_data").append(html); 
	  			}
			})
			
			
			
		}	
		//若已归档时改变页面效果
		function FileShow(){
			if(talentStatus=="3"||talentStatus=="2"){
					//如果候选人已经归档（状态为3），相关功能不可操作
					$(".pushProcess").attr("onclick","");
					$(".pushProcess").css("background-color","#7c8084");
					$(".drop-down-btn").css("background-color","#7c8084");
					$(".drop-down-btn").attr("data","close");
					$(".abandon").attr("onclick","");
					$(".add-assess").hide();
					changeDate($("#processDataAll"),'1');//默认打开总揽
					
				}else{
					$(".add-assess").show();
					$(".abandon").attr("onclick","closeTalent()");}//结束流程按钮
			
		}
			
		/* 保存初筛面试/电话面试信息 */
		function addFirst(id){
			var interviewDate;
			var interviewTime;
			var sectionTime;
			if(linkId=="1"){
				interviewDate=$(".first_interview_text").val();
			}else if(linkId=="2"){
				interviewDate=$(".phone_interview_text").val();
				interviewTime = $("#human_nextTime").val();
				sectionTime = $("#department_nextTime").val();
			}
			$.ajax({
  			url:"com.recruit.process.updateProcess.updateProcess.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{
  				id:candidateId,
  				recruitProcessId:linkId,
  				interviewEvaluation:interviewDate,
  				interviewTime:interviewTime,
  				sectionTime:sectionTime,
  				processId:id
  			},
  			success:function(data){
  				 if(data.out1=="保存成功"){
  				 	layer.msg('操作成功');
  				 	humanNextTime=$("#human_nextTime").val();
  				}else{
  					layer.msg('操作失败');
  				} 
  			}
  			});
		}
		/* 保存待入职 */
		var isDelayed='0';
		function addPending(id){
			$.ajax({
  			url:"com.recruit.process.updateProcess.updateHired.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{
  				id:candidateId,
  				isDelayed:isDelayed,
  				recuitOrderId:orderID,
  				delayedDate:$("#pending_nextTime").val(),
  				delayeReason:$(".pending_interview_text").val(),
  				processId:id,
  				plannedHireDate:$("#entry_interview_time").val()
  			},
  			success:function(data){
  				 if(data.out1=="保存成功"){
  				 	layer.msg('操作成功');
  				 	getTalentData(candidateId);
  				} 
  			}
  			});
		}
		/* 保存人资面试信息 */	
		function addheman(id){
			var actual_interview_time=$("#actual_interview_time").val();//实际时间
			var planned_interview_time=$("#planned_interview_time").val();//计划时间
			var human_form_control=$(".human-form-control").val();
			
			$.ajax({
  			url:"com.recruit.process.updateProcess.updateProcess.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{
  				id:candidateId,
  				recruitProcessId:linkId,
  				interviewEvaluation:human_form_control,//其他评价
  				sectionTime:planned_interview_time,//计划
  				actualInterviewTime:actual_interview_time,//实际
  				processId:id,
  				interviewerId:$(".getStaff").attr("data"),//面试官
  				interviewerName:$(".getStaff").val()
  			},
  			success:function(data){
  				 if(data.out1=="保存成功"){
  				 	layer.msg('操作成功');
  				 	getTalentData(candidateId);
  				} 
  			}
  			});
		}
		
		/* 保存部门面试信息 */	
		function addDepartment(i){
			var actual_department_time="#actual_department_time"+i;
			var actual_interview_time=$(actual_department_time).val();//实际时间
			var planned_department_time="#lanned_department_time"+i
			var planned_interview_time=$(planned_department_time).val();//计划时间
			var department_control=".department-control"+i
			var department_form_control=$(department_control).val();
			
			var staffId=".bumen_Staff"+i;
			var staffName=".bumen_Staff"+i;
			
			
			
			
			$.ajax({
  			url:"com.recruit.process.updateProcess.updateProcess.biz.ext",
  			type:"POST",
  			datatype: "json",
  			data:{
  				id:candidateId,
  				recruitProcessId:linkId,
  				interviewEvaluation:department_form_control,//其他评价
  				sectionTime:planned_interview_time,//计划
  				actualInterviewTime:actual_interview_time//实际
  				,processId:i
  				,interviewerId:$(staffId).attr("data"),//面试官
  				interviewerName:$(staffName).val()
  				//
  			},
  			success:function(data){
  				 if(data.out1=="保存成功"){
  				 	layer.msg('操作成功');
  				 	getTalentData(candidateId);
  				} 
  			}
  			});
		}
		
		/* 切换流程标签页 */	
		function changeDate(obj,id){
			//隐藏面试情况
			if(id=="1"){
				$(".interview").remove();
				$("#addCandidate").hide();
				$("#addhealthReport").hide();
				talentDataShow();
				$("li.processLi").removeClass("active");
				$(obj).addClass("active");
				$(".interview").show();
				$(".interviewSituation").hide();
			}
			if(id=="2"){
				//根据环节显示面试情况
				$("li.processLi").removeClass("active");
				$(obj).addClass("active");
				$(".interview").remove();
				
				
				if(talentStatus=="3"||talentStatus=="2"){
					//如果是归档或已入职人员，切换面试情况只显示体检信息，其他操作不允许出现
					$(".healthReport").show();
				}else{
					if(linkId=="1"){
						$(".first_interview").show();
					}else if(linkId=="2"){
						$(".phone_interview").show();
					}else if(linkId=="3"){
						$(".human_interview").show();
					}else if(linkId=="4"){
						$(".department_interview").show();
						$("#addCandidate").show(); 
					}else if(linkId=="5"){
						$(".pending_interview").show();
						/*$(".interviewSituation").show();*/
						$("#addhealthReport").show();
						$(".healthReport").show();
					}
				}
			}
			
		}	
		
		/* 新添面试 */
		function addCandidate(processId,i){
			layer.open({
			  title: ''
			  ,content: '计划复试时间<input type="text" class="layui-input" id="Timetest" placeholder="yyyy-MM-dd HH:mm:ss">'
			  ,type:'0'
			  ,closeBtn: 0
			  ,btn:['添加', '取消']
			  ,yes: function(index, layero){
			    $.ajax({
		  			url:"com.recruit.process.pushNextProcess.addInterview.biz.ext",
		  			type:"POST",
		  			datatype: "json",
		  			data:{
		  				interviewTime:$("#Timetest").val(),
		  				candidateId:candidateId,
		  				recruitOrderId:orderID
		  				
		  			},
		  			success:function(data){
		  				 getTalentData(candidateId);//重新加载数据
		  			}
		  			});
			    
			    
			    
			    layer.close(index); 
			  }
			});
			laydate.render({
		    elem: '#Timetest' //指定元素
		    ,type: 'datetime'
		    ,position: 'fixed'	
		    ,format: 'yyyy-MM-dd HH:mm:ss'
		  });   
			
			
		}
		/* 推进下环节 */
		function pushProcess(nextLink){
			if(linkId=="1"){
				$(".addfirst").click();//自动保存当前环节面试信息
				
			}
			if(linkId=="2"){
				$(".addphone").click();//自动保存当前环节面试信息
			}
			if(linkId=="3"){
				$(".addhuman").click();//自动保存当前环节面试信息
			}
			if(linkId=="4"){
				if(TemplateRecord==false){
					layer.msg("请先添加评估表后再操作");
					return;
				}
				//自动保存当前环节面试信息
				$(".addDepartment").each(function(i,obj){
					$(obj).click();
				})
			}
			/*if(linkId=="5"){
				$(".addpending").click();//自动保存当前环节面试信息
			}*/
			if(nextLink=="6"){
				//||jobCategory==9,排除一线（暂时不用）talentDataChannels==6 排除内部竞聘
				//if(healthStatus=="合格"||talentDataChannels==6||jobCategory==9){
					getEntryStaff();//入职环节选择人员再推进，
				/*}else{
					layer.msg("体检报告未反馈合格");
					return;
				}*/
				
			}else{
				nextLinkSubmin(nextLink,"",talentName);
			}
			
			
		}
		
		function nextLinkSubmin(nextLink,id,name){
			$(".loadEffect").show();
			$.ajax({
	  			url:"com.recruit.process.pushNextProcess.pushProcess.biz.ext",
	  			type:"POST",
	  			datatype: "json",
	  			data:{
	  				recuitOrderId:orderID,
	  				candidateId:candidateId,
	  				link:linkId,
	  				pushLink:nextLink,
	  				recruitNo:id,
	  				recruitName:name
	  			},
	  			success:function(data){
	  				 if(data.out=="ban"){
	  						layer.msg('待入职只能存在一个候选人',{tipsMore: true,zIndex:999});
	  						getTalentData(candidateId);
	  						return;
	  					 }
	  				 if(data.out=="NoPush"){
	  					layer.msg('工单已被取消，无法推送');
  						getTalentData(candidateId);
  						return;
	  					 
	  				 }
	  				 if(data.out=="推送成功"){
	  				 	/*layer.msg('推送成功');*/
	  				 	if(nextLink=="6"){
	  				 		var url="/myTask/task_details.jsp?recuitOrderId="+orderID;
	  				 		$(".talent-detail-modal").hide();
	  				 		$(".loadEffect").hide();
	  				 		goToUrl(url);//入职成功后回到工单页
	  				 	}else{getTalentData(candidateId);}
	  				 }
	  			}
	  			});
		}
	    
	    //入职人员选择
		function getEntryStaff(){
					var StaffId="null"//人员工号
					var empName=''//人员姓名	
					layer.open({
						type: 2,
						title: '选择入职人员',
						content: server_context+'/coframe/tools/plugIn/selectStaff.jsp?&talentName='+talentName,
						area: ['1212px', '520px'],
						btn: ['确定', '取消'],
						yes: function(index, layero){ 
							    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
							    var a=iframeWin.getData();
							    $(a).each(function(i,obj){
							    	if(i=="0"){
							    		StaffId=obj.order;
							    		empName=obj.username
							    	}
							    	
							    })	
							    if(StaffId=="null"){
							    	layer.msg("请选择已入职人员");
							    	return;
							    }else{
							    	nextLinkSubmin("6",StaffId,empName);
						  			layer.close(layer.index);
							    }
							    	
							    return StaffId;
						},
						btn2: function(index, layero){
							   layer.close(layer.index);
							  }    
						
						
					});
				}
		
		/* 归档候选人 */
		function closeTalent(){
			layer.open({
			  title: '结束流程（淘汰）'
			  ,content: server_context+'/process/abandon.jsp?linkId='+linkId
			  ,type:2
			  ,closeBtn: 0
			  ,area: ['426px', '330px']
			  ,btn:['归档', '取消']
			  ,yes: function(index, layero){
			  	var body = layer.getChildFrame('body', index);
			  	var Details=body.find('.out_interview_text').val();
			  	var Reason=body.find('#backOut').val();
			    $.ajax({
		  			url:"com.recruit.process.archiveProcess.archiveCandidate.biz.ext",
		  			type:"POST",
		  			datatype: "json",
		  			data:{
		  				candidateId:candidateId,
		  				abandonDetails:Details,
		  				abandonReason:Reason,
		  				processId:linkId,
		  				offerStatus:offerstat
		  				
		  			},
		  			success:function(data){
		  				if(data.out1=="归档成功"){
		  					layer.msg('归档成功');
		  					/* 3是归档 */
		  					closeMode();
		  				}
		  			}
		  			});
			    
			    
			    
			    layer.close(index); 
			  }
			});
		}
		
		/* 非空验证 */
		function isEmpty(obj){
		    if(typeof obj == "undefined" || obj == null || obj == ""){
		        return"";
		    }else{
		        return obj;
		    }
		}
		
		/* 切换环节 */
		function changeLink(n){
			$(".pushProcess").attr("onclick","pushProcess(\'"+n+"\')");
		}
		
		
		/* 关闭模态层 */
		function closeMode(){
			$(".talent-detail-modal").hide();
			if(linkId=="6"){}else{callChild();}
			layer.closeAll();//关闭所有层
			
		}
		  
		/* 下拉延期是否 */
		function isYesNo(id){
			if(id=="1"){
				$(".isPending").text("是");
				$(".pending-box").show();
				isDelayed="1";
			}else if(id=="2"){
				$(".isPending").text("否");
				$(".pending-box").hide();
				isDelayed="0";
			}
		}
		/* 调取子页面方法 */
		function callChild(){
		document.getElementById("frame").contentWindow.childSay(linkId);
		}
		
		//查询评估表
		var TemplateRecord=false;
		function selectTemplate(){
			$.ajax({
	  			url:"com.recruit.process.addRemplateRecord.queryTemplateRecord.biz.ext",
	  			type:"POST",
	  			datatype: "json",
	  			data:{
	  				candidateId:candidateId
	  			},
	  			success:function(data){
	  				TemplateRecord=false;
	  				if(data.RecruitTemplateRecord.length>=1){
	  					TemplateRecord=true;
	  				}
	  				$(".assess").text("");
	  				$(data.RecruitTemplateRecord).each(function(i,obj){
	  					var url="";
	  					
	  					var html='<div class="panel panel-info" style="margin-left: 5%;width: 360px;margin-bottom: 10px;">';
	  					html+='<div class="panel-heading">';
	  					html+='<h3 class="panel-title">面试评估表</h3>';
	  					html+='</div>';
	  					html+='<div class="panel-body">';
	  					if(talentStatus=="3"||talentStatus=="2"){
	  						//已归档或入职
	  						url=server_context+"/process/recruit_template/template_update.jsp?id="+obj.id+"&entry=1&sign=g"; 
	  						html+='<span>人资总分：<span>'+obj.hrScore+'</span></span><span style="margin-left: 30px;">部门总分：<span>'+obj.orgScore+'</span></span><a style="float: right;color:#337dbc;margin-right: 10px;"href="'+url+'"  target="_Blank">查看</a></div>';
	  					}else{
	  						url=server_context+"/process/recruit_template/template_update.jsp?id="+obj.id+"&entry=0&sign=g"; 
	  						html+='<span>人资总分：<span>'+obj.hrScore+'</span></span><span style="margin-left: 30px;">部门总分：<span>'+obj.orgScore+'</span></span><a style="float: right;color:#337dbc;"  href="javascript:void(0);" onclick="delectTemplate(\''+obj.id+'\')">删除</a><a style="float: right;color:#337dbc;margin-right: 10px;"href="'+url+'"  target="_Blank">查看/编辑</a></div>';
	  					}
	  					
	  					html+='</div>';
	  					$(".assess").append(html);	
	  				})
	  				
	  				
	  			}
	  			});
		}
		//添加评估
		function addTemplateRecord(){
			
					  $.ajax({
				  			url:"com.recruit.process.addRemplateRecord.addTemplateRecord.biz.ext",
				  			type:"POST",
				  			datatype: "json",
				  			data:{
				  				candidateId:candidateId
				  			},
				  			success:function(data){
				  				selectTemplate();
				  			}
				  			});
		}
		
		//删除评估
		function delectTemplate(id){
			
			
			layer.open({
		        type: 1
		        ,offset:'auto' 
		        ,id: 'layerDemo'
		        ,content: '<div style="padding: 20px 75px 0px;">是否删除该评估？</div>'
		        ,closeBtn: false//不显示关闭图标
		        ,btn:['是','否']
		        ,btnAlign: 'c' //按钮居中
		        ,shade: 0 //不显示遮罩
		        ,yes: function(index){
		        	 $.ajax({
				  			url:"com.recruit.process.addRemplateRecord.delectTemplateRecord.biz.ext",
				  			type:"POST",
				  			datatype: "json",
				  			data:{
				  				id:id
				  			},
				  			success:function(data){
				  				selectTemplate();
				  				
				  			}
				  			});
					  layer.close(index);  
		        }
		        ,btn2: function(index,layero){
		        	layer.close(index);//关闭页面
		        }
		});
				
		}
		
		//同步人才库
		function updaTetalents(){
				if(talentID==""){
					var url=server_context+"/com.recruit.talent.synchronization.flow?id="+candidateId;
					window.open(url);
				}else{
					$.ajax({
			  			url:"com.recruit.process.addCandidatePool.updateTalent_pool.biz.ext",
			  			type:"POST",
			  			datatype: "json",
			  			data:{
			  				candidateId:candidateId
			  			},
			  			success:function(data){
			  				layer.msg('同步成功');
			  			}
			  			});
					
				};
				
		}
		
		//发起EHS体检报告流程
		function sendHealth(){
			layer.open({
				  title: '上传体检报告'
				  ,content: server_context+'process/uploadHealthFile.jsp'
				  ,type:2
				  ,closeBtn: 0
				  ,area: ['365px', '280px']
				  ,btn:['确定', '取消']
				  ,yes: function(index, layero){
				  	var iframeWin = window[layero.find('iframe')[0]['name']];
				  	var result=iframeWin.resultBool;//返回是否上传成功--fasle：上传失败，true：上传成功
				  	var healthReportUrl=iframeWin.healthReportUrl;//附件URL
				  	var healthReportName=iframeWin.healthReportName;//附件名称
				  	var healthReportType=iframeWin.healthReportType;//附件类型
				  	var uploadHtml=iframeWin.notUpload;//赋值附件是否已上传 --fasle：未上传，true：已上传
				  	var json ={"candidateId":candidateId,"candidateName":talentName,"healthReportUrl":healthReportUrl,"healthReportName":healthReportName,"healthReportType":healthReportType};
				  	if(uploadHtml==true){//附件已上传
					  	if(result==true){ //附件上传成功
					  		var load=layer.load(1);//加载条
					  		$.ajax({
						     	url: server_context+'com.recruit.health.addHealthReport.addHealthReport.biz.ext',
								type:'POST',
								data:json,
								cache: false,
								async:true,
									success:function(data){
										if(data.MSG_TYPE=="S"){//S：保存成功，E：保存失败
											getTalentData(candidateId);
											layer.close(load);//隐藏加载条
											layer.close(index);//关闭页面
											layer.msg("发送成功");
										}else{
											layer.close(load);//隐藏加载条
											layer.close(index); //关闭页面
											var msgText = data.MSG_TEXT || "发送失败";
											layer.msg(msgText);//错误提示
			         	 				}
									}
			         	 	});
					  	}else{
					  		layer.msg("附件上传失败,请联系管理员");
					  	}
				  	}else{
				  		layer.msg('未上传文件不能发送！');
				  	}
				  }
				});
			
		}
		
		
		var toMail; //收件人邮箱
		var toName;//收件人姓名
		var toResumeId;//收件人简历id
		var toDegreeType;//收件人学历类别
		var toPosition;//收件人应聘的职位
		var invitationTime;//邀约时间
		var recruitPlan_id;//招聘计划id
		var classroom;//面试地点
		var phoneNumber;//手机号码
		var msgType;//短信模板类型
		var success;//发送邮件返回值
		var successMsg;//发送短信返回值
		var successState;//状态用来标识是否更改通知状态或区分哪个功能调用发送邮件（true为是,false为否,resumeList为校招简历列表调用）
		//打开发送邮件(收件人邮箱，收件人姓名，收件人应聘的职位，手机号码，短信模板类型，
		//		     招聘计划id，状态用来标识是否更改通知状态或区分哪个功能调用发送邮件（true为是,false为否,resumeList为校招简历列表调用）
		//		  ，简历id，学历类别，)
		function openEmail(mail,name,position,phone,type,recruitPlanId,state,resumeId,degreeType){
			debugger
			toMail = mail;//收件人邮箱
			toName =name;//收件人姓名 
			toResumeId = resumeId;//收件人简历id
			toDegreeType = degreeType;//收件人学历类别
			toPosition = position;//收件人应聘的职位
			phoneNumber = phone;//手机号码
			msgType = type;//短信模板类型
			recruitPlan_id = recruitPlanId;//招聘计划id
			successState = state;//标识是否更改通知状态或区分哪个功能调用发送邮件
			layer.open({
            	area: ['87%', '96%'],
				type: 2,
				shade:0.5,//不显示遮罩
				title:'发送邮件',
				resize:false, //是否允许拉伸
				content: [server_context+'campusRecruitment/mail/group_hair_email.jsp','no'],	//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
				end: function(layero, index){ 
					//代表邮箱发送成功，修改学生列表修改通知状态
					if(success==1 && state=="true"){
						//执行校招学生列表子页面修改通知状态
						$("#frame")[0].contentWindow.updateInform(invitationTime,classroom);
						//清除发送邮箱返回值
						success="";
					}
					
					//代表短信发送成功
					if(successMsg==1 && state=="true"){
						//清除发送短信返回值
						successMsg="";
					}
				}   
			});
		}
		var checkmail="";
		//检查用户是否有mail
		function checkEmail(){
			$.ajax({
		     	url: 'com.recruit.mail.mail.checkMail.biz.ext',
				type:'POST',
				data:{},
				success:function(data){
					checkmail=data.success;
				}
     	 	});
			
			
		}
		//用来区分是否批量转移页面
		var isBatch = "no";
		//转移候选人
		function isShiftCandidate(){
	                layer.open({
	                	area: ['85.3%', '76%'],
						type: 2,
						shade: 0,//不显示遮罩
						title:'转移候选人',
						resize:false, //是否允许拉伸
						btn: ['确定', '关闭'],
						content: [server_context+'home/candidate_shift.jsp','no'],	//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以
		                   yes: function(index,lay){
		                    	var iframeWin = window[lay.find('iframe')[0]['name']];	//得到iframe页的窗口对象，执行iframe页的方法：
				                var resultVal = iframeWin.resultJson();	//调用子页面的方法，得到子页面返回的ids
				            	if(resultVal==""){//未选择
				               		layer.msg("请选择工单");
				               		return;
				               	}
				               	var json = $.parseJSON(resultVal);
				               	var recuit_order_id = json.recuitOrderId;//获取新工单号
		                    	layer.open({
							        type: 1
							        ,offset:'auto'
							        ,id: 'layerDemo'
							        ,content: '<div style="padding: 20px 64px 0px;">工单号为'+recuit_order_id+'</div><div style="text-align:center;padding-top:5px">是否转移？</div>'
							        ,closeBtn: false
							        ,resize:false //是否允许拉伸
							        ,btn:['确定','关闭']
							        ,btnAlign: 'c' //按钮居中
							        ,shade: 0 //不显示遮罩
							        ,closeBtn: false//不显示关闭图标
							        ,yes: function(index,layero){
						               	var price= {"candidate_id":candidateId,"candidate_name":talentName,"recuit_order_id":orderID,"new_recuit_order_id":recuit_order_id,"recruit_process_id":linkId};
						                $.ajax({
							          		url: server_context+'com.recruit.home.updateCandidate.updateCandidate.biz.ext',
											type:'POST',
											data:price,
											cache: false,
											async:false,
											success:function(data){
												if(data.MSG_TYPE!="E"){//转移成功
													layer.closeAll();
													layer.msg("转移成功");
												}else{//转移失败
													layer.closeAll();
													layer.msg(data.MSG_TEXT);
												}
											}
							         	 });
							        }
							        ,btn2: function(index,layero){
							          layer.close(index);//关闭页面
							        }
							      });
	       					}
						});
	               
	      
		}
