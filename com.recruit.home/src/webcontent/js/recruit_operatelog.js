	var pathName = document.location.pathname;
	var index = pathName.substr(1).indexOf("/");
	var contextPath = pathName.substr(0,index+1);
	var pageIndex = 1;
	var limit = 10;
	
	$(function(){
		search();
		//初始化操作日期控件
		init_hiredate();
	});
	//初始化操作日期控件
	 function init_hiredate(){
			// 日期
		    layui.use('laydate', function(){
			  	var laydate = layui.laydate;
			  	$("#atual_date").each(function(index,element){
			  		// 执行一个laydate实例
			  		var id = element.id;
				  	laydate.render({
				    	elem: '#'+id //指定元素
				    	,range:'~'
				  	});
			  	});
			});
		}
	function search(){
		var operatorName = $("#operator_name").val();
		var operatedFunciton = $("#operated_function").val();
		var date = $("#atual_date").val();
		var json = {"pageIndex":pageIndex,"limit":limit,"operatorName":operatorName,"operatedFunciton":operatedFunciton,"date":date};
		$.ajax({
			url:contextPath+"/com.recruit.home.recruitOperateLog.queryOperateLog.biz.ext",
			type:"POST",
			data:json,
			cache:false,
			success:function(data){
				layui.use('table',function(){
					table = layui.table;
					table.render({
						elem:'#log'
						,data:data.recruitLog
						,limit:limit
						,height:450
						,cols:[[
						    {field:'operator_id',width:'10%',title:'操作人工号',sort:true,align:'center'}
						    ,{field:'operator_name',width:'10%',title:'操作人姓名',sort:true,align:'center'}
						    ,{field:'operated_function',width:'10%',title:'操作功能',sort:true,align:'center'}
						    ,{field:'operatedTime',width:'10%',title:'操作时间',sort:true,align:'center'}
						    ,{field:'operated_detail',width:'60%',title:'操作内容',sort:true,align:'center'}
						   //,{title:'操作',width:'300',sort:true,templet:'#tempOperation',align:'center'}
						]]
						
					});
				});
				 layui.use(['laypage', 'layer'], function(){
					  var laypage = layui.laypage
					  ,layer = layui.layer;
						laypage.render({
					    elem: 'demo'
					    ,count: data.page.count
					    ,total:true
					    ,curr : pageIndex
					    ,limit:limit
					    ,limits : [10, 20, 30, 40, 50]
					    ,layout: ['count', 'prev', 'page','limit', 'next', 'skip']
					    ,jump: function(obj,first){
					      count = obj.count;
					      limit = obj.limit;
					      pageIndex = obj.curr;
					      if(!first){
					    	  search();
					      }
					    }
					  });
				});
			}
		});
	}
	
	function resetLog(id){
		layer.open({
			type:'1'
			,offset:'auto'
			,id:'layerDemo'
			,content:'<div style="padding:20px 75px;">是否删除日志</div>'
			,closeBtn:false
			,btn:['确定','关闭']
			,btnAlign:'c'
			,shade:0 //不显示遮罩
			,resize:false
			,yes:function(){
				$.ajax({
					url:contextPath+'/com.recruit.home.recruitOperateLog.deleteOperateLog.biz.ext',
					type:'Post',
					data:{id:id},
					dataType:'json',
					success:function(data){
						if(data.result!="N"){
							search();
							layer.closeAll();
							layer.msg("删除成功");
						}else{
							layer.closeAll();
							layer.msg("删除失败");
						}
					}
				});
			}
		});
	}
	
	
	//回车事件
 	document.onkeydown = function(e){
        var ev = document.all ? window.event : e;
        if(ev.keyCode==13) {
        	search();//查询
        }
    };
	
	function resetRecr1(){
		document.getElementById("form1").reset();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	