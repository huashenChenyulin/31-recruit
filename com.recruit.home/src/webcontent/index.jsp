<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-26 21:01:05
  - Description:
-->
<%@include file="/common/common.jsp"%>
<% 
	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	
	 %>
<head>
<title>首页</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script src="<%= request.getContextPath() %>/home/js/echarts.common.min.js"></script>
<style type="text/css">
	.month-Situation{
		
		}
		
	.recruit-card{
		height: 267px;
		border-style: solid;
		border-top-color: #ebebeb;
		border-bottom-color: #cdcdcd;
		border-left-color: #dedede;
		border-right-color: #dedede;
		border-width: 2px;
		    border-top-width: 2px;
		    border-bottom-width: 2px;
		border-top-width: 1px;
		background: white;
		border-radius: 4px;
	}
	.content_month{
	height: 170px;
	border-style: solid;
	border-color: #E3E3E3;
	border-width: 1px;
	float: left;
	margin-left: 2%;
	width: 96%;}
	
	.middle{
	width: 25%;
	float: left;
	background: #F9F9F9;
	height: 100%;
	border-right-style: solid;
	border-right-color: #E3E3E3;
	border-width: 1px;
	box-sizing: border-box;
	cursor: pointer;}
	
	.middle_numble{
		float: left;
		font-size: 48px;
		line-height: 60px;
		text-align: center;
		width: 100%;
		margin: 20px 0px;
	}
	.middle_name{
		text-align: center;
		width: 100%;
		margin: 5px 0px;
		float: left;
	}
	.box-bt{
	font-family: 'Arial Negreta', 'Arial Normal', 'Arial';
	font-weight: 700;
	font-style: normal;
	font-size: 18px;
	color: #f0ad4e;
	}
	.task-doing{
		margin: 0px 7%;

		border-bottom-style: solid;
		
		border-bottom-width: 1px;
		
		border-color: #dbdbdb;
		
		height: 35px;
		
		line-height: 35px;
		color: #676869;
	}
	
	.task-doing{
		cursor: pointer;
	}
	.task-doing:hover{
		background: #f2f2f2;
	}
	.footer{
		height: 75px;
	    background: #fff;
	    margin-top: 22px;
	}
	.footer_left{
		line-height: 40px;
	    margin-left: 25px;
	    text-align: center;
	}
	.footer_left a{
		margin-right: 40px;
		cursor: pointer;
	}
</style>    
</head>

<body>
	
  	
  	<div class="" id="" style="background: #f8f8f8;padding-top: 10px;">
    	<div class="layui-row">
		    <div class="layui-col-xs8" style="padding: 0px 1%;">
		      <div class="month-Situation recruit-card">
		      	<div class="box-bt" style="margin-left: 2%;margin-top: 20px;margin-bottom: 20px;">招聘进展</div>
		      	<div class="content_month">
					<div class="middle " style="" onclick="callParent('/myTask/task_select.jsp')">
						<div class="middle_numble middle_num1">0</div><div class="middle_name">当前在招职位</div>
					</div>
					<div class="middle" onclick="callParent('/home/schedule/schedule.jsp?id=1')">
						<div class="middle_numble middle_num2">0</div><div class="middle_name">今日面试安排</div>
					</div>
					<div class="middle" onclick="callParent('/home/pending_select.jsp?id=5')">
						<div class="middle_numble middle_num3">0</div><div class="middle_name">待入职</div>
					</div>
					<div class="middle" onclick="callParent('/home/pending_select.jsp?id=6')">
						<div class="middle_numble middle_num4">0</div><div class="middle_name">入职</div>
					</div>
				</div>
		      </div>
		    </div>
		    <div class="layui-col-xs4"  style="padding: 0px 1%;">
		      <div class="right-Situation recruit-card">
		      	<div class="box-bt" style="margin-left: 2%;margin-top: 10px;;">内推跟进</div>
		      	<div class="box-doing">
			  		
			  	</div>
		      </div>
		    </div>
  		</div>
  		<div class="layui-row" style="margin-top: 20px;">
		    <div class="layui-col-xs5"  style="padding: 0px 1%;">
		      <div class="recruit-card">
			  	<div id="charts" style="width: 100%;height: 267px;"></div>
			  </div>
		    </div>
		    <div class="layui-col-xs3"  style="padding: 0px 1%;">
		      <div class="recruit-card">
		      	
		      	<div class="box-bt" style="margin-left: 2%;margin-top: 10px;;">待办校招</div>
			  	<div class="box-campus-recuitOrder"></div>
		      </div>
		    </div>
		    <div class="layui-col-xs4"  style="padding: 0px 1%;">
		      <div class="recruit-card">
			  	<div class="box-bt" style="margin-left: 2%;margin-top: 10px;;">招聘中任务</div>
			  	<div class="box-doing-recuitOrder"></div>
			  	
			  </div>
		    </div>
  		</div>
  		
  		<div class="layui-row" style="margin-top: 20px;">
		    
		    <div class="layui-col-xs8" style="padding: 0px 1%;">
		      <div class="recruit-card">
			  	<div id="charts3" style="width: 100%;height: 300px;"></div>
			  </div>
		    </div>
		    
		    <div class="layui-col-xs4" style="padding: 0px 1%;">
		      <div class="recruit-card">
		      <div id="charts2" style="width: 100%;height: 267px;"></div>
			  	<!-- <div class="box-bt" style="margin-left: 2%;margin-top: 10px;;">待办校招</div>
			  	<div class="box-campus-recuitOrder"></div> -->
			  </div>
		    </div>
  		</div>
  		
  		<div class="layui-row" style="margin-top: 10px;">
  			<div class="layui-col-xs12" style="padding: 0px 1%;">
		      <div class="footer" style="width: 100%;">
		      	<div class="footer_left">
		      		
		      		<p>
		      			<a href="http://campus.sfygroup.com/campus/school_index.jsp" target="_blank">索菲亚校园招聘</a>
			      		<a onclick="layuiOpen()" >校招二维码</a>
			      		<a href="http://www.zhipin.com" target="_blank">BOSS直聘</a>
			      		<a href="http://www.liepin.com" target="_blank">猎聘</a>
			      		<a href="http://www.51job.com" target="_blank">前程无忧</a>
			      		<a href="http://www.zhaopin.com" target="_blank">智联招聘</a>
			      		<a href="http://gz.58.com" target="_blank">58同城</a>
			      		<a href="http://www.jjrw.com" target="_blank">JJR</a>
			      		<a href="http://www.lagou.com" target="_blank">拉勾网</a>
			      		<a href="http://oa.sfygroup.com:8088/" target="_blank">OA系统</a>
		      			<a href="http://www.sfygroup.com" target="_blank">索菲亚官网</a>
		      			<a href="http://www.suofeiya.com.cn" target="_blank">衣柜体验网</a>
		      		</p>
		      		<p>保留所有版权© 2018 招聘管理系统 </p>
		      	</div>
		      </div>
		    </div>
  		</div>
  	</div>


	<script type="text/javascript">
	  var layer;
		layui.use('layer', function(){
			layer = layui.layer;
			  
		}); 
	
   		// 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('charts'));
		var myChart2 = echarts.init(document.getElementById('charts2'));
		var myChart3 = echarts.init(document.getElementById('charts3'));
        // 指定图表的配置项和数据，option2候选人来源，option3达标率
        //option本周新增候选人，
        option = {
		    title: {
		        		show:true,
				        text: '本周新增候选人',
				        left: 10,
        				top:10,
        				textStyle:{
				            fontSize:18,
				            color:'#f0ad4e',
				            fontWeight:700
				        },
				    },
			/* tooltip: {
		        trigger: 'axis',
		        formatter: "{c}人"
		    },	   */  
		    xAxis: {
		        type: 'category',
		        smooth: true,
		        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
		    },
		    yAxis: {
		        type: 'value'
		    },
		    series: [{
		        data: [],
		        type: 'line',
		        label:{
		        		show:true,
		        		formatter:'{c}'
		        		}
		    }]
		};
		
		
		option2 = {
		    title: {
		        		show:true,
				        text: '本月添加候选人来源',
				        left: 10,
        				top:10,
        				textStyle:{
				            fontSize:18,
				            color:'#f0ad4e',
				            fontWeight:700
				        },
				    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    
		    series : [
		        {
		            name: '来源',
		            type: 'pie',
		            radius : '55%',
		            center: ['50%', '60%'],
		            data:[
		            ],
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            },
		            label:{
		        		show:true,
		        		formatter:'{b}'
		        		}
		        }
		    ]
		};
		
		option3 = {
    title: {
		        		show:true,
				        text: '达标率',
				        left: 10,
        				top:10,
        				textStyle:{
				            fontSize:18,
				            color:'#f0ad4e',
				            fontWeight:700
				        },
				    },
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['计件','计时']
    },
    toolbox: {
        show : true,
        feature : {
            dataView : {show: true, readOnly: false},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                formatter: '{value} %'
            },
            max: 100
        }
    ],
    series : [
        {
            name:'计件',
            type:'bar',
            data:[],
            label:{
		        		show:false,
		        		formatter:'{c}%'
		        		}
            /* markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
            }, */
            /*markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            }*/
        },
        {
            name:'计时',
            type:'bar',
            data:[],
            label:{
		        		show:false,
		        		formatter:'{c}%'
		        		}
        },{
            "name": "总数",
            "type": "line",
            "stack": "总量",
            symbolSize:10,
            symbol:'circle',
            "itemStyle": {
                "normal": {
                    "color": "#1b76b8",
                    "barBorderRadius": 0,
                    "label": {
                        "show": true,
                        "position": "top",
                        formatter: function(p) {
                            return p.value > 0 ? (p.value) : '';
                        }
                    }
                }
            },
            "data": []
        },
    ]
};

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        myChart2.setOption(option2); 
        myChart3.setOption(option3); 
        
        
        
        $(function(){
        	//首页各数据赋值
        	CountProgress();
        	selectInteroilation();
        	selectRecuitOrder();
			countCandidate(); 
			selectCanpus();
			
        
        });
        
        //统计每月数据
        function CountProgress(){
        		$(".middle_num1").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num2").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num3").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num4").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num5").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
        	$.ajax({
           		url: 'com.recruit.home.querycCountProgress.queryCountProgress.biz.ext',
				type:'POST',
				data:{},
				success:function(data){
					$(".middle_num1").text(data.countPosition);
					$(".middle_num2").text(data.candidate);
					$(".middle_num3").text(data.counttoBeHire);
					$(".middle_num4").text(data.countInOffce);
					
					
					
				}
			});
        }
        //每周添加候选人数量
        function countCandidate(){
        	
        	$.ajax({
           		url: 'com.recruit.home.countAddCandidate.queryIconData.biz.ext',
				type:'POST',
				data:{},
				success:function(data){
					myChart.setOption({
				        series: [{
				            data: data.countRecruitCandidate
				        }]
				    });
				    if(data.RecruitChannels.length==0){
				    	myChart2.setOption({
				        series : [{
					        data:[
				                {value:0, name:'无数据'}
				            ],
					    }]
				    });
				    }else{
				    myChart2.setOption({
				        series : [
					        {
					            data:data.RecruitChannels
					        }
					    ]
				    });
					}
					
					StandardRate();
				}
			});
        
        }
        
        //达标率
        function StandardRate(){
        	
        	$.ajax({
           		url: 'com.recruit.reports.recuitConditionReports.queryStandardIcon.biz.ext',
				type:'POST',
				data:{},
				success:function(data){
					myChart3.setOption({
				        series : [
						        {
						            data:data.byThePiece,
						            
						        },
						        {
						            
						            data:data.byTime,
						            
						        },{
						            "data": data.count,
						        },
						    ]
				    }); 
				    
				    
					
					
					
				}
			});
        
        }
        
        //查询内推情况
        function selectInteroilation(){
        	$.ajax({
           		url: 'com.recruit.home.countAddCandidate.queryRecommend.biz.ext',
				type:'POST',
				data:{
				},
				success:function(data){
					
					var index=0;
					 $(data.RecruitRecommend).each(function(i,obj){
					 	index=i;
						var html='<div class="task-doing" onclick="goToInteroilation('+obj.id+')"><span>'+obj.recommenderName+'</span><span style="float: right;">'+recommendstatus(obj.recommendedStatus)+'</span></div>';
						$(".box-doing").append(html);
					}); 
					index+1;
					for(var j=0;j<(5-index);j++){
						var html='<div class="task-doing" ><span></span><span style="float: right;"></span></div>';
						$(".box-doing").append(html);
					};
				}
			});
        }
        //查询校招计划
        function selectCanpus(){
        	$.ajax({
           		url: 'com.recruit.workbench.campusWorkbench.queryPlanWorkbench.biz.ext',
				type:'POST',
				data:{
				},
				success:function(data){
					var index=0;
					console.log(data);
					$(data.planWorkbench).each(function(i,obj){
						var html=""
						index=i;
						
						 html='<div class="task-doing" onclick="goToCampus(\''+obj.recruitPlanId+'\',\''+obj.recruitPlanName+'\',\''+obj.type+'\',\''+obj.interviewerId+'\')"><span>'+obj.recruitPlanName+'</span></div>';
						
						
						$(".box-campus-recuitOrder").append(html);
					});
					
					 index+1;
					for(var j=0;j<(5-index);j++){
						var html='<div class="task-doing" ><span></span><span style="float: right;"></span></div>';
						$(".box-campus-recuitOrder").append(html);
					}; 
					
					
				}
			});
        
        
        }
        //查询招聘中任务
        function selectRecuitOrder(){
        	$.ajax({
           		url: 'com.recruit.home.countAddCandidate.queryRecuitOrder.biz.ext',
				type:'POST',
				data:{
				},
				success:function(data){
					var index=0;
					$(data.RecuitOrder).each(function(i,obj){
						var html=""
						index=i;
						if(obj.surplusPeriod<0){
						 html='<div class="task-doing" onclick="goToProcesshome(\''+obj.position+'\',\''+obj.recuitOrderId+'\')"><span>'+obj.position+'</span><span style="float: right;">剩余周期<span style="color:red">'+obj.surplusPeriod+'</span>天</span></div>';
						}else{
						 html='<div class="task-doing" onclick="goToProcesshome(\''+obj.position+'\',\''+obj.recuitOrderId+'\')"><span>'+obj.position+'</span><span style="float: right;">剩余周期'+obj.surplusPeriod+'天</span></div>';
						}
						
						$(".box-doing-recuitOrder").append(html);
					});
					
					 index+1;
					for(var j=0;j<(5-index);j++){
						var html='<div class="task-doing" ><span></span><span style="float: right;"></span></div>';
						$(".box-doing-recuitOrder").append(html);
					}; 
					
					
				}
			});
        }
        //跳转招聘任务
        function goToProcesshome(position,order){
        	var positionId=position;
        	window.location.href="<%=request.getContextPath()%>/process/process_home.jsp?position="+ positionId + "&order=" + order;
        }
        
        //跳转校招流程
        function goToCampus(id,planName,type,recruiterId){
        	var positionId=position;
        	window.location.href="<%=request.getContextPath()%>/campusRecruitment/process/campusProcess.jsp?recruitPlanId="+id+"&recruitPlanName="+planName+"&degreeType="+type+"&recruiterId="+recruiterId;

        }
        
        function goToInteroilation(id){
        	var url="<%=request.getContextPath()%>/interpolation/recommend_details.jsp?id="+id;
        	window.location.href=url;
        }
        
        /* 调取父层 弹出招聘流程操作模态层 */
		function callParent(num) {
			parent.goToUrl(num);
		}
        
        
        function layuiOpen(){
        	
        	layer.open({
			  title: '校招二维码'
			  ,content: '<img alt="" src="<%=request.getContextPath()%>/home/images/QRcode.jpg" width="150px" style="margin-left: 35px;" >'
			  , time: 20000 //20s后自动关闭
			  ,btn: []
			  ,offset: '150px'
			});
        }
    </script>
</body>
</html>
