<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 11006633
  - Date: 2018-05-31 17:41:28
  - Description:
-->
<head>
<style type="text/css">
	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
</style>
<title>操作日志</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
     <script type="text/javascript" src="<%=request.getContextPath()%>/home/js/recruit_operatelog.js"></script>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title">
			  <legend>操作日志</legend>

			</fieldset>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			
			<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
				<table style="width: 80%; height: 80%;">
					<tr>
						<td>
							<label>操作人名称：</label> 
							<input id="operator_name" name="operator_name"
								class="form-control" data="" >
						</td>
						<td>
				      		<label>操作功能：</label>  
					        <input id="operated_function" name="operated_function" 
								class="form-control" data="" >
						</td>
						<td>
				      		<label>操作日期：</label>  
					        <input type="text"class="form-control" id="atual_date" name="atual_date"
							placeholder="yyyy-MM-dd ~ yyyy-MM-dd">
						</td>
						<td align="center">
							<button class="btn btn-warning" type="button" onclick="search()">搜索</button>
							<button class="btn btn-default btn-center" type="button" onclick="resetRecr1()">重置</button>
							<a id="dlink"  style="display:none;"></a>
						</td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div class="panel panel-default" >
		<div class="panel-body" style="width: 100%; height: 100%;">
			<table  class="layui-hide" id="log" style="width: 100%; height: 100%;"></table>
		</div>
	<div id="demo"></div>
	</div>
	<script type="text/html" id="tempOperation">
		{{'<div><button class="btn btn-export" type="button" onclick="resetLog('+d.id+')">清除日志</button></div>'}}	
	</script>
</body>
</html>