<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@include file="/common/common.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000100
  - Date: 2018-05-18 14:29:34
  - Description:
-->
<head>
<title>转移候选人</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="<%=request.getContextPath()%>/home/js/candidate_shift.js?v=1.0"></script>
<link style="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/home/css/candidate_shift.css?v=2.0">


</head>
<body>
	<div style="width: 100%;height: 80%; padding: 0px 20px;">
	<div class="panel panel-default par">
			<div class="panel-body rel">
				<form class="form-inline" style="width: 100%; height: 100%;">
					<table style="width: 100%; height: 100%;">
						<tr align="center">
							<td><label>工单号：</label> 
								<input type="text" class="form-control left" id="recruiterId" name="" placeholder=""> 
							</td>
							<!-- <td>
								<label class="lab-center">职位：</label> 
								<input type="text" class="form-control center"  id="recruitPosition" name="" placeholder="">
							</td> -->
						</tr>
						<tr>
							<td align="center" colspan="3" class="td-bottom">
								<button class="btn btn-warning" type="button" onclick="searchOrder()">搜索</button>
								<span style="display: inline-block; width: 25px;"></span>
								<button class="btn btn-default btn-center" type="button" onclick="reset()">重置</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		<table id="demoOrder" lay-filter="test"></table>
		<!-- 分页条 -->
		<div id="pagingToolbar"></div>
		<input id="orderId" value="" type="text" style="display: none;">
		</div>
		
	<!-- 工单号 -->
	<script type="text/html" id="tempOrderId">
		{{
		(d.is_mannul_id==1)?'<span>'+d.recuit_order_id+'</span><span class="layui-badge">口头</span>':'<span>'+d.recuit_order_id+'</span>'	
		}}
	</script>	
	<!-- 职位 -->
	<script type="text/html" id="tempPosition">
		{{'<div class="title" title="'+d.position+'">'+d.position+'</div>'}}
	</script>
	<!-- 部门 -->
	<script type="text/html" id="tempOrgName">
		{{'<div class="title" title="'+formatDepartment(d.L1_name,d.L2_name,d.L3_name,d.L4_name)+'">'+formatDepartment(d.L1_name,d.L2_name,d.L3_name,d.L4_name)+'</div>'}}
	</script>
	<!-- 招聘状态 -->
	<script type="text/html" id="tempRostatus">
		{{'<div>'+rostatus(d.ro_status)+'</div>'}}
	</script>
	<!-- 剩余招聘周期 -->
	<script type="text/html" id="tempPeriod">
		{{'<div>'+d.surplus_period+'天</div>'}}
	</script>
</body>
</html>