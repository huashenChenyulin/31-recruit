<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
	<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-24 16:22:04
  - Description:
-->
<head>
<title>个人设置</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <%
     String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	 String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
     
     %>
    <style type="text/css">
    	#wrap{
    		width: 90%;
		    margin-left: auto;
		    margin-right: auto;
		    margin-top: 1%;
    	}
    	.main{
   		 	box-shadow: 0 2px 2px 0 rgba(51,51,51,.2);
    	}
    	.main,#header,#header-org,.personalSettings{
    		float: left;
    		width: 100%;
    	}
    	legend span{
    		font-size: 14px;
    	}
    	#header{
    		margin-left: 1.5%;
    		margin-top: 1%;
    		margin-bottom: 1%;
    	}
    	#header p{
    		margin-bottom: 10px;
    		margin-left: 10px;
    		font-size: 16px;
    		color:#9c9494;
    	}
    	#update{
    		color: #ec971f;
    		cursor: pointer;
    	}
    	 /*报表的数据高度*/
 .layui-table-view{
		height: 453px!important;
	}
	.layui-table-main{
		height: 413px!important;
	}
    	
    	
    /* 	部门控件样式 */
    	.form-control.down{
    		width:23%;
    	}
    	.form-control.top{
    		width:13%;
    	}
    	.span1{
    		float: left;
    		padding-top:11px;
    	}
    	.span-name{
    		padding-left: 20%;
    	}
    	.span-oddnum{
    		margin-left: 13px;
    	}
    	
    	.layui-table-cell{
    		text-align: center;
    	}
    	#content-data{
    		height:370px; 
    		width:48%;
    		overflow-x:auto; 
    		float: left;
    		border-top: 1px solid #e6e6e6;
    		border-bottom: 1px solid #e6e6e6; 
    		border-left: 1px solid #e6e6e6;
    	} 
    	.layui-tree li{
    		overflow: inherit;
    	} 
    	 
    	.btn-query{
    		width: 70px;
    		height: 29px;
    		padding-top: 3px;
    		margin-left: 6px;
    	}
    	.btn-ok{
    		width: 70px;
    		height: 29px;
    		padding-top: 3px;  
    		margin-left: 50%;   
    	}
    	.btn-cancel{
    		width: 70px;
    		height: 29px;
    		padding-top: 3px;  
    		margin-left: 6px;
    	}
	    .content{
	    	width: 48%;
			height: 370px;
			float: left;
			border-style: solid;
			border-color: #E3E3E3;
			border-width: 1px;
			margin-left: 10px;
			overflow: scroll;
	    }
	    .label-info {
		    border-radius: 5px;
		    font-size: 100%;
		    line-height: 1;
		    margin-left: 15px;
		    margin-top: 10px;
		    font-weight: 100;
		    color: #0b8df5;
    		background-color: #b2daf4;
		    float: left;
		    padding: .2em .6em .3em;
		}
		.xe {
		    font-size: 14px;
		    color: #ffffff;
		    margin-left: 10px;
		    cursor: pointer;
		}
		.log-mail{
			height:85px;
		}
		#mail{
			position: relative;
		    top: -1px;
		    z-index: 10;
		    font-size:15px;
		}
		
		#password{
			position:relative;
			top:-34px;
			font-size:15px;
		}
		.log{
			font-size:16px;
			margin-left:2.4%;
			margin-top:1%;
		}
		.layui-form-label.set{
			margin-left:-15px;
			color:#9c9494;
		}
		.layui-form-label.bottom{
			margin-left:-19px;
			color:#9c9494;
			 width: 115px;
		}
		.label-set.bottom{
			position: relative;
		   	top: -26px;
		    left: 1px;
		     width: 115px;
		    margin-bottom: 13px;
		    color:#9c9494;
		}
		.form-control.none{
			position: relative;
			top:-35px;
		}
		.btn.btn-warning.set{
			margin-bottom: 20px;
   			margin-top: -9px;
		}
		.label-set{
			display: inline-block;
    		width: 100px;
		}
		.span{
			position: relative;
	   		top: -63px;
	   		left: 13.3%;
	   		color:#afa2a2;
   		}
   		#againPassword{
   			position:relative;
			top:-40px;
   			font-size: 15px;
   		}
   		#verifyMail{
   		    position: relative;
    		top: -62px;
    		left: 15%;
   			color:red;
   			display:none;
   		}
   		#verifyPassword{
   			position: relative;
   			top: 23px;
    		left: 15%;
    		color:red;
   		}
   		#verifyPad{
   			position: relative;
    		top: 108px;
    		left: 6.8%;
    		font-size:15px;
    		color:red;
   		}
		
		
		
		
    </style>
</head>
<body>
<div id="wrap">
	<div class="main">
		
		<div class="personalSettings">
			<fieldset class="layui-elem-field layui-field-title" style="margin-bottom:10px;">
				  <legend><a data-toggle="collapse"  href="#collapseTwo"><img id="img" src="<%=request.getContextPath() %>/home/images/bottom.png"></a>个人设置 </legend>
				  </fieldset>
				  	<div class="log collapse in"  id="collapseTwo">
				  		<div class="log-mail">
				  			<div class="parentCls">
				  				<div class="label-set "><label class="layui-form-label set">邮箱：</label></div>
				  					 <input id="mail" type="text" class="form-control top"    placeholder="请输入邮箱">
  			<!-- 别删，用于接收浏览器回调值 --><input  name="title" lay-verify="title" autocomplete="off" placeholder="请输入邮箱1" class="form-control none top" type="text"><span class="span">@sfygroup.com</span>
  							</div>
   						</div>
   						<div class="log-password">
   							<div class="label-set"><label class="layui-form-label set">密码：</label></div>
		    <!--别删，用于接收浏览器回调值 --><input   name="password" placeholder="请输入密码1" autocomplete="off" class="form-control down" type="password" >
   								  	<input id="password" name="password" placeholder="请输入密码"  autocomplete="off"  class="form-control down" type="password" >
   								<div class="label-set bottom"><label class="layui-form-label bottom">确认密码：</label></div>
   								  	<input id="againPassword" name="password" placeholder="请输入确认密码" autocomplete="off"  class="form-control down" type="password" >
   						</div>
				  		 		<button class="btn btn-warning set " type="button" onclick="addOrUpdateMail()">保存</button>
				  	</div>
				
		</div>
	</div>
</div>


	<script type="text/javascript">
		
		//部门设置箭头展开事件
		$('#collapseOne').on('show.bs.collapse', function () {
			$("#img_one").attr("src","<%=request.getContextPath() %>/home/images/bottom.png");
		});
		
		
		//部门设置箭头缩进事件
		$('#collapseOne').on('hide.bs.collapse', function () {
			$("#img_one").attr("src","<%=request.getContextPath() %>/home/images/top.png");
		});
		//个人设置箭头展开事件
		$('#collapseTwo').on('show.bs.collapse', function () {
			$("#img").attr("src","<%=request.getContextPath() %>/home/images/bottom.png");
		});
		
		//个人设置箭头缩进事件
		$('#collapseTwo').on('hide.bs.collapse', function () {
			$("#img").attr("src","<%=request.getContextPath() %>/home/images/top.png");
		});
		
	
		//当前登录人姓名
		 var empname="<%=empname %>";
		 //当前登录人id
		 var empid="<%=empid %>";
	
	
   var list="[]";	
   var  jsonList = JSON.parse(list);
   
   //接收json的参
   var testMap={};
   var value="";//key值
	var text="";//value值
   
  layui.use('table', function(){
  var table = layui.table;
  
});
   	
    $(function() {

			

	
		//加载个人设置邮箱
		$.ajax({
	        url: '<%=request.getContextPath()%>/com.recruit.home.querypersonal.queryEmail.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				if(data.mail!=null){
					var index= data.mail.lastIndexOf("@");//获取最后一个@的字符
						$("#mail").val(data.mail.substring(0,index));//截取@前面的字符
				}
			}
		}); 	
		//加载个人设置邮箱密码
		$.ajax({
	        url: '<%=request.getContextPath()%>/com.recruit.mail.mail.selectMailUser.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				$("#password").val(data.decPwd);//回填密码
				$("#againPassword").val(data.decPwd);//回填确认密码
			}
		}); 
    });
    
   	//添加或修改邮箱
   	function addOrUpdateMail(){
   		var mail =$("#mail").val().replace(/(^\s*)|(\s*$)/g, "");//获取邮件值,清空所有空格
   		var password =$("#password").val().replace(/(^\s*)|(\s*$)/g, "");//获取密码,清空所有空格
   		var verifyPassword =$("#againPassword").val().replace(/(^\s*)|(\s*$)/g, "");//获取确认密码,清空所有空格
   		if(mail==""){//邮件为空
   			layer.msg("邮件不能为空");
   			return;
   		}else if(password==""){//密码为空
   			layer.msg("密码不能为空");
   			return;
   		}else if(verifyPassword==""){//确认密码为空
   			layer.msg("确认密码不能为空");
   			return;
   		}
   		if(password!=verifyPassword){//密码与确认密码不一致
   			layer.msg("确认密码不一致，请重新输入");
   			return;
   		}
   		var json ={"mail":mail+'@sfygroup.com',"pwd":password};
   		var index = layer.load(2); //加载条
   		$.ajax({
    		url: '<%=request.getContextPath()%>/com.recruit.mail.mail.addMailUser.biz.ext',
			type:'POST',
			data:json,
			success:function(data){
				 if(data.success=="true"){//输入密码与索菲亚邮件密码一致
					if(data.success!='E'){//保存成功
						layer.close(index);//关闭加载条
						parent.checkEmail();//调用父层的验证邮箱方法
						layer.msg("保存成功");
					}else{
						layer.close(index);//关闭加载条
						layer.msg("保存失败");
					}
				}else{
					layer.close(index);//关闭加载条
					layer.msg("保存失败，当前密码与索菲亚协同系统密码不一致");
				} 
			}
		});
   	}
    
    </script>
</body>
</html>
