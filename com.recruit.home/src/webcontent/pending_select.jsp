<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-05-30 15:17:11
  - Description:
-->
<head>
<title>面试安排</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
   
    
    <%
    	String id =request.getParameter("id");
    	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
     %>
     
     <style type="text/css">
     	#wrap{
    		width: 90%;
		    margin-left: auto;
		    margin-right: auto;
		    margin-top: 1%;
    	}
    	#main{
   		 	box-shadow: 0 2px 2px 0 rgba(51,51,51,.2);
    	}
    	#main{
    		float: left;
    		width: 100%;
    	}
    	legend span{
    		font-size: 14px;
    	}
     </style>
</head>
<body>
<!-- 部门 -->
	<script type="text/html" id="tempOrgName">
		{{formatDepartment(d.L1_name,d.L2_name,d.L3_name,d.L4_name)}}
	</script>
<div id="wrap">
	<div id="main">
		<fieldset class="layui-elem-field layui-field-title">
			  	<% if(id.equals("5")) { %>
				<legend>待入职</legend>
				<% } else if(id.equals("6")) { %>
				<legend>本月已入职</legend>
				<% } else if(id.equals("4")) { %>
				<legend>面试安排</legend>
				<% } %>
			  
			</fieldset>
			
			<div class="panel panel-default" >
				<div class="panel-body" style="width: 100%; height: 100%;">
					<form class="form-inline" id="form1" style="width: 100%; height: 100%;">
						
						<table style="width: 100%; height: 100%;">
							<tr>
								<% if(id.equals("4")) { %>
								<td>
						      		<label>日期：</label>  
							        <input type="text" class="form-control" id="birthTime" name="birthTime">
								</td>
								<% } %>
								<td align="center" colspan="6">
									<button class="btn btn-defaul btn-export" type="button" onclick="daochu()" style="float: right;">导出Excel</button>
									<a id="dlink"  style="display:none;"></a>
								</td>
							</tr>
							
						</table>
					</form>
				</div>
			</div>
		
		<table class="layui-hide"  id="LAY_table_user" lay-filter="user"></table>
		
		<form id="formExcel" method="post" target="nm_iframe" class="form-inline" style="width: 100%; height: 100%;"type="hidden">
				<input id="jsonObject" name="jsonObject"type="hidden">
				<input id="head" name="head"type="hidden">
				<input id="sheetName" name="sheetName"type="hidden">
				<input id="fileName" name="fileName"type="hidden">
			</form>
	<iframe id="id_iframe" name="nm_iframe" style="display:none;"></iframe>
	</div>
</div>


	<script type="text/javascript">
		var id="<%=id %>";
		//获取当前日期YYYY-MM-DD
    	var myDate = getNowFormatDate();
    	$(function(){
    		//时间控件
	    	layui.use(['laydate','layer'], function(){
			  var laydate = layui.laydate,
			  layer = layui.layer;
			  //执行一个laydate实例
			  laydate.render({
			  	elem: '#birthTime' //指定元素
			  	,showBottom: false
			  	,value: myDate//默认当前日期
			  	,isInitValue: true
			  	,done: function(value, date){
			  		selectInterviewArrange(value);
			    }
			  });
			});
			
    		if(id==4){
    			selectInterviewArrange();
    		}else{
    			selectCandidate();
    		}
    		
    	});
    	
    	//查询已入职或待入职人员信息
    	function selectCandidate(){
			var user="<%=empid %>";
			var sign="<%=id %>";
			
			var json = {
				user:user,
				sign:sign
			};
			$.ajax({
	       		url: 'com.recruit.home.queryCandidateInformation.selectCandidate.biz.ext',
				type:'POST',
				data:json,
				cache: false,
				success:function(data){
					layui.use('table', function(){
			       	table = layui.table;
			        table.render({
			            elem: '#LAY_table_user'
			            ,data:data.cand
			            ,height:470
			            ,limit:data.cand.length
			            ,cols: [[
			               {field:'candidate_name', title: '候选人', width:100, fixed: true,event:'setSign'}
			              ,{field:'offer_time', title: '发送Offer时间',templet: '<div>{{getDate(d.offer_time)}}</div>', width:130,sort: true,event:'setSign'}
					      ,{field:'planned_hire_date', title: '计划入职时间',templet: '<div>{{getDate(d.planned_hire_date)}}</div>', width:130,sort: true,event:'setSign'}
					      ,{field:'atual_hire_date', title: '实际入职时间',templet: '<div>{{getDate(d.atual_hire_date)}}</div>', width:130,sort: true,event:'setSign'}
					      ,{field:'org',title: '部门', width:305 , templet:'#tempOrgName',event:'setSign' }
					      ,{field:'position', title: '职位', width:180,event:'setSign'}
					      ,{field:'recuit_order_id', title: '工单号', width:200,event:'setSign'}
					      ,{field:'recruiter_name', title: '负责人', width:100,event:'setSign'}
					      ,{field:'candidate_mail', title: '邮箱', width:200,event:'setSign'}
			               
			            ]]
			            
					});
					table.on('tool(user)', function(obj){//单击数据表格单击事件
						if(obj.event === 'setSign'){
						datas = obj.data;//获取整行值转给全局变量
						bool=true;//用来标识是否点击到表格
						parent.getTalentData(datas.cand_Id);
						
						}
					});
					
					});
				 }
			});
		}
		
		//查询面试安排信息
		function selectInterviewArrange(time){
			
			var json = {
				time:time
			};
			$.ajax({
	       		url: 'com.recruit.home.queryCandidateInformation.selectInterviewArrange.biz.ext',
				type:'POST',
				data:json,
				cache: false,
				success:function(data){
					layui.use('table', function(){
			       	table = layui.table;
			        table.render({
			            elem: '#LAY_table_user'
			            ,data:data.cand
			            ,height:440
			            ,limit:data.cand.length
			            ,cols: [[
			               {field:'candidate_name', title: '候选人', width:100, fixed: true,event:'setSign'}
			              ,{field:'seal', title: '面试环节', width:100,event:'setSign'}
			              ,{field:'interview_time', title: '计划面试时间', width:200,sort: true,event:'setSign'}
			              ,{field:'actual_interview_time', title: '实际面试时间', width:200,sort: true,event:'setSign'}
			              ,{field:'recuit_order_id', title: '工单号', width:200,event:'setSign'}
					      ,{field:'org',title: '部门', width:305 }
					      ,{field:'position', title: '职位', width:150,event:'setSign'}
					      ,{field:'candidate_phone', title: '电话', width:120,event:'setSign'}
					      ,{field:'candidate_mail', title: '邮箱', width:200,event:'setSign'}
			            ]]
			            
					});
					table.on('tool(user)', function(obj){//单击数据表格单击事件
						if(obj.event === 'setSign'){
						datas = obj.data;//获取整行值转给全局变量
						bool=true;//用来标识是否点击到表格
						parent.getTalentData(datas.id);
						}
					});
					
					});
				 }
			});
		}
		
		
			//导出
		  function daochu(){
		    var urlStr="";
		    var json="";
		    if( id == 4){
		    	urlStr="com.recruit.home.queryCandidateInformation.selectInterviewArrange.biz.ext";
		    	var time = $("#birthTime").val();
				json = {
					time:time
				};
		    }else{
		    	urlStr="com.recruit.home.queryCandidateInformation.selectCandidate.biz.ext";
		    	var user="<%=empid %>";
				json = {
					user:user,
					sign:id
				};
		    }
		  	ExcalStats = true;
		  		//日期
				
		       	$.ajax({
		       		url: urlStr,
					type:'POST',
					data:json,
					cache: false,
					success:function(data){
						if(ExcalStats){
		        	 		//关闭导出入口
					        ExcalStats = false;
			            	//配置Excel表头内容
			            	var head;
			            	if(id == 4){
			            		head= '{"candidate_name":"候选人","seal":"面试环节","interview_time":"计划面试时间","actual_interview_time":"实际面试时间","recuit_order_id":"工单号",'
			            		+'"org":"部门","position":"职位","candidate_phone":"电话","candidate_mail":"邮箱"}';
			            	}else{
			            		head= '{"candidate_name":"候选人","offer_time":"发送Offer时间","planned_hire_date":"计划入职时间","atual_hire_date":"实际入职时间",'
			            		+'"L1_name":"部门","position":"职位","recuit_order_id":"工单号","recruiter_name":"负责人","candidate_mail":"邮箱"}';
			            	}
			            	
			            	//获取Excel表内容
			            	var jsonObject = JSON.stringify(data.cand);
			            	//获取Excel工作簿名称
			            	var sheetName = "";
			            	//获取Excel文件名称
			            	var fileName = "";
			            	if(id == 4){
			            		sheetName = "面试安排";
			            	    fileName = "面试安排";
			            	}else if(id == 5){
			            		sheetName = "待入职";
			            	    fileName = "待入职";
			            	}else if(id == 6){
			            		sheetName = "本月已入职";
			            	    fileName = "本月已入职";
			            	}
			            	//保存到表单中
			            	document.getElementById("jsonObject").value = jsonObject;
		 					document.getElementById("head").value = head;
				            document.getElementById("sheetName").value = sheetName;
		 					document.getElementById("fileName").value = fileName; 
		 					//导出Excel表
				            var form = document.getElementById("formExcel");
						    form.action = "com.recruit.reports.deriveExcel.flow";
						    form.submit(); 
						    }
					
					}
				})
		    }
		    function getNowFormatDate() {
	        var date = new Date();
	        var seperator1 = "-";
	        var year = date.getFullYear();
	        var month = date.getMonth() + 1;
	        var strDate = date.getDate();
	        if (month >= 1 && month <= 9) {
	            month = "0" + month;
	        }
	        if (strDate >= 0 && strDate <= 9) {
	            strDate = "0" + strDate;
	        }
	        var currentdate = year + seperator1 + month + seperator1 + strDate;
	        return currentdate;
	    }
    </script>
</body>
</html>