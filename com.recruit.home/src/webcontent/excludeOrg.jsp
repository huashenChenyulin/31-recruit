<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>

	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>排除部门设置</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
    .min-memu{
    	width: 100%;
    	padding: 10px;
    }
    .left-memu{
   		width: 20%;
    	float:left;
    	overflow: auto;
    }
    .right-memu{
     	width: 79%;
    	float:left;
    }
    .layui-table-cell{
    	height:32px;
    	line-height:32px;
    }
    .layui-laypage-default{
    	float: right;
    }
    #demo{
    	margin-top:-17px;
    }
    </style>
</head>
<body>
	<!--主体 -->
	<div class="min-memu">
		<div class="left-memu">
			<ul id="demo1" class="demo1"></ul>
			
		</div>
		<div class="right-memu">
			<div class="left-btn" style="margin-left: 20px;">
			    <button type="button" id="insert" onclick="addEmp()" data class="btn btn-primary btn-query returnEvent_select"   data-complete-text="Loading finished">添加</button>
			    <span style="display:inline-block;width:20px;"></span>
				<button type="button" id="delete"  onclick="del()" class="btn btn-primary btn-query returnEvent_select"   data-complete-text="Loading finished">删除</button>
				<table class="layui-hide" id="company" lay-filter="companyEvent"></table>
			</div>
		</div>
	</div>
		<table id="demo" height="100%" lay-filter="test">
		</table>

<!-- 	转换日期格式 -->
	<script type="text/html" id="tempTime">
		<div>{{getDateTime(d.addTime)}}</div>
	</script> 
	
	<script type="text/javascript">
    	//部门控件
   	var result ="";//数据集
   	var l1 ="";
	var l1name ="";
	var l2 ="";
	var l2name ="";
	var orgId ="";
	var delId ="";//删除ID
	var bool =false; //标识是否已点击行
    $(function(){
    	selectExcludeOrg();//初始化查询
    	var winHeight = $(window).height();
    	$(".left-memu").css("height",winHeight-10);	
	     $.ajax({
	           url: 'org.gocom.components.coframe.tools.departmentPlugIn.selectL2.biz.ext',
			type:'POST',
			cache: false,
			success:function(data){
				layui.use(['tree', 'layer'], function(){
				  var layer = layui.layer
				  ,$ = layui.jquery; 
				  layui.tree({
				    elem: '#demo1' //指定元素
				    ,skin: 'red'
				    ,target: '_blank' //是否新选项卡打开（比如节点返回href才有效）
				    ,click: function(item){ //点击节点回调
				     var obj="";
				     if(item.l2!=null && item.l2!=""){
				     	obj=item.l2;
				     }else{
				     	obj=item.l1;
				     }
					     orgId =obj ;//赋值部门编号
					     l1 = item.l1;
					     l1name = item.l1name;
					     l2 = item.l2;
					     l2name = item.l2name;
						selectExcludeOrg(orgId);//查询
				    }
				    ,nodes:data.orgorganizantion
				  });
			  });
			}
		});
	})
	
	
	function selectExcludeOrg(orgid){
		//渲染表格
	    var json = {"orgid":orgid};		
		$.ajax({
			url : basePath+"com.recruit.home.recruitPersonal.selectExcludeOrg.biz.ext",
			type : "POST",
			data:json,
			cache:false,//不显示关闭图标
			async:true,
			success : function(data) {
				result=data.data;//工单数据
				layui.use('table', function(){
					var table = layui.table ,form = layui.form;
					  //展示已知数据
						  table.render({
							   	 elem: '#demo'
							    ,cols: [[ //标题栏
							       {field: 'id', title: 'id',width:'10%',unresize:true,templet:'#',event:'setSign',align:"center"}
							      ,{field: 'orgid', title: '部门编号',width:'10%',unresize:true,templet:'#',event:'setSign',align:"center"}
							      ,{field: 'orgname', title: '部门名称',width:'39.4%',unresize:true,templet:'#',event:'setSign',align:"center"}
							      ,{field: 'addEmpid', title: '工号',width:'10%',unresize:true,templet:'#',event:'setSign',align:"center"}
							      ,{field: 'addEmpname', title: '姓名',width:'20%',unresize:true,templet:'#',event:'setSign',align:"center"}
							    ]]
							    ,id:"idTest"
							    ,data: result 
							    ,height:515
							    ,page:true
							    ,done:function(res, curr, count) {
							    	//隐藏列
									$(".layui-table-box").find("[data-field='id']").css("display","none"); //ID   
								}
					  		 });
					  		 table.on('tool(test)', function(obj){//单击数据表格单击事件
					  		 	 if(obj.event === 'setSign'){
								     delId = obj.data.id;//获取整行值转给全局变量
								     bool=true;//用来标识是否点击到表格
					  		 	 }
							 });
					 });
				}
			});
	}
	
	function addEmp(){
		var empid = "";
		for(var i=0;i<result.length;i++){
			empid += result[i].addEmpid+",";
		}
		empid = empid.substring(0,empid.length-1);
		insertCompanyEmp(empid);
	}
	
	//添加
	function insertCompanyEmp(empid){
		layer.open({
				type: 2,
				title: '人员选择',
				content: server_context+'/coframe/tools/plugIn/selecthrorg.jsp?empId='+empid+'',
				area: ['1128px', '570px'],
				btn: ['确定', '取消'],
				yes: function(index, layero){ 
					var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
					    var a=iframeWin.getData();
					    var list="";
					    var list2=""
					    $(a).each(function(i,obj){
					    	if(i=="0"){
					    		list+=obj.username;
					    		list2+=obj.empid;
					    	}else{
					    		list+=","+obj.username;
					    		list2+=","+obj.empid;
					    	}
					    	
					    });
					    layer.close(layer.index);
					    if(list2!=""){
					    	var json = {
						    	empid:list2,
						    	empname:list,
						    	l1:l1,
						    	l2:l2,
						    	l1name,
						    	l2name
					    	};
					    	
							$.ajax({
								url:'com.recruit.home.recruitPersonal.addExcludeEmp.biz.ext',
								type:'POST',
								dataType:'json',
								data:json,
								success:function(data){
									if(data.out1=="成功"){
									layer.msg('添加成功！');
									selectExcludeOrg(orgId);//查询
								}else{
									layer.msg('添加失败！');
								}
								
								}
							});
					    }
				},
				btn2: function(index, layero){
					   layer.close(layer.index);
					  } 
			})
	}
	
	function del(){
		if(bool){
			deleteData(delId);
		}else{
			layer.msg("请选择!");
		}
	}
	 //根据ID删除排除部门表的数据
    function deleteData(i){
    	var json={
    		id:i
    	}
    	$.ajax({
    		url: 'com.recruit.home.recruitPersonal.deleteUnavailable.biz.ext',
			type:'POST',
			data:json,
			success:function(data){
				if(data.success=="ok"){
					bool=false;
					layer.msg('删除成功！');
					selectExcludeOrg(orgId);//查询
				}else{
					bool=false;
					layer.msg('删除失败！');
				}
			}
		});
    }
    </script>
</body>
</html>