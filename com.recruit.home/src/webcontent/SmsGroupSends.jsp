<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
	<%@page import="com.eos.data.datacontext.DataContextManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-06-13 16:02:02
  - Description:
-->
<head>
<title>短信群发</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <style type="text/css">
    
    #wrap{
    		width: 90%;
		    margin-left: auto;
		    margin-right: auto;
		    margin-top: 1%;
    	}
    	.main{
   		 	box-shadow: 0 2px 2px 0 rgba(51,51,51,.2);
   		 	padding-bottom: 10px;
    	}
    	.layui-input-block{
    	margin-left: 30px;
    	}
    	#wordCount{
    		width: 500px;
    	}
    	#textarea{
    		resize:none;
    		margin-top: 10px;
    		margin-bottom: 15px;
    		border: 0px;
    	}
    	.send{
    		float: right;
    	}
    	.form1{
    		width: 780px;
    		padding-bottom: 15px;
    		padding-top: 15px;
		    padding-left: 15px;
		    margin-left: 10px;
		    border: 1px solid #dcd8d8;
    	}
    </style>
</head>
<body>
<div id="wrap">
	<div class="main">
		<div class="personalSettings">
			<fieldset class="layui-elem-field layui-field-title" style="margin-bottom:10px;">
				  <legend>短信群发 </legend>
			</fieldset>
			<form class="layui-form form1">	  
			<div class="layui-form-item">
			    <div class="layui-input-block">
			    	<input type="radio" id="talent" lay-filter="resume" name="resume" value="1" title="" checked="checked" >
      				<input type="radio"  id="candidate" lay-filter="resume" name="resume" value="2" title="">
			    </div>
			 </div>
			 <div class="layui-form-item layui-form-text">
			    <div class="layui-input-block" id="wordCount">
			    	<input type="text" name="title" id="input" placeholder="请输入内容" class="layui-input" onkeyup="cop(value)">
			    	<span class="wordwrap" style="float: right;"><var class="word">10</var>/10</span>
			      <textarea id="textarea" class="layui-textarea" disabled="disabled"></textarea>
			      <button class="btn btn-warning send" type="button" onclick="send()">发 送</button>
			    </div>
			  </div>
			 </form>
		</div>
	</div>
</div>


	<script type="text/javascript">
		var value=1;
	
		layui.use(['form', 'layedit', 'laydate','layer'], function(){
		  var form = layui.form
		  ,layer = layui.layer
		  ,layedit = layui.layedit
		  ,laydate = layui.laydate;
  
  			//监听是选择人才库还是候选人
	  		form.on('radio(resume)',function(data){
	  			value=data.value;
	  		});
		});
		
		$(function(){
	        //先选出 textarea 和 统计字数 dom 节点
	        var wordCount = $("#wordCount"),
	            textArea = wordCount.find("#input"),
	            word = wordCount.find(".word");
	        
	       //调用
       		 statInputNum(textArea,word);
	   	     //查询候选人和人才库数量
	        select_talentAndCandidate();
	    });
		
		
		//查询候选人和人才库数量
		function select_talentAndCandidate(){
			$.ajax({
				url:'com.recruit.home.smsGroupSends.SmsGroupSends.biz.ext',
				type:'POST',
				async: false,
				success:function(data){
					$("#talent").attr('title','人才库（'+data.talent.length+'）');
					$("#candidate").attr('title','候选人（'+data.candidate.length+'）');
				}
			});
		}
		
		cop();
		//输入框改变时文本框跟着改变
		function cop(a){
			if(a=="" || a==null || typeof(a) == "undefined"){
				a=' ';
			}
			$("#textarea").text("【索菲亚全屋定制】亲爱的XXX，索菲亚祝您身体健康，节日愉快！（"+a+"）！");
		}
		
		//发送
		function send(){
			var msg=$("#input").val();
			var json={
				value:value,
				msg:msg
			}
			$.ajax({
				url:'com.recruit.home.smsGroupSends.sendSMS.biz.ext',
				type:'POST',
				data:json,
				async: false,
				success:function(data){
					layer.msg('发送成功');
				}
			});
		}
		
		function statInputNum(textArea,numItem) {
	        var max = numItem.text(),
	                curLength;
	        textArea[0].setAttribute("maxlength", max);
	        curLength = textArea.val().length;
	        numItem.text(max - curLength);
	        textArea.on('input propertychange', function () {
	            var _value = $(this).val().replace(/\n/gi,"");
	            numItem.text(max - _value.length);
	        });
	    }
		
    </script>
</body>
</html>