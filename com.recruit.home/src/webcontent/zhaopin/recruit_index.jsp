<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false"%>
<%@page import="com.eos.data.datacontext.DataContextManager"%>	

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-23 16:32:22
  - Description:
-->
<head>
<title>索菲亚招聘系统</title>
<%
	String empname = DataContextManager.current().getMUODataContext().getUserObject().getUserRealName();
	String empid = DataContextManager.current().getMUODataContext().getUserObject().getUserId();
	String url = request.getContextPath()+"/home/index.jsp";
	
	String recruit_id = null;
	if(empid!=null||empid!="null"){
	//加密操作员
	recruit_id = com.eos.foundation.common.utils.CryptoUtil.encryptByDES(empid,null);
	
	}
	//加密招聘计划
	String planId =com.eos.foundation.common.utils.CryptoUtil.encryptByDES("no",null);
 %>
<%@include file="/common/common.jsp"%>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/home/css/less/style.css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/home/css/recruit_menu.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/home/js/lib/lib.js"></script>




</head>
<style type="text/css">
.loadEffect{
   
 }
 .loading{
 	position: absolute;
     width: 80px;
     height: 40px;
     margin: 0 auto;
     margin-top:100px;
     left: 50%;
	top: 50%;
	margin-left:-50px;
	margin-top:-50px;
	z-index: 99999;
	display: none;
 }
 .loading span{
     display: inline-block;
     width: 8px;
     height: 100%;
     border-radius: 4px;
     background: #cbcac9;
     -webkit-animation: load 1s ease infinite;
 }
 @-webkit-keyframes load{
     0%,100%{
         height: 40px;
         background: #cbcac9;
     }
     50%{
         height: 70px;
         margin: -15px 0;
         background: #787878;
     }
 }
 .loading span:nth-child(2){
     -webkit-animation-delay:0.2s;
 }
 .loading span:nth-child(3){
     -webkit-animation-delay:0.4s;
 }
 .loading span:nth-child(4){
     -webkit-animation-delay:0.6s;
 }
 .loading span:nth-child(5){
     -webkit-animation-delay:0.8s;
 }
 .layui-nav-item{
		margin: 0 10px;
	}
 .renmingbi{
 	position: relative;
top: 1px;
display: inline-block;
font-family: 'Glyphicons Halflings';
font-style: normal;
font-weight: 400;
line-height: 1;
 }	
 		.tip {
    		 display: inline-block;
            position: relative;
            z-index: 1000;
        }

        .tip:before, .tip:after {
            opacity: 0; /*透明度为完全透明*/
            position: absolute;
            z-index: 1000; /*设为最上层*/
            /*鼠标放上元素上时的动画，鼠标放上后效果在.tip-*:hover:before, .tip-*:hover:after中设置;
            0.3s:规定完成过渡效果需要多少秒或毫秒,ease:规定慢速开始，然后变快，然后慢速结束的过渡效果*/
            transition: 0.3s ease;
            -webkit-transition: 0.3s ease;
            -moz-transition: 0.3s ease;
        }

        .tip:before {
            content: '';
            border: 6px solid transparent;
        }

        .tip:after {
            content: attr(data-tip); /*后去要提示的文本*/
            padding: 5px;
            white-space: nowrap; /*强制不换行*/
            background-color: #000000;
            color: #ffffff;
        }

        .tip:hover:before, .tip:hover:after {
            opacity: 1; /*鼠标放上时透明度为完全显示*/
            z-index: 1000;
        }

       /*left*/
        .tip-left:before {
            top: 50%;
            left: 0%;
            border-left-color: rgba(0, 0, 0, 0.8);
            margin-left: 0px;
            margin-top: -3px;
        }

        .tip-left:after {
            top: 50%;
            right: 100%;
            margin-right: 0px;
            margin-top: -6px;
        }

        .tip-left:hover:before {
            margin-left: -6px;
        }

        .tip-left:hover:after {
            margin-right: 6px;
        }
</style>
<body id="body">

<div class="loading loadEffect">
     <span></span>
     <span></span>
     <span></span>
     <span></span>
     <span></span>
</div>





	<div class="header"
		style="width: 100%; float: left; box-shadow: 0px 5px 5px #E2E2E2; position: absolute;">
		<a href="<%=request.getContextPath()%>/home/zhaopin/recruit_index.jsp"><img alt="" src="<%=request.getContextPath()%>/home/images/logo.png" style="width: 200px;height: 36px;margin-top: 6px;float: left;">
		</a>
		<!-- <div class="search-input">
			<i class="layui-icon">&#xe615;</i> <input placeholder="搜索人才库"
				value="" type="text">
		</div> -->
		<%-- <ul class="layui-nav layui-bg" lay-filter="filter">
			<li class="layui-nav-item" lay-unselect="">
				<a href="javascript:goToUrl('#');"><%=empname %><span class="layui-nav-more"></span></a><dl class="layui-nav-child"><dd><a href="#" onclick="goToUrl('#')">行政首页</a></dd><dd><a href="#" onclick="goToUrl('#')">注销</a></dd></dl>
			</li>
		</ul> --%>
		<ul class="layui-nav layui-bg recruit_menu" lay-filter="filter">

		</ul>
		
	</div>
	<div id="page-area"
		style="width: 100%; height: 91%; margin-top: 54px; float: left">
		<iframe class="top-iframe" id="frame"
			style="width: 100%; height: 100%;" frameborder="0" scrolling="yes"
			src="<%= url%>"
			>
		</iframe>
	</div>



	<!--模态层-->
	<div id="" class="talent-detail-modal">
		<div id="" class="talent-detail-mask-view"></div>
		<div id="" class="talent-detail-cont-wrapper-cont">

			<div id="left-area">
				<div class="talent_data" style="color: #666666;">
					<a style="float: right;color: #337dbc;cursor:pointer" a href="javascript:void(0);" onclick="candidateUpdata()"><i class="layui-icon">&#xe642;</i> 查看/编辑</a>
					<div class="name-cont">
						<span class="candidateName"
							style="font-family: 'Arial Normal', 'Arial'; font-weight: 400; font-style: normal; font-size: 28px; color: #333333;"></span>

					</div>
					

					<div style="font-weight: 400; margin-top: 6px;">
						渠道：<span id="channels" style="margin-right: 20px;">12</span>供应商：<span
							id="suppliers"></span>
					</div>

					<div class="info-all">
						<div class="talent-info"
							style="width: 50%; font-weight: 400; float: left">
							<span id="talent-sex" style=""></span><span
								id="talent-old" style=""></span><span
								id="talent-Education" style="margin: 0px 5px;"></span>
						</div>
						<div class="talent-info"
							style="width: 50%; font-weight: 400; float: right;">
							<span class="glyphicon glyphicon-envelope" style="margin-right: 1px;"></span><span
								id="talent-mail">847343248@qq.com</span>
						</div>
					</div>

					<div class="info-all">
						<div class="talent-info"
							style="width: 50%; font-weight: 400; float: left;">
							<span class="renmingbi">￥</span><span
								id="talent-moneny">5000-7000</span>
						</div>
						<div class="talent-info"
							style="width: 50%; font-weight: 400; float: right;">
							<span class="glyphicon glyphicon-phone"></span><span
								id="talent-phone">13888888888</span>
						</div>
					</div>

					<div class="info-all">
						<div class="talent-info"
							style="width: 50%; font-weight: 400; float: left;">
							<!-- <span class="glyphicon glyphicon-briefcase"></span><span>地址</span> -->
						</div>

					</div>
				</div>
				<!---->
				<ul class="nav nav-tabs">
					<li class="processLi" id="processDataAll" onclick="changeDate(this,'1')"><a
						href="#">总览</a></li>
					<li class="processLi active" id="processState" onclick="changeDate(this,'2')"><a
						href="#">面试情况</a></li>

				</ul>

				<!-- -->
				<div class="talent_situation">
					<!--总览 -->
					
					<!--初筛-->
					<div class="first_interview interviewSituation" style="">
						<button class="btn btn-default addfirst" style="float: right;margin-bottom: 5px;"
							onclick="addFirst()">保存</button>
						<span>初筛评价</span>
						<textarea class="form-control first_interview_text" rows="5"></textarea>
					</div>

					<!--电话面试-->
					<div class="phone_interview interviewSituation">
						<button class="btn btn-default addphone" style="float: right;"
							onclick="addFirst()">保存</button>

						<div class="phone_interview_box">
							<span style="float: left; line-height: 38px;">计划人资面试时间</span> <input
								class="layui-input" id="human_nextTime"
								style="float: left; width: 160px;margin-left: 5px;"
								placeholder="yyyy-MM-dd HH:mm:ss" type="text"> <span
								style="margin-left: 50px; float: left; line-height: 38px;">计划部门面试时间</span>
							<input class="layui-input" id="department_nextTime"
								style="width: 160px;float: left;margin-left: 5px;" placeholder="yyyy-MM-dd HH:mm:ss"
								type="text">

						</div>

						<span style="margin-bottom: 5px;float: left;">电话面试评价</span>
						<textarea class="form-control phone_interview_text" rows="5"></textarea>

					</div>
					<!--人资面试-->
					<div class="human_interview_all">
					<div class="human_interview interviewSituation">
  								<button  class="btn btn-default addhuman" style="float: right;" onclick="">保存</button>
  								<div id="human_interview_input" style="margin: 15px 0px 10px 0px;float: left;">
  								<span class="score_title" style="float:left;line-height: 38px;">实际人资面试时间</span>
  								<input class="layui-input dataTime" id="actual_interview_time" placeholder="yyyy-MM-dd HH:mm:ss" value="" style="width: 160px;float:left" type="text">
  								
  								<span class="score_title" style="margin-left: 50px; float:left;line-height: 38px;">计划部门面试时间</span>
  								<input class="layui-input dataTime" id="planned_interview_time" placeholder="yyyy-MM-dd HH:mm:ss" value=""  style="width: 160px;float:left" type="text">
  								<span class="score_title" style="float:left;line-height: 38px;">面试官</span>
  								<input class="layui-input getStaff" id="" onclick="getStaff(this)" data="" placeholder="人员选择" value=""  style="width: 160px;float:left" type="text">
  								</div>
  								
  								<textarea class="form-control human-form-control" rows="4" placeholder="其他评价" value="" >意见</textarea>
  							</div>
  					</div>
					<!-- 部门面试 -->
					<!-- 待入职 -->
					<div class="pending_interview interviewSituation">
						<button class="btn btn-default addpending" style="float: right;" onclick="addPending()">保存</button>
						<div style="float: left;margin-top: 30px;">
							<span class="" style="float:left;line-height: 38px;">计划入职时间</span>
  							<input class="layui-input dataTime" id="entry_interview_time" placeholder="yyyy-MM-dd" value=""  style="width: 150px;float:left;margin-left: 5px;" type="text">
						</div>
						<div class="pending_interview_box">
							
							<span style="float: left; line-height: 38px;">是否延期</span>

							<div class="btn-group" style="float: left;margin-left: 33px;">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									<span class="isPending">否</span> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="javascript:void(0);" onclick="isYesNo('1')">是</a></li>
									<li><a href="javascript:void(0);" onclick="isYesNo('2')">否</a></li>

								</ul>
							</div>

						</div>
						<div class="pending-box" style="margin: 10px 0px; display: none">
							<span style="float: left; line-height: 38px;">延期至</span> <input
								class="layui-input" id="pending_nextTime"
								style="float: left;margin-left: 47px; width: 150px;"
								placeholder="yyyy-MM-dd" type="text">

							<textarea class="form-control pending_interview_text"
								placeholder="延期原因" rows="5" style="margin-top: 5px;float: left;"></textarea>
						</div>
					</div>
				</div>

			</div>

			<!-- 右半部分 -->
			<div id="right-area">

				<ul class="nav nav-pills nav-justified step step-round"
					data-step="3">
					<li><a>初筛</a></li>
					<li><a>电话</a></li>
					<li><a>人资</a></li>
					<li><a>部门</a></li>
					<li><a>待入职</a></li>
				</ul>
				<div class="interview_button">
					<div class="positionName">职位:中级工程师</div>
					<div class="btn-group" style="width: 100%; margin: 10px 0px;">
						<button type="button" class="btn btn-default" id="rencaiButton" style="width: 35%;" onclick="updaTetalents()">同步人才库</button>
						
						<button type="button" class="btn btn-default abandon" id="abandonButton"
							onclick="closeTalent()" style="width: 50%;">结束流程(淘汰)</button>
						<button type="button" class="btn btn-default tip tip-left" id="qianyiButton" data-tip="候选人转移" style="width: 15%;" onclick="isShiftCandidate()"><span class="glyphicon glyphicon-user" style="line-height:20px;"></span><span class="glyphicon glyphicon-transfer" style="line-height:20px;"></span></button>	
					</div>
					<div class="btn-group" style="width: 100%; margin: 10px 0px;">
						<button type="button" class="btn btn-default" id="sendEmail" onclick="sendEmail()" style="width: 40%;margin-bottom: 8px;">发送邮件/邀请</button>
						<button type="button" class="btn btn-default" id="sendOffer" onclick="sendOffer()" style="width: 60%;margin-bottom: 8px;">发送offer</button>
					</div>
					
					<div class="stage-btn-con">
						<span class="btn2 pushProcess" onclick="">推进至下一环节</span> <span
							class="drop-down-btn layui-icon" data="no">&#xe61a;</span>
						<div class="rc-trigger-popup  "
							style="left: 1020px; top: 260px; position: absolute; z-index: 1050; display: none;">
							<ul id="selectcont" class="icon-btn-drop-down-cont"
								style="background-color: #fff; -webkit-box-shadow: 0 1px 5px 0 rgba(51, 51, 51, .2); box-shadow: 0 1px 5px 0 rgba(51, 51, 51, .2); border-radius: 4px; padding: 4px 0; width: 140px;">

							</ul>
						</div>
					</div>
				</div>
				<!-- <div style="margin-left: 5%;">
					<span style="line-height: 38px;">人员入职日期</span> <input
						class="layui-input" id="" style="width: 200px;"
						placeholder="yyyy-MM-dd HH:mm:ss" type="text">
				</div> -->
				<!-- 评估板 -->
				<div class="assess">
					
				</div>
				<div class="add-assess" onclick="addTemplateRecord()">+添加评估</div>
			</div>
			<div id="btn-cont" class="layui-icon" onclick="closeMode()">&#x1006;</div>
		</div>


	</div>
	<script type="text/javascript">
		var layer;
		var laydate;
		var empname='<%=empname %>';
		
		/* var evaluationObj; */
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});              
    	/* layui.use('element', function(){
		  var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
		  
		  //监听导航点击
		  element.on('nav(filter)', function(elem){
		    //console.log(elem)
		    console.log(elem.html());
		  });
		}); */
		
		
		//时间控件
		layui.use('laydate', function(){
		  laydate = layui.laydate;
		  
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#human_nextTime' //指定元素
		    ,type: 'datetime'
		    ,format: 'yyyy-MM-dd HH:mm:ss'
		  });
		  laydate.render({
		    elem: '#department_nextTime' //指定元素
		    ,type: 'datetime'
		    ,format: 'yyyy-MM-dd HH:mm:ss'
		  });
		  
		  laydate.render({
		    elem: '#pending_nextTime' //指定元素
		    ,type: 'date'
		    ,format: 'yyyy-MM-dd'
		  });
		  
		  var a="#actual_interview_time";
		var b="#planned_interview_time";
		var c="#entry_interview_time";
			laydate.render({
			elem: a //指定元素
			,type: 'datetime'
			,format: 'yyyy-MM-dd HH:mm:ss'
			});
			laydate.render({
			elem: b //指定元素
			 ,type: 'datetime'
			,format: 'yyyy-MM-dd HH:mm:ss'
			});
			laydate.render({
			elem: c //指定元素
			 ,type: 'date'
			,format: 'yyyy-MM-dd'
			});
		  
		  
		});
		
		//跳转工作台
		function goToUrlForWord(){
			var recruit_id="<%=recruit_id %>";
			var planId="<%=planId %>";
			var url="<%=request.getContextPath()%>/campusRecruitment/process/workBench.jsp?planId="+planId+"&recruit_id="+recruit_id;
			window.open(url);
		}								
		
		function goToUrl(url){
			if(url=="#"){
				return;
			}
			var frameUrl="<%=request.getContextPath()%>" + url;
			document.getElementById("frame").src = frameUrl;
		}
		
		//防止session过期
		setInterval('refreshQuery()',1700000); 
		/* 刷新查询 */
		function refreshQuery(){
		   $.ajax({
				url: "org.gocom.components.coframe.auth.LoginManager.preventExpiry.biz.ext",
				type: "POST",
				success: function(data){
				}
			})
		} 
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/home/js/recruit_menu.js"></script>
</body>
</html>