<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 30000082
  - Date: 2018-11-19 09:13:50
  - Description:
-->
<head>
<title>日程安排</title>
	
	<%
		String  id = request.getParameter("id");
		
	 %>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/home/schedule/css/fullcalendar.css">
	<style type="text/css">
	#calendar {
		width: 1200px;
		margin: 20px auto 10px auto
	}
	
	body {
		font-size: 12px;
		font-family: '微软雅黑';
	}
	
	a {
		color: #666;
		text-decoration: none;
	}
	</style>
	<script src='<%= request.getContextPath() %>/home/schedule/js/jquery-ui-1.10.2.custom.min.js'></script>
	<script src='<%= request.getContextPath() %>/home/schedule/js/fullcalendar.min.js'></script>
	<script type="text/javascript">
			var date = new Date();//获取系统当前时间
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			
			var myDate=date.toLocaleString(); //获取日期与时间
			//获取前8位
			myDate=myDate.substring(0, 8);
			myDate=myDate.replace("/", "-");
			myDate=myDate.replace("/", "");
			
			var ids=<%=id %>;
			
			$(function() {
			
				if(ids!=null && ids!="" && typeof(ids) != "undefined"){
					$('#calendar').fullCalendar({
						//点击方法
						eventClick: function(calEvent, jsEvent, view) {
							 
					        parent.getTalentData(calEvent.id);
					    },
						header : {
							left : 'prev,next today',
							center : 'title',
							right : 'month,agendaWeek,agendaDay'
						},
						firstDay : 1,
						editable : false,//禁止拖动
						weekends: true ,//隐藏周末  true：不隐藏  false：隐藏
						timeFormat : 'H:mm',
						axisFormat : 'H:mm',
						defaultView:'agendaDay',//设置初始日：agendaDay，可切换月：month，周：agendaWeek
						events: function(start, end, callback) {
						
							var json={
								time:myDate
							}
					        $.ajax({
					            url: 'com.recruit.home.queryCandidateInformation.selectInterviewArrange.biz.ext',
					            type: 'post',
					            data: json,
	            				async:false,
					            success: function(data) {
					                var events = [];
					                var cand = data.cand;
					                //循环面试安排数据
					                for(var i=0;i<cand.length;i++){
					                	if(cand[i].seal=="人资"){
					                		//赋值到events
						               		events.push({
						               			id:cand[i].id,
						                		title:cand[i].candidate_name,
						                		start:cand[i].interview_time,
							                    allDay : false,
							                    color:"#62dbb7"
						                	}); 
					                	}else if(cand[i].seal=="部门"){
					                		//赋值到events
						               		events.push({
						               			id:cand[i].id,
						                		title:cand[i].candidate_name,
						                		start:cand[i].interview_time,
							                    allDay : false,
							                    color:"#ff5722"
						                	}); 
					                	}
					                	
						            }
					                callback(events);
					            }
					        });
					    }
					});
						
				}else{
					
					$('#calendar').fullCalendar({
						//点击方法
						eventClick: function(calEvent, jsEvent, view) {
							 
					        parent.getTalentData(calEvent.id);
					    },
						header : {
							left : 'prev,next today',
							center : 'title',
							right : 'month,agendaWeek,agendaDay'
						},
						firstDay : 1,
						editable : false,//禁止拖动
						weekends: true ,//隐藏周末  true：不隐藏  false：隐藏
						timeFormat : 'H:mm',
						axisFormat : 'H:mm',
						defaultView:'month',//设置初始日：agendaDay，可切换月：month，周：agendaWeek
						events: function(start, end, callback) {
						
							var json={
								time:myDate
							}
					        $.ajax({
					            url: 'com.recruit.home.queryCandidateInformation.selectInterviewArrange.biz.ext',
					            type: 'post',
					            data: json,
	            				async:false,
					            success: function(data) {
					                var events = [];
					                var cand = data.cand;
					                 //循环面试安排数据
					                for(var i=0;i<cand.length;i++){
					                	if(cand[i].seal=="人资"){
					                		//赋值到events
						               		events.push({
						               			id:cand[i].id,
						                		title:cand[i].candidate_name,
						                		start:cand[i].interview_time,
							                    allDay : false,
							                    color:"#62dbb7"
						                	}); 
					                	}else if(cand[i].seal=="部门"){
					                		//赋值到events
						               		events.push({
						               			id:cand[i].id,
						                		title:cand[i].candidate_name,
						                		start:cand[i].interview_time,
							                    allDay : false,
							                    color:"#ff5722"
						                	}); 
					                	}
					                	
						            }
					                callback(events);
					            }
					        });
					    }
					});
				}
				
			});
	
		function aa(){
			
			var date=$(".fc-header-title h2").text();
			//获取前8位
			date=date.substring(0, 8);
			date=date.replace("年", "-");
			date=date.replace("月", "");
			
			//先清空数据
			$("#calendar").fullCalendar('removeEvents');
			
			var json={
				time:date
			}
	        $.ajax({
	            url: 'com.recruit.home.queryCandidateInformation.selectInterviewArrange.biz.ext',
	            type: 'post',
	            data: json,
	            async:false,
	            success: function(data) {
	            
	                var events = [];
	                var cand = data.cand;
	                //循环面试安排数据
	                for(var i=0;i<cand.length;i++){
	                	if(cand[i].seal=="人资"){
	                		//赋值到events
		               		events.push({
		               			id:cand[i].id,
		                		title:cand[i].candidate_name,
		                		start:cand[i].interview_time,
			                    allDay : false,
			                    color:"#62dbb7"
		                	}); 
	                	}else if(cand[i].seal=="部门"){
	                		//赋值到events
		               		events.push({
		               			id:cand[i].id,
		                		title:cand[i].candidate_name,
		                		start:cand[i].interview_time,
			                    allDay : false,
			                    color:"#ff5722"
		                	}); 
	                	}
	                	
		            }
		            
		            //赋值数据
					$("#calendar").fullCalendar('addEventSource',events);
	            }
	        });
		}
	</script>
</head>

<body>
	<div id="main" style="width: 1200px;margin: 0px auto;">
		<fieldset class="layui-elem-field layui-field-title">
			<legend>日程面试安排</legend>
		</fieldset>
		<div>
			<img src="<%= request.getContextPath() %>/home/schedule/img/green.png" style="margin-top: -3px;margin-left: 32px;" width="14px">
			<span>人资面试</span>
			<img src="<%= request.getContextPath() %>/home/schedule/img/red.png" style="margin-top: -3px;margin-left: 10px;" width="14px">
			<span>部门面试</span>
		</div>
		<div id='calendar'></div>
	</div>

</body>