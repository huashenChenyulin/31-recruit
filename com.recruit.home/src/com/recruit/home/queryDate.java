package com.recruit.home;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.eos.system.annotation.Bizlet;

@Bizlet("获取当前时间")
public class queryDate {
	/**
	 * Date 返回当前月度的第一天时间
	 * @return
	 */
	@Bizlet("获取当前月的第一天")
	public static Date queryFirstDay() {
		Calendar calst = Calendar.getInstance();
		calst.add(Calendar.MONTH, 0);
		calst.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天 
		//设置审核时间为0时0分0秒  
		calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
		calst.set(java.util.Calendar.MINUTE, 0);
		calst.set(java.util.Calendar.SECOND, 0);
		Date date = calst.getTime();
		return date;
	}

	public static void main(String[] args) {
		queryFirstDay();
		queryWeek();
	}

	/**
	 * @return
	 */
	@Bizlet("获取本周的日期集合")
	public static List<Date> queryWeek() {
		String yz_time = getTimeInterval(new Date());//获取本周时间
		String array[] = yz_time.split(",");
		String start_time = array[0];//本周第一天
		String end_time = array[1]; //本周最后一天 
		//格式化日期     
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dBegin = null;
		try {
			dBegin = sdf.parse(start_time);
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		Date dEnd = null;
		try {
			dEnd = sdf.parse(end_time);
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		List<Date> lDate = findDates(dBegin, dEnd);//获取这周所有date
		for (Date date : lDate) {
			System.out.println(sdf.format(date));
		}
		return lDate;
	}

	public static String getTimeInterval(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了  
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天  
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		// System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期  
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一  
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		// 获得当前日期是一个星期的第几天  
		int day = cal.get(Calendar.DAY_OF_WEEK);
		// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值  
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
		String imptimeBegin = sdf.format(cal.getTime());
		System.out.println("所在周星期一的日期：" + imptimeBegin);
		cal.add(Calendar.DATE, 6);
		String imptimeEnd = sdf.format(cal.getTime());
		// System.out.println("所在周星期日的日期：" + imptimeEnd);  
		return imptimeBegin + "," + imptimeEnd;
	}

	public static List<Date> findDates(Date dBegin, Date dEnd) {
		List lDate = new ArrayList();
		lDate.add(dBegin);
		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间  
		calBegin.setTime(dBegin);
		Calendar calEnd = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间  
		calEnd.setTime(dEnd);
		// 测试此日期是否在指定日期之后  
		while (dEnd.after(calBegin.getTime())) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量  
			calBegin.add(Calendar.DAY_OF_MONTH, 1);
			lDate.add(calBegin.getTime());
		}
		return lDate;
	}

	/**
	 * @return
	 */
	@Bizlet("获取后一天的日期")
	public static Date queryTomorrow(Date today) {
		Calendar c = Calendar.getInstance();
		c.setTime(today);
		c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  

		Date tomorrow = c.getTime();
		//System.out.println("后一天是:" + f.format(tomorrow));  
		return tomorrow;
	}
	
	/**
	 * today 获取昨天时间的参考时间（）
	 * @return
	 */
	@Bizlet("获取前一天的开始时间")
	public static Date querYesterday(Date today) {
		Calendar calst = Calendar.getInstance();
		calst.setTime(today);
		//设置审核时间为0时0分0秒  
		calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
		calst.set(java.util.Calendar.MINUTE, 0);
		calst.set(java.util.Calendar.SECOND, 0);
		calst.add(Calendar.DAY_OF_MONTH, -1);// 今天-1天  
		Date tomorrow = calst.getTime();
		//System.out.println("前一天是:" + f.format(tomorrow));  
		return tomorrow;
	}
	/**
	 * today 获取明天时间的参考时间（）
	 * @return
	 */
	@Bizlet("获取后一天的开始时间")
	public static Date queryQueenday(Date today) {
		Calendar calst = Calendar.getInstance();
		calst.setTime(today);
		//设置审核时间为0时0分0秒  
		calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
		calst.set(java.util.Calendar.MINUTE, 0);
		calst.set(java.util.Calendar.SECOND, 0);
		calst.add(Calendar.DAY_OF_MONTH, 1);// 今天-1天  
		Date tomorrow = calst.getTime();
		//System.out.println("前一天是:" + f.format(tomorrow));  
		return tomorrow;
	}
	/**
	 * today 获取时间的参考时间（）
	 * @return
	 */
	@Bizlet("获取每天的开始时间")
	public static Date querInitialTime(Date today) {
		Calendar calst = Calendar.getInstance();
		calst.setTime(today);
		//设置时间为0时0分0秒  
		calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
		calst.set(java.util.Calendar.MINUTE, 0);
		calst.set(java.util.Calendar.SECOND, 0);
		Date tomorrow = calst.getTime();
		return tomorrow;
	}
	/**
	 * @param param
	 * @return
	 */
	@Bizlet("将本周每天添加候选人数统计放入List中")
	public static List<Integer> param(int param,List list) {
		//定义一个ArrayList 并实例化
		List<Integer> List = new ArrayList<Integer>();
		//判断当传入的List 不为空时将传入的List放入实例化List中
		if(list!=null){
			List.addAll(list);
		}
		//将统计数放入list中
		List.add(param);//插入第一个元素
        //System.out.println(List);
		return List;
	}
	/**
	 * @param date  2018-07-01 ~ 2018-07-31
	 * @return  
	 */
	@Bizlet("拆分时间字段为数组")
	public static String[] DateString(String date) {
		String[] dateArray = date.split("~");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dates = new Date();

		for (int i = 0; i < dateArray.length; i++) {
			String string = dateArray[i];
			try {
				dates = sdf.parse(string);
				dateArray[i] = sdf.format(dates);
				if (i == dateArray.length - 1) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(dates);
					calendar.add(Calendar.DAY_OF_MONTH, 1);
					dates = calendar.getTime();
					dateArray[i] = sdf.format(dates);
				}
			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		return dateArray;
	}
}
