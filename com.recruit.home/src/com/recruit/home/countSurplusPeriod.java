/**
 * 
 */
package com.recruit.home;

import java.util.Date;

import com.eos.system.annotation.Bizlet;

/**
 * @author 唐能
 * @date 2018-04-23 09:30:38
 *
 */
@Bizlet("计算剩余招聘周期")
public class countSurplusPeriod {

	/**
	 * @param newDate 现在时间
	 * @param auditTime 审核时间
	 * @param recruiPeriod 招聘周期
	 * @return 计算公式 招聘周期 -(现在时间 - 审核时间)/(24*60*60*1000);单位（天）
	 * @author 唐能
	 */
	@Bizlet("")
	public static long countPeriod(Date newDate, Date auditTime,String recruiPeriod) {
		java.util.Calendar calst = java.util.Calendar.getInstance();   
        java.util.Calendar caled = java.util.Calendar.getInstance(); 
        //审核时间
        calst.setTime(auditTime);
        //当前时间
         caled.setTime(newDate);   
         //设置审核时间为0时0分0秒  
         calst.set(java.util.Calendar.HOUR_OF_DAY, 0);   
         calst.set(java.util.Calendar.MINUTE, 0);   
         calst.set(java.util.Calendar.SECOND, 0);
       //设置当前时间为0时0分0秒  
         caled.set(java.util.Calendar.HOUR_OF_DAY, 0);   
         caled.set(java.util.Calendar.MINUTE, 0);   
         caled.set(java.util.Calendar.SECOND, 0);   
        //得到两个日期相差的天数   
         long days = ((int) (caled.getTime().getTime() / 1000) - (int) (calst   
                .getTime().getTime() / 1000)) / 3600 / 24;   
         long countPeriod = Long.parseLong(recruiPeriod) - days - 1;
        return countPeriod;  
	}

}
