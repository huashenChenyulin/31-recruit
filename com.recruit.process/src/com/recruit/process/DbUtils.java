package com.recruit.process;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import com.eos.common.connection.DataSourceHelper;
import com.eos.system.annotation.Bizlet;

/**
 * @author wangyj
 * @date 2015-08-28 21:00:46
 *
 */
@Bizlet("")
public class DbUtils {

	@Bizlet("")
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map getConfValue(String fieldtype) throws Exception {

		Map confs = new HashMap();

		String sqlStr = "select * from org_sfy_fielddefine where fieldtype = '"
				+ fieldtype + "' order by sortnum asc";
		//System.out.println(sqlStr);

		//这句后期要改掉，使用下面那句
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		DataSource ds = DataSourceHelper.getDataSource("default");

		Connection con = null;
		Statement stmt = null;

		try {
			//这句后期也要改掉，使用下面那句
			//con = DriverManager.getConnection("jdbc:mysql://10.10.19.207:3306/DEVELOP_DB", "develop", "primeton123"); 
			con = ds.getConnection();
			stmt = con.createStatement();

			ResultSet resultSet = stmt.executeQuery(sqlStr);
			int i = 0;
			String defineid, visible, fieldname, columnname, sortnum;
			while (resultSet.next()) { //循环遍历查询结果。
				i++;
				defineid = resultSet.getString("defineid");//从结果集中取值的方式。
				sortnum = resultSet.getString("sortnum");
				visible = resultSet.getString("visible");
				fieldname = resultSet.getString("fieldname");
				columnname = resultSet.getString("columnname");
				confs.put("defineid" + i, defineid);
				confs.put("sortnum" + i, sortnum);
				confs.put("visible" + i, visible);
				confs.put("fieldname" + i, fieldname);
				confs.put("columnname" + i, columnname);
			}
			confs.put("length", i);
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return confs;
	}

	@Bizlet("拆分机构表orgseq字段为orgid")
	public List<String> getOrgidsByOrgseq(String orgseq) {
		List<String> orgids = new ArrayList<String>();
		String str[] = orgseq.split("\\.");
		for (int i = 0; i < str.length; i++) {
			if (!str[i].equals("")) {
				orgids.add(str[i]);
			}
		}
		return orgids;
	}

	@Bizlet("时间类型转换")
	public Date getDateByString(String x) {
		Date date = new Date();
		SimpleDateFormat sdf1 = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
		try {
			date = sdf1.parse(x);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	@Bizlet("字符串处理")
	public String getSAPTime(String time) {
		String str = null;
		str = time.replaceAll(":", "");
		if(str.length()<6){
			str = "0" + str;
		}
		return str;
	}
}
