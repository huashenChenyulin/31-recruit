package com.recruit.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author 唐能
 * @date 2018-03-12 19:11:09
 *
 */
@Bizlet("jointPosition")
public class jointPosition {
	/**
	 * @author 唐能
	 * @date 2018-03-12 19:11:09
	 * param1   对象数组，
	 * field  对象中的字段
	 * 备注：将对象数组中的字段值取出并拼接成如 "00000001,00000003" 或者是 "工程师,普工"等等类型的字符串
	 * 用于查询条件中in的需求
	 */
	@Bizlet("")
	public static String position(DataObject[] param1, String field) {

		String position = "";
		//遍历对象数组
		for (int i = 0; i < param1.length; i++) {
			//初始化并获取遍历后的值
			DataObject dataObject = param1[i];
			//判断如果数组程度不等于1时
			if (i != param1.length - 1) {
				if (dataObject.getString(field) != null) {
					position += dataObject.getString(field) + ",";
				}
			} else {
				if (dataObject.getString(field) != null) {
					position += dataObject.getString(field);
				}
			}

		}
		if (position == "") {
			position = null;
		}
		return position;
	}

	@Bizlet("面试官名称，id转List集合")
	public List<Map> listString(String userid,String username) {
		String str[] = null;
		String name[] = null;
		List<Map> list = new ArrayList<Map>();
		
		if (userid != null && userid != "") {
			str = userid.split(",");
		}
		
		if (username != null && username != "") {
			name = username.split(",");
		}
		
		for (int i = 0; i < name.length; i++) {
			Map map = new HashMap();
			map.put("id", str[i]);
			map.put("name", name[i]);
			list.add(map);
		}
		return list;
	}

	/**
	 * @param dateObject 查询出的面试官部门信息集合
	 * @return
	 */
	@Bizlet("拼接面试官部门信息")
	public static Map interviewer(DataObject[] dateObject) {
		Map date = new HashMap();
		String L1 = "";
		String L1Name = "";
		String L2 = "";
		String L2Name = "";
		String L3 = "";
		String L3Name = "";
		String L4 = "";
		String L4Name = "";
		for (int i = 0; i < dateObject.length; i++) {
			if(i==0){
				if(dateObject[i].get("l1")!=null){
					L1 += dateObject[i].get("l1");
				}
				if(dateObject[i].get("l1name")!=null){
					L1Name += dateObject[i].get("l1name");
				}
				if(dateObject[i].get("l2")!=null){
					L2 += dateObject[i].get("l2");
				}
				if(dateObject[i].get("l2name")!=null){
					L2Name += dateObject[i].get("l2name");
				}
				
				if(dateObject[i].get("l3")!=null){
					L3 += dateObject[i].get("l3");
				}
				if(dateObject[i].get("l3name")!=null){
					L3Name += dateObject[i].get("l3name");
				}
				if(dateObject[i].get("l4")!=null){
					L4 += dateObject[i].get("l4");
				}
				if(dateObject[i].get("l4name")!=null){
					L4Name += dateObject[i].get("l4name");
				}
			}else{
				if(dateObject[i].get("l1")!=null){
					L1 +=","+ dateObject[i].get("l1");
				}
				if(dateObject[i].get("l1name")!=null){
					L1Name += ","+dateObject[i].get("l1name");
				}
				if(dateObject[i].get("l2")!=null){
					L2 += ","+dateObject[i].get("l2");
				}
				if(dateObject[i].get("l2name")!=null){
					L2Name += ","+dateObject[i].get("l2name");
				}
				
				if(dateObject[i].get("l3")!=null){
					L3 += ","+dateObject[i].get("l3");
				}
				if(dateObject[i].get("l3name")!=null){
					L3Name += ","+dateObject[i].get("l3name");
				}
				if(dateObject[i].get("l4")!=null){
					L4 += ","+dateObject[i].get("l4");
				}
				if(dateObject[i].get("l4name")!=null){
					L4Name += ","+dateObject[i].get("l4name");
				}
			}
		}
		date.put("L1", L1);
		date.put("L1Name", L1Name);
		date.put("L2", L2);
		date.put("L2Name", L2Name);
		date.put("L3", L3);
		date.put("L3Name", L3Name);
		date.put("L4", L4);
		date.put("L4Name", L4Name);
		
		return date;
	}
}
