package com.recruit.health;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.eos.foundation.common.io.FileUtil;
import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class ConvertBase {
	/**
	 * 文件转化为字节数组字符串，并对其进行Base64编码处理
	 * @param file 附件
	 * @return
	 */
	@Bizlet("将附件进行Base64编码处理")
	public static String fileBase64String(String file) {
		//将文件转化为字节数组字符串，并对其进行Base64编码处理  
		byte[] buffer = null;
		FileInputStream fis = null;
		ByteArrayOutputStream bos = null;
		File f = null;
		try {
			//读取文件字节数组 
			f = new File(file);
			System.out.println(f.isFile());
			System.out.println(f.exists());
			fis = new FileInputStream(f);
			bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			bos.flush();
			buffer = bos.toByteArray();
			return new String(new sun.misc.BASE64Encoder().encode(buffer))
					.replaceAll("\r\n", "");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bos != null) {
					bos.close();
				}
				if (fis != null) {
					fis.close();
				}
				if (file.endsWith(".zip")) {
					FileUtil.deleteFile(file);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
