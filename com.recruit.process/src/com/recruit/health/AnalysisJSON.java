package com.recruit.health;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class AnalysisJSON {

	/**
	 * 	
	 * 解析json格式化，解析出流程号并返回
	 * @param json 接收接口返回json格式
	 * @return 返回流程号
	 */
	@Bizlet("解析流程返回参数")
	public static String anlysisJsonData(String json) {
		JSONObject jsonObj = JSON.parseObject(json);
		JSONObject jsonProcess = (JSONObject) jsonObj.get("processInstInfo");
		return jsonProcess.get("processInstID").toString();
	}
	
}
