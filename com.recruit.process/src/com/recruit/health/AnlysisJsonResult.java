package com.recruit.health;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class AnlysisJsonResult {
	/**
	 * 	
	 * 解析json格式化，解析message号并返回
	 * @param json 接收接口返回json格式
	 * @return 返回流程号
	 */
	@Bizlet("解析JSON返回message数据")
	public static String anlysisResult(String json) {
		JSONObject jsonObj = JSON.parseObject(json);
		return  jsonObj.get("result").toString();

	}

}
