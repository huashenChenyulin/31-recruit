/**
 * 
 */
package com.recruit.health;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.eos.system.annotation.Bizlet;

import commonj.sdo.DataObject;

/**
 * @author jiangsanbin
 * @date 2018-05-09 17:05:181
 * 
 */
@Bizlet("")
public class JSONConvert {

	@Bizlet("转JSON格式")
	public static String convertJson(DataObject[] data) {
		JSONObject jsonObject = new JSONObject();// 所有请求参数信息
		
		DataObject obj = data[0];
		JSONObject entityObj = new JSONObject();// 存放体检信息
		entityObj.put("applydate", obj.get("applydate"));// 申请时间
		entityObj.put("applyemp", obj.get("userId"));// 当前员工工号
		entityObj.put("applyorg", obj.get("userOrg"));// 当前员工部门ID
		entityObj.put("jpbm", obj.get("jpbmfb"));// 工单部门
		entityObj.put("jpbmfb", obj.get("L1"));// 工单中心部门
		entityObj.put("position", obj.get("position"));// 工单职位
		entityObj.put("tjbgid", obj.get("id"));// 体检报告ID
		entityObj.put("tjbstatus", "");// 体检状态
		entityObj.put("title",obj.get("title"));// 体检标题
		
		JSONArray entitiesArray = new JSONArray();
		entitiesArray.add(entityObj);
		
		JSONObject json = new JSONObject();// 申请的流程
		json.put("processDefID", "10421");// 流程号
		
		JSONObject jsonobj = new JSONObject();// 申请人对象
		jsonobj.put("empId", obj.get("userId"));// 当前员工工号
		
		
		//附件
		JSONObject file = new JSONObject();
		file.put("fileName",obj.get("health_report_name"));
		file.put("fileType", obj.get("health_report_type"));// 体检报告文件类型
		file.put("fileBase64Data", obj.get("fileBase64Data"));// 体检报告路径
		
		JSONObject allFile = new JSONObject();
		allFile.put("fileField", "attachment");
		JSONArray jsonArray = new JSONArray();
		jsonArray.add(file);
		allFile.put("files", jsonArray);
		
		JSONArray fileInfos = new JSONArray();
		fileInfos.add(allFile);
		
		jsonObject.put("attachmentInfos", fileInfos);
		jsonObject.put("processDefInfo", json);
		jsonObject.put("entities", entitiesArray);
		jsonObject.put("applyEmpInfo", jsonobj);
		
		return jsonObject.toString();// 返回json格式
	}
}
