package com.recruit.health;

import com.eos.system.annotation.Bizlet;

@Bizlet("")
public class AnalysisOrg {
	/**
	 * 返回最后一级的部门
	 * @param L2 部门编码
	 * @param L3 科组编码
	 * @param L4 
	 * @param L4科室编码
	 * @return
	 */
	@Bizlet("获取最后一级部门")
	public static String filtrateOrg(String L2, String L3, String L4) {
		String resultData = "";
		if (L4 != null) {
			resultData = L4;
		} else if (L3 != null) {
			resultData = L3;
		} else if (L2 != null) {
			resultData = L2;
		}
		return resultData;
	}
}
