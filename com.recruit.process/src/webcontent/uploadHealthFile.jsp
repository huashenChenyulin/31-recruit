<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp" %> 
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): 陈海江
  - Date: 2018-05-17 16:29:42
  - Description:
-->
<head>
<title>文件上传</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="<%=request.getContextPath() %>/process/css/uploadHealthFile.css?v=1.0" rel="stylesheet"  type="text/css">
</head>
<body>
 
<div class="layui-upload">
  <button type="button" class="layui-btn layui-btn-normal" id="fileBnt">选择文件</button>
  <span class="span-black"></span>
  <button type="button" class="layui-btn" id="uploadBnt">开始上传</button>
 
  <div class="layui_bottom">
  	<div id="beforeUpload"></div><!-- 附件未上传前 -->
  	<div id="laterupload"></div><!-- 附件上传后 -->
  </div>
</div>
<div class="layui-bottom">
	<p class="bottom-p">注：多份文件请打包成一份压缩文件再进行上传！</p>
</div>

	
	<script type="text/javascript">
	
	var contextPath ="<%=request.getContextPath() %>";
	var resultBool =false;//返回是否上传成功--false：上传失败，true:上传成功
	var notUpload =false;//返回是否已上传文件--false:未上传，true已上传
	var healthReportUrl=""; //附件上传路径
	var healthReportName=""; //附件名称
	var healthReportType ="";//附件类型
	var uploadHtml ="";//赋值附件是否已上传
	
	//上传附件
	layui.use('upload', function(){
		var $ = layui.jquery, upload = layui.upload;
		var load ="";
			upload.render({
				elem : '#fileBnt',
				url : contextPath + '/interpolation/file_uploadserver.jsp',
				auto : false,
				bindAction : '#uploadBnt',
				accept :'file',
				done : function(res) {
					
					if (res.code == 0) {//文件上传服务器成功
						healthReportUrl =res.data.filePath;//文件上传路径
						healthReportName =res.data.fileName;//文件名称
						healthReportType =res.data.fileType.substring(1);//文件类型
				    	$("#laterupload").show();//显示已上传文件
						$("#beforeUpload").hide();//隐藏未上传文件
				      	$("#laterupload").text(healthReportName+"."+healthReportType);//赋值文件名称
				      	uploadHtml=$("#laterupload").text();//获取附件值
				      	notUpload =true;//文件已上传
						resultBool=true;//文件上传成功
						layer.close(load);//隐藏加载条
					} else {
						resultBool=false;
						layer.close(load);//隐藏加载条
						layer.msg("上传失败，未上传到服务器，请联系管理员！");
					}
				}
				,choose: function(obj){ //文件上传前回调
				    obj.preview(function(index, file, result){//预读文件
				    	var laterupload=$("#laterupload").text();//获取已上传文件元素
				    		if(laterupload!=""){//如果选第二次文件，第一次文件已上传，则覆盖第一次已上传文件并赋值为"";
				    			$("#laterupload").text("");//赋值已上传的文件为空
				    		}
				    	$("#laterupload").hide();//隐藏已上传文件
						$("#beforeUpload").show();//显示未上传文件
				    	$("#beforeUpload").text(file.name);//得到文件名称
				    	notUpload =false;//文件未上传
				    });
				},
				before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
				   	load = layer.load(1); //上传loading
				  }
			});
		});
		
		
		
	</script>
</body>
</html>