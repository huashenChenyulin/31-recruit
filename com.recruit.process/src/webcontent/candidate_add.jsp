<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp" %> 
	<%@page import="com.eos.foundation.eoscommon.BusinessDictUtil"%>
	<%
		String rooturl=BusinessDictUtil.getDictName("OA_CONFIG","OA_ROOT_URL");
		String orderId = request.getParameter("orderId");
	 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-23 14:41:42
  - Description:
-->
<head>
<link href="<%= request.getContextPath() %>/process/css/candidate_add.css" rel="stylesheet">
<link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
<script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
<script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/>
<script src="<%= request.getContextPath() %>/process/js/candidate_add.js?v=1.0" type="text/javascript"></script>

<title>添加候选人信息</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 <style type="text/css">
 	.tp_name{
 		color: #fda900;
 	}
	.tp_name:HOVER{
		color: #fda900;
	}
	.btn-default:hover{
		color: #ffffff; 
     background-color: #ffcc66; 
     border-color: #ffcc66; 
	}
 </style>
</head>
<body >
<div>
  
  	<div>
  	<ul id="myTab" class="nav nav-tabs">
  	
  		<li class="active" onclick="tabAddCandidate()" >
	
		<a href="" data-toggle="tab" class="a">添加候选人</a>
		
		</li>
		
		<li class="archive" onclick="tabTalentPool()">
	
		<a href="" data-toggle="tab" class="a">人才库<i class="layui-icon icon-red" id="iconTalentPoolRed">.</i></a>
		
		</li>
		
		<li class="archive" onclick="tabRecommend()" >
	
		<a href="" data-toggle="tab" class="a">内推库<i class="layui-icon icon-red" id ="iconRecommendRed">.</i></a>
		
		</li>
		<i class='layui-icon icon'>&#x1002;<a href="#"  onclick="change()" class="in">换一换</a></i>
		
	</ul>
		 <!-- 添加候选人 -->
		<div id = "tabAddCandidate">
		<span id ="recuitOrderId" style="display: none;"><%=orderId %></span>
			<form id="form1">
	<div class="from">
	<div class="col-lg-4">
				<input type="hidden"  id="resumeId">
				<span>候选人姓名<samp>*</samp></span>
				<input type="text" class="form-control name" name="candidate_name" id="candidate-name" onblur="validator()" >
	</div>
	<div class="col-lg-4">
				<span>候选人电话<samp>*</samp></span>
				<input type="text" class="form-control phone" name= "candidate_phone" id="candidate-phone" onblur="validator()" >
	</div>
	
	<div class="col-lg-4">
				<span>简历性质<samp>*</samp></span>
				<select class="form-control nature" id="nature" name="nature" onchange="" onblur="validator()">
						<option></option>
						<option value="1">收到</option>
						<option value="2">搜索</option>
				</select>	
		</div>
</div>
	 <div class="from1">
	 	
		<div class="col-lg-4">
				<span>简历渠道<samp>*</samp></span>
				<select class="form-control channel" id="recruitChannel" name="recruitChannel" onblur="validator()" onchange="recruitSuppliers(this.value)">
					<option></option>
				</select>
	</div>
	
	<div class="col-lg-4">
				<span>供应商</span>
				<select class="form-control" id="recruitSupplier" >
				</select>
	</div>
	<div class="col-lg-4 ">
				<span>性别</span>
				<select class="form-control" id="sex" name="" onchange="">
						<option></option>
						<option value="1">男</option>
						<option value="2">女</option>
				</select>	
	</div>
	 
</div> 
</form>
<div class="from1">
	<div class="col-lg-4 col">
				<span>学历</span>
				<select class="form-control" id="degree" name="" onchange="">
						<option></option>
						
				</select>
	</div>
	<div class="col-lg-4 col">
				<span>邮箱</span>
				<input type="text" class="form-control email" id="mail"  name="mail"  onblur="validator()">
	</div>
	</div>
  <div class="form-group note">
 	 <div class="col-lg-6 col-note">
    	<span>备注</span>
    	<textarea class="form-control" rows="3" id="comment"></textarea>
    </div>
  
  </div>

   <div class="form-group FileUpload">
 <div id="resumes">
				<label>附件:</label><br>
				<div id="file_upload" ></div>
			</div>	
  	<!--上传附件 -->
  <div id="uploader" class="wu-example">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			           
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			            </div>
			        </div>
				</div>
  
</div> 
  
   <div class="form-group button">
   
 <button type="button" class="btn btn-warning " id="add" onclick="add()">保存并关闭</button>
 <button type="button" class="btn btn-default" id="save"  onclick="save()">保存并新增下一条</button>
   </div>
  
 
  	<div>
  	

	</div>
		</div>
		
		
		<!-- 人才库 -->
	<div id = "talentPool">
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>姓名</th>
					<th>年龄</th>
					<th>学历</th>
					<th>意向职位</th>
					<th>手机号码</th>
					<th>性别</th>
					<th class="talentPool-operation">添加到工单</th>
				</tr>
			</thead>
		  <tbody id="talentPool-data" ></tbody>
		</table>
	</div>
	

	
	<!-- 内推库 -->
  		<div id = "recommend" >
  			<table class="table table-condensed">
				<thead>
					<tr>
						<th>姓名</th>
						<th>手机号码</th>
						<th>邮箱</th>
						<th>推荐职位</th>
						<th class="recommend-operation">添加到工单</th>
					</tr>
				</thead>
		<tbody id = "recommend-data">
		
		</tbody>
		
	</table>
  </div>
  		
  </div>
	
</div>
<script src="<%= request.getContextPath() %>/process/js/candidate_share.js"></script>
<script src="<%= request.getContextPath() %>/Talent/js/resumeupload.js?v=1.0"></script>
	<script type="text/javascript">
		var href="<%= request.getContextPath() %>/Talent/resumes_select.jsp?id=";
	</script>
  
</body>
</html>