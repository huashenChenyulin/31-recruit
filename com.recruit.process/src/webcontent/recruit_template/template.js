	
	$(function(){
    		queryTemplate();//默认加载查询方法
    		
    	});

	//成功或错误提示
	var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		}); 
		
		
		//根据id查询模版
    	function queryTemplate(){
    		var id=tempId;
    		var json={
    			id:id,
    			or:or
    		};
    		$.ajax({
    			url:"com.recruit.process.recruitTemplate.queryTemplate.biz.ext",
	  			type:"POST",
	  			data:json,
	  			success:function(data){
	  				//编辑模版
	  				if(or==1){
	  				//接收到的模版放进 id为 parent 的div里面
	  					$("#parent").append(data.tempRecord[0].evaluationTemplate);
	  					//加载求和
	  					calculation(null,"1");
	  					
	  					if($(".score_1").val()==null || $(".score_1").val()==""){
	  						//人力资源部评分的失去焦点操作
				    		$(".score_1").blur(function(){
							   calculation($(this),"2");
							});
							//业务部评分的操作
							$(".score_2").blur(function(){
							   calculation($(this),"2");
							});
	  					}else{
	  						//人力资源部评分的值改变操作
				    		$(".score_1").change(function(){
							   calculation($(this),"1");
							});
							//业务部评分的操作
							$(".score_2").change(function(){
							   calculation($(this),"1");
							});
							
							//人力资源部评分
		  					$(".score_1").change(function(){
		  						//获取选中的下拉框
		  						var a=$(this).children('option:selected');
		  						//获取选中的下拉框的值
		  						var value=$(this).children('option:selected').val();
		  						//移除类名“score_1”中option里面的selected属性
		  						$(this).children().removeAttr("selected");
		  						
		  						//给选中的下拉框中的option添加selected属性
		  			    		$(a).attr("selected","selected");
		  			    		$(this).val(value);
		  			    	});
		  					//业务部评分
		  					$(".score_2").change(function(){
		  						//获取选择的下拉框
		  						var a=$(this).children('option:selected');
		  						//获取选择的下拉框的值
		  						var value=$(this).children('option:selected').val();
		  						//移除类名“score_2”中option里面的selected属性
		  						$(this).children().removeAttr("selected");
		  						
		  						//给选中的下拉框中的option添加selected属性
		  			    		$(a).attr("selected","selected");
		  			    		$(this).val(value);
		  			    	});
	  					}
						
						
						//允许操作
	  					$(".illustrate").attr("contenteditable","true");
	  					
	  					$(".score_1").attr("disabled",false);
	  					$(".score_2").attr("disabled",false);
	  					
	  					$("#totalScore").attr("contenteditable","false");
	  					$("#totalScore2").attr("contenteditable","false");
	  					
	  					
	  					//设置到待入职环节不可操作
	  					if(entry!=0){
	  						$("#main-content td").attr("contenteditable","false");
	  						$(".score_1").attr("disabled",false);
		  					$(".score_2").attr("disabled",false);
	  						$("#success").hide();
	  					}else{
	  						$("#main-content td").attr("contenteditable","true");
	  						$("#totalScore").attr("contenteditable","false");
		  					$("#totalScore2").attr("contenteditable","false");
	  						$("#success").show();
	  						$('select').parent().removeAttr("contenteditable");
	  					}
	  					
	  				//新增模版
	  				}else if(or==2){
	  					
	  				//接收到的模版放进 id为 parent 的div里面
	  					$("#parent").append(data.temp[0].template);
	  				
	  					//不允许操作
	  					$(".illustrate").attr("contenteditable","false");
	  					
	  				}
	  			}
    		});
    	}
    	
    	 var isPass=true;//标识是否通过验证
		//评分求和
    	function calculation(obj,a){
    		var text=$(obj).text();
	   		isPass=true;
	   		//验证1位正数或验证1位带0.5的小数
	   		var digit=/^[0-5]{1}$|^[0-5]{1}(.[5]{1})$/;
	   		
	   		if(a=='2'){
	   			//判断如果不成立则改变边框颜色
		   		if(!digit.test(text)){
		   			$(obj).css("border","2px solid red");
		   			isPass=false;
		   		}else{
		   			$(obj).css("border","1px solid #607D8B");
		   		}
	   		}
	   		
	   		
    		var totalScore=0;		//人力资源部总评分
    		var totalScore2=0;	//业务部总评分
    		//人力资源部求和
    		$('.score_1').each(function(){
    			if($(this).val()!=null && $(this).val()!=""){
    				totalScore+=parseInt($(this).val());
    			}else{
    				totalScore += parseFloat($(this).text());
    			}
			 });
			 //业务部求和
    		$('.score_2').each(function(){
    			if($(this).val()!=null && $(this).val()!=""){
    				totalScore2 += parseInt($(this).val());
    			}else{
    				totalScore2 += parseFloat($(this).text());
    			}
			 });
    		
    		$("#totalScore").text(totalScore);
    		$("#totalScore2").text(totalScore2);
    	}
    	
    	//判断评分后事实说明是否没填
    	function isIllustrate(){
    		if(isPass==false){
    			layer.msg('只允许一位数或一位带0.5的数');
    			return;
    		}
    		
    		var result = true;
    		//遍历事实说明
    		$(".illustrate").each(function(){
    			var illustrate =$(this).prev().children().val();
    			var text=$(this).text();
    			$(".illustrate").css("border","1px solid #607D8B");
    			//判断没评分
    			if(illustrate>=4){
    				//判断没写事实说明
    				if(text==null || text==""){
    					layer.msg('请填写事实说明');
    					$(this).css("border","2px solid red");
    					result=false;
    					return false;
    				}
    			}
    		});
    		//跳出方法
    		if(!result){
    			return;
    		}
    		//保存方法
    		
    		addTemplate();
    	}
    	
    	
    	//保存模版
    	function addTemplate(){
    		var template=$("#parent").html();		//模版内容
    		var hr_score=$("#totalScore").text();	//人力资源部总评分
    		var org_score=$("#totalScore2").text();	//业务部总评分
    		var json={
    			id:tempId,
    			or:or,
    			template_name:"面试评估模版",
    			template:template,
    			evaluation_template:template,
    			hr_score:hr_score,
    			org_score:org_score
    		};
    		$.ajax({
    			url:"com.recruit.process.recruitTemplate.addTemplate.biz.ext",
	  			type:"POST",
	  			data:json,
	  			success:function(data){
	  				if(or==1){
	  					if(data.success=="ok"){
		  					layer.msg('修改成功');
		  					//获取上一个页面的参数
		  					var candidateId=window.opener.candidateId;
		  					//获取上一个页面的方法
		  				  	window.opener.getTalentData(candidateId);
		  				}else{
		  					layer.msg('修改失败');
		  				}
	  				}else if(or==2){
	  					
		  				if(data.success=="ok"){
		  					layer.msg('保存成功');
		  				}else{
		  					layer.msg('保存失败');
		  				}
	  				}
	  			}
    		});
    	}
    	
    	//打印
    	function printPage(){
    		$("#success").hide();
    		$("#print").hide();
    		$("#wrap select").css("border","0px");
    		window.print();
    		setTimeout(function(){  //使用  setTimeout（）方法设定定时1000毫秒
				location.reload();//页面刷新
			},1000);
    	}
    	