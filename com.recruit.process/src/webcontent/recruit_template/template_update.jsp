<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-04-19 08:27:12
  - Description:
-->
<head>
<title>面试评估</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/process/recruit_template/template.css">
    <script src="<%= request.getContextPath() %>/process/recruit_template/template.js" type="text/javascript"></script>
    
    <%
    	String id=request.getParameter("id");
    	String entry=request.getParameter("entry");
    	String sign=request.getParameter("sign");
    	
    	if(sign.equals("false")){
    		id = com.eos.foundation.common.utils.CryptoUtil.decryptByDES(id,null);
    		entry = com.eos.foundation.common.utils.CryptoUtil.decryptByDES(entry,null);
    	}
    	
     %>
    
    <style type="text/css">
    	
    </style>
</head>
<body>

<div id="parent">
		
</div>
	
	<button type="button" class="btn btn-warning" id="print" onclick="printPage()">打 印</button>
	<button type="button" class="btn btn-warning" id="success" onclick="isIllustrate()">保 存</button>
					
	<script type="text/javascript">
		var tempId='<%=id %>';
		//标识区分方法
    	var or=1;
    	
    	var entry="<%=entry %>";
    </script>
    
</body>
</html>