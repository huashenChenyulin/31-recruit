//删除附件
		function resumnedelete(id,filepath,name){
			var json = {
				id:id,
				filepath:filepath
			};
			if(id==0){
				$.ajax({
					url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
					type : 'POST',
					async: false,
					data : json,
					success : function(data) {
						
					}
				});
			}else{
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,id: 'layerDemo'
			        ,content: '<div style="padding: 9px 75px 0px;">附件名：'+name+'</div><div style="text-align: center;"><br>是否确定要删除该附件？</div>'
			        ,closeBtn: false
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
			        ,closeBtn: false//不显示关闭图标
			        ,yes: function(index){
			        	$.ajax({
							url : "com.recruit.interpolation.interpolationindex.deleteResume.biz.ext",
							type : 'POST',
							async: false,
							data : json,
							success : function(data) {
								var obj="#recruitAcc"+id;
								$(obj).remove();//清除
								layer.msg('附件删除成功');
								layer.close(index);//关闭页面
							}
						});
			        }
			        ,btn2: function(){
			          layer.closeAll();//关闭页面
			        }
			  }); 
			}
		}
	   	
	   	//删除附件
	   	function delectfile(upid,url){
			 resumnedelete(0,url);//删除服务器上的附件
	            var id="#"+upid.id;
	        	$(id).remove();//清除当前id
     	}