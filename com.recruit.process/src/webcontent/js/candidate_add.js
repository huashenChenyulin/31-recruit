//初始化
	 $(function(){
	
		//查询简历渠道 (学历)
		recruitChannel();
		//禁止下拉 
		$("#recruitSupplier").attr("disabled", true); 
		
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
		
		//查询人才库
		talentPool();
		//查询内推库
		recommend();
		//标识id
		
	}); 
	
	
	 var j = 0;
	//清空获选人信息
	function empty(){
		 $("#candidate-name").val("");//候选人姓名
		 $("#candidate-phone").val("");//候选人电话
         $("#recruitChannel").val("");//简历渠道
         $("#recruitSupplier").val("");//供应商
         $("#nature").val("");//简历性质
         $("#sex").val("");//性别
		 $("#degree").val("");//学历
		 $("#mail").val("");//邮箱
		 $("#comment").val("");//备注	
		 $("#recruitSupplier").attr("disabled", true);	
		 $("#attachment").attr("href", "");
		 $("#attachment").hide();
	}
	
	//添加候选人
	function addCandidate(paramVal){
		
		var resumeId = $("#resumeId").val();//简历id
		var recuitOrderId = $("#recuitOrderId").html();//工单号
		var name = $("#candidate-name").val();//候选人姓名
		var phone = $("#candidate-phone").val();//候选人电话
		var recruitChannel = $("#recruitChannel").val();//简历渠道
		var recruitSupplier = $("#recruitSupplier").val();//供应商
		var nature = $("#nature").val();//简历性质
		var sex = $("#sex").val();//性别
		var degree = $("#degree").val();//学历
		var mail = $("#mail").val();//邮箱
		var comment = $("#comment").val();//备注
		//简历地址
		var uploadURL="";
		//简历名称
		var uploadName="";
		//简历路劲
		var uploadPath="";
		 $(".afiles").each(function(i,obj){
			 uploadURL += getCaption($(obj).attr("href"))+"^";
			 uploadName += $(obj).text()+"^";
			 uploadPath += $(obj).attr("data")+"^";
		 });
		 
		 //简历id不为空为修改方法
		 if(resumeId!=null && resumeId!=""){
			 //是否修改候选人基础信息
				isUpdateCandidate(data,resumeId,recuitOrderId,name,
		        		  phone,recruitChannel,recruitSupplier,nature,
		        		  sex,degree,mail,comment,uploadURL,uploadName,uploadPath);
		 }else{
			    var data = {recuitOrderId:recuitOrderId,name:name,phone:phone,recruitChannels:recruitChannel,
		    			recruitSuppliers:recruitSupplier,nature:nature,sex:sex,degree:degree,mail:mail,
		    			comment:comment,accessory_url:uploadURL,accessory_name:uploadName,accessory_path:uploadPath}; 
			$.ajax({
				url:'com.recruit.process.addCandidate.addCandidate.biz.ext',
				type:'post',
				dataType:'json',
				data:data,
				success: function (data) {
				
					if(paramVal=="add"){
						if(data.out=="添加成功"){
							parent.getDate("1");
							layer.msg('保存成功');
							empty();
						}
					}else{
					
						if(data.out=="添加成功"){
							parent.layer.closeAll();
							parent.layer.msg('保存成功');
						}
					}
					
					if(data.out=="添加失败"){
						 //empty();
						 layer.msg('保存失败！');
						 //清空
					}
					if(data.out=="候选人已存在"){
						 empty();
						 layer.msg('候选人已存在工单中！');
						 //清空
					}
					if(data.out =="其他工单中已有此候选人"){
						var content = "<div style='background: #f8f8f8; '>";
						content+="<table class='table table-condensed'>";
						content+="<thead class='.thead'><tr><th>招聘负责人</th><th>当前工单</th><th>当前面试环节</th><th>候选人状态</th><th>归档时间</th></tr>";
						content+=" </thead>";
						content+=" <tbody id = 'tbody-data'>";
						for(var i=0; i<data.RecruitCandidate.length;i++){
							if(i<5){
							//面试环节
					 		var recruitProcessId =Abandonmentstatus(data.RecruitCandidate[i].recruitProcessId);
							//候选人状态
							var candidateStatus = candidatestatus(data.RecruitCandidate[i].candidateStatus);
							/*recruiterName：招聘负责人 recuitOrderId：工单号 recruitProcessId：面试环节 
							  candidateStatus：候选人状态  archivedTime：归档时间*/
							content+="<tr>";
							content+="<td>"+data.RecruitCandidate[i].recruiterName+"</td>";
							content+="<td>"+data.RecruitCandidate[i].recuitOrderId+"</td>";
							content+="<td>"+recruitProcessId+"</td>";
							content+="<td>"+candidateStatus+"</td>";
							content+="<td>"+getDate(data.RecruitCandidate[i].archivedTime)+"</td>";
							content+="</tr>";
							
							}else{
								break;
							}
						}
						content+="<tr>";
						content+="<td colspan='5' style='font-size:12px;text-align:center;color:red;padding-top:20px;'>此候选人已在上述工单中，是否确认添加到我的工单？</td>";
						content+="</tr>";
						content+="</tbody>";
						content+="</table>";
						content+="</div>";
					
						
						layer.open({
					        type: 1
					        ,offset:'auto' 
					        ,area: '550px'
					        ,content: content
					        ,closeBtn: false//不显示关闭图标
					        ,btn:['确定','关闭']
					        ,btnAlign: 'c' //按钮居中
					        ,shade: 0 //不显示遮罩
					        ,yes: function(index){
					        	//是否修改候选人基础信息
					        	isUpdateCandidate(data,resumeId,recuitOrderId,name,
								        		  phone,recruitChannel,recruitSupplier,nature,
								        		  sex,degree,mail,comment,uploadURL,uploadName,uploadPath);
					        	layer.close(index);
					        }
					        ,btn2: function(index,layero){
					        	//删除附件地址
					        	removeFileUrl();
					        	//清空
					        	empty();
					        	//关闭
					        	layer.close(index);
					        }
					});
						
				}
					
					
			}
		}); 
			 
		 }
	
}
	
	//遍历删除附件方法
	function removeFileUrl(){
		
		//遍历触发点击方法
		$(".deleteUrl").each(function(i,obj){
			$(obj).find(".delete").click();
			
		});
	}
	
	//是否修改候选人基础信息
	function isUpdateCandidate(data,resumeId,recuitOrderId,name,phone,recruitChannel,recruitSupplier,nature,sex,degree,mail,comment,uploadURL,uploadName,uploadPath){
		
		var IsAdd;
		if(resumeId!=null && resumeId!=""){
			
			
			var updateData = {name:name,phone:phone,mail:mail,sex:sex,nature:nature,recruitChannels:recruitChannel,recruitSuppliers:recruitSupplier,
							  degree:degree,recuitOrderId:recuitOrderId,ResumeId:resumeId,comment:comment,IsAdd:IsAdd,
							  accessory_url:uploadURL,accessory_name:uploadName,accessory_path:uploadPath};
			updateCandidate(updateData);
			
		}else{
			$("#resumeId").val(data.RecruitResume[0].id);//简历Id
			if(data.out == "其他工单中已有此候选人"){
				data.out ="是否修改候选人基础信息";
			}
			if(data.out=="是否修改候选人基础信息"){
				 
				layer.open({
			        type: 1
			        ,offset:'auto' 
			        ,content: '<div style="padding: 18px 22px 12px 26px;">该候选人已存在简历信息,是否修改已有简历信息？</div>'
			        			+'<div style="text-align: center;">候选人姓名：'+name+'？</div>'
			        ,closeBtn: false//不显示关闭图标
			        ,btn:['确定','关闭']
			        ,btnAlign: 'c' //按钮居中
			        ,shade: 0 //不显示遮罩
			        ,yes: function(index){
			        	 IsAdd = false;
			        	
							if(data.RecruitResume.length>0){
								$("#resumeId").val(data.RecruitResume[0].id);//简历Id
				    			$("#resumeId").val(data.RecruitResume[0].id);//简历Id
				    			$("#sex").val(data.RecruitResume[0].gender);//性别
				    			$("#degree").val(data.RecruitResume[0].degree);//学历
				    			$("#mail").val(data.RecruitResume[0].candidateMail);//邮箱
				    			//判断是否存在附件
								 if(data.recruitAcc.length > 0){
									 
									 for(var i=0;i<data.recruitAcc.length;i++){
											var accessoryUrl=basePath+"/"+data.recruitAcc[i].accessoryUrl;//拼接附件路径
											$("#file_upload").append("<p id='recruitAcc"+data.recruitAcc[i].id+"' class='recruitAcc'><a href="+accessoryUrl+"  target='_blank'>"+data.recruitAcc[i].accessoryName+"</a>"
															+"&nbsp;&nbsp;&nbsp;&nbsp;<a class='delete'"
															+"onclick='resumnedelete("+data.recruitAcc[i].id+",\""+data.recruitAcc[i].accessoryPath+"\",\""+data.recruitAcc[i].accessoryName+"\")'> X </a></p>");
											}
									 
								}
									    			
							}
							layer.close(index);
			       
			        }
			        ,btn2: function(index,layero){
			        	//点击关闭
				  		IsAdd = true;
				  		var resumeid = $("#resumeId").val();
				  		var json = {name:name,phone:phone,mail:mail,sex:sex,nature:nature,
				  					recruitChannels:recruitChannel,recruitSuppliers:recruitSupplier,
				  					degree:degree,recuitOrderId:recuitOrderId,ResumeId:resumeid,comment:comment,
				  					IsAdd:IsAdd,accessory_url:uploadURL,accessory_name:uploadName,accessory_path:uploadPath};
				  		updateCandidate(json);
			        }
			});
			
		
	  }
	}	
	}
	
	//修改候选人信息
	function updateCandidate(data){
		$.ajax({
			url:'com.recruit.process.addCandidate.updateResume.biz.ext',
			type:'post',
			dataType:'json',
			data:data,
			success: function (data) {
				if(data.out=="添加成功"){
					$("#resumeId").val("");
					if(status=="close"){
						parent.layer.closeAll();
						parent.layer.msg('保存成功');
					}else{
						parent.getDate("1");
						layer.msg('保存成功');
						empty();
					}
					
					
				}else{
					layer.msg("添加失败");
				}
				
			}	
		});
	}
	
	//换一换
	//status 标识talentPool 人才库  recommend内推库
	var status;
	function change(){
		if(status=="talentPool"){
			talentPool();
		}else if(status=="recommend"){
			recommend();
		}
	}

	
	function tabAddCandidate(){
		$("#talentPool").hide();//隐藏人才库
		$("#recommend").hide();//隐藏内推库
		$("#tabAddCandidate").show();//显示添加候选人
		$(".icon").hide();
	}
	
	//点击人才库
	function tabTalentPool(){
		status = "talentPool" ;
		$("#talentPool").show();
		$("#recommend").hide();
		$("#tabAddCandidate").hide();
		$(".icon").show();
		//隐藏小红点
		$("#iconTalentPoolRed").hide();
	}
	//标识是否初始状态（是为true 否为false）
	var statusTag = true;
	//查询人才库信息
	function talentPool(){
		
		var orderId = $("#recuitOrderId").html();//工单号
		var data = {recuitOrderId:orderId};
		$.ajax({
			url : 'com.recruit.process.addCandidate.queryTalentPool.biz.ext',
			type: 'post',
			dataType: 'json',
			data:data,
			success: function (data) {
			
			$("#talentPool-data").empty();
			//数据长度要大于0
			if(data.RecruitTalentPool.length>0){
				if(statusTag == true){
					//显示小红点
					$("#iconTalentPoolRed").show();
					statusTag=false;
				}else{
					$("#iconTalentPoolRed").hide();
				}
				
			 for(var i = 0;i<data.RecruitTalentPool.length;i++){
				//当前工单号数据长度减一 获取最新的单号
				var j = (data.RecruitTalentPool[i].recruitCandidate.length)-1;
				var recuitOrderId;
				//工单号数据长度 为-1 代表没数据
				if(j>=0){
					recuitOrderId = data.RecruitTalentPool[i].recruitCandidate[j].recuitOrderId;
				}else{
					recuitOrderId ="";
				}
				var resume_id=data.RecruitTalentPool[i].resume_id;
				var age = getAge(data.RecruitTalentPool[i].birth_date);//年龄
				var degree = educationstatus(data.RecruitTalentPool[i].degree);//学历
				var gender =sexstatus(data.RecruitTalentPool[i].gender);//性别
				/*
				 *candidate_name(姓名) expected_position（意向职位） candidate_phone（电话）id（人才id） resume_url（简历地址）
				 *resume_id（简历id）candidate_mail（邮箱）recruit_channels（渠道名称类型代码）recruit_suppliers（供应商名称类型代码）
				 * */
				var tr = "<tr >";
				 	tr +="<td id='tp_name"+i+"'><a href='"+href+resume_id+"' target='_Blank' class='tp_name'>"+data.RecruitTalentPool[i].candidate_name+"</a></td>";
				 	tr +="<td id='tp_age"+i+"'>"+age+"</td>";
				 	tr +="<td>"+degree+"</td>";
				 	tr +="<td id='tp_position"+i+"'>"+data.RecruitTalentPool[i].expected_position+"</td>";
				 	tr +="<td id='tp_phone"+i+"'>"+data.RecruitTalentPool[i].candidate_phone+"</td>";
				
					tr +="<td class='hidden' id='tp_id"+i+"'>"+data.RecruitTalentPool[i].id+"</td>";
					tr +="<td class='hidden' id='resume_id"+i+"'>"+data.RecruitTalentPool[i].resume_id+"</td>";
					tr +="<td class='hidden' id='tp_mail"+i+"'>"+data.RecruitTalentPool[i].candidate_mail+"</td>";
					tr +="<td class='' id='tp_gender"+i+"'>"+gender+"</td>";
					tr +="<td class='hidden' id='tp_channels"+i+"'>"+data.RecruitTalentPool[i].recruit_channels+"</td>";
					tr +="<td class='hidden' id='tp_suppliers"+i+"'>"+data.RecruitTalentPool[i].recruit_suppliers+"</td>";
					tr +="<td class='hidden' id='tp_degree"+i+"'>"+data.RecruitTalentPool[i].degree+"</td>";
					tr +="<td class='hidden' id='tp_resume_url"+i+"'>"+data.RecruitTalentPool[i].resume_url+"</td>";
					tr +="<td id='button"+i+"'><button type='button' id='talentPoolOperation"+i+"' onclick='talentPoolOperation("+i+")' class='btn btn-default btn-operation'>+</button></td></td>";
					tr +="</tr>"
					$("#talentPool-data").append(tr);
				}
			  }
			}			
		});
	}
	
	//点击内推库
	function tabRecommend(){
		status="recommend";
		$("#talentPool").hide();
		$("#recommend").show();//隐藏内推库
		$("#tabAddCandidate").hide();//隐藏添加候选人
		$(".icon").show();
		$("#iconRecommendRed").hide()
	}
	
	//内推库
	function recommend(){
	
		var recuitOrderId = $("#recuitOrderId").html();
		var data={recuitOrderId:recuitOrderId};
		$.ajax({
			url:'com.recruit.process.addCandidate.queryTalentRecommend.biz.ext',
			type: 'post',
			dataType: 'json',
			data:data,
			success: function (data) {
			$("#recommend-data").empty();
			//数据长度要大于0
			if(data.RecruitRecommend.length>0){
				if(statusTag==true){
					$("#iconRecommendRed").show();
				}else{
					$("#iconRecommendRed").hide();
				}
				
			 for(var i = 0;i<data.RecruitRecommend.length;i++){
				
				 var candicateMail = data.RecruitRecommend[i].candicate_mail;
				 if(candicateMail==null){
					candicateMail="";
				 }
				var tr = ("<tr>"+
							"<td id='recommend_name"+i+"'>"+data.RecruitRecommend[i].candicate_name+"</td>"+
							"<td id='recommend_phone"+i+"'>"+data.RecruitRecommend[i].candicate_phone+"</td>"+
							"<td id='recommend_mail"+i+"'>"+candicateMail+"</td>"+
							"<td id='recommend_position"+i+"'>"+data.RecruitRecommend[i].recommended_position+"</td>"+
							"<td class='hidden' id='recommend_id"+i+"'>"+data.RecruitRecommend[i].id+"</td>"+
							"<td class='hidden' id='recommend_resume_url"+i+"'>"+data.RecruitRecommend[i].resume_url+"</td>"+
							"<td><button type='button' id='recommend-operation"+i+"' onclick='recommendOperation("+i+")' class='btn btn-default btn-operation'>+</button></td></td>"+
						"</tr>");
				$("#recommend-data").append(tr);
				}
			  }
			}
		});
		
	}
	
	
		//人才库操作
		function talentPoolOperation(num){
		
			var id = $("#tp_id"+num).html();
			var data = {id:id};
				$.ajax({
					url:'com.recruit.process.addCandidate.queryInOrder.biz.ext',
					type: 'post',
					dataType:'json',
					data:data,
					async:false, 
					success: function (data) {
						if(data.RecruitCandidate.length>0){
						//清空元素
						$("#tbody-data").empty();
						var content = "<div style='background: #f8f8f8; '>";
							content+="<table class='table table-condensed'>";
							content+="<thead class='.thead'><tr><th>招聘负责人</th><th>当前工单</th><th>当前面试环节</th><th>候选人状态</th><th>归档时间</th></tr>";
							content+=" </thead>";
							content+=" <tbody id = 'tbody-data'>";
							for(var i=0; i<data.RecruitCandidate.length;i++){
								if(i<5){
								//面试环节
						 		var recruitProcessId =Abandonmentstatus(data.RecruitCandidate[i].recruitProcessId);
								//候选人状态
								var candidateStatus = candidatestatus(data.RecruitCandidate[i].candidateStatus);
								/*recruiterName：招聘负责人 recuitOrderId：工单号 recruitProcessId：面试环节 
								  candidateStatus：候选人状态  archivedTime:归档时间*/
								content+="<tr>";
								content+="<td>"+data.RecruitCandidate[i].recruiterName+"</td>";
								content+="<td>"+data.RecruitCandidate[i].recuitOrderId+"</td>";
								content+="<td>"+recruitProcessId+"</td>";
								content+="<td>"+candidateStatus+"</td>";
								content+="<td>"+getDate(data.RecruitCandidate[i].archivedTime)+"</td>";
								content+="</tr>";
								}else{
									break;
								}
							}
							content+="<tr>";
							content+="<td colspan='5' style='font-size:12px;text-align:center;color:red;padding-top:20px;'>此候选人已在上述工单中，是否确认添加到我的工单？</td>";
							content+="</tr>";
							content+="</tbody>";
							content+="</table>";
							content+="</div>";
						
						layer.open({
							   type: 1
							  ,title: "已添加的工单" //不显示标题栏
							  ,closeBtn: false
							  ,area: '550px'
							  ,shade: 0
							  ,offset:'auto' 
							  ,scrollbar: false
							  ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
							  ,btn: ['确认','关闭']
							  ,btnAlign: 'c'
							  ,moveType: 1 //拖拽模式，0或者1
							  ,content: content
							
							  ,yes: function(index){
								  //人才库添加为候选人
								  talentPoolAddcandidate(num);
								  layer.close(index);//关闭页面
							  }
							});
					
						
				}else{
			    	
					 talentPoolAddcandidate(num);
			    }		
			}
		});		
}
		
		
		//人才库添加为候选人
		function talentPoolAddcandidate(num){
			  var name = $("#tp_name"+num).html();//名称
			  var phone = $("#tp_phone"+num).html();//电话
			  var mail = $("#tp_mail"+num).html();//邮箱
			  var sex = $("#tp_gender"+num).html();//性别
			  var channels = $("#tp_channels"+num).html();//渠道编码
			  var suppliers = $("#tp_suppliers"+num).html();//供应商编码
			  var degree = $("#tp_degree"+num).html();//学历编码
			  var recuitOrderId = $("#recuitOrderId").html();//工单号
			  var tp_id = $("#tp_id"+num).html();//人才库Id
			  var resume_id = $("#resume_id"+num).html();//简历Id
			  var resume_url = $("#tp_resume_url"+num).html();//简历地址
			  var data ={name:name,phone:phone,mail:mail,sex:sex,recruitChannels:channels,recruitSuppliers:suppliers,
					  degree:degree,recuitOrderId:recuitOrderId,ResumeId:resume_id,talentId:tp_id,resumeUrl:resume_url
			  };
			  
			  $.ajax({
				  url:'com.recruit.process.addCandidate.addCandidate.biz.ext',
				  type: 'post',
				  dataType: 'json',
				  data:data,
				  success: function (data) {
					  
					  $("#talentPoolOperation"+num).css("background-color","#C0C0C0");
					  $("#talentPoolOperation"+num).attr("onclick","");
				  }
			  });
		}
		
		
		//内推操作
		function recommendOperation(num){
			//内推库id
			var interpolateId = $("#recommend_id"+num).html();
			
			var data = {interpolateId:interpolateId};
				$.ajax({
					url:'com.recruit.process.addCandidate.queryInOrder.biz.ext',
					type: 'post',
					dataType:'json',
					data:data,
					success: function (data) {
						if(data.RecruitCandidate.length>0){
							//清空元素
							$("#tbody-data").empty();
							var content = "<div style='background: #f8f8f8; '>";
								content+="<table class='table table-condensed'>";
								content+="<thead class='.thead'><tr><th>招聘负责人</th><th>当前工单</th><th>当前面试环节</th><th>候选人状态</th><th>归档时间</th></tr>";
								content+=" </thead>";
								content+=" <tbody id = 'tbody-data'>";
								for(var i=0; i<data.RecruitCandidate.length;i++){
									if(i<5){
									//面试环节
							 		var recruitProcessId =Abandonmentstatus(data.RecruitCandidate[i].recruitProcessId);
									//候选人状态
									var candidateStatus = candidatestatus(data.RecruitCandidate[i].candidateStatus);
									/*recruiterName：招聘负责人 recuitOrderId：工单号 recruitProcessId：面试环节 
									  candidateStatus：候选人状态*/
									content+="<tr>";
									content+="<td>"+data.RecruitCandidate[i].recruiterName+"</td>";
									content+="<td>"+data.RecruitCandidate[i].recuitOrderId+"</td>";
									content+="<td>"+recruitProcessId+"</td>";
									content+="<td>"+candidateStatus+"</td>";
									content+="<td>"+getDate(data.RecruitCandidate[i].archivedTime)+"</td>";
									content+="</tr>";
									
									}else{
										break;
									}
								}
								content+="<tr>";
								content+="<td colspan='5' style='font-size:12px;text-align:center;color:red;padding-top:20px;'>此候选人已在上述工单中，是否确认添加到我的工单？</td>";
								content+="</tr>";
								content+="</tbody>";
								content+="</table>";
								content+="</div>";
							
							layer.open({
								   type: 1
								  ,title: "已添加的工单" //不显示标题栏
								  ,closeBtn: false
								  ,area: '550px'
								  ,shade: 0
								  ,scrollbar: false
								  ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
								  ,btn: ['确认','关闭']
								  ,btnAlign: 'c'
								  ,moveType: 1 //拖拽模式，0或者1
								  ,content: content
								  ,offset:'auto' 
								  ,yes: function(index){
									  //人才库添加为候选人
									  recommendAddcandidate(num);
									  layer.close(index);//关闭页面
								  }
								});
							
					}else{
						recommendAddcandidate(num);
				    }		
				}
				
			});	
		}
		//内推库添加候选人
		function recommendAddcandidate(num){
			var channels=5;//渠道编码
			var suppliers = 28;//供应商编码
			var name = $("#recommend_name"+num).html();//名称
			var phone = $("#recommend_phone"+num).html();//电话
			var mail = $("#recommend_mail"+num).html();//邮箱
			var recuitOrderId = $("#recuitOrderId").html();//工单
			var id = $("#recommend_id"+num).html();//内推id
			var resume_url = $("#recommend_resume_url"+num).html();//简历地址
			var data = {name:name,phone:phone,mail:mail,resumeUrl:resume_url,
						recuitOrderId:recuitOrderId,interpolateId:id,recruitChannels:channels,recruitSuppliers:suppliers
					   };
			$.ajax({
				url:'com.recruit.process.addCandidate.addCandidate.biz.ext',
				type:'post',
				data:data,
				dataType:'json',
				success: function (data) {
					
					$("#recommend-operation"+num).css("background-color","#C0C0C0");
					$("#recommend-operation"+num).attr("onclick","");
				}
			});
		}
    
		//简历渠道动态获取  (动态获取学历)
		function recruitChannel(){
			$.ajax({
				url:'com.recruit.process.addCandidate.queryRecruitProviderl.biz.ext',
				type: 'post',
				dataType: 'json',
				success: function (data) {
					for(var i= 0; i<data.recruitChannel.length;i++){
					 	var option = ("<option value='"+data.recruitChannel[i].dictID+"'>"+data.recruitChannel[i].dictName+"</option>");
						$("#recruitChannel").append(option); 
					}
					//动态给学历赋值
					for(var j = 0;j<data.sfyCofceGree.length;j++){
						var option = ("<option value='"+data.sfyCofceGree[j].dictID+"'>"+data.sfyCofceGree[j].dictName+"</option>");
						$("#degree").append(option); 
					}
				}
			});
		}
		//根据简历渠道id获取供应商
		function recruitSuppliers(id){
			var channel = $("#recruitChannel").val();
			if(channel!=null && channel!=""){
				$("#recruitSupplier").attr("disabled", false);
			}else{
				 $("#recruitSupplier").attr("disabled", true);
			}
			
			$.ajax({
				url:'com.recruit.process.addCandidate.queryRecruitChannel.biz.ext',
				type: 'post',
				data:{ChannelID:id},
				dataType: 'json',
				success: function (data) {
					$("#recruitSupplier")[0].length=0;
					for(var i =0;i<data.recruitProvider.length;i++){
						 var option = ("<option value='"+data.recruitProvider[i].dictid+"'>"+data.recruitProvider[i].dictname+"</option>");
						 $("#recruitSupplier").append(option);  
					}
				}
			});
		}
		
		 var isPass=true;//标识是否通过验证
		 var isPassMsg="";//非空验证提示输出
		 var arr=[];
	   	//失去焦点验证
	   	function validator(){
	   		isPassMsg="";
	   		isPass=true;
	   		arr=[];
	   		//姓名
	  		 	if($('.name').val()==null || $('.name').val()=="" ){
	           	$(".name").css("border","1px solid red");
	           	isPassMsg+="请输入姓名,";
	           	isPass=false;
	           }else{
	           	$(".name").css("border","1px solid #ccc");
	           }
	   		
	   		//电话
				var phone=/^1\d{10}$/;//电话正则
	  		 	if($('.phone').val()==null || $('.phone').val()=="" || !phone.test($(".phone").val())){
	           	$(".phone").css("border","1px solid red");
	           	isPassMsg+="请输入正确的手机号码,";
	           	isPass=false;
	           }else{
	           	$(".phone").css("border","1px solid #ccc");
	           }
	  		 	
	  		 	
	           //简历性质
	  		 	if($('.nature').val()==null || $('.nature').val()=="" ){
	           	$(".nature").css("border","1px solid red");
	           	isPassMsg+="请选择简历性质,";
	           	isPass=false;
	           }else{
	           	$(".nature").css("border","1px solid #ccc");
	           }
	           
	           //渠道
	  		 	if($('.channel').val()==null || $('.channel').val()==""){
	           	$(".channel").css("border","1px solid red");
	           	isPassMsg+="请选择渠道,";
	           	isPass=false;
	           }else{
	           	$(".channel").css("border","1px solid #ccc");
	           }
	  		 	
	  		//邮箱
				var email=/^([a-zA-Z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;//邮箱正则
	  		 	if($('.email').val()!=null && $('.email').val()!="" && !email.test($(".email").val())){
	           	$(".email").css("border","1px solid red");
	           	isPassMsg+="请输入正确的邮箱格式-如：XXX@qq.com";
	           	isPass=false;
	           }else{
	           	$(".email").css("border","1px solid #ccc");
	           }
	  		 	
	  		 	arr=isPassMsg.split(",");
	   	} 
		
	//保存并关闭
  function  add(){
	   validator();
		//非空判断
		if(isPass==false){
			layer.msg(arr[0]);
			return;
		}
	status="close";
  	addCandidate(status);//验证成功后的操作，如ajax  
  }
  
  //保存并新增下一条	
  function save(){
	  validator();
		//非空判断
		if(isPass==false){
			layer.msg(isPassMsg);
			return;
		}
	status = "add";
  	addCandidate(status);
  } 
    		
	 
