<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-04-04 14:53:19
  - Description:
-->
<%@include file="/common/common.jsp" %>
<% 
	String linkId = request.getParameter("linkId");
	
	 %>
<head>
<title>放弃原因</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    
</head>
<body style="padding: 20px;">
	<div id="">
		<div class="">
			<span id="" style="color:#606060;">放弃原因:</span>
			<select name="" id="backOut" style="margin-left: 67px;width: 64%;">
				
			</select>
		</div>
		
		<div style="margin: 10px 0px;">
			<span id="" style="float: left;color:#606060;">具体放弃情况描述:</span>
			<textarea class="form-control out_interview_text" style="width: 64%;margin-left: 15px;float: left;" placeholder="" rows="5" style="margin-top:5px"></textarea>
		</div>
		<p id="ps" style="display: none;color: red;float: left;margin-top: 20px;">此环节归档候选人将结束当前工单并自动创建新工单！</p>
	</div>


	<script type="text/javascript">
		var id="<%=linkId %>"; //环节
    	$(function(){
    	if(id=="5"){
    	 $("#ps").show();//环节等于5 出现提升句子
    	}else{$("#ps").hide();}
		$.ajax({
		  			url:"com.recruit.process.process.queryCause.biz.ext",
		  			type:"POST",
		  			datatype: "json",
		  			data:{},
		  			success:function(data){
		  				$(data.recruitReason).each(function(i,obj){
		  					if(i>0){
		  						$("#backOut").append("<option value='"+obj.dictName+"'>"+obj.dictName+"</option>");
		  					}
		  					
		  				})
		  				
		  			}
		  			});
	}); 
    </script>
</body>
</html>