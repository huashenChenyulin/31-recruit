<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): Administrator
  - Date: 2018-03-21 20:47:57
  - Description:
-->
<%@include file="/common/common.jsp" %>
<% 
	String position = request.getParameter("position");
	String order = request.getParameter("order");
	String orderPage = request.getParameter("orderPage");
	String state = request.getParameter("state");//当前任务查询状态是否点击查询按钮
	String selPage = request.getParameter("selPage");//当前任务分页
	String candidate_name = request.getParameter("candidate_name");//当前任务候选人姓名
	String recuit_order_id = request.getParameter("recuit_order_id");//当前任务工单号
	String startTime = request.getParameter("startTime");//当前任务开始时间
	String endTime = request.getParameter("endTime");//当前任务结束时间
	
	 %>
<head>
<title>process_home</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
   
    
</head>
<style type="text/css">
        .process_content{
        	margin:auto;
        	border-style: solid;
        	border-style: solid;
			border-color: #E3E3E3;
			border-width: 2px;
			width: 98%;
			float: left;
			border-radius: 5px;
			margin-top: 15px;
			padding: 5px 25px 25px;
			min-height: 550px;
			background-color: white;
        	}
        .content_top_current{
        	float: left;
        	width: 50%;
        	margin:auto;
        }
        .content_top_current_gd{
        	float: left;
        	width: 50%;
        	margin:auto;
        	padding: 0% 10%;
        }	
        .current{ 
        	font-size: 28px;
			color: #FFCC66;
			float: left;}	
        .current_value{
        	font-size: 22px;
        	float: left;
        	line-height: 40px;
        }
        .current_gd{ 
        	font-size: 28px;
			color: #FFCC66;
			float: left;}	
        .current_value_gd{
        	font-size: 18px;
        	float: left;
        }
        .content_middle{
        	height: 125px;
        	border-style: solid;
			border-color: #E3E3E3;
			border-width: 1px;
			float: left;
			width: 100%;
        }
        .middle{
        	width: 20%;
        	float: left;
			height: 100%;
			border-right-style: solid;
			border-right-color: #E3E3E3;
			border-width: 1px;
			box-sizing: border-box;
			cursor: pointer;
        }
        .middle:hover{
        	background-color:#DBEDFF;
			cursor:pointer;
        }
        .middle_v2{
        	width: 20%;
        	float: left;
        	background: #F3F3F3;
			height: 100%;
			border-right-style: solid;
			border-right-color: #E3E3E3;
			border-width: 1px;
			box-sizing: border-box;
			-moz-box-shadow:2px 2px 4px #999 inset;               /* For Firefox3.6+ */
			-webkit-box-shadow:2px 2px 4px #999 inset;            /* For Chrome5+, Safari5+ */
			box-shadow:2px 2px 4px #999 inset;                    /* For Latest Opera */
        }
        .middle_numble{
        	
        	float: left;
			font-family: 'Arial Normal', 'Arial';
			font-weight: 400;
			font-style: normal;
			font-size: 40px;
			text-align: center;
			width: 100%;
			margin: 15px 0px;
        }
        .middle_link{
        	
        	font-family: 'Arial Normal', 'Arial';
			font-weight: 400;
			font-style: normal;
			font-size: 18px;
			text-align: center;
			width: 100%;
			margin: 5px 0px;
			float: left;
        }
        
		#addCandidate{
			background-color:#ffcc66;
			width: 92px;
			height: 28px;
			float: right;
			color: white;
			font-weight: 700;
			padding: 6px;
			border-radius: 5px;
		}
		#addCandidate:hover{
			background-color:#f0ad4e;
			cursor:pointer;
		}
		tr.change:hover
			{
			background-color:#DBEDFF;
			cursor:pointer;
			}	
		table,.panel-title{
			color:#666666;
		}
		.table > tbody > tr.active > td, .table > tbody > tr.active > th, .table > tbody > tr > td.active, .table > tbody > tr > th.active, .table > tfoot > tr.active > td, .table > tfoot > tr.active > th, .table > tfoot > tr > td.active, .table > tfoot > tr > th.active, .table > thead > tr.active > td, .table > thead > tr.active > th, .table > thead > tr > td.active, .table > thead > tr > th.active {
		    background-color: #e7e7e7;
		}
		/*模态层*/
		.talent-detail-modal{
			position: fixed;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			background-color: transparent;
			display: none;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			justify-content: center;
			
		}
		.talent-detail-mask-view{
			
			position: absolute;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			background-color: rgba(51,51,51,.75);
		}
		.talent-detail-cont-wrapper-cont{
			min-width: 1040px;
			max-width: 1480px;
			width: 1189px;
			height: 100%;
			background-color: transparent;
			position: relative;
			margin-top: 2%;
			
			float: right;
		}
		#left-area{
			-webkit-box-flex: 1;
			-ms-flex-positive: 1;
			flex-grow: 1;
			width: 789px;
			min-width: 640px;
			height: 94%;
			overflow: auto;
			background-color: #f9f9f9;
			box-sizing: border-box;
			float: left;
			padding: 30px;
					}
		#right-area{
			width: 400px;
			min-width: 400px;
			height: 94%;
			overflow: auto;
			background-color: #f3f3f3;
			-webkit-box-shadow: inset 1px 0 0 0 #eaeaea;
			box-shadow: inset 1px 0 0 0 #eaeaea;
			box-sizing: border-box;
			float: right;
		}
		.name-cont{
		    line-height: 30px;
		    height: 30px;
		    display: -webkit-box;
		    display: -ms-flexbox;
		    display: flex;
		    max-width: 60%;
		}
		.info-all{
			margin: 6px 0px;
			float: left;
			width: 100%;
		}
		.interviewSituation{
			margin-top: 30px;
			display: block;
			background-color: #F3F3F3;
			padding: 15px 5px;
			border-radius: 4px;
		}
		#btn-cont{
			
		}
		
		.back_to_up{  
        	width: 35px;
			height: 35px;
			border-radius: 0px 4px 4px 0px;
			cursor: pointer;
			color: #a8a8a8;
			float: left;
    	}
    	.panel {
		    margin-bottom: 15px;
		} 			
        /*转移候选人按钮样式*/
        #qianyiButton{
        	float: right;
        	height: 28px;
        	border-radius: 5px;
        	margin-left: 1%;
        }    
        </style>
	<body style="background-color: #f9f9f9;">
		<div class="main" style="margin-left: 2%;min-height: 560px;"> 
			<div class="process_content">
				<div class="back_to_up" onclick="goToSelectOrder()">
						<i class="layui-icon" style="font-size: 35px;">&#xe65c;</i> 
				</div>
				<div class="content_top" style="float: left;width: 100%;margin-bottom: 15px;">
					<div class="content_top_current">
						<div class="current">当前招聘职位：</div><div class="current_value"></div>
					</div>
					<div class="content_top_current_gd">
							<div class="current_gd">当前工单：</div>
							<div class="current_value_gd  layui-input-inline">
							<select name="" id="orderSelect" onchange="return dataShow()" style="height: 38px;border: solid 1px #e3e3e3;">
							</select>
							</div>
					</div>
					
					
				</div>
				<!---->
				<!-- <div class="content_data" style="margin: 15px 10px;float: left;width: 100%;">
					<div style="width: 25%;float: left;"><span>招聘负责人：</span><span>陈玉麟</span></div>
					<div style="width: 25%;float: left;"><span>出单日期：</span><span>陈玉麟</span></div>
					<div style="width: 25%;float: left;"><span>剩余招聘周期：</span><span>陈玉麟</span></div>
					<div style="width: 25%;float: left;"><span>部门：</span><span>陈玉麟</span></div>
				</div> -->
				
				
				
				
				<div class="content_middle">
					<div class="middle middle_v2" style="" onclick="clickmiddle(this,'1')">
						<div class="middle_numble middle_num1">0</div><div class="middle_link">初筛</div>
					</div>
					<div class="middle" onclick="clickmiddle(this,'2')">
						<div class="middle_numble middle_num2">0</div><div class="middle_link">电话沟通</div>
					</div>
					<div class="middle" onclick="clickmiddle(this,'3')">
						<div class="middle_numble middle_num3">0</div><div class="middle_link">人力面试</div>
					</div>
					<div class="middle" onclick="clickmiddle(this,'4')">
						<div class="middle_numble middle_num4">0</div><div class="middle_link">部门面试</div>
					</div>
					<div class="middle" style="border-width: 0px;" onclick="clickmiddle(this,'5')">
						<div class="middle_numble middle_num5">0</div><div class="middle_link">待入职</div>
					</div>
					
					
				</div>
				
				
				<!---->
				<div class="content_footer" style="float: left;width: 100%;margin: 20px 0px;">
					
					
				</div>
				
			</div>
		</div>
		
		
		
	</body>
	<script type="text/javascript">
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});
		
		
		layui.use('element', function(){
		  var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
		  
		  //监听导航点击
		  element.on('nav(demo)', function(elem){
		   
		    layer.msg(elem.text());
		  });
		});
		
		
		
		var nowPosition="<%=position %>"; //当前职位
		var nowOfficeId="<%=order %>"; //当前工单号
		var Diction;
		/*流程首页js*/
		$(function () {
			 getDiction();//获取渠供应商道信息
			 
			 countProcess(nowPosition,nowOfficeId);
			 getOrder();
			 getDate("1");
			 
		 });
		/* 加载当前职位的所有工单（下拉） */
		function getOrder(){
			$.ajax({
  			url:"com.recruit.process.process.queryworkOrder.biz.ext",
  			type:"POST",
  			data:{
  				position:nowPosition,
  				
  			},
  			success:function(data){
  				var html="";
  				html+='<option value="">全部</option>';
  				$(data.officeId).each(function(index,obj){
  							
  						if(obj.recuitOrderId==nowOfficeId){
  							html+='<option selected value="'+obj.recuitOrderId+'" >'+obj.recuitOrderId+'</option>';
  						}else{
  							html+='<option value="'+obj.recuitOrderId+'" >'+obj.recuitOrderId+'</option>';
  						}
  						
  					})
  				$("#orderSelect").append(html);	
  				}
  			
  			});
		}
		//查询渠道
		function getDiction(){
			$.ajax({
  			url:"com.recruit.talent.talentpoolselect.selectDiction.biz.ext",
  			type:"POST",
  			data:{
  			},
  			success:function(data){
  				Diction=data;
  			}
  			});
		}
		//翻译渠道
		function getChannels(id){
			var channels="";
			for(var k = 0; k < Diction.rerecruitchannel.length; k++){
			if(Diction.rerecruitchannel[k].dictID==id){
					channels = Diction.rerecruitchannel[k].dictName;
					}
			}
			return channels;
			
		
		}
		//翻译供应商
		function getSuppliers(id){
			var Suppliers="";
			for(var k = 0; k < Diction.rerecruitprovider.length; k++){
			if(Diction.rerecruitprovider[k].dictID==id){
					Suppliers = Diction.rerecruitprovider[k].dictName;
					}
			}
			return Suppliers;
			
		
		}
		
		/* 加载当前环节工单信息 */
		var current="";
		function getDate(num){
		$(".content_footer").text("");
		var Link=num;
  		$.ajax({
  			url:"com.recruit.process.process.queryProcessFlew.biz.ext",
  			type:"POST",
  			data:{
  				position:nowPosition,
  				officeId:nowOfficeId,
  				nowLink:Link
  			},
  			success:function(data){
  				if(data.Positions.recruitOddNumbers.length==0){
  					var html='<div style="text-align:center;">工单已被取消</div>';
  					$(".content_footer").append(html);
  					return;
  				}
  				$(".current_value").text(data.Positions.position);
  				current=encodeURIComponent(data.Positions.position);
  				var object=data.Positions.recruitOddNumbers;
  					$(object).each(function(index,obj){
						    
						    /* 各环节的html拼接   link数为当前环节 */
						    var thead='<div class="workOrder layui-anim layui-anim-upbit">';
						    	thead+='<div class="panel panel-default">';
						    	thead+='<div class="panel-heading" style="padding: 10px 15px 15px 15px;">';
						    	if(Link=="1"||Link==""){
						    	thead+='<h3 class="panel-title"><button type="button"  class="btn btn-default tip tip-left" id="qianyiButton" data-tip="候选人转移" style="" onclick="openBatchTransfer(\''+obj.recuitOrderId+'\',\''+data.Positions.position+'\')"><span class="glyphicon glyphicon-user""></span><span class="glyphicon glyphicon-transfer"></span></button>';
						    		thead+='单号：'+obj.recuitOrderId+'<span></span><div id="addCandidate" style="" onclick="addCandidate(\''+obj.recuitOrderId+'\')">添加候选人</div></h3>';
						    		
						    	}else if(Link=="2"||Link=="3"||Link=="4"){
						    		thead+='<h3 class="panel-title"><button type="button" class="btn btn-default tip tip-left" id="qianyiButton" data-tip="候选人转移" style="" onclick="openBatchTransfer(\''+obj.recuitOrderId+'\',\''+data.Positions.position+'\')"><span class="glyphicon glyphicon-user""></span><span class="glyphicon glyphicon-transfer"></span></button>';
						    		thead+='单号：'+obj.recuitOrderId+'<span></span></h3>';
						    	}else{
						    		thead+='<h3 class="panel-title">单号：'+obj.recuitOrderId+'<span></span></h3>';
						    	}
						    	
								thead+='</div>';
								thead+='<table class="table">';
								if(Link=="1"||Link==""){
								thead+='<thead><tr><th>姓名</th><th>渠道 </th><th>供应商</th><th>性别 </th><th>手机号码</th><th>备注 </th></tr></thead>';
								}else if(Link=="2"){
								thead+='<thead><tr><th>姓名</th><th>年龄</th><th>电话</th><th>渠道 </th><th>供应商</th><th>学历</th><th>性别 </th><th>备注 </th></tr></thead>';
								}else if(Link=="3"){
								thead+='<thead><tr><th>姓名</th><th>年龄</th><th>学历</th><th>电话</th><th>计划人资面试时间</th><th>备注 </th></tr></thead>';
								}else if(Link=="4"){
								thead+='<thead><tr><th>姓名</th><th>年龄</th><th>学历</th><th>电话</th><th>计划部门面试时间</th><th>备注 </th></tr></thead>';
								}else if(Link=="5"){
								thead+='<thead><tr><th>姓名</th><th>年龄</th><th>学历</th><th>意向职位</th><th>电话</th><th>计划入职时间</th><th>备注 </th></tr></thead>';
								}
								thead+='<tbody>';
									$(obj.recruitCandidate).each(function(i,rcdate){
									
									/* candidateStatus为3表示已经归档候选人 */
									if(Link=="1"||Link==""){
										
										if(rcdate.candidateStatus=="3"){
											thead+='<tr class="change active" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getChannels(rcdate.recruitChannels)+'</td><td>'+getSuppliers(rcdate.recruitSuppliers)+'</td><td>'+sexstatus(rcdate.recruitResume.gender)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}else{
											thead+='<tr class="change" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getChannels(rcdate.recruitChannels)+'</td><td>'+getSuppliers(rcdate.recruitSuppliers)+'</td><td>'+sexstatus(rcdate.recruitResume.gender)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}
										
									}else if(Link=="2"){
										if(rcdate.candidateStatus=="3"){
											thead+='<tr class="change active" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+getChannels(rcdate.recruitChannels)+'</td><td>'+getSuppliers(rcdate.recruitSuppliers)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+sexstatus(rcdate.recruitResume.gender)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}else{
											thead+='<tr class="change" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+getChannels(rcdate.recruitChannels)+'</td><td>'+getSuppliers(rcdate.recruitSuppliers)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+sexstatus(rcdate.recruitResume.gender)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}
										
									}else if(Link=="3"){
										if(rcdate.candidateStatus=="3"){
											thead+='<tr class="change active" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.interviewTime)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}else{
											thead+='<tr class="change" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.interviewTime)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}
										
									}else if(Link=="4"){
										if(rcdate.candidateStatus=="3"){
											thead+='<tr class="change active" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.sectionTime)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
											
										}else{
											if(rcdate.offerStatus=="1"){
											thead+='<tr class="change warning" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.sectionTime)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
											}else{
											thead+='<tr class="change" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.sectionTime)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
											}
											
										}
										
									}else if(Link=="5"){
										if(rcdate.candidateStatus=="3"){
											thead+='<tr class="change active" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+isEmpty(rcdate.recruitResume.expectedPosition)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.plannedHireDate)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}else{
											thead+='<tr class="change" onclick="callParent('+rcdate.id+')"><td>'+rcdate.recruitResume.candidateName+'</td><td>'+getAge(rcdate.recruitResume.birthDate)+'</td><td>'+educationstatus(rcdate.recruitResume.degree)+'</td><td>'+isEmpty(rcdate.recruitResume.expectedPosition)+'</td><td>'+rcdate.recruitResume.candidatePhone+'</td><td>'+isEmpty(rcdate.plannedHireDate)+'</td><td>'+isEmpty(rcdate.comment)+'</td></tr>';
										}
										
									}
									
									});
								thead+='</tbody></table></div></div>';
								
							$(".content_footer").append(thead);
						 });
  				
  				
  			}
  		});
  	} 
		/* 切换环节 */
		function clickmiddle(obj,num){
			
			$(".middle").removeClass("middle_v2");
			$(obj).addClass("middle_v2");
			getDate(num);
			
		}
		
		
		/* 统计候选人 */
		
		function countProcess(position,office){
				/* 加载动画 */
				$(".middle_num1").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num2").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num3").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num4").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
  				$(".middle_num5").html('<i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 28px;">&#xe63d;</i>');
			$.ajax({
  			url:"com.recruit.process.process.countProcess.biz.ext",
  			type:"POST",
  			data:{
  				position:position,
  				officeId:office,
  			},
  			success:function(data){
  				/* 赋值各环节的统计数 */
  				$(".middle_num1").text(data.Positions.count1);
  				$(".middle_num2").text(data.Positions.count2);
  				$(".middle_num3").text(data.Positions.count3);
  				$(".middle_num4").text(data.Positions.count4);
  				$(".middle_num5").text(data.Positions.count5);
  			}
  			});
		}
		
		/* 弹出添加候选人 */
		function addCandidate(orderId){
			layer.open({
				type: 2,
				title: '添加候选人'+nowOfficeId,
				content: '<%= request.getContextPath() %>/process/candidate_add.jsp?orderId='+orderId,
				area: ['1212px', '500px'],
				cancel: function(index, layero){ 
				   var iframeWin = window[layero.find('iframe')[0]['name']]; 
				  iframeWin.removeFileUrl();
				},    
				end: function(layero, index){ 
				  
				  
				  getDate("1");
			 	  countProcess(nowPosition,nowOfficeId);
				  
				}    
				
				
			});
		}
		
		function dataShow(){
			//下拉切换
			nowOfficeId=$("#orderSelect").val();//全局变量变更
			$(".middle").removeClass("middle_v2");
			$(".middle_num1").parent().addClass("middle_v2");
			getDate("1");
			countProcess(nowPosition,nowOfficeId); 
		}
		
		/* 调取父层 弹出招聘流程操作模态层 */
		function callParent(num) {
			parent.getTalentData(num);
		}
		
		//供父层调取的方法
		function childSay(id){
			var className=".middle_num"+id;
			$(".middle").removeClass("middle_v2");
			$(className).parent().addClass("middle_v2");
			getDate(id);
			countProcess(nowPosition,nowOfficeId);
		}
		
		function goToSelectOrder(){
			var frameUrl="/myTask/task_select.jsp?current=" + current +"&state="+<%=state %>+"&selPage="+'<%=selPage %>'
			+"&candidate_name="+'<%=candidate_name %>'+"&recuit_order_id="+'<%=recuit_order_id %>'
			+"&startTime="+'<%=startTime %>'+"&endTime="+'<%=endTime %>';
			parent.goToUrl(frameUrl);
		}
		var isBatch = "yes";//用来区分是否批量转移页面
		//打开批量转移候选人页面
		var orderID;//工单号
		var talentPosition;//当前招聘职位
		function openBatchTransfer(orderId,currPosition){
			//工单号
			orderID=orderId;
			//职位
			talentPosition=currPosition;
						
			layer.open({
				type: 2,
				shade: 0.3,//不显示遮罩
				title:'转移候选人',
				resize:false, //是否允许拉伸
				btn: ['确定', '关闭'],
				content: '<%= request.getContextPath() %>/home/candidate_batchTransfer.jsp',
				area: ['95%', '90%'],
				yes: function(index,layero){
					var iframeWin = window[layero.find('iframe')[0]['name']];	//得到iframe页的窗口对象，执行iframe页的方法：
					//获取子页面的候选人id
					var candidateId = iframeWin.getCandidateId();
					//获取子页面的候选人姓名
					var candidateName = iframeWin.getCandidateName();
					//获取子页面的工单号
					var orderId =iframeWin.resultJson();
					if(orderId!=""){
						var json = $.parseJSON(orderId);
					 	orderId = json.recuitOrderId;
					}
					//候选人id和工单号为空时
					if(candidateId=="" && orderId ==""){
						layer.msg("请选择候选人和工单");
					//候选人id为空时
					}else if(candidateId==""){
						layer.msg("请选择候选人");
					//工单号为空时
					}else if(orderId==""){
						layer.msg("请选择工单");
					}else{
						layer.open({
								type: 1
								,offset:'auto'
								,id:'layerDemo'
								,content: '<div style="text-align: center;padding: 20px 64px 0px;">是否将选中的候选人</div>'
										+'<div style="text-align:center;padding: 12px 40px 0px 40px;">转移到    '+orderId+'  工单中？</div>'
								,closeBtn: false
								,resize:false //是否允许拉伸
								,btn:['确定','关闭']
								,btnAlign: 'c' //按钮居中
								,shade: 0 //不显示遮罩
								,closeBtn: false//不显示关闭图标
								,yes: function(index,layero){
								 	 layer.close(index);//关闭页面
									 //加载条
							         layer.load(1); 
							         //调用批量转移候选人方法
							         batchUpdateCandidate(candidateId,orderId,candidateName);	
								}
								,btn2: function(index,layero){
								    layer.close(index);//关闭页面
								}
							});
					
						
					}
				}
				,btn2: function(index,layero){
					//关闭页面
					layer.close(index);
				}
			});
		}
	
		//批量转移候选人  参数candidateId(候选人id) orderId（工单号）
		function batchUpdateCandidate(candidateId,orderId,candidateName){
			var json = {"candidateId":candidateId,"orderId":orderId,"candidateName":candidateName}
			$.ajax({
				url:'com.recruit.home.updateCandidate.batchUpdateCandidate.biz.ext',
				dataType:'json',
				type:'post',
				data:json,
				success:function(data){
					//转移成功
					if(data.MSG_TYPE!="E"){
					   //重新加载流程页
						childSay(1);
						layer.closeAll();
						layer.msg("转移成功");
						layer.close(layer.load(1));
					}else{
						//转移失败
						layer.closeAll();
						layer.close(layer.load(1));
						layer.msg(data.MSG_TEXT);
					}
				}
			});
		}
	
	</script>
</html>