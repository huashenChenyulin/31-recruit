<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="false" %>
	<%@include file="/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 
  - Author(s): zengjunjie
  - Date: 2018-04-17 08:44:05
  - Description:
-->
<head>
<title>Title</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    <!-- 附件上传css和js -->
<link href="<%= request.getContextPath() %>/interpolation/css/webuploader.css" rel="stylesheet"/>
    <script src="<%= request.getContextPath() %>/interpolation/js/webuploader.nolog.min.js"></script>
    <script src="<%= request.getContextPath() %>/interpolation/js/bootstrap.min.js"></script>
   	<link href="<%= request.getContextPath() %>/interpolation/css/resumeupload.css" rel="stylesheet"/>
   	<script src="<%= request.getContextPath() %>/process/js/candidate_share.js"></script>
 
<link href="<%= request.getContextPath() %>/mail/css/uploader.css" rel="stylesheet"/> 
   <link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/Talent/css/currency.css">
  
  <%
  	String candidateId=request.getParameter("candidateId");
   %>
  
  <style type="text/css">
  body{
  	overflow: hidden;
  }
  	#main-left{
    		border: 0px solid #e3e3e3;
    	}
  	
	      #content_button{
	   	float: left;
	    width: 30%;
	    margin-left:57%;
	    margin-top:3%;
	    margin-bottom: 2%;
	   }
	   #layui-laydate1{
	   	 left: 32%!important;
    	 top: 3%!important;
	   }
	   /*上传控件样式*/
	.FileUpload{
		width: 22.4%;
	 	
		padding-left: 0px; 
    	padding-top: 0px;
	}
	.note{
		width: 51.6%;
	}
	
	#uploadFile{
		height: 42px!important;
		padding-top: 10px;
	}
	 .queueList{
		width: 100%!important;
	} 
	#uploader .statusBar{
		height: 34px!important;
		width: 100%!important;
	}
	#uploader .filelist li p.imgWrap{
		width: 256px!important;
	}
	.files{
		width: 100%!important;
	}
	#uploader .statusBar .btns{
    	top: 31px!important;
	}
	#uploader .placeholder {
	    padding-top: 0px!important;
	}
	.afile{
		  width: 231px;
	}
	
	/* 附件样式 */
		
		#file_upload .delete{
			float: right;
			cursor: pointer;
		} 
	   .recruitAcc{
	   		width: 100%;
	   }
	   #file_upload{
	   		min-height: 0px;
	   		overflow-y:auto;
	   		max-height: 205px;
	   }
  </style>  
</head>
<body>
		<!-- 左布局 -->
		<div id="main-left" style="width:77%;">
				<div id="content">
					<div class="col-lg-4">
						<label>候选人姓名<samp>*</samp></label> <input type="text" class="form-control name"
							id="name" onblur="validator()">
					</div>
					<div class="col-lg-4">
						<label>候选人电话<samp>*</samp></label> <input type="text" class="form-control phone"
							id="phone" onblur="validator()">
					</div>
					<div class="col-lg-4 parentCls">
						<label>候选人邮箱</label> <input type="text"
							class="form-control inputElem email" id="email" >
					</div>
					
					<div class="col-lg-4">
						<label>出生日期</label> <input type="text" class="form-control"
							id="birthTime" placeholder="yyyy-MM-dd">
					</div>
					
					<div class="col-lg-4">
						<label>学历<samp>*</samp></label> <select class="form-control study" id="study" onblur="validator()">
							<option></option>
						</select>
					</div>
					<div class="col-lg-4">
						<label>性别</label> <select class="form-control" id="sex">
							<option value="1">男</option>
							<option value="2">女</option>
						</select>
					</div>
					
					
					<div class="col-lg-4">
						<label>简历渠道<samp>*</samp></label> <select class="form-control channel"
							id="channel" name="channel" onchange="suppliers(this.value)" onblur="validator()">
							<option></option>
						</select>
					</div>
					<div class="col-lg-4">
						<label>供应商</label> <select class="form-control"
							id="supplier" name="supplier">
						</select>
					</div>
					<div class="col-lg-4">
						<label>期望薪资</label>
						<select class="form-control" id="expected_salary">
							<option></option>

						</select>	
					</div>
					
					<div id="content_button">
						<button type="button" class="btn btn-warning" id="success"
							name="success" onclick="update()" style="margin-right: 9%;">保 存</button>
						<button type="button" class="btn btn-default" id="reset" onclick="location.reload();">重置</button>
					</div>
				</div>
			</div>
		<div id="main-right" style="width:21%;float: right;margin-right: 10px">
			<div id="resumes">
				<label>附件:</label><br>
				<div id="file_upload" ></div>
			</div>
  			<!--上传附件 -->
  				<div id="uploader" class="wu-example" style="    margin-top: 10px;">
			        <div class="queueList">
			    	
			            <div id="dndArea" class="placeholder">
			           
			            
			                <div id="filePicker"></div>
			              	<p style="font-size: 14px;margin-top: -10px;">支持 word、excel、html、ptf、jpg 等</p>
			            </div>
			        </div>
			
			        <div class="statusBar" style="display:none;">
			           
			            <div class="info"></div>
			            <div class="btns">
			                <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
			                
			            </div>
			        </div>
				</div>
  
			</div> 
		</div>
</body>
	<script type="text/javascript">
		var j = 0;//标识id
		var candidateId='<%=candidateId %>';
		var resume_id;
		//成功或错误提示
		var layer;
		layui.use('layer', function(){
		  layer = layui.layer;
		  
		});  
		
		//默认加载
		$(function(){
			queryDisease();
			
			//时间控件
    		layui.use('laydate', function(){
			  var laydate = layui.laydate;
			  
			  //执行一个laydate实例
			  laydate.render({
			    elem: '#birthTime' //指定元素
			    ,value: '1990-01-01' //必须遵循format参数设定的格式
			    	,isInitValue: false //是否允许填充初始值，默认为 true
			  });
			});
		});
	
		//下拉框赋值
    	function queryDisease(){
    			$.ajax({
    				url: "com.recruit.talent.talent.queryDictionary.biz.ext",
					type:'POST',
					success:function(data){
						//期望薪资循环拼接赋值
						for (var i = 0; i < data.salary.length; i++) {
							$("#expected_salary")
									.append(
											"<option value='"+data.salary[i].dictID+"'>"
													+ data.salary[i].dictName
													+ "</option>");
						}
		  				//学历循环拼接赋值
		  				for(var i=0;i<data.education.length;i++){
		  					$("#study").append("<option value='"+data.education[i].dictID+"'>"+data.education[i].dictName+"</option>"); 
		  				}
			  				//渠道循环拼接赋值
						for (var i = 0; i < data.channel.length; i++) {
							$("#channel").append(
									"<option value='"+data.channel[i].dictID+"'>"
											+ data.channel[i].dictName
											+ "</option>");
						}
						
						candSelect(candidateId);//加载查询方法
					}
    			});
    		}
    		
    	//根据渠道获取供应商
		function suppliers(id,suppliers) {
			var json = {
				parentid : id
			};
			
			//如果id为空则供应商不可操作
			if(id==null || id==""){
				$("#supplier")[0].length = 0;
				$("#supplier").attr("disabled","disabled");
			}else{
				$.ajax({
					url : "com.recruit.talent.talent.queryDictionary.biz.ext",
					type : 'POST',
					data : json,
					success : function(data) {
					$("#supplier").attr("disabled",false);
						$("#supplier")[0].length = 0;
						//供应商循环拼接赋值
						for (var i = 0; i < data.supplier.length; i++) {
							$("#supplier").append(
									"<option value='"+data.supplier[i].dictid+"'>"
											+ data.supplier[i].dictname
											+ "</option>");
						}
						if(suppliers=="" || suppliers==null){
							$("#supplier").val(data.supplier[0].dictid);	//供应商
						}else{
							$("#supplier").val(suppliers);	//供应商
						}
					}
				});
			}
		}
		//根据id查询候选人信息
		function candSelect(id){
			var json={
				id:id
			}
			$.ajax({
				url : "com.recruit.process.updateCandidate.queryCandidate.biz.ext",
				type : 'POST',
				data : json,
				success : function(data) {
					
					//把date里面的值分别赋值到对应的字段
					$("#name").val(data.cand[0].cand_name);		//候选人姓名
					$("#phone").val(data.cand[0].cand_phone);	//电话
					$("#email").val(data.cand[0].cand_mail);	//邮箱
					$("#birthTime").val(data.cand[0].birthTime);	//出生日期
					$("#study").val(data.cand[0].degree);		//学历
					$("#sex").val(data.cand[0].sex);			//性别
					$("#expected_salary").val(data.cand[0].expected_salary);		//薪资
					$("#channel").val(data.cand[0].channels);	//渠道
					
					
					resume_id=data.cand[0].res_id;
					suppliers(data.cand[0].channels,data.cand[0].suppliers); //根据渠道id获取供应商 
					
					$("#supplier").val(data.cand[0].suppliers);	//供应商
					
					//判断是否存在附件
						 if(data.recruitAcc.length <= 0){
							 
							}else{
								for(var i=0;i<data.recruitAcc.length;i++){
									var accessoryUrl=basePath+"/"+data.recruitAcc[i].accessoryUrl;//拼接附件路径
									$("#file_upload").append("<p id='recruitAcc"+data.recruitAcc[i].id+"' class='recruitAcc'><a href="+accessoryUrl+"  target='_blank'>"+data.recruitAcc[i].accessoryName+"</a>"
												+"&nbsp;&nbsp;&nbsp;&nbsp;<a class='delete'"
												+"onclick='resumnedelete("+data.recruitAcc[i].id+",\""+data.recruitAcc[i].accessoryPath+"\",\""+data.recruitAcc[i].accessoryName+"\")'> X </a></p>");
								}
								
							}
				}
			});
		}
		
		//根据id修改简历表与候选人表
		function candUpdate(id){
			var name=$("#name").val();
			var phone=$("#phone").val();
			var email=$("#email").val();
			var study=$("#study").val();
			var sex=$("#sex").val();
			var expected_salary=$("#expected_salary").val();
			var channel=$("#channel").val();
			var supplier=$("#supplier").val();
			var birthTime=$("#birthTime").val();
			
			var uploadURL="";
			var uploadName="";
			var uploadPath="";
			 $(".afiles").each(function(i,obj){
    			 uploadURL += getCaption($(obj).attr("href"))+"^";
    			 uploadName += $(obj).text()+"^";
    			 uploadPath += $(obj).attr("data")+"^";
    		 });
			
			;
			
			
			var json={
				id:id,
				resume_id:resume_id,
				name:name,
				phone:phone,
				email:email,
				study:study,
				sex:sex,
				expected_salary:expected_salary,
				channel:channel,
				supplier:supplier,
				birthTime:birthTime,
				accessory_url:uploadURL,
				accessory_name:uploadName,
				accessory_path:uploadPath
			}
			$.ajax({
				url : "com.recruit.process.updateCandidate.updateCandidate.biz.ext",
				type : 'POST',
				data : json,
				success : function(data) {
					if(data.success="ok"){
						layer.msg("修改成功！");
						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index);
					}else{
						layer.msg("修改失败！");
					}
				}
			});
		}
		
		//保存方法
		function update(){
			validator();
    		//非空判断
    		if(isPass==false){
    			layer.msg(arr=[0]);
    			return;
    		}
			if(candidateId==null || candidateId==""){
				
			}else{
				isEnclosureExist(candidateId);
			}
		}
		
		 var isPass=true;//标识是否通过验证
		   var isPassMsg="";//非空验证提示输出
		    var arr=[];
	   	//失去焦点验证
	   	function validator(){
	   		isPassMsg="";
	   		isPass=true;
	   		 arr=[];
	   		//姓名
  		 	if($('.name').val()==null || $('.name').val()=="" ){
	           	$(".name").css("border","1px solid red");
	           	isPassMsg+="请输入姓名,";
	           	isPass=false;
	        }else{
	           	$(".name").css("border","1px solid #ccc");
           }
	           
	   			//电话
				var phone=/^1\d{10}$/;//电话正则
	  		 	if($('.phone').val()==null || $('.phone').val()=="" || !phone.test($(".phone").val())){
		           	$(".phone").css("border","1px solid red");
		           	isPassMsg+="请输入正确的手机号码,";
		           	isPass=false;
		        }else{
		           	$(".phone").css("border","1px solid #ccc");
	           }
	  		
	           //学历
	  		 	if($('.study').val()==null || $('.study').val()=="" ){
		           	$(".study").css("border","1px solid red");
		           	isPassMsg+="请选择学历,";
		           	isPass=false;
	           }else{
	           		$(".study").css("border","1px solid #ccc");
	           }
	   		
	           //渠道
	  		 	if($('.channel').val()==null || $('.channel').val()==""){
		           	$(".channel").css("border","1px solid red");
		           	isPassMsg+="请选择渠道";
		           	isPass=false;
	           }else{
	           		$(".channel").css("border","1px solid #ccc");
	           }
	           arr=isPassMsg.split(",");
	   	} 
	   	
	   	//返回方法
		function historyGo(){
			//遍历触发点击方法
			$(".deleteUrl").each(function(i,obj){
				$(obj).find(".delete").click();
				
			});
		}
     	
     	//附件是否还有未上传的
		function isEnclosureExist(id){
			var html=$("#uploader .filelist").html();
			if(html!="" && html!=null){
				layer.msg('还有附件未上传！');
			}else{
				candUpdate(id);//保存方法
			}
		}
	   	
	</script>
	<script src="<%= request.getContextPath() %>/Talent/js/resumeupload.js"></script>
</html>